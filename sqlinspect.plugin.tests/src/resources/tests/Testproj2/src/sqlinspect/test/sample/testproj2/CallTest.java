package sqlinspect.test.sample.testproj2;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

public class CallTest {

	private Connection conn;

	public void foo1(String asd) {
		foo2(asd + "1");
	}

	public void foo2(String asd) {
		foo3(asd + "2");
	}

	public void foo3(String asd) {
		foo4(asd + "3");
	}

	public void foo4(String asd) {
		foo5(asd + "4");
	}

	public void foo5(String asd) {
		foo6(asd + "5");
	}

	public void foo6(String asd) {
		foo7(asd + "6");
	}

	public void foo7(String asd) {
		foo8(asd + "7");
	}

	public void foo8(String asd) {
		foo9(asd + "8");
	}

	public void foo9(String asd) {
		foo10(asd + "9");
	}

	public void foo10(String asd) {
		foo11(asd + "A");
	}

	public void foo11(String asd) {
		foo12(asd + "B");
	}

	public void foo12(String asd) {
		foo13(asd + "C");
	}

	public void foo13(String asd) {
		foo14(asd + "D");
	}

	public void foo14(String asd) {
		foo15(asd + "E");
	}

	public void foo15(String asd) {
		test_sql(conn, asd + "F");
	}

	public void test_sql(Connection conn, String asd) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(asd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
