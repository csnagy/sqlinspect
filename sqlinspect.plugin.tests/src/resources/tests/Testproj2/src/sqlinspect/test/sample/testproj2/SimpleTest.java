package sqlinspect.test.sample.testproj2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SimpleTest {
	private static final String DB_URL = "jdbc:mysql://localhost/jdbcdemo";
	private static final String DB_USER = "demo";
	private static final String DB_PASSWORD = "demo";

	private static final String TABLE_CUSTOMERS = "customers";

	private Connection conn = null;

	/**
	 * Initialize the connection to the MySQL server.
	 */
	public void init() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("Could not connect to the database!");
			e.printStackTrace();
		}
	}

	/**
	 * Execute example queries.
	 */
	public void run() throws Exception {
		init();

		// test1(1999);

		close();
	}

	@SuppressWarnings("unused")
	private void test1(int year) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM " + TABLE_CUSTOMERS;

			if (year > 10) {
				sql += " WHERE year >" + year;
			}

			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, year);
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void test2(int year) throws SQLException {
		String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
		while (year > 200) {
			sql = sql + " WHERE true AND year > " + year;
		}

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);
		ResultSet rs = stmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test2b(int year) throws SQLException {
		String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
		if (year > 200) {
			sql = sql + " WHERE true AND year > " + year;
		}

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);
		ResultSet rs = stmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test3(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		int x = 0;

		String s = "SELECT * FROM t";
		while (x < 10) {
			s = s + " WHERE x = " + x;
		}
		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test4(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		int x = 0;

		String s = "SELECT * FROM t WHERE x = " + null;

		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	/**
	 * Close connection.
	 */
	private void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.err.println("Could not close the database connection!");
			e.printStackTrace();
		}
	}

}
