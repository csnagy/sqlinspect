package sqlinspect.test.sample.testproj2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExtractorTest {
	private static final String DB_URL = "jdbc:mysql://localhost/jdbcdemo";
	private static final String DB_USER = "demo";
	private static final String DB_PASSWORD = "demo";

	private static final String TABLE_CUSTOMERS = "customers";

	private Connection conn = null;

	/**
	 * Initialize the connection to the MySQL server.
	 */
	public void init() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("Could not connect to the database!");
			e.printStackTrace();
		}
	}

	/**
	 * Execute example queries.
	 */
	public void run() throws Exception {
		init();

		// test1(1999);

		close();
	}
//
//	@SuppressWarnings("unused")
//	private void test1(int year) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
//
//			stmt = conn.prepareStatement(sql);
//			stmt.setInt(1, year);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@SuppressWarnings("unused")
//	private void test2(int year) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
//			
//			if (year > 10) {
//				sql = sql + " WHERE year > 10";
//			}
//			
//			stmt = conn.prepareStatement(sql);
//			stmt.setInt(1, year);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@SuppressWarnings("unused")
//	private void test3(int year) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
//			
//			if (year > 10) {
//				sql += " WHERE year > 10";
//			}
//			
//			stmt = conn.prepareStatement(sql);
//			stmt.setInt(1, year);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@SuppressWarnings("unused")
//	private void test4(int year) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
//			String cond = " year > ";
//			int x = 10;
//			int y = 20;
//
//			if (year > 10) {
//				sql = sql + " WHERE " + cond + x + " AND " + cond + y;
//			}
//
//			stmt = conn.prepareStatement(sql);
//			stmt.setInt(1, year);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}

//	@SuppressWarnings("unused")
//	private void test5(int year) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String s = "A";
//			String s2 = s.concat("asd");
//			stmt = conn.prepareStatement(s2);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	@SuppressWarnings("unused")
//	private void test6(int year) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String s = "A";
//			bar(s);
//			stmt = conn.prepareStatement(s);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	public void test6(int id, Connection c) {
//		StringBuffer sql = new StringBuffer(12);
//		sql.append("A").append("B").append("C");
//		try {
//			PreparedStatement pstmt = c.prepareStatement(sql.toString());
//			ResultSet rs = pstmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	public void test7(int id, Connection c) {
//		StringBuffer sql = new StringBuffer(12);
//		sql.append("qwe", 12, 13).append("asd");
//		try {
//			PreparedStatement pstmt = c.prepareStatement(sql.toString());
//			ResultSet rs = pstmt.executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}

//	public void test7(int year, Connection c, boolean condition) {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "A";
//			sql += "B";
//			sql += "C";
//			sql += "D";
//
//			stmt = conn.prepareStatement(sql);
//			rs = stmt.executeQuery();
//		} catch (SQLException e) {
//			System.err.println("Error sending the SQL to the database!");
//			e.printStackTrace();
//		} finally {
//		}
//	}
//
	public void test8(int year, Connection c, boolean condition) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "A";
		if (condition)
			sql += "B";
		sql += "C";

		stmt = conn.prepareStatement(sql);
		rs = stmt.executeQuery();
	}
//
//	public void test9(int year, Connection c, boolean condition) throws SQLException {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		String sql = "A";
//		if (condition) {
//			String cond = "1";
//			sql += cond;
//		}
//		sql += "C";
//
//		stmt = conn.prepareStatement(sql);
//		rs = stmt.executeQuery();
//	}

//	public void test10(int year, Connection c, boolean condition) throws SQLException {
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		String sql = "SELECT firstname, lastname, address FROM " + TABLE_CUSTOMERS;
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		System.out.println(sql);
//		stmt = conn.prepareStatement(sql);
//		rs = stmt.executeQuery();
//	}

	public void test11(int year, Connection c, boolean condition) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT firstname, lastname, address FROM " + TABLE_CUSTOMERS;

		if (condition) {
			String cond = " WHERE";
			sql += cond;
		}

		if (true)
			sql += " ";

		stmt = conn.prepareStatement(sql);
		rs = stmt.executeQuery();
	}
	
	
	public void test12(int year, Connection c, boolean condition) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT firstname, lastname, address FROM " + TABLE_CUSTOMERS;

			if (condition) {
				String cond = " WHERE";
				cond += " address != NULL";
				cond = cond + " AND year(dob) > ?";
				sql += cond;
			}

			if (true)
				sql += " ";

			System.out.println(sql);
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, year);

			System.out.println("Results of the query: " + sql);
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			System.err.println("Error sending the SQL to the database!");
			e.printStackTrace();
		} finally {
		}
	}

	private String foo() {
		return "ASD";
	}

	private void bar(String s) {
		s.concat("ASD");
	}

	/**
	 * Close connection.
	 */
	private void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.err.println("Could not close to the database connection!");
			e.printStackTrace();
		}
	}

}
