package sqlinspect.test.sample.testproj2;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

public class CallTest2 {

	private Connection conn;

	public void foo1(String asd) {
		test_sql(conn);
	}

	public void foo2(String asd) {
		test_sql(conn, "1");
	}

	public void foo3(String asd) {
		test_sql(conn, "1", "2");
	}

	public void test_sql(Connection conn, String... asd) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			StringBuilder sb = new StringBuilder();
			for (String s : asd) {
				sb.append(s);
			}
			stmt.executeUpdate(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
