package sqlinspect.test.sample.testproj2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Test1 {
	private final String TABLE_CUSTOMERS = "customers";

	public void test1(int year, boolean condition, Connection conn) throws SQLException {
		String sql = "SELECT firstname, lastname, address FROM " + TABLE_CUSTOMERS ;

		if (condition) {
			String cond = " WHERE";
			cond += " address != NULL";
			cond = cond + " AND year(dob) > ?";
			sql += cond;
		}

		if (true)
			sql += " ";

		System.out.println(sql);
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);

		System.out.println("Results of the query: " + sql);
		ResultSet rs = stmt.executeQuery();
	}
}
