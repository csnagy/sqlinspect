package sqlinspect.test.sample.testproj;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ExtractorTest {
	private static final String DB_URL = "jdbc:mysql://localhost/jdbcdemo";
	private static final String DB_USER = "demo";
	private static final String DB_PASSWORD = "demo";

	private static final String TABLE_CUSTOMERS = "customers";

	private Connection conn = null;

	/**
	 * Initialize the connection to the MySQL server.
	 */
	public void init() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
			if (conn != null) {
				Statement stmt = conn.createStatement();

				// ONLY_FULL_GROUP_BY mode in MySQL can be used to force MySQL
				// warn when a non-aggregated column references a column which
				// do not appear in the group by list.
				// For this demo, we turn this off, if it was on.
				stmt.execute("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("Could not connect to the database!");
			e.printStackTrace();
		}
	}

	/**
	 * Execute example queries.
	 */
	public void run() throws Exception {
		init();

		// test1(1999);

		close();
	}

	@SuppressWarnings("unused")
	private void test1(int year) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT firstname, lastname, address FROM " + TABLE_CUSTOMERS;

			String cond = " WHERE";
			cond += " address != NULL";
			cond = cond + " AND year(dob) > ?";

			sql += cond;

			System.out.println(sql);
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, year);

			System.out.println("Results of the query: " + sql);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				String address = rs.getString("address");

				System.out.println("firstname: " + firstname + " lastname: " + lastname + " address: " + address);
			}
		} catch (SQLException e) {
			System.err.println("Error sending the SQL to the database!");
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				System.err.println("Error disposing resources!");
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unused")
	private void test2(int year, boolean condition) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT firstname, lastname, address FROM " + TABLE_CUSTOMERS;

			if (condition) {
				String cond = " WHERE";
				cond += " address != NULL";
				cond = cond + " AND year(dob) > ?";
				sql += cond;
			}

			if (true)
				sql += " ";

			System.out.println(sql);
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, year);

			System.out.println("Results of the query: " + sql);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				String address = rs.getString("address");

				System.out.println("firstname: " + firstname + " lastname: " + lastname + " address: " + address);
			}
		} catch (SQLException e) {
			System.err.println("Error sending the SQL to the database!");
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				System.err.println("Error disposing resources!");
				e.printStackTrace();
			}
		}

	}

	public static final String PLANNING_TASK = "TSK";
	public static final String PLANNING_REQUIREMENT = "REQ";
	public static final String PLANNING_PROJECT = "PRJ";
	public static final String PLANNING_OCCURENCE = "OCC";
	public static final String PLANNING_RISK = "RSK";
	public static final String PLANNING_COST = "CST";
	public static final String PLANNING_EXPENSE = "EXP";
	public static final String PLANNING_ARTIFACT = "ART";
	public static final String PLANNING_INVOICE = "INV";

	@SuppressWarnings("unused")
	private void test3(String projectId, Connection c, String uto) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;

		String keyWhere = "as";
		if (!keyWhere.equals("")) {
			keyWhere = " AND (" + keyWhere + ")";
		}

		String projectWhere = "", projProjWhere = "";
		if (projectId != null && !projectId.trim().equals("")) {
			projectWhere = " and project_id='" + projectId + "'";
			projProjWhere = " and id='" + projectId + "'";
		}

		String projectResource = "", projProjResource = "";
		projectResource = " and project_id in (select project_id from resource where id='" + uto + "')";
		projProjResource = " and id in (select project_id from resource where id='" + uto + "')";

		String sql = "select p.id, p.description, p.creation_date, p.final_date, " + "(select '" + PLANNING_TASK
				+ "' from task where id=p.id " + projectWhere + projectResource + ") as IS_TSK, " + "(select '"
				+ PLANNING_REQUIREMENT + "' from requeriment where id=p.id " + projectWhere + projectResource
				+ ") as IS_REQ, " + "(select '" + PLANNING_PROJECT + "' from project where id=p.id " + projProjWhere
				+ projProjResource + ") as IS_PRJ, " + "(select '" + PLANNING_OCCURENCE
				+ "' from occurrence where id=p.id " + projectWhere + projectResource + ") as IS_OCC, " + "(select '"
				+ PLANNING_RISK + "' from risk where id=p.id " + projectWhere + projectResource + ") as IS_RSK, "
				+ "t.name as taskname, r.name as riskname, o.name as occname, pr.name as projname " + "from planning p "
				+ "LEFT OUTER JOIN task t on p.id = t.id " + "LEFT OUTER JOIN risk r on p.id = r.id "
				+ "LEFT OUTER JOIN occurrence o on p.id = o.id " + "LEFT OUTER JOIN project pr on p.id = pr.id "
				+ "where p.description is not null and p.id<>'0'" + keyWhere;
		pstmt = c.prepareStatement(sql);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test4(int x, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;

		String s = "A";
		if (x > 10) {
			s += "B";
			if (x > 20) {
				s += "C";
			} else {
				s += "D";
			}
		} else {
			s += "E";
		}
		s += "F";
		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test5(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		int x = 0;
		x++;

		String s = "SELECT * from customers WHERE x=" + x;
		try {
			s += " AND true";
		} catch (Exception e) {
			s += " AND false";
		} finally {
			s += " AND x>10";
		}

		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test6(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		int x = 0;

		String s = "SELECT * FROM t";
		while (x < 10) {
			s = s + " WHERE x = " + x;
			pstmt = c.prepareStatement(s);
			rs = pstmt.executeQuery();
		}
	}

	@SuppressWarnings("unused")
	private void test7(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		int x = 0;

		String s = "SELECT * FROM t";
		while (x < 10) {
			s = s + " WHERE x = " + x;
		}
		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test7b(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		int x = 0;

		String s = "SELECT * FROM t";
		do {
			s = s + " WHERE x = " + x;
		} while (x < 10);
		
		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}
	
	
	@SuppressWarnings("unused")
	private void test8(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String s = "SELECT x FROM test";

		for (int x = 0; x < 10; x++) {
			s = s + " WHERE x = " + x;
		}
		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test9(int y, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String s = "SELECT * FROM test";
		int arr[] = { 1, 2, 3, 4, 5, 6 };

		for (int x : arr) {
			s = s + " WHERE x = " + x;
		}

		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void test10(int x, Connection c) throws SQLException {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String s = "SELECT * FROM test";

		if (x > 10) {
			if (x > 11) {
				if (x > 12) {
					if (x > 13) {
						if (x > 14) {
							if (x > 15) {
								if (x > 16) {
									if (x > 17) {
										if (x > 18) {
											s += " WHERE x > 13";
										}
									}
								}
							}
						}
					}
				}
			}
		}

		pstmt = c.prepareStatement(s);
		rs = pstmt.executeQuery();
	}

	public void test11(int id) {
		String sql = "SELECT Name FROM AD_Val_Rule WHERE AD_Val_Rule_ID= " + id;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		pstmt = DB.prepareStatement(sql, "ASD");

		try {
			rs = pstmt.executeQuery();
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}

	public void test12(int id) {
		String sql = "SELECT Name, TableName " + "FROM AD_Table " + "WHERE TableName LIKE '%_Trl' " + "ORDER BY Name";
		ArrayList<String> trlTables = new ArrayList<String>();
		try {
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
				trlTables.add(rs.getString(2));
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test13(int id) {
		String sql = "SELECT * from test where id > " + id;
		ArrayList<String> trlTables = new ArrayList<String>();
		try {
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
				trlTables.add(rs.getString(2));
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test14(int id, Connection c) {
		StringBuffer sql = new StringBuffer("SELECT * from test where id > ").append(id).append(" OR x > ").append(12);
		try {
			PreparedStatement pstmt = c.prepareStatement(sql.toString());
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test15(int id, Connection c) {
		StringBuffer sql = new StringBuffer(12);
		sql.append("qwe", 12, 13).append("asd");
		sql.append("foo");
		try {
			PreparedStatement pstmt = c.prepareStatement(sql.toString());
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test16(int id, Connection c) {
		StringBuffer sql = new StringBuffer("test");
		sql.append("foo");
		try {
			PreparedStatement pstmt = c.prepareStatement(sql.toString());
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test17(int id) {
		String sql = "SELECT * from test where id > " + id;
		ArrayList<String> trlTables = new ArrayList<String>();
		try {
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
				trlTables.add(rs.getString(2));
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test18(int id, Connection c) {
		String sql = "SELECT * from test where id > " + id;
		ArrayList<String> trlTables = new ArrayList<String>();
		try {
			PreparedStatement pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
				trlTables.add(rs.getString(2));
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test19(int x) {
		String sql = "A";
		if (x > 10) {
			sql += "B";
			if (x > 15) {
				sql += "C";
				if (x > 15) {
					sql += "1";
				} else {
					sql += "2";
				}
				sql += "3";
				if (x > 20) {
					sql += "D";
				}
				sql += "E";
			} else {
				sql += "F";
			}
			sql += "G";
		}
		sql += "H";
		// ABC13DEGH
		// ABC23DEGH
		// ABC13EGH
		// ABC23EGH
		// ABFGH
		// AH
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test20(int x) {
		String s = "A";
		String q = "1";
		if (x > 10) {
			s += "B";
			q += "2";
			if (x > 15) {
				s += "C";
				q += "3";
				if (x > 15) {
					s += "D";
					q += "4";
				} else {
					s += "E";
					q += "5";
				}
				s += "F";
				q += "6";
				if (x > 20) {
					s += "G";
					q += "7";
				}
				s += "H";
				q += "8";
			} else {
				s += "I";
				q += "9";
			}
			s += "J";
			q += "0";
		}
		s += "K";
		q += "|";
		try {
			PreparedStatement pstmt = conn.prepareStatement(s + q);
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test_inter1(int id) {
		String sql = "SELECT * from test where id > " + id;
		test_inter1_sub(sql);
	}

	public void test_inter1_sub(String sql) {
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test_inter2(int id) {
		String sql1 = "SELECT * from ";
		String sql2 = " WHERE id >10 ";
		if (id > 10) {
			test_inter2_sub(sql1 + TABLE_CUSTOMERS, sql2, id);
		} else {
			test_inter2_sub(sql1, "", id);
		}
	}

	private void test_inter2_sub(String sql1, String sql2, int id) {
		try {
			String sql = sql1 + sql2 + Integer.toString(id);
			PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void test_inter3_1(int id) {
		String cond = "WHERE id = " + Integer.toString(id);
		test_inter3_2(cond);
	}

	private void test_inter3_2(String cond) {
		test_inter3_3(cond);
	}

	private void test_inter3_3(String cond) {
		test_inter3_4(cond);
	}

	private void test_inter3_4(String cond) {
		test_inter3_5(cond);
	}

	private void test_inter3_5(String cond) {
		test_inter3_6(cond);
	}

	private void test_inter3_6(String cond) {
		try {
			String sql = "SELECT * FROM t " + cond;
			PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = pstmt.executeQuery();
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test21(int x) {
		StringBuffer sql = new StringBuffer();
		if (x > 12)
			sql.append("UPDATE ").append("ASD").append(" SET DatePrinted=SysDate,IsPrinted='Y' WHERE ").append("QWE")
					.append("ASD").append("QWE");
		if (sql.length() > 0) {
			Statement stmt;
			try {
				stmt = conn.createStatement();
				@SuppressWarnings("unused")
				int no = stmt.executeUpdate(sql.toString());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void test22(int x) {
		StringBuffer buf = new StringBuffer();
		buf.append("asd");
		String sql = buf.toString();
		Statement stmt;
		try {
			stmt = conn.createStatement();
			@SuppressWarnings("unused")
			int no = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test23(String s) {
		String s1 = s;
		String s2 = s1;
		String s3 = s2;
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(s3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test2(String s) {
		String s1 = "asd";
		String s2 = "qwe";
		String s3 = s + s1 + s2;
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(s3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test_inter1(String asd, int i) {
		String s = "asd" + asd;
		test_inter1_foo(s + Integer.toString(i));
	}

	public void test_inter1_foo(String asd) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(asd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test_inter2A() {
		test_inter2_foo("1", "2");
	}

	public void test_inter2B() {
		test_inter2_foo("A", "B");
	}

	public void test_inter2_foo(String s1, String s2) {
		Statement stmt;
		String s = s1 + s2;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(s);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test_recursion1(String asd, int i) {
		if (i < 10) {
			test_recursion1(asd + Integer.toString(i), i + 1);
		} else {
			Statement stmt;
			try {
				stmt = conn.createStatement();
				stmt.executeUpdate(asd);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void test_cfgdepth(int x) {
		Statement stmt;
		try {
			String asd = "0";
			if (x > 10) {
				asd += "1";
				if (x > 20) {
					asd += "2";
					if (x > 30) {
						asd += "3";
						if (x > 40) {
							asd += "4";
							if (x > 50) {
								asd += "5";
								if (x > 60) {
									asd += "6";
									if (x > 70) {
										asd += "7";
										if (x > 80) {
											asd += "8";
											if (x > 90) {
												asd += "9";
												if (x > 100) {
													asd += "A";
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			stmt = conn.createStatement();
			stmt.executeUpdate(asd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void testParens(String s) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(("SELECT * FROM " + s));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void testConditionalExpr1(int x, String s) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(("SELECT * FROM " + s + (x > 10 ? " WHERE " + x : " WHERE 10")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void testMore1(int x, String s) {
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("Select * from asdf");
			if (x>10) {
				System.out.println("asd");
			}
			if (x>20) {
				System.out.println("asd");
			}
			if (x>30) {
				System.out.println("asd");
			}
			if (x>40) {
				System.out.println("asd");
			}
			stmt.executeUpdate();
			stmt = conn.prepareStatement("update asdf set name=123");
			for (int i = 0; i<x; x++) {
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void testMore2(int x, String s) {
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("Select * from asdf");
			if (x>10) {
				System.out.println("asd");
			}
			if (x>20) {
				System.out.println("asd");
			}
			if (x>30) {
				System.out.println("asd");
			}
			if (x>40) {
				System.out.println("asd");
			}
			stmt.executeUpdate();
			for (int i = 0; i<x; x++) {
				stmt = conn.prepareStatement("update asdf set name=123");
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void testSetters(int x, String s) {
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("Select * from asdf where x>?");
			stmt.setInt(1, 1);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Close connection.
	 */
	private void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.err.println("Could not close the database connection!");
			e.printStackTrace();
		}
	}

}
