package sqlinspect.com.androidproject;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import sqlinspect.com.androidproject.datasets.MyDatabase;
import sqlinspect.com.androidproject.datasets.SmellyTable;
import sqlinspect.com.androidproject.datasets.TestTable;

public class MainActivity extends AppCompatActivity {


    private static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SQLiteDatabase db = MyDatabase.getDatabase().getWritableDatabase();
                TestTable.insertTestData(db, 1, 1, "Test1");
                TestTable.insertTestData(db, 2, 2, "Test2");

                final TextView tv = findViewById(R.id.textview);
                tv.setText("Test data inserted!");
            }
        });

        final Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SQLiteDatabase db = MyDatabase.getDatabase().getWritableDatabase();
                List<String> res = TestTable.getTestNames(db);
                final StringBuffer sb = new StringBuffer("Res: ");
                for (String s : res) {
                    sb.append(s).append(" ");
                }
                final TextView tv = findViewById(R.id.textview);
                tv.setText(sb.toString());
            }
        });


        final Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SQLiteDatabase db = MyDatabase.getDatabase().getReadableDatabase();
                List<String> res = SmellyTable.smellySelect(db);
                final TextView tv = findViewById(R.id.textview);
                final StringBuffer sb = new StringBuffer("Res: ");
                for (String s : res) {
                    sb.append(s).append(" ");
                }
                tv.setText(sb.toString());
            }
        });

        final Button button4 = findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SQLiteDatabase db = MyDatabase.getDatabase().getWritableDatabase();
                SmellyTable.insertTable2Data(db, 1,"Test1");
//                SmellyTable.insertTable3Data(db, 2, "Test2");

                final TextView tv = findViewById(R.id.textview);
                tv.setText("Smelly data inserted!");
            }
        });

        final Button button5 = findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyDatabase.reset();

                final TextView tv = findViewById(R.id.textview);
                tv.setText("DB reset!");
            }
        });

    }

    public static Context getContext() {
        return mContext;
    }

}
