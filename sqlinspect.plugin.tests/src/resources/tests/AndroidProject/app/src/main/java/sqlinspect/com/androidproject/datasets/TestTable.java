package sqlinspect.com.androidproject.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import sqlinspect.com.androidproject.model.Test;
import sqlinspect.com.androidproject.util.SqlUtils;

public class TestTable {
    protected static void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_tests ("
                + " test_id INTEGER DEFAULT 0,"
                + " user_id INTEGER DEFAULT 0,"
                + " name TEXT,"
                + " PRIMARY KEY (test_id, user_id))");

        db.execSQL("CREATE TABLE tbl_test_likes("
                + " test_id INTEGER DEFAULT 0,"
                + " user_id INTEGER DEFAULT 0,"
                + " PRIMARY KEY (test_id, user_id))");
    }

    public static void insertTestData(SQLiteDatabase db, int tid, int uid, String name) {
        db.execSQL("INSERT INTO tbl_tests (test_id, user_id, name) values("+tid+","+uid+",'"+name+"')");
    }

    public static List<String> getTestNames(SQLiteDatabase db) {
        List<String> ret = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT name from tbl_tests", null);
        while (cursor.moveToNext()) {
            ret.add(cursor.getString(0));
        }
        cursor.close();
        return ret;
    }


    protected static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_tests");
        db.execSQL("DROP TABLE IF EXISTS tbl_test_likes");
    }

    protected static void reset(SQLiteDatabase db) {
        dropTables(db);
        createTables(db);
    }

    protected static int purge(SQLiteDatabase db) {
        int numDeleted = db.delete("tbl_tests", null, null);
        numDeleted += db.delete("tbl_test_likes", "test_id NOT IN (SELECT DISTINCT test_id FROM tbl_tests)",
                null);
        return numDeleted;
    }

    public static List<Integer> getLikesForPost(Test test) {
        List<Integer> userIds = new ArrayList<>();
        if (test == null) {
            return userIds;
        }

        String[] args = {Long.toString(test.testId)};
        Cursor c = MyDatabase.getReadableDb()
                .rawQuery("SELECT user_id FROM tbl_test_likes WHERE test_id=?", args);
        try {
            if (c.moveToFirst()) {
                do {
                    userIds.add(new Integer((int)c.getLong(0)));
                } while (c.moveToNext());
            }

            return userIds;
        } finally {
            SqlUtils.closeCursor(c);
        }
    }

    public static int getNumLikesForTest(Test test) {
        if (test == null) {
            return 0;
        }
        String[] args = {Long.toString(test.testId) };
        return SqlUtils.intForQuery(MyDatabase.getReadableDb(),
                "SELECT count(*) FROM tbl_test_likes WHERE test_id=? AND test_id=?", args);
    }

    public static void setCurrentUserLikesTest(Test test, boolean isLiked, long userId) {
        if (test == null) {
            return;
        }
        if (isLiked) {
            ContentValues values = new ContentValues();
            values.put("test_id", test.testId);
            values.put("user_id", userId);
            MyDatabase.getWritableDb().insert("tbl_test_likes", null, values);
        } else {
            String[] args = {Long.toString(test.testId), Long.toString(userId)};
            MyDatabase.getWritableDb().delete("tbl_test_likes", "test_id=? AND user_id=?", args);
        }
    }

    public static void setLikesForPost(Test test, List<Integer> userIds) {
        if (test == null) {
            return;
        }

        SQLiteDatabase db = MyDatabase.getWritableDb();
        db.beginTransaction();
        SQLiteStatement stmt =
                db.compileStatement("INSERT INTO tbl_test_likes (test_id, user_id) VALUES (?1,?2)");
        try {
            // first delete all likes for this test
            String[] args = {Long.toString(test.testId)};
            db.delete("tbl_test_likes", "test_id=?", args);

            // now insert the passed likes
            if (userIds != null) {
                stmt.bindLong(1, test.testId);
                for (Integer uId : userIds) {
                    stmt.bindLong(2, uId);
                    stmt.execute();
                }
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            SqlUtils.closeStatement(stmt);
        }
    }
}
