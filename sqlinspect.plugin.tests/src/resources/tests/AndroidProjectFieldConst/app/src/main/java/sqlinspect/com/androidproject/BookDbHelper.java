
package sqlinspect.com.androidproject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import sqlinspect.com.androidproject.DatabaseSchema.BookEntry;

// code snippet from https://github.com/codinguser/gnucash-android/blob/master/app/src/main/java/org/gnucash/android/db/BookDbHelper.java
public class BookDbHelper extends SQLiteOpenHelper {

    private static final String BOOKS_TABLE_CREATE = "CREATE TABLE " + BookEntry.TABLE_NAME + " ("
            + BookEntry._ID 		         + " integer primary key autoincrement, "
            + BookEntry.COLUMN_UID 		     + " varchar(255) not null UNIQUE, "
            + BookEntry.COLUMN_DISPLAY_NAME  + " varchar(255) not null, "
            + BookEntry.COLUMN_ROOT_GUID     + " varchar(255) not null, "
            + BookEntry.COLUMN_TEMPLATE_GUID + " varchar(255), "
            + BookEntry.COLUMN_ACTIVE        + " tinyint default 0, "
            + BookEntry.COLUMN_SOURCE_URI    + " varchar(255), "
            + BookEntry.COLUMN_LAST_SYNC     + " TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "
            + BookEntry.COLUMN_CREATED_AT    + " TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "
            + BookEntry.COLUMN_MODIFIED_AT   + " TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP "
            + ")");

    public BookDbHelper(Context context) {
        super(context, DatabaseSchema.BOOK_DATABASE_NAME, null, DatabaseSchema.BOOK_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BOOKS_TABLE_CREATE);
    }

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
	}
}
