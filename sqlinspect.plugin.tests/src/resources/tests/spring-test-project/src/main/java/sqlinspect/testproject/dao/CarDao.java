package sqlinspect.testproject.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import sqlinspect.testproject.model.Car;

public interface CarDao {
	public List<Car> getCars() throws DataAccessException;

	public Car getCar(Long carId) throws DataAccessException;
}