package sqlinspect.testproject.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sqlinspect.testproject.model.Car;

@Repository
public class CarDaoImpl2 extends HibernateDaoSupport implements CarDao {

	@Transactional
	public List<Car> getCars() throws DataAccessException {
		Session s = getSession();
		org.hibernate.Query query = s.createQuery("select c from Car c");
		List<Car> resultList = query.list();
		return resultList;
	}

	@Transactional
	public Car getCar(Long carId) throws DataAccessException {
		return null;
	}
}