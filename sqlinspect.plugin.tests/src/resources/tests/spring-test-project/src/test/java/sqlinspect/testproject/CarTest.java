package sqlinspect.testproject;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sqlinspect.testproject.dao.CarDao;
import sqlinspect.testproject.model.Car;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class CarTest {

	@Autowired
	private CarDao carDao;

	@Autowired
	private EntityManagerFactory emf;

	private Logger logger = Logger.getLogger("myLog");
	private static Long id = 1L;

	@Before
	public void init() {
		int carNumber = carDao.getCars().size();
		Car c = new Car();
		c.setId(carNumber + 1);
		c.setCompany("Tesla");
		c.setModel("X");
		c.setPrice(100000);
		EntityManager em = emf.createEntityManager();
		Session s = (Session) em.unwrap(Session.class);
		s.persist(c);
		s.flush();
	}

	@Test
	public void listCarsTest() {
		List<Car> cars = carDao.getCars();
//      logger.info("Cars: " + cars.size());
		Assert.assertNotNull(cars);
		Assert.assertEquals(1, cars.size());
	}

	@Test
	public void getCarTest() {
		Car car = carDao.getCar(1L);
		Assert.assertEquals(id.longValue(), car.getId());
		Assert.assertEquals("X", car.getModel());
	}
}