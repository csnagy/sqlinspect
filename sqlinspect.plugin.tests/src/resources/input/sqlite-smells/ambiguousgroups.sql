create table fear (x int, y int, z int not null);
select x, max(y), z from fear group by x; -- should warn for z
select x, max(y), z from test group by x; -- should not warn, as references could not be resolved

SELECT x, CAST((y / 86400000) AS int) AS d, COUNT(*) AS cnt FROM fear WHERE z=1 GROUP BY x, d; -- should not warn

create table t (_id, x int);
create table t2 (_id, y int);
SELECT t._id, x,  COUNT(t2._id) AS count FROM t LEFT OUTER JOIN t2 ON t2.y = t._id GROUP BY t._id; -- should warn for x
