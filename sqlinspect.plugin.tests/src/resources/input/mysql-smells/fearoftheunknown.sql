select x+13 from fear;     -- x is nullable and used in expression without null check
select y+13 from fear;     -- y is nullable with NULL value in the table and used in expression without null chec
select y||'asd' from fear; -- y is nullable with NULL value in the table and used in expression without null chec
select z*0 from fear;      -- no smell
select * from fear where z = null;  -- bad null test
select * from fear where y is null; -- no smell
select * from fear where x <> null; -- bad null test

select x,z from fear where z is null or z > 10;      -- no smell
select x,z from fear where z is not null and z>10;   -- no smell
select x,z from fear where z is not null or z > 10;  -- no smell
select x,y from fear where z is null and z>10;       -- no smell
select x from fear where x is null or x=0;      -- no smell

select x,y from nofear join fear on nofear.x = nofear.y; -- no smell
select x,y from nofear, fear where nofear.x = fear.y; -- no smell
select x from fear where x = ?; -- x is nullable and used in expression without null check
