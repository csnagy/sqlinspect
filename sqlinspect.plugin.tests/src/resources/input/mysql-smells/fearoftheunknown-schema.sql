drop table if exists fear;
create table fear (x int, y int, z int not null);
create table fear2 (x int, y int);
insert into fear (x,y,z) values (11, 11, 11);
insert into fear (x,y,z) values (12, null, 12);
insert into fear2 (x,y) values (11, 11);
insert into fear2 (x,y) values (12, 12);