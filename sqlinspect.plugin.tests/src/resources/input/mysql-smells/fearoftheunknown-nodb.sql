select x+13 from nofear;
select y+13 from nofear;
select y||'asd' from nofear;
select z*0 from nofear;
select * from nofear where z = null;
select * from nofear where y is null;
select * from nofear where x <> null;

select x,z from nofear where z is null or z > 10;
select x,z from nofear where z is not null and z > 10;
select x,z from nofear where z is not null or z > 10;
select x,y from nofear where z is null and z > 10;

update nofear set z=null where z > 10;
update nofear set z=10 where z is null;
update nofear set z=10 where z = null;
