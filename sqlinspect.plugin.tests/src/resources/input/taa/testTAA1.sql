create table test(id int, s varchar(255));

select * from test;
select id, s from test;

alter table test add column x int;
select x from test;

drop table test;