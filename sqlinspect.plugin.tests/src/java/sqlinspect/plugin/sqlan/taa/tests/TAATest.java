package sqlinspect.plugin.sqlan.taa.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.sqlan.taa.ITAAReporter;
import sqlinspect.plugin.sqlan.tests.AbstractPluginTest;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.parser.SQLParser;
import sqlinspect.sql.parser.SymbolTable;
import sqlinspect.sql.parser.VisitorExtraEdges;

class TAATest extends AbstractPluginTest {

	private static final File inputDir = new File(TEST_INPUT_DIR, "taa");
	private static final File refDir = new File(TEST_REF_DIR, "taa");
	private static final File outputDir = new File("taa");

	private final SQLDialect dialect = SQLDialect.SQLITE;

	private void removePaths(JsonNode root) {
		for (JsonNode db : root.get("Databases")) {
			for (JsonNode stmt : db.get("Statements")) {
				if (stmt.isObject()) {
					((ObjectNode) stmt).remove("Path");
				}
			}
		}
	}

	protected static void createOutputDir() throws IOException {
		if (!outputDir.exists() && !outputDir.mkdir()) {
			throw new IOException("Could not create directory: " + outputDir);
		}
	}

	@BeforeAll
	static void beforeAll() throws IOException {
		createOutputDir();
	}

	protected void testFile(String testname) throws JsonParseException, JsonMappingException, IOException {
		File input = new File(inputDir, testname + ".sql");
		File output = new File(outputDir, testname + ".json");
		File outputTAA = new File(outputDir, testname + "-taa.json");
		File reference = new File(refDir, testname + ".json");
		File referenceTAA = new File(refDir, testname + "-taa.json");

		SQLParser parser = SQLParser.create(dialect, input);
		ASG asg = parser.parse();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);
		ASGUtils.exportJson(asg, output);

		ObjectMapper mapper = new ObjectMapper();
		mapper.writerWithDefaultPrettyPrinter().writeValue(output, asg.getRoot());
		SQLRoot root2 = mapper.readValue(reference, SQLRoot.class);
		SQLRoot root = asg.getRoot();
		assertTrue(root.matchTree(root2, false, false, false));

		Map<Statement, Map<Table, Integer>> tableAcc = new HashMap<>();
		Map<Statement, Map<Column, Integer>> columnAcc = new HashMap<>();
		ProjectAnalyzer.runTAA(asg, tableAcc, columnAcc);
		ITAAReporter report = ITAAReporter.create(asg, tableAcc, columnAcc, "json");
		report.writeReport(outputTAA);
		JsonNode tree = mapper.readTree(Files.newInputStream(outputTAA.toPath()));
		removePaths(tree);

		JsonNode treeRef = mapper.readTree(Files.newInputStream(referenceTAA.toPath()));
		removePaths(treeRef);

		assertEquals(tree, treeRef);
	}

	protected void testString(String s, String testname) throws JsonParseException, JsonMappingException, IOException {
		parseString(s, testname, true);
	}

	protected List<Statement> parseString(String input, String testname, boolean assertASG)
			throws JsonParseException, JsonMappingException, IOException {
		File output = new File(outputDir, testname + ".json");
		File reference = new File(refDir, testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		SymbolTable symTab = new SymbolTable();
		List<Statement> result = parser.parseStatements();
		ASG asg = parser.getASG();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);
		ASGUtils.exportJson(asg, output);

		if (assertASG) {
			ObjectMapper mapper = new ObjectMapper();
			SQLRoot root2 = mapper.readValue(reference, SQLRoot.class);
			SQLRoot root = asg.getRoot();
			assertTrue(root.matchTree(root2, false, false, false));
		}

		return result;
	}

	@Test
	void testAlter() throws JsonParseException, JsonMappingException, IOException {
		testString("alter table test add column x int", "testalter");
	}

	@Test
	void testAlter2() throws JsonParseException, JsonMappingException, IOException {
		testString("alter table test rename to test2", "testalter2");
	}

	@Test
	void testDrop() throws JsonParseException, JsonMappingException, IOException {
		testString("DROP TABLE splits_bak", "testdrop");
	}

	@Test
	void testTAA1() throws JsonParseException, JsonMappingException, IOException {
		testFile("testTAA1");
	}

}