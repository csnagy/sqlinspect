package sqlinspect.plugin.sqlan.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.builder.Input.Builder;
import org.xmlunit.diff.DefaultNodeMatcher;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.diff.ElementSelector;
import org.xmlunit.diff.ElementSelectors;
import org.xmlunit.util.Nodes;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.AndroidHotspotFinder;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.utils.EclipseProjectUtil;
import sqlinspect.plugin.utils.XMLQueries;
import sqlinspect.sql.SQLDialect;

class TestAndroid extends AbstractPluginTest {

	private static final Logger LOG = LoggerFactory.getLogger(TestAndroid.class);

	private static final String ANDROID_CLASSPATH = System.getenv().get("TEST_ANDROID_SDK");
	private static final String CLASSPATH_SEPARATOR = ":";

	private void setProjectProperties(IProject iproj) {
		IPreferenceStore prefs = Activator.getDefault().getCurrentPreferenceStore(iproj);
		prefs.setValue(PreferenceConstants.STRING_RESOLVER, InterQueryResolver.class.getSimpleName());
		prefs.setValue(PreferenceConstants.HOTSPOT_FINDERS, AndroidHotspotFinder.class.getSimpleName());
		prefs.setValue(PreferenceConstants.DIALECT, SQLDialect.SQLITE.toString());
		prefs.setValue(PreferenceConstants.RUN_SMELL_DETECTORS, false);
		prefs.setValue(PreferenceConstants.RUN_SQL_METRICS, false);
		prefs.setValue(PreferenceConstants.RUN_TABLE_ACCESS, false);
	}

	@BeforeAll
	static void init() {
		if (ANDROID_CLASSPATH == null) {
			throw new IllegalStateException("TEST_ANDROID_SDK env variable is not set!");
		}
	}

	private void analyzeProject(String projectname) throws CoreException, IOException {
		File projectdir = new File(TESTS_DIR, projectname);
		String queriesSuffix = "-queries.xml";
		File queriesOutput = new File(projectname + queriesSuffix);
		File queriesReference = new File(TEST_REF_DIR, projectname + queriesSuffix);

		IEclipseContext context = EclipseContextFactory.create("test context");
		loadRepositoriesInContext(context);
		
		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir.getAbsolutePath(), null,
				ANDROID_CLASSPATH, CLASSPATH_SEPARATOR);
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, context);
		HotspotRepository hotspotRepository = ContextInjectionFactory.make(HotspotRepository.class, context);
		Project project = projectRepository.createProject(iproj);
		setProjectProperties(iproj);

		ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer(project);
		ContextInjectionFactory.inject(projectAnalyzer, context);
		projectAnalyzer.initAnalyzer();

		SubMonitor submonitor = SubMonitor.convert(new NullProgressMonitor());
		projectAnalyzer.extractHotspots(submonitor);

		List<Query> queries = hotspotRepository.getQueries(project);
		XMLQueries xmlQueries = new XMLQueries(queriesOutput);
		xmlQueries.writeQueries(queries, true);

		ElementSelector selector = ElementSelectors.conditionalBuilder().whenElementIsNamed("Query")
				.thenUse(ElementSelectors.and(ElementSelectors.byXPath("./Value", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./HotspotFinder", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecPackage", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecClass", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecLine", ElementSelectors.byNameAndText)))
				.elseUse(ElementSelectors.byName).build();

		Builder out = Input.from(queriesOutput);
		Builder ref = Input.from(queriesReference);

		Diff myDiff = DiffBuilder.compare(out).withTest(ref).withNodeMatcher(new DefaultNodeMatcher(selector))
				.withNodeFilter(n -> !(n instanceof Element && "ExecFile".equals(Nodes.getQName(n).getLocalPart())))
				.withAttributeFilter(a -> {
					QName attrName = Nodes.getQName(a);
					return !attrName.equals(new QName("file"));
				}).withDifferenceEvaluator(DifferenceEvaluators.Default).checkForSimilar().build();

		if (myDiff.hasDifferences()) {
			LOG.error("Diff: {}", myDiff);
		}

		assertFalse(myDiff.hasDifferences());
	}

	@Test
	void shouldResolveAndroidProject() throws CoreException, IOException {
		analyzeProject("AndroidProject");
	}

	@Test
	void shouldResolveConstantField() throws CoreException, IOException {
		analyzeProject("AndroidProjectFieldConst");
	}

	@Test
	void shouldResolveFieldWithDynamicExpression() throws CoreException, IOException {
		analyzeProject("AndroidProjectFieldDynamic");
	}
}
