package sqlinspect.plugin.sqlan.tests;

import java.io.File;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;

import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.SmellRepository;

public abstract class AbstractPluginTest {

	protected static final File TESTS_DIR = new File(System.getenv().get("TEST_RES_DIR"), "tests");
	protected static final File TEST_INPUT_DIR = new File(System.getenv().get("TEST_RES_DIR"), "input");
	protected static final File TEST_REF_DIR = new File(System.getenv().get("TEST_RES_DIR"), "references");
	protected void loadRepositoriesInContext(IEclipseContext context) {
		context.set(ProjectRepository.class, ContextInjectionFactory.make(ProjectRepository.class, context));
		context.set(HotspotRepository.class, ContextInjectionFactory.make(HotspotRepository.class, context));
		context.set(ASGRepository.class, ContextInjectionFactory.make(ASGRepository.class, context));
		context.set(MetricRepository.class, ContextInjectionFactory.make(MetricRepository.class, context));
		context.set(SmellRepository.class, ContextInjectionFactory.make(SmellRepository.class, context));
		context.set(QueryRepository.class, ContextInjectionFactory.make(QueryRepository.class, context));
	}
}
