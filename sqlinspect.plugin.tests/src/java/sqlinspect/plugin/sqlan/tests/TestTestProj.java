package sqlinspect.plugin.sqlan.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.builder.Input.Builder;
import org.xmlunit.diff.DefaultNodeMatcher;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.diff.ElementSelector;
import org.xmlunit.diff.ElementSelectors;
import org.xmlunit.util.Nodes;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.extractors.JDBCHotspotFinder;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.utils.EclipseProjectUtil;
import sqlinspect.plugin.utils.XMLQueries;
import sqlinspect.sql.SQLDialect;

class TestTestProj extends AbstractPluginTest {
	private static final Logger LOG = LoggerFactory.getLogger(TestTestProj.class);

	private void setProjectProperties(IProject iproj) {
		IPreferenceStore prefs = Activator.getDefault().getCurrentPreferenceStore(iproj);
		prefs.setValue(PreferenceConstants.STRING_RESOLVER, InterQueryResolver.class.getSimpleName());
		prefs.setValue(PreferenceConstants.HOTSPOT_FINDERS, JDBCHotspotFinder.class.getSimpleName());
		prefs.setValue(PreferenceConstants.DIALECT, SQLDialect.MYSQL.toString());
		prefs.setValue(PreferenceConstants.RUN_SMELL_DETECTORS, false);
		prefs.setValue(PreferenceConstants.RUN_SQL_METRICS, false);
		prefs.setValue(PreferenceConstants.RUN_TABLE_ACCESS, false);
	}

	@Test
	void test() throws CoreException, IOException {
		String projectname = "Testproj";
		String projectdir = TESTS_DIR + "/" + projectname;
		String queriesSuff = "-queries.xml";
		String queriesexp = projectname + queriesSuff;
		String queriesref = TEST_REF_DIR + "/" + projectname + queriesSuff;

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir, "src");
		IEclipseContext context = EclipseContextFactory.create("test context");
		loadRepositoriesInContext(context);
		
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, context);
		HotspotRepository hotspotRepository = ContextInjectionFactory.make(HotspotRepository.class, context);
		Project project = projectRepository.createProject(iproj);
		setProjectProperties(iproj);

		ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer(project);
		ContextInjectionFactory.inject(projectAnalyzer, context);
		projectAnalyzer.initAnalyzer();

		SubMonitor submonitor = SubMonitor.convert(new NullProgressMonitor());
		projectAnalyzer.extractHotspots(submonitor);

		List<Query> queries = hotspotRepository.getQueries(project);
		XMLQueries xmlQueries = new XMLQueries(new File(queriesexp));
		xmlQueries.writeQueries(queries, true);

		ElementSelector selector = ElementSelectors.conditionalBuilder().whenElementIsNamed("Query")
				.thenUse(ElementSelectors.and(ElementSelectors.byXPath("./Value", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./HotspotFinder", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecPackage", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecClass", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecLine", ElementSelectors.byNameAndText)))
				.elseUse(ElementSelectors.byName).build();

		Builder out = Input.from(new File(queriesexp));
		Builder ref = Input.from(new File(queriesref));

		Diff myDiff = DiffBuilder.compare(out).withTest(ref).withNodeMatcher(new DefaultNodeMatcher(selector))
				.withNodeFilter(n -> !(n instanceof Element && "ExecFile".equals(Nodes.getQName(n).getLocalPart())))
				.withAttributeFilter(a -> {
					QName attrName = Nodes.getQName(a);
					return !attrName.equals(new QName("file"));
				}).withDifferenceEvaluator(DifferenceEvaluators.Default).checkForSimilar().build();

		if (myDiff.hasDifferences()) {
			LOG.error("Diff: {}", myDiff);
		}

		assertFalse(myDiff.hasDifferences());
	}
}
