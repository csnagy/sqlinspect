package sqlinspect.plugin.smells.sql.tests;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import sqlinspect.sql.SQLDialect;

class SQLiteBadSmellTest extends AbstractBadSmellTest {
	private final static File inputDir = new File(TEST_INPUT_DIR, "sqlite-smells");
	private final static File outputDir = new File("sqlite-smells");
	private final static File refDir = new File(TEST_REF_DIR, "sqlite-smells");

	public SQLiteBadSmellTest() {
		super(SQLDialect.SQLITE, inputDir, outputDir, refDir);
	}

	@Test
	void test1() throws ParserConfigurationException, SAXException, IOException {
		testSmells("ambiguousgroups");
	}

	@Test
	void test2() throws ParserConfigurationException, SAXException, IOException {
		testSmells("randomselection");
	}

}
