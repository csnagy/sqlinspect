package sqlinspect.plugin.smells.sql.tests;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import sqlinspect.sql.SQLDialect;

class MySQLBadSmellTest extends AbstractBadSmellTest {
	private final static File inputDir = new File(TEST_INPUT_DIR, "mysql-smells");
	private final static File outputDir = new File("mysql-smells");
	private final static File refDir = new File(TEST_REF_DIR, "mysql-smells");

	public MySQLBadSmellTest() {
		super(SQLDialect.MYSQL, inputDir, outputDir, refDir);
	}

	@Test
	void test1() throws ParserConfigurationException, SAXException, IOException {
		testSmells("fearoftheunknown-nodb");
	}

	@Test
	void test2() throws ParserConfigurationException, SAXException, IOException {
		testSmells("ambiguousgroups");
	}

	@Test
	void test3() throws ParserConfigurationException, SAXException, IOException {
		testSmells("randomselection");
	}

	@Test
	void test4() throws ParserConfigurationException, SAXException, IOException {
		testSmells("implicitcolumns");
	}
}
