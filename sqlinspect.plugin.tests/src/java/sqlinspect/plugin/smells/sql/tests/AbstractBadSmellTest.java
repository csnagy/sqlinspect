package sqlinspect.plugin.smells.sql.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.BeforeEach;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellCertKind;
import sqlinspect.plugin.smells.sql.common.SQLSmellKind;
import sqlinspect.plugin.smells.sql.reports.ISmellReporter;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.sqlan.tests.AbstractPluginTest;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.parser.SQLParser;
import sqlinspect.sql.parser.SymbolTable;
import sqlinspect.sql.parser.VisitorExtraEdges;

public abstract class AbstractBadSmellTest extends AbstractPluginTest {
	private final File inputDir;
	private final File refDir;
	private final File outputDir;

	private static final String ROOT_NODE = "Smells";
	private static final String SMELL_NODE = "Smell";
	private static final String SMELL_NODE_KIND = "Kind";
	private static final String SMELL_NODE_FILE = "File";
	private static final String SMELL_NODE_LINE = "Line";
	private static final String SMELL_NODE_CERTAINTY = "Certainty";
	private static final String SMELL_NODE_MESSAGE = "Message";

	protected SQLDialect dialect;

	protected AbstractBadSmellTest(SQLDialect dialect, File inputDir, File outputDir, File refDir) {
		super();
		this.dialect = dialect;
		this.inputDir = inputDir;
		this.outputDir = outputDir;
		this.refDir = refDir;
	}

	protected void createOutputDir() throws IOException {
		if (!outputDir.exists() && !outputDir.mkdir()) {
			throw new IOException("Could not create output directory: " + outputDir);
		}
	}

	@BeforeEach
	protected void setUp() throws IOException {
		createOutputDir();
	}

	protected void testSmells(String testname) throws ParserConfigurationException, SAXException, IOException {
		File input = new File(inputDir, testname + ".sql");
		File outputSmells = new File(outputDir, testname + "-smells.xml");
		File reference = new File(refDir, testname + "-smells.xml");
		File jsonOutput = new File(outputDir, testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		ASG asg = parser.parse();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);
		ASGUtils.exportJson(asg, jsonOutput);

		Set<SQLSmell> smells = new HashSet<>();
		ProjectAnalyzer.runSQLSmellDetectors(asg, dialect, smells, null);
		ISmellReporter smellReporter = ISmellReporter.create("xml");
		smellReporter.writeReport(outputSmells, smells);

		Set<SQLSmell> refSmellsSet = loadXMLSmells(reference);
		Set<SQLSmell> outputSmellSet = loadXMLSmells(outputSmells);
		assertEquals(refSmellsSet, outputSmellSet);
	}

	protected Set<SQLSmell> loadXMLSmells(File f) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(f);
		NodeList rootlist = doc.getElementsByTagName(ROOT_NODE);
		Set<SQLSmell> smells = new HashSet<>();

		for (int i = 0; i < rootlist.getLength(); i++) {
			Node r = rootlist.item(i);
			if (r.getNodeType() == Node.ELEMENT_NODE) {
				Element rootElement = (Element) r;
				NodeList nl = rootElement.getElementsByTagName(SMELL_NODE);
				for (int j = 0; j < nl.getLength(); j++) {
					Node n = nl.item(j);

					if (n.getNodeType() == Node.ELEMENT_NODE) {
						SQLSmell smell = loadXMLSmell((Element) n);
						smells.add(smell);
					}
				}
			}
		}

		return smells;
	}

	private static String getFileNameOnly(String path) {
		return path.substring(path.lastIndexOf(File.separator) + 1, path.length());
	}

	private SQLSmell loadXMLSmell(Element e) {
		SQLSmellKind kind = SQLSmellKind.valueOf(e.getElementsByTagName(SMELL_NODE_KIND).item(0).getTextContent());
		String message = e.getElementsByTagName(SMELL_NODE_MESSAGE).item(0).getTextContent();
		SQLSmellCertKind certainty = SQLSmellCertKind
				.valueOf(e.getElementsByTagName(SMELL_NODE_CERTAINTY).item(0).getTextContent());
		String file = getFileNameOnly(e.getElementsByTagName(SMELL_NODE_FILE).item(0).getTextContent());
		int line = Integer.parseInt(e.getElementsByTagName(SMELL_NODE_LINE).item(0).getTextContent());

		return new SQLSmell(null, kind, message, certainty, file, line);
	}

	protected void dumpSmells(Set<SQLSmell> set1, Set<SQLSmell> set2) {
		System.out.println("output: " + set1.size());
		for (SQLSmell s : set1) {
			System.out.println(s.toString());
		}

		System.out.println("ref: " + set2.size());
		for (SQLSmell s : set2) {
			System.out.println(s.toString());
		}

		System.out.println("seteq: " + set2.equals(set1));
	}
}