package sqlinspect.plugin.metrics.sql.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.metrics.sql.SQLMetricDesc;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.statm.Statement;

public class TXTMetricsReporter implements IMetricsReporter {
	private static final Logger LOG = LoggerFactory.getLogger(TXTMetricsReporter.class);

	private Map<Statement, Map<SQLMetricDesc, Integer>> metrics;

	@Override
	public void writeReport(ASG asg, File exportFile, Map<Statement, Map<SQLMetricDesc, Integer>> metrics) {
		this.metrics = metrics;
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(exportFile.toPath()), StandardCharsets.UTF_8);
				BufferedWriter bw = new BufferedWriter(ow);) {

			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeStatement(bw, stmt);
				}
			}
		} catch (IOException e) {
			LOG.error("IO error while writing report!", e);
		}
	}

	private void writeStatement(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("Statement (" + stmt.getId() + ")");
		String lpath = stmt.getPath();
		if (lpath != null) {
			bw.write(" at " + lpath + '(' + stmt.getStartLine() + ")");
		}
		bw.write(" accesses:\n");
		writeMetrics(bw, stmt);
	}

	private void writeMetrics(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("  Metrics:\n");
		Map<SQLMetricDesc, Integer> stmtMetrics = metrics.get(stmt);
		if (stmtMetrics != null) {
			for (Map.Entry<SQLMetricDesc, Integer> me : stmtMetrics.entrySet()) {
				SQLMetricDesc md = me.getKey();
				Integer val = me.getValue();
				bw.write("    " + md.getShortName() + ": " + val + "\n");
			}
		}
	}
}
