package sqlinspect.plugin.metrics.sql.reports;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sqlinspect.plugin.metrics.sql.SQLMetricDesc;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.statm.Statement;

public class JSONMetricsReporter implements IMetricsReporter {
	private static final Logger LOG = LoggerFactory.getLogger(JSONMetricsReporter.class);

	private static final String STATEMENT_ATTR_ID = "Id";
	private static final String STATEMENT_ATTR_PATH = "Path";
	private static final String STATEMENT_ATTR_LINE = "Line";
	private static final String METRIC_ATTR_NAME = "name";
	private static final String METRIC_ATTR_VALUE = "value";
	
	private ObjectMapper mapper;
	private Map<Statement, Map<SQLMetricDesc, Integer>> metrics;
	
	@Override
	public void writeReport(ASG asg, File exportFile, Map<Statement, Map<SQLMetricDesc, Integer>> metrics) throws IOException {
		this.mapper = new ObjectMapper();
		this.metrics = metrics;
		
		ObjectNode rootNode = mapper.createObjectNode();
		ArrayNode dbArr = mapper.createArrayNode();

		for (Database db : asg.getRoot().getDatabases()) {
			ObjectNode dbNode = mapper.createObjectNode();
			ArrayNode stmtArr = mapper.createArrayNode();

			for (Statement stmt : db.getStatements()) {
				stmtArr.add(writeStatement(stmt));

			}
			dbNode.set("Statements", stmtArr);
			dbArr.add(dbNode);
		}

		rootNode.set("Databases", dbArr);

		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(exportFile, rootNode);
		} catch (IOException e) {
			LOG.error("Exception: ", e);
		}
	}

	private ObjectNode writeStatement(Statement stmt) {
		ObjectNode node = mapper.createObjectNode();
		node.put(STATEMENT_ATTR_ID, stmt.getId());
		String lpath = stmt.getPath();
		if (lpath != null) {
			node.put(STATEMENT_ATTR_PATH, lpath);
			node.put(STATEMENT_ATTR_LINE, stmt.getStartLine());
		}

		node.set("Metrics", writeMetrics(stmt));
		return node;
	}

	private ArrayNode writeMetrics(Statement stmt) {
		ArrayNode arr = mapper.createArrayNode();

		Map<SQLMetricDesc, Integer> stmtMetrics = metrics.get(stmt);
		if (stmtMetrics != null) {
			for (Map.Entry<SQLMetricDesc, Integer> me : stmtMetrics.entrySet()) {
				ObjectNode node = mapper.createObjectNode();
				SQLMetricDesc md = me.getKey();
				Integer val = me.getValue();

				node.put(METRIC_ATTR_NAME, md.getShortName());
				node.put(METRIC_ATTR_VALUE, val.toString());
				arr.add(node);
			}
		}

		return arr;
	}
}
