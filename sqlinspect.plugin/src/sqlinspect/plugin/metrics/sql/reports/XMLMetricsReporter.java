package sqlinspect.plugin.metrics.sql.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Map;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.metrics.sql.SQLMetricDesc;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.statm.Statement;

public class XMLMetricsReporter implements IMetricsReporter {
	private static final Logger LOG = LoggerFactory.getLogger(XMLMetricsReporter.class);

	private static final String ROOT_NODE = "Metrics";
	private static final String DATABASE_NODE = "Database";
	private static final String STATEMENT_NODE = "Statement";
	private static final String STATEMENT_ATTR_ID = "Id";
	private static final String STATEMENT_ATTR_PATH = "Path";
	private static final String STATEMENT_ATTR_LINE = "Line";
	private static final String METRICS_NODE = "Metrics";
	private static final String METRIC_NODE = "Metric";
	private static final String METRIC_ATTR_NAME = "name";
	private static final String METRIC_ATTR_VALUE = "value";

	private static final String TABC = "  ";
	private static final String NLC = "\n";

	private Map<Statement, Map<SQLMetricDesc, Integer>> metrics;

	private XMLStreamWriter writer;
	private int indent = 1;

	@Override
	public void writeReport(ASG asg, File exportFile, Map<Statement, Map<SQLMetricDesc, Integer>> metrics) throws IOException {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		this.metrics = metrics;

		try (OutputStream ostream = Files.newOutputStream(exportFile.toPath())) {
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			for (Database db : asg.getRoot().getDatabases()) {
				tab(indent++);
				writer.writeStartElement(DATABASE_NODE);
				nl();

				for (Statement stmt : db.getStatements()) {
					writeStatement(stmt);

				}
				writer.writeEndElement();
				tab(--indent);
				nl();
			}

			writer.writeEndElement();
			nl();
			writer.writeEndDocument();
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			LOG.error("Could not write XML.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					LOG.error("Could not clean XMLStreamWriter!", e);
				}
			}
		}
	}

	private void writeStatement(Statement stmt) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(STATEMENT_NODE);
		writer.writeAttribute(STATEMENT_ATTR_ID, Integer.toString(stmt.getId()));
		String lpath = stmt.getPath();
		if (lpath != null) {
			writer.writeAttribute(STATEMENT_ATTR_PATH, lpath);
			writer.writeAttribute(STATEMENT_ATTR_LINE, Integer.toString(stmt.getStartLine()));
		}
		nl();

		writeMetrics(stmt);

		tab(--indent);
		writer.writeEndElement();
		nl();
	}

	private void writeMetrics(Statement stmt) throws XMLStreamException {
		Map<SQLMetricDesc, Integer> stmtMetrics = metrics.get(stmt);
		if (stmtMetrics != null) {
			tab(indent++);
			writer.writeStartElement(METRICS_NODE);
			nl();
			for (Map.Entry<SQLMetricDesc, Integer> me : stmtMetrics.entrySet()) {
				SQLMetricDesc md = me.getKey();
				Integer val = me.getValue();

				tab(indent);
				writer.writeStartElement(METRIC_NODE);
				writer.writeAttribute(METRIC_ATTR_NAME, md.getShortName());
				writer.writeAttribute(METRIC_ATTR_VALUE, val.toString());
				writer.writeEndElement();
				nl();
			}
			tab(--indent);
			writer.writeEndElement();
			nl();
		}
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters(NLC);
	}

	private void tab(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TABC);
		}
	}
}
