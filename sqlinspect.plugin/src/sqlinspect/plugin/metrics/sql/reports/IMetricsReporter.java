package sqlinspect.plugin.metrics.sql.reports;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import sqlinspect.plugin.metrics.sql.SQLMetricDesc;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.statm.Statement;

public interface IMetricsReporter {
	static final String XML = "xml";
	static final String JSON = "json";
	static final String TXT = "txt";

	void writeReport(ASG asg, File exportFile, Map<Statement, Map<SQLMetricDesc, Integer>> metrics) throws IOException;

	static IMetricsReporter create(String reportFormat) {
		if (XML.equalsIgnoreCase(reportFormat)) {
			return new XMLMetricsReporter();
		} else if (JSON.equalsIgnoreCase(reportFormat)) {
			return new JSONMetricsReporter();
		} else if (JSON.equalsIgnoreCase(reportFormat)) {
			return new TXTMetricsReporter();
		} else {
			throw new IllegalArgumentException("Invalid report format: " + reportFormat);
		}
	}
}
