package sqlinspect.plugin.metrics.sql;

public enum SQLMetricDesc {
	NumberOfResultFields("NORF", "Fields", "Number of Result Fields"),
	NumberOfTables("NOTU", "Tables Used", "Number of Tables Used"),
	NumberOfQueries("NONQ", "Nested Queries", "Number of Nested Queries"),
	NestingLevel("NL", "Nesting Level", "Nesting Level of Queries"),
	NumberOfUnions("NOU", "Union Queries", "Number of UNION Queries"),
	NumberOfJoins("NOJ", "Joins", "Number of Joins Used");

	private final String id;
	private final String shortName;
	private final String description;

	SQLMetricDesc(String id, String shortName, String description) {
		this.id = id;
		this.shortName = shortName;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}