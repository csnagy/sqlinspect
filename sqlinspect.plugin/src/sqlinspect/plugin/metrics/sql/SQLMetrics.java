package sqlinspect.plugin.metrics.sql;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.statm.Delete;
import sqlinspect.sql.asg.statm.Insert;
import sqlinspect.sql.asg.statm.Select;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.statm.Update;
import sqlinspect.sql.asg.traversal.PreOrderTraversal;
import sqlinspect.sql.asg.visitor.Visitor;

public class SQLMetrics extends Visitor {
	private static final Logger LOG = LoggerFactory.getLogger(SQLMetrics.class);

	private Optional<Statement> actualStatement = Optional.empty();

	private final Deque<SelectExpression> actualSelectExpression = new ArrayDeque<>();

	private final Map<Statement, Map<SQLMetricDesc, Integer>> metrics = new HashMap<>();

	private final ASG asg;

	public SQLMetrics(ASG asg) {
		super();
		this.asg = asg;
	}

	public void calculate() {
		metrics.clear();
		PreOrderTraversal traversal = new PreOrderTraversal(this);
		traversal.traverse(asg.getRoot());
	}

	public Map<Statement, Map<SQLMetricDesc, Integer>> getMetrics() {
		return metrics;
	}

	@Override
	public void visit(Select n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Select n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(Insert n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Insert n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(Update n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Update n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(Delete n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Delete n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(SelectExpression n) {
		if (!actualStatement.isPresent()) {
			return;
		}

		actualSelectExpression.addLast(n);

		if (n.getParent().equals(actualStatement.get())) {
			calcFields(n);
			setNumberOfQueries(0);
			setNestingLevel(0);
		} else {
			incNumberOfQueries();
			incNestingLevel();
		}
	}

	@Override
	public void visitEnd(SelectExpression n) {
		if (n.equals(actualSelectExpression.getLast())) {
			actualSelectExpression.removeLast();
		}
	}

	private Integer getMetricValue(SQLMetricDesc metric, int defaultValue) {
		if (!actualStatement.isPresent()) {
			LOG.error("No actual statament is available");
			return 0;
		}

		Map<SQLMetricDesc, Integer> stmtMetrics = metrics.computeIfAbsent(actualStatement.get(),
				k -> new EnumMap<>(SQLMetricDesc.class));
		return stmtMetrics.computeIfAbsent(metric, k -> Integer.valueOf(defaultValue));
	}

	private void setMetricValue(SQLMetricDesc metric, Integer value) {
		if (!actualStatement.isPresent()) {
			LOG.error("No actual statament is available");
			return;
		}

		Map<SQLMetricDesc, Integer> stmtMetrics = metrics.computeIfAbsent(actualStatement.get(),
				k -> new EnumMap<>(SQLMetricDesc.class));
		stmtMetrics.put(metric, value);
	}

	private void incMetricValue(SQLMetricDesc metric, Integer value) {
		if (!actualStatement.isPresent()) {
			LOG.error("No actual statament is available");
			return;
		}

		Map<SQLMetricDesc, Integer> stmtMetrics = metrics.computeIfAbsent(actualStatement.get(),
				k -> new EnumMap<>(SQLMetricDesc.class));

		Integer m = stmtMetrics.get(metric);
		if (m == null) {
			stmtMetrics.put(metric, value);
		} else {
			stmtMetrics.put(metric, m + value);
		}
	}

	private void setNestingLevel(int val) {
		setMetricValue(SQLMetricDesc.NestingLevel, val);
	}

	private void incNestingLevel() {
		Integer val = getMetricValue(SQLMetricDesc.NestingLevel, 0);
		int nl = actualSelectExpression.size() - 1;
		if (nl > val) {
			setMetricValue(SQLMetricDesc.NestingLevel, nl);
		}
	}

	private void setNumberOfQueries(int val) {
		setMetricValue(SQLMetricDesc.NumberOfQueries, val);
	}

	private void incNumberOfQueries() {
		incMetricValue(SQLMetricDesc.NumberOfQueries, 1);
	}

	private void calcFields(SelectExpression selectExpression) {
		Expression columnList = selectExpression.getColumnList();
		if (columnList instanceof ExpressionList expressionList) {
			setMetricValue(SQLMetricDesc.NumberOfResultFields, expressionList.getExpressions().size());
		} else {
			setMetricValue(SQLMetricDesc.NumberOfResultFields, 1);
		}
	}
}
