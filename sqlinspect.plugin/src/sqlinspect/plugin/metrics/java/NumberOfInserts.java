package sqlinspect.plugin.metrics.java;

import sqlinspect.sql.asg.statm.Insert;
import sqlinspect.sql.asg.statm.Statement;

public final class NumberOfInserts extends NumberOfStmt {
	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfInserts;

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Insert;
	}

	@Override
	protected JavaMetricDesc getDesc() {
		return DESC;
	}
}