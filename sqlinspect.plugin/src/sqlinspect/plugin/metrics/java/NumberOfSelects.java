package sqlinspect.plugin.metrics.java;

import sqlinspect.sql.asg.statm.Select;
import sqlinspect.sql.asg.statm.Statement;

public final class NumberOfSelects extends NumberOfStmt {
	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfSelects;

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Select;
	}

	@Override
	protected JavaMetricDesc getDesc() {
		return DESC;
	}
}