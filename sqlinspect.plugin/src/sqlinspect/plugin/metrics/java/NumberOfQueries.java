package sqlinspect.plugin.metrics.java;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.utils.ASTUtils;

public final class NumberOfQueries extends AbstractJavaMetric {
	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfQueries;
	
	@Inject
	private HotspotRepository hotspotRepository;

	@Override
	public Map<IJavaElement, Map<JavaMetricDesc, Integer>> calculate(Project project) {
		LOG.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics = new HashMap<>();
		for (Hotspot hs : hotspotRepository.getHotspots(project)) {
			ASTNode node = hs.getExec();
			if (node != null) {
				ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
				if (cu != null) {
					incMetrics(metrics, cu, DESC, hs.getQueries().size());
				}
			}
		}
		return metrics;
	}
}
