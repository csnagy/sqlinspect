package sqlinspect.plugin.metrics.java;

import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.statm.Update;

public final class NumberOfUpdates extends NumberOfStmt {
	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfUpdates;

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Update;
	}

	@Override
	protected JavaMetricDesc getDesc() {
		return DESC;
	}
}