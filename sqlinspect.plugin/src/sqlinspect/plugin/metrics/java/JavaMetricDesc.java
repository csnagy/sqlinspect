package sqlinspect.plugin.metrics.java;

public enum JavaMetricDesc {
	NumberOfHotspots("NOS", "Hotspots", "Number of Database Access Hotspots"),
	NumberOfQueries("NOQ", "Queries", "Number of Queries"),
	NumberOfInserts("NOI", "Inserts", "Number of Insert Statements"),
	NumberOfSelects("NOSe", "Selects", "Number of Select Statements"),
	NumberOfUpdates("NOU", "Updates", "Number of Update Statements"),
	NumberOfDeletes("NOD", "Deletes", "Number of Delete Statements"),
	NumberOfTableAccesses("NOT", "Tables", "Number of Tables Accessed"),
	NumberOfColumnAccesses("NOC", "Columns", "Number of Columns Accessed");

	private final String id;
	private final String shortName;
	private final String description;

	JavaMetricDesc(String id, String shortName, String description) {
		this.id = id;
		this.shortName = shortName;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public String getShortName() {
		return shortName;
	}

	public String getDescription() {
		return description;
	}
}