package sqlinspect.plugin.metrics.java.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.ui.StandardJavaElementContentProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.metrics.java.JavaMetricDesc;

public class XMLJavaMetricsReporter {
	private static final Logger LOG = LoggerFactory.getLogger(XMLJavaMetricsReporter.class);

	private static final String ROOT_NODE = "JavaMetrics";
	private static final String JAVAELEMENT_NODE = "JavaElement";
	private static final String JAVAELEMENT_KIND = "Kind";
	private static final String JAVAELEMENT_NAME = "Name";
	private static final String METRICS_NODE = "Metrics";
	private static final String METRIC_NODE = "Metric";
	private static final String METRIC_ATTR_NAME = "name";
	private static final String METRIC_ATTR_VALUE = "value";
	private static final String CHILDREN_NODE = "Children";

	private static final String TAB = "  ";

	private final File file;

	private int actualIndent;

	private XMLStreamWriter writer;

	private final Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics;

	private final StandardJavaElementContentProvider standardProvider = new StandardJavaElementContentProvider();

	public XMLJavaMetricsReporter(File exportFile, Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics) {
		this.file = exportFile;
		this.metrics = metrics;
	}

	public void writeReport(IJavaProject project) {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		actualIndent = 0;

		try (OutputStream ostream = Files.newOutputStream(file.toPath())) {
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			writeJavaElement(project);

			writer.writeEndElement();
			nl();
			writer.writeEndDocument();
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			LOG.error("Could not write XML.", e);
		} catch (IOException e) {
			LOG.error("IO error.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					LOG.error("Could not clean XMLStreamWriter!", e);
				}
			}
		}
	}

	private void writeJavaElement(IJavaElement element) throws XMLStreamException {
		indent(actualIndent++);
		writer.writeStartElement(JAVAELEMENT_NODE);
		writer.writeAttribute(JAVAELEMENT_KIND, element.getClass().getSimpleName());
		writer.writeAttribute(JAVAELEMENT_NAME, element.getElementName());
		nl();

		Map<JavaMetricDesc, Integer> elementMetrics = metrics.get(element);
		if (elementMetrics != null) {
			indent(actualIndent++);
			writer.writeStartElement(METRICS_NODE);
			nl();

			for (Map.Entry<JavaMetricDesc, Integer> me : elementMetrics.entrySet()) {
				indent(actualIndent);
				writer.writeStartElement(METRIC_NODE);
				writer.writeAttribute(METRIC_ATTR_NAME, me.getKey().getShortName());
				writer.writeAttribute(METRIC_ATTR_VALUE, me.getValue().toString());

				writer.writeEndElement();
				nl();
			}

			indent(--actualIndent);
			writer.writeEndElement();
			nl();
		}

		IJavaElement[] children = getChildren(element);
		if (children != null) {
			indent(actualIndent++);
			writer.writeStartElement(CHILDREN_NODE);
			nl();

			for (IJavaElement child : children) {
				writeJavaElement(child);
			}
			indent(--actualIndent);
			writer.writeEndElement();
			nl();
		}

		indent(--actualIndent);
		writer.writeEndElement();
		nl();
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters("\n");
	}

	private void indent(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TAB);
		}
	}

	public IJavaElement[] getChildren(IJavaElement element) {
		List<IJavaElement> ret = new ArrayList<>();
		for (Object retElement : standardProvider.getChildren(element)) {
			if (retElement instanceof IJavaElement retJavaElement) {
				Object m = metrics.get(retJavaElement);
				if (m != null) {
					ret.add(retJavaElement);
				}
			}
		}
		return ret.toArray(new IJavaElement[0]);
	}

}
