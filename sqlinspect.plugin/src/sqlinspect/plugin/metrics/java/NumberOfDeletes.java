package sqlinspect.plugin.metrics.java;

import sqlinspect.sql.asg.statm.Delete;
import sqlinspect.sql.asg.statm.Statement;

public final class NumberOfDeletes extends NumberOfStmt {
	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfDeletes;

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Delete;
	}

	@Override
	protected JavaMetricDesc getDesc() {
		return DESC;
	}
}