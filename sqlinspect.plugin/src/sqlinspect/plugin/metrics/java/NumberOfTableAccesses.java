package sqlinspect.plugin.metrics.java;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;

public final class NumberOfTableAccesses extends AbstractJavaMetric {
	@Inject
	private TableAccessRepository tableAccessRepository;

	@Inject
	private QueryRepository queryRepository;

	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfTableAccesses;

	private void addTables(Map<ICompilationUnit, Set<Table>> cuTables, Query q, Set<Table> tables) {
		ASTNode node = q.getHotspot().getExec();
		if (node != null) {
			ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
			if (cu != null) {
				Set<Table> hsTabs = cuTables.computeIfAbsent(cu, k -> new HashSet<>());
				hsTabs.addAll(tables);
			}
		}
	}

	@Override
	public Map<IJavaElement, Map<JavaMetricDesc, Integer>> calculate(Project project) {
		LOG.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics = new HashMap<>();

		Map<Statement, Map<Table, Integer>> tableAcc = tableAccessRepository.getTableAccesses(project);
		Map<ICompilationUnit, Set<Table>> cuTables = new HashMap<>();
		for (Map.Entry<Statement, Map<Table, Integer>> e : tableAcc.entrySet()) {
			Statement stmt = e.getKey();
			if (stmt != null) {
				Set<Table> tabs = e.getValue().keySet();
				Query q = queryRepository.getQueryMap(project).get(stmt);
				if (q != null) {
					addTables(cuTables, q, tabs);
				}
			}
		}

		for (Map.Entry<ICompilationUnit, Set<Table>> he : cuTables.entrySet()) {
			incMetrics(metrics, he.getKey(), DESC, he.getValue().size());
		}

		return metrics;
	}
}
