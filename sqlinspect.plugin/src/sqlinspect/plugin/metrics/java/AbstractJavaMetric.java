package sqlinspect.plugin.metrics.java;

import java.util.EnumMap;
import java.util.Map;

import org.eclipse.jdt.core.IJavaElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Project;

public abstract class AbstractJavaMetric {
	protected static final Logger LOG = LoggerFactory.getLogger(AbstractJavaMetric.class);

	protected static void incMetrics(Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics, JavaMetricDesc md,
			IJavaElement element) {
		incMetrics(metrics, element, md, 1);
	}

	protected static void incMetrics(Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics, IJavaElement element,
			JavaMetricDesc md, int val) {
		IJavaElement parent = element;
		while (parent != null) {
			Map<JavaMetricDesc, Integer> mets = metrics.get(parent);
			if (mets == null) {
				mets = new EnumMap<>(JavaMetricDesc.class);
				mets.put(md, val);
			} else {
				Integer mv = mets.get(md);
				if (mv == null) {
					mv = Integer.valueOf(val);
				} else {
					mv += val;
				}
				mets.put(md, mv);
			}
			metrics.put(parent, mets);
			parent = parent.getParent();
		}
	}

	public abstract Map<IJavaElement, Map<JavaMetricDesc, Integer>> calculate(Project project);
}
