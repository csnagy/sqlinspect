package sqlinspect.plugin.metrics.java;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.asg.statm.Statement;

public abstract class NumberOfStmt extends AbstractJavaMetric {
	protected abstract JavaMetricDesc getDesc();
	
	@Inject
	private HotspotRepository hotspotRepository;

	private int countStmtOfHotspot(Hotspot hs) {
		int ret = 0;
		for (Query q : hs.getQueries()) {
			for (Statement stmt : q.getStatements()) {
				if (isOfStmt(stmt)) {
					++ret;
				}
			}
		}
		return ret;
	}

	protected abstract boolean isOfStmt(Statement stmt);

	@Override
	public Map<IJavaElement, Map<JavaMetricDesc, Integer>> calculate(Project project) {
		LOG.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics = new HashMap<>();
		for (Hotspot hs : hotspotRepository.getHotspots(project)) {
			ASTNode node = hs.getExec();
			if (node != null) {
				ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
				if (cu != null) {
					incMetrics(metrics, cu, getDesc(), countStmtOfHotspot(hs));
				}
			}
		}
		return metrics;
	}
}