package sqlinspect.plugin.metrics.java;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.statm.Statement;

public final class NumberOfColumnAccesses extends AbstractJavaMetric {
	@Inject
	private TableAccessRepository tableAccessRepository;

	@Inject
	private QueryRepository queryRepository;

	public static final JavaMetricDesc DESC = JavaMetricDesc.NumberOfColumnAccesses;

	private void addColumns(Map<ICompilationUnit, Set<Column>> cuColumns, Query q, Set<Column> cols) {
		ASTNode node = q.getHotspot().getExec();
		if (node != null) {
			ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
			if (cu != null) {
				Set<Column> hsCols = cuColumns.computeIfAbsent(cu, k -> new HashSet<>());
				hsCols.addAll(cols);
			}
		}
	}

	@Override
	public Map<IJavaElement, Map<JavaMetricDesc, Integer>> calculate(Project project) {
		LOG.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<JavaMetricDesc, Integer>> metrics = new HashMap<>();

		Map<Statement, Map<Column, Integer>> columnAcc = tableAccessRepository.getColumnAccesses(project);
		Map<ICompilationUnit, Set<Column>> cuColumns = new HashMap<>();
		for (Map.Entry<Statement, Map<Column, Integer>> e : columnAcc.entrySet()) {
			Statement stmt = e.getKey();
			if (stmt != null) {
				Set<Column> cols = e.getValue().keySet();
				Query q = queryRepository.getQueryMap(project).get(stmt);
				if (q != null) {
					addColumns(cuColumns, q, cols);
				}
			}
		}

		for (Map.Entry<ICompilationUnit, Set<Column>> he : cuColumns.entrySet()) {
			incMetrics(metrics, he.getKey(), DESC, he.getValue().size());
		}
		return metrics;
	}
}
