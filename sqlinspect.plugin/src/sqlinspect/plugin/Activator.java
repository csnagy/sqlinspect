package sqlinspect.plugin;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.SmellRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.utils.ElementChangedListener;

public class Activator extends AbstractUIPlugin {
	private static final Logger LOG = LoggerFactory.getLogger(Activator.class);

	private static final QualifiedName SESSION_PROPERTY_SETTINGS_ON = new QualifiedName(Activator.PLUGIN_ID,
			"session_property_settings_on");
	private static final QualifiedName PERSISTENT_PROPERTY_SETTINGS_ON = new QualifiedName(Activator.PLUGIN_ID,
			"persistent_property_settings_on");

	public static final String PLUGIN_ID = "sqlinspect.plugin";

	private static Activator plugin;
	
	private IEclipseContext context;
	
	public ProjectAnalyzer createProjectAnalyzer(Project project) {
		ProjectAnalyzer analyzer = new ProjectAnalyzer(project);
		ContextInjectionFactory.inject(analyzer, context);
		analyzer.initAnalyzer();
		return analyzer;
	}

	public Activator() {
		super();
		plugin = this;
	}

	public static Activator getDefault() {
		return plugin;
	}

	public static ScopedPreferenceStore getProjectPreferenceStore(IProject project) {
		return new ScopedPreferenceStore(new ProjectScope(project), Activator.PLUGIN_ID);
	}

	public IPreferenceStore getCurrentPreferenceStore(IProject project) {
		if (isProjectSettingsEnabledInPersitentProperty(project)) {
			return getProjectPreferenceStore(project);
		} else {
			return getPreferenceStore();
		}
	}

	public static boolean isProjectSettingsEnabledInSession(IProject project) {
		try {
			Boolean val = (Boolean) project.getSessionProperty(SESSION_PROPERTY_SETTINGS_ON);
			return val != null && val;
		} catch (CoreException e) {
			LOG.error("Could not get session property", e);
			return false;
		}
	}

	public static void enableProjectSettingsInSession(IProject project, boolean enabled) {
		try {
			project.setSessionProperty(SESSION_PROPERTY_SETTINGS_ON, Boolean.valueOf(enabled));
		} catch (CoreException e) {
			LOG.error("Could not set session property", e);
		}
	}

	public static boolean isProjectSettingsEnabledInPersitentProperty(IProject project) {
		try {
			return Boolean.parseBoolean(project.getPersistentProperty(PERSISTENT_PROPERTY_SETTINGS_ON));
		} catch (CoreException e) {
			LOG.error("Could not get session property", e);
			return false;
		}
	}

	public static void enableProjectSettingsInPersistentProperty(IProject project, boolean enabled)
			throws CoreException {
		project.setPersistentProperty(PERSISTENT_PROPERTY_SETTINGS_ON, Boolean.toString(enabled));
	}

	private void injectRepositories(IEclipseContext context) {
		context.set(ProjectRepository.class, ContextInjectionFactory.make(ProjectRepository.class, context));
		context.set(HotspotRepository.class, ContextInjectionFactory.make(HotspotRepository.class, context));
		context.set(QueryRepository.class, ContextInjectionFactory.make(QueryRepository.class, context));
		context.set(ASGRepository.class, ContextInjectionFactory.make(ASGRepository.class, context));
		context.set(MetricRepository.class, ContextInjectionFactory.make(MetricRepository.class, context));
		context.set(SmellRepository.class, ContextInjectionFactory.make(SmellRepository.class, context));
		context.set(TableAccessRepository.class, ContextInjectionFactory.make(TableAccessRepository.class, context));
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		IEclipseContext eclipseContext = EclipseContextFactory.getServiceContext(context);
		this.context = eclipseContext;
		ElementChangedListener el = ContextInjectionFactory.make(ElementChangedListener.class, eclipseContext);
		JavaCore.addElementChangedListener(el, ElementChangedEvent.POST_CHANGE);
		injectRepositories(eclipseContext);
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in relative
	 * path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return PlatformUI.isWorkbenchRunning() ? PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(path)
				: null;
	}
}
