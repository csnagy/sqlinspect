package sqlinspect.plugin.utils;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.util.ASGUtils;

public class JSONSchema {
	private static final Logger LOG = LoggerFactory.getLogger(JSONSchema.class);

	private static final String DATABASES = "Databases";
	private static final String DB_NAME = "Name";
	private static final String DB_SCHEMAS = "Schemas";
	private static final String SCHEMA_NAME = "Name";
	private static final String SCHEMA_TABLES = "Tables";
	private static final String TABLE_NAME = "Name";
	private static final String TABLE_COLUMNS = "Columns";
	private static final String COLUMN_NAME = "Name";
	private static final String COLUMN_TYPE = "Type";

	private final File file;
	private final ObjectMapper mapper = new ObjectMapper();

	public JSONSchema(File file) {
		this.file = file;
	}

	public void writeSchema(SQLRoot root) throws IOException {
		ObjectNode rootNode = mapper.createObjectNode();
		ArrayNode dbArr = mapper.createArrayNode();

		try {
			for (Database db : root.getDatabases()) {
				dbArr.add(writeDatabaseNode(db));
			}

			rootNode.set(DATABASES, dbArr);

			mapper.writerWithDefaultPrettyPrinter().writeValue(file, rootNode);
		} catch (IOException e) {
			LOG.error("Exception: ", e);
		}
	}

	private ObjectNode writeDatabaseNode(Database db) {
		ObjectNode node = mapper.createObjectNode();
		node.put(DB_NAME, db.getName());

		ArrayNode schemaArr = mapper.createArrayNode();
		for (Schema sch : db.getSchemas()) {
			schemaArr.add(writeSchemaNode(sch));
		}

		node.set(DB_SCHEMAS, schemaArr);
		return node;
	}

	private ObjectNode writeSchemaNode(Schema sch) {
		ObjectNode node = mapper.createObjectNode();
		node.put(SCHEMA_NAME, sch.getName());

		ArrayNode tableArr = mapper.createArrayNode();
		for (Table tab : sch.getTables()) {
			tableArr.add(writeTableNode(tab));
		}

		node.set(SCHEMA_TABLES, tableArr);
		return node;
	}

	private ObjectNode writeTableNode(Table tab) {
		ObjectNode node = mapper.createObjectNode();
		node.put(TABLE_NAME, tab.getName());

		ArrayNode colArr = mapper.createArrayNode();
		for (Column col : tab.getColumns()) {
			colArr.add(writeColumnNode(col));
		}

		node.set(TABLE_COLUMNS, colArr);
		return node;
	}

	private ObjectNode writeColumnNode(Column col) {
		ObjectNode node = mapper.createObjectNode();
		node.put(COLUMN_NAME, col.getName());
		node.put(COLUMN_TYPE, ASGUtils.columnTypeToString(col));
		return node;
	}
}
