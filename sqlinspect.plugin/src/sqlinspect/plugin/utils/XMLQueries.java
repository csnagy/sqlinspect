package sqlinspect.plugin.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collection;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.CallStack;
import sqlinspect.plugin.model.Query;

public class XMLQueries {
	private static final Logger LOG = LoggerFactory.getLogger(XMLQueries.class);

	private static final String ROOT_NODE = "Queries";
	private static final String QUERY_NODE = "Query";
	private static final String QUERY_HOTSPOTFINDER = "HotspotFinder";
	private static final String CALLSTACK_NODE = "CallStack";
	private static final String CALL_NODE = "Call";
	private static final String CALL_METHOD = "method";
	private static final String CALL_PATH = "file";
	private static final String METHOD_LINE = "methodLine";
	private static final String CALL_LINE = "callLine";
	private static final String QUERY_ATTR_ID = "id";
	private static final String QUERY_NODE_VALUE = "Value";
	private static final String QUERY_NODE_EXEC_PACKAGE = "ExecPackage";
	private static final String QUERY_NODE_EXEC_CLASS = "ExecClass";
	private static final String QUERY_NODE_EXEC_FILE = "ExecFile";
	private static final String QUERY_NODE_EXEC_LINE = "ExecLine";
	private static final String QUERY_NODE_EXEC_STRING = "ExecString";

	private static final String TAB_STR = "  ";

	private final File file;
	private XMLStreamWriter writer;
	private int indent;
	private boolean noIds;

	public XMLQueries(File file) {
		this.file = file;
		indent = 1;
		noIds = false;
	}

	public void writeQueries(Collection<Query> queries, boolean noIds) throws IOException {
		LOG.debug("Dump queries to file {} ", file.getAbsolutePath());
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		this.noIds = noIds;

		try (OutputStream ostream = Files.newOutputStream(file.toPath())) {
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			int id = 0;
			for (Query q : queries) {
				++id;

				writeQueryNode(q, id);
			}

			writer.writeEndElement();
			nl();
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			LOG.error("Could not write XML.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					LOG.error("Could not clean XMLStreamWriter!", e);
				}
			}
		}
		LOG.debug("Dump queries done.");
	}

	private void writeQueryNode(Query q, int id) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(QUERY_NODE);
		if (!noIds) {
			writer.writeAttribute(QUERY_ATTR_ID, Integer.toString(id));
		}
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_VALUE);
		writer.writeCharacters(q.getValue());
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_HOTSPOTFINDER);
		writer.writeCharacters(q.getHotspot().getHotspotFinder().getName());
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_PACKAGE);
		writer.writeCharacters(ASTUtils.getPackageNameOf(q.getHotspot().getUnit()));
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_CLASS);
		writer.writeCharacters(ASTUtils.getQualifiedClassNameOf(q.getHotspot().getExec()));
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_FILE);
		writer.writeCharacters(EclipseProjectUtil
				.getOriginalPathOfFile(q.getHotspot().getProject().getIProject(), q.getHotspot().getPath()).toString());
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_LINE);
		writer.writeCharacters(Integer.toString(ASTUtils.getLineNumber(q.getHotspot().getExec())));
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_STRING);
		writer.writeCharacters(q.getHotspot().getExec().toString());
		writer.writeEndElement();
		nl();

		writeCallStack(q);

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeCallStack(Query q) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(CALLSTACK_NODE);
		nl();

		CallStack stack = CallStack.calculate(q.getRoot());
		for (CallStack.CallStackElement e : stack.stack) {
			writeCall(q, e);
		}

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeCall(Query q, CallStack.CallStackElement e) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(CALL_NODE);

		IMethodBinding mb = e.getMethodDeclBinding();
		writer.writeAttribute(CALL_METHOD, mb == null ? "Unknown" : BindingUtil.qualifiedSignature(mb));

		IPath lpath = e.getPath();
		writer.writeAttribute(CALL_PATH,
				lpath == null ? "Unknown"
						: EclipseProjectUtil.getOriginalPathOfFile(q.getHotspot().getProject().getIProject(), lpath)
								.toString());

		int methodLine = e.getMethodLineNumber();
		writer.writeAttribute(METHOD_LINE, Integer.toString(methodLine));

		int callLine = e.getCallLineNumber();
		writer.writeAttribute(CALL_LINE, Integer.toString(callLine));

		writer.writeEndElement();
		nl();
		--indent;
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters("\n");
	}

	private void tab(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TAB_STR);
		}
	}
}
