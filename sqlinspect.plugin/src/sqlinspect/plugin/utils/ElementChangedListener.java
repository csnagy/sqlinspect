package sqlinspect.plugin.utils;

import java.util.Set;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.cfg.MethodDeclVisitor;

@Creatable
public class ElementChangedListener implements IElementChangedListener {
	private static final Logger LOG = LoggerFactory.getLogger(ElementChangedListener.class);

	@Inject
	private ASTStore astStore;

	@Inject
	private CFGStore cfgStore;

	@Override
	public void elementChanged(ElementChangedEvent event) {
		IJavaElementDelta delta = event.getDelta();
		if (delta != null) {
			traverse(delta);
		}
	}

	private void traverse(IJavaElementDelta delta) {
		IJavaElement element = delta.getElement();

		if (element instanceof ICompilationUnit icu) {
			LOG.debug("Compilation Unit changed: {}", icu.getElementName());
			if (astStore != null && astStore.inStore(icu)) {
				Set<MethodDeclaration> methods = new MethodDeclVisitor().run(astStore.get(icu));
				for (MethodDeclaration decl : methods) {
					cfgStore.remove(decl);
					LOG.debug("Removed method from CFG store: {}", decl.getName());
				}
				astStore.remove(icu);
				LOG.debug("Removed unit from AST store: {}", icu.getElementName());
			}
		}

		IJavaElementDelta[] children = delta.getAffectedChildren();
		for (IJavaElementDelta child : children) {
			traverse(child);
		}
	}
}
