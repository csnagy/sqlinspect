package sqlinspect.plugin.utils;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IPackageBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ASTUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ASTUtils.class);

	private ASTUtils() {
		// Empty constructor. Just to prevent instantiation.
	}

	public static int getLineNumber(ASTNode node) {
		CompilationUnit unit = getCompilationUnit(node);
		if (unit != null) {
			return unit.getLineNumber(node.getStartPosition() - 1);
		} else {
			return 0;
		}
	}

	public static IPath getPath(ASTNode node) {
		CompilationUnit unit = getCompilationUnit(node);
		if (unit != null) {
			IJavaElement element = unit.getJavaElement();
			if (element != null) {
				return element.getPath();
			}
		}
		return null;
	}

	public static CompilationUnit getCompilationUnit(ASTNode node) {
		ASTNode root = node.getRoot();
		if (root instanceof CompilationUnit cu) {
			return cu;
		}
		return null;
	}

	public static Block getParentBlock(ASTNode node) {
		ASTNode parent = node.getParent();
		while (parent != null && !(parent instanceof Block)) {
			parent = parent.getParent();
		}
		if (parent instanceof Block block) {
			return block;
		}
		return null;
	}

	public static Statement getParentStatement(ASTNode node) {
		ASTNode parent = node.getParent();
		while (parent != null && !(parent instanceof Statement)) {
			parent = parent.getParent();
		}
		if (parent instanceof Statement statement) {
			return statement;
		}
		return null;
	}

	/**
	 * Get the statement of the block which contains the given node.
	 *
	 * @param node
	 * @return
	 */
	public static Statement getBlockParentStatement(ASTNode node) {
		ASTNode parent = node.getParent();
		Statement lastStatement = null;
		while (parent != null && !(parent instanceof Block)) {
			if (parent instanceof Statement statement) {
				lastStatement = statement;
			}
			parent = parent.getParent();
		}
		return lastStatement;
	}

	public static MethodDeclaration getParentMethodDeclaration(ASTNode node) {
		ASTNode parent = node;
		while (parent != null && !(parent instanceof MethodDeclaration)) {
			parent = parent.getParent();
		}
		if (parent instanceof MethodDeclaration methodDeclaration) {
			return methodDeclaration;
		} else {
			return null;
		}
	}

	public static IJavaElement getRootJavaElement(ASTNode node) {
		ASTNode root = node.getRoot();
		if (root instanceof CompilationUnit cu) {
			return cu.getJavaElement();
		}
		return null;
	}

	public static ICompilationUnit getICompilationUnit(ASTNode node) {
		ASTNode root = node.getRoot();
		if (root instanceof CompilationUnit cu) {
			IJavaElement javaElement = cu.getJavaElement();
			if (javaElement instanceof ICompilationUnit icu) {
				return icu;
			}
		}
		return null;
	}

	public static ASTNode getParent(ASTNode node, Class<? extends ASTNode> parentClass) {
		ASTNode n = node.getParent();
		while (n != null && !parentClass.isInstance(n)) {
			n = n.getParent();
		}
		return n;
	}

	public static ASTNode findASTNodeInICU(final ICompilationUnit icu, final int offset, final int length) {
		@SuppressWarnings("deprecation")
		ASTParser parser = ASTParser.newParser(AST.JLS9);
		parser.setSource(icu);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		NodeFinder finder = new NodeFinder(cu, offset, length);
		return finder.getCoveringNode();
	}

	public static MethodDeclaration findMethodDeclaration(final CompilationUnit cu, final IMethod method) {
		try {
			ISourceRange range = method.getNameRange();
			if (range == null) {
				LOG.debug("Could not get source range of mehotd {}", method);
				return null;
			}

			final ASTNode node = NodeFinder.perform(cu, range);
			return getParentMethodDeclaration(node);
		} catch (JavaModelException e) {
			LOG.error("Could not get source range of method {}", method, e);
			return null;
		}
	}

	public static Expression leftMostSide(MethodInvocation mi) {
		Expression expr = mi;
		while (expr instanceof MethodInvocation methodInvocation) {
			expr = methodInvocation.getExpression();
		}
		return expr;
	}

	public static String getMethodName(ASTNode node) {
		if (node instanceof MethodInvocation methodInvocation) {
			IMethodBinding binding = methodInvocation.resolveMethodBinding();
			if (binding != null) {
				return binding.getName();
			}

		}
		return null;
	}

	public static String getMethodQualifiedName(ASTNode node) {
		IMethodBinding binding;
		if (node instanceof MethodInvocation methodInvocation) {
			binding = methodInvocation.resolveMethodBinding();
		} else if (node instanceof MethodDeclaration methodDeclaration) {
			binding = methodDeclaration.resolveBinding();
		} else {
			throw new IllegalArgumentException("Node has to be a MethodInvocation or MethodDeclaration");
		}

		if (binding != null) {
			return BindingUtil.qualifiedName(binding);
		}

		return null;
	}

	public static String getPackageNameOf(CompilationUnit unit) {
		PackageDeclaration pkg = unit.getPackage();
		if (pkg!=null) {
			IPackageBinding pkgBinding = pkg.resolveBinding();
			if (pkgBinding!=null) {
				return pkgBinding.getName();
			}
		}
		return "";
	}

	public static String getQualifiedClassNameOf(ASTNode node) {
		TypeDeclaration cl = getParentClass(node);
		if (cl!=null) {
			return cl.getName().getFullyQualifiedName();
		}
		return "";

	}

	public static String getSimpleClassNameOf(ASTNode node) {
		TypeDeclaration cl = getParentClass(node);
		if (cl!=null) {
			return cl.getName().toString();
		}
		return "";
	}

	public static TypeDeclaration getParentClass(ASTNode node) {
		ASTNode parentNode = node.getParent();
		while (parentNode != null && !(parentNode instanceof TypeDeclaration)) {
			parentNode = parentNode.getParent();
		}

		if (parentNode instanceof TypeDeclaration typeDeclaration) {
			return typeDeclaration;
		}

		return null;
	}

}
