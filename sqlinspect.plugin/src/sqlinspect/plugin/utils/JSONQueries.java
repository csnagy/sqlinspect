package sqlinspect.plugin.utils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sqlinspect.plugin.model.CallStack;
import sqlinspect.plugin.model.Query;

public class JSONQueries {
	private static final Logger LOG = LoggerFactory.getLogger(JSONQueries.class);

	private static final String QUERY_HOTSPOTFINDER = "HotspotFinder";
	private static final String CALLSTACK_NODE = "CallStack";
	private static final String CALL_METHOD = "method";
	private static final String CALL_PATH = "file";
	private static final String METHOD_LINE = "methodLine";
	private static final String CALL_LINE = "callLine";
	private static final String QUERY_ATTR_ID = "id";
	private static final String QUERY_NODE_VALUE = "Value";
	private static final String QUERY_NODE_EXEC_PACKAGE = "ExecPackage";
	private static final String QUERY_NODE_EXEC_CLASS = "ExecClass";
	private static final String QUERY_NODE_EXEC_FILE = "ExecFile";
	private static final String QUERY_NODE_EXEC_LINE = "ExecLine";
	private static final String QUERY_NODE_EXEC_STRING = "ExecString";

	private final File file;
	private final ObjectMapper mapper = new ObjectMapper();

	private boolean noIds;

	public JSONQueries(File file) {
		this.file = file;
		noIds = false;
	}

	public void writeQueries(Collection<Query> queries, boolean noIds) throws IOException {
		LOG.debug("Dump queries to file {} ", file.getAbsolutePath());

		ObjectNode rootNode = mapper.createObjectNode();
		ArrayNode queriesArr = mapper.createArrayNode();

		try {
			this.noIds = noIds;

			int id = 0;
			for (Query q : queries) {
				++id;

				queriesArr.add(writeQueryNode(q, id));
			}

			rootNode.set("Queries", queriesArr);

			mapper.writerWithDefaultPrettyPrinter().writeValue(file, rootNode);
		} catch (IOException e) {
			LOG.error("Exception: ", e);
		}
		LOG.debug("Dump queries done.");
	}

	private ObjectNode writeQueryNode(Query q, int id) {
		ObjectNode node = mapper.createObjectNode();

		if (!noIds) {
			node.put(QUERY_ATTR_ID, Integer.toString(id));
		}

		node.put(QUERY_NODE_VALUE, q.getValue());
		node.put(QUERY_HOTSPOTFINDER, q.getHotspot().getHotspotFinder().getName());
		node.put(QUERY_NODE_EXEC_PACKAGE, ASTUtils.getPackageNameOf(q.getHotspot().getUnit()));
		node.put(QUERY_NODE_EXEC_CLASS, ASTUtils.getQualifiedClassNameOf(q.getHotspot().getExec()));
		node.put(QUERY_NODE_EXEC_FILE, EclipseProjectUtil
				.getOriginalPathOfFile(q.getHotspot().getProject().getIProject(), q.getHotspot().getPath()).toString());
		node.put(QUERY_NODE_EXEC_LINE, Integer.toString(ASTUtils.getLineNumber(q.getHotspot().getExec())));
		node.put(QUERY_NODE_EXEC_STRING, q.getHotspot().getExec().toString());

		node.set(CALLSTACK_NODE, writeCallStack(q));
		return node;
	}

	private ArrayNode writeCallStack(Query q) {
		ArrayNode arr = mapper.createArrayNode();

		CallStack stack = CallStack.calculate(q.getRoot());
		for (CallStack.CallStackElement e : stack.stack) {
			arr.add(writeCall(q, e));
		}

		return arr;
	}

	private ObjectNode writeCall(Query q, CallStack.CallStackElement e) {
		ObjectNode node = mapper.createObjectNode();

		IMethodBinding mb = e.getMethodDeclBinding();
		node.put(CALL_METHOD, mb == null ? "Unknown" : BindingUtil.qualifiedSignature(mb));

		IPath lpath = e.getPath();
		node.put(CALL_PATH,
				lpath == null ? "Unknown"
						: EclipseProjectUtil.getOriginalPathOfFile(q.getHotspot().getProject().getIProject(), lpath)
								.toString());

		node.put(METHOD_LINE, e.getMethodLineNumber());
		node.put(CALL_LINE, e.getCallLineNumber());
		return node;
	}
}
