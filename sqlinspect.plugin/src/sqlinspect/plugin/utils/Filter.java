package sqlinspect.plugin.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IPath;

public final class Filter {
	private final List<Pattern> patterns;

	private Filter(String filterFile) throws IOException {
		Path path = Paths.get(filterFile);
		Stream<String> lines = Files.lines(path);
		patterns = lines.map(Pattern::compile).toList();
		lines.close();
	}

	public static Filter loadFromFile(String filterFile) throws IOException {
		return new Filter(filterFile);
	}

	public boolean match(IPath path) {
		for (Pattern p : patterns) {
			if (p.matcher(path.toOSString()).matches()) {
				return true;
			}
		}
		return false;
	}
}
