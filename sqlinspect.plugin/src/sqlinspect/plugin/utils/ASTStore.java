package sqlinspect.plugin.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTRequestor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Creatable
@Singleton
public class ASTStore {
	private static final Logger LOG = LoggerFactory.getLogger(ASTStore.class);

	private final Map<ICompilationUnit, CompilationUnit> store = new HashMap<>();

	private static final ASTParser parser = ASTParser.newParser(AST.JLS19);

	private static class StoreASTRequestor extends ASTRequestor {
		private final Map<ICompilationUnit, CompilationUnit> store;

		public StoreASTRequestor(Map<ICompilationUnit, CompilationUnit> store) {
			this.store = store;
		}

		@Override
		public void acceptAST(ICompilationUnit unit, CompilationUnit ast) {
			LOG.debug("Parsing of compilation is done: {}", unit.getElementName());
			dumpParserErrors(ast);
			store.put(unit, ast);
		}
	}

	private static CompilationUnit parse(ICompilationUnit unit) {
		LOG.debug("Parsing compilation unit: {}", unit.getElementName());

		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit); // set source
		parser.setResolveBindings(true); // we need bindings later on
		parser.setBindingsRecovery(true);
		parser.setStatementsRecovery(true);
		Map<String, String> options = JavaCore.getOptions();
		JavaCore.setComplianceOptions(JavaCore.VERSION_1_8, options);
		options.put(JavaCore.COMPILER_SOURCE, JavaCore.VERSION_1_8);
		options.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM, JavaCore.VERSION_1_8);
		parser.setCompilerOptions(options);

		CompilationUnit cu;
		try {
			cu = (CompilationUnit) parser.createAST(new NullProgressMonitor());

			dumpParserErrors(cu);

			IPath unitPath = unit.getPath();
			IPath absolutePath = unitPath.makeRelativeTo(ResourcesPlugin.getWorkspace().getRoot().getFullPath());
			String source = absolutePath.toOSString();

			cu.setProperty("SOURCE_WSPATH", source);
		} catch (Exception e) {
			LOG.error("Unexpected error while parsing the compilation unit.", e);
			return null;
		}
		return cu;
	}

	public void parse(IJavaProject javaProject) throws JavaModelException {
		LOG.debug("Parsing compilation units in project {}", javaProject.getElementName());

		parser.setProject(javaProject);
		parser.setResolveBindings(true); // we need bindings later on
		parser.setBindingsRecovery(true);
		parser.setStatementsRecovery(true);
		Map<String, String> options = JavaCore.getOptions();
		JavaCore.setComplianceOptions(JavaCore.VERSION_11, options);
		options.put(JavaCore.COMPILER_SOURCE, JavaCore.VERSION_11);
		options.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM, JavaCore.VERSION_11);
		parser.setCompilerOptions(options);

		StoreASTRequestor requestor = new StoreASTRequestor(store);
		List<ICompilationUnit> icus = new ArrayList<>();

		IPackageFragment[] packages = javaProject.getPackageFragments();
		for (IPackageFragment p : packages) {
			if (p.getKind() == IPackageFragmentRoot.K_SOURCE) {
				Collections.addAll(icus, p.getCompilationUnits());
			}
		}

		parser.createASTs(icus.toArray(new ICompilationUnit[0]), new String[] { "" }, requestor,
				new NullProgressMonitor());
	}

	private static void dumpParserErrors(CompilationUnit ast) {
		IProblem[] problems = ast.getProblems();

		if (problems.length > 0) {
			ArrayList<String> messages = new ArrayList<>();
			int error = 0;
			for (IProblem p : problems) {
				if (p.isError()) {
					++error;
					messages.add(Integer.toString(p.getSourceLineNumber()) + ": " + p.getMessage());
				}
			}
			if (error > 0) {
				LOG.warn("There were problems parsing the compilation unit: {}",
						ast.getJavaElement().getElementName());
				for (String s : messages) {
					LOG.warn(s);
				}
			}
		}
	}

	public CompilationUnit get(ICompilationUnit unit) {
		return store.computeIfAbsent(unit, ASTStore::parse);
	}

	public void clearStore() {
		store.clear();
	}

	public void remove(ICompilationUnit unit) {
		store.remove(unit);
	}

	public boolean inStore(ICompilationUnit unit) {
		return store.containsKey(unit);
	}
}
