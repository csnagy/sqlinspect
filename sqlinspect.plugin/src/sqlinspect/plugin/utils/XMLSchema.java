package sqlinspect.plugin.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.util.ASGUtils;

public class XMLSchema {
	private static final Logger LOG = LoggerFactory.getLogger(XMLSchema.class);

	private static final String ROOT_NODE = "Project";
	private static final String DATABASE_NODE = "Database";
	private static final String DATABASE_NAME = "name";
	private static final String DATABASE_SCHEMAS = "Schemas";
	private static final String SCHEMA_NODE = "Schema";
	private static final String SCHEMA_NAME = "name";
	private static final String SCHEMA_TABLES = "Tables";
	private static final String TABLE_NODE = "Table";
	private static final String TABLE_NAME = "name";
	private static final String TABLE_COLUMNS = "Columns";
	private static final String COLUMN_NODE = "Column";
	private static final String COLUMN_TYPE = "type";
	private static final String COLUMN_NAME = "name";

	private static final String TAB_STR = "  ";

	private final File file;
	private XMLStreamWriter writer;
	private int indent;

	public XMLSchema(File file) {
		this.file = file;
		indent = 1;
	}

	public void writeSchema(SQLRoot root) throws IOException {
		LOG.debug("Dump schema to file {} ", file.getAbsolutePath());
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

		try (OutputStream ostream = Files.newOutputStream(file.toPath())) {
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			for (Database db : root.getDatabases()) {
				writeDatabaseNode(db);
			}

			writer.writeEndElement();
			nl();
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			LOG.error("Could not write XML.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					LOG.error("Could not clean XMLStreamWriter!", e);
				}
			}
		}
		LOG.debug("Dump schema done.");
	}

	private void writeDatabaseNode(Database db) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(DATABASE_NODE);
		writer.writeAttribute(DATABASE_NAME, db.getName());
		nl();

		writeDatabaseSchemas(db);

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeDatabaseSchemas(Database db) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(DATABASE_SCHEMAS);
		nl();

		for (Schema sch : db.getSchemas()) {
			writeDatabaseSchema(sch);
		}

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeDatabaseSchema(Schema sch) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(SCHEMA_NODE);
		writer.writeAttribute(SCHEMA_NAME, sch.getName());
		nl();

		writeSchemaTables(sch);

		writer.writeEndElement();
		nl();
		--indent;
	}

	private void writeSchemaTables(Schema sch) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(SCHEMA_TABLES);
		nl();

		for (Table tab : sch.getTables()) {
			writeSchemaTable(tab);
		}

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeSchemaTable(Table tab) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(TABLE_NODE);
		writer.writeAttribute(TABLE_NAME, tab.getName());
		nl();

		writeTableColumns(tab);

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeTableColumns(Table tab) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(TABLE_COLUMNS);
		nl();

		for (Column col : tab.getColumns()) {
			writeTableColumn(col);
		}

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeTableColumn(Column col) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(COLUMN_NODE);

		writer.writeAttribute(COLUMN_NAME, col.getName());
		writer.writeAttribute(COLUMN_TYPE, ASGUtils.columnTypeToString(col));

		writer.writeEndElement();
		nl();
		--indent;
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters("\n");
	}

	private void tab(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TAB_STR);
		}
	}
}
