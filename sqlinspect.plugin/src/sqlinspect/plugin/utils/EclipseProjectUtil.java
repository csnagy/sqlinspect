package sqlinspect.plugin.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.IVMInstallType;
import org.eclipse.jdt.launching.JavaRuntime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class EclipseProjectUtil {
	private static final Logger LOG = LoggerFactory.getLogger(EclipseProjectUtil.class);
	private static final Pattern packagePattern = Pattern.compile("^\\s*package\\s+([^; ]*)\\s*;\\s*$");

	private EclipseProjectUtil() {
		// Empty constructor. Just to prevent instantiation.
	}

	private static void dumpInstalledVMs() {
		LOG.debug("Current JRE version: {} {} {}", System.getProperty("java.vm.vendor"),
				System.getProperty("java.vm.name"), System.getProperty("java.version"));

		final IVMInstallType[] vmTypes = JavaRuntime.getVMInstallTypes();
		if (vmTypes.length > 0) {
			LOG.debug("Installed VMs: ");
			for (IVMInstallType vit : vmTypes) {
				LOG.debug("VM: {} {}", vit.getId(), vit.getName());
			}
		} else {
			LOG.debug("Could not find installed VMs");
		}

		IVMInstall vm = JavaRuntime.getDefaultVMInstall();
		if (vm == null) {
			LOG.debug("Error, could not get default JavaRuntime VM!");
		} else {
			LOG.debug("Default VM: {} {} at {}", vm.getId(), vm.getName(), vm.getInstallLocation());
		}

		IPath jrePath = JavaRuntime.newDefaultJREContainerPath();
		if (jrePath == null) {
			LOG.debug("Error, could not get default JRE ContainerPath!");
		} else {
			LOG.debug("Default JRE Container Path: {}", jrePath);
		}

	}

	public static IProject createEclipseProject(String projectname, String origBaseDir) throws CoreException {
		return createEclipseProject(projectname, origBaseDir, null, null, null);
	}

	public static IProject createEclipseProject(String projectname, String origBaseDir, String origSrcDirName)
			throws CoreException {
		return createEclipseProject(projectname, origBaseDir, origSrcDirName, null, null);
	}

	public static IProject createEclipseProject(String projectname, String origBaseDir, String origSrcDirName,
			String projectCP, String projectCPSeparator) throws CoreException {
		LOG.debug("Creating project {} under {}", projectname, origBaseDir);

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject iproj = root.getProject(projectname);
		if (iproj.exists()) {
			iproj.delete(false, null);
		}

		dumpInstalledVMs();

		iproj = root.getProject(projectname);
		IProjectDescription projDesc = ResourcesPlugin.getWorkspace().newProjectDescription(projectname);
		projDesc.setNatureIds(new String[] { JavaCore.NATURE_ID });
		iproj.create(projDesc, null);
		iproj.open(null);

		IJavaProject javaProject = JavaCore.create(iproj);

		SortedMap<String, Set<String>> rootDirs;
		if (origSrcDirName != null && !origSrcDirName.isEmpty()) {
			rootDirs = getSourceRoots(origBaseDir + "/" + origSrcDirName);
		} else {
			rootDirs = getSourceRoots(origBaseDir);
		}

		LOG.debug("Root directories: ");
		List<IClasspathEntry> cpEntries = new ArrayList<>();
		int i = 0;
		for (String dir : rootDirs.keySet()) {
			LOG.debug(" - {}", dir);
			IPath path = new Path(dir);
			String rootFolderName = Integer.toString(i) + "_" + path.lastSegment();
			cpEntries.add(JavaCore.newSourceEntry(iproj.getFullPath().append(rootFolderName)));
			i++;
		}

		cpEntries.add(JavaRuntime.getDefaultJREContainerEntry());

		if (projectCP != null && !projectCP.isEmpty()) {
			if (projectCPSeparator != null && !projectCPSeparator.isEmpty()) {
				String[] cpes = projectCP.split(projectCPSeparator);
				for (String cpe : cpes) {
					cpEntries.add(JavaCore.newLibraryEntry(new Path(cpe), null, null, true));
				}
			} else {
				throw new IllegalArgumentException(
						"classpath separator cannot be null/empty if classpath is specified");
			}
		}

		cpEntries.stream().forEach(cp -> LOG.debug("Classpath entry: {}", cp.getPath()));

		javaProject.setRawClasspath(cpEntries.toArray(new IClasspathEntry[cpEntries.size()]),
				iproj.getFullPath().append("bin"), null);

		i = 0;
		for (Map.Entry<String, Set<String>> e : rootDirs.entrySet()) {
			String rootDir = e.getKey();
			Set<String> subDirs = e.getValue();
			IPath path = new Path(rootDir);
			String rootFolderName = i + "_" + path.lastSegment();
			IFolder rootFolder = createRootFolder(iproj, rootFolderName, rootDir);
			IPackageFragmentRoot rootPackage = javaProject.getPackageFragmentRoot(rootFolder);
			loadJavaSources(rootPackage, rootFolder, rootDir, subDirs);
			i++;
		}

		LOG.info("Project created");
		return iproj;
	}

	private static void loadJavaSources(IPackageFragmentRoot rootPackage, IFolder rootFolder, String rootPath,
			Set<String> subDirs) throws CoreException {
		File rootDir = new File(rootPath);
		if (!rootDir.exists()) {
			LOG.error("Directory does not exists! {}", rootDir);
			return;
		}

		for (String dir : subDirs) {
			try {
				loadJavaDirectory(rootPackage, rootFolder, rootDir, dir);
			} catch (Exception e) {
				LOG.error("Could not load resource directory: " + dir, e);
			}
		}
	}

	private static void loadJavaDirectory(IPackageFragmentRoot rootPackage, IFolder rootFolder, File rootDir,
			String dir) throws CoreException {
		File d = new File(dir);
		LOG.debug("Reading directory: {}", d.getPath());
		String dirPath = d.getAbsolutePath().substring(rootDir.getAbsolutePath().length() + 1);
		LOG.debug("Shortened path: {}", dirPath);
		IFolder folder = rootFolder.getFolder(dirPath);
		String packagename = dirPath.replace('/', '.').replace('\\', '.');

		if (!folder.exists()) {
			LOG.debug("Create folder: {}", folder.getName());
			folder.create(true, true, null);
		}

		IPackageFragment fragment = rootPackage.getPackageFragment(packagename);
		if (!fragment.exists()) {
			LOG.warn("Create package: {}", packagename);
			fragment = rootPackage.createPackageFragment(packagename, true, null);
		}

		File[] files = d.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isFile() && f.getName().toLowerCase().endsWith(".java")) {
					LOG.debug("Create CU: {}", f.getName());
					try {
						String encoding = null; // platform default
						String content = FileUtils.readFileToString(f, encoding);
						fragment.createCompilationUnit(f.getName(), content, true, null);
					} catch (IOException e) {
						LOG.error("IO Exception: {}", e.getMessage());
					}
				}
			}
		}
	}

	private static IFolder createRootFolder(IProject iproj, String rootFolderName, String rootDir)
			throws CoreException {
		IPath rootPath = new Path(rootDir);
		IFolder rootFolder = iproj.getFolder(rootFolderName);
		IStatus status = ResourcesPlugin.getWorkspace().validateLinkLocation(rootFolder, rootPath);
		if (status.isOK()) {
			rootFolder.createLink(rootPath, IResource.NONE, null);
		} else {
			LOG.error("Cannot create link: {}", status.getMessage());
		}

		LOG.info("Created source directory: {} -> {}", rootFolder.getFullPath(), rootFolder.getLocation());
		return rootFolder;
	}

	private static SortedMap<String, Set<String>> getSourceRoots(String dir) {
		LOG.info("Detecting source root directories...");
		Collection<File> javaFiles = FileUtils.listFiles(new File(dir), new String[] { "java" }, true);
		SortedMap<String, Set<String>> rootDirs = new TreeMap<>();
		for (File f : javaFiles) {
			try {
				String packagename = getPackageDecl(f);
				if (packagename != null) {
					String rootdir = getRootDir(f, packagename);
					if (rootdir != null) {
						Set<String> packageDirs = rootDirs.get(rootdir);
						if (packageDirs == null) {
							packageDirs = new HashSet<>();
							rootDirs.put(rootdir, packageDirs);
						}
						packageDirs.add(f.getParent());
					}
				} else {
					LOG.warn("Could not get package for file: {}", f);
				}
			} catch (IOException e) {
				LOG.error("IO Exception: ", e);
			}
		}
		return rootDirs;
	}

	private static String getRootDir(File f, String packagename) {
		String fpath = f.getAbsolutePath();
		int tocut = fpath.length() - f.getName().length() - packagename.length();
		if (fpath.length() > tocut) {
			String rootPath = fpath.substring(0, tocut - 1);
			if (rootPath.charAt(rootPath.length() - 1) != '/') {
				rootPath += '/';
			}
			File root = new File(rootPath);
			if (!root.exists()) {
				LOG.warn("Conflicting package name! File: {} package: {}", f, packagename);
				return null;
			}
			return rootPath;
		} else {
			return null;
		}
	}

	private static String getPackageDecl(File f) throws IOException {
		String packagename = null;
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(Files.newInputStream(f.toPath()), StandardCharsets.UTF_8))) {
			String line = br.readLine();
			while (packagename == null && line != null) {
				Matcher pkgMatch = packagePattern.matcher(line);
				if (pkgMatch.find()) {
					packagename = pkgMatch.group(1);
				}
				line = br.readLine();
			}
		}
		return packagename;
	}

	public static IPath getOriginalPathOfFile(IProject proj, IPath path) {
		IPath absolute = path.makeAbsolute();
		IFile f = proj.getWorkspace().getRoot().getFile(absolute);
		IPath projPath = f.getProjectRelativePath();
		String rootName = projPath.segment(1);
		IPath sourcePath = projPath.removeFirstSegments(2);
		IFolder rootFolder = proj.getFolder(rootName);
		if (rootFolder != null && rootFolder.isLinked()) {
			IPath realRootPath = rootFolder.getRawLocation();
			IPath realFilePath = realRootPath.append(sourcePath);
			return realFilePath.makeAbsolute();
		} else {
			return f.getRawLocation().makeAbsolute();
		}
	}
}
