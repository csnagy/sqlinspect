package sqlinspect.plugin.smells.sql.common;

import java.util.Objects;

import sqlinspect.sql.asg.base.Base;

public class SQLSmell {
	private Base node;
	private SQLSmellKind kind;
	private String message;
	private SQLSmellCertKind certainty;
	private String file;
	private int line;

	public SQLSmell(Base node, SQLSmellKind kind, String message, SQLSmellCertKind certainty) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = "";
		this.line = 0;
	}

	public SQLSmell(Base node, SQLSmellKind kind, String message, SQLSmellCertKind certainty, String file, int line) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = file;
		this.line = line;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof SQLSmell sqlSmell) {
			return Objects.equals(sqlSmell.getNode(), node) && Objects.equals(sqlSmell.getKind(), kind)
					&& Objects.equals(sqlSmell.getFile(), file) && Objects.equals(sqlSmell.getLine(), line)
					&& Objects.equals(sqlSmell.getMessage(), message)
					&& Objects.equals(sqlSmell.getCertainty(), certainty);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(node, file, line, kind, certainty);
	}

	public Base getNode() {
		return node;
	}

	public void setNode(Base node) {
		this.node = node;
	}

	public SQLSmellKind getKind() {
		return kind;
	}

	public void setKind(SQLSmellKind kind) {
		this.kind = kind;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SQLSmellCertKind getCertainty() {
		return certainty;
	}

	public void setCertainty(SQLSmellCertKind certainty) {
		this.certainty = certainty;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	@Override
	public String toString() {
		return "SQLSmell [node=" + node + ", kind=" + kind + " certainty=" + certainty + " message=" + message
				+ " file: " + file + " sline: " + line + "]";
	}
}
