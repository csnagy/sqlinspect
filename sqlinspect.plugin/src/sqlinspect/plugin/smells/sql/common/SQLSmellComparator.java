package sqlinspect.plugin.smells.sql.common;

import java.util.Comparator;

public class SQLSmellComparator implements Comparator<SQLSmell> {
	@Override
	public int compare(SQLSmell s1, SQLSmell s2) {
		int fileDiff = s1.getNode().getPath().compareTo(s2.getNode().getPath());
		if (fileDiff != 0) {
			return fileDiff;
		}

		int lineDiff = Integer.compare(s1.getNode().getStartLine(), s2.getNode().getStartLine());
		if (lineDiff != 0) {
			return lineDiff;
		}

		int kindDiff = s1.getKind().compareTo(s2.getKind());
		if (kindDiff != 0) {
			return kindDiff;
		}

		int certaintyDiff = s1.getCertainty().compareTo(s2.getCertainty());
		if (certaintyDiff != 0) {
			return certaintyDiff;
		}

		int messageDiff = s1.getMessage().compareTo(s2.getMessage());
		if (messageDiff != 0) {
			return messageDiff;
		}

		return 0;
	}
}
