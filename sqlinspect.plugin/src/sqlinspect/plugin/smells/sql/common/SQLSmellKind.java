package sqlinspect.plugin.smells.sql.common;

public enum SQLSmellKind {
	FEAR_OF_THE_UNKNOWN, AMBIGUOUS_GROUPS, RANDOM_SELECTION, IMPLICIT_COLUMNS;
}
