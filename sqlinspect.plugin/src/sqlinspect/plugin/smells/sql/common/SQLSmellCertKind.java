package sqlinspect.plugin.smells.sql.common;

public enum SQLSmellCertKind {
	HIGH_CERTAINTY, NORMAL_CERTAINTY, LOW_CERTAINTY
}
