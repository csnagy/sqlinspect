package sqlinspect.plugin.smells.sql.util;

import java.util.HashSet;
import java.util.Set;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.visitor.Visitor;

public class ColumnRefsVisitor extends Visitor {
	private final Set<Column> columns = new HashSet<>();
	private final Set<String> unresolved = new HashSet<>();

	@Override
	public void visit(Id id) {
		Base ref = id.getRefersTo();
		if (ref instanceof Column column) {
			columns.add(column);
		} else if (id.getName() != null) {
			unresolved.add(id.getName());
		}
	}

	public Set<Column> getColumns() {
		return columns;
	}

	public Set<String> getUnResolved() {
		return unresolved;
	}
}
