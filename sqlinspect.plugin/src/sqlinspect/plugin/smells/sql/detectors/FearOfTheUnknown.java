package sqlinspect.plugin.smells.sql.detectors;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellCertKind;
import sqlinspect.plugin.smells.sql.common.SQLSmellKind;
import sqlinspect.sql.DBHandler;
import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.common.NodeKind;
import sqlinspect.sql.asg.expr.BinaryExpression;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.IsNull;
import sqlinspect.sql.asg.expr.Joker;
import sqlinspect.sql.asg.expr.Literal;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.expr.Unary;
import sqlinspect.sql.asg.expr.UnaryExpression;
import sqlinspect.sql.asg.kinds.BinaryExpressionKind;
import sqlinspect.sql.asg.kinds.ColumnAttributeKind;
import sqlinspect.sql.asg.kinds.LiteralKind;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.ColumnAttribute;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Update;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.asg.visitor.Visitor;

/*
 * "Many software developers are caught off-guard by the behavior of null
 * in SQL. Unlike in most programming languages, SQL treats null as a
 * special value, different from zero, false, or an empty string. This is true
 * in standard SQL and most brands of database. However, in Oracle and
 * Sybase, null is exactly the same as a string of zero length. The null
 * value follows some special behavior, too.
 *
 * Problem:
 *  - NULL is not the same as zero, an arithmetic expression with NULL will result NULL.
 *    E.g. SELECT hours + 10 FROM Bugs;  - will return NULL if hours is NULL
 *  - NULL is not the same as a string of zero length. Combining any string with null in standard SQL returns NULL
 *   (except Oracle and Sybase)
 *    E.g. SELECT first_name || ' ' || middle_initial || ' ' || last_name AS full_name
 *            FROM Accounts; - will return NULL if middle_initial is NULL
 *  - NULL is not the same as FALSE. Boolean expressions with AND, OR and NOT also produce results that some people find
 *    confusing.
 *    E.g. SELECT * FROM Bugs WHERE NOT (assigned_to = 123) - will not return those records where assigned_to is NULL
 *         SELECT * from Bugs WHERE assigned_to = NULL; - a comparison to NULL is never TRUE
 *         SELECT * from Bugs WHERE assigned_to <> NULL; - a comparison to NULL is never TRUE
 *
 * Implementation:
 *
 *   We check the following:
 *    - a column is used in one of the following expressions: column = NULL or column <> NULL
 *      => Probably an error, HIGH_CERTAINTY
 *    - a column is nullable and it is used in an expression
 *      - if the column is in an arithmetic/string/boolean expression => NORMAL_CERTAINTY
 *        - if the table has a record with a NULL field => HIGH_CERTAINTY
 *      - otherwise, probably not an error, LOW_CERTAINTY
 */

public class FearOfTheUnknown extends Visitor {
	private final Set<SQLSmell> smells = new HashSet<>();

	private final Deque<Base> selectStack = new ArrayDeque<>();
	private final Map<Column, Boolean> nullableColumns = new HashMap<>();

	private static final String MESSAGE1 = "'%s' is in a NULL comparison.";
	private static final String MESSAGE2 = "'%s' has NULL values in the database and it is used in an expression.";
	private static final String MESSAGE3 = "'%s' is nullable and used in an expression without NULL check.";
	private static final String MESSAGE4 = "Dangerous NULL comparison.";

	private static final String QUESTION1 = "{{question}}";
	private static final String QUESTION2 = "?";

	private Optional<DBHandler> dbHandler = Optional.empty();

	private boolean inUpdateSetList;

	public void setDBHandler(DBHandler dbHandler) {
		this.dbHandler = Optional.of(dbHandler);
	}

	@Override
	public void visit(SelectExpression n) {
		selectStack.push(n);
	}

	@Override
	public void visitEnd(SelectExpression n) {
		selectStack.pop();
	}

	@Override
	public void visit(BinaryExpression n) {
		if (!inUpdateSetList && isNullComparison(n)) {
			Expression left = n.getLeft();
			Expression right = n.getRight();

			if ((isNullLiteral(left) && !hasColumnReference(right))
					|| (isNullLiteral(right) && !hasColumnReference(left))) {
				smells.add(
						new SQLSmell(n, SQLSmellKind.FEAR_OF_THE_UNKNOWN, MESSAGE4, SQLSmellCertKind.NORMAL_CERTAINTY));
			}
		}
	}

	@Override
	public void visit(Id n) {
		if (selectStack.isEmpty()) {
			return;
		}

		Column col = getColumnReference(n);
		if (col != null) {
			// the case of = NULL or <> NULL
			Expression parent = getParentExpression(n);
			if (isNullComparison(parent)) {
				String message = String.format(MESSAGE1, ASGUtils.getColumnFullName(col));

				smells.add(new SQLSmell(n, SQLSmellKind.FEAR_OF_THE_UNKNOWN, message, SQLSmellCertKind.HIGH_CERTAINTY));
			}

			if (columnIsNullable(col) && (isRiskyExpression(parent) || isRiskyComparison(parent))
					&& !isInNullCheck(getParentBinaryOrUnary(n), n)) {
				if (columnHasNullRecord(col)) {
					String message = String.format(MESSAGE2, ASGUtils.getColumnFullName(col));

					smells.add(new SQLSmell(n, SQLSmellKind.FEAR_OF_THE_UNKNOWN, message,
							SQLSmellCertKind.HIGH_CERTAINTY));
				} else {
					String message = String.format(MESSAGE3, ASGUtils.getColumnFullName(col));
					smells.add(
							new SQLSmell(n, SQLSmellKind.FEAR_OF_THE_UNKNOWN, message, SQLSmellCertKind.LOW_CERTAINTY));
				}
			}
		}
	}

	@Override
	public void visitEdge_Update_SetList(Update node, ExpressionList end) {
		inUpdateSetList = true;
	}

	@Override
	public void visitEdgeEnd_Update_SetList(Update node, ExpressionList end) {
		inUpdateSetList = false;
	}

	/**
	 * Get the parent expression containing the id. The trick is, that the id can be
	 * a part of a field selector (e.g. table.column) and in this case we return the
	 * parent expression of the field selector.
	 *
	 * @param id
	 * @return
	 */
	private Expression getParentExpression(Id id) {
		if (id == null) {
			return null;
		}

		Base parent = id.getParent();
		while (parent != null) {
			if (parent instanceof BinaryExpression binaryExpression) {
				if (binaryExpression.getKind() == BinaryExpressionKind.FIELDSELECTOR) {
					parent = parent.getParent();
				} else {
					return binaryExpression;
				}
			} else if (parent instanceof Expression expression) {
				return expression;
			} else {
				return null;
			}
		}

		return null;
	}

	private boolean isGoodNullComparison(Expression expr) {
		if (expr instanceof BinaryExpression binaryExpression) {
			Expression left = binaryExpression.getLeft();
			Expression right = binaryExpression.getRight();

			if (!isNullExpr(left) && !isNullExpr(right)) {
				return false;
			}

			if (binaryExpression.getKind() == BinaryExpressionKind.IS) {
				return true;
			}
		}
		return false;
	}

	private boolean isNullComparison(Expression expr) {
		if (expr instanceof BinaryExpression binaryExpression) {
			Expression left = binaryExpression.getLeft();
			Expression right = binaryExpression.getRight();

			if (!isNullExpr(left) && !isNullExpr(right)) {
				return false;
			}

			if (binaryExpression.getKind() == BinaryExpressionKind.EQUALS
					|| binaryExpression.getKind() == BinaryExpressionKind.GREATEROREQUALS
					|| binaryExpression.getKind() == BinaryExpressionKind.LESSOREQUALS
					|| binaryExpression.getKind() == BinaryExpressionKind.NOTEQUALS) {
				return true;
			}
		}
		return false;
	}

	private static boolean isNullLiteral(Expression expr) {
		if (expr != null && expr.getNodeKind() == NodeKind.LITERAL) {
			Literal lit = (Literal) expr;
			if (lit.getKind() == LiteralKind.NULL) {
				return true;
			}
		}
		return false;
	}

	private static boolean isNullExpr(Expression expr) {
		if (expr == null) {
			return false;
		}
		if (expr instanceof UnaryExpression unaryExpression) {
			return isNullExpr(unaryExpression.getExpression());
		}
		if (expr instanceof IsNull) {
			return true;
		}
		return isNullLiteral(expr);
	}

	private static boolean isComparison(BinaryExpressionKind kind) {
		return kind == BinaryExpressionKind.EQUALS || kind == BinaryExpressionKind.GREATEROREQUALS
				|| kind == BinaryExpressionKind.GREATERTHAN || kind == BinaryExpressionKind.LESSOREQUALS
				|| kind == BinaryExpressionKind.LESSTHAN || kind == BinaryExpressionKind.NOTEQUALS;
	}

	private static boolean isArithmeticOrStringOperation(BinaryExpressionKind kind) {
		return kind == BinaryExpressionKind.BITAND || kind == BinaryExpressionKind.BITOR
				|| kind == BinaryExpressionKind.CONCAT || kind == BinaryExpressionKind.MINUS
				|| kind == BinaryExpressionKind.PLUS || kind == BinaryExpressionKind.MULTIPLY
				|| kind == BinaryExpressionKind.DIVIDE || kind == BinaryExpressionKind.MODULO
				|| kind == BinaryExpressionKind.OR;
	}

	private static boolean isRiskyComparison(Expression expr) {
		if (expr instanceof BinaryExpression binaryExpression && isComparison(binaryExpression.getKind())) {
			Expression lhs = binaryExpression.getLeft();
			Expression rhs = binaryExpression.getRight();
			if ((lhs instanceof Id leftId && (QUESTION1.equals(leftId.getName()) || QUESTION2.equals(leftId.getName())))
					|| (lhs instanceof Joker leftJoker
							&& (QUESTION1.equals(leftJoker.getName()) || QUESTION2.equals(leftJoker.getName())))
					|| (rhs instanceof Id rightId
							&& (QUESTION1.equals(rightId.getName()) || QUESTION2.equals(rightId.getName())))
					|| (rhs instanceof Joker rightJoker
							&& (QUESTION1.equals(rightJoker.getName()) || QUESTION2.equals(rightJoker.getName())))
					|| (lhs instanceof Literal && !isNullLiteral(lhs))
					|| (rhs instanceof Literal && !isNullLiteral(rhs))) {
				return true;
			}

		}
		return false;
	}

	private static boolean isRiskyExpression(Expression expr) {
		if (expr instanceof BinaryExpression binaryExpression
				&& isArithmeticOrStringOperation(binaryExpression.getKind())) {
			Expression lhs = binaryExpression.getLeft();
			Expression rhs = binaryExpression.getRight();
			if ((lhs instanceof Id leftId && (QUESTION1.equals(leftId.getName()) || QUESTION2.equals(leftId.getName())))
					|| (lhs instanceof Joker leftJoker
							&& (QUESTION1.equals(leftJoker.getName()) || QUESTION2.equals(leftJoker.getName())))
					|| (rhs instanceof Id rightId
							&& (QUESTION1.equals(rightId.getName()) || QUESTION2.equals(rightId.getName())))
					|| (rhs instanceof Joker rightJoker
							&& (QUESTION1.equals(rightJoker.getName()) || QUESTION2.equals(rightJoker.getName())))
					|| lhs instanceof Literal || rhs instanceof Literal) {
				return true;
			}
		}
		return false;
	}

	private boolean columnIsNullable(Column col) {
		boolean hasNotNull = false;
		boolean hasNull = false;
		for (ColumnAttribute attr : col.getAttributes()) {
			if (attr.getKind() == ColumnAttributeKind.NOTNULL) {
				hasNotNull = true;
			} else if (attr.getKind() == ColumnAttributeKind.NULL) {
				hasNull = true;
			}
		}
		return hasNull || !hasNotNull;
	}

	private boolean columnHasNullRecord(Column col) {
		Boolean b = nullableColumns.get(col);
		if (b != null) {
			return b.booleanValue();
		}

		Table parentTable = ASGUtils.getParentTable(col);

		if (parentTable != null && dbHandler.isPresent()
				&& dbHandler.get().getNullableRecords(parentTable.getName(), col.getName()) > 0) {
			nullableColumns.put(col, Boolean.TRUE);
			return true;
		} else {
			nullableColumns.put(col, Boolean.FALSE);
			return false;
		}
	}

	@SuppressWarnings("unused")
	private boolean isIdExpression(Expression expr) {
		if (expr instanceof Id) {
			return true;
		}
		if (expr instanceof BinaryExpression binaryExpression
				&& binaryExpression.getKind() == BinaryExpressionKind.FIELDSELECTOR) {
			return isIdExpression(binaryExpression.getLeft()) && isIdExpression(binaryExpression.getRight());
		}
		return false;
	}

	private boolean hasColumnReference(Expression expr) {
		if (expr instanceof Id id) {
			return hasColumnReference(id);
		} else if (expr instanceof BinaryExpression binaryExpression) {
			return hasColumnReference(binaryExpression);
		} else {
			return false;
		}
	}

	private boolean hasColumnReference(Id id) {
		return id.getRefersTo() != null && id.getRefersTo().getNodeKind() == NodeKind.COLUMN;
	}

	private boolean hasColumnReference(BinaryExpression n) {
		if (n.getKind() == BinaryExpressionKind.FIELDSELECTOR && n.getRight() != null) {
			return hasColumnReference(n.getRight());
		}
		return false;
	}

	private Column getColumnReference(Expression expr) {
		if (expr instanceof Id id) {
			return getColumnReference(id);
		} else if (expr instanceof BinaryExpression binaryExpression) {
			return getColumnReference(binaryExpression);
		} else {
			return null;
		}
	}

	private Column getColumnReference(Id id) {
		if (id.getRefersTo() != null) {
			Base ref = id.getRefersTo();
			if (ref instanceof Column column) {
				return column;
			}
		}
		return null;
	}

	private Column getColumnReference(BinaryExpression binaryExpression) {
		if (binaryExpression.getKind() == BinaryExpressionKind.FIELDSELECTOR && binaryExpression.getRight() != null) {
			return getColumnReference(binaryExpression.getRight());
		}
		return null;
	}

	private Expression getParentBinaryOrUnary(Expression expr) {
		Expression parentExpression = expr;
		while (parentExpression != null) {
			Base parent = parentExpression.getParent();
			if (parent instanceof BinaryExpression binaryExpression) {
				parentExpression = binaryExpression;
			} else if (parent instanceof Unary unary) {
				parentExpression = unary;
			} else {
				break;
			}
		}
		return parentExpression;
	}

	private boolean isInNullCheck(Expression expr, Expression idExpr) {
		if (expr instanceof IsNull) {
			return true;
		} else if (expr instanceof BinaryExpression binaryExpression) {
			return isInNullCheck(binaryExpression, idExpr);
		} else if (expr instanceof Unary unary) {
			return isInNullCheck(unary, idExpr);
		}

		return false;
	}

	private boolean isInNullCheck(BinaryExpression expr, Expression idExpr) {
		Expression left = expr.getLeft();
		Expression right = expr.getRight();

		if (isGoodNullComparison(expr)) {
			Expression col;
			if (isNullExpr(left)) {
				col = right;
			} else {
				col = left;
			}
			if (refersToSameColumn(col, idExpr)) {
				return true;
			}
		}

		return isInNullCheck(left, idExpr) || isInNullCheck(right, idExpr);
	}

	private boolean isInNullCheck(Unary expr, Expression idExpr) {
		return isInNullCheck(expr.getExpression(), idExpr);
	}

	private boolean refersToSameColumn(Expression expr1, Expression expr2) {
		Column leftCol = getColumnReference(expr1);
		Column rightCol = getColumnReference(expr2);
		return leftCol != null && rightCol != null && leftCol.equals(rightCol);
	}

	@SuppressWarnings("unused")
	private boolean hasSameName(Expression expr1, Expression expr2) {
		String s1 = columnRefToString(expr1);
		String s2 = columnRefToString(expr2);
		return s1 != null && s2 != null && s1.equalsIgnoreCase(s2);
	}

	private String columnRefToString(Expression expr) {
		if (expr instanceof Id id) {
			return id.getName();
		} else if (expr instanceof BinaryExpression binaryExpression) {
			StringBuilder sb = new StringBuilder();
			sb.append(columnRefToString(binaryExpression.getLeft())).append('.')
					.append(columnRefToString(binaryExpression.getRight()));
			return sb.toString();
		}
		return null;
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}
}
