package sqlinspect.plugin.smells.sql.detectors;

import java.util.HashSet;
import java.util.Set;

import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellCertKind;
import sqlinspect.plugin.smells.sql.common.SQLSmellKind;
import sqlinspect.sql.asg.expr.Asterisk;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.statm.Insert;
import sqlinspect.sql.asg.visitor.Visitor;

/*
 * "Although using wildcards and unnamed columns satisfies the goal of
 *  less typing, this habit creates several hazards."
 *
 *    SELECT * FROM Bugs;
 *    INSERT INTO Bugs VALUES (DEFAULT, CURDATE(), 'New bug' , 'Test T987 fails...' ,
 *       NULL, 123, NULL, NULL, DEFAULT, 'Medium' , NULL);
 *
 * Implementation:
 *  - if we find a star in the column list of a select exptession, we trigger a warning
 *  - if we find an insert statement withouth a column list, we trigger a warning
 */
public class ImplicitColumns extends Visitor {
	private final Set<SQLSmell> smells = new HashSet<>();
	private static final String MESSAGE = "Name columns explicitly.";

	@Override
	public void visit(SelectExpression n) {
		Expression columns = n.getColumnList();
		if (columns instanceof ExpressionList expressionList) {
			for (Expression expr : expressionList.getExpressions()) {
				if (expr instanceof Asterisk) {
					smells.add(
							new SQLSmell(n, SQLSmellKind.IMPLICIT_COLUMNS, MESSAGE, SQLSmellCertKind.NORMAL_CERTAINTY));
				}
			}
		}
	}

	@Override
	public void visit(Insert n) {
		if (n.getColumnList() == null) {
			smells.add(new SQLSmell(n, SQLSmellKind.IMPLICIT_COLUMNS, MESSAGE, SQLSmellCertKind.NORMAL_CERTAINTY));
		}
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}
}
