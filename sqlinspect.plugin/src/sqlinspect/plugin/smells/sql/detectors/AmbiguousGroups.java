package sqlinspect.plugin.smells.sql.detectors;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellCertKind;
import sqlinspect.plugin.smells.sql.common.SQLSmellKind;
import sqlinspect.plugin.smells.sql.util.ColumnRefsVisitor;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.expr.Alias;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.FunctionCall;
import sqlinspect.sql.asg.expr.FunctionParams;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.traversal.PreOrderTraversal;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.asg.visitor.Visitor;

/*
 * "Every column in the select-list of a query must have a single value row
 *  per row group. This is called the Single-Value Rule. Columns named in
 *  the GROUP BY clause are guaranteed to be exactly one value per group,
 *  no matter how many rows the group matches.
 *  The MAX ( ) expression is also guaranteed to result in a single value for
 *  each group: the highest value found in the argument of MAX ( ) over all
 *  the rows in the group.
 *  However, the database server can’t be so sure about any other column
 *  named in the select-list. It can’t always guarantee that the same value
 *  occurs on every row in a group for those other columns."
 *
 *  Implementation:
 *
 *  Check if the ids in the column list of a select expression are contained in either an
 *  aggregate function or the GROUP BY clause
 *
 *  MySQL reference:
 *    https://dev.mysql.com/doc/refman/5.7/en/group-by-handling.html
 */

public class AmbiguousGroups extends Visitor {
	private final SQLDialect dialect;
	private final Set<SQLSmell> smells = new HashSet<>();

	private final Map<SelectExpression, Set<Column>> groupByColumns = new HashMap<>();
	private final Map<SelectExpression, Set<String>> groupByColumnsUnResolved = new HashMap<>();

	private final Deque<SelectExpression> actualColumnList = new ArrayDeque<>();
	private final Deque<SelectExpression> actualSelectExpression = new ArrayDeque<>();
	private final Deque<SelectExpression> actualGroupBy = new ArrayDeque<>();
	private boolean inSetFunctionCall;

	private static final String MESSAGE = "'%s' refers to a non-grouped column '%s'";

	private static final String[] MYSQL_SET_FUNCTIONS = { "AVG", "COUNT", "MIN", "MAX", "STD", "STD_DEV", "BIT_AND",
			"BIT_OR", "BIT_OR", "BIT_XOR", "VARIANCE", "SUM", "GROUP_CONCAT" };
	private static final String[] IMPALA_SET_FUNCTIONS = { "AVG", "COUNT", "DENSE_RANK", "FIRST_VALUE", "LAG",
			"LAST_VALUE", "LEAD", "MAX", "MIN", "RANK", "ROW_NUMBER", "SUM" };
	private static final String[] SQLITE_AGGREGATE_FUNCTIONS = { "AVG", "COUNT", "GROUP_CONCAT", "MAX", "MIN", "SUM",
			"TOTAL" };

	public AmbiguousGroups(SQLDialect dialect) {
		super();
		this.dialect = dialect;
	}

	@Override
	public void visit(SelectExpression n) {
		actualSelectExpression.addLast(n);

		ExpressionList groupBy = n.getGroupBy();
		if (groupBy != null) {

			ColumnRefsVisitor visitor = new ColumnRefsVisitor();
			PreOrderTraversal traversal = new PreOrderTraversal(visitor);
			traversal.traverse(groupBy);

			groupByColumns.put(n, visitor.getColumns());
			groupByColumnsUnResolved.put(n, visitor.getUnResolved());
		}
	}

	@Override
	public void visitEnd(SelectExpression n) {
		actualSelectExpression.removeLast();
	}

	@Override
	public void visit(Id n) {
		if (actualSelectExpression.isEmpty()) {
			return;
		}

		if (!actualColumnList.isEmpty() && actualColumnList.getLast().equals(actualSelectExpression.getLast())) {
			idInSelectList(n);
		}
	}

	@Override
	public void visitEdge_SelectExpression_ColumnList(SelectExpression node, Expression end) {
		actualColumnList.addLast(node);
	}

	@Override
	public void visitEdgeEnd_SelectExpression_ColumnList(SelectExpression node, Expression end) {
		actualColumnList.removeLast();
	}

	@Override
	public void visitEdge_SelectExpression_GroupBy(SelectExpression node, ExpressionList end) {
		actualGroupBy.addLast(node);
	}

	@Override
	public void visitEdgeEnd_SelectExpression_GroupBy(SelectExpression node, ExpressionList end) {
		actualGroupBy.removeLast();
	}

	@Override
	public void visitEdge_FunctionCall_ParamList(FunctionCall node, FunctionParams end) {
		if (isSetFunctionCall(node.getExpression())) {
			inSetFunctionCall = true;
		}
	}

	@Override
	public void visitEdgeEnd_FunctionCall_ParamList(FunctionCall node, FunctionParams end) {
		inSetFunctionCall = false;
	}

	private void idInSelectList(Id id) {
		// if we are in a setfunctioncall ignore this
		if (inSetFunctionCall) {
			return;
		}

		final Set<Column> columnsInGroupBy = groupByColumns.get(actualColumnList.getLast());
		final Set<String> unresolved = groupByColumnsUnResolved.get(actualColumnList.getLast());

		final Alias alias = hasAlias(id);

		final Base ref = id.getRefersTo();
		if (ref instanceof Column column) {
			boolean notInGroupBy = columnsInGroupBy != null && !columnsInGroupBy.contains(ref);
			boolean notInUnresolved = id.getName() != null && unresolved != null && !unresolved.contains(id.getName());
			boolean aliasNotInGroupBy = alias != null && unresolved != null && !unresolved.contains(alias.getName());
			if (notInGroupBy && notInUnresolved && (alias == null || aliasNotInGroupBy)) {
				String message = String.format(MESSAGE, id.getName(), ASGUtils.getColumnFullName(column));

				smells.add(new SQLSmell(id, SQLSmellKind.AMBIGUOUS_GROUPS, message, SQLSmellCertKind.NORMAL_CERTAINTY));
			}
		}
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}

	private Alias hasAlias(Id id) {
		Base parent = id.getParent();
		while (parent instanceof Expression) {
			if (parent instanceof Alias alias) {
				return alias;
			}
			parent = parent.getParent();
		}
		return null;
	}

	private boolean isSetFunctionCall(Expression callExpression) {
		if (callExpression instanceof Id id) {
			return isSetFunction(id);
		}
		return false;
	}

	private boolean isSetFunction(Id functionId) {
		if (dialect == SQLDialect.IMPALA) {
			return Arrays.asList(IMPALA_SET_FUNCTIONS).contains(functionId.getName().toUpperCase(Locale.ENGLISH));
		} else if (dialect == SQLDialect.MYSQL) {
			return Arrays.asList(MYSQL_SET_FUNCTIONS).contains(functionId.getName().toUpperCase(Locale.ENGLISH));
		} else if (dialect == SQLDialect.SQLITE) {
			return Arrays.asList(SQLITE_AGGREGATE_FUNCTIONS).contains(functionId.getName().toUpperCase(Locale.ENGLISH));
		} else {
			return false;
		}
	}
}
