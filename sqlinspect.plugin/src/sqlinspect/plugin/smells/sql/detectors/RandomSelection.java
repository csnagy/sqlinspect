package sqlinspect.plugin.smells.sql.detectors;

import java.util.HashSet;
import java.util.Set;

import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellCertKind;
import sqlinspect.plugin.smells.sql.common.SQLSmellKind;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.FunctionCall;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.InnerFunctionCall;
import sqlinspect.sql.asg.expr.Query;
import sqlinspect.sql.asg.expr.UserFunctionCall;
import sqlinspect.sql.asg.visitor.Visitor;

/*
 * "The most common SQL trick to pick a random row from a query is to
 * sort the query randomly and pick the first row. This technique is easy
 * to understand and easy to implement:
 *
 *    SELECT * FROM Bugs ORDER BY RAND() LIMIT 1;
 *
 * Although this is a popular solution, it quickly shows its weakness."
 *
 * Implementation:
 *  - if we find rand() in the order by clause, it triggers a warning
 */
public class RandomSelection extends Visitor {
	private final SQLDialect dialect;
	private final Set<SQLSmell> smells = new HashSet<>();

	private static final String MYSQL_RAND = "RAND";
	private static final String IMPALA_RAND = "RAND";
	private static final String SQLITE_RAND = "RANDOM";
	private static final String MESSAGE = "Avoid sorting data randomly to select a random record.";

	private boolean inOrderBy;

	public RandomSelection(SQLDialect dialect) {
		super();
		this.dialect = dialect;
	}

	private void visit(FunctionCall n) {
		if (inOrderBy) {
			Expression func = n.getExpression();
			if (func instanceof Id id && isRandomFunction(id.getName())) {
				smells.add(new SQLSmell(n, SQLSmellKind.RANDOM_SELECTION, MESSAGE, SQLSmellCertKind.NORMAL_CERTAINTY));
			}
		}
	}

	@Override
	public void visit(InnerFunctionCall n) {
		visit((FunctionCall) n);
	}

	@Override
	public void visit(UserFunctionCall n) {
		visit((FunctionCall) n);
	}

	@Override
	public void visitEdge_Query_OrderBy(Query node, ExpressionList end) {
		inOrderBy = true;
	}

	@Override
	public void visitEdgeEnd_Query_OrderBy(Query node, ExpressionList end) {
		inOrderBy = false;
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}

	private boolean isRandomFunction(String functionName) {
		if (dialect == SQLDialect.IMPALA) {
			return IMPALA_RAND.equalsIgnoreCase(functionName);
		} else if (dialect == SQLDialect.MYSQL) {
			return MYSQL_RAND.equalsIgnoreCase(functionName);
		} else if (dialect == SQLDialect.SQLITE) {
			return SQLITE_RAND.equalsIgnoreCase(functionName);
		} else {
			return false;
		}
	}
}
