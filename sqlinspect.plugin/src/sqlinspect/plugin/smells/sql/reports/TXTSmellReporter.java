package sqlinspect.plugin.smells.sql.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.smells.sql.common.SQLSmell;

public class TXTSmellReporter implements ISmellReporter {
	private static final Logger LOG = LoggerFactory.getLogger(TXTSmellReporter.class);

	@Override
	public void writeReport(File file, Collection<SQLSmell> smells) {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(file.toPath()),
				StandardCharsets.UTF_8); BufferedWriter bw = new BufferedWriter(ow);) {

			for (SQLSmell sqlSmell : smells) {
				writeSmell(bw, sqlSmell);
			}
		} catch (IOException e) {
			LOG.error("IO error while writing report!", e);
		}
	}

	public void writeSmell(BufferedWriter bw, SQLSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(smell.getNode().getPath()).append('(').append(smell.getNode().getStartLine()).append("): ")
				.append(smell.getKind().toString()).append('(').append(smell.getCertainty().toString()).append("): ")
				.append(smell.getMessage()).append('\n');

		bw.write(sb.toString());
	}

}
