package sqlinspect.plugin.smells.sql.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.smells.sql.common.SQLSmell;

public class StreamSmellReporter implements ISmellReporter {
	private static final Logger LOG = LoggerFactory.getLogger(StreamSmellReporter.class);

	@Override
	public void writeReport(File file, Collection<SQLSmell> smells) throws FileNotFoundException {
		writeReport(new FileOutputStream(file), smells);
	}

	public void writeReport(OutputStream stream, Collection<SQLSmell> smells) {
		try {
			for (SQLSmell sqlSmell : smells) {
				writeSmell(stream, sqlSmell);
			}
		} catch (IOException e) {
			LOG.error("IO error while writing to stream", e);
		}

	}

	public void writeSmell(OutputStream stream, SQLSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(smell.getNode().getPath()).append('(').append(smell.getNode().getStartLine()).append("): ")
				.append(smell.getKind().toString()).append('(').append(smell.getCertainty().toString()).append("): ")
				.append(smell.getMessage()).append('\n');

		stream.write(sb.toString().getBytes(StandardCharsets.UTF_8));
	}

}
