package sqlinspect.plugin.smells.sql.reports;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import sqlinspect.plugin.smells.sql.common.SQLSmell;

public interface ISmellReporter {
	static final String XML = "xml";
	static final String JSON = "json";
	static final String TXT = "txt";

	public void writeReport(File file, Collection<SQLSmell> smells) throws IOException;

	public static ISmellReporter create(String exportFormat) {
		if (XML.equalsIgnoreCase(exportFormat)) {
			return new XMLSmellReporter();
		} else if (JSON.equalsIgnoreCase(exportFormat)) {
			return new JSONSmellReporter();
		} else {
			return new TXTSmellReporter();
		}

	}
}
