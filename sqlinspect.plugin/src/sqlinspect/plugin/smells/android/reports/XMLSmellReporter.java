package sqlinspect.plugin.smells.android.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collection;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.utils.ASTUtils;

public class XMLSmellReporter implements ISmellReporter {
	private static final Logger LOG = LoggerFactory.getLogger(XMLSmellReporter.class);

	private static final String ROOT_NODE = "Smells";
	private static final String SMELL_NODE = "Smell";
	private static final String SMELL_NODE_KIND = "Kind";
	private static final String SMELL_NODE_FILE = "File";
	private static final String SMELL_NODE_LINE = "Line";
	private static final String SMELL_NODE_CERTAINTY = "Certainty";
	private static final String SMELL_NODE_MESSAGE = "Message";
	private static final String TAB = "  ";

	private XMLStreamWriter writer;

	@Override
	public void writeReport(File file, Collection<AndroidSmell> smells) {
		LOG.debug("Dump smells to file: {}", file.getAbsolutePath());

		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

		try (OutputStream ostream = Files.newOutputStream(file.toPath())) {
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			for (AndroidSmell smell : smells) {
				writeSmellNode(smell);
			}

			writer.writeEndElement();
			nl();
			writer.writeEndDocument();
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			LOG.error("Could not write XML.", e);
		} catch (IOException e) {
			LOG.error("IO error.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					LOG.error("Could not clean XMLStreamWriter!", e);
				}
			}
		}
		LOG.debug("Dump smells done.");
	}

	private void writeSmellNode(AndroidSmell smell) throws XMLStreamException {
		indent(1);
		writer.writeStartElement(SMELL_NODE);
		nl();

		indent(2);
		writer.writeStartElement(SMELL_NODE_KIND);
		writer.writeCharacters(smell.getKind().toString());
		writer.writeEndElement();
		nl();

		indent(2);
		writer.writeStartElement(SMELL_NODE_FILE);
		String lpath = ASTUtils.getPath(smell.getNode()).toString();
		writer.writeCharacters(lpath);
		writer.writeEndElement();
		nl();

		indent(2);
		writer.writeStartElement(SMELL_NODE_LINE);
		int lineNo = ASTUtils.getLineNumber(smell.getNode());
		writer.writeCharacters(Integer.toString(lineNo));
		writer.writeEndElement();
		nl();

		indent(2);
		writer.writeStartElement(SMELL_NODE_CERTAINTY);
		writer.writeCharacters(smell.getCertainty().toString());
		writer.writeEndElement();
		nl();

		indent(2);
		writer.writeStartElement(SMELL_NODE_MESSAGE);
		writer.writeCharacters(smell.getMessage());
		writer.writeEndElement();
		nl();

		indent(1);
		writer.writeEndElement();
		nl();
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters("\n");
	}

	private void indent(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TAB);
		}
	}
}
