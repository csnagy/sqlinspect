package sqlinspect.plugin.smells.android.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.utils.ASTUtils;

public class TXTSmellReporter implements ISmellReporter {
	private static final Logger LOG = LoggerFactory.getLogger(TXTSmellReporter.class);

	@Override
	public void writeReport(File file, Collection<AndroidSmell> smells) {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(file.toPath()),
				StandardCharsets.UTF_8); BufferedWriter bw = new BufferedWriter(ow)) {

			for (AndroidSmell smell : smells) {
				writeSmell(bw, smell);
			}

		} catch (IOException e) {
			LOG.error("IO error while writing report!", e);
		}
	}

	public void writeSmell(BufferedWriter bw, AndroidSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		String lpath = ASTUtils.getPath(smell.getNode()).toString();
		int lineNo = ASTUtils.getLineNumber(smell.getNode());
		sb.append(lpath).append('(').append(lineNo).append("): ").append(smell.getKind().toString()).append('(')
				.append(smell.getCertainty().toString()).append("): ").append(smell.getMessage()).append('\n');

		bw.write(sb.toString());
	}
}
