package sqlinspect.plugin.smells.android.reports;

import java.io.File;
import java.util.Collection;

import sqlinspect.plugin.smells.android.common.AndroidSmell;

public interface ISmellReporter {
	static final String JSON = "json";

	void writeReport(File file, Collection<AndroidSmell> smells);

	public static ISmellReporter create(String format) {
		if (JSON.equals(format)) {
			return new JSONSmellReporter();
		} else {
			return new XMLSmellReporter();
		}
	}
}
