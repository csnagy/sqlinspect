package sqlinspect.plugin.smells.android.reports;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.utils.ASTUtils;

public class JSONSmellReporter implements ISmellReporter {
	private static final Logger LOG = LoggerFactory.getLogger(JSONSmellReporter.class);

	private static final String SMELL_NODE_KIND = "Kind";
	private static final String SMELL_NODE_FILE = "File";
	private static final String SMELL_NODE_LINE = "Line";
	private static final String SMELL_NODE_CERTAINTY = "Certainty";
	private static final String SMELL_NODE_MESSAGE = "Message";

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public void writeReport(File file, Collection<AndroidSmell> smells) {
		LOG.debug("Dump smells to file: {}", file.getAbsolutePath());

		ObjectNode rootNode = mapper.createObjectNode();
		ArrayNode smellsArr = mapper.createArrayNode();

		for (AndroidSmell smell : smells) {
			smellsArr.add(writeSmellNode(smell));
		}
		rootNode.set("Smells", smellsArr);

		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(file, rootNode);
		} catch (IOException e) {
			LOG.error("Exception: ", e);
		}

		LOG.debug("Dump smells done.");
	}

	private ObjectNode writeSmellNode(AndroidSmell smell) {
		ObjectNode node = mapper.createObjectNode();

		node.put(SMELL_NODE_KIND, smell.getKind().toString());
		node.put(SMELL_NODE_FILE, ASTUtils.getPath(smell.getNode()).toString());
		node.put(SMELL_NODE_LINE, ASTUtils.getLineNumber(smell.getNode()));
		node.put(SMELL_NODE_CERTAINTY, smell.getCertainty().toString());
		node.put(SMELL_NODE_MESSAGE, smell.getMessage());

		return node;
	}
}
