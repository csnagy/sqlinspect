package sqlinspect.plugin.smells.android.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.utils.ASTUtils;

public class StreamSmellReporter implements ISmellReporter {
	private static final Logger LOG = LoggerFactory.getLogger(StreamSmellReporter.class);

	@Override
	public void writeReport(File file, Collection<AndroidSmell> smells) {
		try {
			writeReport(new FileOutputStream(file), smells);
		} catch (FileNotFoundException e) {
			LOG.error("Error opening file", e);
		}
	}

	public void writeReport(OutputStream stream, Collection<AndroidSmell> smells) {
		try {
			for (AndroidSmell smell : smells) {
				writeSmell(stream, smell);
			}
		} catch (IOException e) {
			LOG.error("IO error while writing to stream", e);
		}

	}

	public void writeSmell(OutputStream stream, AndroidSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		String path = ASTUtils.getPath(smell.getNode()).toString();
		int lineNo = ASTUtils.getLineNumber(smell.getNode());
		sb.append(path).append('(').append(lineNo).append("): ").append(smell.getKind().toString()).append('(')
				.append(smell.getCertainty().toString()).append("): ").append(smell.getMessage()).append('\n');

		stream.write(sb.toString().getBytes(StandardCharsets.UTF_8));
	}

}
