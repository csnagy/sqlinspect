package sqlinspect.plugin.smells.android.detectors;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.smells.android.common.AndroidSmellCertKind;
import sqlinspect.plugin.smells.android.common.AndroidSmellDetector;
import sqlinspect.plugin.smells.android.common.AndroidSmellKind;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.asg.statm.Insert;
import sqlinspect.sql.asg.statm.ParserError;
import sqlinspect.sql.asg.statm.Statement;

public class ExecuteInsertShouldSendInsert implements AndroidSmellDetector {
	private static final Logger LOG = LoggerFactory.getLogger(ExecuteInsertShouldSendInsert.class);
	private final Set<AndroidSmell> smells = new HashSet<>();
	private static final String MESSAGE = "executeInsert() should send an INSERT to be a useful call.";
	
	@Inject
	private HotspotRepository hotspotRepository;

	private boolean isExecuteHotspot(Hotspot hs) {
		String methodName = ASTUtils.getMethodName(hs.getExec());
		return "executeInsert".equalsIgnoreCase(methodName);
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {
		LOG.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : hotspotRepository.getHotspots(project)) {
			if (isExecuteHotspot(hs)) {
				for (Query q : hs.getQueries()) {
					for (Statement stmt : q.getStatements()) {
						if (stmt != null && !(stmt instanceof Insert) && !(stmt instanceof ParserError)) {
							smells.add(
									new AndroidSmell(hs.getExec(), AndroidSmellKind.EXECUTE_INSERT_SHOULD_SEND_INSERT,
											MESSAGE, AndroidSmellCertKind.HIGH_CERTAINTY));
						}
					}
				}
			}
		}

		return smells;
	}
}
