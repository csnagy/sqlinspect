package sqlinspect.plugin.smells.android.detectors;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.smells.android.common.AndroidSmellCertKind;
import sqlinspect.plugin.smells.android.common.AndroidSmellDetector;
import sqlinspect.plugin.smells.android.common.AndroidSmellKind;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.asg.expr.BinaryQuery;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.expr.Values;
import sqlinspect.sql.asg.statm.Select;
import sqlinspect.sql.asg.statm.Statement;

public class MoreThanOneColumnInSQLiteStatement implements AndroidSmellDetector {
	private static final Logger LOG = LoggerFactory.getLogger(MoreThanOneColumnInSQLiteStatement.class);
	private final Set<AndroidSmell> smells = new HashSet<>();
	private static final String MESSAGE = "An SQL statement in SQLIteStatement cannot return multiple rows or columns, but single value (1 x 1) result sets are supported.";
	
	@Inject
	private HotspotRepository hotspotRepository;

	private boolean isSQLiteStatementCall(Hotspot hs) {
		String qualifiedName = ASTUtils.getMethodQualifiedName(hs.getExec());
		return qualifiedName != null && qualifiedName.startsWith("android.database.sqlite.SQLiteStatement");
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {
		LOG.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : hotspotRepository.getHotspots(project)) {
			if (isSQLiteStatementCall(hs)) {
				for (Query q : hs.getQueries()) {
					for (Statement stmt : q.getStatements()) {
						int count = countStatementColumns(stmt);
						if (count > 1) {
							smells.add(new AndroidSmell(hs.getExec(),
									AndroidSmellKind.MORE_THAN_ONE_COLUMN_IN_SQLITESTATEMENT, MESSAGE,
									AndroidSmellCertKind.HIGH_CERTAINTY));
						}
					}
				}
			}
		}

		return smells;
	}

	private int countColumns(SelectExpression selectExpression) {
		Expression columnList = selectExpression.getColumnList();
		if (columnList instanceof ExpressionList expressionList) {
			return expressionList.getExpressions().size();
		}
		return 0;
	}

	private int countColumns(Values val) {
		Expression columnList = val.getList();
		if (columnList instanceof ExpressionList expressionList) {
			return expressionList.getExpressions().size();
		}
		return 0;
	}

	private int countColumns(sqlinspect.sql.asg.expr.Query query) {
		if (query instanceof SelectExpression selectExpression) {
			return countColumns(selectExpression);
		} else if (query instanceof Values values) {
			return countColumns(values);
		} else if (query instanceof BinaryQuery binaryQuery) {
			if (binaryQuery.getLeft() != null) {
				return countColumns(binaryQuery.getLeft());
			} else if (binaryQuery.getRight() != null) {
				return countColumns(binaryQuery.getRight());
			}
		}
		return 0;
	}

	private int countStatementColumns(Statement stmt) {
		if (stmt instanceof Select select) {
			return countColumns(select.getQuery());
		}
		return 0;
	}
}
