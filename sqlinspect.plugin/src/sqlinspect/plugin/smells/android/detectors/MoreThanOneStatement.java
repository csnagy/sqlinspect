package sqlinspect.plugin.smells.android.detectors;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.smells.android.common.AndroidSmellCertKind;
import sqlinspect.plugin.smells.android.common.AndroidSmellDetector;
import sqlinspect.plugin.smells.android.common.AndroidSmellKind;
import sqlinspect.plugin.utils.ASTUtils;

public class MoreThanOneStatement implements AndroidSmellDetector {
	private static final Logger LOG = LoggerFactory.getLogger(MoreThanOneStatement.class);
	private final Set<AndroidSmell> smells = new HashSet<>();
	private static final String MESSAGE = "%s() cannot execute more than one SQL statement.";
	
	@Inject
	private HotspotRepository hotspotRepository;

	private boolean isExecuteOrRawHotspot(Hotspot hs) {
		String methodName = ASTUtils.getMethodName(hs.getExec());
		return "execSQL".equalsIgnoreCase(methodName) || "rawQuery".equalsIgnoreCase(methodName)
				|| "execute".equalsIgnoreCase(methodName);
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {

		LOG.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : hotspotRepository.getHotspots(project)) {
			if (isExecuteOrRawHotspot(hs)) {
				for (Query q : hs.getQueries()) {
					int count = q.getStatements().size();
					if (count > 1) {
						String message = String.format(MESSAGE, ASTUtils.getMethodName(hs.getExec()));

						smells.add(new AndroidSmell(hs.getExec(), AndroidSmellKind.MORE_THAN_ONE_STATEMENT, message,
								AndroidSmellCertKind.HIGH_CERTAINTY));
					}
				}
			}
		}

		return smells;
	}
}
