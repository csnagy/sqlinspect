package sqlinspect.plugin.smells.android.common;

public enum AndroidSmellCertKind {
	HIGH_CERTAINTY, NORMAL_CERTAINTY, LOW_CERTAINTY
}
