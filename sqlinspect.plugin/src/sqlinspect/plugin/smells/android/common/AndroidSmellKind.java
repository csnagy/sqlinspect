package sqlinspect.plugin.smells.android.common;

public enum AndroidSmellKind {
	MORE_THAN_ONE_STATEMENT, EXEC_SQL_RETURNS_DATA, EXECUTE_INSERT_SHOULD_SEND_INSERT,
	MORE_THAN_ONE_COLUMN_IN_SQLITESTATEMENT
}
