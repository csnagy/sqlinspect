package sqlinspect.plugin.smells.android.common;

import java.util.Objects;

import org.eclipse.jdt.core.dom.ASTNode;

public class AndroidSmell {
	private ASTNode node;
	private AndroidSmellKind kind;
	private String message;
	private AndroidSmellCertKind certainty;
	private String file;
	private int line;

	public AndroidSmell(ASTNode node, AndroidSmellKind kind, String message, AndroidSmellCertKind certainty) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = "";
		this.line = 0;
	}

	public AndroidSmell(ASTNode node, AndroidSmellKind kind, String message, AndroidSmellCertKind certainty,
			String file, int line) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = file;
		this.line = line;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof AndroidSmell androidSmell) {
			return Objects.equals(androidSmell.getNode(), node) && Objects.equals(androidSmell.getKind(), kind)
					&& Objects.equals(androidSmell.getFile(), file) && Objects.equals(androidSmell.getLine(), line)
					&& Objects.equals(androidSmell.getMessage(), message)
					&& Objects.equals(androidSmell.getCertainty(), certainty);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(node, file, line, kind, certainty);
	}

	public ASTNode getNode() {
		return node;
	}

	public void setNode(ASTNode node) {
		this.node = node;
	}

	public AndroidSmellKind getKind() {
		return kind;
	}

	public void setKind(AndroidSmellKind kind) {
		this.kind = kind;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AndroidSmellCertKind getCertainty() {
		return certainty;
	}

	public void setCertainty(AndroidSmellCertKind certainty) {
		this.certainty = certainty;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	@Override
	public String toString() {
		return "AndroidSmell [node=" + node + ", kind=" + kind + " certainty=" + certainty + " message=" + message
				+ "file: " + file + "line: " + line + "]";
	}
}
