package sqlinspect.plugin.smells.android.common;

import java.util.Set;

import sqlinspect.plugin.model.Project;

public interface AndroidSmellDetector {
	Set<AndroidSmell> execute(Project project);
}
