package sqlinspect.plugin.model;

import java.util.List;
import java.util.regex.Pattern;

import sqlinspect.sql.asg.statm.Statement;

public class Query {

	private Hotspot hotspot;
	private String value;
	private final QueryPart root;
	private List<Statement> statements;

	private static final String QUESTION_PLACEHOLDER = "{{question$1}}";
	private static final Pattern QUESTION_PATTERN = Pattern.compile("\\?([0-9]*)");

	public Query(Hotspot hotspot, QueryPart root) {
		this.hotspot = hotspot;
		this.root = root;
	}

	public Query(QueryPart root) {
		this.root = root;
	}

	public void setHotspot(Hotspot hotspot) {
		this.hotspot = hotspot;
	}

	public Hotspot getHotspot() {
		return hotspot;
	}

	public QueryPart getRoot() {
		return root;
	}

	public String replaceSpecialChars(String s) {
		return QUESTION_PATTERN.matcher(s).replaceAll(QUESTION_PLACEHOLDER);
	}

	public String getValue() {
		if (value == null) {
			value = replaceSpecialChars(root.getValue());
		}
		return value;
	}

	public List<Statement> getStatements() {
		return statements;
	}

	public void setStatements(List<Statement> statements) {
		this.statements = statements;
	}
}
