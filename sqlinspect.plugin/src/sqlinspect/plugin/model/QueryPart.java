package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

public abstract class QueryPart {
	/**
	 * The AST node of the QueryPart
	 */
	private final ASTNode node;

	private int id;

	private QueryPart parent;

	protected QueryPart(ASTNode node) {
		this.node = node;
	}

	public ASTNode getNode() {
		return node;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public abstract String getValue();

	public QueryPart getParent() {
		return parent;
	}

	public void setParent(QueryPart parent) {
		this.parent = parent;
	}

	public abstract List<QueryPart> getChildren();

	public List<QueryPart> getSubTree() {
		List<QueryPart> ret = new ArrayList<>();
		ret.add(this);
		for (QueryPart child : getChildren()) {
			ret.addAll(child.getSubTree());
		}
		return ret;
	}

	public String getShortDesc(int maxLength, boolean adddots) {
		String longDesc = node.toString();
		if (longDesc.length() > maxLength) {
			if (adddots) {
				return longDesc.substring(0, maxLength - 2) + "..";
			} else {
				return longDesc.substring(0, maxLength);
			}
		} else {
			return longDesc;
		}
	}

	public String getShortDesc(int maxLength) {
		return getShortDesc(maxLength, false);
	}

}
