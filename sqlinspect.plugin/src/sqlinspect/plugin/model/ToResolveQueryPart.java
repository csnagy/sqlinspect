package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.SimpleName;

/**
 * For unresolvable expressions
 *
 */
public class ToResolveQueryPart extends QueryPart {
	private static final String UNKNOWN = "{{na}}";

	public ToResolveQueryPart(SimpleName node) {
		super(node);
	}

	@Override
	public String getValue() {
		return UNKNOWN;
	}

	@Override
	public List<QueryPart> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ToResolveQueryPart[").append(getShortDesc(10)).append(']');
		return sb.toString();
	}
}
