package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

/**
 * For unresolvable expressions
 *
 */
public class UnresolvedQueryPart extends QueryPart {
	public static final String UNKNOWN = "{{na}}";

	public UnresolvedQueryPart(ASTNode node) {
		super(node);
	}

	@Override
	public String getValue() {
		return UNKNOWN;
	}

	@Override
	public List<QueryPart> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Unresolved[").append(getShortDesc(10)).append(']');
		return sb.toString();
	}
}
