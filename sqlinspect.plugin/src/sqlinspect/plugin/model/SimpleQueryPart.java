package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

// For simple, resolvable expressions
public class SimpleQueryPart extends QueryPart {

	private final String value;

	public SimpleQueryPart(ASTNode node, String value) {
		super(node);
		this.value = value.intern(); // use the intern string value here to save memory
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public List<QueryPart> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Simple[").append(getShortDesc(10)).append(':').append(getValue()).append(']');
		return sb.toString();
	}

}
