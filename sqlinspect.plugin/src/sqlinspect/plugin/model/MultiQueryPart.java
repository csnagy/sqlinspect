package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

// For conditionals, loops
public class MultiQueryPart extends QueryPart {

	private final List<QueryPart> parts = new ArrayList<>();

	private String value;

	public MultiQueryPart(ASTNode node, QueryPart... parts) {
		super(node);
		this.parts.addAll(Arrays.asList(parts));
		for (QueryPart part : parts) {
			part.setParent(this);
		}
	}

	public MultiQueryPart(ASTNode node, List<QueryPart> parts) {
		super(node);
		this.parts.addAll(parts);
	}

	@Override
	public String getValue() {
		if (value == null) {
			StringBuilder sb = new StringBuilder("{{(}}");
			boolean first = true;
			for (QueryPart part : parts) {
				if (!first) {
					sb.append("{{,}}");
				}
				if (part == null) {
					sb.append("{{EMPTY}}");
				} else {
					sb.append(part.getValue());
				}
				first = false;
			}
			sb.append("{{)}}");
			value = sb.toString();
		}
		return value;
	}

	@Override
	public List<QueryPart> getChildren() {
		return parts;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Multi[").append(getShortDesc(10)).append(':');
		boolean first = true;
		for (QueryPart qp : parts) {
			if (!first) {
				sb.append(", ");
			}
			if (qp != null) {
				sb.append(qp.toString());
			} else {
				sb.append("NULL");
			}
			first = false;
		}
		sb.append(']');
		return sb.toString();
	}

}
