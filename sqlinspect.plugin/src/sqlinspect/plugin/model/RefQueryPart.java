package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

// For variable references
public class RefQueryPart extends QueryPart {
	private final QueryPart refpart;

	public RefQueryPart(ASTNode node, QueryPart refpart) {
		super(node);
		this.refpart = refpart;
		refpart.setParent(this);
	}

	public QueryPart getRefPart() {
		return refpart;
	}

	@Override
	public String getValue() {
		return refpart.getValue();
	}

	@Override
	public List<QueryPart> getChildren() {
		List<QueryPart> ret = new ArrayList<>();
		ret.add(refpart);
		return ret;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Ref[").append(getShortDesc(10)).append(':').append(refpart == null ? "NULL" : refpart.toString()).append(']');
		return sb.toString();
	}

}
