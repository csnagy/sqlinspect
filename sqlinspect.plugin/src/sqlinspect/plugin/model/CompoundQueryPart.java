package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;

// For string operators
public class CompoundQueryPart extends QueryPart {

	private final List<QueryPart> parts = new ArrayList<>();

	private String value;

	public CompoundQueryPart(ASTNode node, QueryPart... parts) {
		super(node);
		this.parts.addAll(Arrays.asList(parts));
	}

	public CompoundQueryPart(ASTNode node, List<QueryPart> parts) {
		super(node);
		this.parts.addAll(parts);
	}

	@Override
	public String getValue() {
		if (value == null) {
			StringBuilder sb = new StringBuilder();
			for (QueryPart part : parts) {
				sb.append(part.getValue());
			}
			value = sb.toString();
		}
		return value;
	}

	@Override
	public List<QueryPart> getChildren() {
		return parts;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Compound[").append(getShortDesc(10)).append(':');
		boolean first = true;
		for (QueryPart qp : parts) {
			if (!first) {
				sb.append(", ");
			}
			if (qp != null) {
				sb.append(qp.toString());
			} else {
				sb.append("NULL");
			}
			first = false;
		}
		sb.append(']');
		return sb.toString();
	}
}
