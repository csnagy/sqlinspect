package sqlinspect.plugin.model;

import java.util.ArrayDeque;
import java.util.Deque;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import sqlinspect.plugin.utils.ASTUtils;

public class CallStack {
	public final Deque<CallStackElement> stack = new ArrayDeque<>();

	private static class PreOrderElement {
		public final QueryPart node;
		public final int depth;

		public PreOrderElement(QueryPart node, int depth) {
			this.node = node;
			this.depth = depth;
		}
	}

	public static class CallStackElement {
		public final ASTNode node;

		public CallStackElement(ASTNode node) {
			this.node = node;
		}

		public IMethodBinding getMethodDeclBinding() {
			MethodDeclaration md = ASTUtils.getParentMethodDeclaration(node);
			return md.resolveBinding();
		}

		public IPath getPath() {
			return ASTUtils.getPath(node);
		}

		public int getCallLineNumber() {
			return ASTUtils.getLineNumber(node);
		}

		public int getMethodLineNumber() {
			MethodDeclaration md = ASTUtils.getParentMethodDeclaration(node);
			return ASTUtils.getLineNumber(md);
		}

	}

	public void add(ASTNode node) {
		MethodDeclaration md = ASTUtils.getParentMethodDeclaration(node);
		if (stack.isEmpty() || (md != null && !md.equals(ASTUtils.getParentMethodDeclaration(stack.getLast().node)))) {
			stack.add(new CallStackElement(node));
		}
	}

	public static CallStack calculate(QueryPart part) {
		CallStack callstack = new CallStack();
		Deque<PreOrderElement> nexts = new ArrayDeque<>();
		nexts.add(new PreOrderElement(part, 0));
		int maxdepth = -1;
		while (!nexts.isEmpty()) {
			PreOrderElement curr = nexts.pop();
			if (curr.depth > maxdepth) {
				maxdepth = curr.depth;
				callstack.add(curr.node.getNode());
			}

			for (QueryPart child : curr.node.getChildren()) {
				nexts.add(new PreOrderElement(child, curr.depth + 1));
			}
		}
		return callstack;
	}

}
