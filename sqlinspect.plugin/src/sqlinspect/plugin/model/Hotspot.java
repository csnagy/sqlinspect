package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.extractors.IHotspotFinder;
import sqlinspect.plugin.utils.ASTUtils;

public class Hotspot {

	private static final Logger LOG = LoggerFactory.getLogger(Hotspot.class);

	/**
	 * Parent project of the hotspot
	 */
	private Project project;

	/**
	 * Compilation Unit of the hotspot
	 */
	private final CompilationUnit unit;

	/**
	 * AST node of the hotspot
	 */
	private final ASTNode exec;

	/**
	 * Path of the compilation unit where the execution point was found.
	 */
	private final IPath path;

	private final IHotspotFinder hsf;

	private final List<Query> queries = new ArrayList<>();

	public Hotspot(IHotspotFinder hsf, CompilationUnit unit, ASTNode exec, IPath path) {
		this.hsf = hsf;
		this.unit = unit;
		this.exec = exec;
		this.path = path;
	}

	public Hotspot(Project project, IHotspotFinder hsf, CompilationUnit unit, ASTNode exec, IPath path) {
		this.project = project;
		this.hsf = hsf;
		this.unit = unit;
		this.exec = exec;
		this.path = path;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exec == null) ? 0 : exec.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		result = prime * result + queries.hashCode();
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((hsf == null) ? 0 : hsf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Hotspot)) {
			return false;
		}

		Hotspot other = (Hotspot) obj;
		if (exec == null) {
			if (other.exec != null) {
				return false;
			}
		} else if (!exec.equals(other.exec)) {
			return false;
		}
		if (path == null) {
			if (other.path != null) {
				return false;
			}
		} else if (!path.equals(other.path)) {
			return false;
		}
		if (project == null) {
			if (other.project != null) {
				return false;
			}
		} else if (!project.equals(other.project)) {
			return false;
		}
		if (!queries.equals(other.queries)) {
			return false;
		}
		if (unit == null) {
			if (other.unit != null) {
				return false;
			}
		} else if (!unit.equals(other.unit)) {
			return false;
		}
		if (hsf == null) {
			if (other.hsf != null) {
				return false;
			}
		} else if (!hsf.equals(other.hsf)) {
			return false;
		}
		return true;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Project getProject() {
		return project;
	}

	public CompilationUnit getUnit() {
		return unit;
	}

	public ASTNode getExec() {
		return exec;
	}

	public IPath getPath() {
		return path;
	}

	public int getStartLine() {
		if (exec != null) {
			return ASTUtils.getLineNumber(exec);
		} else {
			return 0;
		}
	}

	public void addQuery(Query q) {
		if (q.getHotspot() == null) {
			queries.add(q);
			q.setHotspot(this);
			LOG.debug("Query adedd to hotspot! {}", q);
		} else {
			LOG.error("Query already has a hotspot! {}", q);
		}
	}

	public void addQueries(Collection<Query> queries) {
		if (queries != null) {
			for (Query q : queries) {
				addQuery(q);
			}
		}
	}

	public List<Query> getQueries() {
		return queries;
	}

	public IHotspotFinder getHotspotFinder() {
		return hsf;
	}
}
