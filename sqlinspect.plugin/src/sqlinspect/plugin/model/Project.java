package sqlinspect.plugin.model;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

/**
 * Project is the highest level unit we represent for an analysis. This is
 * typically related to an eclipse IProject, unless the analysis is executed
 * from command line, when we don't have an eclipse project.
 */
public final class Project {
	private final String name;
	private final IProject iProject;

	private boolean analyzed;

	public Project(String name, IProject iproj) {
		this.name = name;
		this.iProject = iproj;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Project)) {
			return false;
		}
		Project other = (Project) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the corresponding IProject, null if not available
	 */
	public IProject getIProject() {
		return iProject;
	}

	/**
	 * @return the corresponding IJavaProject, null if not available
	 */
	public IJavaProject getIJavaProject() {
		return JavaCore.create(iProject);
	}

	public void setAnalyzed(boolean analyzed) {
		this.analyzed = analyzed;
	}

	public boolean isAnalyzed() {
		return analyzed;
	}
	
	public String toString() {
		return name;
	}
}
