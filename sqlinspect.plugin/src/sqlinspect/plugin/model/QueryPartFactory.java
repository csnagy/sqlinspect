package sqlinspect.plugin.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.SimpleName;

public class QueryPartFactory {
	private final List<QueryPart> qps = new ArrayList<>();

	public QueryPart get(int id) {
		return qps.get(id);
	}

	public int getSize() {
		return qps.size();
	}

	public CompoundQueryPart createCompoundQueryPart(ASTNode node, List<QueryPart> parts) {
		CompoundQueryPart qp = new CompoundQueryPart(node, parts);
		add(qp);
		return qp;
	}

	public CompoundQueryPart createCompoundQueryPart(ASTNode node, QueryPart... parts) {
		CompoundQueryPart qp = new CompoundQueryPart(node, parts);
		add(qp);
		return qp;
	}

	public MultiQueryPart createMultiQueryPart(ASTNode node, QueryPart... parts) {
		MultiQueryPart qp = new MultiQueryPart(node, parts);
		add(qp);
		return qp;
	}

	public SimpleQueryPart createSimpleQueryPart(ASTNode node, String value) {
		SimpleQueryPart qp = new SimpleQueryPart(node, value);
		add(qp);
		return qp;
	}

	public RefQueryPart createRefQueryPart(ASTNode node, QueryPart part) {
		RefQueryPart qp = new RefQueryPart(node, part);
		add(qp);
		return qp;
	}

	public UnresolvedQueryPart createUnresolvedQueryPart(ASTNode node) {
		UnresolvedQueryPart qp = new UnresolvedQueryPart(node);
		add(qp);
		return qp;
	}

	public ToResolveQueryPart createToResolveQueryPart(SimpleName node) {
		ToResolveQueryPart qp = new ToResolveQueryPart(node);
		add(qp);
		return qp;
	}

	public List<QueryPart> getQps() {
		return qps;
	}

	private void add(QueryPart qp) {
		int index = qps.size();
		qps.add(qp);
		qp.setId(index);
	}
}
