package sqlinspect.plugin.repository;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.utils.JSONSchema;
import sqlinspect.plugin.utils.XMLSchema;
import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.parser.SymbolTable;

@Creatable
@Singleton
public class ASGRepository {
	private static final String JSON = "json";

	private Map<Project, ASG> asgs = new HashMap<>();
	private Map<Project, SymbolTable> symbolTables = new HashMap<>();

	public void setASG(Project project, ASG asg) {
		asgs.put(project, asg);
	}

	public ASG getASG(Project project) {
		return asgs.computeIfAbsent(project, k -> new ASG());
	}

	public SymbolTable getSymbolTable(Project project) {
		return symbolTables.computeIfAbsent(project, k -> new SymbolTable());
	}

	public Project getParentProject(Base node) {
		SQLRoot root = ASGUtils.getRoot(node);
		if (root != null) {
			for (Map.Entry<Project, ASG> e : asgs.entrySet()) {
				Project project = e.getKey();
				ASG asg = e.getValue();
				if (asg.getRoot().equals(root)) {
					return project;
				}
			}
		}
		return null;
	}

	public void exportSchema(Project project, File file, String exportFormat) throws IOException {
		if (JSON.equals(exportFormat)) {
			JSONSchema schemaExport = new JSONSchema(file);
			schemaExport.writeSchema(getASG(project).getRoot());
		} else {
			XMLSchema schemaExport = new XMLSchema(file);
			schemaExport.writeSchema(getASG(project).getRoot());
		}
	}
}