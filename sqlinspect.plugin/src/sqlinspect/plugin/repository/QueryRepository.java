package sqlinspect.plugin.repository;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.model.QueryPartFactory;
import sqlinspect.sql.asg.statm.Statement;

@Creatable
@Singleton
public class QueryRepository {
	private final Map<Project, Map<Statement, Query>> queryMap = new HashMap<>();

	public Map<Statement, Query> getQueryMap(Project project) {
		return queryMap.computeIfAbsent(project, k -> new HashMap<>());
	}

	public void addQuery(Project project, Statement stmt, Query query) {
		getQueryMap(project).put(stmt, query);
	}

	public void clear(Project project) {
		queryMap.remove(project);
	}
	
	private final QueryPartFactory queryPartFactory = new QueryPartFactory();

	public QueryPartFactory getQueryPartFactory() {
		return queryPartFactory;
	}	
}
