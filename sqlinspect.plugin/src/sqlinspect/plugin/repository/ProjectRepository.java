package sqlinspect.plugin.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import org.eclipse.core.resources.IProject;
import org.eclipse.e4.core.di.annotations.Creatable;

import sqlinspect.plugin.model.Project;

@Creatable
@Singleton
public class ProjectRepository {
	private final Map<String, Project> projects = new HashMap<>();

	public Project createProject(IProject iproj) {
		String name = iproj.getName();
		Project ret = projects.get(name);
		if (ret == null) {
			ret = new Project(name, iproj);
			projects.put(iproj.getName(), ret);
		} else if (!ret.getIProject().equals(iproj)) {
			throw new IllegalArgumentException("Project already exists with the same name: " + ret.getName());
		}
		return ret;
	}

	public Project getProject(String name) {
		return projects.get(name);
	}

	public Project getProject(IProject iproj) {
		for (Map.Entry<String, Project> e : projects.entrySet()) {
			Project project = e.getValue();
			if (project.getIProject().equals(iproj)) {
				return project;
			}
		}
		return null;
	}

	public List<Project> getProjects() {
		return projects.values().stream().toList();
	}

	public List<String> getProjectNames() {
		List<String> projectNames = new ArrayList<>();
		for (Project p : getProjects()) {
			projectNames.add(p.getName());
		}
		return projectNames;
	}
}
