package sqlinspect.plugin.repository;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellComparator;

@Creatable
@Singleton
public class SmellRepository {
	private static final Logger LOG = LoggerFactory.getLogger(SmellRepository.class);

	private final Map<Project, Set<AndroidSmell>> androidSmells = new HashMap<>();
	private final Map<Project, Set<SQLSmell>> sqlSmells = new HashMap<>();

	public Set<SQLSmell> getSQLSmells(Project project) {
		return sqlSmells.computeIfAbsent(project, k -> new HashSet<>());
	}

	public Set<AndroidSmell> getAndroidSmells(Project project) {
		return androidSmells.computeIfAbsent(project, k -> new HashSet<>());
	}

	public void clear(Project project) {
		androidSmells.remove(project);
		sqlSmells.remove(project);
	}

	public void reportSQLSmells(Project project, File exportFile, String exportFormat) {
		LOG.info("Write smell report: {} ({})", exportFile, exportFormat);

		try {
			sqlinspect.plugin.smells.sql.reports.ISmellReporter report = sqlinspect.plugin.smells.sql.reports.ISmellReporter.create(exportFormat);
			Set<SQLSmell> smellsNoDuplicates = new TreeSet<>(new SQLSmellComparator());
			smellsNoDuplicates.addAll(getSQLSmells(project));
			report.writeReport(exportFile, smellsNoDuplicates);
		} catch (IOException e) {
			LOG.error("IO error while writing report: ", e);
		}
	}

	public void reportAndroidSmells(Project project, File exportFile, String exportFormat) {
		LOG.info("Write smell report: {} ({})", exportFile, exportFormat);

		sqlinspect.plugin.smells.android.reports.ISmellReporter report = sqlinspect.plugin.smells.android.reports.ISmellReporter.create(exportFormat);
		report.writeReport(exportFile, getAndroidSmells(project));
	}

}
