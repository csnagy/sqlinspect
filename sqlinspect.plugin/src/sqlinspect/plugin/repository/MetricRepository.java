package sqlinspect.plugin.repository;

import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jdt.core.IJavaElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.metrics.java.JavaMetricDesc;
import sqlinspect.plugin.metrics.java.reports.XMLJavaMetricsReporter;
import sqlinspect.plugin.metrics.sql.SQLMetricDesc;
import sqlinspect.plugin.metrics.sql.reports.IMetricsReporter;
import sqlinspect.plugin.model.Project;
import sqlinspect.sql.asg.statm.Statement;

@Singleton
@Creatable
public class MetricRepository {
	private static final Logger LOG = LoggerFactory.getLogger(MetricRepository.class);

	@Inject
	private ASGRepository asgRepository;

	private final Map<Project, Map<Statement, Map<SQLMetricDesc, Integer>>> sqlMetrics = new HashMap<>();
	private final Map<Project, Map<IJavaElement, Map<JavaMetricDesc, Integer>>> javaMetrics = new HashMap<>();

	public Map<Statement, Map<SQLMetricDesc, Integer>> getSQLMetrics(Project project) {
		return sqlMetrics.computeIfAbsent(project, k -> new HashMap<>());
	}

	public Map<IJavaElement, Map<JavaMetricDesc, Integer>> getJavaMetrics(Project project) {
		return javaMetrics.computeIfAbsent(project, k -> new HashMap<>());
	}

	public Map<JavaMetricDesc, Integer> getJavaMetricsOfElement(Project project, IJavaElement javaElement) {
		return getJavaMetrics(project).computeIfAbsent(javaElement, k -> new EnumMap<>(JavaMetricDesc.class));
	}

	public void addJavaMetrics(Project project, Map<IJavaElement, Map<JavaMetricDesc, Integer>> newMetrics) {
		for (Entry<IJavaElement, Map<JavaMetricDesc, Integer>> newMetricEntry : newMetrics.entrySet()) {
			Map<JavaMetricDesc, Integer> elemMetrics = getJavaMetricsOfElement(project, newMetricEntry.getKey());
			for (Entry<JavaMetricDesc, Integer> m : newMetricEntry.getValue().entrySet()) {
				elemMetrics.put(m.getKey(), m.getValue());
			}
		}
	}

	public void clear(Project project) {
		sqlMetrics.remove(project);
		javaMetrics.remove(project);
	}

	public void reportJavaMetrics(Project project, File reportFile, String reportFormat) {
		XMLJavaMetricsReporter report;
		if ("XML".equalsIgnoreCase(reportFormat)) {
			report = new XMLJavaMetricsReporter(reportFile, getJavaMetrics(project));
			report.writeReport(project.getIJavaProject());
		} else {
			throw new IllegalArgumentException("Invalid report format");
		}
	}

	public void reportSQLMetrics(Project project, File exportFile, String exportFormat) {
		LOG.info("Write metrics report: {} ({})", exportFile, exportFormat);

		try {
			IMetricsReporter report = IMetricsReporter.create(exportFormat);
			report.writeReport(asgRepository.getASG(project), exportFile, getSQLMetrics(project));
		} catch (IOException e) {
			LOG.error("IO error writing report: ", e);
		}
	}
}
