package sqlinspect.plugin.repository;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.sqlan.taa.ITAAReporter;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;

@Creatable
@Singleton
public class TableAccessRepository {
	private static final Logger LOG = LoggerFactory.getLogger(TableAccessRepository.class);

	@Inject
	private ASGRepository asgRepository;

	private final Map<Project, Map<Statement, Map<Column, Integer>>> columnAccesses = new HashMap<>();
	private final Map<Project, Map<Statement, Map<Table, Integer>>> tableAccesses = new HashMap<>();

	public void clear(Project project) {
		columnAccesses.remove(project);
		tableAccesses.remove(project);
	}

	public Map<Statement, Map<Table, Integer>> getTableAccesses(Project project) {
		return tableAccesses.computeIfAbsent(project, k -> new HashMap<>());
	}

	public Map<Statement, Map<Column, Integer>> getColumnAccesses(Project project) {
		return columnAccesses.computeIfAbsent(project, k -> new HashMap<>());
	}

	public void reportTAA(Project project, File reportFile, String reportFormat) {
		LOG.info("Write TAA report: {} ({})", reportFile, reportFormat);

		ITAAReporter report = ITAAReporter.create(asgRepository.getASG(project), getTableAccesses(project),
				getColumnAccesses(project), reportFormat);
		report.writeReport(reportFile);
	}
}
