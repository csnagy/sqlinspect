package sqlinspect.plugin.repository;

import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import org.eclipse.core.runtime.IPath;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.utils.ASTUtils;

@Creatable
@Singleton
public class HotspotRepository {
	private static final Logger LOG = LoggerFactory.getLogger(HotspotRepository.class);

	private final Map<Project, List<Hotspot>> hotspots = new HashMap<>();
	private final Map<Project, Map<ICompilationUnit, List<Hotspot>>> cuHotspots = new HashMap<>();

	private static final String NL = "\n";

	public void addHotspot(Project project, Hotspot hotspot) {
		if (hotspot.getProject() != null) {
			throw new IllegalArgumentException("Hotspot already has a project!");
		}
		hotspot.setProject(project);

		List<Hotspot> projectHotspots = getHotspots(project);
		projectHotspots.add(hotspot);

		ASTNode node = hotspot.getExec();
		if (node != null) {
			ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
			List<Hotspot> projectCuHotspots = getHotspots(project, cu);
			projectCuHotspots.add(hotspot);
		}
	}

	public void addHotspots(Project project, Collection<Hotspot> hotspots) {
		hotspots.stream().forEach(hs -> addHotspot(project, hs));
	}

	public List<Hotspot> getHotspots(Project project) {
		return hotspots.computeIfAbsent(project, k -> new ArrayList<Hotspot>());
	}

	public Map<ICompilationUnit, List<Hotspot>> getCuHotspotMap(Project project) {
		return cuHotspots.computeIfAbsent(project, k -> new HashMap<ICompilationUnit, List<Hotspot>>());
	}

	public List<Hotspot> getHotspots(Project project, ICompilationUnit cu) {
		return getCuHotspotMap(project).computeIfAbsent(cu, k -> new ArrayList<>());
	}

	public List<Hotspot> findHotspots(Project project, IPath path, int startLine) {
		List<Hotspot> ret = new ArrayList<>();
		for (Hotspot hs : getHotspots(project)) {
			IPath hsPath = hs.getPath();
			if (hsPath.equals(path) && hs.getStartLine() == startLine) {
				ret.add(hs);
			}
		}
		return ret;
	}

	public List<Hotspot> findHotspots(IPath path, int startLine) {
		List<Hotspot> ret = new ArrayList<>();
		for (List<Hotspot> hsLists : hotspots.values()) {
			if (hsLists != null) {
				for (Hotspot hs : hsLists) {
					IPath hsPath = hs.getPath();
					if (hsPath.equals(path) && hs.getStartLine() == startLine) {
						ret.add(hs);
					}
				}
			}
		}
		return ret;
	}

	public void clear(Project project) {
		hotspots.remove(project);
		cuHotspots.remove(project);
	}

	public List<Query> getQueries(Project project) {
		return getHotspots(project).stream().flatMap(hs -> hs.getQueries().stream()).toList();
	}

	public void dumpStats(Project project, File f) {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(f.toPath()), StandardCharsets.UTF_8);
				BufferedWriter bw = new BufferedWriter(ow);) {
			bw.write("Project,Hotspots,Queries,AVG(Queries),MAX(Queries)" + NL);
			int lhotspots = getHotspots(project).size();
			int maxQuery = 0;
			int totalQuery = 0;
			for (Hotspot hs : getHotspots(project)) {
				int qnum = hs.getQueries().size();
				if (qnum > maxQuery) {
					maxQuery = qnum;
				}
				totalQuery += qnum;
			}
			bw.write(project.getName() + "," + getHotspots(project).size() + "," + totalQuery + ","
					+ (lhotspots > 0 ? totalQuery / lhotspots : 0) + "," + maxQuery);
		} catch (IOException e) {
			LOG.error("Error while dumping statistics: ", e);
		}
	}

}
