package sqlinspect.plugin.preferences;

public final class PreferenceConstants {
	public static final String STRING_SEPERATOR = ";";

	// SQLAnalyzer
	public static final String DIALECT = "dialect";

	public static final String LOAD_SCHEMA_FROM_DB = "load_schema_from_db";
	public static final String LOAD_SCHEMA_FROM_FILE = "load_schema_from_file";
	public static final String DB_USER = "db_user";
	public static final String DB_PASSWORD = "db_pwd";
	public static final String DB_URL = "db_url";
	public static final String DB_SCHEMA = "db_schema_file";
	public static final String RUN_SMELL_DETECTORS = "run_smell_detectors";
	public static final String RUN_TABLE_ACCESS = "run_table_access";
	public static final String RUN_SQL_METRICS = "run_sql_metrics";
	public static final String DUMP_STATISTICS = "dump_statistics";

	public static final String FILTER_FILE = "filter_file";

	// SQLExtractor
	public static final String HOTSPOT_FINDERS = "hotspot_finders";
	public static final String STRING_RESOLVER = "string_resolver";

	// JDBCHotspotFinder
	public static final String JDBC_STMT_EXECUTE = "jdbc_query_hotspots";
	public static final String JDBC_PSTMT_EXECUTE = "jdbc_pstmt_hotspots";
	public static final String JDBC_PSTMT = "jdbc_pstmt";
	public static final String JDBC_PSTMT_SETTER = "jdbc_pstmt_setter";

	// AndroidHotspotFinder
	public static final String ANDR_EXECSQL = "andr_execsql";
	public static final String ANDR_STMT_EXEC = "andr_stmt_exec";
	public static final String ANDR_COMPILE_STMT = "andr_compile_stmt";
	public static final String ANDR_CURSOR_RAWQUERY = "andr_cursor_rawquery";
	public static final String ANDR_PSTMT_SETTER = "android_pstmt_setter";

	// HibernateHotspotFinder
	public static final String HIBERNATE_QUERY_CREATE = "hibernate_query_create";
	public static final String HIBERNATE_QUERY_EXEC = "hibernate_query_exec";
	public static final String HIBERNATE_PSTMT_SETTER = "hibernate_pstmt_setter";

	// JPAHotspotFinder
	public static final String JPA_QUERY_CREATE = "jpa_query_create";
	public static final String JPA_QUERY_EXEC = "jpa_query_exec";
	public static final String JPA_PSTMT_SETTER = "jpa_pstmt_setter";

	// SpringHotspotFinder
	public static final String SPRING_HIBERNATETEMPLATE_HOTSPOTS = "spring_spring_hibernatetemplate_hotspots";

	// InterQueryResolver
	public static final String INTERSR_MAX_CFG_DEPTH = "intersr_max_cfg_depth";
	public static final String INTERSR_MAX_CALL_DEPTH = "intersr_max_call_depth";
	public static final String INTERSR_LOOPHANDLING = "intersr_loophandling";

	public static final String PSTMT_RESOLVER_MAXDEPTH = "pstmt_resolver_maxdepth";

	private PreferenceConstants() {
	}
}
