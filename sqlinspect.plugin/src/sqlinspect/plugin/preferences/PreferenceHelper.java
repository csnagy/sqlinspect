package sqlinspect.plugin.preferences;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public final class PreferenceHelper {
	public static final String SEPERATOR = PreferenceConstants.STRING_SEPERATOR;

	private PreferenceHelper() {
	}

	public static List<String> parseStringList(String stringList) {
		return Collections.list(new StringTokenizer(stringList, SEPERATOR)).stream().map(String.class::cast).toList();
	}

	public static String[] parseStringArray(String stringList) {
		return Collections.list(new StringTokenizer(stringList, SEPERATOR)).stream().map(String.class::cast)
				.toArray(String[]::new);
	}

	public static String createStringList(List<String> items) {
		return items.stream().collect(Collectors.joining(SEPERATOR));
	}

	public static String createStringList(String[] items) {
		return Arrays.stream(items).collect(Collectors.joining(SEPERATOR));
	}
}
