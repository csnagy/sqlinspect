package sqlinspect.plugin.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.AndroidHotspotFinder;
import sqlinspect.plugin.extractors.HibernateHotspotFinder;
import sqlinspect.plugin.extractors.HotspotFinderFactory;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.extractors.JDBCHotspotFinder;
import sqlinspect.plugin.extractors.JPAHotspotFinder;
import sqlinspect.plugin.extractors.PreparedStatementResolver;
import sqlinspect.plugin.extractors.SpringHotspotFinder;
import sqlinspect.sql.SQLDialect;

public class PreferenceInitializer extends AbstractPreferenceInitializer {
	private static final Logger LOG = LoggerFactory.getLogger(PreferenceInitializer.class);

	@Override
	public void initializeDefaultPreferences() {
		LOG.debug("Initialize preferences");

		IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		initSQLAnPrefs(store);

		initSQLExtractorPrefs(store);
	}

	private void initSQLAnPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.DIALECT, SQLDialect.MYSQL.toString());
		store.setDefault(PreferenceConstants.LOAD_SCHEMA_FROM_DB, false);
		store.setDefault(PreferenceConstants.LOAD_SCHEMA_FROM_FILE, false);
		store.setDefault(PreferenceConstants.RUN_SMELL_DETECTORS, true);
		store.setDefault(PreferenceConstants.RUN_TABLE_ACCESS, true);
		store.setDefault(PreferenceConstants.RUN_SQL_METRICS, true);
		store.setDefault(PreferenceConstants.DUMP_STATISTICS, true);

		store.setDefault(PreferenceConstants.FILTER_FILE, "");
	}

	private void initSQLExtractorPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.HOTSPOT_FINDERS,
				HotspotFinderFactory.getHotspotFindersAsString(PreferenceConstants.STRING_SEPERATOR));
		store.setDefault(PreferenceConstants.STRING_RESOLVER, InterQueryResolver.class.getSimpleName());

		store.setDefault(PreferenceConstants.PSTMT_RESOLVER_MAXDEPTH, PreparedStatementResolver.DEFAULT_MAX_DEPTH);

		initJDBCHotspotFinderPrefs(store);
		initAndroidHotspotFinderPrefs(store);
		initHibernateHotspotFinderPrefs(store);
		initJPAHotspotFinderPrefs(store);
		initSpringHotspotFinderPrefs(store);

		initInterQueryResolverPrefs(store);
	}

	private void initJDBCHotspotFinderPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.JDBC_STMT_EXECUTE,
				PreferenceHelper.createStringList(JDBCHotspotFinder.DEFAULT_JDBC_STMT_EXECUTE));
		store.setDefault(PreferenceConstants.JDBC_PSTMT_EXECUTE,
				PreferenceHelper.createStringList(JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_EXECUTE));
		store.setDefault(PreferenceConstants.JDBC_PSTMT,
				PreferenceHelper.createStringList(JDBCHotspotFinder.DEFAULT_JDBC_PSTMT));
		store.setDefault(PreferenceConstants.JDBC_PSTMT_SETTER,
				PreferenceHelper.createStringList(JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_SETTER));
	}

	private void initAndroidHotspotFinderPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.ANDR_EXECSQL,
				PreferenceHelper.createStringList(AndroidHotspotFinder.DEFAULT_ANDR_EXECSQL));
		store.setDefault(PreferenceConstants.ANDR_STMT_EXEC,
				PreferenceHelper.createStringList(AndroidHotspotFinder.DEFAULT_ANDR_STMT_EXEC));
		store.setDefault(PreferenceConstants.ANDR_COMPILE_STMT,
				PreferenceHelper.createStringList(AndroidHotspotFinder.DEFAULT_ANDR_COMPILE_STMT));
		store.setDefault(PreferenceConstants.ANDR_CURSOR_RAWQUERY,
				PreferenceHelper.createStringList(AndroidHotspotFinder.DEFAULT_ANDR_CURSOR_RAWQERY));
		store.setDefault(PreferenceConstants.ANDR_PSTMT_SETTER,
				PreferenceHelper.createStringList(AndroidHotspotFinder.DEFAULT_ANDR_PSTMT_SETTER));
	}

	private void initHibernateHotspotFinderPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.HIBERNATE_QUERY_CREATE,
				PreferenceHelper.createStringList(HibernateHotspotFinder.DEFAULT_HIBERNATE_QUERY_CREATE));
		store.setDefault(PreferenceConstants.HIBERNATE_QUERY_EXEC,
				PreferenceHelper.createStringList(HibernateHotspotFinder.DEFAULT_HIBERNATE_QUERY_EXEC));
		store.setDefault(PreferenceConstants.HIBERNATE_PSTMT_SETTER,
				PreferenceHelper.createStringList(HibernateHotspotFinder.DEFAULT_HIBERNATE_PSTMT_SETTER));
	}

	private void initJPAHotspotFinderPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.JPA_QUERY_CREATE,
				PreferenceHelper.createStringList(JPAHotspotFinder.DEFAULT_JPA_QUERY_CREATE));
		store.setDefault(PreferenceConstants.JPA_QUERY_EXEC,
				PreferenceHelper.createStringList(JPAHotspotFinder.DEFAULT_JPA_QUERY_EXEC));
		store.setDefault(PreferenceConstants.JPA_PSTMT_SETTER,
				PreferenceHelper.createStringList(JPAHotspotFinder.DEFAULT_JPA_PSTMT_SETTER));
	}

	private void initSpringHotspotFinderPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.SPRING_HIBERNATETEMPLATE_HOTSPOTS,
				PreferenceHelper.createStringList(SpringHotspotFinder.getDefaultSpringHibernateTemplateHotspots()));
	}

	private void initInterQueryResolverPrefs(IPreferenceStore store) {
		store.setDefault(PreferenceConstants.INTERSR_MAX_CFG_DEPTH, InterQueryResolver.DEFAULT_MAX_CFG_DEPTH);
		store.setDefault(PreferenceConstants.INTERSR_MAX_CALL_DEPTH, InterQueryResolver.DEFAULT_MAX_CALL_DEPTH);
		store.setDefault(PreferenceConstants.INTERSR_LOOPHANDLING, InterQueryResolver.DEFAULT_LOOP_HANDLING.toString());
	}
}
