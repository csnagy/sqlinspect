package sqlinspect.plugin.extractors;

import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;

public class CalleeVisitor extends ASTVisitor {

	// we put it in the sorted map for testing purposes - the performance dropback
	// is negligible otherwise results are always random
	private final SortedMap<Integer, MethodInvocation> callees = new TreeMap<>();

	private IMethodBinding method;

	@Override
	public boolean visit(MethodInvocation node) {
		IMethodBinding binding = node.resolveMethodBinding();
		if (method.isEqualTo(binding)) {
			callees.put(Integer.valueOf(node.getStartPosition()), node);
		}
		return false;
	}

	public SortedMap<Integer, MethodInvocation> run(CompilationUnit unit, IMethodBinding lookingfor) {
		callees.clear();
		this.method = lookingfor;
		unit.accept(this);
		return callees;
	}
}
