package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;

public final class HotspotFinderFactory {

	private HotspotFinderFactory() {
	}

	public static IHotspotFinder create(IEclipseContext context, String name) {
		if (JDBCHotspotFinder.class.getSimpleName().equals(name)) {
			return ContextInjectionFactory.make(JDBCHotspotFinder.class, context);
		} else if (JPAHotspotFinder.class.getSimpleName().equals(name)) {
			return ContextInjectionFactory.make(JPAHotspotFinder.class, context);
		} else if (SpringHotspotFinder.class.getSimpleName().equals(name)) {
			return ContextInjectionFactory.make(SpringHotspotFinder.class, context);
		} else if (AndroidHotspotFinder.class.getSimpleName().equals(name)) {
			return ContextInjectionFactory.make(AndroidHotspotFinder.class, context);
		} else {
			throw new IllegalArgumentException("Invalid hotspotfinder: " + name);
		}
	}

	public static List<Class<?>> getHotspotFinders() {
		List<Class<?>> ret = new ArrayList<>();

		ret.add(JDBCHotspotFinder.class);
		ret.add(JPAHotspotFinder.class);
		ret.add(SpringHotspotFinder.class);
		ret.add(AndroidHotspotFinder.class);

		return ret;
	}

	public static String getHotspotFindersAsString() {
		return getHotspotFinders().stream().map(Class::getSimpleName).collect(Collectors.joining(","));
	}

	public static String getHotspotFindersAsString(String seperator) {
		return getHotspotFinders().stream().map(Class::getSimpleName).collect(Collectors.joining(seperator));
	}

	public static List<String> getHotspotFindersAsStringList() {
		return getHotspotFinders().stream().map(Class::getSimpleName).toList();
	}

	public static List<IHotspotFinder> create(IEclipseContext context, String... names) {
		List<IHotspotFinder> ret = new ArrayList<>();
		for (String name : names) {
			ret.add(create(context, name));
		}
		return ret;
	}

}
