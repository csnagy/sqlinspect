package sqlinspect.plugin.extractors;

public enum LoopHandling {
	LOOPS_ONCE("LOOPS_ONCE", 1), LOOPS_INF("LOOPS_INF", 0);
	private final String text;
	private final int value;

	LoopHandling(String text, int value) {
		this.text = text;
		this.value = value;
	}

	@Override
	public String toString() {
		return text;
	}

	public int getValue() {
		return value;
	}

	public static LoopHandling getLoopHandling(String value) {
		for (LoopHandling d : values()) {
			if (d.toString().equalsIgnoreCase(value)) {
				return d;
			}
		}
		throw new IllegalArgumentException();
	}

	public static LoopHandling getLoopHandling(int value) {
		for (LoopHandling d : values()) {
			if (d.getValue() == value) {
				return d;
			}
		}
		throw new IllegalArgumentException();
	}

}