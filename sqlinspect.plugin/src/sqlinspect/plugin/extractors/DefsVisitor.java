package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefsVisitor extends ASTVisitor {
	private static final Logger LOG = LoggerFactory.getLogger(DefsVisitor.class);
	private final List<SimpleName> defs = new ArrayList<>();

	@Override
	public boolean visit(SimpleName node) {
		if (node.resolveBinding() instanceof IVariableBinding) {
			defs.add(node);
		}
		return false;
	}

	@Override
	public boolean visit(Assignment node) {
		Expression lhs = node.getLeftHandSide();
		if (lhs != null) {
			lhs.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(PostfixExpression node) {
		return true;
	}

	@Override
	public boolean visit(PrefixExpression node) {
		return true;
	}

	@Override
	public boolean visit(MethodInvocation node) {
		// we are either too strict or too permissive here
		// at the moment we follow a permissive approach, we don't go after parameters
		Expression expr = node.getExpression();
		if (expr != null) {
			expr.accept(this);
		}
		return false;
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		defs.add(node.getName());
		return false;
	}

	public List<SimpleName> run(Statement stmt) {
		LOG.trace("Getting Defs of statement: {} ({})", stmt, stmt.getStartPosition());
		defs.clear();
		stmt.accept(this);

		if (LOG.isTraceEnabled()) {
			String ids = defs.stream().map(sn -> sn.toString() + " (" + sn.getStartPosition() + ")")
					.collect(Collectors.joining(", "));
			LOG.trace("Defs: {}", ids);
		}
		return defs;
	}

	public List<SimpleName> run(FieldDeclaration fieldDecl) {
		LOG.trace("Getting Defs of field declaration: {} ({})", fieldDecl, fieldDecl.getStartPosition());
		defs.clear();
		fieldDecl.accept(this);

		if (LOG.isTraceEnabled()) {
			String ids = defs.stream().map(sn -> sn.toString() + " (" + sn.getStartPosition() + ")")
					.collect(Collectors.joining(", "));
			LOG.trace("Defs: {}", ids);
		}
		return defs;
	}

}
