package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.BasicBlock;
import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGNodeFinder;
import sqlinspect.cfg.CFGRevPreOrder;
import sqlinspect.cfg.CFGStore;
import sqlinspect.plugin.utils.ASTUtils;

/**
 * Starting from an initial expression 'con.executeQuery(sql)', the goal is to
 * determine the potential value of the string parameter. So we trace back all
 * its defining expressions, until we can.
 *
 * Suppose we have the code snippet: s1_1 = "foo" s2_1 = s1_2 + "bar";
 *
 * s1_1, s2_1 <- variable definition s1_2 <- variable reference
 *
 * DefsOf: the variable definition X is defined by the expression s2_1 -> s1_2 +
 * "bar"
 *
 * RefsTo: the variable X refers to the variable definition s1_2 -> s1_1
 *
 */

public class MethodSlicer extends CFGRevPreOrder {

	private final CFGStore cfgStore;

	private static final Logger LOG = LoggerFactory.getLogger(MethodSlicer.class);

	private final Map<SimpleName, Expression> defsOf = new HashMap<>();

	private final Map<SimpleName, SimpleName> refsTo = new HashMap<>();

	// variables that are still undefined
	private final Set<SimpleName> toFindDefOf = new HashSet<>();

	private final Map<BasicBlock, List<SimpleName>> bbDefs = new HashMap<>();
	private final Map<BasicBlock, List<SimpleName>> bbRefs = new HashMap<>();

	private final UsesVisitor usesVisitor = new UsesVisitor();
	private final DefsVisitor defsVisitor = new DefsVisitor();

	private final List<Slice> slices = new ArrayList<>();

	private boolean cutInLoop; // cut slice of a reference if it is defined inside a loop

	private Optional<ListIterator<ASTNode>> startIt = Optional.empty();
	private CFG cfg;

	public MethodSlicer(CFGStore cfgStore) {
		this.cfgStore = cfgStore;
	}

	public List<Slice> run(Set<Expression> exprs, int maxDepth, boolean cutInLoop) {
		String exprString = exprs.stream().map(Expression::toString).collect(Collectors.joining(","));
		LOG.debug("Slicer looking for expressions (maxdepth: {}, cutInLoop: {}): {}", maxDepth, cutInLoop, exprString);

		slices.clear();

		Set<Statement> stmts = exprs.stream().map(ASTUtils::getParentStatement).collect(Collectors.toSet());
		if (stmts.size() != 1) {
			LOG.error("The expressions must have the same parent statement");
			return slices;
		}

		Statement stmt = stmts.stream().findFirst().orElseThrow(IllegalStateException::new);

		MethodDeclaration method = ASTUtils.getParentMethodDeclaration(stmt);
		if (method != null) {
			cfg = cfgStore.get(method);
		} else {
			LOG.error("Could not get method declaration for statement: {}", stmt);
			return slices;
		}

		CFGNodeFinder nodeFinder = new CFGNodeFinder(cfg, stmt);
		if (!nodeFinder.findStatement()) {
			LOG.error("Could not find the statement in the cfg!");
			return slices;
		}

		BasicBlock startBB = nodeFinder.getBlock();
		startIt = Optional.ofNullable(nodeFinder.getListIterator());

		clearSets();

		toFindDefOf.addAll(exprs.stream().map(usesVisitor::run).flatMap(List<SimpleName>::stream)
				.filter(u -> u.resolveConstantExpressionValue() == null).collect(Collectors.toSet()));

		this.cutInLoop = cutInLoop;
		super.run(startBB, maxDepth);

		clearSets();

		LOG.debug("Slicer done");
		return slices;
	}

	protected void clearSets() {
		defsOf.clear();
		refsTo.clear();
		toFindDefOf.clear();
		bbDefs.clear();
		bbRefs.clear();
	}

	@Override
	protected boolean visit(BasicBlock basicBlock) {
		LOG.trace("Visit bb: {}", basicBlock.getId());

		findDefsInBB(basicBlock);
		startIt = Optional.empty(); // we need it only for the first block

		if (basicBlock.equals(cfg.getEntry())) {
			String ids = toFindDefOf.stream().map(id -> id.toString() + " (" + id.getStartPosition() + ")")
					.collect(Collectors.joining(","));
			LOG.trace("Reached entry. Could not find ref of: {}", ids);

			TypeDeclaration classDecl = ASTUtils.getParentClass(cfg.getMethod());
			findDefsInFieldDeclarations(classDecl);
		}

		if (toFindDefOf.isEmpty() || basicBlock.equals(cfg.getEntry())) {
			if (LOG.isTraceEnabled()) {
				LOG.trace("No more ids in CFG path.");
				// end of actual back slice
				dumpDefinitions();
			}
			slices.add(new Slice(defsOf, refsTo, toFindDefOf));
			return false;
		} else {
			return true;
		}
	}

	@Override
	protected void visitEnd(BasicBlock basicBlock) {
		LOG.trace("VisitEnd bb: {}", basicBlock.getId());

		List<SimpleName> actualBBDefs = bbDefs.get(basicBlock);
		List<SimpleName> actualBBRefs = bbRefs.get(basicBlock);

		if (actualBBDefs != null) {
			actualBBDefs.stream().forEach(defsOf::remove);
			actualBBDefs.clear();
		}

		if (actualBBRefs != null) {
			toFindDefOf.addAll(actualBBRefs);
			actualBBRefs.clear();
		}
	}

	private void findDefsInBB(BasicBlock basicBlock) {
		if (LOG.isTraceEnabled()) {
			String ids = toFindDefOf.stream().map(id -> id.toString() + " (" + id.getStartPosition() + ")")
					.collect(Collectors.joining(","));
			LOG.trace("Resolve ids in bb: {} -> {}", basicBlock.getId(), ids);
		}

		if (basicBlock.nodes().isEmpty()) {
			return;
		}

		bbDefs.computeIfAbsent(basicBlock, k -> new ArrayList<>()).clear();
		bbRefs.computeIfAbsent(basicBlock, k -> new ArrayList<>()).clear();

		ListIterator<ASTNode> it = startIt.isPresent() ? startIt.get() : basicBlock.nodes().listIterator(basicBlock.nodes().size());
		while (it.hasPrevious()) {
			ASTNode node = it.previous();
			if (node instanceof Statement statement) {
				findDefsInStatement(basicBlock, statement);
			}
		}
	}

	private void findDefsInStatement(BasicBlock basicBlock, Statement stmt) {
		if (LOG.isTraceEnabled()) {
			String ids = toFindDefOf.stream().map(id -> id.toString() + " (" + id.getStartPosition() + ")")
					.collect(Collectors.joining(","));
			LOG.trace("Resolve ids in statement: {}  {} -> {}", basicBlock.getId(), stmt, ids);
		}

		List<SimpleName> stmtDefs = defsVisitor.run(stmt);
		List<SimpleName> newRefs = new LinkedList<>();
		List<SimpleName> foundRefs = new LinkedList<>();
		for (SimpleName def : stmtDefs) {
			IBinding defBinding = def.resolveBinding();
			if (defBinding != null) {
				for (SimpleName ref : toFindDefOf) {
					IBinding refBinding = ref.resolveBinding();
					if (defBinding.equals(refBinding)) {
						LOG.trace("Working on def {} ({}) -> {} ({})", def, def.getStartPosition(), ref,
								ref.getStartPosition());
						refsTo.put(ref, def);
						LOG.trace("New ref: {} ({})-> {} ({})", ref, ref.getStartPosition(), def,
								def.getStartPosition());
						bbRefs.get(basicBlock).add(ref);
						foundRefs.add(ref);

						Expression definingExpression = getDefiningExpression(stmt, def);
						if (!(cutInLoop && inLoop(stmt)) && definingExpression != null) {
							defsOf.put(def, definingExpression);
							LOG.trace("New def: {} ({}) -> {} ({})", def, def.getStartPosition(), definingExpression,
									definingExpression.getStartPosition());
							bbDefs.get(basicBlock).add(def);

							usesVisitor.run(definingExpression).stream()
									.filter(u -> u.resolveConstantExpressionValue() == null).forEach(newRefs::add);
						}
					}
				}
			}
		}

		toFindDefOf.removeAll(foundRefs);
		toFindDefOf.addAll(newRefs);
	}

	@SuppressWarnings("unchecked")
	private static Expression getDefiningExpression(FieldDeclaration fieldDecl, SimpleName name) {
		if (LOG.isTraceEnabled()) {
			LOG.trace("Get defining expression of: {} ({}) -> {} ({})", name, name.getStartPosition(), fieldDecl,
					fieldDecl.getStartPosition());
		}

		for (VariableDeclarationFragment fragment : (List<VariableDeclarationFragment>) fieldDecl.fragments()) {
			if (fragment.getName().equals(name)) {
				return fragment.getInitializer();
			}
		}
		return null;
	}

	private static Expression getDefiningExpression(Statement stmt, SimpleName name) {
		if (stmt instanceof ExpressionStatement expressionStatement) {
			return getDefiningExpression(expressionStatement, name);
		} else if (stmt instanceof VariableDeclarationStatement variableDeclarationStatement) {
			return getDefiningExpression(variableDeclarationStatement, name);
		} else {
			LOG.error("Unsupported statement: {}", stmt);
			return null;
		}
	}

	private static Expression getDefiningExpression(ExpressionStatement stmt, SimpleName name) {
		if (LOG.isTraceEnabled()) {
			LOG.trace("Get defining expression of: {} ({}) -> {} ({})", name, name.getStartPosition(), stmt,
					stmt.getStartPosition());
		}

		Expression expr = stmt.getExpression();
		if (expr instanceof Assignment assignment) {
			Expression lhs = assignment.getLeftHandSide();
			if (lhs instanceof SimpleName && lhs.equals(name)) {
				return assignment;
			}
		} else if (expr instanceof MethodInvocation) {
			return expr;
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	private static Expression getDefiningExpression(VariableDeclarationStatement stmt, SimpleName name) {
		if (LOG.isTraceEnabled()) {
			LOG.trace("Get defining expression of: {} ({}) -> {} ({})", name, name.getStartPosition(), stmt,
					stmt.getStartPosition());
		}

		for (VariableDeclarationFragment fragment : (List<VariableDeclarationFragment>) stmt.fragments()) {
			if (fragment.getName().equals(name)) {
				return fragment.getInitializer();
			}
		}
		return null;
	}

	private void findDefsInFieldDeclarations(TypeDeclaration classDecl) {
		for (FieldDeclaration fieldDecl : classDecl.getFields()) {
			findDefsInFieldDeclaration(fieldDecl);
		}
	}

	private void findDefsInFieldDeclaration(FieldDeclaration fieldDecl) {
		if (LOG.isTraceEnabled()) {
			String ids = toFindDefOf.stream().map(id -> id.toString() + " (" + id.getStartPosition() + ")")
					.collect(Collectors.joining(","));
			LOG.trace("Resolve ids in field declaration: {} -> {}", fieldDecl, ids);
		}

		List<SimpleName> declDefs = defsVisitor.run(fieldDecl);
		List<SimpleName> newRefs = new LinkedList<>();
		List<SimpleName> foundRefs = new LinkedList<>();
		for (SimpleName declDef : declDefs) {
			IBinding declDefBinding = declDef.resolveBinding();
			if (declDefBinding != null) {
				for (SimpleName ref : toFindDefOf) {
					IBinding refBinding = ref.resolveBinding();
					if (declDefBinding.equals(refBinding)) {
						LOG.trace("Working on def {} ({}) -> {} ({})", declDef, declDef.getStartPosition(), ref,
								ref.getStartPosition());
						refsTo.put(ref, declDef);
						LOG.trace("New ref: {} ({})-> {} ({})", ref, ref.getStartPosition(), declDef,
								declDef.getStartPosition());
						foundRefs.add(ref);

						Expression definingExpression = getDefiningExpression(fieldDecl, declDef);
						if (definingExpression != null) {
							defsOf.put(declDef, definingExpression);
							LOG.trace("New def: {} ({}) -> {} ({})", declDef, declDef.getStartPosition(),
									definingExpression, definingExpression.getStartPosition());

							usesVisitor.run(definingExpression).stream()
									.filter(u -> u.resolveConstantExpressionValue() == null).forEach(newRefs::add);
						}
					}
				}
			}
		}

		toFindDefOf.removeAll(foundRefs);
		toFindDefOf.addAll(newRefs);
	}

	private void dumpDefinitions() {
		LOG.trace("Definitions: ");
		for (Entry<SimpleName, Expression> e : defsOf.entrySet()) {
			LOG.trace(" {} ({})-> {} ({})", e.getKey(), e.getKey().getStartPosition(), e.getValue(),
					e.getValue().getStartPosition());
		}
		LOG.trace("References: ");
		for (Entry<SimpleName, SimpleName> e : refsTo.entrySet()) {
			LOG.trace(" {} ({})-> {} ({})", e.getKey(), e.getKey().getStartPosition(), e.getValue(),
					e.getValue().getStartPosition());
		}
	}

	private static boolean inLoop(Statement stmt) {
		ASTNode parent = stmt.getParent();
		while (parent != null) {
			if (parent instanceof ForStatement || parent instanceof EnhancedForStatement
					|| parent instanceof WhileStatement || parent instanceof DoStatement) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
}
