package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;

public final class QueryResolverFactory {

	private QueryResolverFactory() {
	}

	public static IQueryResolver create(IEclipseContext context, String name) {
		if (InterQueryResolver.class.getSimpleName().equals(name)) {
			return ContextInjectionFactory.make(InterQueryResolver.class, context);
		} else {
			throw new IllegalArgumentException("Invalid query resolver: " + name);
		}
	}

	public static List<Class<?>> getQueryResolvers() {
		List<Class<?>> ret = new ArrayList<>();

		ret.add(InterQueryResolver.class);

		return ret;
	}
}
