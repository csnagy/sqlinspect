package sqlinspect.plugin.extractors;

import java.util.Collection;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.CompilationUnit;

import sqlinspect.plugin.model.Hotspot;

public interface IHotspotFinder {
	String getName();

	void setDescriptors(Collection<HotspotDesc> hsdescs);

	void setupWithProjectSettings(IProject project);

	void setupWithDefaults();

	/**
	 * Starts the process.
	 */

	Collection<Hotspot> extract(CompilationUnit unit, IPath path, IQueryResolver resolver);

	Collection<Hotspot> extract(ICompilationUnit unit, IQueryResolver resolver);
}
