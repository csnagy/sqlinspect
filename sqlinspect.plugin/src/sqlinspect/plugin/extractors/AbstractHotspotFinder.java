package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.model.QueryPart;
import sqlinspect.plugin.model.UnresolvedQueryPart;
import sqlinspect.plugin.utils.ASTStore;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.plugin.utils.BindingUtil;

public abstract class AbstractHotspotFinder extends ASTVisitor implements IHotspotFinder {

	@Inject
	protected ASTStore astStore;

	@Inject
	protected CFGStore cfgStore;

	private CompilationUnit unit;

	private IPath path;

	private static final Logger LOG = LoggerFactory.getLogger(AbstractHotspotFinder.class);

	private IQueryResolver resolver;
	private PreparedStatementResolver psResolver;

	private final Map<String, HotspotDesc> hsMap = new HashMap<>();

	private final List<Hotspot> hotspots = new ArrayList<>();

	@Override
	public void setDescriptors(Collection<HotspotDesc> hsdescs) {
		hsMap.clear();
		for (HotspotDesc hd : hsdescs) {
			hsMap.put(hd.getSignature(), hd);
		}
	}

	public void setPSResolver(PreparedStatementResolver psResolver) {
		this.psResolver = psResolver;
	}

	private void processStatement(MethodInvocation node, HotspotDesc hd) {
		LOG.debug("Found query hotspot at {} : {}: {}", unit.getJavaElement().getElementName(),
				ASTUtils.getLineNumber(node), node);

		Hotspot hs = new Hotspot(this, unit, node, path);

		if (node.arguments().size() < hd.getArgnum()) {
			LOG.warn("expected at least {} arguments, but got only {}", hd.getArgnum(), node.arguments().size());
		} else {
			Expression arg = (Expression) node.arguments().get(hd.getArgnum() - 1);
			List<QueryPart> rootparts = resolver.resolve(arg);

			// eliminate duplicated queries
			Map<String, QueryPart> queries = rootparts.stream()
					.collect(Collectors.toMap(QueryPart::getValue, q -> q, (q1, q2) -> q1));

			queries.values().forEach(q -> hs.addQuery(new Query(q)));
		}
		hotspots.add(hs);
	}

	private void processPrepareStatement(MethodInvocation node) {
		LOG.debug("Found prepare statement hotspot at line {}: {}", ASTUtils.getLineNumber(node), node);

		Hotspot hs = new Hotspot(this, unit, node, path);

		Set<Expression> exprList = new HashSet<>();
		exprList.addAll(psResolver.resolve(node));
		if (exprList.isEmpty()) {
			Query query = new Query(new UnresolvedQueryPart(node));
			LOG.debug("Query: {}", query.getValue());
			hs.addQuery(query);
		} else {
			for (Expression expr : exprList) {

				if (expr != null) {
					List<QueryPart> rootparts = resolver.resolve(expr);

					// eliminate duplicated queries
					Map<String, QueryPart> queries = rootparts.stream()
							.collect(Collectors.toMap(QueryPart::getValue, q -> q, (q1, q2) -> q1));

					queries.values().forEach(q -> hs.addQuery(new Query(q)));
				} else {
					QueryPart qp = new UnresolvedQueryPart(node);
					Query query = new Query(qp);
					hs.addQuery(query);
				}
			}
		}

		hotspots.add(hs);
	}

	@Override
	public boolean visit(MethodInvocation node) {
		IMethodBinding binding = node.resolveMethodBinding();
		if (binding == null) {
			return false;
		}

		String qsig = BindingUtil.qualifiedSignature(binding);

		// check if the method signature is in the list of hotspotdescriptions
		HotspotDesc hd = hsMap.get(qsig);
		if (hd == null) {
			return false;
		}

		if (hd.getArgnum() < 0) {
			LOG.warn("Illegal argument number for hotspot descriptor: {}", hd);
		} else if (hd.getArgnum() == 0) {
			processPrepareStatement(node);
		} else {
			processStatement(node, hd);
		}
		return false;
	}

	@Override
	public Collection<Hotspot> extract(CompilationUnit unit, IPath path, IQueryResolver resolver) {
		hotspots.clear();
		this.path = path;
		this.unit = unit;
		this.resolver = resolver;
		this.unit.accept(this);
		return hotspots;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Collection<Hotspot> extract(ICompilationUnit unit, IQueryResolver resolver) {
		CompilationUnit cu = astStore.get(unit);
		if (cu != null) {
			Collection<Hotspot> ret = extract(cu, unit.getPath(), resolver);
			if (ret.isEmpty()) {
				astStore.remove(unit);
			}
			return ret;
		} else {
			LOG.warn("Could not get compilation unit for unit: {}", unit);
			return new ArrayList<>(0);
		}
	}
}
