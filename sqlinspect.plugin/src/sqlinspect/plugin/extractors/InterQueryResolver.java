package sqlinspect.plugin.extractors;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.internal.corext.callhierarchy.CallHierarchy;
import org.eclipse.jdt.internal.corext.callhierarchy.MethodWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.plugin.model.QueryPart;
import sqlinspect.plugin.model.QueryPartFactory;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.utils.ASTStore;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.plugin.utils.BindingUtil;

@SuppressWarnings("restriction")
public class InterQueryResolver implements IQueryResolver {

	private static final Logger LOG = LoggerFactory.getLogger(InterQueryResolver.class);

	private final ASTStore astStore;

	private final QueryPartFactory qpf;

	public static final LoopHandling DEFAULT_LOOP_HANDLING = LoopHandling.LOOPS_INF;
	public static final int DEFAULT_MAX_CFG_DEPTH = 15;
	public static final int DEFAULT_MAX_CALL_DEPTH = 5;

	private int maxCFGDepth = DEFAULT_MAX_CFG_DEPTH;
	private int maxCallDepth = DEFAULT_MAX_CALL_DEPTH;
	private LoopHandling loopHandling = DEFAULT_LOOP_HANDLING;

	private static final String STRINGBUFFER_TOSTRING = "java.lang.StringBuffer.toString";
	private static final String STRINGBUFFER_APPEND = "java.lang.StringBuffer.append";
	private static final String STRINGBUFFER = "java.lang.StringBuffer";
	private static final String STRINGBUILDER_TOSTRING = "java.lang.StringBuilder.toString";
	private static final String STRINGBUILDER_APPEND = "java.lang.StringBuilder.append";
	private static final String STRINGBUILDER = "java.lang.StringBuilder";
	private static final String SIMPLETYPE_INT = "int";

	private final MethodSlicer slicer;

	private final List<QueryPart> queries = new ArrayList<>();

	private final Deque<PreOrderElement> toVisit = new ArrayDeque<>();
	private final Map<Statement, Integer> visited = new HashMap<>();
	private final Map<Statement, Set<Expression>> exprs = new HashMap<>();
	private final Map<Statement, List<Slice>> slices = new HashMap<>();
	private final Map<Statement, List<Statement>> children = new HashMap<>();
	private final Map<Statement, Slice> crossSlices = new HashMap<>();

	private static class PreOrderElement {
		private final Statement element;
		private final int depth;

		public PreOrderElement(Statement element, int depth) {
			this.element = element;
			this.depth = depth;
		}

		public Statement getElement() {
			return element;
		}

		public int getDepth() {
			return depth;
		}
	}

	@Inject
	public InterQueryResolver(QueryRepository queryRepository, CFGStore cfgStore, ASTStore astStore) {
		this.astStore = astStore;
		this.slicer = new MethodSlicer(cfgStore);
		this.qpf = queryRepository.getQueryPartFactory();
	}

	/**
	 * The entry method of the resolver. Resolve the expression and return the root
	 * query part.
	 */
	@Override
	public List<QueryPart> resolve(Expression expr) {
		LOG.debug("Resolve expression: {}", expr);

		queries.clear();

		callGraphPreOrder(expr);

		LOG.debug("Resolve expression {} done.", expr);
		return queries;
	}

	private void callGraphPreOrder(Expression startExpression) {
		Statement stmt = ASTUtils.getParentStatement(startExpression);

		slices.clear();
		children.clear();
		exprs.clear();
		crossSlices.clear();

		toVisit.clear();
		visited.clear();

		toVisit.addFirst(new PreOrderElement(stmt, 0));
		toVisit.addFirst(new PreOrderElement(stmt, 0));
		exprs.computeIfAbsent(stmt, k -> new HashSet<>()).add(startExpression);

		while (!toVisit.isEmpty()) {
			PreOrderElement actualElement = toVisit.pop();
			Statement actualCallStatement = actualElement.getElement();
			Integer count = visited.computeIfAbsent(actualCallStatement, k -> Integer.valueOf(0));

			if (count % 2 == 0) {
				// visit
				LOG.debug("Visit statement ({}): {}", count, actualCallStatement);
				visited.put(actualCallStatement, count + 1);
				visitCall(actualCallStatement, actualElement.getDepth());
			} else {
				// visitEnd
				LOG.debug("Visit end ({}): {}", count, actualCallStatement);
				visited.put(actualCallStatement, count + 1);
				visitEndCall(actualCallStatement);
			}
		}

		slices.get(stmt).stream().forEach(slice -> {
			LOG.debug("Interpret query: {}", startExpression);
			queries.add(interpret(startExpression, slice));
		});

		toVisit.clear();
		visited.clear();

		slices.clear();
		children.clear();
		exprs.clear();
		crossSlices.clear();
	}

	private void visitCall(Statement callStatement, int depth) {
		// do the slice
		Set<Expression> params = exprs.get(callStatement);
		final boolean cutLoop = loopHandling == LoopHandling.LOOPS_INF;

		slices.computeIfAbsent(callStatement, k -> new ArrayList<>()).addAll(slicer.run(params, maxCFGDepth, cutLoop));

		if (depth >= maxCallDepth) {
			return;
		}

		// look for unresolved method parameters
		MethodDeclaration actualMethodDecl = ASTUtils.getParentMethodDeclaration(callStatement);

		if (actualMethodDecl == null) {
			LOG.error("Could not get parent method declaration for callStatement: {}", callStatement);
			return;
		}

		@SuppressWarnings("unchecked")
		List<SingleVariableDeclaration> paramDecls = actualMethodDecl.parameters();

		Map<IBinding, Integer> paramBindings = IntStream.range(0, paramDecls.size()).mapToObj(Integer::valueOf)
				.collect(Collectors.toMap(i -> paramDecls.get(i).resolveBinding(), i -> i));

		Map<IBinding, SimpleName> lookingForVariables = slices.get(callStatement).stream()
				.flatMap(slice -> slice.getToFindDefOf().stream())
				.collect(Collectors.toMap(SimpleName::resolveBinding, var -> var, (var1, var2) -> var1)); // ignore
																											// duplicates
		Map<IBinding, Integer> lookingForParams = paramBindings.entrySet().stream()
				.filter(e -> lookingForVariables.keySet().contains(e.getKey()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue));

		if (!lookingForParams.isEmpty()) {
			String paramString = lookingForParams.entrySet().stream()
					.map(e -> "(" + e.getKey() + "," + e.getValue() + ")").collect(Collectors.joining(","));
			LOG.debug("Looking for parameters: {}", paramString);

			Map<String, ICompilationUnit> callers = getCallersOf(actualMethodDecl).stream().filter(Optional::isPresent)
					.map(caller -> caller.get().getCompilationUnit()).filter(Objects::nonNull)
					.collect(Collectors.toMap(ICompilationUnit::getElementName, cu -> cu, (cu1, cu2) -> cu1)); // ignore
																												// duplicates
			for (Entry<String, ICompilationUnit> icu : callers.entrySet()) {
				LOG.debug("Caller: {}", icu.getKey());

				CompilationUnit cu = astStore.get(icu.getValue());
				SortedMap<Integer, MethodInvocation> invocations = new CalleeVisitor().run(cu,
						actualMethodDecl.resolveBinding());
				for (Entry<Integer, MethodInvocation> mi : invocations.entrySet()) {
					Statement miStmt = ASTUtils.getParentStatement(mi.getValue());
					if (visited.getOrDefault(miStmt, 0) == 0) {
						LOG.debug("Invocation: {}", mi.getValue());
						toVisit.addFirst(new PreOrderElement(miStmt, depth + 1));
						toVisit.addFirst(new PreOrderElement(miStmt, depth + 1));
						children.computeIfAbsent(callStatement, k -> new ArrayList<>()).add(miStmt);

						Set<Expression> methodExprs = exprs.computeIfAbsent(miStmt, k -> new HashSet<>());
						@SuppressWarnings("rawtypes")
						List args = mi.getValue().arguments();
						if (args.size() == paramDecls.size()) {
							Slice crossSlice = new Slice();
							for (Entry<IBinding, Integer> p : lookingForParams.entrySet()) {
								Object arg = args.get(p.getValue());
								Integer index = p.getValue();
								if (arg instanceof Expression argExpression) {
									methodExprs.add(argExpression);
									crossSlice.getRefsTo().put(lookingForVariables.get(p.getKey()),
											paramDecls.get(index).getName());
									crossSlice.getDefsOf().put(paramDecls.get(index).getName(), argExpression);
								} else {
									LOG.error("Could not find parameter {} in invocation {}!", p, mi);
								}
							}
							crossSlices.put(miStmt, crossSlice);
						}
					}
				}
			}
		}
	}

	private void visitEndCall(Statement callStatement) {
		List<Statement> childs = children.computeIfAbsent(callStatement, k -> new ArrayList<>()).stream()
				.filter(child -> !child.equals(callStatement)).toList();
		if (!childs.isEmpty()) {
			List<Slice> newSlices = new ArrayList<>();
			for (Slice slice : slices.getOrDefault(callStatement, new ArrayList<>())) {
				for (Statement child : childs) {
					LOG.trace("Combining with stmt: {}", child);
					for (Slice childSlice : slices.getOrDefault(child, new ArrayList<>())) {
						Slice newSlice = new Slice(slice);
						Slice crossSlice = crossSlices.get(child);
						if (crossSlice != null) {
							newSlice.combine(crossSlice);
						} else {
							LOG.trace("No cross slices");
						}
						newSlice.combine(childSlice);
						newSlices.add(newSlice);
					}
				}
			}
			slices.put(callStatement, newSlices);
		}
	}

	private QueryPart interpret(Expression expr, Slice slice) {
		if (expr instanceof SimpleName simpleName) {
			return interpret(simpleName, slice);
		} else if (expr instanceof QualifiedName qualifiedName) {
			return interpret(qualifiedName, slice);
		} else if (expr instanceof InfixExpression infixExpression) {
			return interpret(infixExpression, slice);
		} else if (expr instanceof StringLiteral stringLiteral) {
			return interpret(stringLiteral);
		} else if (expr instanceof NullLiteral nullLiteral) {
			return interpret(nullLiteral);
		} else if (expr instanceof ParenthesizedExpression parenthesizedExpression) {
			return interpret(parenthesizedExpression, slice);
		} else if (expr instanceof ConditionalExpression conditionalExpression) {
			return interpret(conditionalExpression);
		} else if (expr instanceof Assignment assignment) {
			return interpret(assignment, slice);
		} else if (expr instanceof MethodInvocation methodInvocation) {
			// we support only StringBuilder and StringBuffer.toString() and append() here
			return interpret(methodInvocation, slice);
		} else if (expr instanceof ClassInstanceCreation classInstanceCreation) {
			return interpret(classInstanceCreation, slice);
		} else {
			// try to use the constant expression resolver of eclipse, if
			// possible
			Object constExpr = expr.resolveConstantExpressionValue();
			if (constExpr != null) {
				return qpf.createSimpleQueryPart(expr, constExpr.toString());
			} else {
				LOG.warn("Unhandled expression type for expression resolver: {}", expr.getClass());
				return qpf.createUnresolvedQueryPart(expr);
			}
		}
	}

	/**
	 * Resolve the string value of a SimpleName expression. A simple name is an
	 * identifier in JDT.
	 *
	 * @param node the simple name node
	 * @return
	 */
	private QueryPart interpret(SimpleName node, Slice slice) {
		// if it is a constant variable, the constant expression resolver of
		// eclipse may simply resolve its value
		Object constExpr = node.resolveConstantExpressionValue();
		if (constExpr != null) {
			return qpf.createSimpleQueryPart(node, constExpr.toString());
		}

		SimpleName def = slice.getRefsTo().get(node);
		if (def != null) {
			Expression expr = slice.getDefsOf().get(def);
			if (expr != null) {
				return qpf.createRefQueryPart(node, qpf.createRefQueryPart(def, interpret(expr, slice)));
			}
		}

		return qpf.createUnresolvedQueryPart(node);
	}

	/**
	 * Resolve the string value of a QualifiedName.
	 *
	 * @param node the qualified name node
	 * @return
	 */
	private QueryPart interpret(QualifiedName node, Slice slice) {
		Object constExpr = node.resolveConstantExpressionValue();
		if (constExpr != null) {
			return qpf.createSimpleQueryPart(node, constExpr.toString());
		}

		return qpf.createUnresolvedQueryPart(node);
	}

	/**
	 * Resolve an infix expression. We support only string concatenation (e.g., str1
	 * + str2). Return unresolved otherwise.
	 *
	 * @param node
	 * @return
	 */
	private QueryPart interpret(InfixExpression node, Slice slice) {
		QueryPart ret;

		// try to use the constant expression resolver of eclipse, if possible
		Object constExpr = node.resolveConstantExpressionValue();
		if (constExpr != null) {
			ret = qpf.createSimpleQueryPart(node, constExpr.toString());
		} else if (node.getOperator() == InfixExpression.Operator.PLUS) {
			List<QueryPart> parts = new ArrayList<>();
			parts.add(interpret(node.getLeftOperand(), slice));
			parts.add(interpret(node.getRightOperand(), slice));

			for (Object operand : node.extendedOperands()) {
				if (operand instanceof Expression expression) {
					parts.add(interpret(expression, slice));
				}
			}

			ret = qpf.createCompoundQueryPart(node, parts);
		} else {
			LOG.warn("Unhandled InfixExpression opreator: {}", node.getOperator());
			ret = qpf.createUnresolvedQueryPart(node);
		}

		return ret;
	}

	/**
	 * Resolve a string literal. Simply return its value.
	 *
	 * @param node
	 * @return
	 */
	private QueryPart interpret(StringLiteral node) {
		// try to use the constant expression resolver of eclipse, if possible
		Object subExpr = node.resolveConstantExpressionValue();
		if (subExpr != null) {
			return qpf.createSimpleQueryPart(node, subExpr.toString());
		} else {
			return qpf.createSimpleQueryPart(node, node.getLiteralValue());
		}
	}

	private QueryPart interpret(NullLiteral node) {
		return qpf.createUnresolvedQueryPart(node);
	}

	/**
	 * Resolve a ParenthesizedExpression: resolve its subexpression.
	 *
	 * @param node
	 * @return
	 */
	private QueryPart interpret(ParenthesizedExpression node, Slice slice) {
		return qpf.createRefQueryPart(node, interpret(node.getExpression(), slice));
	}

	/**
	 * Resolve a ConditionalExpression: resolve its subexpressions.
	 *
	 * @param node
	 * @return
	 */
	private QueryPart interpret(ConditionalExpression node) {
		return qpf.createUnresolvedQueryPart(node);
	}

	/**
	 * Resolve a ConditionalExpression: resolve its subexpressions.
	 *
	 * @param node
	 * @return
	 */
	private QueryPart interpret(Assignment node, Slice slice) {
		Expression lhs = node.getLeftHandSide();
		Expression rhs = node.getRightHandSide();

		if (node.getOperator() == Assignment.Operator.ASSIGN) {
			return qpf.createRefQueryPart(node, interpret(rhs, slice));
		} else if (node.getOperator() == Assignment.Operator.PLUS_ASSIGN) {
			return qpf.createCompoundQueryPart(node, interpret(lhs, slice), interpret(rhs, slice));
		} else {
			return qpf.createUnresolvedQueryPart(node);
		}

	}

	/**
	 * Resolve method invocations. StringBuilder/StringBuffer.toString() and
	 * StringBuilder/StringBuffer.append() methods are supported here. Return
	 * unresolved otherwise.
	 *
	 * @param node
	 * @return
	 */
	private QueryPart interpret(MethodInvocation node, Slice slice) {
		IMethodBinding binding = node.resolveMethodBinding();
		if (binding != null) {
			String name = BindingUtil.qualifiedName(binding);
			if (STRINGBUFFER_TOSTRING.equals(name) || STRINGBUILDER_TOSTRING.equals(name)) {
				Expression expr = node.getExpression();
				return qpf.createRefQueryPart(node, interpret(expr, slice));
			} else if (STRINGBUFFER_APPEND.equals(name) || STRINGBUILDER_APPEND.equals(name)) {
				Expression expr = node.getExpression();
				if (node.arguments().size() == 1) {
					Expression arg = (Expression) node.arguments().get(0);
					return qpf.createCompoundQueryPart(node, interpret(expr, slice), interpret(arg, slice));
				}
			}
		}

		return qpf.createUnresolvedQueryPart(node);
	}

	/**
	 * Resolve a ClassInstanceCreation method. 'new StringBuffer()' is supported
	 * here. Return unresolved otherwise.
	 *
	 * @param node
	 * @return
	 */

	private QueryPart interpret(ClassInstanceCreation node, Slice slice) {
		Type type = node.getType();
		if (type instanceof SimpleType simpleType) {
			ITypeBinding binding = simpleType.resolveBinding();
			String name = BindingUtil.qualifiedName(binding);
			if (STRINGBUFFER.equals(name) || STRINGBUILDER.equals(name)) {
				// Return the strings for: StringBuffer(String),
				// StringBuffer(CharSequence)
				// Return empty string for: StringBuffer(), StringBuffer(int
				// capacity)
				// Do the same for StringBuilder
				@SuppressWarnings("unchecked")
				List<Expression> args = node.arguments();
				if (args.isEmpty()) {
					// StringBuffer()
					return qpf.createSimpleQueryPart(node, "");
				} else if (args.size() == 1) {
					Expression arg = args.get(0);
					ITypeBinding argType = arg.resolveTypeBinding();
					String typeName = BindingUtil.qualifiedName(argType);
					if (SIMPLETYPE_INT.equals(typeName)) {
						// StringBuffer(int)
						return qpf.createSimpleQueryPart(node, "");
					} else {
						// StringBuffer(String), StringBuffer(CharSequence)
						return qpf.createRefQueryPart(node, interpret(arg, slice));
					}
				}
			}
		}

		return qpf.createUnresolvedQueryPart(node);
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	public void setMaxCFGDepth(int maxDepth) {
		this.maxCFGDepth = maxDepth;
	}

	public void setMaxCallDepth(int maxCallDepth) {
		this.maxCallDepth = maxCallDepth;
	}

	public List<Optional<IMethod>> getCallersOf(MethodDeclaration md) {
		List<Optional<IMethod>> callers = new ArrayList<>();

		IMethodBinding methodBinding = md.resolveBinding();
		if (methodBinding == null) {
			LOG.error("Could not resolve metod declaration: {}", md.getName());
			return callers;
		}

		IJavaElement javaElement = methodBinding.getJavaElement();
		if (!(javaElement instanceof IMethod)) {
			LOG.error("Method binding is not a Java element: {}", md.getName());
			return callers;
		}

		IMethod javaMethod = (IMethod) javaElement;

		CallHierarchy callHierarchy = CallHierarchy.getDefault();

		IMember[] members = { javaMethod };

		try {
			MethodWrapper[] methodWrappers = callHierarchy.getCallerRoots(members);
			Stream<MethodWrapper> callerStream = Stream.of(methodWrappers)
					.flatMap(mw -> Stream.of(mw.getCalls(new NullProgressMonitor())));
			callers = callerStream.map(InterQueryResolver::getIMethodFromMethodWrapper).toList();
		} catch (Exception e) {
			LOG.error("Unexpected exception while retriewing callers of {}:", md, e);
			return callers;
		}

		return callers;
	}

	private static Optional<IMethod> getIMethodFromMethodWrapper(MethodWrapper m) {
		IMember im = m.getMember();
		if (im != null && im.getElementType() == IJavaElement.METHOD) {
			return Optional.of((IMethod) m.getMember());
		}
		return Optional.empty();
	}

	public void setLoopHandling(LoopHandling loopHandling) {
		this.loopHandling = loopHandling;
	}
}
