package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsesVisitor extends ASTVisitor {
	private static final Logger LOG = LoggerFactory.getLogger(UsesVisitor.class);
	private final List<SimpleName> uses = new ArrayList<>();

	@Override
	public boolean visit(SimpleName node) {
		uses.add(node);
		return false;
	}

	@Override
	public boolean visit(Assignment expr) {
		Expression lhs = expr.getLeftHandSide();
		if (!Assignment.Operator.ASSIGN.equals(expr.getOperator()) && lhs != null) {
			lhs.accept(this);
		}

		Expression rhs = expr.getRightHandSide();
		if (rhs != null) {
			rhs.accept(this);
		}
		return false;
	}

	public List<SimpleName> run(Expression expr) {
		LOG.trace("Getting Uses of expressions: {} ({})", expr, expr.getStartPosition());
		uses.clear();
		expr.accept(this);

		if (LOG.isTraceEnabled()) {
			String ids = uses.stream().map(sn -> sn.toString() + " (" + sn.getStartPosition() + ")")
					.collect(Collectors.joining(", "));
			LOG.debug("Uses: {}", ids);
		}
		return uses;
	}

}
