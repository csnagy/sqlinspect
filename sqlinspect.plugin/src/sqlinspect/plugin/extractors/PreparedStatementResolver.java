package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.plugin.utils.BindingUtil;

public final class PreparedStatementResolver {
	private static final Logger LOG = LoggerFactory.getLogger(PreparedStatementResolver.class);

	private final CFGStore cfgStore;

	private final Map<String, HotspotDesc> hsMap = new HashMap<>();
	private List<Pattern> setters = new ArrayList<>();

	public static final int DEFAULT_MAX_DEPTH = 100;

	private final int maxDepth;

	public PreparedStatementResolver(CFGStore cfgStore, int maxdepth) {
		this.cfgStore = cfgStore;
		this.maxDepth = maxdepth;
	}

	public void setDescriptors(Collection<HotspotDesc> hsdescs) {
		hsMap.clear();
		for (HotspotDesc hd : hsdescs) {
			hsMap.put(hd.getSignature(), hd);
		}
	}

	public void setSetters(Collection<String> setters) {
		this.setters = setters.stream().map(Pattern::compile).toList();
	}

	public List<Expression> resolve(MethodInvocation node) {
		// typically we have sg like:
		// PreparedStatement stmt = connection.prepareStatement(query);
		// ....
		// stmt.executeQuery();
		// OR:
		// PreparedStatement stmt = null;
		// if ( ..) {
		// stmt = connection.prepareStatement(query1);
		// } else {
		// stmt = connection.prepareStatement(query2);
		// }
		// ...
		// stmt.executeQuery();

		// so we try to locate the last assignments of 'stmt'

		List<Expression> ret = new ArrayList<>();
		List<Expression> initExprs = getInitializeExpression(node);

		if (initExprs != null) {
			for (Expression ie : initExprs) {
				if (ie instanceof MethodInvocation methodInvocation) {
					IMethodBinding binding = methodInvocation.resolveMethodBinding();

					if (binding != null) {
						String qsig = BindingUtil.qualifiedSignature(binding);

						HotspotDesc hd = hsMap.get(qsig);
						if (hd != null) {
							if (hd.getArgnum() < 0) {
								LOG.error("Illegel argument number for hotspot descriptor: {}", hd);
								return ret;
							} else {
								ret.add((Expression) methodInvocation.arguments().get(hd.getArgnum() - 1));
							}
						}
					}
				}
			}
		}

		return ret;
	}

	public Expression getAssignmentExpression(ExpressionStatement exprStmt, SimpleName simpleName) {
		Expression innerexpr = exprStmt.getExpression();

		if (innerexpr instanceof Assignment assignment) {
			Expression lhs = assignment.getLeftHandSide();
			Expression rhs = assignment.getRightHandSide();

			if (lhs instanceof SimpleName innerSimpleName) {
				IBinding binding = innerSimpleName.resolveBinding();
				if (binding != null && binding.equals(simpleName.resolveBinding())) {
					return rhs;
				}
			}
		}
		return null;
	}

	public Expression getAssignmentExpression(VariableDeclarationStatement vardecl, SimpleName simpleName) {
		for (Object fragmentObject : vardecl.fragments()) {
			if (fragmentObject instanceof VariableDeclarationFragment variableDeclarationFragment) {
				IBinding binding = variableDeclarationFragment.resolveBinding();
				if (binding != null && binding.equals(simpleName.resolveBinding())) {
					return variableDeclarationFragment.getInitializer();
				}
			}
		}
		return null;
	}

	public Expression getAssignmentExpression(Statement stmt, SimpleName simpleName) {
		if (stmt instanceof ExpressionStatement expressionStatement) {
			return getAssignmentExpression(expressionStatement, simpleName);
		} else if (stmt instanceof VariableDeclarationStatement variableDeclarationStatement) {
			return getAssignmentExpression(variableDeclarationStatement, simpleName);
		}
		return null;
	}

	public List<Expression> getInitializeExpression(MethodInvocation node) {
		Expression expr = node.getExpression();

		// we have 'stmt' now
		if (expr instanceof SimpleName simpleName) {
			PreparedStatementVisitor visitor = new PreparedStatementVisitor(cfgStore, setters);
			List<Statement> defStatements = visitor.run(simpleName, maxDepth);
			return defStatements.stream().map(stmt -> getAssignmentExpression(stmt, simpleName))
					.filter(Objects::nonNull).toList();
		}

		return new ArrayList<>(0);
	}
}
