package sqlinspect.plugin.extractors;

public class HotspotDesc {

	private String signature;
	private int argnum;

	public static final String PARAM_SEP = "/";

	public HotspotDesc(String signature, int argnum) {
		this.signature = signature;
		this.argnum = argnum;
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature
	 *            the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return the argnum
	 */
	public int getArgnum() {
		return argnum;
	}

	/**
	 * @param argnum
	 *            the argnum to set
	 */
	public void setArgnum(int argnum) {
		this.argnum = argnum;
	}

	public static HotspotDesc parse(String s) {
		String[] a = s.trim().split(PARAM_SEP);
		int arg = a.length == 2 ? Integer.parseInt(a[1]) : 0;
		String desc = a[0];
		return new HotspotDesc(desc, arg);
	}

	@Override
	public String toString() {
		return signature + PARAM_SEP + argnum;
	}
}
