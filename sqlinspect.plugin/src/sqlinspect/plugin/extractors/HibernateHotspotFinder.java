package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;

public class HibernateHotspotFinder extends AbstractHotspotFinder {

	public static final String[] DEFAULT_HIBERNATE_QUERY_CREATE = {
			"org.hibernate.Session.createQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"org.hibernate.Session.createSQLQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"org.hibernate.SharedSessionContract.createQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"org.hibernate.SharedSessionContract.createSQLQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_HIBERNATE_QUERY_EXEC = { "org.hibernate.Query.executeUpdate()",
			"org.hibernate.Query.iterate()", "org.hibernate.Query.list()", "org.hibernate.Query.scroll()",
			"org.hibernate.Query.scrollMode(org.hibernate.ScrollMode)", "org.hibernate.Query.uniqueResult()",
			"org.hibernate.SQLQuery.executeUpdate()", "org.hibernate.SQLQuery.iterate()",
			"org.hibernate.SQLQuery.list()", "org.hibernate.SQLQuery.scroll()",
			"org.hibernate.SQLQuery.scrollMode(org.hibernate.ScrollMode)", "org.hibernate.SQLQuery.uniqueResult()" };
	public static final String[] DEFAULT_HIBERNATE_PSTMT_SETTER = { "org\\.hibernate\\.Query\\.set.*", // setInteger,
																										// setString,
																										// ...
			"org\\.hibernate\\.Query\\.add.*", // addScalar, ...
			"org\\.hibernate\\.SQLQuery\\.set.*", // setInteger, setString, ...
			"org\\.hibernate\\.SQLQuery\\.add.*" // addScalar ...
	};

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	public static List<HotspotDesc> getQueryCreateHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.HIBERNATE_QUERY_CREATE);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultQueryCreateHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_HIBERNATE_QUERY_CREATE) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getQueryExecHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.HIBERNATE_QUERY_EXEC);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	private static List<HotspotDesc> getDefaultQueryExecHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_HIBERNATE_QUERY_EXEC) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getDefaultHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getDefaultQueryCreateHotspots());
		ret.addAll(getDefaultQueryExecHotspots());
		return ret;
	}

	public static List<HotspotDesc> getHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getQueryCreateHotspots(project));
		ret.addAll(getQueryExecHotspots(project));
		return ret;
	}

	public static List<String> getDefaultSetters() {
		return Arrays.asList(DEFAULT_HIBERNATE_PSTMT_SETTER);
	}

	public static List<String> getSetters(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.HIBERNATE_PSTMT_SETTER);
		return PreferenceHelper.parseStringList(signaturePreference);
	}

	@Override
	public void setupWithProjectSettings(IProject project) {
		setDescriptors(getHotspots(project));
		int maxDepth = Activator.getDefault().getCurrentPreferenceStore(project)
				.getInt(PreferenceConstants.PSTMT_RESOLVER_MAXDEPTH);
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxDepth);
		psResolver.setDescriptors(getQueryCreateHotspots(project));
		psResolver.setSetters(getSetters(project));
		setPSResolver(psResolver);
	}

	@Override
	public void setupWithDefaults() {
		setDescriptors(getDefaultHotspots());
		int maxdepth = PreparedStatementResolver.DEFAULT_MAX_DEPTH;
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxdepth);
		psResolver.setDescriptors(getDefaultQueryCreateHotspots());
		psResolver.setSetters(getDefaultSetters());
		setPSResolver(psResolver);
	}
}
