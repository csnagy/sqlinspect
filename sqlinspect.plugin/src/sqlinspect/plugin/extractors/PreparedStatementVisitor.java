package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.BasicBlock;
import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGNodeFinder;
import sqlinspect.cfg.CFGRevBreadthFirst;
import sqlinspect.cfg.CFGStore;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.plugin.utils.BindingUtil;

public final class PreparedStatementVisitor extends CFGRevBreadthFirst {
	private static final Logger LOG = LoggerFactory.getLogger(PreparedStatementVisitor.class);

	private Optional<ListIterator<ASTNode>> startIt = Optional.empty();

	private final List<Statement> defs = new ArrayList<>();

	private final DefsVisitor defsVisitor = new DefsVisitor();

	private final CFGStore cfgStore;

	private final List<Pattern> setters;

	private IBinding toFind;

	public PreparedStatementVisitor(CFGStore cfgStore, List<Pattern> setters) {
		super();
		this.cfgStore = cfgStore;
		this.setters = setters;
	}

	public List<Statement> run(SimpleName sn, int maxDepth) {
		LOG.debug("Looking for the last definition(s) of {}", sn);

		defs.clear();

		toFind = sn.resolveBinding();
		if (toFind == null) {
			LOG.warn("Could not resolve binding of simple name {}", sn);
			return defs;
		}

		Statement stmt = ASTUtils.getParentStatement(sn);
		if (stmt == null) {
			LOG.warn("Could not get parent statement for simple name {}", sn);
			return defs;
		}

		MethodDeclaration method = ASTUtils.getParentMethodDeclaration(stmt);
		CFG cfg = cfgStore.get(method);

		CFGNodeFinder nodeFinder = new CFGNodeFinder(cfg, stmt);
		if (!nodeFinder.findStatement()) {
			LOG.error("Could not find the statement in the cfg!");
			return defs;
		}

		BasicBlock startBB = nodeFinder.getBlock();
		startIt = Optional.ofNullable(nodeFinder.getListIterator());
		if (startIt.isPresent()) {
			startIt.get().previous();
			super.run(startBB, maxDepth);
		}

		return defs;
	}

	@Override
	protected boolean visit(BasicBlock basicBlock) {
		ListIterator<ASTNode> it = startIt.isPresent() ? startIt.get()
				: basicBlock.nodes().listIterator(basicBlock.nodes().size());
		if (startIt.isPresent()) {
			startIt = Optional.empty();
		}
		while (it.hasPrevious()) {
			ASTNode node = it.previous();
			if (node instanceof Statement statement) {
				List<IBinding> stmtDefs = defsVisitor.run(statement).stream().map(SimpleName::resolveBinding)
						.filter(Objects::nonNull).toList();

				if (stmtDefs.contains(toFind) && !isPreparedStatementSetter(statement)) {
					defs.add(statement);
					return false;
				}
			}
		}
		return true;
	}

	private boolean isPreparedStatementSetter(Statement stmt) {
		if (stmt instanceof ExpressionStatement expressionStatement) {
			Expression expr = expressionStatement.getExpression();
			if (expr instanceof MethodInvocation methodInvocation) {
				IMethodBinding binding = methodInvocation.resolveMethodBinding();
				if (binding == null) {
					return false;
				}

				String signature = BindingUtil.qualifiedSignature(binding);
				for (Pattern pattern : setters) {
					Matcher matcher = pattern.matcher(signature);
					if (matcher.matches()) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
