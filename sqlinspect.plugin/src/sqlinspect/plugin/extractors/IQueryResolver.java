package sqlinspect.plugin.extractors;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;

import sqlinspect.plugin.model.QueryPart;

public interface IQueryResolver {
	String getName();

	List<QueryPart> resolve(Expression expr);
}
