package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;

public class JDBCHotspotFinder extends AbstractHotspotFinder {

	public static final String[] DEFAULT_JDBC_STMT_EXECUTE = {
			"java.sql.Statement.executeQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Statement.execute(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Statement.executeUpdate(java.lang.String)" + HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_JDBC_PSTMT_EXECUTE = { "java.sql.PreparedStatement.executeQuery()",
			"java.sql.PreparedStatement.execute()", "java.sql.PreparedStatement.executeUpdate()" };
	public static final String[] DEFAULT_JDBC_PSTMT = {
			"java.sql.Connection.prepareStatement(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Connection.prepareStatement(java.lang.String,int)" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Connection.prepareStatement(java.lang.String,int[])" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Connection.prepareStatement(java.lang.String,int,int)" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Connection.prepareStatement(java.lang.String,int,int,int)" + HotspotDesc.PARAM_SEP + "1",
			"java.sql.Connection.prepareStatement(java.lang.String,java.lang.String[])" + HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_JDBC_PSTMT_SETTER = { "java\\.sql\\.PreparedStatement\\.set.*" }; // setInt(),
																											// setString(),
																											// ...

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	private static List<HotspotDesc> getDefaultQueryHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_JDBC_STMT_EXECUTE) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getQueryHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JDBC_STMT_EXECUTE);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	private static List<HotspotDesc> getDefaultPstmtHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_JDBC_PSTMT_EXECUTE) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getPstmtHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JDBC_PSTMT_EXECUTE);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultPsHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_JDBC_PSTMT) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getPsHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JDBC_PSTMT);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getDefaultQueryHotspots());
		ret.addAll(getDefaultPstmtHotspots());
		return ret;
	}

	public static List<HotspotDesc> getHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getQueryHotspots(project));
		ret.addAll(getPstmtHotspots(project));
		return ret;
	}

	public static List<String> getDefaultSetters() {
		return Arrays.asList(DEFAULT_JDBC_PSTMT_SETTER);
	}

	public static List<String> getSetters(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JDBC_PSTMT_SETTER);
		return PreferenceHelper.parseStringList(signaturePreference);
	}

	@Override
	public void setupWithProjectSettings(IProject project) {
		setDescriptors(getHotspots(project));
		int maxDepth = Activator.getDefault().getCurrentPreferenceStore(project)
				.getInt(PreferenceConstants.PSTMT_RESOLVER_MAXDEPTH);
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxDepth);
		psResolver.setDescriptors(JDBCHotspotFinder.getPsHotspots(project));
		psResolver.setSetters(JDBCHotspotFinder.getSetters(project));
		setPSResolver(psResolver);
	}

	@Override
	public void setupWithDefaults() {
		setDescriptors(getDefaultHotspots());
		int maxdepth = PreparedStatementResolver.DEFAULT_MAX_DEPTH;
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxdepth);
		psResolver.setDescriptors(JDBCHotspotFinder.getDefaultPsHotspots());
		psResolver.setSetters(JDBCHotspotFinder.getDefaultSetters());
		setPSResolver(psResolver);
	}
}
