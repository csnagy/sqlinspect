package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;

public class AndroidHotspotFinder extends AbstractHotspotFinder {

	public static final String[] DEFAULT_ANDR_EXECSQL = {
			"android.database.sqlite.SQLiteDatabase.execSQL(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"android.database.sqlite.SQLiteDatabase.execSQL(java.lang.String,java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.DatabaseUtils.longForQuery(android.database.sqlite.SQLiteDatabase,java.lang.String,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "2",
			"android.database.DatabaseUtils.stringForQuery(android.database.sqlite.SQLiteDatabase,java.lang.String,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "2" };
	public static final String[] DEFAULT_ANDR_STMT_EXEC = {
			"android.database.sqlite.SQLiteStatement.execute()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.executeInsert()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.executeUpdateDelete()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.simpleQueryForBlobFileDescriptor()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.simpleQueryForLong()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.simpleQueryForString()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.DatabaseUtils.longForQuery(android.database.sqlite.SQLiteStatement,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.DatabaseUtils.stringForQuery(android.database.sqlite.SQLiteStatement,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_ANDR_COMPILE_STMT = {
			"android.database.sqlite.SQLiteDatabase.compileStatement(java.lang.String)" + HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_ANDR_CURSOR_RAWQERY = {
			"android.database.sqlite.SQLiteDatabase.rawQuery(java.lang.String,java.lang.String[],android.os.CancellationSignal)"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.sqlite.SQLiteDatabase.rawQuery(java.lang.String,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.sqlite.SQLiteDatabase.rawQueryWithFactory(android.database.sqlite.SQLiteDatabase.CursorFactory,java.lang.String,java.lang.String,java.lang.String[],java.lang.String,android.os.CancellationSignal)"
					+ HotspotDesc.PARAM_SEP + "2",
			"android.database.sqlite.SQLiteDatabase.rawQueryWithFactory(android.database.sqlite.SQLiteDatabase.CursorFactory,java.lang.String,java.lang.String,java.lang.String[],java.lang.String)"
					+ HotspotDesc.PARAM_SEP + "2" };

	public static final String[] DEFAULT_ANDR_PSTMT_SETTER = { "android\\.database\\.sqlite\\.SQLiteProgram\\.bind.*" }; // bindLong(),
																															// bindString(),
																															// ...

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	private static List<HotspotDesc> getDefaultQueryHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_ANDR_EXECSQL) {
			ret.add(HotspotDesc.parse(s));
		}
		for (String s : DEFAULT_ANDR_CURSOR_RAWQERY) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getQueryHotspots(IProject project) {
		String andrExecSQLPreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.ANDR_EXECSQL);
		List<String> andrExecSQL = PreferenceHelper.parseStringList(andrExecSQLPreference);

		String cursorRawQueryPreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.ANDR_CURSOR_RAWQUERY);
		List<String> cursorRawQuery = PreferenceHelper.parseStringList(cursorRawQueryPreference);

		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(andrExecSQL.stream().map(HotspotDesc::parse).toList());
		ret.addAll(cursorRawQuery.stream().map(HotspotDesc::parse).toList());

		return ret;
	}

	private static List<HotspotDesc> getDefaultPstmtHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_ANDR_STMT_EXEC) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getPstmtHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.ANDR_STMT_EXEC);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultPsHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_ANDR_COMPILE_STMT) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getPsHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.ANDR_COMPILE_STMT);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getDefaultQueryHotspots());
		ret.addAll(getDefaultPstmtHotspots());
		return ret;
	}

	public static List<HotspotDesc> getHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getQueryHotspots(project));
		ret.addAll(getPstmtHotspots(project));
		return ret;
	}

	public static List<String> getDefaultSetters() {
		return Arrays.asList(DEFAULT_ANDR_PSTMT_SETTER);
	}

	public static List<String> getSetters(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.ANDR_PSTMT_SETTER);
		return PreferenceHelper.parseStringList(signaturePreference);
	}

	@Override
	public void setupWithProjectSettings(IProject project) {
		setDescriptors(getHotspots(project));
		int maxDepth = Activator.getDefault().getCurrentPreferenceStore(project)
				.getInt(PreferenceConstants.PSTMT_RESOLVER_MAXDEPTH);
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxDepth);
		psResolver.setDescriptors(AndroidHotspotFinder.getPsHotspots(project));
		psResolver.setSetters(getSetters(project));
		setPSResolver(psResolver);
	}

	@Override
	public void setupWithDefaults() {
		setDescriptors(getDefaultHotspots());
		int maxdepth = PreparedStatementResolver.DEFAULT_MAX_DEPTH;
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxdepth);
		psResolver.setDescriptors(getDefaultPsHotspots());
		psResolver.setSetters(getDefaultSetters());
		setPSResolver(psResolver);
	}
}
