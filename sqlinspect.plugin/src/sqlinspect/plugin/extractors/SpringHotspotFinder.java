package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;

public class SpringHotspotFinder extends AbstractHotspotFinder {

	public static final String[] DEFAULT_SPRING_HIBERNATETEMPLATE_HOTSPOTS1 = {
			"org.springframework.orm.hibernate.HibernateTemplate.find(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.find(java.lang.String, java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.find(java.lang.String, java.lang.Object[], net.sf.hibernate.type.Type[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.find(java.lang.String, java.lang.Object, net.sf.hibernate.type.Type)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String[], java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String[], java.lang.Object[], net.sf.hibernate.type.Type[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String, java.lang.Object, net.sf.hibernate.type.Type)"
					+ HotspotDesc.PARAM_SEP + "1",

			"org.springframework.orm.hibernate3.HibernateTemplate.find(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.find(java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.find(java.lang.String, java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String[], java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.findByValueBeans(java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",

			"org.springframework.orm.hibernate.HibernateTemplate.iterate(java.lang.String)" + HotspotDesc.PARAM_SEP
					+ "1",
			"org.springframework.orm.hibernate.HibernateTemplate.iterate(java.lang.String, java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.iterate(java.lang.String, java.lang.Object[], net.sf.hibernate.type.Type[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate.HibernateTemplate.iterate(java.lang.String, java.lang.Object, net.sf.hibernate.type.Type)"
					+ HotspotDesc.PARAM_SEP + "1",

			"org.springframework.orm.hibernate3.HibernateTemplate.iterate(java.lang.String)" + HotspotDesc.PARAM_SEP
					+ "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.iterate(java.lang.String, java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate3.HibernateTemplate.iterate(java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1" };

	public static final String[] DEFAULT_SPRING_HIBERNATETEMPLATE_HOTSPOTS2 = {
			"org.springframework.orm.hibernate4.HibernateTemplate.find(java.lang.String, java.lang.Object...)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate4.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String[], java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate4.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate4.HibernateTemplate.findByValueBeans(java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",

			"org.springframework.orm.hibernate5.HibernateTemplate.find(java.lang.String, java.lang.Object...)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate5.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String[], java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate5.HibernateTemplate.findByNamedParam(java.lang.String, java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",
			"org.springframework.orm.hibernate5.HibernateTemplate.findByValueBeans(java.lang.String, java.lang.Object)"
					+ HotspotDesc.PARAM_SEP + "1",

			"org.springframework.orm.hibernate4.HibernateTemplate.iterate(java.lang.String, java.lang.Object...)"
					+ HotspotDesc.PARAM_SEP + "1",

			"org.springframework.orm.hibernate5.HibernateTemplate.iterate(java.lang.String, java.lang.Object...)"
					+ HotspotDesc.PARAM_SEP + "1" };

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	public static List<HotspotDesc> getDAOHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.SPRING_HIBERNATETEMPLATE_HOTSPOTS);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultDAOHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(Arrays.asList(DEFAULT_SPRING_HIBERNATETEMPLATE_HOTSPOTS1).stream().map(HotspotDesc::parse)
				.toList());
		ret.addAll(Arrays.asList(DEFAULT_SPRING_HIBERNATETEMPLATE_HOTSPOTS2).stream().map(HotspotDesc::parse)
				.toList());
		return ret;
	}

	public static List<HotspotDesc> getDefaultHotspots() {
		return getDefaultDAOHotspots();
	}

	public static List<HotspotDesc> getHotspots(IProject project) {
		return getDAOHotspots(project);
	}

	@Override
	public void setupWithProjectSettings(IProject project) {
		setDescriptors(getHotspots(project));
	}

	@Override
	public void setupWithDefaults() {
		setDescriptors(getDefaultHotspots());
	}

	public static List<String> getDefaultSpringHibernateTemplateHotspots() {
		List<String> ret = new ArrayList<>();
		ret.addAll(Arrays.asList(SpringHotspotFinder.DEFAULT_SPRING_HIBERNATETEMPLATE_HOTSPOTS1));
		ret.addAll(Arrays.asList(SpringHotspotFinder.DEFAULT_SPRING_HIBERNATETEMPLATE_HOTSPOTS2));
		return ret;
	}
}
