package sqlinspect.plugin.extractors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;

public class JPAHotspotFinder extends AbstractHotspotFinder {

	public static final String[] DEFAULT_JPA_QUERY_CREATE = {
			"javax.persistence.EntityManager.createQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"javax.persistence.EntityManager.createNativeQuery(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"javax.persistence.EntityManager.createNativeQuery(java.lang.String, java.lang.Class)"
					+ HotspotDesc.PARAM_SEP + "1",
			"javax.persistence.EntityManager.createNativeQuery(java.lang.String, java.lang.String)"
					+ HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_JPA_QUERY_EXEC = { "javax.persistence.Query.executeUpdate()",
			"javax.persistence.Query.getResultList()", "javax.persistence.Query.getSingleResult()",
			"javax.persistence.TypedQuery.executeUpdate()", "javax.persistence.TypedQuery.getResultList()",
			"javax.persistence.TypedQuery.getSingleResult()" };
	public static final String[] DEFAULT_JPA_PSTMT_SETTER = { "javax\\.persistence\\.Query\\.set.*" }; // setMaxResults(),
																										// setParameter(),
																										// ...

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	public static List<HotspotDesc> getQueryCreateHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JPA_QUERY_CREATE);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	public static List<HotspotDesc> getDefaultQueryCreateHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_JPA_QUERY_CREATE) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getQueryExecHotspots(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JPA_QUERY_EXEC);
		List<String> signatures = PreferenceHelper.parseStringList(signaturePreference);
		return signatures.stream().map(HotspotDesc::parse).toList();
	}

	private static List<HotspotDesc> getDefaultQueryExecHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_JPA_QUERY_EXEC) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getDefaultHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getDefaultQueryCreateHotspots());
		ret.addAll(getDefaultQueryExecHotspots());
		return ret;
	}

	public static List<HotspotDesc> getHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		ret.addAll(getQueryCreateHotspots(project));
		ret.addAll(getQueryExecHotspots(project));
		return ret;
	}

	public static List<String> getDefaultSetters() {
		return Arrays.asList(DEFAULT_JPA_PSTMT_SETTER);
	}

	public static List<String> getSetters(IProject project) {
		String signaturePreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JPA_PSTMT_SETTER);
		return PreferenceHelper.parseStringList(signaturePreference);
	}

	@Override
	public void setupWithProjectSettings(IProject project) {
		setDescriptors(getHotspots(project));
		int maxDepth = Activator.getDefault().getCurrentPreferenceStore(project)
				.getInt(PreferenceConstants.PSTMT_RESOLVER_MAXDEPTH);
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxDepth);
		psResolver.setDescriptors(getQueryCreateHotspots(project));
		psResolver.setSetters(getSetters(project));
		setPSResolver(psResolver);
	}

	@Override
	public void setupWithDefaults() {
		setDescriptors(getDefaultHotspots());
		int maxdepth = PreparedStatementResolver.DEFAULT_MAX_DEPTH;
		PreparedStatementResolver psResolver = new PreparedStatementResolver(cfgStore, maxdepth);
		psResolver.setDescriptors(getDefaultQueryCreateHotspots());
		psResolver.setSetters(getDefaultSetters());
		setPSResolver(psResolver);
	}
}
