package sqlinspect.plugin.extractors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slice {
	private static final Logger LOG = LoggerFactory.getLogger(Slice.class);

	private final Map<SimpleName, Expression> defsOf = new HashMap<>();

	private final Map<SimpleName, SimpleName> refsTo = new HashMap<>();

	// undefined variables
	private final Set<SimpleName> toFindDefOf = new HashSet<>();

	public Slice() {
		// empty slice
	}

	public Slice(Map<SimpleName, Expression> defsOf, Map<SimpleName, SimpleName> refsTo, Set<SimpleName> toFindDefOf) {
		this.defsOf.putAll(defsOf);
		this.refsTo.putAll(refsTo);
		this.toFindDefOf.addAll(toFindDefOf);
	}

	public Slice(Slice other) {
		this.defsOf.putAll(other.getDefsOf());
		this.refsTo.putAll(other.getRefsTo());
		this.toFindDefOf.addAll(other.getToFindDefOf());
	}

	public void combine(Slice other) {
		if (LOG.isTraceEnabled()) {
			LOG.trace("Combining slices:");
			this.dump();
			other.dump();
		}
		this.defsOf.putAll(other.getDefsOf());
		this.refsTo.putAll(other.getRefsTo());
		this.toFindDefOf.addAll(other.getToFindDefOf());
	}

	public Map<SimpleName, Expression> getDefsOf() {
		return defsOf;
	}

	public Map<SimpleName, SimpleName> getRefsTo() {
		return refsTo;
	}

	public Set<SimpleName> getToFindDefOf() {
		return toFindDefOf;
	}

	public void dump() {
		LOG.trace("Definitions: ");
		for (Entry<SimpleName, Expression> e : defsOf.entrySet()) {
			LOG.trace(" {} ({})-> {} ({})", e.getKey(), e.getKey().getStartPosition(), e.getValue(),
					e.getValue().getStartPosition());
		}
		LOG.trace("References: ");
		for (Entry<SimpleName, SimpleName> e : refsTo.entrySet()) {
			LOG.trace(" {} ({})-> {} ({})", e.getKey(), e.getKey().getStartPosition(), e.getValue(),
					e.getValue().getStartPosition());
		}
	}
}
