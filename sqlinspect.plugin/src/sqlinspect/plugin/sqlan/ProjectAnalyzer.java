package sqlinspect.plugin.sqlan;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.HotspotFinderFactory;
import sqlinspect.plugin.extractors.IHotspotFinder;
import sqlinspect.plugin.extractors.IQueryResolver;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.extractors.LoopHandling;
import sqlinspect.plugin.extractors.QueryResolverFactory;
import sqlinspect.plugin.metrics.java.AbstractJavaMetric;
import sqlinspect.plugin.metrics.java.JavaMetricDesc;
import sqlinspect.plugin.metrics.java.NumberOfColumnAccesses;
import sqlinspect.plugin.metrics.java.NumberOfDeletes;
import sqlinspect.plugin.metrics.java.NumberOfHotspots;
import sqlinspect.plugin.metrics.java.NumberOfInserts;
import sqlinspect.plugin.metrics.java.NumberOfQueries;
import sqlinspect.plugin.metrics.java.NumberOfSelects;
import sqlinspect.plugin.metrics.java.NumberOfTableAccesses;
import sqlinspect.plugin.metrics.java.NumberOfUpdates;
import sqlinspect.plugin.metrics.sql.SQLMetricDesc;
import sqlinspect.plugin.metrics.sql.SQLMetrics;
import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.SmellRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.smells.android.common.AndroidSmellDetector;
import sqlinspect.plugin.smells.android.detectors.ExecSQLReturnsData;
import sqlinspect.plugin.smells.android.detectors.ExecuteInsertShouldSendInsert;
import sqlinspect.plugin.smells.android.detectors.MoreThanOneColumnInSQLiteStatement;
import sqlinspect.plugin.smells.android.detectors.MoreThanOneStatement;
import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.detectors.AmbiguousGroups;
import sqlinspect.plugin.smells.sql.detectors.FearOfTheUnknown;
import sqlinspect.plugin.smells.sql.detectors.ImplicitColumns;
import sqlinspect.plugin.smells.sql.detectors.RandomSelection;
import sqlinspect.plugin.sqlan.taa.TAA;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.plugin.utils.EclipseProjectUtil;
import sqlinspect.plugin.utils.Filter;
import sqlinspect.sql.DBHandler;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.traversal.MultiPreOrderTraversal;
import sqlinspect.sql.asg.traversal.PreOrderTraversal;
import sqlinspect.sql.parser.SQLParser;
import sqlinspect.sql.parser.VisitorExtraEdges;

public final class ProjectAnalyzer {
	private static final Logger LOG = LoggerFactory.getLogger(ProjectAnalyzer.class);
	private static final char SEMI = ';';

	private final Project project;
	private final List<IHotspotFinder> hsfs = new ArrayList<>();
	private IQueryResolver resolver;

	@Inject
	private HotspotRepository hotspotRepository;
	@Inject
	private QueryRepository queryRepository;
	@Inject
	private MetricRepository metricRepository;
	@Inject
	private SmellRepository smellRepository;
	@Inject
	private TableAccessRepository tableAccessRepository;
	@Inject
	private ASGRepository asgRepository;

	@Inject
	private IEclipseContext context;

	public ProjectAnalyzer(Project project) {
		this.project = project;
	}

	public void clear() {
		hotspotRepository.clear(project);
		queryRepository.clear(project);
		metricRepository.clear(project);
		smellRepository.clear(project);
		tableAccessRepository.clear(project);
	}

	public void initAnalyzer() {
		LOG.info("Initializing analyzer");
		initHotspotFinders(project);
		initQueryResolver(project);
	}

	private void initHotspotFinders(Project project) {
		hsfs.clear();

		String projectHsfPreference = Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getString(PreferenceConstants.HOTSPOT_FINDERS);
		List<String> projectHsf = PreferenceHelper.parseStringList(projectHsfPreference);

		for (String hsfStr : projectHsf) {
			IHotspotFinder hsf = HotspotFinderFactory.create(context, hsfStr);
			hsf.setupWithProjectSettings(project.getIProject());
			hsfs.add(hsf);
		}
	}

	private IQueryResolver initQueryResolver(Project project) {
		IProject iproj = project.getIProject();
		String projectResolverPreference = Activator.getDefault().getCurrentPreferenceStore(iproj)
				.getString(PreferenceConstants.STRING_RESOLVER);
		resolver = QueryResolverFactory.create(context, projectResolverPreference);

		if (resolver instanceof InterQueryResolver interQueryResolver) {
			String loopHandlingPreference = Activator.getDefault().getCurrentPreferenceStore(iproj)
					.getString(PreferenceConstants.INTERSR_LOOPHANDLING);
			int maxCFGDepthPreference = Activator.getDefault().getCurrentPreferenceStore(iproj)
					.getInt(PreferenceConstants.INTERSR_MAX_CFG_DEPTH);
			int maxCallDepthPreference = Activator.getDefault().getCurrentPreferenceStore(iproj)
					.getInt(PreferenceConstants.INTERSR_MAX_CALL_DEPTH);
			interQueryResolver.setLoopHandling(LoopHandling.getLoopHandling(loopHandlingPreference));
			interQueryResolver.setMaxCFGDepth(maxCFGDepthPreference);
			interQueryResolver.setMaxCallDepth(maxCallDepthPreference);
		}

		return resolver;
	}

	private static Optional<Filter> loadFilter(Project project) {
		IProject iproj = project.getIProject();
		String filterFilePreference = Activator.getDefault().getCurrentPreferenceStore(iproj)
				.getString(PreferenceConstants.FILTER_FILE);
		if (!filterFilePreference.isEmpty()) {
			LOG.info("Loading filter: {}", iproj.getName());
			try {
				return Optional.of(Filter.loadFromFile(filterFilePreference));
			} catch (IOException e) {
				LOG.error("Could not load filter", e);
			}
		}
		return Optional.empty();
	}

	private boolean filtered(Filter filter, ICompilationUnit unit) {
		IPath path = EclipseProjectUtil.getOriginalPathOfFile(project.getIProject(), unit.getPath());
		return !filter.match(path);
	}

	public void extractHotspots(SubMonitor monitor) throws CoreException {
		extractHotspotsOfJavaElement(null, monitor);
	}

	public void extractHotspotsOfJavaElement(IJavaElement javaElement, SubMonitor monitor) throws CoreException {
		LOG.info("Analyzing project: {}", project);

		Optional<Filter> filter = loadFilter(project);
		IJavaProject javaProject = project.getIJavaProject();
		IPackageFragment[] packages = javaProject.getPackageFragments();
		monitor.beginTask("Extracting hotspots", packages.length);
		for (IPackageFragment p : packages) {
			if (p.getKind() == IPackageFragmentRoot.K_SOURCE
					&& ((javaElement instanceof IPackageFragment && p.equals(javaElement))
							|| !(javaElement instanceof IPackageFragment))) {
				SubMonitor subMonitor = monitor.split(1);
				subMonitor.beginTask("Analyze package: " + p.getElementName(), p.getCompilationUnits().length);
				for (ICompilationUnit unit : p.getCompilationUnits()) {
					if ((javaElement instanceof ICompilationUnit && unit.equals(javaElement))
							|| !(javaElement instanceof ICompilationUnit)) {
						if (filter.isEmpty() || !filtered(filter.get(), unit)) {
							extractHotspotsOfCompilationUnit(unit);
							subMonitor.worked(1);
						} else {
							LOG.debug("Filtered: {}",
									EclipseProjectUtil.getOriginalPathOfFile(project.getIProject(), unit.getPath()));
						}
					}
				}
			} else {
				monitor.worked(1);
			}
		}
	}

	private void extractHotspotsOfCompilationUnit(ICompilationUnit unit) {
		for (IHotspotFinder hsf : hsfs) {
			LOG.info("Analyzing ({}, {}) ICompilationUnit: {} ", hsf.getClass().getSimpleName(),
					resolver.getClass().getSimpleName(), unit.getElementName());

			Collection<Hotspot> hotspots = hsf.extract(unit, resolver);
			hotspotRepository.addHotspots(project, hotspots);
		}
	}

	public void analyze(CompilationUnit unit, IPath path, IHotspotFinder hsf, IQueryResolver resolver) {
		LOG.info("Analyzing CompilationUnit: {}", path);

		Collection<Hotspot> hotspots = hsf.extract(unit, path, resolver);
		hotspotRepository.addHotspots(project, hotspots);
	}

	public void parseSchemaFromFiles(List<String> schemaFiles) {
		String schemaFilesString = schemaFiles.stream().collect(Collectors.joining(","));
		LOG.debug("Parsing schema files: {}", schemaFilesString);
		schemaFiles.forEach(schemaFile -> parse(new File(schemaFile)));
	}

	private void parse(File file) {
		SQLParser parser;
		try {
			parser = SQLParser.create(getSQLDialect(), file, getASG());
			parser.parse();
		} catch (Exception e) {
			LOG.error("Error occured while parsing strings: ", e);
		}
	}

	private SQLDialect getSQLDialect() {
		String dialectString = Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getString(PreferenceConstants.DIALECT);
		return SQLDialect.getSQLDialect(dialectString);
	}

	private String getDBUser() {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getString(PreferenceConstants.DB_USER);
	}

	private String getDBPassword() {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getString(PreferenceConstants.DB_PASSWORD);
	}

	private String getDBUrl() {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getString(PreferenceConstants.DB_URL);
	}

	private boolean isLoadSchemaFromDB() {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getBoolean(PreferenceConstants.LOAD_SCHEMA_FROM_DB);
	}

	private static String addSemicolonIfMissing(String queryString) {
		String s = queryString.trim();
		if (s.length() > 0 && s.charAt(s.length() - 1) != SEMI) {
			return s + SEMI;
		} else {
			return s;
		}
	}

	private static String getQueryLocationFile(Query query) {
		return EclipseProjectUtil
				.getOriginalPathOfFile(query.getHotspot().getProject().getIProject(), query.getHotspot().getPath())
				.toString();
	}

	private static int getQueryLocationRow(Query query) {
		return ASTUtils.getLineNumber(query.getHotspot().getExec()) - 1;
	}

	private record QueryStringWithLocation(Query query, String string, String locationFile, int locationRow) {
	}

	public void parseQueries(SubMonitor monitor) {
		monitor.beginTask("Parse queries", 100);

		if (isLoadSchemaFromDB() && getDBUrl() != null && !getDBUrl().isEmpty()) {
			DBHandler dbHandler = new DBHandler(getDBUrl(), getDBUser(), getDBPassword());
			dbHandler.connect();
			parseDBSchema(dbHandler);
			dbHandler.close();
		}

		List<Query> queries = hotspotRepository.getQueries(project);
		List<QueryStringWithLocation> queriesWithLocations = queries.stream().map(q -> new QueryStringWithLocation(q,
				addSemicolonIfMissing(q.getValue()), getQueryLocationFile(q), getQueryLocationRow(q))).toList();
		Map<Query, List<Statement>> parsedQueries = parseQueriesWithLocations(queriesWithLocations);
		monitor.worked(90);

		for (Map.Entry<Query, List<Statement>> e : parsedQueries.entrySet()) {
			Query q = e.getKey();
			q.setStatements(e.getValue());
			for (Statement stmt : e.getValue()) {
				queryRepository.addQuery(project, stmt, q);
			}
		}

		if (!parsedQueries.isEmpty()) {
			VisitorExtraEdges.resolveIdentifiers(getASG(), asgRepository.getSymbolTable(project));
		}
		monitor.worked(10);
	}

	private Map<Query, List<Statement>> parseQueriesWithLocations(
			List<QueryStringWithLocation> queryStringsWithLocation) {
		Map<Query, List<Statement>> ret = new HashMap<>();
		int i = 0;
		for (QueryStringWithLocation q : queryStringsWithLocation) {
			int internalId = i + 1;
			List<Statement> statements = parseSQL(q.string, getSQLDialect(), q.locationFile, q.locationRow, internalId, getASG());
			ret.put(q.query, statements);
		}
		return ret;
	}

	public static List<Statement> parseSQL(String str, SQLDialect dialect) {
		return parseSQL(addSemicolonIfMissing(str), dialect, "", 0, 0, new ASG());
	}

	public static List<Statement> parseSQL(String str, SQLDialect dialect, ASG asg) {
		return parseSQL(addSemicolonIfMissing(str), dialect, "", 0, 0, asg);
	}

	private static List<Statement> parseSQL(String stringToParse, SQLDialect dialect, String locationFile, int locationRow, int internalId, ASG asg) {
		ArrayList<Statement> ret = new ArrayList<>();
		SQLParser parser = SQLParser.create(dialect, stringToParse, asg);
		if (locationFile != null) {
			parser.setInputName(locationFile);
		}
		parser.setInitialRow(locationRow);
		parser.setInternalId(internalId);

		ret.addAll(parser.parseStatements());
		return ret;
	}

	public void runTAA() {
		runTAA(getASG(), tableAccessRepository.getTableAccesses(project),
				tableAccessRepository.getColumnAccesses(project));
	}

	public void runSQLMetrics() {
		runSQLMetrics(getASG(), metricRepository.getSQLMetrics(project));
	}

	private ASG getASG() {
		return asgRepository.getASG(project);
	}
	
	private void parseDBSchema(DBHandler dbHandler) {
		LOG.debug("Parsing Schema from database");
		String schema = dbHandler.getDBSchema();
		LOG.debug("Schema: {}", schema);
		parseSQL(schema, getSQLDialect(), getASG());
		LOG.debug("Done.");
	}

	public void runSQLSmellDetectors() {
		ASG asg = getASG();
		SQLDialect dialect = getSQLDialect();
		Set<SQLSmell> smells = smellRepository.getSQLSmells(project);
		runSQLSmellDetectors(asg, dialect, smells, null);
	}

	public static void runSQLSmellDetectors(ASG asg, SQLDialect dialect, Set<SQLSmell> smells, DBHandler dbHandler) {
		LOG.debug("Run code smells.");

		FearOfTheUnknown fearVisitor = new FearOfTheUnknown();
		if (dbHandler != null) {
			fearVisitor.setDBHandler(dbHandler);
		}
		AmbiguousGroups abgVisitor = new AmbiguousGroups(dialect);
		RandomSelection randomVisitor = new RandomSelection(dialect);
		ImplicitColumns implicitColsVisitor = new ImplicitColumns();

		MultiPreOrderTraversal traversal = new MultiPreOrderTraversal();
		traversal.addVisitor(fearVisitor);
		traversal.addVisitor(abgVisitor);
		traversal.addVisitor(randomVisitor);
		traversal.addVisitor(implicitColsVisitor);

		traversal.traverse(asg.getRoot());

		smells.addAll(fearVisitor.getSmells());
		smells.addAll(abgVisitor.getSmells());
		smells.addAll(randomVisitor.getSmells());
		smells.addAll(implicitColsVisitor.getSmells());

		LOG.debug("Done");
	}

	public static void runTAA(ASG asg, Map<Statement, Map<Table, Integer>> tableAcc,
			Map<Statement, Map<Column, Integer>> columnAcc) {
		LOG.debug("Run TAA.");

		TAA tableAccessVisitor = new TAA();
		PreOrderTraversal traversal = new PreOrderTraversal(tableAccessVisitor);
		traversal.traverse(asg.getRoot());
		tableAcc.putAll(tableAccessVisitor.getTableAcc());
		columnAcc.putAll(tableAccessVisitor.getColumnAcc());

		LOG.debug("Done.");
	}

	private static void runSQLMetrics(ASG asg, Map<Statement, Map<SQLMetricDesc, Integer>> metrics) {
		LOG.debug("Run SQL Metrics.");

		SQLMetrics sqlMetrics = new SQLMetrics(asg);
		sqlMetrics.calculate();
		metrics.putAll(sqlMetrics.getMetrics());

		LOG.debug("Done.");
	}

	public void runAndroidSmellDetectors() {
		LOG.debug("Run Android Smell Detectors.");

		final List<AndroidSmellDetector> detectors = new ArrayList<>();
		detectors.add(new MoreThanOneStatement());
		detectors.add(new ExecSQLReturnsData());
		detectors.add(new ExecuteInsertShouldSendInsert());
		detectors.add(new MoreThanOneColumnInSQLiteStatement());

		for (AndroidSmellDetector asd : detectors) {
			ContextInjectionFactory.inject(asd, context);
		}

		detectors.stream().forEach(detector -> {
			Set<AndroidSmell> detectedSmells = detector.execute(project);
			smellRepository.getAndroidSmells(project).addAll(detectedSmells);
		});

		LOG.debug("Done.");
	}

	public void runJavaMetrics() {
		LOG.debug("Calculating metrics for project: {}", project.getName());

		List<AbstractJavaMetric> toCalculate = new ArrayList<>();
		toCalculate.add(new NumberOfHotspots());
		toCalculate.add(new NumberOfQueries());
		toCalculate.add(new NumberOfInserts());
		toCalculate.add(new NumberOfDeletes());
		toCalculate.add(new NumberOfUpdates());
		toCalculate.add(new NumberOfSelects());
		toCalculate.add(new NumberOfTableAccesses());
		toCalculate.add(new NumberOfColumnAccesses());

		for (AbstractJavaMetric am : toCalculate) {
			ContextInjectionFactory.inject(am, context);
		}

		for (AbstractJavaMetric jm : toCalculate) {
			Map<IJavaElement, Map<JavaMetricDesc, Integer>> results = jm.calculate(project);
			metricRepository.addJavaMetrics(project, results);
		}

		LOG.debug("Calculating metrics done.");
	}
}
