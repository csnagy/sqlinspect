package sqlinspect.plugin.sqlan.taa;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.util.ASGUtils;

public class JSONTAAReporter implements ITAAReporter {
	private static final Logger LOG = LoggerFactory.getLogger(JSONTAAReporter.class);

	private static final String STATEMENT_ATTR_ID = "Id";
	private static final String STATEMENT_ATTR_PATH = "Path";
	private static final String STATEMENT_ATTR_LINE = "Line";

	private final ASG asg;

	private final Map<Statement, Map<Table, Integer>> tableAcc;
	private final Map<Statement, Map<Column, Integer>> columnAcc;

	private final ObjectMapper mapper = new ObjectMapper();

	public JSONTAAReporter(ASG asg, Map<Statement, Map<Table, Integer>> tableAcc,
			Map<Statement, Map<Column, Integer>> columnAcc) {
		this.asg = asg;
		this.tableAcc = tableAcc;
		this.columnAcc = columnAcc;
	}

	@Override
	public void writeReport(File file) {
		ObjectNode rootNode = mapper.createObjectNode();
		ArrayNode dbArr = mapper.createArrayNode();

		for (Database db : asg.getRoot().getDatabases()) {
			ObjectNode dbNode = mapper.createObjectNode();
			ArrayNode stmtArr = mapper.createArrayNode();
			for (Statement stmt : db.getStatements()) {
				stmtArr.add(writeAccesses(stmt));

			}
			dbNode.set("Statements", stmtArr);
			dbArr.add(dbNode);
		}

		rootNode.set("Databases", dbArr);

		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(file, rootNode);
		} catch (IOException e) {
			LOG.error("Exception: ", e);
		}
	}

	private ObjectNode writeAccesses(Statement stmt) {
		ObjectNode node = mapper.createObjectNode();
		node.put(STATEMENT_ATTR_ID, stmt.getId());
		String lpath = stmt.getPath();
		if (lpath != null) {
			node.put(STATEMENT_ATTR_PATH, lpath);
			node.put(STATEMENT_ATTR_LINE, stmt.getStartLine());
		}

		node.set("TableAccesses", writeTableAccesses(stmt));
		node.set("ColumnAccesses", writeColumnAccesses(stmt));
		return node;
	}

	private ArrayNode writeColumnAccesses(Statement stmt) {
		ArrayNode arr = mapper.createArrayNode();
		Map<Column, Integer> cac = columnAcc.get(stmt);
		if (cac != null) {
			cac.keySet().stream().map(ASGUtils::getQualifiedName).sorted().forEach(arr::add);
		}
		return arr;
	}

	private ArrayNode writeTableAccesses(Statement stmt) {
		ArrayNode arr = mapper.createArrayNode();
		Map<Table, Integer> tac = tableAcc.get(stmt);
		if (tac != null) {
			tac.keySet().stream().map(ASGUtils::getQualifiedName).sorted().forEach(arr::add);
		}
		return arr;
	}
}
