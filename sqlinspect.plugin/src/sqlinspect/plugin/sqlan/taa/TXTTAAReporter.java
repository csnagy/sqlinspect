package sqlinspect.plugin.sqlan.taa;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.util.ASGUtils;

public class TXTTAAReporter implements ITAAReporter {
	private static final Logger LOG = LoggerFactory.getLogger(TXTTAAReporter.class);

	private final ASG asg;
	private final Map<Statement, Map<Table, Integer>> tableAcc;
	private final Map<Statement, Map<Column, Integer>> columnAcc;

	public TXTTAAReporter(ASG asg, Map<Statement, Map<Table, Integer>> tableAcc,
			Map<Statement, Map<Column, Integer>> columnAcc) {
		this.asg = asg;
		this.tableAcc = tableAcc;
		this.columnAcc = columnAcc;
	}

	@Override
	public void writeReport(File file) {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(file.toPath()),
				StandardCharsets.UTF_8); BufferedWriter bw = new BufferedWriter(ow);) {
			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeAccesses(bw, stmt);
				}
			}
		} catch (IOException e) {
			LOG.error("IO error while writing report!", e);
		}
	}

	private void writeAccesses(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("Statement (" + stmt.getId() + ")");
		String lpath = stmt.getPath();
		if (lpath != null) {
			bw.write(" at " + lpath + '(' + stmt.getStartLine() + ")");
		}
		bw.write(" accesses:\n");
		writeTableAccesses(bw, stmt);
		writeColumnAccesses(bw, stmt);
	}

	private void writeColumnAccesses(BufferedWriter bw, Statement stmt) throws IOException {
		Map<Column, Integer> cac = columnAcc.get(stmt);
		bw.write("  Columns:\n");
		if (cac != null) {
			for (Column col : cac.keySet()) {
				bw.write("    " + ASGUtils.getQualifiedName(col) + "\n");
			}
		}
	}

	private void writeTableAccesses(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("  Tables:\n");
		Map<Table, Integer> tac = tableAcc.get(stmt);
		if (tac != null) {
			for (Table tab : tac.keySet()) {
				bw.write("    " + ASGUtils.getQualifiedName(tab) + "\n");
			}
		}
	}
}
