package sqlinspect.plugin.sqlan.taa;

import java.io.File;
import java.util.Map;

import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;

public interface ITAAReporter {
	static final String XML = "xml";
	static final String JSON = "json";
	static final String TXT = "txt";

	public void writeReport(File reportFile);

	public static ITAAReporter create(ASG asg, Map<Statement, Map<Table, Integer>> tableAccesses, Map<Statement, Map<Column, Integer>> columnAccesses, String format) {
		if (XML.equalsIgnoreCase(format)) {
			return new XMLTAAReporter(asg, tableAccesses, columnAccesses);
		} else if (JSON.equalsIgnoreCase(format)) {
			return new JSONTAAReporter(asg, tableAccesses, columnAccesses);
		} else {
			return new TXTTAAReporter(asg, tableAccesses, columnAccesses);
		}
	}
}
