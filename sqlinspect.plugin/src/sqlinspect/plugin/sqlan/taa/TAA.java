package sqlinspect.plugin.sqlan.taa;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.expr.Alias;
import sqlinspect.sql.asg.expr.Asterisk;
import sqlinspect.sql.asg.expr.BinaryExpression;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.Join;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.kinds.BinaryExpressionKind;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.AlterTable;
import sqlinspect.sql.asg.statm.CreateTable;
import sqlinspect.sql.asg.statm.Delete;
import sqlinspect.sql.asg.statm.DropTable;
import sqlinspect.sql.asg.statm.Insert;
import sqlinspect.sql.asg.statm.Select;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.statm.Update;
import sqlinspect.sql.asg.visitor.Visitor;

public class TAA extends Visitor {
	private static final Logger LOG = LoggerFactory.getLogger(TAA.class);
	private Optional<Statement> actualStmt = Optional.empty();
	private final Map<Statement, Map<Column, Integer>> columnAcc = new HashMap<>();
	private final Map<Statement, Map<Table, Integer>> tableAcc = new HashMap<>();

	public static class AccessMode {
		public static final int CREATE = 1;
		public static final int READ = 2;
		public static final int UPDATE = 4;
		public static final int DELETE = 8;
		public static final int IMPLICIT = 16;

		private AccessMode() {
		}
	}

	public Map<Statement, Map<Column, Integer>> getColumnAcc() {
		return columnAcc;
	}

	public Map<Statement, Map<Table, Integer>> getTableAcc() {
		return tableAcc;
	}

	public void visit(Statement n) {
		actualStmt = Optional.of(n);
	}

	public void visitEnd(Statement n) {
		actualStmt = Optional.empty();
	}

	@Override
	public void visit(Insert n) {
		visit((Statement) n);
		Expression tab = n.getTable();
		if (tab != null) {
			addAllColumsnOfFromElement(tab, AccessMode.CREATE);
		}
	}

	@Override
	public void visitEnd(Insert n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(Delete n) {
		visit((Statement) n);
		Expression from = n.getFrom();
		if (from != null) {
			addAllColumsnOfFromElement(from, AccessMode.DELETE);
		}
		Expression using = n.getUsing();
		if (using != null) {
			addAllColumsnOfFromElement(using, AccessMode.DELETE);
		}
	}

	@Override
	public void visitEnd(Delete n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(Select n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(Select n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(Update n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(Update n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(CreateTable n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(CreateTable n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(AlterTable n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(AlterTable n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(DropTable n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(DropTable n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(SelectExpression n) {
		Expression columnList = n.getColumnList();
		Expression from = n.getFrom();
		if (columnList != null) {
			getColumnAccesses(columnList, from);
		}
	}

	@Override
	public void visit(Id id) {
		if (!actualStmt.isPresent()) {
			return;
		}

		Base ref = id.getRefersTo();
		if (ref != null) {
			if (ref instanceof Column column) {
				addColumnAccess(actualStmt.get(), column, AccessMode.READ);
			} else if (ref instanceof Table table) {
				addTableAccess(actualStmt.get(), table, AccessMode.READ);
			}
		}
	}

	private void getColumnAccesses(Expression columnList, Expression from) {
		if (columnList instanceof BinaryExpression binaryExpression) {
			getColumnAccesses(binaryExpression, AccessMode.READ);
		} else if (columnList instanceof ExpressionList expressionList) {
			getColumnAccesses(expressionList, from);
		} else if (columnList instanceof Asterisk) {
			getAllColumnAccesses(from, AccessMode.READ);
		} else if (columnList instanceof Alias || columnList instanceof Id) {
			// we don't need to do anything, we will visit the Id through the preorder visit
		} else if (columnList != null) {
			LOG.warn("Unhandled Expression kind in getColumnAccesses : {}", columnList);
		} else {
			// this can happen if the AST is not complete, ignore this case
		}
	}

	private void getColumnAccesses(BinaryExpression expr, int mode) {
		final Expression left = expr.getLeft();
		final Expression right = expr.getRight();
		if (expr.getKind() == BinaryExpressionKind.FIELDSELECTOR && left != null && right != null) {
			if (right instanceof Asterisk) {
				addAllColumsnOfFromElement(left, mode | AccessMode.IMPLICIT);
			}
		} else {
			// we don't have to do anything here
		}
	}

	private void getColumnAccesses(ExpressionList exprList, Expression from) {
		for (Expression expr : exprList.getExpressions()) {
			getColumnAccesses(expr, from);
		}
	}

	private void getAllColumnAccesses(Expression from, int mode) {
		if (from != null) {
			addAllColumsnOfFromElement(from, mode | AccessMode.IMPLICIT);
		}
	}

	private Base resolveAlias(Alias alias) {
		Expression expr = alias.getExpression();
		if (expr != null) {
			if (expr instanceof Id id) {
				Base ref = id.getRefersTo();
				if (ref instanceof Alias refAlias) {
					return resolveAlias(refAlias);
				} else {
					return ref;
				}
			} else {
				return expr;
			}
		}
		return null;
	}

	private void addColumnAccess(Statement stmt, Column col, int accessMode) {
		LOG.debug("New Column access {} - {} - {}", stmt, col, accessMode);
		Map<Column, Integer> acc = columnAcc.computeIfAbsent(stmt, k -> new HashMap<>());
		Integer oldMode = acc.get(col);
		acc.put(col, oldMode == null ? accessMode : accessMode | oldMode);
	}

	private void addTableAccess(Statement stmt, Table tab, int accessMode) {
		LOG.debug("New Table access {} - {} - {}", stmt, tab, accessMode);
		Map<Table, Integer> acc = tableAcc.computeIfAbsent(stmt, k -> new HashMap<>());
		Integer oldMode = acc.get(tab);
		acc.put(tab, oldMode == null ? accessMode : accessMode | oldMode);
	}

	private void addAllColumnsOfTable(Table tab, int accessMode) {
		if (actualStmt.isPresent()) {
			for (Column col : tab.getColumns()) {
				addColumnAccess(actualStmt.get(), col, accessMode);
			}
		}
	}

	private void addAllColumsnOfFromElement(Expression expr, int accessMode) {
		if (expr instanceof ExpressionList expressionList) {
			addAllColumsnOfFromElement(expressionList, accessMode);
		} else if (expr instanceof Id id) {
			addAllColumsnOfFromElement(id, accessMode);
		} else if (expr instanceof Alias alias) {
			addAllColumsnOfFromElement(alias, accessMode);
		} else if (expr instanceof Join join) {
			addAllColumsnOfFromElement(join, accessMode);
		} else if (expr instanceof SelectExpression) {
			// no need to do anything, we will add the columns while visiting the node
		} else {
			LOG.warn("Unhandled from element: {}", expr);
		}
	}

	private void addAllColumsnOfFromElement(ExpressionList list, int accessMode) {
		for (Expression expr : list.getExpressions()) {
			addAllColumsnOfFromElement(expr, accessMode);
		}
	}

	private void addAllColumsnOfFromElement(Id expr, int accessMode) {
		Base ref = expr.getRefersTo();
		if (ref instanceof Table table) {
			addAllColumnsOfTable(table, accessMode);
		}
	}

	private void addAllColumsnOfFromElement(Alias alias, int accessMode) {
		Base ref = resolveAlias(alias);
		if (ref != null) {
			if (ref instanceof Table table) {
				addAllColumnsOfTable(table, accessMode);
			} else if (ref instanceof Expression expression) {
				addAllColumsnOfFromElement(expression, accessMode);
			}
		}
	}

	private void addAllColumsnOfFromElement(Join join, int accessMode) {
		Expression left = join.getLeft();
		if (left != null) {
			addAllColumsnOfFromElement(left, accessMode);
		}

		Expression right = join.getRight();
		if (right != null) {
			addAllColumsnOfFromElement(right, accessMode);
		}
	}
}
