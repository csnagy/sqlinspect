package sqlinspect.cfgvisualizer.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.gef.graph.Edge;
import org.eclipse.gef.graph.Graph;
import org.eclipse.gef.graph.Node;
import org.eclipse.gef.layout.algorithms.SpringLayoutAlgorithm;
import org.eclipse.gef.zest.fx.ZestProperties;
import org.eclipse.jdt.core.dom.ASTNode;

import com.google.inject.Module;

import sqlinspect.cfg.BasicBlock;
import sqlinspect.cfg.CFG;

public class CFGGraph extends AbstractGraph {

	public static Graph createEmptyGraph() {
		List<Node> nodes = new ArrayList<>();
		List<Edge> edges = new ArrayList<>();
		HashMap<String, Object> attrs = new HashMap<>();
		return new Graph(attrs, nodes, edges);
	}

	private static Graph createNodeGraph(BasicBlock bb) {
		List<Node> nodes = new ArrayList<>();
		List<Edge> edges = new ArrayList<>();
		if (!bb.nodes().isEmpty()) {
			ASTNode first = bb.nodes().get(0);
			nodes.add(cfgNode(first.toString(), "", ""));
			bb.nodes().stream().skip(1).forEach(stmt -> {
				int nodesSize = nodes.size();
				nodes.add(cfgNode(stmt.toString(), "", ""));
				edges.add(cfgEdge(nodes.get(nodesSize - 1), nodes.get(nodesSize)));
			});
		}
		HashMap<String, Object> attrs = new HashMap<>();
		attrs.put(ZestProperties.LAYOUT_ALGORITHM__G, new SpringLayoutAlgorithm());
		return new Graph(attrs, nodes, edges);
	}

	public static Graph createGraph(CFG cfg) {
		List<Node> nodes = new ArrayList<>();
		List<Edge> edges = new ArrayList<>();

		if (nodes.size() < cfg.basicblocks().size()) {
			Node empty = cfgNode("", "", "");
			nodes.addAll(Collections.<Node>nCopies(cfg.basicblocks().size() - nodes.size(), empty));
		}

		final BasicBlock entry = cfg.getEntry();
		final Node entryNode = node(Integer.toString(entry.getId()), "bb", "Exit block " + Integer.toString(entry.getId()),
				ZestProperties.SHAPE_CSS_STYLE__N, "-fx-fill: green");
		nodes.set(entry.getId(), entryNode);

		final BasicBlock exit = cfg.getExit();
		final Node exitNode = node(Integer.toString(exit.getId()), "bb", "Exit block " + Integer.toString(exit.getId()),
				ZestProperties.SHAPE_CSS_STYLE__N, "-fx-fill: red");
		nodes.set(exit.getId(), exitNode);

		cfg.basicblocks().stream().filter(bb -> bb.getId() != entry.getId() && bb.getId() != exit.getId())
				.forEach(bb -> {
					Node node = cfgNode(Integer.toString(bb.getId()), "bb", "Basic block " + Integer.toString(bb.getId()));
					if (nodes.size() == bb.getId()) {
						nodes.add(node);
					} else {
						nodes.set(bb.getId(), node);
					}
					if (!bb.nodes().isEmpty()) {
						node.setNestedGraph(createNodeGraph(bb));
					}
				});

		cfg.basicblocks().stream().forEach(bb -> bb.succs().stream()
				.forEach(succ -> edges.add(cfgEdge(nodes.get(bb.getId()), nodes.get(succ.getId())))));

		HashMap<String, Object> attrs = new HashMap<>();
		attrs.put(ZestProperties.LAYOUT_ALGORITHM__G, new SpringLayoutAlgorithm());
		return new Graph(attrs, nodes, edges);
	}

	private static Edge cfgEdge(Node from, Node to) {
		return edge(from, to, ZestProperties.TARGET_DECORATION__E, new javafx.scene.shape.Polygon(0, 0, 10, 3, 10, -3),
				ZestProperties.TARGET_DECORATION_CSS_STYLE__E, "-fx-fill: white;", ZestProperties.LABEL__NE, "");
	}

	private static Node cfgNode(String label, String idPrefix, String tooltip) {
		return node(label, ZestProperties.CSS_ID__NE, idPrefix + label, ZestProperties.TOOLTIP__N, tooltip);
	}

	@Override
	protected Graph createGraph() {
		return createEmptyGraph();
	}

	@Override
	protected Module createModule() {
		return new CFGGraphModule();
	}
}
