package sqlinspect.cfgvisualizer.graph;

import org.eclipse.gef.graph.Edge;
import org.eclipse.gef.graph.Edge.Builder;
import org.eclipse.gef.graph.Graph;
import org.eclipse.gef.graph.Node;
import org.eclipse.gef.zest.fx.ZestFxModule;
import org.eclipse.gef.zest.fx.ZestProperties;

import com.google.inject.Module;

public abstract class AbstractGraph {
	private static int id;
	protected static final String ATTR_ID = ZestProperties.CSS_ID__NE;
	protected static final String ATTR_LABEL = ZestProperties.LABEL__NE;
	protected static final String CSS_CLASS = ZestProperties.CSS_CLASS__NE;
	protected static final String LAYOUT_IRRELEVANT = ZestProperties.LAYOUT_IRRELEVANT__NE;

	protected abstract Graph createGraph();

	protected static String genId() {
		return Integer.toString(id++);
	}

	protected static Edge edge(Node from, Node to, Object... attr) {
		String label = (String) from.attributesProperty().get(ATTR_LABEL) + (String) to.attributesProperty().get(ATTR_LABEL);
		Builder builder = new Edge.Builder(from, to).attr(ATTR_LABEL, label).attr(ATTR_ID, genId());
		for (int i = 0; i < attr.length; i += 2) {
			builder.attr(attr[i].toString(), attr[i + 1]);
		}
		return builder.buildEdge();
	}

	protected static Edge edge(Graph graph, Node from, Node to, Object... attr) {
		Edge edge = edge(from, to, attr);
		graph.getEdges().add(edge);
		return edge;
	}

	protected static Node node(String id, Object... attr) {
		Node.Builder builder = new Node.Builder();
		builder.attr(ATTR_ID, id).attr(ATTR_LABEL, id);
		for (int i = 0; i < attr.length; i += 2) {
			builder.attr(attr[i].toString(), attr[i + 1]);
		}
		return builder.buildNode();
	}

	protected static Node node(Graph graph, String id, Object... attr) {
		Node node = node(id, attr);
		graph.getNodes().add(node);
		return node;
	}

	protected Module createModule() {
		return new ZestFxModule();
	}
}
