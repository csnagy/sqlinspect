package sqlinspect.cfgvisualizer.graph;

import org.eclipse.gef.common.adapt.inject.AdapterInjectionSupport;
import org.eclipse.gef.common.adapt.inject.AdapterInjectionSupport.LoggingMode;
import org.eclipse.gef.zest.fx.ZestFxModule;

public class CFGGraphModule extends ZestFxModule  {

	@Override
	protected void enableAdapterMapInjection() {
		install(new AdapterInjectionSupport(LoggingMode.PRODUCTION));
	}
}
