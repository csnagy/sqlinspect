package sqlinspect.cfgvisualizer.ui.handlers;

import javax.inject.Inject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGBuilder;
import sqlinspect.cfgvisualizer.ui.views.CFGView;
import sqlinspect.plugin.utils.ASTStore;
import sqlinspect.plugin.utils.ASTUtils;

public class ShowLooplessCFGHandler extends AbstractHandler {
	private static final Logger LOG = LoggerFactory.getLogger(ShowLooplessCFGHandler.class);

	@Inject
	private ASTStore astStore;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		final ISelectionService service = window.getSelectionService();
		ISelection sel = service.getSelection();

		if (sel instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			if (element instanceof IMethod method) {
				try {
					LOG.debug("Method: {}", method.getSignature());
					ICompilationUnit icu = method.getCompilationUnit();
					CompilationUnit cu = astStore.get(icu);
					MethodDeclaration methodDeclaration = ASTUtils.findMethodDeclaration(cu, method);
					CFG cfg = CFGBuilder.build(methodDeclaration, true);
					cfg.dump();
					IWorkbenchPage wbPage = window.getActivePage();
					CFGView cfgView = (CFGView) wbPage.showView(CFGView.ID);
					cfgView.setGraph(cfg);
				} catch (JavaModelException | PartInitException e) {
					LOG.error("Error:", e);
				}
			}
		}

		return null;
	}

}
