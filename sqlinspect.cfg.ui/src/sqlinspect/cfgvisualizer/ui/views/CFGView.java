package sqlinspect.cfgvisualizer.ui.views;

import org.eclipse.gef.mvc.fx.ui.actions.FitToViewportAction;
import org.eclipse.gef.mvc.fx.ui.actions.FitToViewportActionGroup;
import org.eclipse.gef.mvc.fx.ui.actions.ScrollActionGroup;
import org.eclipse.gef.mvc.fx.ui.actions.ZoomActionGroup;
import org.eclipse.gef.zest.fx.ui.ZestFxUiModule;
import org.eclipse.gef.zest.fx.ui.parts.ZestFxUiView;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;

import com.google.inject.Guice;
import com.google.inject.util.Modules;

import sqlinspect.cfg.CFG;
import sqlinspect.cfgvisualizer.graph.CFGGraph;
import sqlinspect.cfgvisualizer.graph.CFGGraphModule;

public final class CFGView extends ZestFxUiView {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "sqlinspect.cfgvisualizer.ui.views.CFGView";

	private ZoomActionGroup zoomActionGroup;
	private FitToViewportActionGroup fitToViewportActionGroup;
	private ScrollActionGroup scrollActionGroup;

	public CFGView() {
		super(Guice.createInjector(Modules.override(new CFGGraphModule()).with(new ZestFxUiModule())));
		setGraph(CFGGraph.createEmptyGraph());
	}

	public void setGraph(CFG cfg) {
		super.setGraph(CFGGraph.createGraph(cfg));
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		// create actions
		zoomActionGroup = new ZoomActionGroup(new FitToViewportAction());
		getContentViewer().setAdapter(zoomActionGroup);
		fitToViewportActionGroup = new FitToViewportActionGroup();
		getContentViewer().setAdapter(fitToViewportActionGroup);
		scrollActionGroup = new ScrollActionGroup();
		getContentViewer().setAdapter(scrollActionGroup);

		// contribute to toolbar
		IActionBars actionBars = getViewSite().getActionBars();
		IToolBarManager mgr = actionBars.getToolBarManager();
		zoomActionGroup.fillActionBars(actionBars);
		mgr.add(new Separator());
		fitToViewportActionGroup.fillActionBars(actionBars);
		mgr.add(new Separator());
		scrollActionGroup.fillActionBars(actionBars);
	}

	@Override
	public void dispose() {
		// dispose actions
		if (zoomActionGroup != null) {
			getContentViewer().unsetAdapter(zoomActionGroup);
			zoomActionGroup.dispose();
			zoomActionGroup = null;
		}
		if (scrollActionGroup != null) {
			getContentViewer().unsetAdapter(scrollActionGroup);
			scrollActionGroup.dispose();
			scrollActionGroup = null;
		}
		if (fitToViewportActionGroup != null) {
			getContentViewer().unsetAdapter(fitToViewportActionGroup);
			fitToViewportActionGroup.dispose();
			fitToViewportActionGroup = null;
		}
		super.dispose();
	}
}
