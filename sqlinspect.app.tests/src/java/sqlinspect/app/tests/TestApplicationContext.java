package sqlinspect.app.tests;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.osgi.framework.Bundle;

public class TestApplicationContext implements IApplicationContext {
	private final Map<String, Object> arguments = new HashMap<>();

	@Override
	public Map<String, Object> getArguments() {
		return arguments;
	}

	@Override
	public void applicationRunning() {
      // only for testing
	}

	@Override
	public String getBrandingApplication() {
		return null;
	}

	@Override
	public String getBrandingName() {
		return null;
	}

	@Override
	public String getBrandingDescription() {
		return null;
	}

	@Override
	public String getBrandingId() {
		return null;
	}

	@Override
	public String getBrandingProperty(String key) {
		return null;
	}

	@Override
	public Bundle getBrandingBundle() {
		return null;
	}

	@Override
	public void setResult(Object result, IApplication application) {
		// only for testing
	}
}