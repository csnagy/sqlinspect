package sqlinspect.app.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import sqlinspect.app.SQLInspectCLI;

class SQLInspectCLITest {
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	protected static final File TEST_PROJ_DIR = new File(System.getenv().get("TEST_PROJ_DIR"));
	protected static final File TEST_OUT_DIR = new File(System.getenv().get("TEST_OUT_DIR"));

	@BeforeEach
	void setUp() {
		System.setOut(new PrintStream(outputStreamCaptor, true, StandardCharsets.UTF_8));
	}

	@Test
	void shouldWriteHelp() throws CoreException, IOException {
		SQLInspectCLI cli = new SQLInspectCLI();
		String[] args = { "-help" };
		cli.run(args);

		assertThat(outputStreamCaptor.toString(StandardCharsets.UTF_8).trim(),
				StringContains.containsString("Print this help."));
	}

	@ParameterizedTest
	@ValueSource(strings = { "help", "-asdf", "--qwe" })
	void shouldFindInvalidParameter(String testParam) {
		SQLInspectCLI cli = new SQLInspectCLI();
		String[] args = { testParam };

		Exception e = assertThrows(IllegalArgumentException.class, () -> cli.run(args));
		assertEquals("Invalid parameter: " + testParam, e.getMessage());
	}

	@Test
	void shouldMissArgument() {
		SQLInspectCLI cli = new SQLInspectCLI();
		String[] args = { "-hotspotfinder" };

		Exception e = assertThrows(IllegalArgumentException.class, () -> cli.run(args));
		assertEquals("Missing argument for: -hotspotfinder", e.getMessage());
	}

	// based on eclipse.pde.ui.NonUIThreadTestApplication
	private IApplication getApplication(String appId) throws CoreException {
		IExtension extension = Platform.getExtensionRegistry().getExtension(Platform.PI_RUNTIME,
				Platform.PT_APPLICATIONS, appId);
		IConfigurationElement[] elements = extension.getConfigurationElements();
		if (elements.length > 0) {
			IConfigurationElement[] runs = elements[0].getChildren("run");
			if (runs.length > 0) {
				Object runnable = runs[0].createExecutableExtension("class");
				if (runnable instanceof IApplication app) {
					return app;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@ParameterizedTest
	@ValueSource(strings = { "Testproj", "Testproj2" })
	void shouldAnalyzeMySQLJDBCProject(String projectName) throws Exception {
		String appId = "sqlinspect.app.SQLInspectApp";
		IApplication app = getApplication(appId);
		IApplicationContext context = new TestApplicationContext();
		File testProjectDirectory = new File(TEST_PROJ_DIR, projectName);
		String[] args = { "-hotspotfinder", "JDBCHotspotFinder", "-dialect", "MySQL", "-projectname", projectName,
				"-projectdir", testProjectDirectory.getAbsolutePath(), "-outdir", TEST_OUT_DIR.getAbsolutePath() };
		context.getArguments().put(IApplicationContext.APPLICATION_ARGS, args);
		createOutDirIfNotExists();

		app.start(context);

		assertAll("Files created",
				() -> assertTrue("queries.xml should exist",
						new File(TEST_OUT_DIR, projectName + "-queries.xml").exists()),
				() -> assertTrue("schema.xml should exist",
						new File(TEST_OUT_DIR, projectName + "-schema.xml").exists()),
				() -> assertTrue("sqls.xml should exist",
						new File(TEST_OUT_DIR, projectName + "-sqlsmells.xml").exists()),
				() -> assertTrue("sqlmetrics.xml should exist",
						new File(TEST_OUT_DIR, projectName + "-sqlmetrics.xml").exists()));
	}

	private void createOutDirIfNotExists() throws IOException {
		if (!TEST_OUT_DIR.exists()) {
			Files.createDirectory(TEST_OUT_DIR.toPath());
		}
	}

}
