/**
 */
package sqlinspect.dbmodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sqlinspect.dbmodel.DB#getEntities <em>Entities</em>}</li>
 *   <li>{@link sqlinspect.dbmodel.DB#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see sqlinspect.dbmodel.DBModelPackage#getDB()
 * @model
 * @generated
 */
public interface DB extends EObject {
	/**
	 * Returns the value of the '<em><b>Entities</b></em>' containment reference list.
	 * The list contents are of type {@link sqlinspect.dbmodel.DBEntity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entities</em>' containment reference list.
	 * @see sqlinspect.dbmodel.DBModelPackage#getDB_Entities()
	 * @model containment="true"
	 * @generated
	 */
	EList<DBEntity> getEntities();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see sqlinspect.dbmodel.DBModelPackage#getDB_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link sqlinspect.dbmodel.DB#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // DB
