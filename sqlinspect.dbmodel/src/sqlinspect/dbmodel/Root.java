/**
 */
package sqlinspect.dbmodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sqlinspect.dbmodel.Root#getDatabases <em>Databases</em>}</li>
 * </ul>
 *
 * @see sqlinspect.dbmodel.DBModelPackage#getRoot()
 * @model
 * @generated
 */
public interface Root extends EObject {
	/**
	 * Returns the value of the '<em><b>Databases</b></em>' containment reference list.
	 * The list contents are of type {@link sqlinspect.dbmodel.DB}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Databases</em>' containment reference list.
	 * @see sqlinspect.dbmodel.DBModelPackage#getRoot_Databases()
	 * @model containment="true"
	 * @generated
	 */
	EList<DB> getDatabases();

} // Root
