/**
 */
package sqlinspect.dbmodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sqlinspect.dbmodel.Collection#getSchemas <em>Schemas</em>}</li>
 * </ul>
 *
 * @see sqlinspect.dbmodel.DBModelPackage#getCollection()
 * @model
 * @generated
 */
public interface Collection extends DBEntity {
	/**
	 * Returns the value of the '<em><b>Schemas</b></em>' containment reference list.
	 * The list contents are of type {@link sqlinspect.dbmodel.Schema}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schemas</em>' containment reference list.
	 * @see sqlinspect.dbmodel.DBModelPackage#getCollection_Schemas()
	 * @model containment="true"
	 * @generated
	 */
	EList<Schema> getSchemas();

} // Collection
