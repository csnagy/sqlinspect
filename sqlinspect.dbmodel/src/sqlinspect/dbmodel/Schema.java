/**
 */
package sqlinspect.dbmodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schema</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link sqlinspect.dbmodel.Schema#getFields <em>Fields</em>}</li>
 * </ul>
 *
 * @see sqlinspect.dbmodel.DBModelPackage#getSchema()
 * @model
 * @generated
 */
public interface Schema extends EObject {
	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link sqlinspect.dbmodel.Field}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see sqlinspect.dbmodel.DBModelPackage#getSchema_Fields()
	 * @model containment="true"
	 * @generated
	 */
	EList<Field> getFields();

} // Schema
