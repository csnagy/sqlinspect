package sqlinspect.dbmodel.util;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.ecore.EClass;
import org.emfjson.jackson.annotations.EcoreTypeInfo;
import org.emfjson.jackson.module.EMFModule;
import org.emfjson.jackson.utils.ValueWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

import sqlinspect.dbmodel.Root;

public class DBModelJSonExport {

	private static final Logger LOG = LoggerFactory.getLogger(DBModelJSonExport.class);

	private DBModelJSonExport() {
	}

	public static void exportBasic(Root root, File outFile) {
		LOG.debug("Export schema to: {}", outFile);
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(outFile, root);
		} catch (IOException e) {
			LOG.error("Exception: ", e);
		}
		LOG.debug("Schema export done.");
	}

	// See https://emfjson.github.io/projects/jackson/latest/#getting-started
	public static void export(Root root, File outFile) throws IOException {
		LOG.debug("Export schema to: {}", outFile);

		ObjectMapper mapper = new ObjectMapper();
		EMFModule module = new EMFModule();
		module.setTypeInfo(new EcoreTypeInfo("kind"));
		module.setTypeInfo(new EcoreTypeInfo("kind", new ValueWriter<EClass, String>() {
			@Override
			public String writeValue(EClass value, SerializerProvider context) {
				return value.getName();
			}
		}));
		mapper.registerModule(module);

		mapper.writerWithDefaultPrettyPrinter().writeValue(outFile, root);

		LOG.debug("Schema export done.");
	}
}
