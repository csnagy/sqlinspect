package sqlinspect.dbmodel.util;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.dbmodel.Root;

public class DBModelXMIExport {

	private static final Logger LOG = LoggerFactory.getLogger(DBModelXMIExport.class);

	private DBModelXMIExport() {
	}

	public static void export(Root root, File outFile) {
		LOG.debug("Export schema to: {}", outFile);
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("dbmodel", new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		Resource resource = resSet.createResource(URI.createFileURI(outFile.getPath()));

		resource.getContents().add(root);

		try {
			resource.save(Collections.emptyMap());
		} catch (IOException e) {
			LOG.error("Could not export to XMI: ", e);
		}
		LOG.debug("Schema export done.");
	}

}
