/**
 */
package sqlinspect.dbmodel;

import org.eclipse.emf.ecore.EObject;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see sqlinspect.dbmodel.DBModelPackage#getType()
 * @model abstract="true"
 * @generated
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "type")
public interface Type extends EObject {
} // Type
