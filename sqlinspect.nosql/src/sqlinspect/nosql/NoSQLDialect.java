package sqlinspect.nosql;

public enum NoSQLDialect {
	MONGODB("MongoDB");

	private final String text;

	NoSQLDialect(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public static boolean isNoSQLDialect(String value) {
		for (NoSQLDialect d : values()) {
			if (d.toString().equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static NoSQLDialect getNoSQLDialect(String value) {
		for (NoSQLDialect d : values()) {
			if (d.toString().equalsIgnoreCase(value)) {
				return d;
			}
		}
		throw new IllegalArgumentException();
	}

}
