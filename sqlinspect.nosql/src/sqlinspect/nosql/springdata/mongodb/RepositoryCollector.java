package sqlinspect.nosql.springdata.mongodb;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.ParameterizedType;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.utils.BindingUtil;

public class RepositoryCollector extends ASTVisitor {

	private static final Logger LOG = LoggerFactory.getLogger(RepositoryCollector.class);

	public static final String MONGODB_REPOSITORY = "org.springframework.data.mongodb.repository.MongoRepository";

	private final Map<ITypeBinding, Optional<ITypeBinding>> repositories = new HashMap<>();

	public void collect(CompilationUnit unit) {
		unit.accept(this);
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		if (extendsMongoRepository(node)) {
			ITypeBinding repositoryBinding = node.resolveBinding();
			Optional<ITypeBinding> collectionBinding = getCollectionTypeOfRepositoryDeclaration(node);
			collectionBinding.ifPresentOrElse(
					n -> LOG.debug("MongoRepository: {} -> {}", repositoryBinding.getQualifiedName(),
							n.getQualifiedName()),
					() -> LOG.debug("MongoRepository: {} -> {}", repositoryBinding.getQualifiedName(), "UNKNOWN"));
			repositories.put(node.resolveBinding(), collectionBinding);
		}

		return true;

	}

	// eg: public interface DatafileVersionsRepository extends
	// MongoRepository<DatafileVersionLight, String>
	private static boolean extendsMongoRepository(TypeDeclaration node) {
		for (Object o : node.superInterfaceTypes()) {
			if (o instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) o;
				if (parameterizedType.getType() instanceof SimpleType) {
					SimpleType simpleType = (SimpleType) parameterizedType.getType();
					ITypeBinding binding = simpleType.resolveBinding();
					String name = BindingUtil.qualifiedName(binding);
					if (MONGODB_REPOSITORY.equals(name)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private static Optional<ITypeBinding> getCollectionTypeOfRepositoryDeclaration(TypeDeclaration node) {
		for (Object o : node.superInterfaceTypes()) {
			if (o instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) o;
				if (parameterizedType.getType() instanceof SimpleType) {
					SimpleType simpleType = (SimpleType) parameterizedType.getType();
					ITypeBinding binding = simpleType.resolveBinding();
					String name = BindingUtil.qualifiedName(binding);
					if (MONGODB_REPOSITORY.equals(name)) {
						Type parameter = (Type) parameterizedType.typeArguments().get(0);
						return Optional.of(parameter.resolveBinding());
					}
				}
			}
		}
		return Optional.empty();
	}

	public Map<ITypeBinding, Optional<ITypeBinding>> getRepositories() {
		return repositories;
	}
}
