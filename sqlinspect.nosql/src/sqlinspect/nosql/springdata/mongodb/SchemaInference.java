package sqlinspect.nosql.springdata.mongodb;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.bson.BsonType;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IAnnotationBinding;
import org.eclipse.jdt.core.dom.IMemberValuePairBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.dbmodel.ArrayType;
import sqlinspect.dbmodel.Collection;
import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Field;
import sqlinspect.dbmodel.ObjectType;
import sqlinspect.dbmodel.Schema;
import sqlinspect.dbmodel.SimpleType;
import sqlinspect.dbmodel.Type;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.utils.ASTStore;

public class SchemaInference {
	private static final Logger LOG = LoggerFactory.getLogger(SchemaInference.class);

	private static final DBModelFactory factory = DBModelFactory.eINSTANCE;

	// Based on
	// https://mongodb.github.io/mongo-java-driver/3.6/javadoc/?org/bson/codecs/BsonTypeClassMap.html
	private static final Set<String> arrayTypes = Stream.of("java.util.List", "java.util.Set")
			.collect(Collectors.toCollection(HashSet::new));

	// Based on
	// https://mongodb.github.io/mongo-java-driver/3.6/javadoc/?org/bson/codecs/BsonTypeClassMap.html
	// https://docs.mongodb.com/manual/reference/bson-types/

	private static final Map<String, BsonType> bsonTypeMap = Stream
			.of(new Object[][] { { "org.bson.Document", BsonType.DOCUMENT }, { "java.util.List", BsonType.ARRAY },
					{ "java.util.Date", BsonType.DATE_TIME }, { "java.lang.Boolean", BsonType.BOOLEAN },
					{ "java.lang.Double", BsonType.DOUBLE }, { "java.lang.Integer", BsonType.INT32 },
					{ "java.lang.Long", BsonType.INT64 },
					// { "org.bson.types.Decimal128", BsonType.DECIMAL128 }, -- since 3.4
					{ "java.lang.String", BsonType.STRING }, { "org.bson.types.Binary", BsonType.BINARY },
					{ "org.bson.types.ObjectId", BsonType.OBJECT_ID },
					{ "org.bson.types.RegularExpression", BsonType.REGULAR_EXPRESSION },
					{ "org.bson.types.Symbol", BsonType.SYMBOL }, { "org.bson.types.DBPointer", BsonType.DB_POINTER },
					{ "org.bson.types.MaxKey", BsonType.MAX_KEY }, { "org.bson.types.MinKey", BsonType.MIN_KEY },
					{ "org.bson.types.Code", BsonType.JAVASCRIPT },
					{ "org.bson.types.CodeWithScope", BsonType.JAVASCRIPT_WITH_SCOPE },
					{ "org.bson.types.BSONTimestamp", BsonType.TIMESTAMP },
					{ "org.bson.types.Undefined", BsonType.UNDEFINED }, })
			.collect(Collectors.toMap(data -> (String) data[0], data -> (BsonType) data[1]));

	// Base on AST.resolveWellKnownType
//	private static final Set<String> wellKnownType = Stream
//			.of("java.lang.AssertionError", "java.lang.Boolean", "java.lang.Byte", "java.lang.Character",
//					"java.lang.Class", "java.lang.Cloneable", "java.lang.Double", "java.lang.Error",
//					"java.lang.Exception", "java.lang.Float", "java.lang.Integer", "java.lang.Long", "java.lang.Object",
//					"java.lang.RuntimeException", "java.lang.Short", "java.lang.String", "java.lang.StringBuffer",
//					"java.lang.Throwable", "java.lang.Void", "java.io.Serializable")
//			.collect(Collectors.toCollection(HashSet::new));

	@Inject
	private ASTStore astStore;

	private CompilationUnit currentCompilationUnit;

	public DB inferDB(Project project, IEclipseContext context) throws CoreException {
		LOG.debug("Infer DB of project: {}", project.getName());

		RepositoryCollector repositoryCollector = ContextInjectionFactory.make(RepositoryCollector.class, context);

		astStore.parse(project.getIJavaProject());

		IPackageFragment[] packages = project.getIJavaProject().getPackageFragments();
		for (IPackageFragment p : packages) {
			if (p.getKind() == IPackageFragmentRoot.K_SOURCE) {
				for (ICompilationUnit unit : p.getCompilationUnits()) {
					currentCompilationUnit = astStore.get(unit);
					repositoryCollector.collect(currentCompilationUnit);
				}
			}
		}

		Map<ITypeBinding, Optional<ITypeBinding>> repositories = repositoryCollector.getRepositories();

		DB db = factory.createDB();
		db.setName("DB");

		for (Optional<ITypeBinding> collectionBinding : repositories.values()) {
			if (collectionBinding.isPresent()) {
				Collection collection = inferCollection(collectionBinding.get());

				db.getEntities().add(collection);
			}
		}

		return db;
	}

	private Collection inferCollection(ITypeBinding collectionBinding) {
		LOG.debug("Infer collection: {}", collectionBinding.getQualifiedName());

		Collection collection = factory.createCollection();

		String name = collectionBinding.getName();

		// @Document(collection = "repositoryHistories")
		IAnnotationBinding[] annotations = collectionBinding.getAnnotations();
		if (annotations != null && annotations.length > 0) {
			for (IAnnotationBinding annotation : annotations) {
				if ("Document".equals(annotation.getName())) {
					IMemberValuePairBinding[] params = annotation.getDeclaredMemberValuePairs();
					for (IMemberValuePairBinding param : params) {
						if ("collection".equals(param.getName())) {
							name = param.getValue().toString();
						}
					}
				}
			}
		}

		collection.setName(name);

		Schema schema = inferSchema(collectionBinding);
		collection.getSchemas().add(schema);

		return collection;
	}

	private Schema inferSchema(ITypeBinding collectionBinding) {
		LOG.trace("Infer schema of collection: {}", collectionBinding.getQualifiedName());
		Schema schema = factory.createSchema();

		if (collectionBinding.getSuperclass() != null && !collectionBinding.getSuperclass()
				.equals(currentCompilationUnit.getAST().resolveWellKnownType("java.lang.Object"))) {
			Schema parentSchema = inferSchema(collectionBinding.getSuperclass());
			for (Field field : parentSchema.getFields()) {
				schema.getFields().add(EcoreUtil.copy(field));
			}
		}

		for (IVariableBinding declaredField : collectionBinding.getDeclaredFields()) {
			Field field = inferField(declaredField);
			schema.getFields().add(field);
		}

		return schema;
	}

	// @Field("field_name")
	// String field;

	private Field inferField(IVariableBinding fieldBinding) {
		LOG.trace("Infer field: {} of {}", fieldBinding.getKey(), fieldBinding.getType().getQualifiedName());

		Field field = factory.createField();

		String name = fieldBinding.getName();

		IAnnotationBinding[] annotations = fieldBinding.getAnnotations();
		if (annotations != null && annotations.length > 0) {
			for (IAnnotationBinding annotation : annotations) {
				if ("Field".equals(annotation.getName())) {
					IMemberValuePairBinding[] params = annotation.getDeclaredMemberValuePairs();
					if (params != null && params.length > 0 && params[0].getValue() instanceof String) {
						name = (String) params[0].getValue();
					}
				}
			}
		}

		field.setName(name);

		ITypeBinding type = fieldBinding.getType();
		field.setType(inferType(type));
		return field;
	}

	private Type inferType(ITypeBinding typeBinding) {
		LOG.trace("Infer type: {}", typeBinding.getQualifiedName());

		if (typeBinding.isPrimitive()) {
			return inferPrimitiveType(typeBinding);
		} else if (typeBinding.isArray()) {
			return inferArrayType(typeBinding);
		} else if (typeBinding.isParameterizedType()) {
			String binaryName = typeBinding.getBinaryName();
			if (arrayTypes.contains(binaryName)) {
				return inferCollectionType(typeBinding);
			} else {
				return inferObjectType(typeBinding);
			}
		} else if (typeBinding.isEnum()) {
			return inferEnumType(typeBinding);
		} else if (typeBinding.isClass()) {
			if (isBsonType(typeBinding)) {
				return inferBsonType(typeBinding);
			} else {
				return inferObjectType(typeBinding);
			}
		}

		SimpleType type = factory.createSimpleType();
		type.setName("UNKNOWN");
		return type;
	}

	private boolean isBsonType(ITypeBinding typeBinding) {
		return bsonTypeMap.containsKey(typeBinding.getQualifiedName());
	}

	private Type inferBsonType(ITypeBinding typeBinding) {
		LOG.trace("Infer Bson type: {}", typeBinding.getQualifiedName());

		SimpleType type = factory.createSimpleType();
		type.setName(bsonTypeMap.get(typeBinding.getQualifiedName()).toString());
		return type;
	}

	private Type inferObjectType(ITypeBinding typeBinding) {
		LOG.trace("Infer Object type: {}", typeBinding.getQualifiedName());

		ObjectType type = factory.createObjectType();

//		if (typeBinding != null && !wellKnownType.contains(typeBinding.getQualifiedName())) {
		if (!typeBinding.getQualifiedName().startsWith("java.")) {
			type.setSchema(inferSchema(typeBinding));
		}
		return type;
	}

	private Type inferArrayType(ITypeBinding typeBinding) {
		LOG.trace("Infer Array: {}", typeBinding.getQualifiedName());

		ArrayType type = factory.createArrayType();
		type.setElementType(inferType(typeBinding.getElementType()));
		return type;
	}

	private Type inferCollectionType(ITypeBinding typeBinding) {
		LOG.trace("Infer Collection type: {}", typeBinding.getQualifiedName());

		ArrayType type = factory.createArrayType();
		type.setElementType(inferType(typeBinding.getTypeArguments()[0]));
		return type;
	}

	private Type inferPrimitiveType(ITypeBinding typeBinding) {
		LOG.trace("Infer primitive: {}", typeBinding.getQualifiedName());

		SimpleType type = factory.createSimpleType();
		type.setName(typeBinding.getName());
		return type;
	}

	private Type inferEnumType(ITypeBinding typeBinding) {
		LOG.trace("Infer Enum type: {}", typeBinding.getQualifiedName());

		SimpleType type = factory.createSimpleType();
		type.setName(BsonType.STRING.toString());
		return type;
	}
}
