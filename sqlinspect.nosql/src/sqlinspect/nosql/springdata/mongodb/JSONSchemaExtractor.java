package sqlinspect.nosql.springdata.mongodb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import sqlinspect.dbmodel.ArrayType;
import sqlinspect.dbmodel.Collection;
import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Field;
import sqlinspect.dbmodel.ObjectType;
import sqlinspect.dbmodel.Schema;
import sqlinspect.dbmodel.SimpleType;
import sqlinspect.dbmodel.Type;

/**
 * Extract the schema of a MongoDB Document from a Document Schema.
 *
 * For example: { "bsonType": "object", "title": "<Type Name>", "required":
 * ["<Required Field Name>", ...], "properties": { "<Field Name>": <Schema
 * Document> } }
 *
 * More details: https://docs.mongodb.com/realm/mongodb/document-schemas
 */
public class JSONSchemaExtractor {

	private int collectionCounter;

	private static final Logger LOG = LoggerFactory.getLogger(JSONSchemaExtractor.class);

	private static final DBModelFactory dbFactory = DBModelFactory.eINSTANCE;

	private final DB db;

	public JSONSchemaExtractor(DB db) {
		this.db = db;
	}

	public void extractSchema(File json) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode root = objectMapper.readTree(json);
		parseRoot(root);
	}

	public void extractSchema(String json) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode root = objectMapper.readTree(json);
		parseRoot(root);
	}

	private void parseRoot(JsonNode root) {
		if (root.isArray()) {
			Iterator<JsonNode> it = root.elements();
			while (it.hasNext()) {
				JsonNode collectionNode = it.next();
				db.getEntities().add(parseCollectionNode(collectionNode));
			}
		} else if (root.isObject()) {
			db.getEntities().add(parseCollectionNode(root));
		} else {
			LOG.error("Invalid schema");
		}
	}

	private Collection parseCollectionNode(JsonNode node) {
		Collection collection = dbFactory.createCollection();
		++collectionCounter;
		String title = "Collection" + collectionCounter;
		if (node.isObject()) {
			JsonNode titleNode = node.get("title");
			if (titleNode != null && titleNode.isValueNode()) {
				title = titleNode.asText();
			}

			Schema schema = parseSchema(node);
			collection.getSchemas().add(schema);
		}
		collection.setName(title);
		return collection;
	}

	private Schema parseSchema(JsonNode node) {
		Schema schema = dbFactory.createSchema();

		JsonNode propertiesNode = node.get("properties");
		if (propertiesNode != null) {
			List<Field> fields = parsePropertiesNode(propertiesNode);
			schema.getFields().addAll(fields);
		}
		return schema;
	}

	private List<Field> parsePropertiesNode(JsonNode propertiesNode) {
		List<Field> fields = new ArrayList<>();
		Iterator<Entry<String, JsonNode>> properties = propertiesNode.fields();

		while (properties.hasNext()) {
			Entry<String, JsonNode> property = properties.next();
			Field field = parsePropertyNode(property.getKey(), property.getValue());

			fields.add(field);
		}
		return fields;
	}

	private Field parsePropertyNode(String name, JsonNode node) {
		if (!node.isObject()) {
			throw new IllegalArgumentException("Invalid property node: " + node);
		}

		Field field = dbFactory.createField();
		field.setName(name);

		field.setType(parseTypeNode(node));

		return field;
	}

	private Type parseObjectTypeNode(JsonNode node) {
		JsonNode typeNode = node.get("bsonType");

		if (typeNode == null) {
			LOG.warn("Node does not have bsonType: {}", node);
			SimpleType type = dbFactory.createSimpleType();
			type.setName("Unknown");
			return type;
		}

		if (typeNode.isValueNode()) {
			String typeText = typeNode.asText();

			if ("array".equals(typeText)) {
				ArrayType type = dbFactory.createArrayType();
				Type elementType = parseTypeNode(node.get("items"));
				type.setElementType(elementType);
				return type;
			} else if ("object".equals(typeText)) {
				ObjectType type = dbFactory.createObjectType();
				Schema schema = parseSchema(node);
				type.setSchema(schema);
				return type;
			} else {
				SimpleType type = dbFactory.createSimpleType();
				type.setName(typeText);
				return type;
			}
		} else {
			throw new IllegalArgumentException("Invalid type node: " + node);
		}
	}

	private Type parseSimpleTypeNode(JsonNode node) {
		if (node.isValueNode()) {
			String typeText = node.asText();

			if ("array".equals(typeText)) {
				return dbFactory.createArrayType();
			} else if ("object".equals(typeText)) {
				return dbFactory.createObjectType();
			} else {
				SimpleType type = dbFactory.createSimpleType();
				type.setName(typeText);
				return type;
			}
		} else {
			throw new IllegalArgumentException("Invalid type node: " + node);
		}
	}

	private Type parseTypeNode(JsonNode node) {
		if (node == null) {
			LOG.warn("Null type.");
			SimpleType type = dbFactory.createSimpleType();
			type.setName("Unknown");
			return type;
		}

		if (node.isObject()) {
			return parseObjectTypeNode(node);
		} else if (node.isValueNode()) {
			return parseSimpleTypeNode(node);
		} else {
			throw new IllegalArgumentException("Invalid type node: " + node);
		}
	}

}
