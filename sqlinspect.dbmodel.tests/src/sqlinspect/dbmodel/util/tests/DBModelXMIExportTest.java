package sqlinspect.dbmodel.util.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.xmlunit.matchers.CompareMatcher;

import sqlinspect.dbmodel.Collection;
import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Root;
import sqlinspect.dbmodel.util.DBModelXMIExport;

class DBModelXMIExportTest {

	@Test
	void test() throws IOException {
		DBModelFactory factory = DBModelFactory.eINSTANCE;
		Root root = factory.createRoot();
		DB db = factory.createDB();
		root.getDatabases().add(db);
		db.setName("DB");

		Collection collection = factory.createCollection();
		collection.setName("Test");
		db.getEntities().add(collection);

		File outFile = new File("test.xmi");

		DBModelXMIExport.export(root, outFile);

		assertTrue(outFile.exists());

		String refXmi = "<?xml version=\"1.0\" encoding=\"ASCII\"?>\n"
				+ "<sqlinspect.dbmodel:Root xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:sqlinspect.dbmodel=\"http://sqlinspect/dbmodel\">\n"
				+ "  <databases name=\"DB\">\n"
				+ "    <entities xsi:type=\"sqlinspect.dbmodel:Collection\" name=\"Test\"/>\n"
				+ "  </databases>\n"
				+ "</sqlinspect.dbmodel:Root>\n";

		try (InputStream outXml = DBModelJSonExportTest.class.getClassLoader().getResourceAsStream(outFile.getPath())) {
			assertThat(outXml, CompareMatcher.isIdenticalTo(refXmi));
		}
	}

}
