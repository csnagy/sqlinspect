package sqlinspect.dbmodel.util.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import sqlinspect.dbmodel.Collection;
import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Root;
import sqlinspect.dbmodel.util.DBModelJSonExport;

class DBModelJSonExportTest {

	@Test
	void test() throws IOException {
		File outJsonFile = new File("test.json");
		DBModelFactory factory = DBModelFactory.eINSTANCE;
		Root root = factory.createRoot();
		DB db = factory.createDB();
		root.getDatabases().add(db);
		db.setName("DB");

		Collection collection = factory.createCollection();
		collection.setName("Test");
		db.getEntities().add(collection);

		DBModelJSonExport.export(root, outJsonFile);

		assertTrue(outJsonFile.exists());

		String refJson = "{\n"
				+ "  \"kind\" : \"Root\",\n"
				+ "  \"databases\" : [ {\n"
				+ "    \"entities\" : [ {\n"
				+ "      \"kind\" : \"Collection\",\n"
				+ "      \"name\" : \"Test\"\n"
				+ "    } ],\n"
				+ "    \"name\" : \"DB\"\n"
				+ "  } ]\n"
				+ "}";

		ObjectMapper mapper = new ObjectMapper();
		try (InputStream input = Files.newInputStream(outJsonFile.toPath())) {
			assertEquals(mapper.readTree(refJson), mapper.readTree(input));
		}
	}

}
