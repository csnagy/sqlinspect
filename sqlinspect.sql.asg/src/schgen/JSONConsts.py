class JSON:
    SCOPE_ROOT = "root"
    SCOPE_SEP = '.'
    
    ENUM_NAME = "name"
    ENUM_PREFIX = "prefix"
    ENUM_FIELDS = "fields"
    
    SCOPE_NAME = "name"
    SCOPE_SCOPES = "scopes"
    SCOPE_CLASSES = "classes"
    SCOPE_ENUMS = "enums"

    CLASS_NAME = "name"
    CLASS_EXTENDS = "extends"
    CLASS_ABSTRACT = "abstract"
    CLASS_ATTRIBUTES = "attributes"
    CLASS_EDGES = "edges"

    ATTRIBUTE_NAME = "name"
    ATTRIBUTE_TYPE = "type"
    ATTRIBUTE_MATCH_IGNORE = "match_ignore"
    ATTRIBUTE_MATCH_IGNORE_LITERAL = "match_ignore_literal"
    ATTRIBUTE_MATCH_IGNORE_NAME = "match_ignore_name"
    ATTRIBUTE_MATCH_IGNORE_REF = "match_ignore_ref"

    EDGE_NAME = "name"
    EDGE_KIND = "kind"
    EDGE_MULT = "mult"
    EDGE_TYPE = "type"
    EDGE_MATCH_IGNORE = "match_ignore"
    EDGE_MATCH_IGNORE_LITERAL = "match_ignore_literal"
    EDGE_MATCH_IGNORE_NAME = "match_ignore_name"
    EDGE_MATCH_IGNORE_REF = "match_ignore_ref"
    
    EDGE_TREE = "tree"
    EDGE_CROSS = "cross"
    
    MULT_ONE = "1"
    MULT_MANY = "*"
