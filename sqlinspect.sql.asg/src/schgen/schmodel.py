import collections

from JSONConsts import JSON

class Enum:
    """ Enum in the model"""
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.fields = list()
    
class Scope:
    """Scope in the model"""
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent 
        self.classes = list()
        self.enums = list()
        self.scopes = list()
    
    def getClassByName(self, name):
        for cl in self.classes:
            if cl.name == name:
                return cl
        return None

    def getEnumByName(self, name):
        for e in self.enums:
            if e.name == name:
                return e
        return None

    
    def getScopeByName(self, name):
        for s in self.scopes:
            if s.name == name:
                return s
        return None
    
    def getClassByFullName(self, fullName):
        qs = fullName.split(JSON.SCOPE_SEP)
        sc = self  # root
        i = 0
        while (i < len(qs) - 1):
            newsc = sc.getScopeByName(qs[i])
            if (newsc is None):
                return None
        
            sc = newsc
            i = i + 1
        
        cl = sc.getClassByName(qs[i])
        return cl

    def getClassOrEnumByFullName(self, fullName):
        qs = fullName.split(JSON.SCOPE_SEP)
        sc = self  # root
        i = 0
        while (i < len(qs) - 1):
            newsc = sc.getScopeByName(qs[i])
            if (newsc is None):
                return None
        
            sc = newsc
            i = i + 1
        
        cl = sc.getClassByName(qs[i])
        if cl is None:
            cl = sc.getEnumByName(qs[i])
        return cl


    def getAllClasses(self):
        classes = list()
        l = collections.deque()
        l.append(self)
        while l:
            sc = l.popleft()
            for cl in sc.classes:
                classes.append(cl)
            for child in sc.scopes:
                l.append(child)
        return classes
        
class Class:
    """Class in the model"""
    
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.extends = None
        self.subClasses = list()
        self.attributes = list()
        self.edges = list()
        self.abstract = False
    
class Attribute:
    """Attribute of the model"""
    def __init__(self, name, typeName, matchIgnore, matchIgnoreLiteral, matchIgnoreName, matchIgnoreRef, parent):
        self.name = name
        self.type = Type(typeName)
        self.matchIgnore = matchIgnore
        self.matchIgnoreLiteral = matchIgnoreLiteral
        self.matchIgnoreName = matchIgnoreName
        self.matchIgnoreRef = matchIgnoreRef
        self.parent = parent

class Edge:
    """Edge of the model"""
    def __init__(self, name, kind, mult, typeName, matchIgnore, matchIgnoreLiteral, matchIgnoreName, matchIgnoreRef, parent):
        self.name = name
        self.kind = kind
        self.mult = mult
        self.type = Type(typeName)
        self.matchIgnore = matchIgnore
        self.matchIgnoreLiteral = matchIgnoreLiteral
        self.matchIgnoreName = matchIgnoreName
        self.matchIgnoreRef = matchIgnoreRef
        self.parent = parent

class Type:
    """Type used to represent type references in the model"""
    def __init__(self, name):
        self.name = name
        self.pointsTo = None

        hasDot = name.rfind(JSON.SCOPE_SEP)
        if hasDot > -1:
            self.shortName = name[(hasDot + 1):]
        else:
            self.shortName = name

    
