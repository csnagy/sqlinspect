import yaml
import json
import logging

from schmodel import Attribute, Edge, Class, Enum, Scope
from JSONConsts import JSON

class YamlError(Exception):
    """Yaml Parser Exception"""

    def __init__(self, message):
        self.message = message

class YamlParser:
    """Yaml Parser class"""
            
    def __init__(self, _input):
        self.input = _input
        
    def parseAttribute(self, cl, attr):
        matchIgnore = attr[JSON.ATTRIBUTE_MATCH_IGNORE] if JSON.ATTRIBUTE_MATCH_IGNORE in attr else False
        matchIgnoreLiteral = attr[JSON.ATTRIBUTE_MATCH_IGNORE_LITERAL] if JSON.ATTRIBUTE_MATCH_IGNORE_LITERAL in attr else False
        matchIgnoreName = attr[JSON.ATTRIBUTE_MATCH_IGNORE_NAME] if JSON.ATTRIBUTE_MATCH_IGNORE_NAME in attr else False
        matchIgnoreRef = attr[JSON.ATTRIBUTE_MATCH_IGNORE_REF] if JSON.ATTRIBUTE_MATCH_IGNORE_REF in attr else False
        newAttr = Attribute(attr[JSON.ATTRIBUTE_NAME], attr[JSON.ATTRIBUTE_TYPE], matchIgnore, matchIgnoreLiteral, matchIgnoreName, matchIgnoreRef, cl)
        return newAttr

    def parseEdge(self, cl, edge):
        matchIgnore = edge[JSON.EDGE_MATCH_IGNORE] if JSON.EDGE_MATCH_IGNORE in edge else False
        matchIgnoreLiteral = edge[JSON.EDGE_MATCH_IGNORE_LITERAL] if JSON.EDGE_MATCH_IGNORE_LITERAL in edge else False
        matchIgnoreName = edge[JSON.EDGE_MATCH_IGNORE_NAME] if JSON.EDGE_MATCH_IGNORE_NAME in edge else False
        matchIgnoreRef = edge[JSON.EDGE_MATCH_IGNORE_REF] if JSON.EDGE_MATCH_IGNORE_REF in edge else False
        newEdge = Edge(edge[JSON.EDGE_NAME], edge[JSON.EDGE_KIND], edge[JSON.EDGE_MULT], edge[JSON.EDGE_TYPE], matchIgnore, matchIgnoreLiteral, matchIgnoreName, matchIgnoreRef, cl)
        return newEdge
        
    def parseClass(self, scope, cl):
        newClass = Class(cl[JSON.CLASS_NAME], scope)

        if JSON.CLASS_EXTENDS in cl:
            extCl = scope.getClassByName(cl[JSON.CLASS_EXTENDS])
            if extCl is None:
                extCl = self.root.getClassByFullName(cl[JSON.CLASS_EXTENDS])
            if extCl is None:
                raise YamlError("Could not get parent class for class: " + newClass.name)
            else:
                if not extCl.abstract:
                    raise YamlError("Parent class has to be abstract: " + newClass.name + " parent: " + extCl.name)
                else:
                    newClass.extends = extCl
                    extCl.subClasses.append(newClass)
        
        if JSON.CLASS_ABSTRACT in cl:
            newClass.abstract = cl[JSON.CLASS_ABSTRACT]

        if JSON.CLASS_ATTRIBUTES in cl:
            for attr in cl[JSON.CLASS_ATTRIBUTES]:
                newAttr = self.parseAttribute(newClass, attr)
                newClass.attributes.append(newAttr)
                    
        if JSON.CLASS_EDGES in cl:
            for edge in cl[JSON.CLASS_EDGES]:
                newEdge = self.parseEdge(newClass, edge)
                newClass.edges.append(newEdge)

        return newClass
    
    def parseScope(self, parent, scope):
        newScope = Scope(scope[JSON.SCOPE_NAME], parent)

        if (JSON.SCOPE_SCOPES in scope):
            for sc in scope[JSON.SCOPE_SCOPES]:
                scope.scopes.append(self.parseScope(scope, sc))


        if (JSON.SCOPE_CLASSES in scope):
            for cl in scope[JSON.SCOPE_CLASSES]:
                newClass = self.parseClass(newScope, cl) 
                newScope.classes.append(newClass)

        if (JSON.SCOPE_ENUMS in scope):
            for e in scope[JSON.SCOPE_ENUMS]:
                newEnum = self.parseEnum(newScope, e)
                newScope.enums.append(newEnum)
                    
        return newScope
    
    def parseEnum(self, parent, enum):
        newEnum = Enum(enum[JSON.ENUM_NAME], parent)
        
        if JSON.ENUM_FIELDS in enum:
            for field in enum[JSON.ENUM_FIELDS]:
                newEnum.fields.append(field)
                
        return newEnum
    
    def resolveType(self, scope, _type):
        cl = scope.getClassByFullName(_type.name)
        if cl:
            _type.pointsTo = cl
        else:
            _type.pointsTo = self.root.getClassOrEnumByFullName(_type.name)
    
    def resolveTypes(self):
        for cl in self.root.getAllClasses():
            for attr in cl.attributes:
                self.resolveType(cl.parent, attr.type)
            for e in cl.edges:
                self.resolveType(cl.parent, e.type)
        

    def parseInputJSON(self):
        f = open(self.input, "r")
        parsed_json = json.load(f)

        self.root = Scope(JSON.SCOPE_ROOT, "")
        
        for scope in parsed_json[JSON.SCOPE_SCOPES]:
            self.root.scopes.append(self.parseScope(self.root, scope))
        
        f.close()
        return self.root;
    
    def parseInputYAML(self):
        f = open(self.input, "r")
        parsed_yaml = yaml.load(f, Loader=yaml.Loader)

        self.root = Scope(JSON.SCOPE_ROOT, "")
        
        for scope in parsed_yaml[JSON.SCOPE_SCOPES]:
            self.root.scopes.append(self.parseScope(self.root, scope))
        
        self.resolveTypes()
        
        f.close()
        return self.root;

