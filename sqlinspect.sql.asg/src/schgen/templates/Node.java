package {{ mypackage }};

{% for i in imports %}import {{ i }};
{% endfor %}

{% if cl.name != consts.BASE %}
import sqlinspect.sql.asg.base.*;
{% endif %}
import sqlinspect.sql.asg.common.*;
import sqlinspect.sql.asg.visitor.*;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

@SuppressWarnings("unused")
@JsonIdentityInfo(
generator = ObjectIdGenerators.PropertyGenerator.class,
property = "id")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME,  include = JsonTypeInfo.As.PROPERTY,  property = "type")
{% if cl.subClasses %}
@JsonSubTypes({
  {% for s in cl.subClasses %}
  @JsonSubTypes.Type(value = {{ s | getClassFullName }}.class, name="{{s.name}}"),
  {% endfor %}
})
{% endif %}

/**
 * The class implementation representing a {{ cl.name }} node in the AST.
 */
public {% if cl.abstract %}abstract{% endif %} class {{ cl.name }} {% if cl.extends %} extends {{ cl.extends | getClassFullName }} {% endif %} {% if cl.name == consts.BASE %} implements java.io.Serializable {% endif %}
{
  private static final long serialVersionUID = {{ serialuid }}L;
  private static final Logger LOG = LoggerFactory.getLogger({{ cl.name }}.class);

  /**
   * Default constructor. Normally this should not be used, but it is needed for the JSON serialization.
   */
  public {{ cl.name }}() {
    super();
  }
{% if cl.name != consts.BASE %}
 /**
  * Use this construction to instantiate a node.
  * @param id the identifier of the node (in the main container).
  */
  public {{ cl.name}}(int id) {
   super(id);
  }
{% endif %}

{% if cl.name != consts.BASE and not cl.abstract %}
  /**
   * Returns the node kind of the instance.
   */
  @Override
  public NodeKind getNodeKind() {
    return NodeKind.{{ cl.name | upper }};
  }
{% endif %}

{% if cl.name == consts.BASE %}
  /**
   * Reference to the parent node in the AST.
   */
  @JsonBackReference
  protected {{ consts.BASE }} parent;

  public {{ consts.BASE }}(int id) {
    this._id = id;
  }

  public {{ consts.BASE}} getParent() {
    return parent;
  }

  public void setParent({{consts.BASE}} parent) {
    if (this.parent==null) {
      this.parent = parent;
    } else {
      if (!this.parent.equals(parent)) {
        LOG.trace("Node " + this.toString() + " already had a parent node! (old: " + this.parent.toString() + ", new: " + parent.toString());
      }
    }
  }

  @JsonIgnore  public abstract NodeKind getNodeKind();
  
  @Override
  public String toString() {
    return "[Id: " + _id + " NodeKind: " + getNodeKind() + "]";
  }
  
{% endif %}

{% for attr in cl.attributes %}
 /**
  * <code>{{ attr.name }}</code> attribute of the node.
  */
  protected {{ attr.type | getType }} {{ attr | getAttrInternalName }};

  public {{ attr.type | getType }} {{ attr | getAttrGetterName }}() {
    return {{ attr | getAttrInternalName }};
  }

  public void {{ attr | getAttrSetterName }}({{ attr.type | getType }} {{ attr | getAttrInternalName }}) {
    this.{{ attr | getAttrInternalName }} = {{ attr | getAttrInternalName }};
  }
{% endfor %}

{% for e in cl.edges %}
{% if e.kind == JSON.EDGE_TREE %}
  {% if e.mult == JSON.MULT_ONE %}
        /**
         * <code>{{ e.name }}</code> tree edge.
         */
        @JsonManagedReference
        protected {{ e.type | getType }} {{ e | getEdgeInternalName }};

        public {{ e.type | getType }}  {{ e | getEdgeGetterName }}() {
          return {{ e | getEdgeInternalName }};
        }

        public void {{ e | getEdgeSetterName }}({{ e.type | getType }} {{ e | getEdgeInternalName }}) {
          this.{{ e | getEdgeInternalName }} = {{ e | getEdgeInternalName }};
          {{ e | getEdgeInternalName }}.setParent(this);
        }
  {% elif e.mult == JSON.MULT_MANY %}
        /**
         * <code>{{ e.name }}</code> tree edge.
         */
        protected {{ consts.EDGE_MANY_TYPE}}<{{ e.type | getType }}> {{ e | getEdgeInternalName }} = new {{ consts.EDGE_MANY_TYPE}}<>();

        public {{ consts.EDGE_MANY_TYPE}}<{{ e.type | getType }}> {{ e | getEdgeGetterName }}() {
          return {{ e | getEdgeInternalName }};
        }

        public void {{ e | getEdgeSetterName }}({{ e.type | getType }} {{ e | getEdgeInternalName | nonPlural }}) {
          this.{{ e | getEdgeInternalName }}.add({{ e | getEdgeInternalName | nonPlural }});
          {{ e | getEdgeInternalName | nonPlural }}.setParent(this);
        }
  {% else %}
  /* UNHANDLED EDGE MULTIPLICITY {{ e.name }}: {{ e.mult }} */
  {% endif %}
{% elif e.kind == JSON.EDGE_CROSS %}
  {% if e.mult == JSON.MULT_ONE %}
        /**
         * <code>{{ e.name }}</code> cross edge.
         */
        @JsonManagedReference
        protected {{ e.type | getType }} {{ e | getEdgeInternalName }};

        public {{ e.type | getType }}  {{ e | getEdgeGetterName }}() {
          return {{ e | getEdgeInternalName }};
        }

        public void {{ e | getEdgeSetterName }}({{ e.type | getType }} {{ e | getEdgeInternalName }}) {
          this.{{ e | getEdgeInternalName }} = {{ e | getEdgeInternalName }};
        }
  {% elif e.mult == JSON.MULT_MANY %}
       /**
       * <code>{{ e.name }}</code> cross edge.
       */
       protected {{ consts.EDGE_MANY_TYPE}}<{{ e.type | getType }}> {{ e | getEdgeInternalName }} = new {{ consts.EDGE_MANY_TYPE}}<>();

       public {{ consts.EDGE_MANY_TYPE}}<{{ e.type | getType }}> {{ e | getEdgeGetterName }}() {
         return {{ e | getEdgeInternalName }};
       }

       public void {{ e | getEdgeSetterName }}({{ e.type | getType }} {{ e | getEdgeInternalName | nonPlural }}) {
        this.{{ e | getEdgeInternalName }}.add({{ e | getEdgeInternalName | nonPlural }});
       }
  {% else %}
  /* UNHANDLED EDGE MULTIPLICITY {{ e.name }}: {{ e.mult }} */
  {% endif %}
{% else %}
/* UNHANDLED EDGE KIND {{ e.name }}: {{ e.kind }} */
{% endif %}
{% endfor %}

   {% if cl.name != consts.BASE %}
   @Override
   {% endif %}
   public boolean match({{ consts.BASE }} arg) {
	   return match(arg, false, false, false);
   }

  /**
   * Checks if the AST node matches another AST node. Two nodes match if their nodekind and their attributes are equal.

   * @param arg the AST node to compare the node with.
   * @return   true if the nodes match, false otherwise.
   */
   {% if cl.name != consts.BASE %}
   @Override
   {% endif %}
  public boolean match({{ consts.BASE }} arg, boolean ignoreLiterals, boolean ignoreNames, boolean ignoreRefs) {
    if (arg == null) {
     return false;
    }
    
    {% if cl.name != "Joker" %} {# testjoker #}

    if (arg.getNodeKind() == NodeKind.JOKER) {
    	return true;
    }
    
    {% if cl.name != consts.BASE %}
    if (!super.match(arg, ignoreLiterals, ignoreNames, ignoreRefs)) {
      LOG.trace("Node doesn't match: " + this.toString() + " - " + arg.toString());
      return false;
    }
    {% else %}
    if (arg.getNodeKind() != getNodeKind()) {
      LOG.trace("Node doesn't match: " + this.toString() + " - " + arg.toString());
      return false;
    }
    {% endif %}
    
    {# we have to play a little trick here to count the attributes so we don't generate dead statements later #}
    {% set count = [] %}
    {% for attr in cl.attributes %}
      {% if attr.matchIgnore %}
      // Ignored attribute : {{ attr.name }}
      {% else %}
      {% if count.append(1) %}{% endif %}
      {% endif %}
    {% endfor %}

    {% if count %} {# testcount #}
    
    {{ cl.name }} n = ({{cl.name}})arg; 
    
    {% for attr in cl.attributes if not attr.matchIgnore %}
      {% if attr.type is javaPrimitive %}
	if ({{ attr | getAttrGetterName}}() != n.{{ attr | getAttrGetterName }}()) { return false; }
      {% else %}
      {% if attr.matchIgnoreLiteral %}
      if (!ignoreLiterals) {
      {% elif attr.matchIgnoreName %}
      if (!ignoreNames) {
      {% elif attr.matchIgnoreRef %}
      if (!ignoreRefs) {
      {% endif %}
        if ({{ attr | getAttrGetterName}}() != null && n.{{ attr | getAttrGetterName }}() == null ) {
          LOG.trace("Attr {{ attr.name }} doesn't match: " + this.toString() + " - " + n.toString());
          return false;
        }
        if ({{ attr | getAttrGetterName}}() == null && n.{{ attr | getAttrGetterName }}() != null ) {
          LOG.trace("Attr {{ attr.name }} doesn't match: " + this.toString() + " - " + n.toString());
          return false;
        }
        if ({{ attr | getAttrGetterName}}() != null && n.{{ attr | getAttrGetterName }}() != null && ! {{ attr | getAttrGetterName}}().equals(n.{{ attr | getAttrGetterName}}())) {
          LOG.trace("Attr {{ attr.name }} doesn't match: " + this.toString() + " - " + n.toString());
          return false;
        }
      {% if attr.matchIgnoreLiteral %}
      }
      {% elif attr.matchIgnoreName %}
      }
      {% elif attr.matchIgnoreRef %}
      }
      {% endif %}
      {% endif %}
    {% endfor %}

    {% endif %} {# testcount #}
    {% endif %} {# testjoker #}
    return true;
  }

  /**
   * Checks if the AST node and its subtree matches another AST node and its subtree.
   * The nodes and their subtrees match if their attributes are equal and the nodes following
   * their tree edges match too.

   * @param arg the AST node to compare the node with.
   * @return   true if the nodes and their subtree match, false otherwise.
   */
  {% if cl.name != consts.BASE %}
  @Override
  {% endif %}
  public boolean matchTree(Base arg, boolean ignoreLiterals, boolean ignoreNames, boolean ignoreRefs) {
    if (arg == null) {
      return false;
    }

    {% if cl.name != "Joker" %} {# testjoker #}
    
    if (arg.getNodeKind() == NodeKind.JOKER) {
      return true;
    }

    {% if cl.name != consts.BASE %}
    if (!super.matchTree(arg, ignoreLiterals, ignoreNames, ignoreRefs)) {
      LOG.trace("Node doesn't match: " + this.toString() + " - " + arg.toString());
      return false;
    }
    {% endif %}
    if (!match(arg, ignoreLiterals, ignoreNames, ignoreRefs)) {
      LOG.trace("Node doesn't match: " + this.toString() + " - " + arg.toString());
      return false;
    }
    
    {# we have to play a little trick here to count the edges so we don't generate dead statements later #}
    {% set count = [] %}
    {% for e in cl.edges %}
      {% if e.matchIgnore %}
      // Ignored edge : {{ e.name }}
      {% elif e.kind == JSON.EDGE_TREE %}
      {% if count.append(1) %}{% endif %}
      {% else %}
      // Non-tree edge: {{ e.name }} ({{e.kind}})
      {% endif %}
    {% endfor %}

    {% if count %} {# testcount #}
    {{ cl.name }} n = ({{cl.name}})arg; 
    
    {% for e in cl.edges if e.kind == JSON.EDGE_TREE%}
    {% if e.matchIgnoreLiteral %}
    if (!ignoreLiterals) {
    {% elif e.matchIgnoreName %}
    if (!ignoreNames) {
    {% elif e.matchIgnoreRef %}
    if (!ignoreRefs) {
    {% endif %}
    {% if e.mult == JSON.MULT_ONE %}
    if (({{ e | getEdgeGetterName}}() == null && n.{{ e | getEdgeGetterName}}() != null) || (n.{{ e | getEdgeGetterName}}() == null && {{ e | getEdgeGetterName}}() != null)) {
     LOG.trace("Edge {{ e.name }} doesn't match: " + this.toString() + " - " + n.toString());
     return false;
    }
    if ({{ e | getEdgeGetterName}}() != null && n.{{ e | getEdgeGetterName}}() != null) {
      try {
        boolean res = {{ e | getEdgeGetterName}}().matchTree(n.{{ e | getEdgeGetterName}}(), ignoreLiterals, ignoreNames, ignoreRefs);
        if (!res) {
          LOG.trace("Edge {{ e.name }} doesn't match: " + this.toString() + " - " + n.toString());
          return false;
        } else {
            LOG.trace("Edge {{ e.name }} match: " + this.toString() + " - " + n.toString());
        }
      } catch (Throwable e) { // NOPMD - MethodHandle.invoke throws throwable
        LOG.error("Failed to invoke subtreematch", e);
      }
    }
    {% elif e.mult == JSON.MULT_MANY %}
    if ({{ e | getEdgeGetterName}}()!=null && n.{{ e | getEdgeGetterName}}()!=null) {
      {{ e.type.shortName }}[] e1_{{ e.type.shortName }} = {{ e | getEdgeGetterName}}().toArray(new {{e.type.shortName}}[{{ e | getEdgeGetterName}}().size()]);
      {{ e.type.shortName }}[] e2_{{ e.type.shortName }} = n.{{ e | getEdgeGetterName}}().toArray(new {{e.type.shortName}}[n.{{ e | getEdgeGetterName}}().size()]);
      if (e1_{{e.type.shortName}}.length != e2_{{e.type.shortName}}.length) {
        LOG.trace("Edge {{ e.name }} doesn't match: " + this.toString() + " - " + n.toString());
        return false;
      }
      for (int i=0; i<e1_{{e.type.shortName}}.length; i++) {
        try {
          boolean res = e1_{{e.type.shortName}}[i].matchTree(e2_{{e.type.shortName}}[i], ignoreLiterals, ignoreNames, ignoreRefs);
          if (!res) {
            LOG.trace("Edge {{ e.name }} doesn't match: " + this.toString() + " - " + n.toString());
            return false;
          } else {
            LOG.trace("Edge {{ e.name }} match: " + this.toString() + " - " + n.toString());
          }
        } catch (Throwable e) { // NOPMD - MethodHandle.invoke throws throwable
          LOG.error("Failed to invoke subtreematch", e);
          return false;
        }
      }
    } else if ({{ e | getEdgeGetterName}}() != n.{{ e | getEdgeGetterName}}()) {
      LOG.trace("Edge {{ e.name }} doesn't match: " + this.toString() + " - " + n.toString());
      return false;
    }
    {% else %}
    /* UNHANDLED EDGE MULTIPLICITY {{ e.name }}: {{ e.mult }} */
    {% endif %}
    {% if e.matchIgnoreLiteral %}
    }
    {% elif e.matchIgnoreName %}
    }
    {% elif e.matchIgnoreRef %}
    }
    {% endif %}
    {% endfor %}
    
    {% endif %} {# testcount #}
    {% endif %} {# testjoker #}

    return true;
  }

 {% if cl.name == consts.BASE %}
    public abstract void accept(AbstractVisitor visitor);

    public abstract void acceptEnd(AbstractVisitor visitor);
{% elif not cl.abstract %}
    @Override
    public void accept(AbstractVisitor visitor) {
      visitor.visit(this);
    }

    @Override
    public void acceptEnd(AbstractVisitor visitor) {
      visitor.visitEnd(this);
    }
{% endif %}

  {% if cl.name == "Named" or cl.name == "Id" %}
  @Override
  public String toString() {
	  return "["
       + "Id: " + getId() + " Name: " + getName() + " NodeKind: " + getNodeKind()
       + "]";
  }
  {% endif %}

}
