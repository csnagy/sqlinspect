package {{ mypackage }};

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

{% for i in imports %}import {{ i }};
{% endfor %}

@SuppressWarnings("unused")
public class VisitorXML extends Visitor {
  private static final Logger LOG = LoggerFactory.getLogger(VisitorXML.class);

  private static final String TAB = "  ";

  private PrintWriter pw;

  public VisitorXML(String filename) {
	super();
    try {
      pw = new PrintWriter(filename, StandardCharsets.UTF_8.name());
    } catch (java.io.FileNotFoundException e) {
        LOG.error("Could not open file for XML output!", e);
    } catch (UnsupportedEncodingException e) {
        LOG.error("Unsupported character set!", e);
	}
  }

  public VisitorXML(PrintWriter writer) {
	super();
    this.pw = writer;
  }

  public void startXML() {
    pw.println("<?xml version='1.0' encoding=\"utf-8\"?>");
  }

  public void endXML() {
    pw.close();
  }

  public void printIndentation() {
    for (int i=0; i<getDepth(); i++) { pw.print(TAB); }
  }

  {% for cl in classes %}
  {% if not cl.abstract %}
  @Override
  {% endif %}
  public void visit({{ cl | getClassFullName }} node) {
    printIndentation();
    pw.print("{{ cl.name }}");
    writeAttributes(node);
    pw.println(">");
  }

  {% if not cl.abstract %}
  @Override
  {% endif %}
  public void visitEnd({{ cl | getClassFullName }} node) {
    printIndentation();
    pw.println("{{ cl.name }}");
  }
  {% endfor %}

  {% for cl in classes %}
  public void writeAttributes({{ cl | getClassFullName }} node) {
    {% if cl.extends %}
    writeAttributes(({{ cl.extends.name }})node);
    {% endif %}
    {% for attr in cl.attributes %}
    pw.print(" {{ attr.name }} =\""+node.{{ attr | getAttrGetterName }}() + "\"");
    {% endfor %}
  }   
  {% endfor %}
}
