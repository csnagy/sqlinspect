package {{ mypackage }};

{% for i in imports %}
import {{ i }};
{% endfor %}

@SuppressWarnings("unused")
public abstract class AbstractVisitor {
  private int depth;

  public void incDepth() { ++depth; };
  public void decDepth() { --depth; };

  public int getDepth() { return depth; };

  protected AbstractVisitor() {
    depth=0;
  }

	
  {% for cl in classes if not cl.abstract %}
  public abstract void visit({{ cl | getClassFullName }} node);
  
  public abstract void visitEnd({{cl | getClassFullName }} node);
  {% endfor %}
  
  {% for cl in classes %}
  {% for e in cl.edges if e is treeEdge %}
  public abstract void visitEdge_{{ e | getEdgeVisitMethodName }}({{ cl.name }} node, {{ e.type | getType }} end);
  
  public abstract void visitEdgeEnd_{{ e | getEdgeVisitMethodName }}({{ cl.name }} node, {{ e.type | getType }} end);
  {% endfor %}
  {% endfor %}
}
