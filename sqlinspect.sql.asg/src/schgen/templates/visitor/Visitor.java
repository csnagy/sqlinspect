package {{ mypackage }};

{% for i in imports %}
import {{ i }};
{% endfor %}

@SuppressWarnings("unused")
public class Visitor extends AbstractVisitor {  
  {% for cl in classes if not cl.abstract %}
  @Override
  public void visit({{ cl | getClassFullName }} node) {
	  //to override
  }
  
  @Override
  public void visitEnd({{cl | getClassFullName }} node) {
	  //to override
  }
  {% endfor %}

  {% for cl in classes %}
  {% for e in cl.edges if e is treeEdge %}
  @Override
  public void visitEdge_{{ e | getEdgeVisitMethodName }}({{ cl.name }} node, {{ e.type | getType }} end) {
	  //to override
  }
  
  @Override
  public void visitEdgeEnd_{{ e | getEdgeVisitMethodName }}({{ cl.name }} node, {{ e.type | getType }} end) {
	  //to override	  
  }
  {% endfor %}
  {% endfor %}
}
