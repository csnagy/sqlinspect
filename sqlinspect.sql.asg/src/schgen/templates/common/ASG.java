package {{ mypackage }};

import java.util.ArrayList;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

{% for i in imports %}import {{ i }};
{% endfor %}

/**
 * Owner of the Abstract Semantic Graph (the Abstract Syntax Tree extended with
 * semantic information, cross edges) and the AST Node factory. An instance
 * holds the AST nodes. New nodes can be created using the createX factory
 * methods.
 */
@SuppressWarnings("unused")
public class ASG implements java.io.Serializable {
  private static final long serialVersionUID = {{ serialuid }}L;
  private static final Logger LOG = LoggerFactory.getLogger(ASG.class);
  
  private final ArrayList< {{ consts.BASE }} > nodes =  new ArrayList<>();
  private {{ consts.ROOT }} root;

  public ASG() {
    try {
      root=({{ consts.ROOT }})createNode({{consts.ROOT_KIND}},{{consts.ROOT_INDEX}});
    } catch (ASGException e) {
      LOG.error("Error creating root node!", e);
    }
  };

  public {{consts.ROOT}} getRoot() {
    return root;
  };

  public {{consts.BASE}} getRef(int id) {
    {{consts.BASE}} node = null;
    if (id < nodes.size()) {
      node = nodes.get(id);
    }
    if (node == null) {
      throw new ASGException("Invalid node id: " + id);
    }
    return node;
  }

  public {{consts.BASE}} createNode(NodeKind kind) {
    return createNode(kind, nodes.size());
  }

  private {{consts.BASE}} createNode(NodeKind kind, int id) {
    {{consts.BASE}} ret = null;
    switch (kind) {
      {% for cl in classes if not cl.abstract %}
        case {{ cl.name | upper }}: ret = new {{ cl.name }}(id);break;
      {% endfor %}
      default: throw new ASGException("NodeKind cannot be instantiated: " + kind.name());
    }

    if (nodes.size() < id) {
      nodes.addAll(Collections.<{{ consts.BASE }}>nCopies(id - nodes.size(), null));
    }

    if (nodes.size() == id) {
      nodes.add(ret);
    } else {
      nodes.set(id, ret);
    }

    return ret;
  }
  
  public void clearNodes() {
    nodes.clear();
    try {
      root=({{ consts.ROOT }})createNode({{consts.ROOT_KIND}},{{consts.ROOT_INDEX}});
    } catch (ASGException e) {
      LOG.error("Error creating root node!", e);
    }
  }
}
