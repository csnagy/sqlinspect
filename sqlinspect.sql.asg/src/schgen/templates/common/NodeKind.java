package {{ mypackage }};

public enum NodeKind {
  {% for class in classes %}{{ class.name | upper }}{% if not loop.last %},{% endif %}
  {% endfor %}
}
