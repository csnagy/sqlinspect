package {{ mypackage }};

import sqlinspect.sql.asg.base.Base;

public interface ITraversal {
  void traverse(Base node);
}
