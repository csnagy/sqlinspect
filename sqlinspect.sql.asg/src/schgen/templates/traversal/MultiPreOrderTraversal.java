package {{ mypackage }};

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.visitor.AbstractVisitor;

{% for i in imports %}
import {{ i }};
{% endfor %}

@SuppressWarnings("unused")
public class MultiPreOrderTraversal implements ITraversal {
  protected final List<AbstractVisitor> visitors = new ArrayList<>();

  public void addVisitor(AbstractVisitor visitor) {
    this.visitors.add(visitor);
  }
  
  @Override
  public void traverse(Base node) {
    if (node == null) {
      return;
    }

    for (AbstractVisitor visitor : visitors) {
      node.accept(visitor);
      visitor.incDepth();
    }
     
    switch (node.getNodeKind()) {
    {% for cl in classes %}
    {% if cl.abstract %}
    /* Abstract {{ cl.name }} */
    {% else %}
    case {{ cl.name | upper}}:
  	  traverseNode(({{ cl.name }})node);break;
    {% endif%}
    {% endfor %}
    default:
  	  throw new ASGException("Invalid node kind: " + node.getNodeKind().toString());
  }

    for (AbstractVisitor visitor : visitors) {
      node.acceptEnd(visitor);
      visitor.decDepth();
    }
  }

  {% for cl in classes %}
  protected void traverseNode({{cl.name}} node) {
  {% if cl.extends %}
    traverseNode(({{cl.extends.name }})node);
  {% endif %}
  {% for e in cl.edges %}
    {% if e.kind == JSON.EDGE_TREE %}
      {% if e.mult == JSON.MULT_ONE %}
      for (AbstractVisitor visitor : visitors) {
        visitor.visitEdge_{{ e | getEdgeVisitMethodName }}(node, node.{{ e | getEdgeGetterName}}());
      }
        traverse(node.{{ e | getEdgeGetterName}}());
      for (AbstractVisitor visitor : visitors) {
        visitor.visitEdgeEnd_{{ e | getEdgeVisitMethodName }}(node, node.{{ e | getEdgeGetterName}}());
      }
      {% elif e.mult == JSON.MULT_MANY %}
      for ({{ e.type | getType }} n : node.{{ e | getEdgeGetterName }}()) {
        for (AbstractVisitor visitor : visitors) {
          visitor.visitEdge_{{ e | getEdgeVisitMethodName }}(node, n);
        }
        traverse(n);
        for (AbstractVisitor visitor : visitors) {
          visitor.visitEdgeEnd_{{ e | getEdgeVisitMethodName }}(node, n);
        }
      }
      {% else %}
      /* IGNORED EDGE MULTIPLICITY {{ e.name }}: {{ e.mult }} ! */
      {% endif %}
    {% else %}
    /* IGNORED EDGE KIND {{ e.name }}: {{ e.kind }} ! */
    {% endif %}
  {% endfor %}
  }
  {% endfor %}
}
