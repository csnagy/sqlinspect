package {{ mypackage }};

import java.util.Iterator;

import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.visitor.AbstractVisitor;

{% for i in imports %}
import {{ i }};
{% endfor %}

@SuppressWarnings("unused")
public class PreOrderTraversal implements ITraversal {
  protected final AbstractVisitor visitor;

  public PreOrderTraversal(Visitor visitor) {
    this.visitor=visitor;
  }
  
  @Override
  public void traverse(Base node) {
    if (node == null) {
      return;
    }

    node.accept(visitor);
    visitor.incDepth();
     
    switch (node.getNodeKind()) {
      {% for cl in classes %}
      {% if cl.abstract %}
      /* Abstract {{ cl.name }} */
      {% else %}
      case {{ cl.name | upper}}:
    	  traverseNode(({{ cl.name }})node);break;
      {% endif%}
      {% endfor %}
      default:
    	  throw new ASGException("Invalid node kind: " + node.getNodeKind().toString());
    }

    node.acceptEnd(visitor);
    visitor.decDepth();
  }

  {% for cl in classes %}
  protected void traverseNode({{cl.name}} node) {
  {% if cl.extends %}
    traverseNode(({{cl.extends.name }})node);
  {% endif %}
  {% for e in cl.edges %}
    {% if e.kind == JSON.EDGE_TREE %}
      {% if e.mult == JSON.MULT_ONE %}
      visitor.visitEdge_{{ e | getEdgeVisitMethodName }}(node, node.{{ e | getEdgeGetterName}}());
      traverse(node.{{ e | getEdgeGetterName}}());
      visitor.visitEdgeEnd_{{ e | getEdgeVisitMethodName }}(node, node.{{ e | getEdgeGetterName}}());
      {% elif e.mult == JSON.MULT_MANY %}
      for ({{ e.type | getType }} n : node.{{ e | getEdgeGetterName }}()) {
        visitor.visitEdge_{{ e | getEdgeVisitMethodName }}(node, n);
        traverse(n);
        visitor.visitEdgeEnd_{{ e | getEdgeVisitMethodName }}(node, n);
      }
      {% else %}
      /* IGNORED EDGE MULTIPLICITY {{ e.name }}: {{ e.mult }} ! */
      {% endif %}
    {% else %}
    /* IGNORED EDGE KIND {{ e.name }}: {{ e.kind }} ! */
    {% endif %}
  {% endfor %}
  }
  {% endfor %}
}
