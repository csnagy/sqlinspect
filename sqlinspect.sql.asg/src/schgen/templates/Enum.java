package {{ mypackage }};

public enum {{ cl.name }}
{
  {% for field in cl.fields %}
  {{ field | upper }}{% if not loop.last %},{% endif %}
  {% endfor %}
}

