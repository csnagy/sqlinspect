package {{ mypackage }};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sqlinspect.sql.asg.common.*;

{% for i in imports %}import {{ i }};
{% endfor %}

@SuppressWarnings("unused")
public {% if cl.abstract %}abstract{% endif %} class {{cl.name}}Builder {
  private static final Logger LOG = LoggerFactory.getLogger({{ cl.name }}Builder.class);

  protected {{cl.name}} _item;

  public {{cl.name}}Builder(ASG asg) {
    {% if not cl.abstract %}
    this._item = ({{cl.name}})asg.createNode(NodeKind.{{cl.name | upper}});
    {% endif %}
  }

  public {{cl.name}}Builder(ASG asg, {{cl.name}} _item) {
    if (_item == null) {
      this._item = ({{cl.name}})asg.createNode(NodeKind.{{cl.name | upper}});
    } else {
      this._item = _item;
    }
  }

  public {{cl.name}} result() {
    return _item;
  }

  {% for attr in inheritedAttributes %}
  public {{cl.name}}Builder {{ attr | getAttrSetterName}}({{attr.type | getType}} arg) {
    {% if attr.type is javaPrimitive %}
    _item.{{ attr | getAttrSetterName}}(arg);
    {% else %}
    if (arg!=null) {
      _item.{{ attr | getAttrSetterName}}(arg);
    } else {
      LOG.warn("Null parameter for {{ attr | getAttrSetterName}}!");
    }
    {% endif %}
    return this;
  }
  {% endfor %}

  {% for attr in cl.attributes %}
  public {{cl.name}}Builder {{ attr | getAttrSetterName}}({{attr.type | getType}} arg) {
    {% if attr.type is javaPrimitive %}
    _item.{{ attr | getAttrSetterName}}(arg);
    {% else %}
    if (arg!=null) {
      _item.{{ attr | getAttrSetterName}}(arg);
    } else {
      LOG.warn("Null parameter for {{ attr | getAttrSetterName}}!");
    }
    {% endif %}
    return this;
  }
  {% endfor %}  
  
  {% for edge in inheritedEdges %}
  public {{cl.name}}Builder {{ edge | getEdgeSetterName}}({{edge.type | getType}} arg) {
    {% if edge.type is javaPrimitive %}
    _item.{{ edge | getEdgeSetterName}}(arg);
    {% else %}
    if (arg!=null) {
      _item.{{ edge | getEdgeSetterName}}(arg);
    } else {
      LOG.warn("Null parameter for {{ edge | getEdgeSetterName}}!");
    }
    {% endif %}
    return this;
  }
  {% endfor %}

  {% for edge in cl.edges %}
  public {{cl.name}}Builder {{ edge | getEdgeSetterName}}({{edge.type | getType}} arg) {
    {% if edge.type is javaPrimitive %}
    _item.{{ edge | getEdgeSetterName}}(arg);
    {% else %}
    if (arg!=null) {
      _item.{{ edge | getEdgeSetterName}}(arg);
    } else {
      LOG.warn("Null parameter for {{ edge | getEdgeSetterName}}!");
    }
    {% endif %}
    return this;
  }
  {% endfor %}
}
