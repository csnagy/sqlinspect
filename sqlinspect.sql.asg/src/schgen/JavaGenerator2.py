import os
import logging
import hashlib

from JSONConsts import JSON

from jinja2 import Environment, PackageLoader, FileSystemLoader

from JavaGeneratorConstants import JavaGeneratorConstants as JGConsts

class JavaGenerator2:
    """Java Generator class"""
    
    # default output directory
    output = "."
    serialUID = 17760704
    eng = None

    def __init__(self, output, package, root):
        self.output = output
        self.package = package
        self.root = root

    def generateSchema(self):
        templates_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates')
        logging.info("Loading templates from directory: " + templates_dir)
        self.env = Environment(
            loader=FileSystemLoader(templates_dir),
            trim_blocks=True, # remove first new line after block, looks nicer for Java
            lstrip_blocks=True # strip leading spaces and tabs to a block, again for Java
        )
        self.env.filters['getClassFullName'] = self.getClassFullName
        self.env.filters['getAttrGetterName'] = self.getAttrGetterName
        self.env.filters['getAttrSetterName'] = self.getAttrSetterName
        self.env.filters['getAttrInternalName'] = self.getAttrInternalName
        self.env.filters['getEdgeGetterName'] = self.getEdgeGetterName
        self.env.filters['getEdgeSetterName'] = self.getEdgeSetterName
        self.env.filters['getEdgeInternalName'] = self.getEdgeInternalName
        self.env.filters['getEdgeVisitMethodName'] = self.getEdgeVisitMethodName
        self.env.filters['getType'] = self.getType
        self.env.filters['nonPlural'] = self.nonPlural
        self.env.tests['javaPrimitive'] = self.isJavaPrimitiveType
        self.env.tests['treeEdge'] = self.isTreeEdge
        
        self.generateScope(self.root)
        self.generateNodeKind()
        self.generateASG()
        self.generateAbstractVisitor()
        self.generateVisitor()
        self.generateVisitorXML()
        self.generateITraversal()
        self.generatePreOrderTraversal()
        self.generateMultiPreOrderTraversal()
        self.generateBuilders()
    
    def write(self, f, ind, s):
        f.write(self.TAB * ind + s)

    def isJavaPrimitiveType(self, _type):
        if self.getType(_type) in JGConsts.JAVA_PRIMITIVES:
            return True
        return False

    def isJavaPrimitive(self, _type):
        if _type in JGConsts.JAVA_PRIMITIVES:
            return True
        return False

    def getScopeDir(self, scope):
        if scope and scope.parent and scope.name != JSON.SCOPE_ROOT:
            return self.getScopeDir(scope.parent) + JGConsts.DSEP + scope.name
        else:
            if self.package:
                return self.output + JGConsts.DSEP + self.package.replace(JGConsts.PSEP, JGConsts.DSEP)
            else:
                return self.output
    
    def createDir(self, d):
        if not os.path.exists(d):
            os.makedirs(d)
        
    def getScopeFullName(self, sc):
        if (sc and sc.parent and sc.name != JSON.SCOPE_ROOT):
            return self.getScopeFullName(sc.parent) + JGConsts.PSEP + sc.name
        else:
            if self.package:
                return self.package
            else:
                return ""
        
    
    def getClassFullName(self, cl):
        return self.getScopeFullName(cl.parent) + JGConsts.PSEP + cl.name
    
    def getEdgeInternalName(self, e):
        if e.mult == JSON.MULT_ONE:
            return '_' + self.nonPlural(self.firstLower(e.name))
        elif e.mult == JSON.MULT_MANY: 
            return '_' + self.plural(self.firstLower(e.name))
        else:
            logging.error("Unhandled edge type! " + e.name + " kind: " + e.kind + "mult: " + e.mult)

    def getEdgeGetterName(self, e):
        if e.mult == JSON.MULT_ONE:
            if e.type.name == "Boolean":
                return "is" + self.nonPlural(self.firstUpper(e.name))
            return 'get' + self.nonPlural(self.firstUpper(e.name))
        elif e.mult == JSON.MULT_MANY: 
            return 'get' + self.plural(self.firstUpper(e.name))
        else:
            logging.error("Unhandled edge type! " + e.name + " kind: " + e.kind + "mult: " + e.mult)

    def getEdgeSetterName(self, e):
        if e.mult == JSON.MULT_ONE:
            return 'set' + self.nonPlural(self.firstUpper(e.name))
        elif e.mult == JSON.MULT_MANY: 
            return 'add' + self.nonPlural(self.firstUpper(e.name))
        else:
            logging.error("Unhandled edge type! " + e.name + " kind: " + e.kind + "mult: " + e.mult)
    
    def getAttrInternalName(self, a):
        return '_' + self.firstLower(a.name)

    def getAttrGetterName(self, a):
        if a.type.name == "Boolean":
            return "is" + self.firstUpper(a.name)
        return 'get' + self.firstUpper(a.name)

    def getAttrSetterName(self, a):
        return 'set' + self.firstUpper(a.name)
    
    def getEdgeVisitMethodName(self, e):
        return e.parent.name + "_" + e.name

    def isTreeEdge(self, e):
        return e.kind == JSON.EDGE_TREE
    
    def generateEnum(self, enum):
        logging.info("Create enum: " + enum.name)
        
        template = self.env.get_template("Enum.java")
        
        parent = enum.parent
        d = self.getScopeDir(parent)
        
        fname = d + JGConsts.DSEP + enum.name + ".java"
        
        f = open(fname, "w+")

        package = self.getScopeFullName(enum.parent)
        f.write(template.render(mypackage=package, cl=enum, consts=JGConsts, JSON=JSON))

        f.close()
        
    
    def generateScope(self, scope):
        logging.info("Create package: " + scope.name)
        d = self.getScopeDir(scope)
        
        if not os.path.exists(d):
            os.makedirs(d)
        
        for sc in scope.scopes:
            self.generateScope(sc)
        
        for cl in scope.classes:
            self.generateClass(cl)

        for e in scope.enums:
            self.generateEnum(e)

        logging.info("Package done.")
    
    def getClassImports(self, cl):
        imports = set()
        hasMany = False
        for f in cl.attributes:
            if f.type.pointsTo:
                imports.add(self.getClassFullName(f.type.pointsTo))
        for e in cl.edges:
            if e.type.pointsTo:
                imports.add(self.getClassFullName(e.type.pointsTo))
            if e.kind == JSON.MULT_MANY:
                hasMany = True
        if hasMany:
            imports.add(self.EDGE_MANY_TYPE)
        return imports
            
    def firstUpper(self, s):
        return s[0].capitalize() + s[1:]
    
    def firstLower(self, s):
        return s[0].lower() + s[1:]
    
    def plural(self, s):
        if s[-1] != 's':
            return s + 's'
        else:
            return s

    def nonPlural(self, s):
        if s[-1] == 's':
            return s[0:-1]
        else:
            return s
    
    def generateClass(self, cl):
        logging.info("Create class: " + cl.name)

        template = self.env.get_template("Node.java")
        
        parent = cl.parent
        d = self.getScopeDir(parent)
        
        fname = d + JGConsts.DSEP + cl.name + ".java"
        
        f = open(fname, "w+")
        
        package = self.getScopeFullName(cl.parent)
        imports = self.getClassImports(cl)
        cl_serialuid = self.serialUID
        self.serialUID = self.serialUID+1
        f.write(template.render(mypackage=package, imports=imports, cl=cl, serialuid=cl_serialuid, consts=JGConsts, JSON=JSON))
        f.close()

    def getType(self, _type):
        if (_type.name == "ID"):
            return "int"
        elif (_type.name == "int"):
            return "int" 
        elif (_type.name == "Boolean"):
            return "boolean" 
        elif (_type.name == "String"):
            return "String"
        else:
            return _type.shortName
    
    def generateImportAllScopes(self, f, ind):
        for sc in self.root.scopes:
            self.write(f, ind, "import " + self.getScopeFullName(sc) + JGConsts.PSEP + "*;\n")

        self.write(f, ind, "import " + self.package + JGConsts.PSEP + JGConsts.VISITOR + JGConsts.PSEP + "*;\n")
        self.write(f, ind, "\n")
    
    def importsForAllScopes(self):
        imports = list()
        for sc in self.root.scopes:
            imports.append(self.getScopeFullName(sc) + JGConsts.PSEP + "*")
        imports.append(self.package + JGConsts.PSEP + JGConsts.VISITOR + JGConsts.PSEP + "*")
        return imports

    def generateASG(self):
        logging.info("Create ASG Class")

        template = self.env.get_template("common/ASG.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.COMMON
        self.createDir(d)
        fname = d + JGConsts.DSEP + "ASG.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.COMMON
        imports = self.importsForAllScopes()
        asg_serialuid = self.serialUID
        self.serialUID = self.serialUID+1
        f.write(template.render(mypackage=package, imports=imports, serialuid=asg_serialuid, classes=self.root.getAllClasses(), consts=JGConsts))

        f.close()
        
    def generateNodeKind(self):
        logging.info("Create NodeKind")
        
        template = self.env.get_template("common/NodeKind.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.COMMON
        self.createDir(d)
        fname = d + JGConsts.DSEP + "NodeKind.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.COMMON
        f.write(template.render(mypackage=package, classes=self.root.getAllClasses()))

        f.close()

    def generateBuilder(self, cl):
        logging.info("Create builder for: " + cl.name)
        
        template = self.env.get_template("NodeBuilder.java")
        
        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.BUILDER
        self.createDir(d)
        fname = d + JGConsts.DSEP + cl.name + "Builder.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.BUILDER
        imports = self.importsForAllScopes()
        
        inheritedAttributes = list()
        inheritedEdges = list()
        
        clP = cl.extends
        while clP:
            inheritedAttributes.extend(clP.attributes)
            inheritedEdges.extend(clP.edges)
            clP = clP.extends

        
        f.write(template.render(mypackage=package, imports=imports, cl=cl, inheritedAttributes=inheritedAttributes, inheritedEdges=inheritedEdges, consts=JGConsts, JSON=JSON))
        
        f.close()

    def generateBuilderInterface(self):
        d = self.getScopeDir(self.root) + JGConsts.DSEP + self.BUILDER
        self.createDir(d)
        fname = d + JGConsts.DSEP + "Builder.java"
        f = open(fname, "w+")
        ind = 0

        self.write(f, ind, "package " + self.package + JGConsts.PSEP + JGConsts.BUILDER + ";\n\n")
        self.write(f, ind, "import " + self.ANTLR_TOKEN + ";\n")
        self.write(f, ind, "import " + self.package + JGConsts.PSEP + JGConsts.COMMON + ".*;\n")
        self.write(f, ind, "public interface Builder<T> {\n")
        self.write(f, ind + 1, "public T result();\n")
        self.write(f, ind, "}\n")

        f.close()


    def generateBuilders(self):
        logging.info("Create Builders")
        
#         self.generateBuilderInterface()
        
        for cl in self.root.getAllClasses():
            if not cl.abstract:
                self.generateBuilder(cl)
            

    def generateAbstractVisitor(self):
        logging.info("Create Abstract Visitor")

        template = self.env.get_template("visitor/AbstractVisitor.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.VISITOR
        self.createDir(d)
        fname = d + JGConsts.DSEP + "AbstractVisitor.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.VISITOR
        imports = self.importsForAllScopes()
        
        f.write(template.render(mypackage=package, imports=imports, classes=self.root.getAllClasses(), consts=JGConsts))

        f.close()

    def generateVisitor(self):
        logging.info("Create Visitor")

        template = self.env.get_template("visitor/Visitor.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.VISITOR
        self.createDir(d)
        fname = d + JGConsts.DSEP + "Visitor.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.VISITOR
        imports = self.importsForAllScopes()
        
        f.write(template.render(mypackage=package, imports=imports, classes=self.root.getAllClasses(), consts=JGConsts))

        f.close()


    def generateITraversal(self):
        logging.info("Create ITraversal")

        template = self.env.get_template("traversal/ITraversal.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.TRAVERSAL
        self.createDir(d)
        fname = d + JGConsts.DSEP + "ITraversal.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.TRAVERSAL
        f.write(template.render(mypackage=package, consts=JGConsts))

        f.close()

    def generatePreOrderTraversal(self):
        logging.info("Create PreOrderTraversal")

        template = self.env.get_template("traversal/PreOrderTraversal.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.TRAVERSAL
        self.createDir(d)
        fname = d + JGConsts.DSEP + "PreOrderTraversal.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.TRAVERSAL
        imports = self.importsForAllScopes()
        f.write(template.render(mypackage=package, imports=imports, classes=self.root.getAllClasses(), consts=JGConsts, JSON=JSON))

        f.close()

    def generateMultiPreOrderTraversal(self):
        logging.info("Create MultiPreOrderTraversal")

        template = self.env.get_template("traversal/MultiPreOrderTraversal.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.TRAVERSAL
        self.createDir(d)
        fname = d + JGConsts.DSEP + "MultiPreOrderTraversal.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.TRAVERSAL
        imports = self.importsForAllScopes()
        f.write(template.render(mypackage=package, imports=imports, classes=self.root.getAllClasses(), consts=JGConsts, JSON=JSON))

        f.close()

    def generateVisitorXML(self):
        logging.info("Create VisitorXML")

        template = self.env.get_template("visitor/VisitorXML.java")

        d = self.getScopeDir(self.root) + JGConsts.DSEP + JGConsts.VISITOR
        self.createDir(d)
        fname = d + JGConsts.DSEP + "VisitorXML.java"
        f = open(fname, "w+")
        
        package = self.package + JGConsts.PSEP + JGConsts.VISITOR
        imports = self.importsForAllScopes()
        f.write(template.render(mypackage=package, imports=imports, classes=self.root.getAllClasses(), consts=JGConsts))

        f.close()
