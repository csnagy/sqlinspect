class JavaGeneratorConstants:
    """Constants for Java Generator class"""

    # directory separator
    DSEP = '/'

    # package separator
    PSEP = '.'

    # the name of the common package
    COMMON = "common"

    # the name of the visitors package
    VISITOR = "visitor"

    # the name of the traversals package
    TRAVERSAL = "traversal"

    # the name of the builder package
    BUILDER = "builder"

    # name of the Base node
    BASE = "Base"

    # type of the Root node
    ROOT = "SQLRoot"

    # type of the Root node
    ROOT_KIND = "NodeKind.SQLROOT"

    # index of the Root node in the container
    ROOT_INDEX = 100


    # name of nodes container
    NODES = "nodes"

    ANTLR_TOKEN = "org.antlr.v4.runtime.Token"

    EDGE_MANY_TYPE = "java.util.ArrayList"
    EDGE_MANY_TYPE_SHORT = "ArrayList"

    JAVA_PRIMITIVES = [ "byte", "short", "int", "long", "float", "double", "boolean", "char" ]
