import sys, getopt, os
import logging

from YamlParser import YamlParser, YamlError
from JavaGenerator2 import JavaGenerator2

def _help():
    print('schgen.py -i <inputfile> -o <outputfile> -p <package>')
        
def main(argv):
    try:
        opts, _args = getopt.getopt(argv, "hi:o:p:", ["input=", "output=", "package="])
    except getopt.GetoptError:
        help()
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h':
            _help()
            sys.exit()
        elif opt in ("-i", "--input"):
            _input = arg
        elif opt in ("-o", "--output"):
            output = arg
        elif opt in ("-p", "--package"):
            package = arg
      
    if ((_input == "") or (output == "")):
        _help();
        sys.exit(2)
        
    logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
        
    try:
        if os.path.isdir(output):
            logging.info("Output directory already exists. Remove it first to regenerate the schema.")
            sys.exit(0)
         
        logging.info("Processing input: " + _input)
        parser = YamlParser(_input)
        root = parser.parseInputYAML()
        logging.info("Processing input done.")

        logging.info("Generating Java code to folder: " + output)
        generator = JavaGenerator2(output, package, root)
        generator.generateSchema()
        logging.info("Generating Java code done.")

    except YamlError as err:
        logging.error(err.message)

if __name__ == "__main__":
    main(sys.argv[1:])

