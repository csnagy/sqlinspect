class TXTGenerator:
    """TXT Generator class"""
    
    def __init__(self, output):
        self.output = output
    
    def generateScope(self, scope):
        print("SCOPE NAME: ", scope.name)
        if scope.parent:
            print("SCOPE PARENT: ", scope.parent.name)
        
        for sc in scope.scopes:
            self.generateScope(sc)
        
        for cl in scope.classes:
            self.generateClass(cl)
            
    def generateClass(self, cl):
        print("  CLASS NAME: ", cl.name)
        if cl.extends:
            print("  CLASS EXTENDS: ", cl.extends)
        
        if cl.attributes:
            print("  CLASS ATTRIBUTES: ")
            for attr in cl.attributes:
                self.generateAttribute(attr)
    
    def generateAttribute(self, attr):
        print ("    ATTR NAME:", attr.name)
        print ("    ATTR TYPE:", attr.type)
