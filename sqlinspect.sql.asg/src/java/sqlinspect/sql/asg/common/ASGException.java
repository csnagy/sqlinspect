package sqlinspect.sql.asg.common;

public class ASGException extends RuntimeException {

	private static final long serialVersionUID = 6839353332631579909L;

	public ASGException() {
		super();
	}

	public ASGException(String message) {
		super(message);
	}

	public ASGException(String message, Throwable cause) {
		super(message, cause);
	}

	public ASGException(Throwable cause) {
		super(cause);
	}
}
