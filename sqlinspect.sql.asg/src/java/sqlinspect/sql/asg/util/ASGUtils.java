package sqlinspect.sql.asg.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.base.Named;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.common.NodeKind;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.FunctionParams;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.FunctionDefParams;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.type.DateType;
import sqlinspect.sql.asg.type.NumericType;
import sqlinspect.sql.asg.type.StringType;
import sqlinspect.sql.asg.type.Type;

public final class ASGUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ASGUtils.class);

	private ASGUtils() {
	}

	public static boolean typeEquals(Type t1, Type t2) {
		return t1.match(t2);
	}

	public static boolean functionDefParamsEquals(FunctionDefParams p1, FunctionDefParams p2) {
		if (p1 == null && p2 == null) {
			return true;
		}

		if (p1 == null || p2 == null) {
			return false;
		}

		ArrayList<Type> params1 = p1.getParameters();
		ArrayList<Type> params2 = p2.getParameters();

		if (params1.size() != params2.size()) {
			return false;
		}

		for (int i = 0; i < params1.size(); i++) {
			if (!typeEquals(params1.get(i), params2.get(i))) {
				return false;
			}
		}

		return true;
	}

	public static boolean functionParamsEquals(FunctionDefParams defs, FunctionParams params) {
		if (params == null && defs == null) {
			return true;
		}

		if (params == null || defs == null) {
			return false;
		}

		List<Type> params2 = defs.getParameters();
		Expression par = params.getParameter();
		if (par instanceof ExpressionList) {
			ExpressionList list = (ExpressionList) par;
			List<Expression> params1 = list.getExpressions();

			return params1.size() == params2.size();
		} else {
			return params2.size() == 1;
		}
	}

	public static Table getParentTable(Column col) {

		Base parent = col.getParent();

		if (parent.getNodeKind() != NodeKind.TABLE) {
			LOG.error("Column parent should be a table {}!", col);
			return null;
		}

		return (Table) parent;
	}

	public static String getColumnFullName(Column col) {
		Table tab = getParentTable(col);

		return (tab == null ? "" : tab.getName()) + "." + col.getName();
	}

	public static String getQualifiedName(Named named) {
		if (named == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder(named.getName());

		Base n = named.getParent();
		while (n != null && !(n instanceof SQLRoot)) {
			if (n instanceof Named) {
				sb.insert(0, ((Named) n).getName() + ".");
			}
			n = n.getParent();
		}

		return sb.toString();
	}

	public static String typeToString(Type type) {
		StringBuilder sb = new StringBuilder();
		if (type != null) {
			switch (type.getNodeKind()) {
			case DATETYPE:
				DateType dt = (DateType) type;
				sb.append(dt.getKind().toString());
				if (dt.getLength() > 0) {
					sb.append('(').append(dt.getLength());
					if (dt.getPrecision() > 0) {
						sb.append(',').append(dt.getPrecision());
					}
					sb.append(')');
				}
				break;
			case NUMERICTYPE:
				NumericType nt = (NumericType) type;
				if (nt.getUnsigned()) {
					sb.append("UNSIGNED");
				}
				sb.append(nt.getKind().toString());
				if (nt.getScale() > 0) {
					sb.append('(').append(nt.getScale());
					if (nt.getPrecision() > 0) {
						sb.append(',').append(nt.getPrecision());
					}
					sb.append(')');
				}
				break;
			case STRINGTYPE:
				StringType st = (StringType) type;
				sb.append(st.getKind().toString());
				if (st.getLength() > 0) {
					sb.append('(').append(st.getLength()).append(')');
				}
				break;
			default:
				sb.append(type.getNodeKind().toString());
				break;
			}
		}
		return sb.toString();
	}

	public static String columnTypeToString(Column col) {
		if (col == null || col.getType() == null) {
			return "";
		} else {
			return typeToString(col.getType());
		}
	}

	public static Database findDatabase(ASG asg, String name) {
		for (Database db : asg.getRoot().getDatabases()) {
			if (db.getName().equalsIgnoreCase(name)) {
				return db;
			}
		}
		return null;
	}

	public static Schema findSchema(Database db, String name) {
		for (Schema sch : db.getSchemas()) {
			if (sch.getName().equalsIgnoreCase(name)) {
				return sch;
			}
		}
		return null;
	}

	public static Table findTable(Schema sch, String name) {
		for (Table tab : sch.getTables()) {
			if (tab.getName().equalsIgnoreCase(name)) {
				return tab;
			}
		}
		return null;
	}

	// qname: db.sch.table
	public static Table findTable(ASG asg, String qName) {
		String[] parts = qName.split("\\.");
		if (parts.length != 3) {
			return null;
		}
		Database db = findDatabase(asg, parts[0]);
		if (db != null) {
			Schema sch = findSchema(db, parts[1]);
			if (sch != null) {
				return findTable(sch, parts[2]);
			}
		}

		return null;
	}

	public static Column findColumn(Table tab, String name) {
		if (tab != null) {
			for (Column col : tab.getColumns()) {
				if (col.getName().equalsIgnoreCase(name)) {
					return col;
				}
			}
		}
		return null;
	}

	public static SQLRoot getRoot(Base node) {
		Base p = node;
		while (p != null && p.getParent() != null) {
			p = p.getParent();
		}

		if (p instanceof SQLRoot) {
			return (SQLRoot) p;
		} else {
			throw new ASGException("Cannot get SQLRoot for node: " + (node == null ? "NULL" : node.toString()));
		}
	}


	public static boolean exportJson(ASG asg, File file) {
		try {
			LOG.info("Exporting AST to {}", file);
			ObjectMapper mapper = new ObjectMapper();

			SQLRoot root = asg.getRoot();
			mapper.writerWithDefaultPrettyPrinter().writeValue(file, root);
			LOG.info("Done.");
			return true;
		} catch (IOException e) {
			LOG.error("Could not export to file " + file, e);
			return false;
		}
	}

	public static void dumpErrors(ASG asg, String dumpFile, String format) {
		LOG.info("Dump parser errors: {} ({})", dumpFile, format);

		IErrorDump dump;
		if ("XML".equalsIgnoreCase(format)) {
			dump = new XMLErrorDump(asg, dumpFile, false);
		} else {
			dump = new TXTErrorDump(asg, dumpFile, false);
		}

		dump.writeDump();
		LOG.info("Done.");
	}

}
