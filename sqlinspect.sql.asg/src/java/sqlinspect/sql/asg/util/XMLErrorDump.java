package sqlinspect.sql.asg.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.statm.Statement;

public class XMLErrorDump implements IErrorDump {
	private static final Logger LOG = LoggerFactory.getLogger(XMLErrorDump.class);

	private static final String TABC = "  ";
	private static final String NLC = "\n";

	private static final String ROOT_NODE = "Errors";
	private static final String ERROR_NODE = "Error";
	private static final String ERROR_ATTR_STMT_ID = "stmtId";
	private static final String ERROR_ATTR_INTERNAL_ID = "internalId";
	private static final String ERROR_ATTR_FILE = "file";
	private static final String ERROR_ATTR_LINE = "line";
	private static final String ERROR_ATTR_COL = "col";
	private static final String ERROR_MSG_NODE = "Message";
	private static final String ERROR_TRACE_NODE = "Trace";

	private XMLStreamWriter writer;
	private int indent;
	private final String path;
	private final ASG asg;
	private final boolean writeTrace;

	public XMLErrorDump(ASG asg, String path, boolean writeTrace) {
		this.asg = asg;
		this.path = path;
		this.writeTrace = writeTrace;
		indent = 1;
	}

	@Override
	public void writeDump() {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

		try (OutputStream ostream = Files.newOutputStream(new File(path).toPath())) {
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeStatement(stmt);
				}
			}

			writer.writeEndElement();
			nl();
			writer.writeEndDocument();
			writer.flush();
		} catch (FileNotFoundException e) {
			LOG.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			LOG.error("Could not write XML.", e);
		} catch (IOException e) {
			LOG.error("IO error.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					LOG.error("Could not clean XMLStreamWriter!", e);
				}
			}
		}
	}

	private void writeStatement(Statement stmt) throws XMLStreamException {
		if (stmt.getError()) {
			writeError(stmt);
		}
	}

	private void writeError(Statement stmt) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(ERROR_NODE);
		writer.writeAttribute(ERROR_ATTR_STMT_ID, Integer.toString(stmt.getId()));
		writer.writeAttribute(ERROR_ATTR_INTERNAL_ID, Integer.toString(stmt.getInternalId()));
		String lpath = stmt.getPath();
		if (lpath != null) {
			writer.writeAttribute(ERROR_ATTR_FILE, lpath);
		}

		writer.writeAttribute(ERROR_ATTR_LINE, Integer.toString(stmt.getErrorLine()));
		writer.writeAttribute(ERROR_ATTR_COL, Integer.toString(stmt.getErrorCol()));
		nl();

		tab(indent);
		writer.writeStartElement(ERROR_MSG_NODE);
		writer.writeCharacters(stmt.getErrorMessage());
		writer.writeEndElement();
		nl();

		if (writeTrace) {
			tab(indent);
			writer.writeStartElement(ERROR_TRACE_NODE);
			writer.writeCharacters(stmt.getErrorTrace());
			writer.writeEndElement();
			nl();
		}

		tab(--indent);
		writer.writeEndElement();
		nl();
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters(NLC);
	}

	private void tab(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TABC);
		}
	}
}