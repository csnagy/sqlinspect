package sqlinspect.sql.asg.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.visitor.Visitor;

public class TXTErrorDump extends Visitor implements IErrorDump {
	private static final Logger LOG = LoggerFactory.getLogger(TXTErrorDump.class);

	private final String path;
	private final ASG asg;
	private final boolean writeTrace;

	public TXTErrorDump(ASG asg, String path, boolean writeTrace) {
		super();
		this.asg = asg;
		this.path = path;
		this.writeTrace = writeTrace;
	}

	@Override
	public void writeDump() {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(new File(path).toPath()), StandardCharsets.UTF_8);
				BufferedWriter bw = new BufferedWriter(ow);) {

			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeStatement(bw, stmt);
				}
			}
		} catch (IOException e) {
			LOG.error("IO error while writing report!", e);
		}
	}

	private void writeStatement(BufferedWriter bw, Statement stmt) throws IOException {
		if (stmt.getError()) {
			writeError(bw, stmt);
		}
	}

	private void writeError(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("Statement (Id: " + stmt.getId() + ", internalId: " + stmt.getInternalId() + ") ");
		bw.write(" at " + stmt.getPath() + ": " + stmt.getErrorLine() + ", " + stmt.getErrorCol() + ": ");
		bw.write(stmt.getErrorMessage() + "\n");
		if (writeTrace) {
			bw.write("Trace: " + stmt.getErrorTrace());
		}
	}
}
