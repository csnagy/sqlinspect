package sqlinspect.app;

import java.util.function.Consumer;

public record CLIOption(String name, String param, String description, String paramDescription,
		Consumer<String> handler) {
}
