package sqlinspect.app;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Root;
import sqlinspect.dbmodel.util.DBModelJSonExport;
import sqlinspect.dbmodel.util.DBModelXMIExport;
import sqlinspect.nosql.NoSQLDialect;
import sqlinspect.nosql.springdata.mongodb.JSONSchemaExtractor;
import sqlinspect.nosql.springdata.mongodb.SchemaInference;
import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.HotspotFinderFactory;
import sqlinspect.plugin.extractors.IHotspotFinder;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.extractors.JDBCHotspotFinder;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.SmellRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.utils.EclipseProjectUtil;
import sqlinspect.plugin.utils.JSONQueries;
import sqlinspect.plugin.utils.XMLQueries;
import sqlinspect.sql.BenchMark;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.util.ASGUtils;

public class SQLInspectCLI {
	private static final String OFF = "off";
	private static final String ON = "on";
	private static final String XML = "xml";
	private static final String JSON = "json";
	private static final Logger LOG = LoggerFactory.getLogger(SQLInspectCLI.class);
	private final List<CLIOption> options = new ArrayList<>();
	private List<IHotspotFinder> hsfs;
	private String dialect;
	private String[] schemaFiles = {};
	private int maxCallDepth = InterQueryResolver.DEFAULT_MAX_CALL_DEPTH;
	private int maxCFGDepth = InterQueryResolver.DEFAULT_MAX_CFG_DEPTH;
	private String projectName;
	private String projectDir;
	private String srcDir;
	private String projectCP;
	private String projectCPFile;
	private String projectCPSeparator = ":";
	private String outDir;
	private String filterFile;
	private String exportFormat = XML;
	private boolean exportAST;
	private boolean inferSchema;
	private boolean helpRequested;

	private IEclipseContext context = EclipseContextFactory.create("context");
	private final ProjectRepository projectRepository;
	private final HotspotRepository hotspotRepository;
	private final QueryRepository queryRepository;
	private final ASGRepository asgRepository;
	private final MetricRepository metricRepository;
	private final SmellRepository smellRepository;
	private final TableAccessRepository tableAccessRepository;

	public SQLInspectCLI() {
		initOptions();

		projectRepository = ContextInjectionFactory.make(ProjectRepository.class, context);
		context.set(ProjectRepository.class, projectRepository);
		hotspotRepository = ContextInjectionFactory.make(HotspotRepository.class, context);
		context.set(HotspotRepository.class, hotspotRepository);
		queryRepository = ContextInjectionFactory.make(QueryRepository.class, context);
		context.set(QueryRepository.class, queryRepository);
		asgRepository = ContextInjectionFactory.make(ASGRepository.class, context);
		context.set(ASGRepository.class, asgRepository);
		metricRepository = ContextInjectionFactory.make(MetricRepository.class, context);
		context.set(MetricRepository.class, metricRepository);
		smellRepository = ContextInjectionFactory.make(SmellRepository.class, context);
		context.set(SmellRepository.class, smellRepository);
		tableAccessRepository = ContextInjectionFactory.make(TableAccessRepository.class, context);
		context.set(TableAccessRepository.class, tableAccessRepository);
	}

	private static String getAllDialects() {
		Stream<String> sqlDialects = Stream.of(SQLDialect.values()).map(SQLDialect::toString);
		Stream<String> noSQLDialects = Stream.of(NoSQLDialect.values()).map(NoSQLDialect::toString);

		return Stream.concat(sqlDialects, noSQLDialects).collect(Collectors.joining(","));
	}

	public void help() {
		System.out.format("SQLInspect [params]%n");
		for (CLIOption option : options) {
			System.out.format(" -%-20s %s %s%n", option.name() + " " + option.param(), option.description(),
					option.paramDescription());
		}
	}

	private void initOptions() {
		options.add(new CLIOption("help", "", "Print this help.", "", arg -> helpRequested = true));

		options.add(new CLIOption("hotspotfinder", "<hsfs>", "Hotspot finder(s).",
				"Comma separated. Options: " + HotspotFinderFactory.getHotspotFindersAsString(), arg -> {
					String hsfNames = arg;
					if (hsfNames == null) {
						hsfNames = JDBCHotspotFinder.class.getSimpleName();
					}

					try {
						IEclipseContext eclipseContext = EclipseContextFactory.create();
						hsfs = HotspotFinderFactory.create(eclipseContext, hsfNames.split(PreferenceHelper.SEPERATOR));
					} catch (IllegalArgumentException e) {
						throw new IllegalArgumentException("Invalid hotspot finder, options are: "
								+ HotspotFinderFactory.getHotspotFindersAsString(), e);
					}
				}));

		options.add(new CLIOption("dialect", "<dialect>", "Dialect.", "Options: " + getAllDialects(), arg -> {
			dialect = arg;
			if (dialect == null) {
				dialect = SQLDialect.MYSQL.toString();
			}
			if (!SQLDialect.isSQLDialect(dialect) && !NoSQLDialect.isNoSQLDialect(dialect)) {
				throw new IllegalArgumentException("Invalid dialect, options are: " + getAllDialects());
			}
		}));

		options.add(new CLIOption("schema", "<files>", "Schema SQL file(s).",
				"Comma separated. E.g: schema1.sql,schema2.sql",
				arg -> schemaFiles = arg.split(PreferenceHelper.SEPERATOR)));

		options.add(new CLIOption("maxcalldepth", "<int>", "Maximum CALL depth.",
				"Default: " + InterQueryResolver.DEFAULT_MAX_CALL_DEPTH, arg -> maxCallDepth = Integer.parseInt(arg)));

		options.add(new CLIOption("maxcfgdepth", "<int>", "Maximum CFG depth.",
				"Default: " + InterQueryResolver.DEFAULT_MAX_CFG_DEPTH, arg -> maxCFGDepth = Integer.parseInt(arg)));

		options.add(new CLIOption("projectname", "<name>", "Name of the project.", "E.g: Test", arg -> {
			projectName = arg;
			if (projectName == null || projectName.isEmpty()) {
				throw new IllegalArgumentException(
						"The project name has to be specified! See the parameter -projectname!");
			}
		}));

		options.add(new CLIOption("projectdir", "<dir>", "Directory of the project to be analyzed.",
				"E.g: projects/Test", arg -> {
					projectDir = arg;
					if (projectDir == null || projectDir.isEmpty()) {
						throw new IllegalArgumentException(
								"The project directory has to be specified! See the parameter -projectdir!");
					}
					File dir = new File(projectDir);
					if (!dir.exists()) {
						throw new IllegalArgumentException("Project directory does not exist: " + projectDir);
					}
				}));

		options.add(
				new CLIOption("projectcp", "<classpath>", "Classpath of the project.", "E.g: lib/android.jar", arg -> {
					if (projectCPFile != null) {
						throw new IllegalArgumentException(
								"The arguments 'projectcp' and 'projectcp-file' cannot be specified together.");
					}
					projectCP = arg;
				}));

		options.add(new CLIOption("projectcp-file", "<file>",
				"File containing the classpath for the analysis of the project.", "E.g: classpath.txt", arg -> {
					if (projectCP != null) {
						throw new IllegalArgumentException(
								"The arguments 'projectcp' and 'projectcp-file' cannot be specified together.");
					}
					projectCPFile = arg;
					try {
						projectCP = new String(Files.readAllBytes(Paths.get(projectCPFile)), StandardCharsets.UTF_8);
					} catch (IOException e) {
						throw new IllegalArgumentException("Error reading projectcp-file: " + projectCPFile, e);
					}
				}));

		options.add(new CLIOption("projectcp-separator", "<char>",
				"Overrides the character separating classpath entries", "E.g: ';'. Default: ':'", arg -> {
					if (arg.length() != 1) {
						throw new IllegalArgumentException("The projectcp-separator has to be a single character!");
					}
					projectCPSeparator = arg;
				}));

		options.add(new CLIOption("srcdir", "<dir>", "Source dir under project dir (autodetected if unspecified).",
				"E.g: src/", arg -> srcDir = arg));

		options.add(new CLIOption("outdir", "<dir>", "Output directory of the analysis results.", "E.g: out/", arg -> {
			outDir = arg;
			File dir = new File(outDir);
			if (!dir.exists()) {
				throw new IllegalArgumentException("Output directory does not exist: " + outDir);
			}
		}));

		options.add(new CLIOption("exportast", "<on/off>", "Export the AST.", "Options: on,off", arg -> {
			if (ON.equalsIgnoreCase(arg)) {
				exportAST = true;
			} else if (OFF.equalsIgnoreCase(arg)) {
				exportAST = false;
			} else {
				throw new IllegalArgumentException("Invalid value for -exportast. Possible options: on/off.");
			}
		}));

		options.add(new CLIOption("filter", "<file>", "Filter file for the files to be analyzed.", "E.g: filter.txt",
				arg -> {
					filterFile = arg;
					File f = new File(filterFile);
					if (!f.exists()) {
						throw new IllegalArgumentException("Filter file does not exist: " + filterFile);
					}
				}));

		options.add(new CLIOption("format", "<format>", "Export format.", "Options: xml,json", arg -> {
			exportFormat = arg.toLowerCase();
			if (!XML.equals(exportFormat) && !JSON.equals(exportFormat)) {
				throw new IllegalArgumentException(
						"Invalid export format: " + exportFormat + "! Valid formats are: xml, json.");
			}
		}));

		options.add(
				new CLIOption("inferschema", "<on/off>", "Infer schema from source code.", "Options: on,off", arg -> {
					if (ON.equalsIgnoreCase(arg)) {
						inferSchema = true;
					} else if (OFF.equalsIgnoreCase(arg)) {
						inferSchema = false;
					} else {
						throw new IllegalArgumentException("Invalid value for -inferschema. Possible options: on/off.");
					}
				}));
	}

	public void run(String[] args) throws CoreException, IOException {
		processArguments(args);

		if (helpRequested) {
			help();
		} else {
			analyzeDir();
		}
	}

	private CLIOption getOptionForArgument(String arg) {
		if (!arg.isEmpty() && arg.charAt(0) == '-') {
			String pkey = arg.substring(1);
			Optional<CLIOption> matchingOption = options.stream().filter(o -> o.name().equals(pkey)).findAny();
			if (matchingOption.isPresent()) {
				return matchingOption.get();
			}
		}
		throw new IllegalArgumentException("Invalid parameter: " + arg);
	}

	private Map<String, String> splitArgs(String[] args) {
		Map<String, String> conf = new HashMap<>();
		int i = 0;
		while (i < args.length) {
			CLIOption option = getOptionForArgument(args[i]);
			if (!option.param().isEmpty()) {
				if (i + 1 < args.length && !args[i + 1].isEmpty() && args[i + 1].charAt(0) != '-') {
					conf.put(option.name(), args[i + 1]);
					++i;
				} else {
					throw new IllegalArgumentException("Missing argument for: " + args[i]);
				}
			} else {
				conf.put(option.name(), "");
			}

			++i;
		}

		return conf;
	}

	private void processArguments(String[] args) {
		Map<String, String> argMap = splitArgs(args);

		for (CLIOption option : options) {
			if (argMap.get(option.name()) != null) {
				option.handler().accept(argMap.get(option.name()));
			}
		}
	}

	private void setProjectProperties(IProject iproj) {
		IPreferenceStore prefs = Activator.getDefault().getCurrentPreferenceStore(iproj);

		prefs.setValue(PreferenceConstants.STRING_RESOLVER, InterQueryResolver.class.getSimpleName());

		String[] hsfNames = hsfs.stream().map(hsf -> hsf.getClass().getSimpleName()).toArray(String[]::new);
		prefs.setValue(PreferenceConstants.HOTSPOT_FINDERS, PreferenceHelper.createStringList(hsfNames));

		prefs.setValue(PreferenceConstants.DIALECT, dialect);
		prefs.setValue(PreferenceConstants.RUN_SMELL_DETECTORS, true);
		prefs.setValue(PreferenceConstants.RUN_SQL_METRICS, true);
		prefs.setValue(PreferenceConstants.RUN_TABLE_ACCESS, true);

		prefs.setValue(PreferenceConstants.INTERSR_MAX_CALL_DEPTH, maxCallDepth);
		prefs.setValue(PreferenceConstants.INTERSR_MAX_CFG_DEPTH, maxCFGDepth);
		if (filterFile != null) {
			prefs.setValue(PreferenceConstants.FILTER_FILE, filterFile);
		}
	}

	private void analyzeNoSQLProject(Project project, IEclipseContext eclipseContext)
			throws CoreException, IOException {
		if (schemaFiles != null && schemaFiles.length > 0) {
			Root root = DBModelFactory.eINSTANCE.createRoot();
			DB db = DBModelFactory.eINSTANCE.createDB();
			db.setName("DB");
			root.getDatabases().add(db);

			JSONSchemaExtractor schemaExtractor = new JSONSchemaExtractor(db);

			Arrays.stream(schemaFiles).forEach(fname -> {
				try {
					File f = new File(fname);
					if (f.exists()) {
						schemaExtractor.extractSchema(f);
					} else {
						LOG.error("Schema file does not exist: {}", fname);
					}
				} catch (IOException e) {
					LOG.error("Error extracting schema from file: ", e);
				}
			});

			exportNoSQLSchema(root, new File(outDir, project.getName() + "-schema." + exportFormat));
		}
		if (inferSchema) {
			Root root = DBModelFactory.eINSTANCE.createRoot();

			SchemaInference schemaInference = ContextInjectionFactory.make(SchemaInference.class, eclipseContext);
			DB db = schemaInference.inferDB(project, eclipseContext);
			root.getDatabases().add(db);

			exportNoSQLSchema(root, new File(outDir, project.getName() + "-inferschema." + exportFormat));
		} else {
			LOG.debug("Schema file are not provided.");
		}
	}

	private void analyzeSQLProject(Project project, String outDir) {
		ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer(project);
		ContextInjectionFactory.inject(projectAnalyzer, context);
		IJavaProject javaProject = project.getIJavaProject();
		BenchMark bm = new BenchMark();
		bm.startAll(javaProject.getElementName() + ":ALL");

		projectAnalyzer.initAnalyzer();

		bm.run(javaProject.getElementName() + ":SCHEMA", () -> {
			if (schemaFiles != null && schemaFiles.length > 0) {
				projectAnalyzer.parseSchemaFromFiles(Arrays.asList(schemaFiles));
			} else {
				LOG.debug("Schema files are not provided.");
			}
		});

		bm.run(javaProject.getElementName() + ":HOTSPOTS", () -> {
			try {
				projectAnalyzer.extractHotspots(SubMonitor.convert(new NullProgressMonitor()));
			} catch (Exception e) {
				LOG.error("Error occurred while extracting queries from the project {}!", project.getName(), e);
			}
		});

		bm.run(javaProject.getElementName() + ":QUERIES", () -> {
			try {
				projectAnalyzer.parseQueries(SubMonitor.convert(new NullProgressMonitor()));
			} catch (Exception e) {
				LOG.error("Error occurred while analyzing queries of the project {}!", project.getName(), e);
			}
		});

		bm.run(javaProject.getElementName() + ":SQLMRICS", () -> projectAnalyzer.runSQLMetrics());
		bm.run(javaProject.getElementName() + ":JAVAMETRICS", () -> projectAnalyzer.runJavaMetrics());
		bm.run(javaProject.getElementName() + ":ANDROIDSMELLS", () -> projectAnalyzer.runAndroidSmellDetectors());
		bm.run(javaProject.getElementName() + ":SQLSMELLS", () -> projectAnalyzer.runSQLSmellDetectors());
		bm.run(javaProject.getElementName() + ":TAA", () -> projectAnalyzer.runTAA());

		project.setAnalyzed(true);

		bm.stopAll(javaProject.getElementName() + ":ALL");

		bm.dumpFile(new File(outDir, project.getName() + "-SQLInspect.stats"));
		hotspotRepository.dumpStats(project, new File(outDir, project.getName() + "-Model.stats"));
	}

	private void exportQueries(Project project, File file) {
		try {
			List<Query> queries = hotspotRepository.getQueries(project);
			if (JSON.equals(exportFormat)) {
				JSONQueries queriesExport = new JSONQueries(file);
				queriesExport.writeQueries(queries, false);
			} else {
				XMLQueries queriesExport = new XMLQueries(file);
				queriesExport.writeQueries(queries, false);
			}
		} catch (IOException e) {
			LOG.error("IO error while writing queries: ", e);
		}
	}

	private void exportNoSQLSchema(Root root, File file) throws IOException {
		if (JSON.equals(exportFormat)) {
			DBModelJSonExport.export(root, file);
		} else {
			DBModelXMIExport.export(root, file);
		}
	}

	private void exportSQLSchema(Project project, File file) throws IOException {
		asgRepository.exportSchema(project, file, exportFormat);
	}

	private void exportAndroidSmells(Project project, File exportFile) {
		smellRepository.reportAndroidSmells(project, exportFile, exportFormat);
	}

	private void exportAST(Project project, File exportFile) {
		ASGUtils.exportJson(asgRepository.getASG(project), exportFile);
	}

	private void assertProjectNameAndDir() {
		if (projectName == null || projectName.isEmpty()) {
			throw new IllegalArgumentException("Project name has to be specified!");
		}
		if (projectDir == null || projectDir.isEmpty()) {
			throw new IllegalArgumentException("Project directroy has to be specified!");
		}
	}

	private void analyzeDir() throws CoreException, IOException {
		assertProjectNameAndDir();

		LOG.info("Analyze project '{}' under '{}' with source '{}'", projectName, projectDir, srcDir);
		LOG.debug("Current working directory: {}", System.getProperty("user.dir"));

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectName, projectDir, srcDir, projectCP,
				projectCPSeparator);
		setProjectProperties(iproj);
		Project project = projectRepository.createProject(iproj);

		if (NoSQLDialect.isNoSQLDialect(dialect)) {
			analyzeNoSQLProject(project, context);
		} else if (SQLDialect.isSQLDialect(dialect)) {
			analyzeSQLProject(project, outDir);
			exportQueries(project, new File(outDir, project.getName() + "-queries." + exportFormat));
			exportSQLSchema(project, new File(outDir, project.getName() + "-schema." + exportFormat));
			exportAndroidSmells(project, new File(outDir, project.getName() + "-androidsmells." + exportFormat));
			if (exportAST) {
				exportAST(project, new File(outDir, project.getName() + "-ast.json"));
			}
			smellRepository.reportSQLSmells(project, new File(outDir, project.getName() + "-sqlsmells." + exportFormat),
					exportFormat);
			metricRepository.reportSQLMetrics(project,
					new File(outDir, project.getName() + "-sqlmetrics." + exportFormat), exportFormat);
			metricRepository.reportJavaMetrics(project,
					new File(outDir, project.getName() + "-javametrics." + exportFormat), exportFormat);
			tableAccessRepository.reportTAA(project, new File(outDir, project.getName() + "-taa." + exportFormat),
					exportFormat);
		}

		LOG.info("Analyze project done.");
	}

}