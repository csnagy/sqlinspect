package sqlinspect.app;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLInspectApp implements IApplication {
	private static final Logger LOG = LoggerFactory.getLogger(SQLInspectApp.class);

	private final SQLInspectCLI cli = new SQLInspectCLI();

	@Override
	public Object start(IApplicationContext context) throws CoreException, IOException {
		String[] args = (String[]) context.getArguments().get(IApplicationContext.APPLICATION_ARGS);
		try {
			cli.run(args);
		} catch (IllegalArgumentException e) {
			LOG.error("Error: {}", e.getMessage());
			cli.help();
			return IApplication.EXIT_OK;
		}

		return IApplication.EXIT_OK;
	}

	@Override
	public void stop() {
		// nothing to do here
	}
}