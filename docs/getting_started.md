# Getting Started

SQLInspect can be used as an Eclipse plug-in or a command line tool.

## Download

Automatic builds of SQLInspect are available at the [Downloads](https://bitbucket.org/csnagy/sqlinspect/downloads/) site.

The following packages are available: 

  * `sqlinspect.updatesite.eclipse-repository-*.zip`: Eclipse plug-in.
  * `sqlinspect.product-*.zip`: Command-line interfaces.

## Install the Eclipse Plug-in

Installation requirements:

  * Java 21 or newer
  * Eclipse 2024-09 or newer
  
Installation steps:

  * Go to the *Help/Install New Software* menu of Eclipse.
  * _Add_ a new site:
    - Name: Eclipse Orbit R20230531010532
    - Location: https://download.eclipse.org/tools/orbit/downloads/drops/R20230531010532/repository
  * _Add_ a new site:
    - Name: SQLInspect
    - Location: select `sqlinspect.updatesite-*.zip` as an `Archive` (alternatively, select `Local` and specify the folder of the unzipped updatesite)
  * Select all SQLInspect plug-ins from the list and follow the next steps of the wizard.

## Usage Example of the Eclipse Plug-in

### JDBC Project Example

Import an example JDBC project to Eclipse:

  * Clone [Plandora Project Management](https://sourceforge.net/projects/plandora/):
    `svn checkout https://svn.code.sf.net/p/plandora/code/ plandora-code`
  * Under *File > Import* select *General > Existing Projects Into Workspace* and follow the wizard.
    - Select root directory `plandora-code`, then click *Finish*.
    - _Note_: It is OK to see errors in the project. SQLInspect does not require the project to be compilable.

Set up SQLInspect for the project:

  * Select the project in the *Package Explorer*, right-click, and select *Properties* to access project properties.
  * Check *Enable project specific settings* for SQLInspect
  * Add schema files for the project under *Project Properties > SQLInspect > SQLAnalyzer*:
    - Press the *Browse* button next to *Schema file*
    - Navigate to `plandora-code` and select `mysql.sql`

Analyze the project:

  * Start an analysis of your project in the project menu *SQLInspect > Analyze current project*
  * The analysis can take a few minutes depending on the project, the config, and the hardware. The progress can be followed in Eclipse (bottom right side of the view).
  * The plug-in will show a notification when the results are ready.
  
Explore the results:

  * Open *Window > Show View > SQLInspect* and explore the results in the *Queries/Hotspots/Schema/Metrics views*
  * Look for SQL smells under the *Problems view*
  
Export the results:

  * *SQLInspect > Export* menu has options to export the analysis results.
  
### Android Project Example

#### Preface

> *Notice*: Google recommends Android Studio for Android development. As a result, Android projects are typically not prepared for Eclipse and need additional configuration. However, the Android Development Tools for Eclipse (ADT) plug-in supports Android development under Eclipse.

An Android project can be imported in Eclipse as a *General Java project*, a *Gradle project* (if *Buildship Gradle Integration* is installed), or an *Android project* (if *Andmore: Development Tools for Android™* is installed).
For SQLInspect, the important thing is that the project has a Java project nature, and the `android.jar` is in the build path. However, an incomplete classpath likely leads to imprecision in the analysis.
In this tutorial, we show an example analysis of an Android project.

#### <a id="sec-android-sdk"></a>Install Android SDK

> *Android SDK* is needed to analyze an Android project.

Option 1 (preferred), install Android Studio:

  - Download and install [Android Studio](https://developer.android.com/studio).
  - Optional (needed only for Android Development Tools for Eclipse (ADT)):
    - After installing Android Studio, go to _Android Studio > Tools > SDK Manager_ and install *Android SDK Tools*
      - Un-check the *Hide Obsolete Packages* under the *SDK Tools* tab
      - Then select and install *Android SDK Tools (Obsolete)*
      
Option 2, install with Android Command line tools

  - Assuming that your current working directory is `work`, i.e., `cd work`
  - Download the latest [Android Command line tools](https://developer.android.com/studio#downloads)
  - `unzip commandlinetools-mac-*_latest.zip`
  - `./cmdline-tools/bin/sdkmanager --install "platforms;android-34"`
  
  
#### Install and Configure Android Development Tools for Eclipse (ADT)

  * Install "Android Development Tools for Eclipse" from Eclipse Marketplace.
  * After installation, configure ADT with an Android SDK.
  	* *Preferences > Android*  "SDK Location" should point to the SDK (e.g., `~/Android/Sdk` on Linux, or `C:\Users\user\AppData\Local\Android\Sdk` on Windows).
  	* Notice: SDK Path is present under _Android Studio > Tools > SDK Manager_.
  	* If you get "*Could not find folder 'tools' inside SDK*" error, install *Android SDK Tools* from _Android Studio > Tools > SDK Manager_ (see above).
  * A tutorial is also available here: https://www.geeksforgeeks.org/how-to-install-and-setup-eclipse-ide-for-android-app-development/

#### Install Andmore: Development Tools for Android™ - *NOT RECOMMENDED*

> *Notice*: Andmore is archived by Eclipse. It is not recommended to use it anymore.

  * *Help > Install New Software*, add [Eclipse Orbit](https://download.eclipse.org/tools/orbit/downloads/) as a repository
      - For Eclipse 2021-09, add: `https://download.eclipse.org/tools/orbit/downloads/drops/R20210825222808/repository`
      - Don't select anything to install, just the repository is needed.
  * Install [Andmore: Development Tools for Android™](https://marketplace.eclipse.org/content/andmore-development-tools-android%E2%84%A2) from the marketplace.
  * *Andmore: Development Tools for Android™* might complain about:
      - Missing AVD (Android Virtual Device). It can be ignored.
      - DDMS instance for debugging. It can be ignored.
      - ADT version numbers from the SDK. It can be ignored.
  * Make sure that under *Preferences > Android*, *SDK Location* points to a valid path, and at least one Android SDK is on the list.

#### Import an Android project to Eclipse (Andmore or ADT)

> *Notice*: This approach works both for Andmore and ADT.

  * Clone [GnuCash Android](https://github.com/codinguser/gnucash-android.git):
    `git clone https://github.com/codinguser/gnucash-android.git`
  * Under *File > Import* select *Android > Existing Android Code Into Workspace* and follow the wizard.
      - Set the *Root directory* as the directory where you cloned the project (`gnucash-android`)
      - Check the imported subprojects and their project name.
      - The default project name is used multiple times for some projects, resulting in later errors. If you see this, make sure that the subprojects have different names.
  * Some projects define different source folders. GnuCash is a project like this.
      - For each subproject, go to *Project Properties > Java Build Path > Source*. Click *Add Folder* and select *java*.
  * _Note_: It is OK to see errors in the project. SQLInspect does not require the project to be compilable.      
      
#### Import an Android project to Eclipse (generic) - *PREFERRED*

  * Clone [GnuCash Android](https://github.com/codinguser/gnucash-android.git):
    `git clone https://github.com/codinguser/gnucash-android.git`
  * Under *File > Import* select *General > Existing Projects Into Workspace* and follow the wizard.
      - Set the *Root directory* as the directory where you cloned the project (`gnucash-android`)
      - Check the imported subprojects and their project name.
  * Add `android.jar` to the Build Path:
      - Go to project *Properties > Java Build Path > Libraries*
      - Select *Classpath* and *Add External Jar*
      - Select `android.jar` (from your Android SDK Path, e.g., `~/Android/Sdk/platforms/android-29/android.jar`)
        - Notice: SDK Path is present under _Android Studio > Tools > SDK Manager_.
  * _Note_: It is OK to see errors in the project. SQLInspect does not require the project to be compilable.


#### Using SQLInspect with an Android project

> *Notice*: The only important things for SQLInspect are that `android.jar` is in the build path and that `AndroidHotspotFinder` is enabled for the project (it is, by default). The SQL dialect should also be SQLite (if that's the project's SQL).

Configuring SQLInspect for Android projects:

  * For each project, go to *Project Properties > SQLInspect*.
      - *SQLAnalyzer* => set the SQL dialect to *SQLite*
      - *SQLExtractor* => select *AndroidHotspotFinder*

Analyze the projects:

  * Right-click on one of the projects (e.g., *AccountsActivity*), and in the context menu, select *SQLInspect > Analyze current project*.
  * The analysis can take a few minutes depending on the project, the config, and the hardware. The progress can be followed in Eclipse (bottom right side of the view).
  * The plug-in will show a notification when the results are ready.
  * Repeat the analysis for the other project(s).
  * If there are no queries (and there should be any), check the following:
      - *AndroidHotspotFinder* is enabled under *Properties > SQLInspect > SQL Extractor*
      - *SQLite* dialect is selected under *Properties > SQLInspect > SQL Analyzer*
      - `android.jar` is in the build path under *Properties > Java Build Path > Libraries*
      - If the `.java` files are under a folder `java` and not `src`, check that `java` is selected under *Properties > Java Build Path > Source*
  
Explore the results:

  * Open *Window > Show View > SQLInspect* and explore the results in the *Queries/Hotspots/Schema/Metrics views*.
  * Look for SQL smells under the *Problems view*.
  
Export the results:

  * *SQLInspect > Export* menu has options to export the analysis results.
  
## Usage Example of the Command Line Interface ##

### Prepare the Command Line Interface:

  * Create a folder for the projects and the analysis: `mkdir work`.
  * Unzip the `sqlinspect.product*.zip` of your architecture (i.e., Linux/Windows/MacOS) under `work/sqlinspect.product`.
    - MacOS:
      ```
      mkdir sqlinspect.product
      cd sqlinspect.product
      wget https://bitbucket.org/csnagy/sqlinspect/downloads/sqlinspect.product-0.4.2.v20241021-1301-macosx.cocoa.x86_64.tar.gz
      tar -zxf sqlinspect.product-0.4.2.v20241021-1301-macosx.cocoa.x86_64.tar.gz
      cd ..
      ```
    - Linux:
      ```
      mkdir sqlinspect.product
      cd sqlinspect.product
      wget https://bitbucket.org/csnagy/sqlinspect/downloads/sqlinspect.product-0.4.2.v20241021-1301-linux.gtk.x86_64.tar.gz
      tar -zxf sqlinspect.product-0.4.2.v20241021-1301-linux.gtk.x86_64.tar.gz
      cd ..
      ```
    - Windows:
      ```
      mkdir sqlinspect.product
      cd sqlinspect.product
      curl -L -O "https://bitbucket.org/csnagy/sqlinspect/downloads/sqlinspect.product-0.4.2.v20241021-1301-win32.win32.x86_64.zip
      tar -xf sqlinspect.product-0.4.2.v20241021-1301-win32.win32.x86_64.zip
      cd ..
      ```
  * Assuming that your current working directory is `work`, i.e., `cd work`, run SQLInspect as follows:
    - Linux: 
      ```
      sqlinspect.product/sqlinspect -help
      ```
    - MacOS:
      ```
      sqlinspect.product/Eclipse.app/Contents/MacOS/sqlinspect -help
      ```
      -  Note: If you get the error `Eclipse.app is damaged and can't be opened.`, run `xattr -r -d com.apple.quarantine sqlinspect.product/Eclipse.app`
    - Windows:
      ```
      sqlinspect.product\sqlinspect_console.exe -help
      ```
  
### Clone an example JDBC/Spring/Android project:

  * Assuming that your current working directory is `work`, i.e., `cd work`.
  * Clone [Plandora Project Management](https://sourceforge.net/projects/plandora/):
    `svn checkout https://svn.code.sf.net/p/plandora/code/ plandora-code`  
  
### Analyze a projects

  * Assuming that your current working directory is `work`, i.e., `cd work`.
  * Create an output folder for the results: `mkdir out`
  * Run SQLInspect as follows:
    - MacOS:
      ```
      sqlinspect.product/Eclipse.app/Contents/MacOS/sqlinspect \ 
      -hotspotfinder JDBCHotspotFinder \
      -dialect mysql \
      -schema $(pwd)/plandora-code/mysql.sql \
      -projectdir $(pwd)/plandora-code/ \
      -outdir $(pwd)/out \
      -projectname plandora-code
      ```
      - Notice: It is important that the directories are provided as absolute directories; hence, replace `$(pwd)`, if needed.
    - Linux:
      ```
      sqlinspect.product/sqlinspect \
      -hotspotfinder JDBCHotspotFinder \
      -dialect mysql \
      -schema plandora-code/mysql.sql \
      -projectdir plandora-code/ \
      -outdir out \
      -projectname plandora-code
      ```
    - Windows (in CMD):
      ```
      sqlinspect.product\sqlinspect_console.exe -consoleLog ^
      -hotspotfinder JDBCHotspotFinder ^
      -dialect mysql -schema plandora-code/mysql.sql ^
      -projectdir plandora-code/ ^
      -outdir out ^
      -projectname plandora-code
      ```
  * Check out the results files in the output folder.
    - E.g., `cat out/plandora-code-queries.xml`
  
### Analyze an Android projects

Notes:

  - Use `-hotspotfinder AndroidHotspotFinder`
  - Use `-dialect sqlite`
  - Add Android SDK to the classpath: `-projectcp <path to android sdk>/android.jar`
  	- See [Install Android SDK](#sec-android-sdk) how to install Android SDK

Example project:

  - Assuming that your current working directory is `work`, i.e., `cd work`.
  - Clone an example project: `git clone https://github.com/codinguser/gnucash-android.git`
  - Create an output directory: `mkdir out`
  - MacOS/Linux:
    ```
    sqlinspect.product/Eclipse.app/Contents/MacOS/sqlinspect \
      -hotspotfinder AndroidHotspotFinder \
      -dialect sqlite \
      -projectdir $(pwd)/gnucash-android/ \
      -outdir $(pwd)/out \
      -projectname gnucash-android \
      -projectcp $HOME/Android/Sdk/platforms/android-34/android.jar
    ```
    - Notice: It is important that the directories are provided as absolute directories; hence, replace `$(pwd)`, if needed.
  - Linux:
    ```
    sqlinspect.product/sqlinspect \
      -hotspotfinder AndroidHotspotFinder \
      -dialect sqlite \
      -projectdir $(pwd)/gnucash-android/ \
      -outdir $(pwd)/out \
      -projectname gnucash-android \
      -projectcp $HOME/Android/Sdk/platforms/android-34/android.jar
    ```
    - Notice: It is important that the directories are provided as absolute directories; hence, replace `$(pwd)`, if needed.
  - Windows (in CMD):
    ```
    sqlinspect.product\sqlinspect_console.exe -consoleLog ^
      -hotspotfinder AndroidHotspotFinder ^
      -dialect sqlite ^
      -projectdir C:\Users\user\work\gnucash-android ^
      -outdir C:\Users\user\work\out ^
      -projectname gnucash-android ^
      -projectcp "C:\Users\user\AppData\Local\Android\Sdk\platforms\android-34\android.jar" ^
      -projectcp-separator ";"
    ```
