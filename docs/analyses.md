## Analyses Details ##

### Query Extraction ###

SQLInspect implements a query extraction technique inspired by Meurice et al. [3]. It has two main steps: (1) identifying _hotspots_ where SQL statements are sent to the database, followed by (2) extracting SQL strings sent to the database through the hotspots.

#### Hotspot Identification ####

Hotspot identification is implemented for _JDBC_ by the `JDBCHotspotFinder` and for _Android_ by the `AndroidHotspotFinder`.

It looks for the following hotspots in an application using _JDBC_:

  * _Statement execute_ signatures: where a query is sent to the database directly through a string parameter of the call (e.g. `Statement.execute()`).
  * _PreparedStatement execute_ signatures: where the query is prepared initially as a `PreparedStatement` object, and then the `execute()` call sends the query to the DB (e.g. `PreparedStatement.execute()`). For these hotspots, JDBCHotspotFinder walks through the backward slice and collects the definitions of the prepared statements looking for method calls defined as _Prepare statement_ signature, e.g. `Connection.prepareStatement(java.lang.String)`.
  
It looks for the following hotspots in an application using _Android_:

  * _SQLiteDatabase.execSQL_ signatures: where a query is sent to the database directly through a string parameter of the call (e.g. `SQLiteDatabase.execSQL()`).
  * _SQLiteStatement.execute_ signatures: the execution of prepared statements (e.g. `SQLiteStatement.execute()`). The query extractor walks through the backward slice and collects the definitions of the prepared statements looking for method calls defined as _SQLiteDatabase.compileStatement_ signatures, e.g. `SQLiteDatabase.compileStatement()`.
  * _SQLiteStatement.rawQuery_ signatures: cursor calls, they are handled the same way as execSQL calls, e.g. `android.database.sqlite.SQLiteDatabase.rawQuery()`. 

Project-specific method signatures for hotspots can be defined under the properties of a project. For example, `java.sql.Statement.executeQuery(java.lang.String)/1` describes a method call for a statement execution, where the first parameter will be extracted later by the SQL String resolution.

#### SQL String Resolution ####

The SQL String resolution is the process where the `QueryResolver` resolves the possible String values of the String parameters of the hotspots identified before.
The resolver tries to find out the string values of a string expression by resolving each variable involved in it.

The actual `QueryResolver` implementation is the `InterQueryResolver` an inter-procedural implementation of Meurice et al. [1]. It walks through the backward slice of a hotspot but supports control-flow branches. Since branches in the control flow can exponentially increase the number of possible paths reaching a hotspot, thresholds can be specified under the project's property page. If it reaches one of these thresholds for a subexpression, that part of the query will remain unresolved as an `UnresolvedQueryPart`.

The configuration parameters are the followings:
  * _Maximum Nesting Level_: maximum nesting level of conditional branches,
  * _Maximum Def-use Level_: maximum redefinitions of a variable in the query slice,
  * _Maximum Call Level_: maximum number of calls in the call chain of a slice.

### SQL Parser ###

SQLInspect has its internal SQL parser(s). The currently supported SQL dialects are:

  * MySQL 5.7
  * Apache Impala 2.7.0
  * SQLite 3.24

After parsing, a name resolver will determine the references of identifiers in the SQL statements. This resolution is precise only if the schema is provided (see the properties of the project).

The AST of the parsed SQL statements can be exported to `.json` for further analyses.

#### Unrecognized Code Fragments ####

In many cases, it can happen that a specific part of the SQL string cannot be resolved. This part of the SQL is represented by `{{na}}` in the query string. The parser builds a `Joker` node in the AST for these. Similarly, `?` markers in prepared statements are represented by `{{question}}` in the query string and `Joker` nodes in the AST.

### Table/Column Access Analysis ###

The _Table/Column Access Analysis_ determines which tables/columns are accessed from the queries. It is also provided whether the column is explicitly written in the query (e.g., for `select * from t where x>10` x is explicitly accessed while all the other columns are implicitly accessed).

The results of this analysis can be observed from the _Schema view_ or can be exported to `.xml`.

## Query Search ##

The execution point of a concrete query can be searched from the _Search_ menu of Eclipse.

SQLInspect searches the AST for queries with similar structures even if they contain _Unrecognized Code Fragments_. If a SQL appears in a dynamic trace or server log with concrete, literal values (e.g., `select x+15 from t where i>10`), it can be easily located in the source code. Even if it was constructed through a prepared statement (e.g., `select x+? from t where i>?`). Besides, the query search can be configured to ignore identifier names or any literal value.