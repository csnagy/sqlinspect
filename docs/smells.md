## SQL Smells ##

The following smells [1,2] were inspired by the *SQL Antipattern* catalog of *Bill Karwin* [4]. As he says, _"SQL Antipatterns describes the most frequently made missteps I've seen people naively make while using SQL."_ The book categorizes antipatterns into Logical Database Design, Physical Database Design, Query, and Application Development categories. Our purpose was to support working with queries embedded in Java code. Hence, we implemented a prototype tool for the identification of Query antipatterns. We refer to these as *SQL code smells* since they are specific to SQL queries and indicate 'smelly' code, i.e., there might be a bug or an issue nearby.

### Fear of the Unknown ###

#### Problem ####

The reason behind this antipattern is the special meaning of `NULL` in relational databases that can easily confuse developers. `NULL` indicates when data is not available, i.e., it is `unknown`. `NULL` is not the same as zero. A number ten greater than an `unknown` is still `unknown`.

`NULL` is not the same as false, either. Hence, a boolean expression with `AND`, `OR`, and `NOT` can easily produce a result that someone may find confusing. Also, `NULL` is not the same as a string with zero length.

A string combined with `NULL` returns `NULL` in standard SQL, but some RDBMSs handle it differently, e.g., in Oracle and Sybase the concatenation of `NULL` to a string will result in the original string.

```sql
SELECT * FROM rentals WHERE customer_id = 456;
-- will not return records where customer_id is null
SELECT * FROM rentals WHERE NOT (customer_id = 456);
```

Both the first and the second queries are syntactically and semantically correct. It is tempting to assume that the result of the second query is the complement of the first query. However, this is not always the case.
For records in the table with a `NULL` value for `customer_id`, the expression in the `WHERE` clause evaluates to `unknown,' which do not satisfy the search criteria, so these records will not show up in the output.

```sql
-- will return NULL if age is NULL
SELECT age + 10 FROM customers;
-- will return NULL if e.g. middle_initial is NULL
SELECT first_name || ' ' || middle_initial || ' '
       || last_name AS full_name FROM customers;

-- should use IS NULL or IS NOT NULL
SELECT * FROM rentals WHERE customer_id = NULL;
SELECT * FROM rentals WHERE customer_id <> NULL;
```

None of the last two queries of the example above will return records where `customer_id` is `NULL`. The proper way to test for `NULL` is to use the `IS NULL` operator in SQL. Both queries are syntactically and semantically correct and will run without errors, however.

#### Solution ####

  * Use the `NOT NULL` constraint on columns where you know that `NULL` values are not allowed.
  * Use the `IS NULL` operator for `NULL` tests.
  * Be careful with expressions where NULL is likely to be involved. Particularly with aggregates like `COUNT` or `AVG`.

### Ambiguous Groups ###

#### Problem ####

A possible source of errors is the handling of columns in the `SELECT` list of a query with a `GROUP BY` clause. The SQL-92 standard is straightforward:
> it does not permit queries for which the select list, HAVING condition, or ORDER BY list refer to nonaggregated columns that are not named in the GROUP BY clause.

Therefore, the reference to the `address` column in the example makes the query invalid according to the standard. Later standards, however, such as SQL-99
> permits such non-aggregates [...] if they are functionally dependent on GROUP BY columns.

So, if `address` functionally depends on `name`, the query is acceptable. This relaxation of the rule leads to various implementations in RDBMSs. For instance, Oracle follows the strict [standard](https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_10002.htm#i2182483). While the default behavior of [MySQL](https://dev.mysql.com/doc/refman/5.7/en/group-by-handling.html) is that the
 > use of GROUP BY permits the select list, HAVING condition, or ORDER BY list to refer to nonaggregated columns even if the columns are not functionally dependent on GROUP BY columns.

 Moreover,
 > the server is free to choose any value from each group, so unless they are the same, the values chosen are indeterminate, which is probably not what you want.

 [SQLite](https://sqlite.org/lang_select.html), as another popular RDBMS implements a similar behavior:
> if the expression is an aggregate expression, it is evaluated across all rows in the group. Otherwise, it is evaluated against a single *arbitrarily chosen* row from within the group.

This is a reason for queries that sometimes work as the developer wants it, but sometimes may also result in unexpected behavior.

```sql
-- the reference to 'address' causes unpredictable
  -- behavior in MySQL and SQLite
  SELECT name, address, AVG(age) FROM customers
    GROUP BY name;
```

#### Solution ####

  * Eliminate ambiguous columns from the query.

### Random Selection

#### Problem ####

The problem with the example below is that it performs a full table scan and an expensive sort operation, which sounds unnecessary for selecting a single record. The possibilities in SQL are limited, and even popular [StackOverflow posts](https://stackoverflow.com/questions/580639/how-to-randomly-select-rows-in-sql) come to the same solution.

```sql
SELECT * FROM customers ORDER BY RAND() LIMIT 1;
```

#### Solution ####
  * Some database server offer ready solutions, e.g., `TABLESAMPLE` in SQL Server, or `SAMPLE` in Oracle.

### Implicit Columns ###

#### Problem ####

```sql
SELECT * FROM customers c JOIN rentals r
    ON c.customer_id = r.customer_id;

INSERT INTO customers VALUES
    (DEFAULT, 'Brown', 'Peter', NULL, NULL);
```

#### Solution ####

  * Name columns explicitly.
  
## Android Smells ##

### SQLiteDatabase.execSQL() returns data ###

#### Problem ####

As the [Developer Guide](https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html#execSQL(java.lang.String)) says, the purpose of execSQL() is to
>execute a single SQL statement that is NOT a SELECT or any other SQL statement that returns data.

#### Solution ####

  * Statements such as `SELECT`, `INSERT`, `UPDATE`, `DELETE` should be sent to the database through other, specific API calls (e.g., `SQLiteDatabase.insert()` for an `INSERT`).
  
### More than one statement in SQLiteDatabase.execSQL(), SQLiteDatabase.rawQuery() or SQLiteStatement.execute() ###

#### Problem ####

The purpose of these statements is to send a single SQL statement to the database and not more than one. What is worse, in the case of multiple statements, the database will recognize only the first one and will not report any error.

#### Solution ####

  * Use multiple execute calls for more than statements.

### SQLiteStatement.executeInsert() should send an INSERT statement ###

#### Problem ####

`executeInsert()` returns the ID of the row inserted. Although there is no checking of the statement sent to the database, as the [Developer Guide](https://developer.android.com/reference/android/database/sqlite/SQLiteStatement.html#executeInsert()) says,
> the SQL statement should be an INSERT for this to be a useful call.

#### Solution ####

  * Statements such as `SELECT`, `UPDATE`, `DELETE` should be sent to the database through other, specific API calls (e.g., `SQLiteDatabase.rawQuery()`, `SQLiteDatabase.update()`).
  
### More than one column in an SQLiteStatement execution ###

#### Problem ####

A statement executed through the `SQLiteStatement` API
> cannot return multiple rows or columns, but single value (1 x 1) result sets are supported.

#### Solution ####

  * Use for example cursors, e.g. `SQLiteDatabase.rawQuery()`, `SQLiteDatabase.query()` to query multiple rows or columns from the database.