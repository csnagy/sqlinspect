## Metrics ##

### Java-SQL Metrics ###

For a JavaElement, we calculate these metrics by summing up from lower elements at lower levels to their parents. The lowest level is a CompilationUnit, i.e., a method.

  * *NumberOfHotspots*: The total number of hotspots in the JavaElement.
  * *NumberOfQueries*: The total number of queries in the JavaElement.
  * *NumberOfColumnAccesses*: The total number of (unique) columns accessed through the JavaElement. When aggregated to the parents, the union of the accessed column sets will be calculated.
  * *NumberOfTableAccesses*: The total number of (unique) tables accessed through the JavaElement. When aggregated to the parents, the union of the accessed column sets will be calculated.

### SQL Metrics ###

The following metrics are calculated for a SQL query:

  * *NumberOfResultFields*: The number of returned fields by the query
  * *NumberOfTables*: The number of tables used in the query
  * *NumberOfQueries*: The number of subqueries
  * *NestingLevel*: The nesting level of subqueries