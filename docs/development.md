## Building ##

### Compilation requirements ###

  * Apache Maven 3.9.9 (or newer)
  * Java 21 (or newer)
  * Python 3.x (in the PATH)
     - PyYaml >= 6.0 (`pip install pyyaml`)
     - Jinja2 >= 3.0 (`pip install Jinja2`)
  * Android SDK 35 (or newer, OPTIONAL, only for unit tests)

### How to compile ###

  * Create and activate Python environment: 
    - `python3 -m venv .venv`
    - `. .venv/bin/activate`
    - `pip3 install pyyaml Jinja2`
  * Install third-party maven dependencies: 
    - `cd sqlinspect.mavendeps`
    - `mvn clean install`
    - See: https://flames-of-code.netlify.app/blog/maven-and-tycho-deps/
    - Or: https://wiki.eclipse.org/Tycho/How_Tos/Dependency_on_pom-first_artifacts
    - Or: https://vogella.com/blog/tycho-advanced/
  * Update the target file to point the mavendeps package:
    - `sed -i "/sqlinspect.mavendeps/ s@path=\".*\/\.m2@path=\"$HOME\/\.m2@" sqlinspect.target-platform/sqlinspect.target-platform.target`
    - On Mac: `sed -i "" "/sqlinspect.mavendeps/ s@path=\".*\/\.m2@path=\"$HOME\/\.m2@" sqlinspect.target-platform/sqlinspect.target-platform.target`
  * Compile without tests: `mvn clean package`
  * Compile with tests:
    - the pom.xml of sqlinspect.plugin.test looks for `${env.ANDROID_SDK_ROOT}/platforms/android-26/android.jar`, so make sure that `ANDROID_SDK_ROOT` is set and/or update the pom.xml to point to `android.jar`
    - run `mvn clean package`
    - run `mvn test`
  * If you get `[ERROR] Failed to execute goal org.apache.maven.plugins:maven-dependency-plugin:3.0.0:copy-dependencies (copy-dependencies) on project sqlinspect.sqlan` -> rerun the compilation without clean
    
### Notes for the maven build ###

  * To generate ANTLR sources:
    - `mvn generate-sources`
  * To generate javadoc:
    - `mvn javadoc:javadoc`
  * To generate project documentation, including reports:
    - `mvn -Dmdep.skip=true site`
  * Package without running tests:
    - `mvn package -DskipTests`
  * Run integration tests with Jacoco:
    - `mvn verify -Pjacoco`
  * Run integration tests with a specific android jar:
    - `mvn verify -Dandroid.jar.path=$ANDROID_SDK_ROOT/platforms/android-30/android.jar`
  * Run only a specific test:
    - `mvn verify -Dtest=sqlinspect.plugin.ui.tests.SimpleTest -DfailIfNoTests=false`
  * Exclude a specific test:
    - `mvn verify '-Dtest=!sqlinspect.plugin.ui.tests.PreferencesTest' -DfailIfNoTests=false`
  * Switch to mvn offline mode and skip "Fetching p2.index"
    - `mvn -o`
  * Update version:
    - `mvn org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=0.3.0-SNAPSHOT`


### Development in Eclipse ###

  * Import the project as a Maven build project to Eclipse
  * For the project configuration of the Eclipse project:
    - Add `target/generated-sources/antlr4` to the build path
    - Toggle `ignore optional compiler warnings:yes`
      (Apt M2E Connector Eclipse plugin should solve this, but it is
      not working anymore in Eclipse.)
  * In some cases, Eclipse does not recognize the generated source directories.
    If it happens, add the directories under `target/generated-sources/` to
    the build path as a source directory.

### Notes for Bitbucket Pipelines ###

  * Test pipelines:
    - docker run -it --volume <sqlinspectsrcdir>:/sqlinspect --workdir="/sqlinspect" --memory=2048m alvrme/alpine-android:android-35-jdk21 /bin/bash
    
### Notes for Windows ###

  * Prevent Windows Terminal to close automatically: 
    - Under Settings/Advanced add `"closeOnExit": "never"` to `profiles.defaults`
    - See: https://learn.microsoft.com/en-us/windows/terminal/customize-settings/profile-advanced#profile-termination-behavior