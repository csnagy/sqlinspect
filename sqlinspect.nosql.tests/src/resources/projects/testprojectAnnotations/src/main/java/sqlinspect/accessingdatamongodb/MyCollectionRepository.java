package sqlinspect.accessingdatamongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MyCollectionRepository extends MongoRepository<MyCollection, String> {

}