package sqlinspect.accessingdatamongodb;

import org.springframework.data.annotation.Id;
import java.util.Set;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "my_collection")
public class MyCollection {

	@Id
	public String id;

	@Field("field_name")
	public String name;

}