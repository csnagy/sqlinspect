package sqlinspect.accessingdatamongodb;

import org.springframework.data.annotation.Id;

public class BaseCollection {

	@Id
	public String id;

}