package sqlinspect.accessingdatamongodb;

import org.springframework.data.annotation.Id;
import java.util.Set;

public class MyCollection {

	@Id
	public String id;

	public String name;
	
    Set<String> stringSet;
    
    boolean booleanField;

}