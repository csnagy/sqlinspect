package sqlinspect.nosql.tests.springdata.mongodb;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.dbmodel.ArrayType;
import sqlinspect.dbmodel.Collection;
import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Field;
import sqlinspect.dbmodel.ObjectType;
import sqlinspect.dbmodel.Root;
import sqlinspect.dbmodel.Schema;
import sqlinspect.dbmodel.SimpleType;
import sqlinspect.dbmodel.util.PrettyPrinter;
import sqlinspect.nosql.springdata.mongodb.JSONSchemaExtractor;

class JSONSchemaExtractorTest {
	private static final Logger LOG = LoggerFactory.getLogger(JSONSchemaExtractorTest.class);

	// TODO: test this: https://www.journaldev.com/2324/jackson-json-java-parser-api-example-tutorial

	@Test
	void test() throws IOException {
		// example based on https://docs.mongodb.com/realm/mongodb/enforce-a-document-schema/
		String input = "{\n"
				+ "  \"bsonType\": \"object\",\n"
				+ "  \"title\": \"votes\",\n"
				+ "  \"required\": [\"name\", \"age\", \"favoriteColors\"],\n"
				+ "  \"properties\": {\n"
				+ "    \"name\": {\n"
				+ "      \"bsonType\": \"string\"\n"
				+ "    },\n"
				+ "    \"age\": {\n"
				+ "      \"bsonType\": \"int\",\n"
				+ "      \"minimum\": 13,\n"
				+ "      \"exclusiveMinimum\": false\n"
				+ "    },\n"
				+ "    \"favoriteColors\": {\n"
				+ "      \"bsonType\": \"array\",\n"
				+ "      \"uniqueItems\": true,\n"
				+ "      \"items\": {\n"
				+ "        \"bsonType\": \"object\",\n"
				+ "        \"properties\": {\n"
				+ "          \"rank\": { \"bsonType\": \"int\" },\n"
				+ "          \"name\": { \"bsonType\": \"string\" },\n"
				+ "          \"hexCode\": {\n"
				+ "            \"bsonType\": \"string\",\n"
				+ "            \"pattern\": \"^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$\"\n"
				+ "          }\n"
				+ "        }\n"
				+ "      }\n"
				+ "    }\n"
				+ "  }\n"
				+ "}";

		DBModelFactory factory = DBModelFactory.eINSTANCE;

		Root rootExtracted = factory.createRoot();
		DB dbExtracted = factory.createDB();
		dbExtracted.setName("DB");
		rootExtracted.getDatabases().add(dbExtracted);

		JSONSchemaExtractor extractor = new JSONSchemaExtractor(dbExtracted);
		extractor.extractSchema(input);

		Root rootReference = factory.createRoot();
		DB dbReference = factory.createDB();
		dbReference.setName("DB");
		rootReference.getDatabases().add(dbReference);

		Collection collection = factory.createCollection();
		collection.setName("votes");
		dbReference.getEntities().add(collection);

		Schema schema = factory.createSchema();
		collection.getSchemas().add(schema);

		Field field1 = factory.createField();
		field1.setName("name");
		schema.getFields().add(field1);
		SimpleType type1 = factory.createSimpleType();
		type1.setName("string");
		field1.setType(type1);

		Field field2 = factory.createField();
		field2.setName("age");
		schema.getFields().add(field2);
		SimpleType type2 = factory.createSimpleType();
		type2.setName("int");
		field2.setType(type2);

		Field field3 = factory.createField();
		field3.setName("favoriteColors");
		schema.getFields().add(field3);
		ArrayType type3 = factory.createArrayType();
		field3.setType(type3);

		ObjectType objectType = factory.createObjectType();
		type3.setElementType(objectType);

		Schema subSchema = factory.createSchema();
		objectType.setSchema(subSchema);

		Field subSchemaField1 = factory.createField();
		subSchemaField1.setName("rank");
		SimpleType subSchemaField1Type = factory.createSimpleType();
		subSchemaField1Type.setName("int");
		subSchemaField1.setType(subSchemaField1Type);
		subSchema.getFields().add(subSchemaField1);

		Field subSchemaField2 = factory.createField();
		subSchemaField2.setName("name");
		SimpleType subSchemaField2Type = factory.createSimpleType();
		subSchemaField2Type.setName("string");
		subSchemaField2.setType(subSchemaField2Type);
		subSchema.getFields().add(subSchemaField2);

		Field subSchemaField3 = factory.createField();
		subSchemaField3.setName("hexCode");
		SimpleType subSchemaField3Type = factory.createSimpleType();
		subSchemaField3Type.setName("string");
		subSchemaField3.setType(subSchemaField3Type);
		subSchema.getFields().add(subSchemaField3);

		LOG.debug(PrettyPrinter.prettyPrint(rootExtracted));
		LOG.debug(PrettyPrinter.prettyPrint(rootReference));

		assertTrue(EcoreUtil.equals(rootExtracted, rootReference));
	}
}
