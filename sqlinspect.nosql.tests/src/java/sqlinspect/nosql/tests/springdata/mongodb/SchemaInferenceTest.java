package sqlinspect.nosql.tests.springdata.mongodb;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.bson.BsonType;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.JavaModelException;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.dbmodel.ArrayType;
import sqlinspect.dbmodel.Collection;
import sqlinspect.dbmodel.DB;
import sqlinspect.dbmodel.DBModelFactory;
import sqlinspect.dbmodel.Field;
import sqlinspect.dbmodel.Root;
import sqlinspect.dbmodel.Schema;
import sqlinspect.dbmodel.SimpleType;
import sqlinspect.dbmodel.util.PrettyPrinter;
import sqlinspect.nosql.springdata.mongodb.SchemaInference;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.utils.EclipseProjectUtil;

class SchemaInferenceTest {
	private static final Logger LOG = LoggerFactory.getLogger(SchemaInferenceTest.class);

	protected static final String TEST_PROJ_DIR = System.getenv().get("NOSQLTEST_RES_DIR") + "/projects";
	protected static final String TEST_REF_DIR = System.getenv().get("NOSQLTEST_RES_DIR") + "/references";

	private final DBModelFactory factory = DBModelFactory.eINSTANCE;

	private final String classPath = System.getenv().get("SPRINGDATA_JARS");
	private static final String CLASSPATH_SEPARATOR = ":";

	@Test
	void testAccessingDataMongodb() throws IOException, JavaModelException, CoreException {
		String projectname = "accessing-data-mongodb";

		File projectdir = new File(TEST_PROJ_DIR, projectname);

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir.getAbsolutePath(), null,
				classPath, CLASSPATH_SEPARATOR);

		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);

		Root root = factory.createRoot();
		SchemaInference schemaInference = ContextInjectionFactory.make(SchemaInference.class, eclipseContext);
		DB db = schemaInference.inferDB(project, eclipseContext);
		root.getDatabases().add(db);

		Root refRoot = factory.createRoot();
		DB refDB = factory.createDB();
		refDB.setName("DB");
		refRoot.getDatabases().add(refDB);

		Collection refCollection = factory.createCollection();
		refCollection.setName("Customer");
		refDB.getEntities().add(refCollection);

		Schema refSchema = factory.createSchema();
		refCollection.getSchemas().add(refSchema);

		Field fieldFirstName = factory.createField();
		fieldFirstName.setName("firstName");
		SimpleType firstNameType = factory.createSimpleType();
		firstNameType.setName(BsonType.STRING.toString());
		fieldFirstName.setType(firstNameType);
		refSchema.getFields().add(fieldFirstName);

		Field fieldId = factory.createField();
		fieldId.setName("id");
		SimpleType idType = factory.createSimpleType();
		idType.setName(BsonType.STRING.toString());
		fieldId.setType(idType);
		refSchema.getFields().add(fieldId);

		Field fieldLastName = factory.createField();
		fieldLastName.setName("lastName");
		SimpleType lastNameType = factory.createSimpleType();
		lastNameType.setName(BsonType.STRING.toString());
		fieldLastName.setType(lastNameType);
		refSchema.getFields().add(fieldLastName);

		LOG.debug(PrettyPrinter.prettyPrint(root));
		LOG.debug(PrettyPrinter.prettyPrint(refRoot));

		assertTrue(EcoreUtil.equals(root, refRoot));
	}

	@Test
	void testTestprojectTypes() throws IOException, JavaModelException, CoreException {
		String projectname = "testprojectTypes";

		File projectdir = new File(TEST_PROJ_DIR, projectname);

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir.getAbsolutePath(), null,
				classPath, CLASSPATH_SEPARATOR);

		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);

		Root root = factory.createRoot();
		SchemaInference schemaInference = ContextInjectionFactory.make(SchemaInference.class, eclipseContext);
		DB db = schemaInference.inferDB(project, eclipseContext);
		root.getDatabases().add(db);

		Root refRoot = factory.createRoot();
		DB refDB = factory.createDB();
		refDB.setName("DB");
		refRoot.getDatabases().add(refDB);

		Collection refCollection = factory.createCollection();
		refCollection.setName("MyCollection");
		refDB.getEntities().add(refCollection);

		Schema refSchema = factory.createSchema();
		refCollection.getSchemas().add(refSchema);

		Field fieldBoolean = factory.createField();
		fieldBoolean.setName("booleanField");
		SimpleType booleanType = factory.createSimpleType();
		booleanType.setName("boolean"); // TODO check this
		fieldBoolean.setType(booleanType);
		refSchema.getFields().add(fieldBoolean);

		Field fieldId = factory.createField();
		fieldId.setName("id");
		SimpleType idType = factory.createSimpleType();
		idType.setName(BsonType.STRING.toString());
		fieldId.setType(idType);
		refSchema.getFields().add(fieldId);

		Field fieldName = factory.createField();
		fieldName.setName("name");
		SimpleType nameType = factory.createSimpleType();
		nameType.setName(BsonType.STRING.toString());
		fieldName.setType(nameType);
		refSchema.getFields().add(fieldName);

		Field fieldStringSet = factory.createField();
		fieldStringSet.setName("stringSet");
		ArrayType arrayType = factory.createArrayType();
		SimpleType elementType = factory.createSimpleType();
		elementType.setName(BsonType.STRING.toString());
		arrayType.setElementType(elementType);
		fieldStringSet.setType(arrayType);
		refSchema.getFields().add(fieldStringSet);

		LOG.debug(PrettyPrinter.prettyPrint(root));
		LOG.debug(PrettyPrinter.prettyPrint(refRoot));

		assertTrue(EcoreUtil.equals(root, refRoot));
	}

	@Test
	void testTestprojectInheritance() throws IOException, JavaModelException, CoreException {
		String projectname = "testprojectInheritance";

		File projectdir = new File(TEST_PROJ_DIR, projectname);

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir.getAbsolutePath(), null,
				classPath, CLASSPATH_SEPARATOR);

		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);

		Root root = factory.createRoot();
		SchemaInference schemaInference = ContextInjectionFactory.make(SchemaInference.class, eclipseContext);
		DB db = schemaInference.inferDB(project, eclipseContext);
		root.getDatabases().add(db);

		Root refRoot = factory.createRoot();
		DB refDB = factory.createDB();
		refDB.setName("DB");
		refRoot.getDatabases().add(refDB);

		Collection refCollection = factory.createCollection();
		refCollection.setName("MyCollection");
		refDB.getEntities().add(refCollection);

		Schema refSchema = factory.createSchema();
		refCollection.getSchemas().add(refSchema);

		Field fieldId = factory.createField();
		fieldId.setName("id");
		SimpleType idType = factory.createSimpleType();
		idType.setName(BsonType.STRING.toString());
		fieldId.setType(idType);
		refSchema.getFields().add(fieldId);

		Field fieldName = factory.createField();
		fieldName.setName("name");
		SimpleType nameType = factory.createSimpleType();
		nameType.setName(BsonType.STRING.toString());
		fieldName.setType(nameType);
		refSchema.getFields().add(fieldName);

		Field fieldBoolean = factory.createField();
		fieldBoolean.setName("intField");
		SimpleType booleanType = factory.createSimpleType();
		booleanType.setName(BsonType.INT32.toString());
		fieldBoolean.setType(booleanType);
		refSchema.getFields().add(fieldBoolean);

		LOG.debug(PrettyPrinter.prettyPrint(root));
		LOG.debug(PrettyPrinter.prettyPrint(refRoot));

		assertTrue(EcoreUtil.equals(root, refRoot));
	}

	@Test
	void testTestprojectEnum() throws IOException, JavaModelException, CoreException {
		String projectname = "testprojectEnum";

		File projectdir = new File(TEST_PROJ_DIR, projectname);

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir.getAbsolutePath(), null,
				classPath, CLASSPATH_SEPARATOR);

		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);

		Root root = factory.createRoot();
		SchemaInference schemaInference = ContextInjectionFactory.make(SchemaInference.class, eclipseContext);
		DB db = schemaInference.inferDB(project, eclipseContext);
		root.getDatabases().add(db);

		Root refRoot = factory.createRoot();
		DB refDB = factory.createDB();
		refDB.setName("DB");
		refRoot.getDatabases().add(refDB);

		Collection refCollection = factory.createCollection();
		refCollection.setName("MyCollection");
		refDB.getEntities().add(refCollection);

		Schema refSchema = factory.createSchema();
		refCollection.getSchemas().add(refSchema);

		Field fieldName = factory.createField();
		fieldName.setName("enumField");
		SimpleType nameType = factory.createSimpleType();
		nameType.setName(BsonType.STRING.toString());
		fieldName.setType(nameType);
		refSchema.getFields().add(fieldName);

		Field fieldId = factory.createField();
		fieldId.setName("id");
		SimpleType idType = factory.createSimpleType();
		idType.setName(BsonType.STRING.toString());
		fieldId.setType(idType);
		refSchema.getFields().add(fieldId);

		LOG.debug(PrettyPrinter.prettyPrint(root));
		LOG.debug(PrettyPrinter.prettyPrint(refRoot));

		assertTrue(EcoreUtil.equals(root, refRoot));
	}

	@Test
	void testTestprojectAnnotations() throws IOException, JavaModelException, CoreException {
		String projectname = "testprojectAnnotations";

		File projectdir = new File(TEST_PROJ_DIR, projectname);

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir.getAbsolutePath(), null,
				classPath, CLASSPATH_SEPARATOR);

		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);

		Root root = factory.createRoot();
		SchemaInference schemaInference = ContextInjectionFactory.make(SchemaInference.class, eclipseContext);
		DB db = schemaInference.inferDB(project, eclipseContext);
		root.getDatabases().add(db);

		Root refRoot = factory.createRoot();
		DB refDB = factory.createDB();
		refDB.setName("DB");
		refRoot.getDatabases().add(refDB);

		Collection refCollection = factory.createCollection();
		refCollection.setName("my_collection");
		refDB.getEntities().add(refCollection);

		Schema refSchema = factory.createSchema();
		refCollection.getSchemas().add(refSchema);

		Field fieldId = factory.createField();
		fieldId.setName("id");
		SimpleType idType = factory.createSimpleType();
		idType.setName(BsonType.STRING.toString());
		fieldId.setType(idType);
		refSchema.getFields().add(fieldId);

		Field fieldName = factory.createField();
		fieldName.setName("field_name");
		SimpleType nameType = factory.createSimpleType();
		nameType.setName(BsonType.STRING.toString());
		fieldName.setType(nameType);
		refSchema.getFields().add(fieldName);

		LOG.debug(PrettyPrinter.prettyPrint(root));
		LOG.debug(PrettyPrinter.prettyPrint(refRoot));

		assertTrue(EcoreUtil.equals(root, refRoot));
	}
}
