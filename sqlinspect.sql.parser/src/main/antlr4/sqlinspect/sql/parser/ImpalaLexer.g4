lexer grammar ImpalaLexer;

options {
  language = Java;
  superClass = SQLLexer;
}

//------------------------------------------------------------------
// LEXER RULES
//------------------------------------------------------------------

// Lexer fragments for case insensitive rules
fragment A_	: ('a'|'A');
fragment B_	: ('b'|'B');
fragment C_	: ('c'|'C');
fragment D_	: ('d'|'D');
fragment E_	: ('e'|'E');
fragment F_	: ('f'|'F');
fragment G_	: ('g'|'G');
fragment H_	: ('h'|'H');
fragment I_	: ('i'|'I');
fragment J_	: ('j'|'J');
fragment K_	: ('k'|'K');
fragment L_	: ('l'|'L');
fragment M_	: ('m'|'M');
fragment N_	: ('n'|'N');
fragment O_	: ('o'|'O');
fragment P_	: ('p'|'P');
fragment Q_	: ('q'|'Q');
fragment R_	: ('r'|'R');
fragment S_	: ('s'|'S');
fragment T_	: ('t'|'T');
fragment U_	: ('u'|'U');
fragment V_	: ('v'|'V');
fragment W_	: ('w'|'W');
fragment X_	: ('x'|'X');
fragment Y_	: ('y'|'Y');
fragment Z_	: ('z'|'Z');

// keywords
KW_ADD: A_ D_ D_;
KW_AGGREGATE: A_ G_ G_ R_ E_ G_ A_ T_ E_;
KW_ALL: A_ L_ L_;
KW_ALTER: A_ L_ T_ E_ R_;
KW_ANALYTIC: A_ N_ A_ L_ Y_ T_ I_ C_;
KW_AND: '&&' | A_ N_ D_;
KW_ANTI: A_ N_ T_ I_;
KW_API_VERSION: A_ P_ I_ '_' V_ E_ R_ S_ I_ O_ N_;
KW_ARRAY: A_ R_ R_ A_ Y_;
KW_AS: A_ S_;
KW_ASC: A_ S_ C_;
KW_AVRO: A_ V_ R_ O_;
KW_BETWEEN: B_ E_ T_ W_ E_ E_ N_;
KW_BIGINT: B_ I_ G_ I_ N_ T_;
KW_BINARY: B_ I_ N_ A_ R_ Y_;
KW_BLOCKSIZE: B_ L_ O_ C_ K_ '_' S_ I_ Z_ E_;
KW_BOOLEAN: B_ O_ O_ L_ E_ A_ N_;
KW_BY: B_ Y_;
KW_CACHED: C_ A_ C_ H_ E_ D_;
KW_CASCADE: C_ A_ S_ C_ A_ D_ E_;
KW_CASE: C_ A_ S_ E_;
KW_CAST: C_ A_ S_ T_;
KW_CHANGE: C_ H_ A_ N_ G_ E_;
KW_CHAR: C_ H_ A_ R_;
KW_CLASS: C_ L_ A_ S_ S_;
KW_CLOSE_FN: C_ L_ O_ S_ E_ '_' F_ N_;
KW_COLUMN: C_ O_ L_ U_ M_ N_;
KW_COLUMNS: C_ O_ L_ U_ M_ N_ S_;
KW_COMMENT: C_ O_ M_ M_ E_ N_ T_;
KW_COMPRESSION: C_ O_ M_ P_ R_ E_ S_ S_ I_ O_ N_;
KW_COMPUTE: C_ O_ M_ P_ U_ T_ E_;
KW_CREATE: C_ R_ E_ A_ T_ E_;
KW_CROSS: C_ R_ O_ S_ S_;
KW_CURRENT: C_ U_ R_ R_ E_ N_ T_;
KW_DATA: D_ A_ T_ A_;
KW_DATABASE: D_ A_ T_ A_ B_ A_ S_ E_;
KW_DATABASES: D_ A_ T_ A_ B_ A_ S_ E_ S_;
KW_DATE: D_ A_ T_ E_;
KW_DATETIME: D_ A_ T_ E_ T_ I_ M_ E_;
KW_DECIMAL: D_ E_ C_ I_ M_ A_ L_;
KW_DEFAULT: D_ E_ F_ A_ U_ L_ T_;
KW_DELETE: D_ E_ L_ E_ T_ E_;
KW_DELIMITED: D_ E_ L_ I_ M_ I_ T_ E_ D_;
KW_DESC: D_ E_ S_ C_;
KW_DESCRIBE: D_ E_ S_ C_ R_ I_ B_ E_;
KW_DISTINCT: D_ I_ S_ T_ I_ N_ C_ T_;
KW_DIV: D_ I_ V_;
KW_DOUBLE: D_ O_ U_ B_ L_ E_;
KW_DROP: D_ R_ O_ P_;
KW_ELSE: E_ L_ S_ E_;
KW_ENCODING: E_ N_ C_ O_ D_ I_ N_ G_;
KW_END: E_ N_ D_;
KW_ESCAPED: E_ S_ C_ A_ P_ E_ D_;
KW_EXISTS: E_ X_ I_ S_ T_ S_;
KW_EXPLAIN: E_ X_ P_ L_ A_ I_ N_;
KW_EXTENDED: E_ X_ T_ E_ N_ D_ E_ D_;
KW_EXTERNAL: E_ X_ T_ E_ R_ N_ A_ L_;
KW_FALSE: F_ A_ L_ S_ E_;
KW_FIELDS: F_ I_ E_ L_ D_ S_;
KW_FILEFORMAT: F_ I_ L_ E_ F_ O_ R_ M_ A_ T_;
KW_FILES: F_ I_ L_ E_ S_;
KW_FINALIZE_FN: F_ I_ N_ A_ L_ I_ Z_ E_ '_' F_ N_;
KW_FIRST: F_ I_ R_ S_ T_;
KW_FLOAT: F_ L_ O_ A_ T_;
KW_FOLLOWING: F_ O_ L_ L_ O_ W_ I_ N_ G_;
KW_FOR: F_ O_ R_;
KW_FORMAT: F_ O_ R_ M_ A_ T_;
KW_FORMATTED: F_ O_ R_ M_ A_ T_ T_ E_ D_;
KW_FROM: F_ R_ O_ M_;
KW_FULL: F_ U_ L_ L_;
KW_FUNCTION: F_ U_ N_ C_ T_ I_ O_ N_;
KW_FUNCTIONS: F_ U_ N_ C_ T_ I_ O_ N_ S_;
KW_GRANT: G_ R_ A_ N_ T_;
KW_GROUP: G_ R_ O_ U_ P_;
KW_HASH: H_ A_ S_ H_;
KW_IGNORE: I_ G_ N_ O_ R_ E_;
KW_HAVING: H_ A_ V_ I_ N_ G_;
KW_IF: I_ F_;
KW_ILIKE: I_ L_ I_ K_ E_;
KW_IN: I_ N_;
KW_INCREMENTAL: I_ N_ C_ R_ E_ M_ E_ N_ T_ A_ L_;
KW_INIT_FN: I_ N_ I_ T_ '_' F_ N_;
KW_INNER: I_ N_ N_ E_ R_;
KW_INPATH: I_ N_ P_ A_ T_ H_;
KW_INSERT: I_ N_ S_ E_ R_ T_;
KW_INT: I_ N_ T_;
KW_INTERMEDIATE: I_ N_ T_ E_ R_ M_ E_ D_ I_ A_ T_ E_;
KW_INTERVAL: I_ N_ T_ E_ R_ V_ A_ L_;
KW_INTO: I_ N_ T_ O_;
KW_INVALIDATE: I_ N_ V_ A_ L_ I_ D_ A_ T_ E_;
KW_IREGEXP: I_ R_ E_ G_ E_ X_ P_;
KW_IS: I_ S_;
KW_JOIN: J_ O_ I_ N_;
KW_KUDU: K_ U_ D_ U_;
KW_LAST: L_ A_ S_ T_;
KW_LEFT: L_ E_ F_ T_;
KW_LIKE: L_ I_ K_ E_;
KW_LIMIT: L_ I_ M_ I_ T_;
KW_LINES: L_ I_ N_ E_ S_;
KW_LOAD: L_ O_ A_ D_;
KW_LOCATION: L_ O_ C_ A_ T_ I_ O_ N_;
KW_MAP: M_ A_ P_;
KW_MERGE_FN: M_ E_ R_ G_ E_ '_' F_ N_;
KW_METADATA: M_ E_ T_ A_ D_ A_ T_ A_;
KW_NOT: N_ O_ T_;
KW_NULL: N_ U_ L_ L_;
KW_NULLS: N_ U_ L_ L_ S_;
KW_OFFSET: O_ F_ F_ S_ E_ T_;
KW_ON: O_ N_;
KW_OR: '||' | O_ R_;
KW_ORDER: O_ R_ D_ E_ R_;
KW_OUTER: O_ U_ T_ E_ R_;
KW_OVER: O_ V_ E_ R_;
KW_OVERWRITE: O_ V_ E_ R_ W_ R_ I_ T_ E_;
KW_PARQUET: P_ A_ R_ Q_ U_ E_ T_;
KW_PARQUETFILE: P_ A_ R_ Q_ U_ E_ T_ F_ I_ L_ E_;
KW_PARTITION: P_ A_ R_ T_ I_ T_ I_ O_ N_;
KW_PARTITIONED: P_ A_ R_ T_ I_ T_ I_ O_ N_ E_ D_;
KW_PARTITIONS: P_ A_ R_ T_ I_ T_ I_ O_ N_ S_;
KW_PRECEDING: P_ R_ E_ C_ E_ D_ I_ N_ G_;
KW_PREPARE_FN: P_ R_ E_ P_ A_ R_ E_ '_' F_ N_;
KW_PRIMARY: P_ R_ I_ M_ A_ R_ Y_;
KW_PRODUCED: P_ R_ O_ D_ U_ C_ E_ D_;
KW_PURGE: P_ U_ R_ G_ E_;
KW_RANGE: R_ A_ N_ G_ E_;
KW_RCFILE: R_ C_ F_ I_ L_ E_;
KW_RECOVER: R_ E_ C_ O_ V_ E_ R_;
KW_REFRESH: R_ E_ F_ R_ E_ S_ H_;
KW_REGEXP: R_ E_ G_ E_ X_ P_;
KW_RENAME: R_ E_ N_ A_ M_ E_;
KW_REPEATABLE: R_ E_ P_ E_ A_ T_ A_ B_ L_ E_;
KW_REPLACE: R_ E_ P_ L_ A_ C_ E_;
KW_REPLICATION: R_ E_ P_ L_ I_ C_ A_ T_ I_ O_ N_;
KW_RESTRICT: R_ E_ S_ T_ R_ I_ C_ T_;
KW_RETURNS: R_ E_ T_ U_ R_ N_ S_;
KW_REVOKE: R_ E_ V_ O_ K_ E_;
KW_RIGHT: R_ I_ G_ H_ T_;
KW_RLIKE: R_ L_ I_ K_ E_;
KW_ROLE: R_ O_ L_ E_;
KW_ROLES: R_ O_ L_ E_ S_;
KW_ROW: R_ O_ W_;
KW_ROWS: R_ O_ W_ S_;
KW_SCHEMA: S_ C_ H_ E_ M_ A_;
KW_SCHEMAS: S_ C_ H_ E_ M_ A_ S_;
KW_SELECT: S_ E_ L_ E_ C_ T_;
KW_SEMI: S_ E_ M_ I_;
KW_SEQUENCEFILE: S_ E_ Q_ U_ E_ N_ C_ E_ F_ I_ L_ E_;
KW_SERDEPROPERTIES: S_ E_ R_ D_ E_ P_ R_ O_ P_ E_ R_ T_ I_ E_ S_;
KW_SERIALIZE_FN: S_ E_ R_ I_ A_ L_ I_ Z_ E_ '_' F_ N_;
KW_SET: S_ E_ T_;
KW_SHOW: S_ H_ O_ W_;
KW_SMALLINT: S_ M_ A_ L_ L_ I_ N_ T_;
KW_SORT: S_ O_ R_ T_;
KW_STORED: S_ T_ O_ R_ E_ D_;
KW_STRAIGHT_JOIN: S_ T_ R_ A_ I_ G_ H_ T_ '_' J_ O_ I_ N_;
KW_STRING: S_ T_ R_ I_ N_ G_;
KW_STRUCT: S_ T_ R_ U_ C_ T_;
KW_SYMBOL: S_ Y_ M_ B_ O_ L_;
KW_TABLE: T_ A_ B_ L_ E_;
KW_TABLES: T_ A_ B_ L_ E_ S_;
KW_TABLESAMPLE: T_ A_ B_ L_ E_ S_ A_ M_ P_ L_ E_;
KW_TBLPROPERTIES: T_ B_ L_ P_ R_ O_ P_ E_ R_ T_ I_ E_ S_;
KW_TERMINATED: T_ E_ R_ M_ I_ N_ A_ T_ E_ D_;
KW_TEXTFILE: T_ E_ X_ T_ F_ I_ L_ E_;
KW_THEN: T_ H_ E_ N_;
KW_TIMESTAMP: T_ I_ M_ E_ S_ T_ A_ M_ P_;
KW_TINYINT: T_ I_ N_ Y_ I_ N_ T_;
KW_TRUNCATE: T_ R_ U_ N_ C_ A_ T_ E_;
KW_STATS: S_ T_ A_ T_ S_;
KW_TO: T_ O_;
KW_TRUE: T_ R_ U_ E_;
KW_UNBOUNDED: U_ N_ B_ O_ U_ N_ D_ E_ D_;
KW_UNCACHED: U_ N_ C_ A_ C_ H_ E_ D_;
KW_UNION: U_ N_ I_ O_ N_;
KW_UPDATE: U_ P_ D_ A_ T_ E_;
KW_UPDATE_FN: U_ P_ D_ A_ T_ E_ '_' F_ N_;
KW_UPSERT: U_ P_ S_ E_ R_ T_;
KW_USE: U_ S_ E_;
KW_USING: U_ S_ I_ N_ G_;
KW_VALUES: V_ A_ L_ U_ E_ S_;
KW_VARCHAR: V_ A_ R_ C_ H_ A_ R_;
KW_VIEW: V_ I_ E_ W_;
KW_WHEN: W_ H_ E_ N_;
KW_WHERE: W_ H_ E_ R_ E_;
KW_WITH: W_ I_ T_ H_;

// Non-keyword tokens
//IDENT: IDENTIFIER
COLON: ':';
SEMICOLON: ';';
COMMA: ',';
DOT: '.';
DOTDOTDOT: '...';
STAR: '*';
LPAREN: '(';
RPAREN: ')';
LBRACKET: '[';
RBRACKET: ']';
DIVIDE: '/';
MOD: '%';
ADD: '+';
SUBTRACT: '-';
//UNARYSIGN: UNARYSIGN
BITAND: '&';
BITOR: '|';
BITXOR: '^';
BITNOT: '~';
EQUAL: '=';
NOT: '!';
NOTEQUAL: '!=';
LESSTHAN: '<'; //OK
GREATERTHAN: '>'; //OK
//FACTORIAL: FACTORIAL;
//COMMENTED_PLAN_HINT_START: COMMENTED_PLAN_HINT_START;
//COMMENTED_PLAN_HINT_END: COMMENTED_PLAN_HINT_END;
//IDENT: IDENT;
//EMPTY_IDENT: EMPTY_IDENT;
//NUMERIC_OVERFLOW: NUMERIC_OVERFLOW;
//INTEGER_LITERAL:INTEGER_LITERAL;
//DECIMAL_LITERAL: DECIMAL_LITERAL;
//STRING_LITERAL: STRING_LITERAL;
//UNMATCHED_STRING_LITERAL: UNMATCHED_STRING_LITERAL;
//UNEXPECTED_CHAR: UNEXPECTED_CHAR;
//EOF: EOF;

fragment LineTerminator: '\r'|'\n'|'\r\n';

Whitespace: ( LineTerminator | ' ' | '\t' | '\f' ) -> channel(HIDDEN);

fragment UpperCaseLetter: [\p{Lu}];
fragment LowerCaseLetter: [\p{Ll}];
fragment TitleCaseLetter: [\p{Lt}];
fragment ModifierLetter: [\p{Lm}];
fragment OtherLetter: [\p{Lo}];
fragment IsLetter: UpperCaseLetter | LowerCaseLetter | TitleCaseLetter | ModifierLetter;
fragment LetterNumber: [\p{Nl}];

// According to jflex 'jletter includes all characters for which the Java function Character.isJavaIdentifierStart returns true'
// http://jflex.de/manual.html

// Following the specification of Java Character.isJavaIdentifierStart
// https://docs.oracle.com/javase/7/docs/api/java/lang/Character.html#isJavaIdentifierStart(char)

fragment JLetter: IsLetter | LetterNumber | '$' | '_';
fragment Digit: [\p{Digit}];
fragment Letter: [\p{Letter}];
fragment JLetterDigit: JLetter | Digit;
fragment IntegerLiteral: Digit;

fragment FLit1: [0-9]+ '.' [0-9]*;
fragment FLit2: '.' [0-9]+;
fragment FLit3: [0-9]+;
fragment Exponent: [eE] [+-]? [0-9]+;
fragment DecimalLiteral: (FLit1|FLit2|FLit3) Exponent?;

fragment IdentifierOrKw: [0-9]* JLetter JLetterDigit* | '&&' | '||';
fragment QuotedIdentifier:  '`' ([^`])* '`';

fragment SingleQuotedStringLiteral:  '\'' (~['])* '\'';
fragment DoubleQuotedStringLiteral:  '"' (~["])* '"';

fragment EolHintBegin: '--' ' '* '+';
fragment CommentedHintBegin: '/*' ' '* '+';
fragment CommentedHintEnd: '*/';

INTEGER_LITERAL: IntegerLiteral;
STRING_LITERAL: SingleQuotedStringLiteral | DoubleQuotedStringLiteral;
DECIMAL_LITERAL: DecimalLiteral;
IDENT: IdentifierOrKw | QuotedIdentifier; 

COMMENTED_PLAN_HINT_START: CommentedHintBegin | EolHintBegin;
COMMENTED_PLAN_HINT_END: CommentedHintEnd;


EndOfLineComment: ( ('--'|'#') ~('\n'|'\r')* '\r'? '\n' ) -> channel(HIDDEN);
TraditionalComment: ( '/*' .*? '*/' ) -> channel(HIDDEN);



