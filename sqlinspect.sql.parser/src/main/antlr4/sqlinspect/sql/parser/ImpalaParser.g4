parser grammar ImpalaParser;

options {
    tokenVocab = ImpalaLexer;
    language = Java;
    superClass = SQLParser;
}

@header {
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.builder.*;
import sqlinspect.sql.asg.clause.*;
import sqlinspect.sql.asg.common.*;
import sqlinspect.sql.asg.expr.*;
import sqlinspect.sql.asg.schema.*;
import sqlinspect.sql.asg.statm.*;
import sqlinspect.sql.asg.type.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
}

@members {
    private static final Logger logger = LoggerFactory.getLogger(ImpalaParser.class);

    private ImpalaParserActions act = new ImpalaParserActions(this);

    {
        setErrorHandler(new SQLErrorStrategy(ImpalaLexer.SEMICOLON));
    }
    
    // a bit of a hack, see https://github.com/antlr/antlr4/issues/1133
    public ImpalaParser(TokenStream input, ASG asg) {
    	super(input, asg);
    	_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
    }
    
    @Override
    public SQLDialect getDialect() {
    	return SQLDialect.IMPALA;
    }

    @Override
    protected SQLParserActions getActions() {
    	return act;
    }

    @Override
    public List<Statement> parseStatements() {
        return start().ret;
    }
}

//------------------------------------------------------------------
// PARSER RULES
//------------------------------------------------------------------

start returns [List<Statement> ret] @init { $ret = new ArrayList<Statement>(); }
:
   (stmt =  statement
       { $ret.add($stmt.ret); }
   )* EOF
;

statement returns [Statement ret] @init {
    int lastErrorCount = 0;
    Statement s = null;
    $ret = null;
 }
:
    (
        query = query_stmt
        { s=$query.ret; }

        | insert = insert_stmt
        {  s=$insert.ret;  }

        | update = update_stmt
        { s = $update.ret; }

        | upsert = upsert_stmt
        { s = $upsert.ret; }

        | delete = delete_stmt
        { s = $delete.ret; }

        | use = use_stmt
        { s = $use.ret; }

        | show_tables = show_tables_stmt
        | show_dbs = show_dbs_stmt
        | show_partitions = show_partitions_stmt
        | show_range_partitions = show_range_partitions_stmt
        | show_stats = show_stats_stmt
        | show_functions = show_functions_stmt
        | show_data_srcs = show_data_srcs_stmt
        | show_create_tbl = show_create_tbl_stmt
        | show_create_function = show_create_function_stmt
        | show_files = show_files_stmt
        | describe_db = describe_db_stmt
        | describe_table = describe_table_stmt
        | alter_tbl = alter_tbl_stmt
        | alter_view = alter_view_stmt
        | compute_stats = compute_stats_stmt
        | drop_stats = drop_stats_stmt
        | create_tbl_as_select = create_tbl_as_select_stmt
        { s = $create_tbl_as_select.ret; }

        | create_tbl_like = create_tbl_like_stmt
        { s = $create_tbl_like.ret; }

        | create_tbl = create_tbl_stmt
        { s = $create_tbl.ret; }

        | create_view = create_view_stmt
        { s = $create_view.ret; }

        | create_data_src = create_data_src_stmt
        | create_db = create_db_stmt
        { s = $create_db.ret; }

        | create_udf = create_udf_stmt
        { s = $create_udf.ret; }

        | create_uda = create_uda_stmt
        | drop_db = drop_db_stmt
        { s = $drop_db.ret; }

        | drop_tbl = drop_tbl_stmt
        { s = $drop_tbl.ret; }

        | drop_view = drop_view_stmt
        { s = $drop_view.ret; }

        | drop_function = drop_function_stmt
        { s = $drop_function.ret; }

        | drop_data_src = drop_data_src_stmt
        | explain = explain_stmt
        | load = load_stmt
        | truncate = truncate_stmt
        { s = $truncate.ret; }

        | reset_metadata = reset_metadata_stmt
        | set = set_stmt
        { s = $set.ret; }

        | show_roles = show_roles_stmt
        | show_grant_role = show_grant_role_stmt
        | create_drop_role = create_drop_role_stmt
        | grant_role = grant_role_stmt
        | revoke_role = revoke_role_stmt
        | grant_privilege = grant_privilege_stmt
        | revoke_privilege = revoke_privilege_stmt
    ) t = SEMICOLON
    | t = SEMICOLON
;

catch [ RecognitionException re ] {
    _localctx.exception = re;
    _errHandler.reportError(this, re);
    _errHandler.recover(this, re);
    setStmtError(true);
    setStmtErrorDetails(sqlErrorListener.getLast());
    logger.debug("LAST ERROR: " + (sqlErrorListener.getLast() == null ? "NULL" : sqlErrorListener.getLast().toString()));
} catch [ASGException e] {
    SQLErrorStrategy.consumeUntil(this, ImpalaLexer.SEMICOLON);
    logger.error("ASGException while parsing statement: ", e);
    setStmtError(true);
    setStmtErrorDetails(new SQLError(0, 0, e.toString(), e));
} catch [Throwable tr] {
    SQLErrorStrategy.consumeUntil(this, ImpalaLexer.SEMICOLON);
    logger.error("Unexpected exception while parsing statement: ", tr);
    setStmtError(true);
    setStmtErrorDetails(new SQLError(0, 0, tr.toString(), tr));
} finally {
	if (s == null && isStmtError()) {
		s = new ParserErrorBuilder(asg).result();
	}
	
    if (s != null) {
    	if (isStmtError()) {
    		SQLError err = getStmtErrorDetails();
    		s.setError(true);
    		if (err != null) {
    			s.setErrorMessage(err.getMessage());
    			s.setErrorLine(err.getLine());
    			s.setErrorCol(err.getCharPositionInLine());
			
    		}
    	}
        act.finishNode(s, $t);
    }
    $ret = s;
    setStmtError(false);
}


load_stmt
:
    KW_LOAD KW_DATA KW_INPATH path = STRING_LITERAL overwrite = overwrite_val?
    KW_INTO KW_TABLE table = table_name partition = opt_partition_spec?

;

truncate_stmt returns [Truncate ret] @init{ $ret = null; boolean ifExists = false; }
:
    t= KW_TRUNCATE KW_TABLE?
    ( if_exists = if_exists_val { ifExists = true; } )?
    tbl_name = table_name
    { $ret = act.buildTruncate($t, $tbl_name.ret, ifExists); }
;

overwrite_val
:
    KW_OVERWRITE
;

reset_metadata_stmt
:
    KW_INVALIDATE KW_METADATA
    | KW_INVALIDATE KW_METADATA table = table_name
    | KW_REFRESH table = table_name
    | KW_REFRESH table = table_name partition = partition_spec
    | KW_REFRESH KW_FUNCTIONS db = ident_or_default
;

explain_stmt
:
    KW_EXPLAIN query = query_expr
    | KW_EXPLAIN insert = insert_stmt
    | KW_EXPLAIN ctas_stmt = create_tbl_as_select_stmt
    | KW_EXPLAIN update = update_stmt
    | KW_EXPLAIN upsert = upsert_stmt
    | KW_EXPLAIN delete = delete_stmt
;

insert_stmt returns [Insert ret]
@init { $ret = null; ExpressionList with = null; boolean overwrite = false; } :
    (
      w = opt_with_clause
      { with = $w.ret; }
    )?
    t=KW_INSERT
    ( KW_OVERWRITE { overwrite = true; } )?
    opt_kw_table? table = table_name
    { $ret = act.buildInsert($t, overwrite, $table.ret, with); }

    (
      LPAREN (
        col_perm = ident_list
        { act.insertSetColumnList($ret, $col_perm.ret); }
      )? RPAREN
    )?
    (
      list = partition_clause
      { act.insertSetPartition($ret, $list.ret); }
    )?
    hints = opt_plan_hints?
    (
      query = query_expr
      { act.insertSetQuery($ret, $query.ret); }
    )?
;

update_stmt returns [Update ret] @init { $ret = null; } :
    t=KW_UPDATE target_table = dotted_path KW_SET values = update_set_expr_list
    { $ret = act.buildUpdate($t, $target_table.ret, $values.ret); }
    (
      tables = from_clause
      { act.updateSetFrom($ret, $tables.ret); }
    )?
    (
      where_predicate = where_clause
      { act.updateSetWhere($ret, $where_predicate.ret); }
    )?
;

update_set_expr_list returns [ExpressionList ret] @init { $ret = null; }
:
	slot = slot_ref t = EQUAL e = expr
	{ $ret = act.buildExpressionList(act.buildKeyValue($t, $slot.ret, $e.ret)); }

	(
		COMMA slot = slot_ref t = EQUAL e = expr
		{ act.addToExpressionList($ret, act.buildKeyValue($t, $slot.ret, $e.ret)); }

	)*
;

upsert_stmt returns [Upsert ret] @init { $ret = null; ExpressionList with = null; } :
    (
      w = opt_with_clause
      { with = $w.ret; }
    )?
    t=KW_UPSERT KW_INTO opt_kw_table? table = table_name
    { $ret = act.buildUpsert($t, $table.ret, with); }

    (
      LPAREN (
          col_perm = ident_list
          { act.upsertSetColumnList($ret, $col_perm.ret); }
      )? RPAREN
    )?
    hints = opt_plan_hints?
    (
      query = query_expr
      { act.upsertSetQuery($ret, $query.ret); }
    )?

;

delete_stmt returns [Delete ret] @init { $ret = null; }
:
    dt = KW_DELETE
    (
        KW_FROM? target_table = dotted_path
        { $ret = act.buildDelete($dt, $target_table.ret); }

        | target_table = dotted_path from = from_clause
        { $ret = act.buildDelete($dt, $from.ret); }

    )
    ( where = where_clause
      { act.deleteSetWhere($ret, $where.ret); }
    )?
;

opt_kw_table
:
    KW_TABLE
;

show_roles_stmt
:
    KW_SHOW KW_ROLES

    | KW_SHOW KW_ROLE KW_GRANT KW_GROUP group = ident_or_default

    | KW_SHOW KW_CURRENT KW_ROLES

;

show_grant_role_stmt
:
    KW_SHOW KW_GRANT KW_ROLE role = ident_or_default

    | KW_SHOW KW_GRANT KW_ROLE role = ident_or_default KW_ON server_kw =
    server_ident

    | KW_SHOW KW_GRANT KW_ROLE role = ident_or_default KW_ON KW_DATABASE
    db_name = ident_or_default

    | KW_SHOW KW_GRANT KW_ROLE role = ident_or_default KW_ON KW_TABLE tbl_name
    = table_name

    | KW_SHOW KW_GRANT KW_ROLE role = ident_or_default KW_ON uri_kw = uri_ident
    STRING_LITERAL

;

create_drop_role_stmt
:
    KW_CREATE KW_ROLE role = ident_or_default

    | KW_DROP KW_ROLE role = ident_or_default

;

grant_role_stmt
:
    KW_GRANT KW_ROLE role = ident_or_default KW_TO KW_GROUP group =
    ident_or_default

;

revoke_role_stmt
:
    KW_REVOKE KW_ROLE role = ident_or_default KW_FROM KW_GROUP group =
    ident_or_default

;

grant_privilege_stmt
:
    KW_GRANT priv = privilege_spec KW_TO opt_role = opt_kw_role? role =
    ident_or_default grant_opt = opt_with_grantopt?

;

revoke_privilege_stmt
:
    KW_REVOKE grant_opt = opt_grantopt_for? priv = privilege_spec KW_FROM
    opt_role = opt_kw_role? role = ident_or_default

;

privilege_spec
:
    priv = privilege KW_ON server_kw = server_ident

    | priv = privilege KW_ON server_kw = server_ident server_name =
    ident_or_default

    | priv = privilege KW_ON KW_DATABASE db_name = ident_or_default

    | priv = privilege KW_ON KW_TABLE tbl_name = table_name

    | priv = privilege LPAREN ( cols = ident_list )? RPAREN KW_ON KW_TABLE
    tbl_name = table_name

    | priv = privilege KW_ON uri_kw = uri_ident uri = STRING_LITERAL

;

privilege
:
    KW_SELECT

    | KW_INSERT

    | KW_ALL

;

opt_grantopt_for
:
    KW_GRANT option = option_ident KW_FOR

;

opt_with_grantopt
:
    KW_WITH KW_GRANT option = option_ident

;

opt_kw_role
:
    KW_ROLE
;

partition_def
:
    partition = partition_spec ( location = location_val )? cache_op = cache_op_val?

;

partition_def_list
:
    item = partition_def

    | list = partition_def_list item = partition_def

;

alter_tbl_stmt returns [AlterTable ret] @init { $ret = null; Table table = null; }
:
    KW_ALTER KW_TABLE table = table_name replace = replace_existing_cols_val
    KW_COLUMNS LPAREN col_defs = column_def_list [table] RPAREN

    | KW_ALTER KW_TABLE table = table_name KW_ADD ( if_not_exists =
     if_not_exists_val )? partitions1 = partition_def_list

    | KW_ALTER KW_TABLE table = table_name KW_DROP opt_kw_column? col_name =
    ident_or_default

    | KW_ALTER KW_TABLE table = table_name KW_ADD ( if_not_exists =
     if_not_exists_val )? KW_RANGE partition2 = range_param

    | KW_ALTER KW_TABLE table = table_name KW_CHANGE opt_kw_column? col_name =
    ident_or_default col_def = column_def [table]

    | KW_ALTER KW_TABLE table = table_name KW_DROP ( if_exists = if_exists_val )?
    partitions3 = partition_set ( purge = purge_val )?

    | KW_ALTER KW_TABLE table = table_name partitions4 = opt_partition_set?
    KW_SET KW_FILEFORMAT file_format = file_format_val

    | KW_ALTER KW_TABLE table = table_name KW_DROP ( if_exists = if_exists_val )?
    KW_RANGE partition = range_param

    | KW_ALTER KW_TABLE table = table_name partitions5 = opt_partition_set?
    KW_SET KW_LOCATION location = STRING_LITERAL

    | KW_ALTER KW_TABLE table = table_name KW_RENAME KW_TO new_table =
    table_name

    | KW_ALTER KW_TABLE table = table_name partitions6 = opt_partition_set?
    KW_SET target = table_property_type LPAREN properties = properties_map
    RPAREN

    | KW_ALTER KW_TABLE table = table_name KW_SORT KW_BY LPAREN
    ( col_names = ident_list )? RPAREN

    | KW_ALTER KW_TABLE table = table_name partition7 = opt_partition_set? KW_SET
    KW_COLUMN KW_STATS col = ident_or_default LPAREN map = properties_map
    RPAREN

    | KW_ALTER KW_TABLE table = table_name partitions8 = opt_partition_set?
    KW_SET cache_op = cache_op_val?

    | KW_ALTER KW_TABLE table = table_name KW_RECOVER KW_PARTITIONS

    | KW_ALTER KW_TABLE table = table_name KW_ALTER opt_kw_column? col_name =
    ident_or_default KW_SET opts = column_options_map

    | KW_ALTER KW_TABLE table = table_name KW_ALTER opt_kw_column? col_name =
    ident_or_default KW_DROP KW_DEFAULT

;

table_property_type
:
    KW_TBLPROPERTIES

    | KW_SERDEPROPERTIES

;

opt_kw_column
:
    KW_COLUMN
;

replace_existing_cols_val :
  KW_REPLACE
  | KW_ADD
  ;

create_db_stmt returns [CreateDatabase ret] @init { $ret = null; boolean ifNotExists = false; Token loc = null; } :
  t = KW_CREATE db_or_schema_kw
  (
    if_not_exists = if_not_exists_val
    { ifNotExists = true; }
  )?
  db_name = ident_or_default
  comment = comment_val?
  ( location = location_val { loc = $location.ret; } )?
  { $ret = act.buildCreateDatabase($t, $db_name.ret, ifNotExists, loc); }
  ;

create_tbl_as_select_stmt returns [CreateTable ret] @init { $ret = null; } :
  tbl_def = tbl_def_without_col_defs
  { $ret = $tbl_def.ret; }

  opts = tbl_options [$ret.getSchemaTable() ]
  KW_AS select_stmt1 = query_expr
  | tbl_def = tbl_def_without_col_defs
  { $ret = $tbl_def.ret; }
    primary = primary_keys
    partition_params = partitioned_data_layout
    opts = tbl_options [$ret.getSchemaTable() ]
    KW_AS select_stmt2 = query_expr
  | tbl_def = tbl_def_without_col_defs
  { $ret = $tbl_def.ret; }
    KW_PARTITIONED KW_BY LPAREN partition_cols = ident_list RPAREN
    opts = tbl_options [$ret.getSchemaTable() ]
    KW_AS select_stmt3 = query_expr
  ;

create_tbl_stmt returns [CreateTable ret] @init { $ret = null; } :
  tbl_def1 = tbl_def_without_col_defs
  { $ret = $tbl_def1.ret; }
    opts = tbl_options [$ret.getSchemaTable() ]
  | tbl_def2 = tbl_def_without_col_defs
  { $ret = $tbl_def2.ret; }
    partition = partition_column_defs[$ret.getSchemaTable()]
    opts = tbl_options [$ret.getSchemaTable() ]
  | tbl_def3 = tbl_def_with_col_defs
  { $ret = $tbl_def3.ret; }
    data_layout = opt_tbl_data_layout[$ret.getSchemaTable()]
    opts = tbl_options [$ret.getSchemaTable() ]
  | tbl_def4 = tbl_def_with_col_defs
  { $ret = $tbl_def4.ret; }
    KW_PRODUCED KW_BY KW_DATA is_source_id = source_ident data_src_name = ident_or_default
    init_string = opt_init_string_val
    comment = comment_val?
  | tbl_def5 = tbl_def_without_col_defs
  { $ret = $tbl_def5.ret; }
    KW_LIKE schema_file_format = file_format_val
    schema_location = STRING_LITERAL
    data_layout = opt_tbl_data_layout[$ret.getSchemaTable()]
    opts = tbl_options [$ret.getSchemaTable() ]
  ;

create_tbl_like_stmt returns [CreateTable ret] @init { $ret = null; boolean ifNotExists = false; } :
  tbl_def = tbl_def_without_col_defs
  { $ret = $tbl_def.ret; }

  ( sort_cols = opt_sort_cols )?

  KW_LIKE other_table = table_name
  { act.createTableLike($ret.getSchemaTable(), $other_table.ret); }

  comment = comment_val?
  file_format  = file_format_create_table_val?
  ( location = location_val { act.tableSetLocation($ret.getSchemaTable(), $location.ret); })?
  ;

tbl_def_without_col_defs returns [CreateTable ret] @ init
{ $ret = null; boolean ifNotExists = false; boolean isExternal = false; } :
  t = KW_CREATE
  ( external  = external_val { isExternal = true; })?
  KW_TABLE
  ( if_not_exists = if_not_exists_val { ifNotExists = true; })?
  table = table_name
  { $ret = act.buildCreateTable($t, $table.ret, ifNotExists, isExternal, false, null); }
  ;

tbl_def_with_col_defs returns [CreateTable ret] @init { $ret = null; } :
  tbl_def = tbl_def_without_col_defs
  { $ret = $tbl_def.ret; }

  LPAREN list  = column_def_list[$ret.getSchemaTable()] RPAREN

  ( COMMA
    keys = primary_keys RPAREN
  )?
  ;

primary_keys :
  KW_PRIMARY key_ident LPAREN col_names = ident_list RPAREN
  ;

tbl_options [ Table tab ]:
  ( sort_cols = opt_sort_cols )?
  comment = comment_val? row_format = row_format_val?
  serde_props = serde_properties? file_format = file_format_create_table_val?
  ( location = location_val { act.tableSetLocation(tab, $location.ret); } )?
  cache_op = cache_op_val?
  tbl_props = tbl_properties?
  ;

opt_sort_cols :
  KW_SORT KW_BY LPAREN ( col_names = ident_list )? RPAREN
  ;

opt_tbl_data_layout [Table table] :
  partition = partition_column_defs[table]
  | data_layout = partitioned_data_layout
  ;

partitioned_data_layout :
  partition_params = partition_param_list
  | /* empty */
  ;

partition_column_defs [Table table]:
  KW_PARTITIONED KW_BY LPAREN col_defs = column_def_list [table] RPAREN
  ;

partition_param_list :
  KW_PARTITION KW_BY list = hash_partition_param_list
  | KW_PARTITION KW_BY rng = range_partition_param
  | KW_PARTITION KW_BY list = hash_partition_param_list COMMA rng = range_partition_param
  ;

hash_partition_param_list :
  dc = hash_partition_param
  | list = hash_partition_param_list COMMA d = hash_partition_param
  ;

hash_partition_param :
  KW_HASH LPAREN cols = ident_list RPAREN KW_PARTITIONS numPartitions = INTEGER_LITERAL
  | KW_HASH KW_PARTITIONS numPartitions = INTEGER_LITERAL
  ;

range_partition_param :
  KW_RANGE LPAREN cols = ident_list RPAREN LPAREN ranges = range_params_list RPAREN
  | KW_RANGE LPAREN ranges = range_params_list RPAREN
  ;

range_params_list :
  param  = range_param
  | list = range_params_list COMMA param = range_param
  ;

range_param :
  KW_PARTITION lower_val = opt_lower_range_val? KW_VALUES upper_val = opt_upper_range_val?
  | KW_PARTITION val = dotted_path EQUAL e = expr
  | KW_PARTITION val = dotted_path EQUAL LPAREN l = expr_list RPAREN

  ;

opt_lower_range_val :
  l = expr LESSTHAN
  | l = expr LESSTHAN EQUAL
  ;

opt_upper_range_val :
  LESSTHAN l = expr
  | LESSTHAN EQUAL l = expr
  ;

create_udf_stmt returns [CreateFunction ret]
@init {
  $ret = null; boolean ifNotExists = false;
  FunctionDefParams params = null; Type returnType = null;
}
:
  t=KW_CREATE KW_FUNCTION
  ( if_not_exists = if_not_exists_val { ifNotExists = true; } )?
  fn_name = function_name
  ( fn_args = function_def_args { params = $fn_args.ret; } )?
  ( KW_RETURNS return_type = type_def { returnType = $return_type.ret; } )?
  KW_LOCATION binary_path = STRING_LITERAL
  arg_map = function_def_args_map?
  { $ret = act.buildCreateFunction($t, $fn_name.ret, params, returnType, ifNotExists); }
  ;

create_uda_stmt :
  KW_CREATE KW_AGGREGATE KW_FUNCTION ( if_not_exists = if_not_exists_val )?
  fn_name = function_name fn_args = function_def_args
  KW_RETURNS return_type = type_def
  intermediate_type = opt_aggregate_fn_intermediate_type_def?
  KW_LOCATION binary_path = STRING_LITERAL
  arg_map = function_def_args_map?
  ;

cache_op_val :
  KW_CACHED KW_IN pool_name = STRING_LITERAL replication = opt_cache_op_replication?
  | KW_UNCACHED
  ;

opt_cache_op_replication :
  KW_WITH KW_REPLICATION EQUAL replication = INTEGER_LITERAL
  ;

comment_val returns [Token ret_tok, Literal ret] @init {$ret = null; }:
  t=KW_COMMENT comment = STRING_LITERAL
  { $ret_tok = $t; $ret = act.buildLiteral($comment); }
  ;

location_val returns [ Token ret ] @init { $ret = null; } :
  KW_LOCATION location = STRING_LITERAL
  { $ret = $location; }
  ;

opt_init_string_val :
  LPAREN init_string = STRING_LITERAL RPAREN
  ;

external_val :
  KW_EXTERNAL
  ;

purge_val :
  KW_PURGE
  ;

if_not_exists_val :
  KW_IF KW_NOT KW_EXISTS
  ;

row_format_val :
  KW_ROW KW_FORMAT KW_DELIMITED field_terminator = field_terminator_val?
  escaped_by = escaped_by_val? line_terminator = line_terminator_val?
  ;

escaped_by_val :
  KW_ESCAPED KW_BY escaped_by = STRING_LITERAL
  ;

line_terminator_val :
  KW_LINES line_terminator = terminator_val
  ;

field_terminator_val :
  KW_FIELDS field_terminator = terminator_val
  ;

terminator_val :
  KW_TERMINATED KW_BY terminator = STRING_LITERAL
  ;

file_format_create_table_val :
  KW_STORED KW_AS file_format = file_format_val
  ;

file_format_val :
  KW_KUDU
  | KW_PARQUET
  | KW_PARQUETFILE
  | KW_TEXTFILE
  | KW_SEQUENCEFILE
  | KW_RCFILE
  | KW_AVRO
  ;

tbl_properties :
  KW_TBLPROPERTIES LPAREN map = properties_map RPAREN
  ;

serde_properties :
  KW_WITH KW_SERDEPROPERTIES LPAREN map = properties_map RPAREN
  ;

properties_map :
  key = STRING_LITERAL EQUAL value = STRING_LITERAL
  | properties = properties_map COMMA key = STRING_LITERAL EQUAL value = STRING_LITERAL
  ;

column_def_list [Table table] returns [ArrayList<Column> ret] @init { $ret = null; } :
  col_def = column_def [table]
  { $ret = new ArrayList<Column>(); }

  (
    COMMA col_def = column_def [table]
    { $ret.add($column_def.ret); }
  )*
  ;

column_def [Table table] returns [Column ret] @init { $ret = null; } :
  col_name = ident_or_default t1 = type_def
  { $ret = act.newColumn(table, $col_name.ret, $t1.ret); }

  | col_name = ident_or_default t1 = type_def opts = column_options_map
  { $ret = act.newColumn(table, $col_name.ret, $t1.ret, $opts.ret); }
  ;

column_options_map returns [List<ColumnAttribute> ret]
@init { $ret = new ArrayList<ColumnAttribute>(); }
:
  col_option = column_option
  { $ret.add($col_option.ret); }
  ( col_option = column_option
      { $ret.add($col_option.ret); }
  )*
  ;

column_option returns [ColumnAttribute ret]:
  primary_key = is_primary_key_val
  { $ret = $primary_key.ret; }

  | nullability = nullability_val
  { $ret = $nullability.ret; }

  | encoding = encoding_val
  { $ret = $encoding.ret; }

  | compression = compression_val
  { $ret = $compression.ret; }

  | dv = default_val
  { $ret = $dv.ret; }

  | block_size = block_size_val
  { $ret = $block_size.ret; }

  | comment = comment_val
  { $ret = act.buildColumnAttribute($comment.ret_tok, $comment.ret); }
  ;

is_primary_key_val returns [ColumnAttribute ret] @init { $ret = null; } :
  t=KW_PRIMARY key_ident
  { act.buildColumnAttribute($t); }
  ;

nullability_val returns [ColumnAttribute ret] @init { $ret = null; } :
  t1=KW_NOT t2=KW_NULL
  { act.buildColumnAttribute($t1, $t2); }

  | t2=KW_NULL
  { act.buildColumnAttribute($t2); }
  ;

encoding_val returns [ColumnAttribute ret] @init { $ret = null; } :
  t=KW_ENCODING encoding_ident = ident_or_default
  { act.buildColumnAttribute($t, $encoding_ident.ret); }
  ;

compression_val returns [ColumnAttribute ret] @init { $ret = null; } :
  t=KW_COMPRESSION compression_ident = ident_or_default
  { act.buildColumnAttribute($t, $compression_ident.ret); }
  ;

default_val returns [ColumnAttribute ret] @init { $ret = null; } :
  t=KW_DEFAULT dv = expr
  { act.buildColumnAttribute($t, $dv.ret); }
  ;

block_size_val returns [ColumnAttribute ret] @init { $ret = null; } :
  t=KW_BLOCKSIZE block_size = literal
  { act.buildColumnAttribute($t, $block_size.ret); }
  ;

create_view_stmt returns [CreateView ret] @init { $ret = null; boolean ifNotExists = false; } :
  t=KW_CREATE KW_VIEW
  ( if_not_exists = if_not_exists_val { ifNotExists = true; } )?
  view_name = table_name
  { $ret = act.buildCreateView($t, $view_name.ret, ifNotExists); }

  ( col_defs = view_column_defs [ $ret.getView() ] )?
  comment = comment_val?
  KW_AS view_def = query_expr
  { act.createViewSetQuery($ret, $view_def.ret); }
  ;

create_data_src_stmt :
  KW_CREATE KW_DATA is_source_id = source_ident
  ( if_not_exists = if_not_exists_val )? data_src_name = ident_or_default
  KW_LOCATION location = STRING_LITERAL
  KW_CLASS class_name = STRING_LITERAL
  KW_API_VERSION api_version = STRING_LITERAL
  ;

key_ident :
  ident = IDENT
  ;

system_ident :
  ident = IDENT
  ;

source_ident :
  ident = IDENT
  ;

sources_ident :
  ident = IDENT
  ;

uri_ident :
  ident = IDENT
  ;

server_ident :
  ident = IDENT
  ;

option_ident :
  ident = IDENT
  ;

view_column_defs [ View view ]:
  LPAREN view_col_defs = view_column_def_list [ view ] RPAREN
  ;

view_column_def_list [ View view ]:
  col_def = view_column_def [ view ]
  ( COMMA col_def = view_column_def [ view ] )*
  ;

view_column_def [ View view ]:
  col_name = ident_or_default comment = comment_val?
  { act.newColumn(view, $col_name.ret, null); }
  ;

alter_view_stmt :
  KW_ALTER KW_VIEW table = table_name KW_AS view_def = query_expr
  | KW_ALTER KW_VIEW before_table = table_name KW_RENAME KW_TO new_table = table_name
  ;

cascade_val :
  KW_CASCADE
  | KW_RESTRICT
  ;

compute_stats_stmt :
  KW_COMPUTE KW_STATS table = table_name
  | KW_COMPUTE KW_INCREMENTAL KW_STATS table = table_name
  | KW_COMPUTE KW_INCREMENTAL KW_STATS table = table_name partitions = partition_set
  ;

drop_stats_stmt :
  KW_DROP KW_STATS table = table_name
  | KW_DROP KW_INCREMENTAL KW_STATS table = table_name partitions = partition_set
  ;

drop_db_stmt returns [ DropDatabase ret] @init { $ret = null; boolean ifExists = false; }:
  t=KW_DROP db_or_schema_kw
  ( if_exists = if_exists_val { ifExists = true; })?
  db_name = ident_or_default
  cascade = cascade_val?
  { $ret = act.buildDropDatabase($t, $db_name.ret, ifExists); }
  ;

drop_tbl_stmt returns [ DropTable ret] @init { $ret = null; boolean ifExists = false; boolean purge = false; }:
  t=KW_DROP KW_TABLE
  ( if_exists = if_exists_val { ifExists = true; })?
  table = table_name
  ( purge = purge_val { purge = true; } )?
  { $ret = act.buildDropTable($t, $table.ret, ifExists, false, purge); }
  ;

drop_view_stmt returns [ DropView ret] @init { $ret = null; boolean ifExists = false; }:
  t=KW_DROP KW_VIEW
  ( if_exists = if_exists_val { ifExists = true; })?
  table = table_name
  { $ret = act.buildDropView($t, $table.ret, ifExists); }
  ;


drop_function_stmt returns [ DropFunction ret] @init { $ret = null; boolean ifExists = false; }:
  t=KW_DROP is_aggregate = opt_is_aggregate_fn? KW_FUNCTION
  ( if_exists = if_exists_val )? fn_name = function_name
  { $ret = act.buildDropFunction($t, $fn_name.ret, ifExists); }

  ( fn_args = function_def_args )?
  ;

drop_data_src_stmt :
  KW_DROP KW_DATA is_source_id = source_ident ( if_exists = if_exists_val )?
  data_src_name = ident_or_default
  ;

db_or_schema_kw :
  KW_DATABASE
  | KW_SCHEMA
  ;

dbs_or_schemas_kw :
  KW_DATABASES
  | KW_SCHEMAS
  ;

if_exists_val :
  KW_IF KW_EXISTS
  ;

partition_clause returns [ ExpressionList ret ] @init { $ret = null; } :
  KW_PARTITION LPAREN list = partition_key_value_list RPAREN
  { $ret = $list.ret; }
  ;

partition_key_value_list returns [ ExpressionList ret ] @init { $ret = null; } :
  item = partition_key_value
  { $ret = act.buildExpressionList($item.ret); }
  (
    COMMA item = partition_key_value
    { act.addToExpressionList($ret, $item.ret); }
  )*
  ;

partition_set :
  KW_PARTITION LPAREN list = expr_list RPAREN
  ;

opt_partition_set :
  ps = partition_set
  ;

partition_spec :
  KW_PARTITION LPAREN list = static_partition_key_value_list RPAREN
  ;

opt_partition_spec :
  ps = partition_spec
  ;

static_partition_key_value_list :
  item = static_partition_key_value
  | list = static_partition_key_value_list COMMA item = static_partition_key_value
  ;

partition_key_value returns [ KeyValue ret] @init {$ret = null; } :
  column = ident_or_default
  { $ret = act.buildKeyValue(null, $column.ret, null); }

  | partition = static_partition_key_value
  { $ret = $partition.ret; }
  ;

static_partition_key_value returns [ KeyValue ret] @init { $ret = null; } :
  column = ident_or_default t=EQUAL e = expr
  { $ret = act.buildKeyValue($t, $column.ret, $e.ret); }
  ;

function_def_args returns [FunctionDefParams ret] @init { $ret = null; boolean varargs = false; } :
  LPAREN RPAREN
  { $ret = act.buildFunctionDefParams(null, false); }
  | LPAREN args = function_def_arg_list
    ( var_args = opt_is_varargs { varargs = true; } )?
    RPAREN
  { $ret = act.buildFunctionDefParams($args.ret, varargs); }
  ;

function_def_arg_list returns [ArrayList<Type> ret] @init { $ret = null; } :
  td1 = type_def
  { $ret = new ArrayList<Type>();
    $ret.add($td1.ret);
  }
  (
    COMMA td2 = type_def
    { $ret.add($td2.ret); }
  )*
  ;

opt_is_aggregate_fn :
  KW_AGGREGATE
  ;

opt_is_varargs :
  DOTDOTDOT
  ;

opt_aggregate_fn_intermediate_type_def :
  KW_INTERMEDIATE td = type_def
  ;

function_def_args_map :
  key = function_def_arg_key EQUAL value = STRING_LITERAL
  ( key = function_def_arg_key EQUAL value = STRING_LITERAL )*
  ;

function_def_arg_key :
  KW_COMMENT
  | KW_SYMBOL
  | KW_PREPARE_FN
  | KW_CLOSE_FN
  | KW_UPDATE_FN
  | KW_INIT_FN
  | KW_SERIALIZE_FN
  | KW_MERGE_FN
  | KW_FINALIZE_FN
  ;

query_stmt returns [Select ret] @init { $ret = null; } :
  query = query_expr
  { $ret = act.buildSelect($query.ret); }
  ;

query_expr returns [Query ret]
@init { $ret = null; ExpressionList with = null; }
:
	(
		w = opt_with_clause
		{ with = $w.ret; }

	)?
	(
		operands = union_operand_list
		{ $ret = $operands.ret; }

		| union = union_with_order_by_or_limit
		{ $ret = $union.ret; }

	)
	{  if (with!=null) {
          act.querySetWith($ret, with);
      }
    }

;

opt_with_clause returns [ExpressionList ret] @init { $ret = null; } :
  KW_WITH list = with_view_def_list
  { $ret = $list.ret; }
  ;

with_view_def returns [ViewDefinition ret] @init { $ret = null; } :
  alias1 = ident_or_default KW_AS LPAREN query = query_expr RPAREN
  { $ret = act.buildViewDefinition($alias1.ret, $query.ret, null); }

  | alias2 = STRING_LITERAL KW_AS LPAREN query = query_expr RPAREN
  { $ret = act.buildViewDefinition(act.buildLiteral($alias2), $query.ret, null); }

  | alias3 = ident_or_default LPAREN col_names = ident_list RPAREN t=KW_AS LPAREN
    query = query_expr RPAREN
  { $ret = act.buildViewDefinition($alias3.ret, $query.ret, $col_names.ret); }


  | alias4 = STRING_LITERAL LPAREN col_names = ident_list RPAREN
    t=KW_AS LPAREN query = query_expr RPAREN
  { $ret = act.buildViewDefinition(act.buildLiteral($alias4), $query.ret, $col_names.ret); }
  ;

with_view_def_list returns [ExpressionList ret] @init { $ret = null; } :
  v = with_view_def
  { $ret=act.buildExpressionList($v.ret); }
  (
    COMMA v = with_view_def
    { act.addToExpressionList($ret, $v.ret); }
  )*
  ;

union_with_order_by_or_limit returns [Query ret] @init { $ret = null; } :
  operands = union_operand_list
    { $ret = $operands.ret; }

    (

      KW_ORDER KW_BY orderByClause = order_by_elements
      { act.querySetOrderBy($ret, $orderByClause.ret); }

      (
        offsetExpr = opt_offset_param
      { act.querySetLimit($ret, act.buildLimit($tl, null, $offsetExpr.ret)); }
      )?
    |
      tl=KW_LIMIT limitExpr = expr
      { act.querySetLimit($ret, act.buildLimit($tl, $limitExpr.ret, null)); }

    |
      KW_ORDER KW_BY orderByClause = order_by_elements
      { act.querySetOrderBy($ret, $orderByClause.ret); }

      tl=KW_LIMIT limitExpr = expr
      {
         Limit limit = act.buildLimit($tl, $limitExpr.ret, null);
         act.querySetLimit($ret, limit);
      }
      (
        offsetExpr = opt_offset_param
       { act.limitSetOffset(limit, $offsetExpr.ret); }
      )?

  )
  ;

union_operand returns [Query ret] @init { $ret = null; } :
  select = select_stmt
  { $ret = $select.ret; }

  | values = values_stmt
  { $ret = $values.ret; }

  | LPAREN query = query_expr RPAREN
  { $ret = $query.ret; }

  ;

union_operand_list returns [Query ret] @init { $ret = null; } :
  operand = union_operand
  { $ret = $operand.ret; }

  (
     op = union_op operand = union_operand
     { $ret = act.buildBinaryQuery($op.tok1, $op.tok2, $ret, $operand.ret); }
  )*
  ;

union_op returns [Token tok1, Token tok2] @init { $tok1 = null; $tok2 = null; } :
  t1=KW_UNION
  { $tok1 = $t1; }

  | t1=KW_UNION t2=KW_DISTINCT
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_UNION t2=KW_ALL
  { $tok1 = $t1; $tok2 = $t2; }
  ;


values_stmt returns [Values ret] @init { $ret = null; } :
  t=KW_VALUES
  ( operands = values_operand_list
  | LPAREN operands = values_operand_list RPAREN
  )
  { $ret = act.buildValues($t, $operands.ret); }

  (
    orderByClause = opt_order_by_clause
    { act.querySetOrderBy($ret, $orderByClause.ret); }

  )?
  (
    limitOffsetClause = opt_limit_offset_clause
    { act.querySetLimit($ret, $limitOffsetClause.ret); }
  )?

  ;

values_operand_list returns [Expression ret] @init { $ret = null; boolean listed = false; } :
  LPAREN selectList = select_list RPAREN
  {  $ret = $selectList.ret;  }

  (
     COMMA LPAREN selectList = select_list RPAREN
     {
       if (listed == false) {
         $ret = act.buildExpressionList(act.getExpressionList($ret));
         listed = true;
       }
       act.addToExpressionList((ExpressionList)$ret, act.getExpressionList($selectList.ret));
     }
  )*
  ;

use_stmt returns [Use ret] @init { $ret = null; } :
  t=KW_USE db = ident_or_default
  { $ret = act.buildUse($t, $db.ret); }
  ;

show_tables_stmt :
  KW_SHOW KW_TABLES
  | KW_SHOW KW_TABLES showPattern = show_pattern
  | KW_SHOW KW_TABLES KW_IN db = ident_or_default
  | KW_SHOW KW_TABLES KW_IN db = ident_or_default showPattern = show_pattern
  ;

show_dbs_stmt :
  KW_SHOW dbs_or_schemas_kw
  | KW_SHOW dbs_or_schemas_kw showPattern = show_pattern
  ;

show_stats_stmt :
  KW_SHOW KW_TABLE KW_STATS table = table_name
  | KW_SHOW KW_COLUMN KW_STATS table = table_name
  ;

show_partitions_stmt :
  KW_SHOW KW_PARTITIONS table = table_name
  ;

show_range_partitions_stmt :
  KW_SHOW KW_RANGE KW_PARTITIONS table = table_name
  ;

show_functions_stmt :
  KW_SHOW fn_type = opt_function_category? KW_FUNCTIONS
  | KW_SHOW fn_type = opt_function_category? KW_FUNCTIONS showPattern = show_pattern
  | KW_SHOW fn_type = opt_function_category? KW_FUNCTIONS KW_IN db = ident_or_default
  | KW_SHOW fn_type = opt_function_category? KW_FUNCTIONS KW_IN db = ident_or_default
      showPattern = show_pattern
  ;

opt_function_category :
  KW_AGGREGATE
  | KW_ANALYTIC
  ;

show_data_srcs_stmt :
  KW_SHOW KW_DATA is_sources_id = sources_ident
  | KW_SHOW KW_DATA is_sources_id = sources_ident showPattern = show_pattern
  ;

show_pattern :
  showPattern = STRING_LITERAL
  | KW_LIKE showPattern = STRING_LITERAL
  ;

show_create_tbl_stmt :
  KW_SHOW KW_CREATE object_type = show_create_tbl_object_type table = table_name
  ;

show_create_tbl_object_type :
  KW_TABLE
  | KW_VIEW
  ;

show_create_function_stmt :
  KW_SHOW KW_CREATE KW_FUNCTION fn_name = function_name
  | KW_SHOW KW_CREATE KW_AGGREGATE KW_FUNCTION fn_name = function_name
  ;

show_files_stmt :
  KW_SHOW KW_FILES KW_IN table = table_name partitions = opt_partition_set?
  ;

describe_db_stmt :
  KW_DESCRIBE db_or_schema_kw style = describe_output_style? db = ident_or_default
  ;

describe_table_stmt :
  KW_DESCRIBE style = describe_output_style? path = dotted_path
  ;

describe_output_style :
  KW_FORMATTED
  | KW_EXTENDED
  ;

select_stmt returns [SelectExpression ret] @init { $ret = null; } :
    selectList = select_clause
    { $ret = $selectList.ret; }

  |
    selectList = select_clause
    { $ret = $selectList.ret; }

    fromClause = from_clause
    { act.selectExpressionSetFrom($ret, $fromClause.ret); }

    ( wherePredicate = where_clause
      { act.selectExpressionSetWhere($ret, $wherePredicate.ret); }
    )?

    (
      groupingExprs = group_by_clause
      { act.selectExpressionSetGroupBy($ret, $groupingExprs.ret); }
    )?
    (
      havingPredicate = having_clause
      { act.selectExpressionSetHaving($ret, $havingPredicate.ret); }
    )?
    (
      orderByClause = opt_order_by_clause
      { act.querySetOrderBy($ret, $orderByClause.ret); }
    )?
    (
      limitOffsetClause = opt_limit_offset_clause
      { act.querySetLimit($ret, $limitOffsetClause.ret); }
    )?
  ;

select_clause returns [SelectExpression ret] @init { $ret = null; } :
  ts=KW_SELECT hints = opt_plan_hints? l = select_list
  { $ret = act.buildSelectExpression($ts, null, $l.ret); }

  | ts=KW_SELECT t2=KW_ALL hints = opt_plan_hints? l = select_list
  { $ret = act.buildSelectExpression($ts, $t2, $l.ret); }

  | ts=KW_SELECT t2=KW_DISTINCT hints = opt_plan_hints? l = select_list
  { $ret = act.buildSelectExpression($ts, $t2, $l.ret); }
  ;

set_stmt returns [Set ret] @init { $ret = null; }:
  t=KW_SET { $ret = act.buildSet($t); }
  ( key = ident_or_default EQUAL
      ( l1 = literal
        { act.addSetValue($ret,act.buildSetValue($key.ret, $l1.ret)); }
      | ts=SUBTRACT l2 = numeric_literal
        { act.addSetValue($ret,act.buildSetValue($key.ret, act.buildUnaryExpression($ts, $l2.ret))); }
      | ident = ident_or_default
        { act.addSetValue($ret,act.buildSetValue($key.ret, $ident.ret)); }
      )
  )?
  ;

select_list returns [Expression ret] @init { $ret = null; } :
  item = select_list_item
  { $ret = $item.ret; }

  ( COMMA item = select_list_item
    { $ret = act.buildOrAddToExpressionList($ret, $item.ret); }
  )*
  ;

select_list_item returns [Expression ret] @init { $ret = null; } :
  e1 = expr alias = alias_clause
  { $ret = act.buildAlias($e1.ret, $alias.ret, $alias.tok); }

  | e2 = expr
  { $ret = $e2.ret; }

  | e3 = star_expr
  { $ret = $e3.ret; }

  ;

alias_clause returns [Expression ret, Token tok] @init { $ret = null; $tok = null; } :
  t=KW_AS?  ident = ident_or_default
  { $ret = $ident.ret; $tok = $t; }

  | t=KW_AS? l = STRING_LITERAL
  { $ret = act.buildLiteral($l); $tok = $t; }

  ;

star_expr returns [Expression ret] @init { $ret = null; } :
  t = STAR
  { $ret = act.buildAsterisk($t); }

  | path = dotted_path td=DOT t=STAR
  { $ret = act.buildBinaryExpression($td, $path.ret, act.buildAsterisk($t)); }

  ;

table_name returns [Expression ret] @init { $ret = null; } :
  tbl = ident_or_default
  { $ret = $tbl.ret; }

  | db = ident_or_default t=DOT tbl = ident_or_default
  { $ret = act.buildBinaryExpression($t, $db.ret, $tbl.ret); }

  ;

function_name returns [Expression ret] @init { $ret = null; } :
  // Use 'dotted_path' to avoid a reduce/reduce with slot_ref.
  path = dotted_path
  { $ret = $path.ret; }

  ;

from_clause returns [Expression ret] @init { $ret = null; } :
  KW_FROM l = table_ref_list
  { $ret = $l.ret; }
  ;

table_ref_list returns [Expression ret] @init { $ret = null; } :
  table = table_ref hints = opt_plan_hints?
  { $ret = $table.ret; }

  | list = table_ref_list t=COMMA table = table_ref hints = opt_plan_hints?
  { $ret = act.buildJoin($list.ret, $table.ret, $t); }

  | list = table_ref_list t=KW_CROSS KW_JOIN join_hints = opt_plan_hints? table = table_ref
    (table_hints = opt_plan_hints)?
  { $ret = act.buildJoin($list.ret, $table.ret, $t); }

  | list = table_ref_list op = join_operator join_hints = opt_plan_hints? table = table_ref
    (table_hints = opt_plan_hints)?
  { $ret = act.buildJoin($list.ret, $table.ret, $op.tok1, $op.tok2); }

  | list = table_ref_list op = join_operator join_hints = opt_plan_hints? table = table_ref
    (table_hints = opt_plan_hints)? ton = KW_ON e = expr
  { $ret = act.buildJoin($list.ret, $table.ret, $op.tok1, $op.tok2);
    On on = act.buildOn($e.ret, $ton);
    act.joinSetCondition((Join)$ret, on);
  }

  | list = table_ref_list op = join_operator join_hints = opt_plan_hints? table = table_ref
    table_hints = opt_plan_hints? tusing = KW_USING LPAREN colNames = ident_list RPAREN
  { $ret = act.buildJoin($list.ret, $table.ret, $op.tok1, $op.tok2);
    Using using = act.buildUsing($colNames.ret, $tusing);
    act.joinSetCondition((Join)$ret, using);
  }
  ;

table_ref returns [TableRef ret] @init { $ret = null; } :
  ( path = dotted_path
    { $ret = act.buildTableRef($path.ret, null); }

    | path = dotted_path alias = alias_clause
    { $ret = act.buildTableRef($path.ret, $alias.ret); }

    | LPAREN query = query_expr RPAREN alias = alias_clause
    { $ret = act.buildTableRef($query.ret, $alias.ret); }
  )
  (
    tblsmpl = opt_tablesample
    { act.tableRefSetTableSample($ret, $tblsmpl.ret); }
  )?
  ;

join_operator returns [ Token tok1, Token tok2 ] @init{ $tok1 = null; $tok2 = null; }:
  t1=KW_INNER? KW_JOIN
  { $tok1 = $t1; }

  | t1=KW_LEFT t2=KW_OUTER? KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_RIGHT t2=KW_OUTER? KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_FULL t2=KW_OUTER? KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_LEFT t2=KW_SEMI KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_RIGHT t2=KW_SEMI KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_LEFT t2=KW_ANTI KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_RIGHT t2=KW_ANTI KW_JOIN
  { $tok1 = $t1; $tok2 = $t2; }

  ;

opt_plan_hints :
  COMMENTED_PLAN_HINT_START hints = plan_hint_list COMMENTED_PLAN_HINT_END
  | KW_STRAIGHT_JOIN
  | LBRACKET hints = plan_hint_list RBRACKET
  ;

plan_hint :
  KW_STRAIGHT_JOIN
  | name = IDENT
  | name = IDENT LPAREN args = ident_list RPAREN
  ;

plan_hint_list :
  hint = plan_hint
  | hints = plan_hint_list COMMA hint = plan_hint
  ;

opt_tablesample returns [TableSampleClause ret] @init { $ret = null; } :
  t=KW_TABLESAMPLE system_ident LPAREN p = INTEGER_LITERAL RPAREN
  { $ret = act.buildTableSampleClause($t, act.buildLiteral($p), null); }

  | t=KW_TABLESAMPLE system_ident LPAREN p = INTEGER_LITERAL RPAREN
    KW_REPEATABLE LPAREN s = INTEGER_LITERAL RPAREN
  { $ret = act.buildTableSampleClause($t, act.buildLiteral($p), act.buildLiteral($s)); }

  ;

ident_list returns [ExpressionList ret] @init { $ret = null; } :
  ident = ident_or_default
  { $ret = act.buildExpressionList($ident.ret); }

  (
    COMMA ident = ident_or_default
    { act.addToExpressionList($ret, $ident.ret); }
  )*
  ;

expr_list returns [ExpressionList ret] @init { $ret = null; } :
  e = expr
  { $ret = act.buildExpressionList($e.ret); }

  (
    COMMA e = expr
    { act.addToExpressionList($ret, $e.ret); }
  )*
  ;

where_clause returns [Expression ret] @init { $ret = null; } :
  KW_WHERE e = expr
  { $ret = $e.ret; }
  ;

group_by_clause returns [ExpressionList ret] @init { $ret = null; } :
  KW_GROUP KW_BY l = expr_list
  { $ret = $l.ret; }
  ;

having_clause returns [Expression ret] @init { $ret = null; } :
  KW_HAVING e = expr
  { $ret = $e.ret; }
  ;

opt_order_by_clause returns [ExpressionList ret] @init { $ret = null; } :
  KW_ORDER KW_BY l = order_by_elements
  { $ret = $l.ret; }
  ;

order_by_elements returns [ExpressionList ret] @init { $ret = null; } :
  e = order_by_element
  { $ret = act.buildExpressionList($e.ret); }

    (
       COMMA e = order_by_element
       { act.addToExpressionList( $ret, $e.ret); }
    )*
  ;

order_by_element returns [OrderByElement ret] @init { $ret = null; } :
  e = expr
  { $ret = act.buildOrderByElement($e.ret); }

  (
    o = opt_order_param
    { act.orderByElementSetKind($ret, $o.ret); }
  )?

  (
    n = opt_nulls_order_param
    { act.orderByElementSetNullOrder($ret, $n.tok1, $n.tok2); }
  )?
  ;

opt_order_param returns [Token ret] @init { $ret = null; } :
  t=KW_ASC
  { $ret = $t; }

  | t=KW_DESC
  { $ret = $t; }
  ;

opt_nulls_order_param returns [Token tok1, Token tok2] @init { $tok2 = null; $tok2=null; } :
  t1=KW_NULLS t2=KW_FIRST
  { $tok1 = $t1; $tok2 = $t2; }

  | t1=KW_NULLS t2=KW_LAST
  { $tok1 = $t1; $tok2 = $t2; }
  ;

opt_offset_param returns [Expression ret] @init { $ret = null; } :
  KW_OFFSET e = expr
  { $ret = $e.ret; }
  ;

opt_limit_offset_clause returns [Limit ret] @init { $ret = null; } :
  limitExpr = opt_limit_clause
  { $ret = $limitExpr.ret; }

  (
    offsetExpr = opt_offset_clause
    { act.limitSetOffset($ret, $offsetExpr.ret); }
  )?
  ;

opt_limit_clause returns [Limit ret] @init { $ret = null; } :
  t=KW_LIMIT limitExpr = expr
  { $ret=act.buildLimit($t, $limitExpr.ret, null); }
  ;

opt_offset_clause returns [Expression ret] @init { $ret = null; } :
  KW_OFFSET offsetExpr = expr
  { $ret = $offsetExpr.ret; }
  ;

cast_expr returns [Cast ret] @init { $ret = null; } :
  t=KW_CAST LPAREN e = expr KW_AS targetType = type_def RPAREN
  { $ret = act.buildCast($t, $e.ret, $targetType.ret); }
  ;

case_expr returns [Case ret] @init { $ret = null; } :
  t=KW_CASE caseExpr = expr
  { $ret = act.buildCase($t, $caseExpr.ret); }

    whenClauseList = case_when_clause_list
    { act.caseAddWhenClauseList($ret, $whenClauseList.ret); }

    (
      elseExpr = case_else_clause
      { act.caseSetElse($ret, $elseExpr.ret); }
    )?

    KW_END
  | t=KW_CASE
    { $ret = act.buildCase($t, null); }

    whenClauseList = case_when_clause_list
    { act.caseAddWhenClauseList($ret, $whenClauseList.ret); }

    (
      elseExpr = case_else_clause
      { act.caseSetElse($ret, $elseExpr.ret); }
    )?

    KW_END
  ;

case_when_clause_list returns [ArrayList<WhenClause> ret] @init { $ret = null; } :
  t=KW_WHEN whenExpr = expr KW_THEN thenExpr = expr
  {
    $ret = new ArrayList<WhenClause>();
    $ret.add(act.buildWhenClause($t, $whenExpr.ret, $thenExpr.ret));
  }

  (
    t=KW_WHEN whenExpr = expr KW_THEN thenExpr = expr
    { $ret.add(act.buildWhenClause($t, $whenExpr.ret, $thenExpr.ret)); }
  )*
  ;

case_else_clause returns [Expression ret] @init { $ret = null; } :
  KW_ELSE e = expr
  { $ret = $e.ret; }
  ;

sign_chain_expr returns [Expression ret] @init { $ret = null; } :
  t=SUBTRACT e = expr
  { $ret = act.addLiteralSign($t, $e.ret); }

  | t=ADD e = expr
  { $ret = act.addLiteralSign($t, $e.ret); }
  ;

expr returns [Expression ret] @init { $ret = null; } :
  npe = non_pred_expr
  { $ret = $npe.ret; }

  |
//timestamp_arithmetic_expr:
    it=KW_INTERVAL v = expr u = IDENT t=ADD te = expr
    { $ret = act.buildBinaryExpression($t, act.buildInterval($it, $v.ret, $u), $te.ret); }
    | te = expr t=SUBTRACT it=KW_INTERVAL v = expr u = IDENT
    { $ret = act.buildBinaryExpression($t, $te.ret, act.buildInterval($it, $v.ret, $u)); }

    | te = expr t=ADD it=KW_INTERVAL v = expr u = IDENT
    { $ret = act.buildBinaryExpression($t, $te.ret, act.buildInterval($it, $v.ret, $u)); }

    | functionName = function_name LPAREN l = expr_list COMMA
      it = KW_INTERVAL v = expr u = IDENT RPAREN
    {
      $ret = act.buildUserFunctionCall($functionName.ret);
      act.addToExpressionList($l.ret, act.buildInterval($it, $v.ret, $u));
      act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($l.ret));
    }

  |
    LPAREN p9 = expr RPAREN
    { $ret = $p9.ret; }
  |
//arithmetic_expr :
    <assoc=left> e = expr t=NOT // FACTORIAL
    { $ret = act.buildFactorial($t, $e.ret); }

    | es = sign_chain_expr
    { $ret = $es.ret; }

    | e1 = expr t=BITAND e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=BITOR e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=BITXOR e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | <assoc=right> t=BITNOT e = expr
    { $ret = act.buildUnaryExpression($t, $e.ret); }

    | e1 = expr t=STAR e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=DIVIDE e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=MOD e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=KW_DIV e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=ADD e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=SUBTRACT e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

// predicate:
  |
    e1 = expr t1=KW_IS t2=KW_NULL
    { $ret = act.buildIsNull($e1.ret, false, $t1, $t2); }

    | e1 = expr t1=KW_IS KW_NOT t2=KW_NULL
    { $ret = act.buildIsNull($e1.ret, true, $t1, $t2); }
  |
//  comparison_predicate :
    e1 = expr t=EQUAL e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=NOTEQUAL e2 = expr // single != token
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    // this rule breaks the factorial precedence in ANTLR
    // wondering if we really need this rule
//    | e1 = expr t1=NOT t2=EQUAL e2 = expr // separate ! and = tokens
//    { $ret = act.buildBinaryExpression($t1, $t2, $e1.ret, $e2.ret); }

    | e1 = expr t1=LESSTHAN t2=GREATERTHAN e2 = expr
    { $ret = act.buildBinaryExpression($t1, $t2, $e1.ret, $e2.ret); }

    | e1 = expr t1=LESSTHAN t2=EQUAL e2 = expr
    { $ret = act.buildBinaryExpression($t1, $t2, $e1.ret, $e2.ret); }

    | e1 = expr t1=GREATERTHAN t2=EQUAL e2 = expr
    { $ret = act.buildBinaryExpression($t1, $t2, $e1.ret, $e2.ret); }

    | e1 = expr t=LESSTHAN e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=GREATERTHAN e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t1=LESSTHAN t2=EQUAL t3=GREATERTHAN e2 = expr
    { $ret = act.buildBinaryExpression($t1, $t2, $t3, $e1.ret, $e2.ret); }

    | e1 = expr t1=KW_IS t2=KW_DISTINCT t3=KW_FROM e2 = expr
    { $ret = act.buildBinaryExpression($t1, $t2, $t3, $e1.ret, $e2.ret); }

    | e1 = expr t1=KW_IS t2=KW_NOT t3=KW_DISTINCT KW_FROM e2 = expr
    { $ret = act.buildBinaryExpression($t1, $t2, $t3, $e1.ret, $e2.ret); }
  |
//like_predicate :
    e1 = expr t=KW_LIKE e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=KW_ILIKE e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=KW_RLIKE e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=KW_REGEXP e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=KW_IREGEXP e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr tn=KW_NOT t=KW_LIKE e2 = expr
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

    | e1 = expr tn=KW_NOT t=KW_ILIKE e2 = expr
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

    | e1 = expr tn=KW_NOT t=KW_RLIKE e = expr
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

    | e1 = expr tn=KW_NOT t=KW_REGEXP e2 = expr
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

    | e1 = expr tn=KW_NOT t=KW_IREGEXP e2 = expr
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

//  between_predicate :
    e1 = expr t=KW_BETWEEN e2 = expr KW_AND e3 = expr
    { $ret = act.buildBetween($t, $e1.ret, $e2.ret, $e3.ret, false); }

    | e1 = expr t=KW_BETWEEN e2= expr KW_AND e3 = expr
    { $ret = act.buildBetween($t, $e1.ret, $e2.ret, $e3.ret, false); }

    | e1 = expr KW_NOT t=KW_BETWEEN e2 = expr KW_AND e3 = expr
    { $ret = act.buildBetween($t, $e1.ret, $e2.ret, $e3.ret, true); }

    | e1 = expr KW_NOT t=KW_BETWEEN e2 = expr KW_AND e3 = expr
    { $ret = act.buildBetween($t, $e1.ret, $e2.ret, $e3.ret, true); }

  |
//in_predicate :
    e = expr t=KW_IN LPAREN l = expr_list RPAREN
    { $ret = act.buildBinaryExpression($t, $e.ret, $l.ret); }

    | e = expr tn=KW_NOT t=KW_IN LPAREN l = expr_list RPAREN
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e.ret, $l.ret)); }

    | e = expr t=KW_IN s = subquery
    { $ret = act.buildBinaryExpression($t, $e.ret, $s.ret); }

    | e = expr tn=KW_NOT t=KW_IN s = subquery
    { $ret = act.buildUnaryExpression($tn, act.buildBinaryExpression($t, $e.ret, $s.ret)); }

  | p7 = exists_predicate
  { $ret = $p7.ret; }

  |
//  compund_predicate :
     <assoc=right> t=KW_NOT e = expr
    { $ret = act.buildUnaryExpression($t, $e.ret); }

    | <assoc=right> t=NOT e = expr
    { $ret = act.buildUnaryExpression($t, $e.ret); }

    | e1 = expr t=KW_AND e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

    | e1 = expr t=KW_OR e2 = expr
    { $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }


  ;

exists_predicate returns [UnaryExpression ret] @init { $ret = null; }
:
    t = KW_EXISTS s = subquery
    { $ret = act.buildUnaryExpression($t, $s.ret); }

;

non_pred_expr returns [Expression ret] @init { $ret = null; } :
//  e1 = sign_chain_expr // moved to expr
//  { $ret = $e1.ret; }

  l = literal
  { $ret = $l.ret; }

  | e2 = function_call_expr[false]
  { $ret = $e2.ret; }

  | e3 = analytic_expr
  { $ret = $e3.ret; }

  | t=KW_IF LPAREN exprs = expr_list RPAREN
  {
    $ret = act.buildInnerFunctionCall(act.buildId($t));
    act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($exprs.ret));
  }

  | t=KW_REPLACE LPAREN exprs = expr_list RPAREN
  {
    $ret = act.buildInnerFunctionCall(act.buildId($t));
    act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($exprs.ret));
  }

  | t=KW_TRUNCATE LPAREN exprs = expr_list RPAREN
  {
    $ret = act.buildInnerFunctionCall(act.buildId($t));
    act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($exprs.ret));
  }

  | c1 = cast_expr
  { $ret = $c1.ret; }

  | c2 = case_expr
  { $ret = $c2.ret; }

  | c3 = slot_ref
  { $ret = $c3.ret; }
//  | e4 = timestamp_arithmetic_expr // MOVED TO EXPR
//  | e5 = arithmetic_expr // // MOVED TO EXPR
//  | LPAREN e6 = non_pred_expr RPAREN // MOVED TO EXPR
  | s = subquery
  { $ret = $s.ret; }
  ;

function_call_expr [boolean analytic] returns [FunctionCall ret] @init { $ret = null; } :
  fn_name = function_name
  { if (analytic) {
      $ret = act.buildAnalyticFunctionCall($fn_name.ret);
    } else {
      $ret = act.buildUserFunctionCall($fn_name.ret);
    }
  }
  ( LPAREN RPAREN
  | LPAREN params = function_params RPAREN
    { act.functionCallSetParamList($ret, $params.ret); }

  | LPAREN u = ident_or_default KW_FROM t = expr RPAREN
    { act.functionCallSetParamList($ret, act.buildFunctionParams(act.buildFromExpr($u.ret, $t.ret))); }
  )
  ;

analytic_expr returns [AnalyticFunctionCall ret] @init { $ret = null; } :
  e = function_call_expr[true] KW_OVER LPAREN
  { $ret = (AnalyticFunctionCall) $e.ret; }
    (
      p = opt_partition_by_clause
      { act.analyticFunctionCallSetPartition($ret, $p.ret); }
    )?
    (
      o = opt_order_by_clause
      { act.analyticFunctionCallSetOrderBy($ret, $o.ret); }
    )?
    (
      w = opt_window_clause
      { act.analyticFunctionCallSetWindow($ret, $w.ret); }
    )?
    RPAREN
  ;

opt_partition_by_clause returns [ExpressionList ret] @init { $ret = null; } :
  KW_PARTITION KW_BY l = expr_list
  { $ret = $l.ret; }
  ;

opt_window_clause returns [AnalyticWindow ret] @init { $ret = null; } :
  t = window_type b = window_boundary
  { $ret = act.buildAnalyticWindow($t.tok, $b.ret, null); }

  | t = window_type KW_BETWEEN l = window_boundary KW_AND r = window_boundary
  { $ret = act.buildAnalyticWindow($t.tok, $l.ret, $r.ret); }

  ;

window_type returns [Token tok] @init { $tok = null; } :
  t=KW_ROWS
  { $tok = $t; }

  | t=KW_RANGE
  { $tok = $t; }

  ;

window_boundary returns [Boundary ret] @init { $ret = null; } :
  t1=KW_UNBOUNDED t2=KW_PRECEDING
  { $ret = act.buildBoundary($t1, $t2, null); }

  | t1=KW_UNBOUNDED t2=KW_FOLLOWING
  { $ret = act.buildBoundary($t1, $t2, null); }

  | t1=KW_CURRENT t2=KW_ROW
  { $ret = act.buildBoundary($t1, $t2, null); }

  | e = expr t1=KW_PRECEDING
  { $ret = act.buildBoundary($t1, null, $e.ret); }

  | e = expr t1=KW_FOLLOWING
  { $ret = act.buildBoundary($t1, null, $e.ret); }

  ;

numeric_literal returns [Literal ret] @init { $ret = null; } :
  l = INTEGER_LITERAL
  { $ret = act.buildLiteral($l); }

  | l = DECIMAL_LITERAL
  { $ret = act.buildLiteral($l); }
  ;

literal returns [Literal ret] @init { $ret = null; } :
  l1 = numeric_literal
  { $ret = $l1.ret; }

  | ls = STRING_LITERAL
  { $ret = act.buildLiteral($ls); }

  | lt=KW_TRUE
  { $ret = act.buildLiteral($lt); }

  | lf=KW_FALSE
  { $ret = act.buildLiteral($lf); }

  | ln=KW_NULL
  { $ret = act.buildLiteral($ln); }

//  | l3 = UNMATCHED_STRING_LITERAL e = expr
//  | l4 = NUMERIC_OVERFLOW
  ;

function_params returns [FunctionParams ret] @init { $ret = null; } :
  t=STAR
  { $ret = act.buildFunctionParams(act.buildAsterisk($t)); }

  | ta=KW_ALL t=STAR
  { $ret = act.buildFunctionParams($ta, null, act.buildAsterisk($t)); }

  | exprs = expr_list
  { $ret = act.buildFunctionParams($exprs.ret); }

  | ta=KW_ALL exprs = expr_list
  { $ret = act.buildFunctionParams($ta, null, $exprs.ret); }

  | td = KW_DISTINCT exprs = expr_list
  { $ret = act.buildFunctionParams($td, null, $exprs.ret); }

  | exprs = expr_list ti=KW_IGNORE tn=KW_NULLS
  { $ret = act.buildFunctionParams($ti, $tn, $exprs.ret); }
  ;

subquery returns [Query ret] @init { $ret = null; } :
  LPAREN s1 = subquery RPAREN
  { $ret = $s1.ret; }

  | LPAREN s2 = query_expr RPAREN
  { $ret = $s2.ret; }
  ;

slot_ref returns [Expression ret] @init { $ret = null; } :
  path = dotted_path
  { $ret = $path.ret; }
  ;

dotted_path returns [Expression ret] @init { $ret = null; } :
  ident = ident_or_default
  { $ret = $ident.ret; }

  (t = DOT ident = ident_or_default
   { $ret = act.buildBinaryExpression($t, $ret, $ident.ret); }
  )*
  ;

type_def returns [Type ret] @init { $ret = null; } :
  t = type
  { $ret = $t.ret; }
  ;

type returns [Type ret] @init { $ret = null; } :
  t=KW_TINYINT
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_SMALLINT
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_INT
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_BIGINT
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_BOOLEAN
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_FLOAT
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_DOUBLE
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_DATE
  { $ret = act.newDateType($t, null, null); }

  | t=KW_DATETIME
  { $ret = act.newDateType($t, null, null); }

  | t=KW_TIMESTAMP
  { $ret = act.newDateType($t, null, null); }

  | t=KW_STRING
  { $ret = act.newStringType($t, null); }

  | t=KW_VARCHAR LPAREN len = INTEGER_LITERAL RPAREN
  { $ret = act.newStringType($t, $len); }

  | t=KW_VARCHAR
  { $ret = act.newStringType($t, null); }

  | t=KW_BINARY
  { $ret = act.newStringType($t, null); }

  | t=KW_CHAR LPAREN len = INTEGER_LITERAL RPAREN
  { $ret = act.newStringType($t, $len); }

  | t=KW_DECIMAL LPAREN precision = INTEGER_LITERAL RPAREN
  { $ret = act.newNumericType($t, $precision, null, null); }

  | t=KW_DECIMAL LPAREN precision = INTEGER_LITERAL COMMA scale = INTEGER_LITERAL RPAREN
  { $ret = act.newNumericType($t, $precision, $scale, null); }

  | t=KW_DECIMAL
  { $ret = act.newNumericType($t, null, null, null); }

  | t=KW_ARRAY LESSTHAN value_type = type GREATERTHAN
  { $ret = act.newArrayType($t, $value_type.ret); }

  | t=KW_MAP LESSTHAN key_type = type COMMA value_type = type GREATERTHAN
  { $ret = act.newMapType($t, $type.ret, $value_type.ret); }

  | t=KW_STRUCT LESSTHAN fields = struct_field_def_list GREATERTHAN
  { $ret = act.newStructType($t, $fields.ret); }
  ;

struct_field_def returns [StructField ret] @init {$ret = null; } :
  name = ident_or_keyword COLON t = type comment = comment_val?
  { $ret = act.buildStructField($name.ret, $type.ret); }
  ;

struct_field_def_list returns [ArrayList<StructField> ret] @init {$ret = null; } :
  field_def = struct_field_def
  {
    $ret = new ArrayList<StructField>();
    $ret.add($field_def.ret);
  }
  ( COMMA field_def = struct_field_def
    { $ret.add($field_def.ret); }
  )*
  ;

ident_or_default returns [Expression ret] @init { $ret = null; } :
  name = IDENT
  { $ret = act.buildId($name); }

  | name = KW_DEFAULT
  { $ret = act.buildDefault($name); }

  ;

ident_or_keyword returns [Token ret] @init { $ret = null; }
@after { $ret = $r; }
:
  r = IDENT
  | r = KW_ADD
  | r = KW_AGGREGATE
  | r = KW_ALL
  | r = KW_ALTER
  | r = KW_ANALYTIC
  | r = KW_AND
  | r = KW_ANTI
  | r = KW_API_VERSION
  | r = KW_ARRAY
  | r = KW_AS
  | r = KW_ASC
  | r = KW_AVRO
  | r = KW_BETWEEN
  | r = KW_BIGINT
  | r = KW_BINARY
  | r = KW_BLOCKSIZE
  | r = KW_BOOLEAN
  | r = KW_BY
  | r = KW_CACHED
  | r = KW_CASCADE
  | r = KW_CASE
  | r = KW_CAST
  | r = KW_CHANGE
  | r = KW_CHAR
  | r = KW_CLASS
  | r = KW_CLOSE_FN
  | r = KW_COLUMN
  | r = KW_COLUMNS
  | r = KW_COMMENT
  | r = KW_COMPRESSION
  | r = KW_COMPUTE
  | r = KW_CREATE
  | r = KW_CROSS
  | r = KW_CURRENT
  | r = KW_DATA
  | r = KW_DATABASE
  | r = KW_DATABASES
  | r = KW_DATE
  | r = KW_DATETIME
  | r = KW_DECIMAL
  | r = KW_DEFAULT
  | r = KW_DELETE
  | r = KW_DELIMITED
  | r = KW_DESC
  | r = KW_DESCRIBE
  | r = KW_DISTINCT
  | r = KW_DIV
  | r = KW_DOUBLE
  | r = KW_DROP
  | r = KW_ELSE
  | r = KW_ENCODING
  | r = KW_END
  | r = KW_ESCAPED
  | r = KW_EXISTS
  | r = KW_EXPLAIN
  | r = KW_EXTENDED
  | r = KW_EXTERNAL
  | r = KW_FALSE
  | r = KW_FIELDS
  | r = KW_FILEFORMAT
  | r = KW_FILES
  | r = KW_FINALIZE_FN
  | r = KW_FIRST
  | r = KW_FLOAT
  | r = KW_FOLLOWING
  | r = KW_FOR
  | r = KW_FORMAT
  | r = KW_FORMATTED
  | r = KW_FROM
  | r = KW_FULL
  | r = KW_FUNCTION
  | r = KW_FUNCTIONS
  | r = KW_GRANT
  | r = KW_GROUP
  | r = KW_HAVING
  | r = KW_HASH
  | r = KW_IF
  | r = KW_IGNORE
  | r = KW_ILIKE
  | r = KW_IN
  | r = KW_INCREMENTAL
  | r = KW_INIT_FN
  | r = KW_INNER
  | r = KW_INPATH
  | r = KW_INSERT
  | r = KW_INT
  | r = KW_INTERMEDIATE
  | r = KW_INTERVAL
  | r = KW_INTO
  | r = KW_INVALIDATE
  | r = KW_IREGEXP
  | r = KW_IS
  | r = KW_JOIN
  | r = KW_KUDU
  | r = KW_LAST
  | r = KW_LEFT
  | r = KW_LIKE
  | r = KW_LIMIT
  | r = KW_LINES
  | r = KW_LOAD
  | r = KW_LOCATION
  | r = KW_MAP
  | r = KW_MERGE_FN
  | r = KW_METADATA
  | r = KW_NOT
  | r = KW_NULL
  | r = KW_NULLS
  | r = KW_OFFSET
  | r = KW_ON
  | r = KW_OR
  | r = KW_ORDER
  | r = KW_OUTER
  | r = KW_OVER
  | r = KW_OVERWRITE
  | r = KW_PARQUET
  | r = KW_PARQUETFILE
  | r = KW_PARTITION
  | r = KW_PARTITIONED
  | r = KW_PARTITIONS
  | r = KW_PRECEDING
  | r = KW_PREPARE_FN
  | r = KW_PRIMARY
  | r = KW_PRODUCED
  | r = KW_PURGE
  | r = KW_RANGE
  | r = KW_RCFILE
  | r = KW_RECOVER
  | r = KW_REFRESH
  | r = KW_REGEXP
  | r = KW_RENAME
  | r = KW_REPEATABLE
  | r = KW_REPLACE
  | r = KW_REPLICATION
  | r = KW_RESTRICT
  | r = KW_RETURNS
  | r = KW_REVOKE
  | r = KW_RIGHT
  | r = KW_RLIKE
  | r = KW_ROLE
  | r = KW_ROLES
  | r = KW_ROW
  | r = KW_ROWS
  | r = KW_SCHEMA
  | r = KW_SCHEMAS
  | r = KW_SELECT
  | r = KW_SEMI
  | r = KW_SEQUENCEFILE
  | r = KW_SERDEPROPERTIES
  | r = KW_SERIALIZE_FN
  | r = KW_SET
  | r = KW_SHOW
  | r = KW_SMALLINT
  | r = KW_SORT
  | r = KW_STORED
  | r = KW_STRAIGHT_JOIN
  | r = KW_STRING
  | r = KW_STRUCT
  | r = KW_SYMBOL
  | r = KW_TABLE
  | r = KW_TABLES
  | r = KW_TABLESAMPLE
  | r = KW_TBLPROPERTIES
  | r = KW_TERMINATED
  | r = KW_TEXTFILE
  | r = KW_THEN
  | r = KW_TIMESTAMP
  | r = KW_TINYINT
  | r = KW_TRUNCATE
  | r = KW_STATS
  | r = KW_TO
  | r = KW_TRUE
  | r = KW_UNBOUNDED
  | r = KW_UNCACHED
  | r = KW_UNION
  | r = KW_UPDATE
  | r = KW_UPDATE_FN
  | r = KW_UPSERT
  | r = KW_USE
  | r = KW_USING
  | r = KW_VALUES
  | r = KW_VARCHAR
  | r = KW_VIEW
  | r = KW_WHEN
  | r = KW_WHERE
  | r = KW_WITH
  ;
