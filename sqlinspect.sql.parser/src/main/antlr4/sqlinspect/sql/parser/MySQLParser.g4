parser grammar MySQLParser;

options {
    tokenVocab = MySQLLexer;
    language = Java;
    superClass = SQLParser;
}

@header {
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.builder.*;
import sqlinspect.sql.asg.clause.*;
import sqlinspect.sql.asg.common.*;
import sqlinspect.sql.asg.expr.*;
import sqlinspect.sql.asg.kinds.*;
import sqlinspect.sql.asg.schema.*;
import sqlinspect.sql.asg.statm.*;
import sqlinspect.sql.asg.type.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
}

@members {
    private static final Logger logger = LoggerFactory.getLogger(MySQLParser.class);

    private MySQLParserActions act = new MySQLParserActions(this);
  
    {
        setErrorHandler(new SQLErrorStrategy(MySQLLexer.SEMICOLON));
    }
    
    // a bit of a hack, see https://github.com/antlr/antlr4/issues/1133
    public MySQLParser(TokenStream input, ASG asg) {
    	super(input, asg);
    	_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
    }
        
    @Override
    public SQLDialect getDialect() {
    	return SQLDialect.MYSQL;
    }
    
    @Override
    protected SQLParserActions getActions() {
    	return act;
    }

    @Override
    public List<Statement> parseStatements() {
    	sqlErrorListener.clear();
        return start().ret;
    }
}

//------------------------------------------------------------------
// PARSER RULES
//------------------------------------------------------------------

start returns [List<Statement> ret] @init { $ret = new ArrayList<Statement>(); }
:
   (stmt =  statement
       { $ret.add($stmt.ret); }
   )* EOF
;

statement returns [Statement ret] @init {
    int lastErrorCount = 0;
    Statement s = null;
    $ret = null;
 }
:
    st1 = daStatement t = SEMICOLON
    {  s=$st1.ret;  }

    | st2 = ddStatement t = SEMICOLON
    {  s=$st2.ret;  }

    | st3 = dmStatement t = SEMICOLON
    {  s=$st3.ret;  }

    | notImplementedYet t = SEMICOLON
    | t = SEMICOLON
;

catch [RecognitionException re] {
    _localctx.exception = re;
    _errHandler.reportError(this, re);
    _errHandler.recover(this, re);
    setStmtError(true);
    setStmtErrorDetails(sqlErrorListener.getLast());
    logger.debug("LAST ERROR: " + (sqlErrorListener.getLast() == null ? "NULL" : sqlErrorListener.getLast().toString()));
} catch [ASGException e] {
    SQLErrorStrategy.consumeUntil(this, MySQLLexer.SEMICOLON);
    logger.error("ASGException while parsing statement: ", e);
    setStmtError(true);
    setStmtErrorDetails(new SQLError(0, 0, e.toString(), e));
} catch [Throwable tr] {
    SQLErrorStrategy.consumeUntil(this, MySQLLexer.SEMICOLON);
    logger.error("Unexpected exception while parsing statement: ", tr);
    setStmtError(true);
    setStmtErrorDetails(new SQLError(0, 0, tr.toString(), tr));
} finally {
	if (s == null && isStmtError()) {
		s = new ParserErrorBuilder(asg).result();
	}
	
    if (s != null) {
    	if (isStmtError()) {
    		SQLError err = getStmtErrorDetails();
    		s.setError(true);
    		if (err != null) {
    			s.setErrorMessage(err.getMessage());
    			s.setErrorLine(err.getLine());
    			s.setErrorCol(err.getCharPositionInLine());
    		}
    	}
        act.finishNode(s, $t);
    }
    $ret = s;
    setStmtError(false);
}

// -----------------------------------------------------------------------
// Not-yet-implemented Statements
// -----------------------------------------------------------------------

notImplementedYet
:
    lockTableStatement
    | unlockTableStatement
    | truncateStatement
    | replaceStatement
    | createIndexStatement
    | createUserStatement
    | commitStatement
    | renameStatement
    | prepareStatement
    | deallocateStatement
    | executeStatement
    | grantStatement
;

// --------------- LOCK  ---------------

lockTableStatement
:
    LOCK_SYM
    (
        t1 = TABLE_SYM
        | t2 = TABLES
    )
    (
        t = ~SEMICOLON
    )*
;

// --------------- UNLOCK  ---------------

unlockTableStatement
:
    UNLOCK_SYM
    (
        t1 = TABLE_SYM
        | t2 = TABLES
    )
    (
        t = ~SEMICOLON
    )*
;

// --------------- TRUNCATE  ---------------

truncateStatement
:
    TRUNCATE_SYM
    (
        t = ~SEMICOLON
    )*
;

// --------------- REPLACE  ---------------

replaceStatement
:
    REPLACE
    (
        t = ~SEMICOLON
    )*
;

// --------------- CREATE INDEX ---------------

createIndexStatement
:
    CREATE INDEX_SYM
    (
        t = ~SEMICOLON
    )*
;

// --------------- CREATE USER ---------------

createUserStatement
:
    CREATE USER
    (
        t = ~SEMICOLON
    )*
;

// --------------- COMMIT  ---------------

commitStatement
:
    COMMIT_SYM
    (
        t = ~SEMICOLON
    )*
;

// --------------- RENAME  ---------------

renameStatement
:
    RENAME
    (
        t = ~SEMICOLON
    )*
;

// --------------- PREPARE ---------------

prepareStatement
:
    PREPARE_SYM ident FROM prepareSrc
;

prepareSrc
:
    LIT_STRING
    | AT ident_or_text
;

// --------------- DEALLOCATE  ---------------

deallocateStatement
:
    (
        DEALLOCATE_SYM
        | DROP
    ) PREPARE_SYM ident
;

// --------------- EXECUTE  ---------------

executeStatement
:
    EXECUTE_SYM ident executeUsing?
;

// --------------- GRANT  ---------------

grantStatement
:
    GRANT
    (
        t = ~SEMICOLON
    )*
;

executeUsing
:
    USING executeVarList
;

executeVarList
:
    executeVarIdent
    (
        COMMA executeVarIdent
    )*
;

executeVarIdent
:
    AT ident_or_text
;

// -----------------------------------------------------------------------
// Database Administration Statements
// -----------------------------------------------------------------------

daStatement returns [Statement ret] @init { $ret = null; }
:
    s1 = set
    {  $ret = $s1.ret;  }

    | s2 = showStatement
    {  $ret = $s2.ret;  }

;

// --------------- SHOW  ---------------

set returns [Set ret] @init { $ret = null; }
:
    t = SET
    { $ret = act.buildSet($t); }

    vl = start_option_value_list
;

start_option_value_list
:
    option_value_no_option_type option_value_list_continued?
    | TRANSACTION_SYM transaction_characteristics
    | option_type start_option_value_list_following_option_type
    | PASSWORD equal password
    | PASSWORD equal PASSWORD LPAREN password RPAREN
    | PASSWORD FOR_SYM user equal password
    | PASSWORD FOR_SYM user equal PASSWORD LPAREN password RPAREN
;

start_option_value_list_following_option_type
:
    option_value_following_option_type option_value_list_continued?
    | TRANSACTION_SYM transaction_characteristics
;

option_value_list_continued
:
    COMMA option_value_list
;

// Repeating list of option values after first option value.

option_value_list
:
    option_value
    (
        COMMA option_value
    )*
;

option_value
:
    option_type option_value_following_option_type
    | option_value_no_option_type
;

option_type
:
    GLOBAL_SYM
    | LOCAL_SYM
    | SESSION_SYM
;

opt_var_type
:
    GLOBAL_SYM
    | LOCAL_SYM
    | SESSION_SYM
;

opt_var_ident_type
:
    GLOBAL_SYM DOT
    | LOCAL_SYM DOT
    | SESSION_SYM DOT
;

option_value_following_option_type
:
    internal_variable_name equal set_expr_or_default
;

option_value_no_option_type
:
    internal_variable_name equal set_expr_or_default
    | AT ident_or_text equal expr
    | AT AT opt_var_ident_type? internal_variable_name equal
    set_expr_or_default
    | charset old_or_new_charset_name_or_default
    | NAMES_SYM equal expr
    | NAMES_SYM charset_name_or_default opt_collate?
;

internal_variable_name returns [Expression ret] @init { $ret = null; }
:
    i = ident
    { $ret = $i.ret; }

    | i1 = ident t = DOT i2 = ident
    {  $ret = act.buildBinaryExpression($t, $i1.ret, $i2.ret); }

    | d = DEFAULT t = DOT i3 = ident
    {  $ret = act.buildBinaryExpression($t, act.buildDefault($d), $i3.ret); }

;

transaction_characteristics
:
    transaction_access_mode opt_isolation_level?
    | isolation_level opt_transaction_access_mode?
;

transaction_access_mode
:
    transaction_access_mode_types
;

opt_transaction_access_mode
:
    COMMA transaction_access_mode
;

isolation_level
:
    ISOLATION LEVEL_SYM isolation_types
;

opt_isolation_level
:
    COMMA isolation_level
;

transaction_access_mode_types
:
    READ_SYM ONLY_SYM
    | READ_SYM WRITE_SYM
;

isolation_types
:
    READ_SYM UNCOMMITTED_SYM
    | READ_SYM COMMITTED_SYM
    | REPEATABLE_SYM READ_SYM
    | SERIALIZABLE_SYM
;

password
:
    text_string
;

equal returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = EQ
    | t = SET_VAR
;

set_expr_or_default returns [Expression ret] @init { $ret = null; }
:
    e = expr
    { $ret = $e.ret; }

    | t = DEFAULT
    { $ret = act.buildDefault($t); }

    | ON
    | ALL
    | BINARY
;

showStatement returns [ShowCreateTable ret] @init { $ret = null; }
:
    s = SHOW
    (
        opt_var_type? VARIABLES wildAndWhere
        | c = CREATE t = TABLE_SYM e = expr
        { $ret = act.buildShowCreateTable($s, $e.ret); }

    )
;

wildAndWhere
:
    LIKE LIT_STRING
    | WHERE expr
;

// -----------------------------------------------------------------------

// Data Definition Statements
// -----------------------------------------------------------------------

ddStatement returns [Statement ret] @init { $ret = null; }
:
    s1 = create
    {  $ret = $s1.ret;  }

    | s2 = createDatabaseStatement
    {  $ret = $s2.ret;  }

    | s3 = dropTableStatement
    {  $ret = $s3.ret;  }

    | s4 = dropDatabaseStatement
    {  $ret = $s4.ret;  }

    | s5 = alterTableStatement
    {  $ret = $s5.ret;  }

    | s6 = use
    {  $ret = $s6.ret;  }

;

// --------------- CREATE TABLE ---------------

create returns [CreateTable ret] @init { Table tab = null;
    $ret = null; boolean ifNotExists = false; boolean isTemporary = false;
}
:
    t = CREATE
    (
        TEMPORARY
        { isTemporary = true; }

    )? TABLE_SYM
    (
        IF NOT_SYM EXISTS
        { ifNotExists = true; }

    )? tab = table_ident
    { $ret = act.buildCreateTable($t, $tab.ret, ifNotExists, false, isTemporary, null); }

    create2 [$ret.getSchemaTable()]
    //    | CREATE opt_unique INDEX_SYM ident key_alg ON table_ident LPAREN key_list RPAREN
    //    normal_key_options opt_index_lock_algorithm
    //    | CREATE fulltext INDEX_SYM ident init_key_options ON table_ident LPAREN
    //    key_list RPAREN fulltext_key_options opt_index_lock_algorithm
    //    | CREATE spatial INDEX_SYM ident init_key_options ON table_ident LPAREN
    //    key_list RPAREN spatial_key_options opt_index_lock_algorithm
    //    | CREATE DATABASE opt_if_not_exists ident opt_create_database_options
    //    | CREATE view_or_trigger_or_sp_or_event
    //    | CREATE USER opt_if_not_exists clear_privileges grant_list require_clause
    //    connect_options opt_account_lock_password_expire_options
    //    | CREATE LOGFILE_SYM GROUP_SYM logfile_group_info
    //    | CREATE TABLESPACE_SYM tablespace_info
    //    | CREATE SERVER_SYM ident_or_text FOREIGN DATA_SYM WRAPPER_SYM
    //    ident_or_text OPTIONS_SYM LPAREN server_options_list RPAREN

;

create2 [Table tab]
:
    LPAREN
    (
        create_field_list [$tab] r = RPAREN create_table_options? partitioning?
        create3 [$tab]?
        | partitioning? create_select RPAREN // TODO: union_opt?

    )
    | create_table_options? partitioning? create3 [$tab]?
    | LIKE other_table = table_ident
    { act.createTableLike($tab, $other_table.ret); }

    | LPAREN LIKE other_table = table_ident RPAREN
    { act.createTableLike($tab, $other_table.ret); }

;

create3 [Table tab]
:
    opt_duplicate? AS? create_select // TODO: opt_union_clause?

    | opt_duplicate? AS? LPAREN create_select /* TODO: union_opt? */ RPAREN
;

opt_duplicate
:
    REPLACE
    | IGNORE_SYM
;

opt_field_term
:
    COLUMNS field_term_list
;

field_term_list
:
    field_term_list field_term
    | field_term
;

field_term
:
    TERMINATED BY text_string
    | OPTIONALLY ENCLOSED BY text_string
    | ENCLOSED BY text_string
    | ESCAPED BY text_string
;

opt_line_term
:
    LINES line_term_list
;

line_term_list
:
    line_term_list line_term
    | line_term
;

line_term
:
    TERMINATED BY text_string
    | STARTING BY text_string
;

partitioning
:
    PARTITION_SYM partition
;

partition_entry
:
    PARTITION_SYM partition
;

partition
:
    BY part_type_def opt_num_parts? opt_sub_part? part_defs?
;

part_type_def
:
    opt_linear? KEY_SYM opt_key_algo? LPAREN part_field_list? RPAREN
    | opt_linear? HASH_SYM part_func
    | RANGE_SYM part_func
    | RANGE_SYM part_column_list
    | LIST_SYM part_func
    | LIST_SYM part_column_list
;

opt_linear
:
    LINEAR_SYM
;

opt_key_algo
:
    ALGORITHM_SYM EQ real_ulong_num
;

part_field_list
:
    part_field_item_list
;

part_field_item_list
:
    part_field_item
    (
        COMMA part_field_item
    )*
;

part_field_item
:
    ident
;

part_column_list
:
    COLUMNS LPAREN part_field_list? RPAREN
;

part_func
:
    LPAREN part_func_expr RPAREN
;

sub_part_func
:
    LPAREN part_func_expr RPAREN
;

opt_num_parts
:
    PARTITIONS_SYM real_ulong_num
;

opt_sub_part
:
    SUBPARTITION_SYM BY opt_linear HASH_SYM sub_part_func opt_num_subparts?
    | SUBPARTITION_SYM BY opt_linear KEY_SYM opt_key_algo LPAREN
    sub_part_field_list RPAREN opt_num_subparts?
;

sub_part_field_list
:
    sub_part_field_item
    (
        COMMA sub_part_field_item
    )*
;

sub_part_field_item
:
    ident
;

part_func_expr
:
    bit_expr //bit_expr

;

opt_num_subparts
:
    SUBPARTITIONS_SYM real_ulong_num
;

part_defs
:
    LPAREN part_def_list RPAREN
;

part_def_list
:
    part_definition
    (
        COMMA part_definition
    )*
;

part_definition
:
    PARTITION_SYM part_name opt_part_values opt_part_options?
    opt_sub_partition?
;

part_name
:
    ident
;

opt_part_values
:
    VALUES LESS_SYM THAN_SYM part_func_max
    | VALUES IN_SYM part_values_in
;

part_func_max
:
    MAX_VALUE_SYM
    | part_value_item
;

part_values_in
:
    part_value_item
    | LPAREN part_value_list RPAREN
;

part_value_list
:
    part_value_item
    | part_value_list COMMA part_value_item
;

part_value_item
:
    LPAREN part_value_item_list RPAREN
;

part_value_item_list
:
    part_value_expr_item
    (
        COMMA part_value_expr_item
    )*
;

part_value_expr_item
:
    MAX_VALUE_SYM
    | bit_expr //bit_expr

;

opt_sub_partition
:
    LPAREN sub_part_list RPAREN
;

sub_part_list
:
    sub_part_definition
    (
        COMMA sub_part_definition
    )*
;

sub_part_definition
:
    SUBPARTITION_SYM sub_name opt_part_options?
;

sub_name
:
    ident_or_text
;

opt_part_options
:
    opt_part_option_list
;

opt_part_option_list
:
    opt_part_option
    (
        opt_part_option
    )*
;

opt_part_option
:
    TABLESPACE_SYM equal? ident
    | STORAGE_SYM? ENGINE_SYM equal? storage_engines
    | NODEGROUP_SYM equal? real_ulong_num
    | MAX_ROWS equal? real_ulonglong_num
    | MIN_ROWS equal? real_ulonglong_num
    | DATA_SYM DIRECTORY_SYM equal? LIT_STRING
    | INDEX_SYM DIRECTORY_SYM equal? LIT_STRING
    | COMMENT_SYM equal? LIT_STRING
;

opt_table_options
:
    table_options
;

table_options
:
    table_option
    | table_option table_options
;

table_option
:
    TEMPORARY
;

opt_if_not_exists
:
    IF not EXISTS
;

create_table_options_space_separated
:
    create_table_option
    | create_table_option create_table_options_space_separated
;

create_table_options
:
    create_table_option
    (
        COMMA? create_table_option
    )*
;

create_table_option
:
    ENGINE_SYM equal? storage_engines
    | MAX_ROWS equal? ulonglong_num
    | MIN_ROWS equal? ulonglong_num
    | AVG_ROW_LENGTH equal? ulong_num
    | PASSWORD equal? LIT_STRING
    | COMMENT_SYM equal? LIT_STRING
    | COMPRESSION_SYM equal? LIT_STRING
    | ENCRYPTION_SYM equal? LIT_STRING
    | AUTO_INC equal? ulonglong_num
    | PACK_KEYS_SYM equal? ulong_num
    | PACK_KEYS_SYM equal? DEFAULT
    | STATS_AUTO_RECALC_SYM equal? ulong_num
    | STATS_AUTO_RECALC_SYM equal? DEFAULT
    | STATS_PERSISTENT_SYM equal? ulong_num
    | STATS_PERSISTENT_SYM equal? DEFAULT
    | STATS_SAMPLE_PAGES_SYM equal? ulong_num
    | STATS_SAMPLE_PAGES_SYM equal? DEFAULT
    | CHECKSUM_SYM equal? ulong_num
    | TABLE_CHECKSUM_SYM equal? ulong_num
    | DELAY_KEY_WRITE_SYM equal? ulong_num
    | ROW_FORMAT_SYM equal? row_types
    | UNION_SYM equal? LPAREN table_list? RPAREN
    | default_charset
    | default_collation
    | INSERT_METHOD equal? merge_insert_types
    | DATA_SYM DIRECTORY_SYM equal? LIT_STRING
    | INDEX_SYM DIRECTORY_SYM equal? LIT_STRING
    | TABLESPACE_SYM equal? ident
    | STORAGE_SYM DISK_SYM
    | STORAGE_SYM MEMORY_SYM
    | CONNECTION_SYM equal? LIT_STRING
    | KEY_BLOCK_SIZE equal? ulong_num
;

storage_engines
:
    ident_or_text
;

known_storage_engines
:
    ident_or_text
;

row_types
:
    DEFAULT
    | FIXED_SYM
    | DYNAMIC_SYM
    | COMPRESSED_SYM
    | REDUNDANT_SYM
    | COMPACT_SYM
;

merge_insert_types
:
    NO_SYM
    | FIRST_SYM
    | LAST_SYM
;

udf_type
:
    STRING_SYM
    | REAL
    | DECIMAL_SYM
    | INT_SYM
;

create_field_list [Table tab]
:
    field_list [$tab]
;

field_list [Table tab]
:
    field_list_item [$tab]
    (
        COMMA field_list_item [$tab]
    )*
;

field_list_item [Table tab]
:
    column_def [$tab]
    | key_def [$tab]
;

column_def [Table tab]
:
    field_spec [$tab] opt_check_constraint?
    | field_spec [$tab] references
;

key_def [Table tab]
:
    normal_key_type opt_ident? key_alg LPAREN key_list RPAREN normal_key_options
    | fulltext opt_key_or_index opt_ident? LPAREN key_list RPAREN
    fulltext_key_options
    | spatial opt_key_or_index opt_ident? LPAREN key_list RPAREN
    spatial_key_options
    | opt_constraint? constraint_key_type opt_ident? key_alg? LPAREN key_list
    RPAREN normal_key_options
    | opt_constraint? FOREIGN KEY_SYM opt_ident? LPAREN key_list RPAREN
    references
    | opt_constraint? check_constraint
;

opt_check_constraint
:
    check_constraint
;

check_constraint
:
    CHECK_SYM LPAREN expr RPAREN
;

opt_constraint
:
    constraint
;

constraint
:
    CONSTRAINT opt_ident?
;

field_spec [Table tab] returns [Column ret]
:
    col_name = field_ident fd = field_def
    { $ret = act.newColumn(tab, $col_name.ret, $fd.ret_type, $fd.ret_attrs); }

;

field_def returns [Type ret_type, List<ColumnAttribute> ret_attrs]
:
    t = type
    { $ret_type = $t.ret; }

    (
        a = attribute_list
        { $ret_attrs = $a.ret; }

    )?
    | t = type
    { $ret_type = $t.ret; }

    collate_explicit? opt_generated_always? AS LPAREN generated_column_func
    RPAREN opt_stored_attribute? opt_gcol_attribute_list?
;

opt_generated_always
:
    GENERATED ALWAYS_SYM
;

opt_gcol_attribute_list
:
    gcol_attribute_list
;

gcol_attribute_list
:
    gcol_attribute_list gcol_attribute
    | gcol_attribute
;

gcol_attribute
:
    UNIQUE_SYM
    | UNIQUE_SYM KEY_SYM
    | COMMENT_SYM text_string_sys
    | not NULL_SYM
    | NULL_SYM
    | opt_primary? KEY_SYM
;

opt_stored_attribute
:
    VIRTUAL_SYM
    | STORED_SYM
;

parse_gcol_expr
:
    PARSE_GCOL_EXPR_SYM LPAREN generated_column_func RPAREN
;

generated_column_func
:
    expr // expr

;

normal_key_type
:
    key_or_index
;

constraint_key_type
:
    PRIMARY_SYM KEY_SYM
    | UNIQUE_SYM opt_key_or_index?
;

key_or_index
:
    KEY_SYM
    | INDEX_SYM
;

opt_key_or_index
:
    key_or_index
;

table_list returns [ExpressionList ret] @init { $ret = null; }
:
    i1 = table_ident
    { $ret = act.buildExpressionList($i1.ret); }

    (
        COMMA i2 = table_ident
        { act.addToExpressionList($ret, $i2.ret); }

    )*
;

table_name
:
    table_ident
;

table_alias_ref_list returns [ExpressionList ret] @init { $ret = null; }
:
    ti = table_ident_opt_wild
    { $ret = act.buildExpressionList($ti.ret); }

    (
        COMMA ti = table_ident_opt_wild
        { act.addToExpressionList($ret, $ti.ret); }

    )*
;

fulltext
:
    FULLTEXT_SYM
;

spatial
:
    SPATIAL_SYM
;

key_alg
:
    key_using_alg
;

normal_key_options
:
    normal_key_opts?
;

fulltext_key_options
:
    fulltext_key_opts
;

spatial_key_options
:
    spatial_key_opts
;

normal_key_opts
:
    normal_key_opt normal_key_opt*
;

spatial_key_opts
:
    spatial_key_opt spatial_key_opt*
;

fulltext_key_opts
:
    fulltext_key_opt fulltext_key_opt*
;

key_using_alg
:
    USING btree_or_rtree
    | TYPE_SYM btree_or_rtree
;

all_key_opt
:
    KEY_BLOCK_SIZE equal? ulong_num
    | COMMENT_SYM LIT_STRING
;

normal_key_opt
:
    all_key_opt
    | key_using_alg
;

spatial_key_opt
:
    all_key_opt
;

fulltext_key_opt
:
    all_key_opt
    | WITH PARSER_SYM ident
;

btree_or_rtree
:
    BTREE_SYM
    | RTREE_SYM
    | HASH_SYM
;

key_list
:
    key_list COMMA key_part order_dir?
    | key_part order_dir?
;

key_part
:
    ident
    | ident LPAREN LIT_INTEGER RPAREN
;

ws_nweights
:
    LPAREN real_ulong_num RPAREN
;

ws_level_flag_desc
:
    ASC
    | DESC
;

ws_level_flag_reverse
:
    REVERSE_SYM
;

ws_level_flags
:
    ws_level_flag_desc
    | ws_level_flag_desc ws_level_flag_reverse
    | ws_level_flag_reverse
;

ws_level_number
:
    real_ulong_num
;

ws_level_list_item
:
    ws_level_number ws_level_flags?
;

ws_level_list
:
    ws_level_list_item
    | ws_level_list COMMA ws_level_list_item
;

ws_level_range
:
    ws_level_number '-' ws_level_number
;

ws_level_list_or_range
:
    ws_level_list
    | ws_level_range
;

opt_ws_levels
:
    LEVEL_SYM ws_level_list_or_range
;

opt_primary
:
    PRIMARY_SYM
;

references
:
    REFERENCES table_ident ref_list? opt_match_clause? opt_on_update_delete?
;

ref_list
:
    LPAREN ident
    (
        COMMA ident
    )* RPAREN
;

opt_match_clause
:
    t = MATCH FULL
    | t = MATCH PARTIAL
    | t = MATCH SIMPLE_SYM
;

opt_on_update_delete
:
    ON UPDATE_SYM delete_option
    (
        ON DELETE_SYM delete_option
    )?
    | ON DELETE_SYM delete_option
    (
        ON UPDATE_SYM delete_option
    )?
;

delete_option
:
    t = RESTRICT
    | t = CASCADE
    | t = SET NULL_SYM
    | t = NO_SYM ACTION
    | t = SET DEFAULT
;

type returns [Type ret] @init { $ret = null; }
:
    ti = int_type l = field_length? opts = field_options?
    { $ret = act.newNumericType($ti.ret, $l.ctx == null ? null : $l.ret, null, $opts.ctx == null ? null : $opts.ret); }

    | tr = real_type p = precision? opts = field_options?
    { $ret = act.newNumericType($tr.ret, $p.ctx == null ? null : $p.length, $p.ctx == null ? null : $p.dec, $opts.ctx == null ? null : $opts.ret); }

    | t = FLOAT_SYM fo = float_options? opts = field_options?
    { $ret = act.newNumericType($t, $fo.ctx == null ? null : $fo.prec, $fo.ctx == null ? null : $fo.scale, $opts.ctx == null ? null : $opts.ret); }

    | t = BIT_SYM l = field_length?
    { $ret = act.newNumericType($t, $l.ctx== null ? null : $l.ret, null, null); }

    |
    (
        t = BOOL_SYM
        | t = BOOLEAN_SYM
    )
    { $ret = act.newNumericType($t, null, null, null); }

    | t = CHAR_SYM l = field_length binary?
    { $ret = act.newStringType($t, $l.ret); }

    | tc = nchar l = field_length binary?
    { $ret = act.newStringType(StringTypeKind.NCHAR, $l.ret); }

    | tvc = varchar l = field_length binary?
    { $ret = act.newStringType(StringTypeKind.VARCHAR, $l.ret); }

    | tnvc = nvarchar l = field_length binary?
    { $ret = act.newStringType(StringTypeKind.NVARCHAR, $l.ret); }

    | t = BINARY l = field_length? binary?
    { $ret = act.newStringType($t, $l.ctx == null ? null : $l.ret); }

    | t = VARBINARY l = field_length binary?
    { $ret = act.newStringType($t, $l.ret); }

    | t = YEAR_SYM l = field_length? field_options?
    { $ret = act.newDateType($t, $l.ret, null); }

    | t = DATE_SYM
    { $ret = act.newDateType($t, null, null); }

    |
    (
        t = TIME_SYM
        | t = TIMESTAMP
        | t = DATETIME
    ) dp = type_datetime_precision?
    { $ret = act.newDateType($t, null, $dp.ctx == null ? null : $dp.ret); }

    |
    (
        t = TINYBLOB
        | t = MEDIUMBLOB
        | t = LONGBLOB
    )
    { $ret = act.newStringType($t, null); }

    | t = BLOB_SYM l = field_length?
    { $ret = act.newStringType($t, $l.ctx == null ? null : $l.ret); }

    | spatial_type
    | LONG_SYM VARBINARY // mapped to MEDIUMBLOB
    { $ret = act.newStringType(StringTypeKind.MEDIUMBLOB, null); }

    | t = LONG_SYM varchar binary?
    { $ret = act.newStringType($t, null); }

    |
    (
        t = TINYTEXT
        | t = MEDIUMTEXT
        | t = LONGTEXT
    ) binary?
    { $ret = act.newStringType($t, null); }

    | t = TEXT_SYM l = field_length? binary?
    { $ret = act.newStringType($t, $l.ctx == null ? null : $l.ret); }

    |
    (
        t = DECIMAL_SYM
        | t = FIXED_SYM
        | t = NUMERIC_SYM
    ) fo = float_options? opts = field_options?
    { $ret = act.newNumericType($t, $fo.ctx == null ? null : $fo.prec, $fo.ctx == null ? null : $fo.scale, $opts.ctx == null ? null : $opts.ret); }

    | t = ENUM LPAREN string_list RPAREN
    | t = LONG_SYM binary?
    { $ret = act.newStringType($t, null); }

    | t = SERIAL_SYM
    { $ret = act.newNumericType($t, null, null, null); }

;

field_length returns [ Token ret ]
:
    LPAREN l = LIT_INTEGER RPAREN
    { $ret = $l; }

;

binary
:
    (
        ASCII_SYM
        | BINARY ASCII_SYM
        | ASCII_SYM BINARY
    )
    |
    (
        UNICODE_SYM
        | BINARY UNICODE_SYM
        | UNICODE_SYM BINARY
    )
    | BYTE_SYM
    |
    (
        CHARSET
        | CHAR_SYM SET
    )
    (
        ident
        | LIT_STRING
    ) BINARY?
    | BINARY
    (
        CHARSET
        | CHAR_SYM SET
    )
    (
        ident
        | LIT_STRING
    )
    | BINARY
;

field_options returns [ List<Token> ret ]
@init { List<Token> opts = new ArrayList<Token>(); }
@after { $ret = opts; }
:
    (
        t = field_option
        { opts.add($t.ret); }

    )+
;

field_option returns [ Token ret ]
:
    s = SIGNED_SYM
    { $ret = $s; }

    | u = UNSIGNED
    { $ret = $u; }

    | z = ZEROFILL
    { $ret = $z; }

;

int_type returns [Token ret]
:
    i = INT_SYM
    { $ret = $i; }

    | ti = TINYINT
    { $ret = $ti; }

    | si = SMALLINT
    { $ret = $si; }

    | mi = MEDIUMINT
    { $ret = $mi; }

    | bi = BIGINT
    { $ret = $bi; }

;

real_type returns [Token ret]
:
    t = REAL
    { $ret = $t; }

    | t = DOUBLE_SYM
    { $ret = $t; }

    | t = DOUBLE_SYM PRECISION
    { $ret = $t; }

;

float_options returns [Token prec, Token scale]
:
    l = field_length
    { $prec = $l.ret; }

    | p = precision
    { $prec = $p.length;
      $scale = $p.dec;
    }

;
// length: total number of digits (the precision)
// dec: is the number of digits after the decimal point (the scale)

precision returns [Token length, Token dec]
:
    LPAREN f = LIT_INTEGER
    { $length = $f; }

    COMMA s = LIT_INTEGER
    { $dec = $s; }

    RPAREN
;

type_datetime_precision returns [Token ret]
:
    LPAREN t = LIT_INTEGER RPAREN
    { $ret = $t; }

;

func_datetime_precision returns [FunctionParams ret] @init { $ret = null; }
:
    LPAREN
    (
        l = num_lit
        { $ret = act.buildFunctionParams($l.ret); }

    )? RPAREN
;

spatial_type
:
    t = GEOMETRY_SYM
    | t = GEOMETRYCOLLECTION
    | t = POINT_SYM
    | t = MULTIPOINT
    | t = LINESTRING
    | t = MULTILINESTRING
    | t = POLYGON
    | t = MULTIPOLYGON
;

nchar

:
    NCHAR_SYM
    | NATIONAL_SYM CHAR_SYM
;

varchar
:
    CHAR_SYM VARYING
    | VARCHAR
;

nvarchar
:
    NVARCHAR_SYM
    | NATIONAL_SYM VARCHAR
    | NCHAR_SYM VARCHAR
    | NATIONAL_SYM CHAR_SYM VARYING
    | NCHAR_SYM VARYING
;

filterStringList
:
    LPAREN RPAREN
    | LPAREN LIT_STRING
    (
        COMMA LIT_STRING
    )* RPAREN
;

opt_ident
:
    field_ident
;

opt_component returns [Token ret_tok, Expression ret]
:
    t = DOT
    { $ret_tok = $t; }

    i = ident
    { $ret = $i.ret; }

;

string_list
:
    text_string
    (
        COMMA text_string
    )*
;

now returns [InnerFunctionCall ret] @init { $ret = null;  }
:
    n = NOW_SYM f = func_datetime_precision?
    { $ret = act.buildInnerFunctionCall($n, $f.ctx == null ? null : $f.ret); }

;

attribute_list returns [List<ColumnAttribute> ret]
@init { $ret = new ArrayList<ColumnAttribute>(); }
:
    (
        attr = attribute
        { $ret.add($attr.ret); }

    )+
;

attribute returns [ColumnAttribute ret] @init { $ret = null; }
:
    tn = not t1 = NULL_SYM
    { $ret = act.buildColumnAttribute($t1, $tn.ret); }

    | t1 = NULL_SYM
    { $ret = act.buildColumnAttribute($t1); }

    | t1 = DEFAULT
    (
        ex1 = now
        { $ret = act.buildColumnAttribute($t1, $ex1.ret); }

        | ex2 = signed_literal
        { $ret = act.buildColumnAttribute($t1, $ex2.ret); }

    )
    | ON t1 = UPDATE_SYM now // TODO: handle in AST

    | t1 = AUTO_INC
    { $ret = act.buildColumnAttribute($t1); }

    | t1 = SERIAL_SYM DEFAULT VALUE_SYM
    { $ret = act.buildColumnAttribute($t1); }

    | t2 = PRIMARY_SYM? t1 = KEY_SYM
    { $ret = act.buildColumnAttribute($t1, $t2); }

    | t1 = UNIQUE_SYM t2 = KEY_SYM
    { $ret = act.buildColumnAttribute($t1, $t2); }

    | t1 = UNIQUE_SYM
    { $ret = act.buildColumnAttribute($t1); }

    | t1 = COMMENT_SYM l = LIT_STRING // TODO: handle in AST
    { $ret = act.buildColumnAttribute($t1, act.buildLiteral($l)); }

    | t1 = COLLATE_SYM // TODO: handle in AST

    (
        ident
        | LIT_STRING
    )
    | t1 = COLUMN_FORMAT_SYM // TODO: handle in AST

    (
        t2 = DEFAULT
        | t2 = FIXED_SYM
        | t2 = DYNAMIC_SYM
    )
    | t1 = STORAGE_SYM // TODO: handle in AST

    (
        t2 = DEFAULT
        | t2 = DISK_SYM
        | t2 = MEMORY_SYM
    )
;

// --------------- CREATE DATABASE ---------------

createDatabaseStatement returns [CreateDatabase ret]
@init { $ret = null; boolean ifNotExists = false;  }
:
    t = CREATE DATABASE
    (
        IF NOT_SYM EXISTS
        { ifNotExists = true; }

    )? i = ident
    { $ret = act.buildCreateDatabase($t, $i.ret, ifNotExists); }

    create_database_options?
;

create_database_options
:
    create_database_option+
;

create_database_option
:
    default_collation
    | default_charset
;

default_collation
:
    DEFAULT? COLLATE_SYM equal? collation_name_or_default
;

collate_explicit
:
    COLLATE_SYM collation_name
;

collation_name_or_default
:
    collation_name
    | DEFAULT
;

opt_load_data_charset
:
    charset charset_name_or_default
;

old_or_new_charset_name
:
    ident_or_text
    | BINARY
;

old_or_new_charset_name_or_default
:
    old_or_new_charset_name
    | DEFAULT
;

collation_name
:
    ident_or_text
;

opt_collate
:
    COLLATE_SYM collation_name_or_default
;

default_charset
:
    DEFAULT? charset equal? charset_name_or_default
;

charset_name_or_default
:
    charset_name
    | DEFAULT
;

charset
:
    CHAR_SYM SET
    | CHARSET
;

charset_name
:
    ident_or_text
    | BINARY
;

// --------------- DROP TABLE ---------------

dropTableStatement returns [DropTable ret]
@init { $ret = null; boolean isTemporary = false, ifExists = false; }
:
    d = DROP
    (
        temp = TEMPORARY
        { isTemporary = true; }

    )?
    (
        t = TABLE_SYM
        | t = TABLES
    )
    (
        IF e = EXISTS
        { ifExists = true; }

    )? l = table_list
    (
        r = RESTRICT
        | c = CASCADE
    )?
    { $ret = act.buildDropTable($d, $l.ret, ifExists, isTemporary, false); }

;

// --------------- DROP DATABASE ---------------

dropDatabaseStatement returns [DropDatabase ret]
@init { $ret = null; boolean ifExists = false; }
:
    d = DROP db = DATABASE
    (
        IF e = EXISTS
        { ifExists = true; }

    )? i = ident
    { $ret = act.buildDropDatabase($d, $i.ret, ifExists); }

;

// --------------- ALTER TABLE  ---------------

alterTableStatement returns [AlterTable ret] @init { $ret = null; }
:
    a = ALTER t = TABLE_SYM i = ident
    { act.buildAlterTable($a, $i.ret); }

    (
        t = ~SEMICOLON
    )*
;

// --------------- USE  ---------------

use returns [Use ret] @init { UseBuilder b = new UseBuilder(asg);  }
@after { $ret = b.result(); }
:
    u = USE_SYM i = ident
    { act.buildUse($u, $i.ret); }

;

// -----------------------------------------------------------------------
// Data Modification Statements
// -----------------------------------------------------------------------

dmStatement returns [Statement ret] @init { $ret = null; }
:
    s1 = select
    {  $ret = $s1.ret;  }

    | s2 = insert_stmt
    {  $ret = $s2.ret;  }

    | s3 = update_stmt
    {  $ret = $s3.ret;  }

    | s4 = delete_stmt
    {  $ret = $s4.ret;  }

;

// --------------- INSERT---------------

insert_stmt returns [Insert ret] @init { $ret = null; }
:
    i = INSERT insert_lock_option? IGNORE_SYM? INTO? t = table_ident
    { $ret = act.buildInsert($i, false, $t.ret, null); }

    use_partition?
    (
        insert_from_constructor [$ret]
        | SET ul = update_list
        { act.insertSetSetList($ret, $ul.ret); }

        | insert_from_subquery [$ret]
    ) opt_insert_update_list?
;

insert_lock_option
:
    l = LOW_PRIORITY
    | d = DELAYED_SYM
    | h = HIGH_PRIORITY
;

insert_from_constructor [ Insert insert ]
:
    vl = insert_values
    { act.insertSetQuery($insert, $vl.ret); }

    | LPAREN RPAREN vl = insert_values
    { act.insertSetQuery($insert, $vl.ret); }

    | LPAREN cols = fields RPAREN
    { act.insertSetColumnList($insert, $cols.ret); }

    vl = insert_values
    { act.insertSetQuery($insert, $vl.ret); }

;

insert_from_subquery [ Insert insert ]
:
    q = insert_query_expression
    { act.insertSetQuery($insert, $q.ret); }

    | LPAREN RPAREN q = insert_query_expression
    { act.insertSetQuery($insert, $q.ret); }

    | LPAREN cols = fields
    { act.insertSetColumnList($insert, $cols.ret); }

    RPAREN q = insert_query_expression
    { act.insertSetQuery($insert, $q.ret); }

;

insert_values returns [ Values ret ] @init { $ret = null; }
:
    t = value_or_values vl = values_list
    { $ret = act.buildValues($t.ret, $vl.ret); }

;

insert_query_expression returns [Query ret] @init { $ret = null; }
:
    s = create_select
    { $ret = $s.ret; }

    (
        un = opt_union_clause [$ret]
        { $ret = $un.ret; }

    )?
    | LPAREN s = create_select RPAREN union_opt [$s.ret]?
    { $ret = $s.ret; }

;

value_or_values returns [ Token ret ]
:
    t = VALUE_SYM
    { $ret = $t; }

    | t = VALUES
    { $ret = $t; }

;

values_list returns [ ExpressionList ret] @init { $ret = null; }
:
    e = row_value
    { act.buildExpressionList($e.ret); }

    (
        COMMA e = row_value
        { act.addToExpressionList($ret, $e.ret); }

    )*
;

row_value returns [ Expression ret] @init { $ret = null; }
:
    LPAREN
    (
        v = values
        { $ret = $v.ret; }

    )? RPAREN
;

values returns [ ExpressionList ret] @init { $ret = null; }
:
    e = expr_or_default
    { $ret = act.buildExpressionList($e.ret); }

    (
        COMMA e = expr_or_default
        { act.addToExpressionList($ret, $e.ret); }

    )*
;

expr_or_default returns [Expression ret] @init { $ret = null; }
:
    e = expr
    { $ret = $e.ret; }

    | d = DEFAULT
    { $ret = act.buildDefault($d); }

;

opt_insert_update_list
:
    ON DUPLICATE_SYM KEY_SYM UPDATE_SYM update_list
;

use_partition
:
    PARTITION_SYM LPAREN using_list RPAREN
;

using_list returns [ExpressionList ret] @init { $ret = null; }
:
    i = ident
    { $ret = act.buildExpressionList($i.ret); }

    (
        COMMA i = ident
        { act.addToExpressionList($ret, $i.ret); }

    )*
;

fields returns [ExpressionList ret]
@init { ExpressionListBuilder b = new ExpressionListBuilder(asg); }
@after { $ret = b.result(); }
:
    i1 = insert_ident
    {  b.addExpression($i1.ret); }

    (
        COMMA i2 = insert_ident
        {  b.addExpression($i2.ret); }

    )*
;

// --------------- UPDATE ---------------

update_stmt returns [Update ret] @init { $ret = null; }
:
    t = UPDATE_SYM LOW_PRIORITY? IGNORE_SYM? tablist = join_table_list SET vals
    = update_list
    { $ret = act.buildUpdate($t, $tablist.ret, $vals.ret); }

    (
        w = where_clause
        { act.updateSetWhere($ret, $w.ret); }

    )?
    (
        o = order_clause
        { act.updateSetOrderBy($ret, $o.ret); }

    )?
    (
        l = opt_simple_limit
        { act.updateSetLimit($ret, $l.ret); }

    )?
;

update_list returns [ExpressionList ret] @init { $ret = null; }
:
    e1 = update_elem
    { $ret = act.buildExpressionList($e1.ret); }

    (
        COMMA e2 = update_elem
        { act.addToExpressionList($ret, $e2.ret); }

    )*
;

update_elem returns [BinaryExpression ret] @init { $ret = null; }
:
    i = simple_ident_nospvar t = equal e = expr_or_default
    { $ret = act.buildBinaryExpression($t.ret, $i.ret, $e.ret); }

;

limit_options returns [Expression limit, Expression offset] @init { $limit = null; $offset = null; }
:
    lo1=limit_option
    { $limit = $lo1.ret; }
    
    | lo1=limit_option COMMA lo2=limit_option
    { $limit = $lo2.ret; $offset=$lo1.ret; }
    
    | lo1=limit_option OFFSET_SYM lo2=limit_option
    { $limit = $lo1.ret; $offset=$lo2.ret; }
;

limit_option returns [Expression ret]
:
    i = ident
    { $ret = $i.ret; }

    | param_marker
    | n = num_lit
    { $ret = $n.ret; }

;

opt_simple_limit returns [Limit ret] @init { $ret = null; }
:
    l = LIMIT lo = limit_option
    { $ret = act.buildLimit($l, $lo.ret, null); }

;

// --------------- DELETE ---------------

delete_stmt returns [Delete ret] @init { $ret = null; }
:
    dt = DELETE_SYM opt_delete_options? FROM tab = table_ident
    { $ret = act.buildDelete($dt, $tab.ret); }

    use_partition?
    (
        where = where_clause
        { act.deleteSetWhere($ret, $where.ret); }

    )?
    (
        o = order_clause
        { act.deleteSetOrder($ret, $o.ret); }

    )?
    (
        limit = opt_simple_limit
        { act.deleteSetLimit($ret, $limit.ret); }

    )?
    | dt = DELETE_SYM opt_delete_options? tab_ref_list = table_alias_ref_list
    FROM join_tab_list = join_table_list
    { $ret = act.buildDelete($dt, $join_tab_list.ret);
            act.deleteSetTableReferenceList($ret, $tab_ref_list.ret); }

    (
        where = where_clause
        { act.deleteSetWhere($ret, $where.ret); }

    )?
    | dt = DELETE_SYM opt_delete_options? FROM tab_ref_list =
    table_alias_ref_list
    { $ret = act.buildDelete($dt, $tab_ref_list.ret); }

    USING using = join_table_list
    { act.deleteSetUsing($ret, $using.ret); }

    (
        where = where_clause
        { act.deleteSetWhere($ret, $where.ret); }

    )?
;

opt_delete_options
:
    opt_delete_option+
;

opt_delete_option
:
    QUICK
    | LOW_PRIORITY
    | IGNORE_SYM
;

// --------------- SELECT ---------------

create_select returns [SelectExpression ret] @init { $ret = null; }
:
    s = SELECT_SYM opts = select_options? sl = select_item_list
    { $ret = act.buildSelectExpression($s, null, $sl.ret);
      if ($opts.ctx != null) {
        act.selectExpressionOptions($ret, $opts.ret);
      }
    }

    table_expression [$ret]
;

select returns [Select ret]
:
    sel = select_init
    { $ret = act.buildSelect($sel.ret); }

;

//select_init returns [SelectExpression ret] @init { $ret = null; }
//:
//    st = SELECT_SYM sel = select_part2 [$st] opt_union_clause [$sel.ret]?
//    { $ret = $sel.ret; }
//
//    | LPAREN s = select_paren RPAREN union_opt [$s.ret]?
//    { $ret = $s.ret; }
//
//;

select_init returns [Query ret] @init { $ret = null; }
:
    st = SELECT_SYM sel = select_part2 [$st]
    { $ret = $sel.ret; }

    (
        union = opt_union_clause [$sel.ret]
        { $ret = $union.ret; }

    )?
    | LPAREN s = select_paren RPAREN union_opt [$s.ret]?
    { $ret = $s.ret; }

;


select_paren returns [SelectExpression ret] @init { $ret = null; }
:
    st = SELECT_SYM sel = select_part2 [$st]
    { $ret = $sel.ret; }

    | LPAREN sel_par = select_paren RPAREN
    { $ret = $sel_par.ret; }

;

select_paren_derived returns [SelectExpression ret] @init { $ret = null; }
:
    st = SELECT_SYM sel = select_part2_derived [$st]
    { $ret = $sel.ret; }

    table_expression [$ret]
    | LPAREN sel_par = select_paren_derived
    { $ret = $sel_par.ret; }

    RPAREN
;

select_part2 [Token st] returns [SelectExpression ret] @init { $ret = null; }
:
    opts = select_options_and_item_list
    {
      $ret = act.buildSelectExpression($st, null, $opts.ret_list);
      if ($opts.ret_options != null) {
        act.selectExpressionOptions($ret, $opts.ret_options);
      }
    }

    (
        i = into
        { act.selectExpressionSetInto($ret, $i.ret); }

    )?

    (
        from = from_clause
        { act.selectExpressionSetFrom($ret, $from.ret); }
    )?

    (
        where = where_clause
        { act.selectExpressionSetWhere($ret, $where.ret); }

    )?
    (
        group = group_clause
        { act.selectExpressionSetGroupBy($ret, $group.ret); }

    )?
    (
        having = having_clause
        { act.selectExpressionSetHaving($ret, $having.ret); }

    )?
    (
        order = order_clause
        { act.querySetOrderBy($ret, $order.ret); }

    )?
    (
        limit = limit_clause
        { act.querySetLimit($ret, $limit.ret); }

    )? opt_procedure_analyse_clause?
    (
        into
        { act.selectExpressionSetInto($ret, $into.ret); }

    )? opt_select_lock_type?
;

select_options_and_item_list returns
[List<Token> ret_options, ExpressionList ret_list]
@init { $ret_options = null; $ret_list = null; }
:
    (
        opts = select_options
        { $ret_options = $opts.ret; }

    )? list = select_item_list
    { $ret_list = $list.ret; }

;

opt_select_lock_type
:
    FOR_SYM UPDATE_SYM
    | LOCK_SYM IN_SYM SHARE_SYM MODE_SYM
;

select_item_list returns [ExpressionList ret] @init { $ret = null; }
:
    i1 = select_item
    { $ret = act.buildExpressionList($i1.ret); }

    (
        COMMA i2 = select_item
        { act.addToExpressionList($ret, $i2.ret); }

    )*
;

select_item returns [Expression ret] @init { $ret = null; }
:
    a = ASTERISK // this is originally not here in the grammar, but it is more general this way
    { $ret = act.buildAsterisk($a); }

    | t = table_wild
    { $ret = $t.ret; }

    | e = expr
    { $ret = $e.ret; }

    (
        al = select_alias [$e.ret]
        { $ret = $al.ret; }

    )?
;

select_alias [Expression left] returns [Alias ret] @init { $ret = null; }
:
    t1 = AS i1 = ident
    { $ret = act.buildAlias($left, $i1.ret, $t1); }

    | i2 = ident
    { $ret = act.buildAlias($left, $i2.ret, null); }

    | t3 = AS i3 = text_string_sys
    { $ret = act.buildAlias($left, $i3.ret, $t3); }

    | i4 = text_string_sys
    { $ret = act.buildAlias($left, $i4.ret, null); }

;

into returns [ExpressionList ret] @init { $ret = null; }
:
    INTO id = into_destination
    { $ret = $id.ret; }

;

into_destination returns [ExpressionList ret] @init { $ret = null; }
:
    OUTFILE text_string_filesystem opt_load_data_charset? opt_field_term?
    opt_line_term?
    | DUMPFILE text_string_filesystem
    | sl = select_var_list
    { $ret = $sl.ret; }

;

select_var_list returns [ExpressionList ret] @init { $ret = null; }
:
    i1 = select_var_ident
    { $ret = act.buildExpressionList($i1.ret); }

    (
        COMMA i2 = select_var_ident
        { act.addToExpressionList($ret, $i2.ret); }

    )*
;

select_var_ident returns [Expression ret] @init { $ret = null; }
:
    AT i = ident_or_text
    {  $ret = $i.ret; }

    | i = ident_or_text
    {  $ret = $i.ret; }

;

table_expression [SelectExpression sel]
:
    (
        from = from_clause
        { act.selectExpressionSetFrom($sel, $from.ret); }

    )?
    (
        where = where_clause
        { act.selectExpressionSetWhere($sel, $where.ret); }

    )?
    (
        group = group_clause
        { act.selectExpressionSetGroupBy($sel, $group.ret); }

    )?
    (
        having = having_clause
        { act.selectExpressionSetHaving($sel, $having.ret); }

    )?
    (
        order = order_clause
        { act.querySetOrderBy($sel, $order.ret); }

    )?
    (
        limit = limit_clause
        { act.querySetLimit($sel, $limit.ret); }

    )? opt_procedure_analyse_clause? opt_select_lock_type?
;

from_clause returns [Expression ret] @init { $ret = null; }
:
    FROM sl = table_reference_list
    {  $ret = $sl.ret; }

;

table_reference_list returns [Expression ret] @init { $ret = null; }
:
    t = join_table_list
    {  $ret=$t.ret; }

    | d = DUAL_SYM
    {  $ret = act.buildId($d); }

;

select_options returns [ List<Token> ret]
@init { $ret = new ArrayList<Token>(); }
:
    list = select_option_list
    { $ret = $list.ret; }

;

select_option_list returns [ List<Token> ret]
@init { $ret = new ArrayList<Token>(); }
:
    (
        o = select_option
        { $ret.add($o.ret); }

    )+
;

select_option returns [Token ret] @init { $ret = null; }
:
    so = query_spec_option
    { $ret = $so.ret; }

    | t = SQL_NO_CACHE_SYM
    { $ret = $t; }

    | t = SQL_CACHE_SYM
    { $ret = $t; }

;

join_table_list returns [Expression ret] @init { $ret = null; }
:
    s1 = table_ref
    { $ret=$s1.ret; }

    (
        t = COMMA s2 = table_ref
        { $ret = act.buildJoin($ret, $s2.ret, $t); }

    )*

;

when_list returns [List<WhenClause> ret]
@init { $ret = new ArrayList<WhenClause>(); }
:
    t = WHEN_SYM e1 = expr THEN_SYM e2 = expr
    { $ret.add(act.buildWhenClause($t, $e1.ret, $e2.ret));}

    (
        t = WHEN_SYM e1 = expr THEN_SYM e2 = expr
        { $ret.add(act.buildWhenClause($t, $e1.ret, $e2.ret));}

    )*
;

table_ref returns [Expression ret] @init { $ret = null; }
:
    tf = table_factor
    { $ret = $tf.ret; }

    | tr = table_ref
    { $ret = $tr.ret; }

    (
        i = inner_join_table[$ret]
        { $ret = $i.ret; }

        | s = straight_join_table[$ret]
        { $ret = $s.ret; }

        | n = natural_join_table[$ret]
        { $ret = $n.ret;}

        | o = outer_join_table[$ret]
        { $ret = $o.ret;}

    )
;

table_factor returns [Expression ret] @init { $ret = null; }
:
    i = table_ident
    { $ret = $i.ret; }

    use_partition?
    (
        a = opt_table_alias
        { $ret = act.buildAlias($i.ret, $a.ret_expr, $a.ret_tok); }

    )? opt_key_definition
    | t = SELECT_SYM opts = select_options? list = select_item_list
    { $ret = act.buildSelectExpression($t, null, $list.ret);
      if ($opts.ctx != null) {
        act.selectExpressionOptions((SelectExpression)$ret, $opts.ret);
      }
    }

    table_expression [(SelectExpression)$ret]
    | LPAREN sel = select_derived_union
    { $ret = $sel.ret; }

    RPAREN
    (
        a = opt_table_alias
        { $ret = act.buildAlias($ret, $a.ret_expr, $a.ret_tok); }

    )?
;

// TODO: MySQL parser does a strange thing here to handle the union in from clauses
// it generates a too general language and accepts bad syntax like 'table UNION SELECT 1'
// we can try to handle this with semantic predicates
select_derived_union returns [Expression ret]
:
//    sel = select_derived
    sel = join_table_list
    { $ret = $sel.ret; }
    //    (
    //        { $sel.ret instanceOf Query }?
    //
    //        opt_union_order_or_limit [$sel.ret]
    //        { $ret = $sel.ret; }
    //
    //    )?
    //    | selu = select_derived_union
    //    { $selU.ret instanceOf Query }?
    //
    //    t = UNION_SYM op = union_option right = query_specification
    //    { $ret = act.buildBinaryQuery($op.ret, null, $selu.ret, $right.ret); }

;

select_part2_derived [Token st] returns [SelectExpression ret]
@init { $ret = null; }
:
    opts = opt_query_spec_options list = select_item_list
    { $ret = act.buildSelectExpression($st, null, $list.ret);
      act.selectExpressionOptions($ret, $opts.ret);
    }

    | list = select_item_list
    { $ret = act.buildSelectExpression($st, null, $list.ret); }

;

//select_derived returns [Expression ret]
//:
//    sel = derived_table_list
//    { $ret = $sel.ret; }
//
//;

index_hint_clause
:
    FOR_SYM JOIN_SYM
    | FOR_SYM ORDER_SYM BY
    | FOR_SYM GROUP_SYM BY
;

index_hint_type
:
    FORCE_SYM
    | IGNORE_SYM
;

index_hint_definition
:
    index_hint_type key_or_index index_hint_clause LPAREN key_usage_list RPAREN
    | USE_SYM key_or_index index_hint_clause LPAREN key_usage_list? RPAREN
;

index_hints_list
:
    index_hint_definition
    | index_hints_list index_hint_definition
;

opt_index_hints_list
:
    index_hints_list?
;

opt_key_definition
:
    opt_index_hints_list
;

key_usage_element
:
    ident
    | PRIMARY_SYM
;

key_usage_list
:
    key_usage_element
    (
        COMMA key_usage_element
    )*
;

tableAlias [Expression left] returns [Alias ret] @init { $ret = null; }
:
    (
        t = AS
        | t = EQ
    )? i = ident
    {  $ret = act.buildAlias($left, $i.ret, $t); }

;

// moved under table_ref to avoid left recursion
// join_table returns [Expression ret] @init { $ret = null; }
//:
//    t = table_ref
//    {  $ret=$t.ret; }
//
//    (
//        i = inner_join_table [$ret]
//        {  $ret=$i.ret; }
//
//        | s = straight_join_table [$ret]
//        {  $ret=$s.ret;}
//
//        | n = natural_join_table [$ret]
//        {  $ret=$n.ret;}
//
//        | o = outer_join_table [$ret]
//        {  $ret=$o.ret;}
//
//    )
//;

normal_join returns [Token ret_tok1, Token ret_tok2]
@init { $ret_tok1 = null; $ret_tok2 = null; }
:
    tok1 = JOIN_SYM
    { $ret_tok1 = $tok1; }

    | tok1 = INNER_SYM tok2 = JOIN_SYM
    { $ret_tok1 = $tok1; $ret_tok2 = $tok2; }

    | tok1 = CROSS tok2 = JOIN_SYM
    { $ret_tok1 = $tok1; $ret_tok2 = $tok2; }

;

natural_join returns [Token ret_tok1, Token ret_tok2, Token ret_tok3]
@init { $ret_tok1 = null; $ret_tok2 = null; $ret_tok3 = null; }
:
    tok1 = NATURAL
    { $ret_tok1 = $tok1; }

    (
        tok2 = LEFT
        { $ret_tok2 = $tok2; }

        | tok2 = RIGHT
        { $ret_tok2 = $tok2; }

    )? OUTER? tok3 = JOIN_SYM
    { $ret_tok3 = $tok3; }

;

inner_join_table [Expression left] returns [Join ret] @init { $ret = null; }
:
    j = normal_join right = table_ref
    { $ret = act.buildJoin($left, $right.ret, $j.ret_tok1); }

    (
        jc = join_condition
        {  act.joinSetCondition($ret, $jc.ret); }

    )?
;

straight_join_table [Expression left] returns [Join ret] @init { $ret = null; }
:
    t = STRAIGHT_JOIN right = table_factor
    { $ret = act.buildJoin($left, $right.ret, $t); }

    (
        jc = join_condition
        {  act.joinSetCondition($ret, $jc.ret); }

    )?
;

natural_join_table [Expression left] returns [Join ret] @init { $ret = null; }
:
    j = natural_join right = table_factor
    { $ret = act.buildJoin($left, $right.ret, $j.ret_tok1, $j.ret_tok2); }

;

outer_join_table [Expression left] returns [Join ret] @init { $ret = null; }
:
    (
        t = LEFT
        | t = RIGHT
    ) o = OUTER? j = JOIN_SYM right = table_ref
    { $ret = act.buildJoin($left, $right.ret, $t); }

    (
        jc = join_condition
        {  act.joinSetCondition($ret, $jc.ret); }

    )?
;

join_condition returns [JoinConditionClause ret] @init { $ret = null; }
:
    o = ON e = expr
    { $ret = act.buildOn($e.ret, $o); }

    | u = USING LPAREN ul = using_list RPAREN
    { $ret = act.buildUsing($ul.ret, $u); }

;

table_alias returns [Token ret] @init { $ret = null; }
:
    t = AS
    { $ret = $t; }

    | t = EQ
    { $ret = $t; }

;

opt_table_alias returns [Expression ret_expr, Token ret_tok]
@init { $ret_expr = null; $ret_tok = null; }
:
    (
        tok = table_alias
        { $ret_tok = $tok.ret; }

    )? ret = ident
    { $ret_expr = $ret.ret; }

;

opt_all returns [Token ret] @init { $ret = null; }
:
    t = ALL { $ret = $t; }
;

where_clause returns [Expression ret] @init { $ret = null; }
:
    WHERE e = expr
    {  $ret = $e.ret; }

;

order_clause returns [ExpressionList ret] @init { $ret = null; }
:
    ORDER_SYM BY ol = order_list
    {  $ret = $ol.ret; }

;

order_list returns [ExpressionList ret] @init { $ret = null; }
:
    e1 = order_expr
    { $ret = act.buildExpressionList($e1.ret); }

    (
        COMMA e2 = order_expr
        { act.addToExpressionList($ret, $e2.ret); }

    )*
;

order_dir returns [Token ret]
:
    t = ASC
    { $ret = $t; }

    | t = DESC
    { $ret = $t; }

;

limit_clause returns [Limit ret] @init { $ret = null; }
:
    l = LIMIT lo = limit_options
    { $ret = act.buildLimit($l, $lo.limit, $lo.offset); }

;

having_clause returns [Expression ret] @init { $ret = null; }
:
    HAVING e = expr
    { $ret = $e.ret; }

;

opt_escape
:
    ESCAPE_SYM simple_expr
;

/*
   group by statement in select
*/
group_clause returns [ExpressionList ret] @init { $ret = null; }
:
    GROUP_SYM BY gl = group_list olap_opt?
    { $ret = $gl.ret; }

;

group_list returns [ExpressionList ret] @init { $ret = null; }
:
    e1 = order_expr
    { $ret = act.buildExpressionList($e1.ret); }

    (
        COMMA e2 = order_expr
        { act.addToExpressionList($ret, $e2.ret); }

    )*
;

olap_opt
:
    WITH_CUBE_SYM
    | WITH_ROLLUP_SYM
;

optional_braces
:
    LPAREN RPAREN
;

// MySQL precedences (lowest priority on the top)

//%right UNIQUE_SYM KEY_SYM
//%left   JOIN_SYM INNER_SYM STRAIGHT_JOIN CROSS LEFT RIGHT
//%left   TABLE_REF_PRIORITY
//%left   SET_VAR
//%left   OR_OR_SYM OR_SYM OR2_SYM
//%left   XOR
//%left   AND_SYM AND_AND_SYM
//%left   BETWEEN_SYM CASE_SYM WHEN_SYM THEN_SYM ELSE
//%left   EQ EQUAL_SYM GE GT_SYM LE LT NE IS LIKE REGEXP IN_SYM
//%left   '|'
//%left   '&'
//%left   SHIFT_LEFT SHIFT_RIGHT
//%left   '-' '+'
//%left   '*' '/' '%' DIV_SYM MOD_SYM
//%left   '^'
//%left   NEG '~'
//%right  NOT_SYM NOT2_SYM
//%right  BINARY COLLATE_SYM
//%left  INTERVAL_SYM

//expr returns [Expression ret] @init { $ret = null; }
//:
//    bp1 = bool_pri
//    { $ret = $bp1.ret; }
//
//    | t1 = NOT_SYM e = expr // prec NOT_SYM
//    { $ret = $e.ret; }
//
//    | bp1 = bool_pri t1 = IS t2 = TRUE_SYM // prec IS
//    { $ret = act.buildBinaryExpression($t1, $bp1.ret, act.buildLiteral($t2)); }
//
//    | bp1 = bool_pri t1 = IS t2n = not t3 = TRUE_SYM // prec IS
//    { $ret = act.buildBinaryExpression($t1, $bp1.ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }
//
//    | bp1 = bool_pri t1 = IS t2 = FALSE_SYM // prec IS
//    { $ret = act.buildBinaryExpression($t1, $bp1.ret, act.buildLiteral($t2)); }
//
//    | bp1 = bool_pri t1 = IS t2n = not t3 = FALSE_SYM // prec IS
//    { $ret = act.buildBinaryExpression($t1, $bp1.ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }
//
//    | bp1 = bool_pri t1 = IS t2 = UNKNOWN_SYM //prec IS
//    { $ret = act.buildBinaryExpression($t1, $bp1.ret, act.buildLiteral($t2)); }
//
//    | bp1 = bool_pri t1 = IS t2n = not t3 = UNKNOWN_SYM // prec IS
//    { $ret = act.buildBinaryExpression($t1, $bp1.ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }
//
//    | e1 = expr t1a = and e2 = expr // prec AND_SYM
//    { $ret = act.buildBinaryExpression($t1a.ret, $e1.ret, $e2.ret); }
//
//    | e1 = expr t1 = XOR e2 = expr // prec XOR
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = expr t1o = or e2 = expr // prec OR_SYM
//    { $ret = act.buildBinaryExpression($t1o.ret, $e1.ret, $e2.ret); }
//
//;

expr returns [Expression ret] @init { $ret = null; }
:
    e1 = xor_expr
    { $ret = $e1.ret; }

    (
        t = or e2 = xor_expr
        { $ret = act.buildBinaryExpression($t.ret, $ret, $e2.ret); }

    )*
;

xor_expr returns [Expression ret] @init { $ret = null; }
:
    e1 = and_expr
    { $ret = $e1.ret; }

    (
        t = XOR e2 = and_expr
        { $ret = act.buildBinaryExpression($t, $ret, $e2.ret); }

    )*
;

and_expr returns [Expression ret] @init { $ret = null; }
:
    e1 = not_expr
    { $ret = $e1.ret; }

    (
        t = and e2 = not_expr
        { $ret = act.buildBinaryExpression($t.ret, $ret, $e2.ret); }

    )*
;

not_expr returns [Expression ret] @init { $ret = null; }
:
    t = NOT_SYM e = is_expr
    { $ret = act.buildUnaryExpression($t, $e.ret); }

    | e = is_expr
    { $ret = $e.ret; }

;

is_expr returns [Expression ret] @init { $ret = null; }
:
    e = bool_pri
    { $ret = $e.ret; }

    (
        t1 = IS t2 = TRUE_SYM
        { $ret = act.buildBinaryExpression($t1, $ret, act.buildLiteral($t2)); }

        | t1 = IS t2n = not t3 = TRUE_SYM
        { $ret = act.buildBinaryExpression($t1, $ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }

        | t1 = IS t2 = FALSE_SYM
        { $ret = act.buildBinaryExpression($t1, $ret, act.buildLiteral($t2)); }

        | t1 = IS t2n = not t3 = FALSE_SYM
        { $ret = act.buildBinaryExpression($t1, $ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }

        | t1 = IS t2 = UNKNOWN_SYM
        { $ret = act.buildBinaryExpression($t1, $ret, act.buildLiteral($t2)); }

        | t1 = IS t2n = not t3 = UNKNOWN_SYM
        { $ret = act.buildBinaryExpression($t1, $ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }

    )?
;

bool_pri returns [Expression ret] @init { $ret = null; }
:
    e1 = bool_is
    { $ret = $e1.ret; }

    (
        tc = comp_op e2 = bool_is // prec EQ
        { $ret = act.buildBinaryExpression($tc.ret, $ret, $e2.ret); }

        | tc = comp_op all_or_any LPAREN s = subselect RPAREN // prec EQ
        { $ret = act.buildBinaryExpression($tc.ret, $ret, $s.ret); }

    )*
;

bool_is returns [Expression ret] @init { $ret = null; }
:
    p = predicate
    { $ret = $p.ret; }

    | e1 = predicate t1 = IS t2 = NULL_SYM // prec IS
    { $ret = act.buildIsNull($e1.ret, false, $t1, $t2); }
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, act.buildLiteral($t2)); }

    | e1 = predicate t1 = IS t2n = not t3 = NULL_SYM // prec IS
    { $ret = act.buildIsNull($e1.ret, true, $t1, $t3); }
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, act.buildUnaryExpression($t2n.ret, act.buildLiteral($t3))); }

;


//predicate returns [Expression ret] @init { $ret = null; }
//:
//    e1 = bit_expr
//    { $ret = $e1.ret; }
//
//    | e1 = bit_expr tl = LIKE se = simple_expr esc = opt_escape?
//    { $ret = act.buildBinaryExpression($tl, $e1.ret, $se.ret); }
//
//    | e1 = bit_expr tn = not tl = LIKE se = simple_expr esc = opt_escape?
//    { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($tl, $e1.ret, $se.ret)); }
//
//    | e1 = bit_expr ts = SOUNDS_SYM tl = LIKE be = bit_expr
//    { $ret = act.buildBinaryExpression($ts, $e1.ret, $be.ret); }
//
//    | e1 = bit_expr tr = REGEXP be = bit_expr
//    { $ret = act.buildBinaryExpression($tr, $e1.ret, $be.ret); }
//
//    | e1 = bit_expr tn = not tr = REGEXP be = bit_expr
//    { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($tr, $e1.ret, $be.ret)); }
//
//    | e1 = bit_expr ti = IN_SYM LPAREN es = subselect RPAREN
//    { $ret = act.buildBinaryExpression($ti, $e1.ret, $es.ret); }
//
//    | e1 = bit_expr tn = not ti = IN_SYM LPAREN es = subselect RPAREN
//    { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($ti, $e1.ret, $es.ret)); }
//
//    | e1 = bit_expr ti = IN_SYM LPAREN el = expr_list RPAREN
//    { $ret = act.buildBinaryExpression($ti, $e1.ret, $el.ret); }
//
//    | e1 = bit_expr tn = not ti = IN_SYM LPAREN el = expr_list RPAREN
//    { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($ti, $e1.ret, $el.ret)); }
//
//    | e1 = bit_expr tb = BETWEEN_SYM be = bit_expr AND_SYM p = predicate
//    { $ret = act.buildBetween($tb, $e1.ret, $be.ret, $p.ret, false); }
//
//    | e1 = bit_expr tn = not tb = BETWEEN_SYM be = bit_expr AND_SYM p =
//    predicate
//    { $ret = act.buildUnaryExpression($tn.ret, $ret = act.buildBetween($tb, $e1.ret, $be.ret, $p.ret, true)); }
//
//;

predicate returns [Expression ret] @init { $ret = null; }
:
    e1 = bit_expr
    { $ret = $e1.ret; }

    (
        tl = LIKE se = simple_expr esc = opt_escape?
        { $ret = act.buildBinaryExpression($tl, $ret, $se.ret); }

        | tn = not tl = LIKE se = simple_expr esc = opt_escape?
        { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($tl, $ret, $se.ret)); }

        | ts = SOUNDS_SYM tl = LIKE se = simple_expr
        { $ret = act.buildBinaryExpression($ts, $ret, $be.ret); }

        | tr = REGEXP be = bit_expr
        { $ret = act.buildBinaryExpression($tr, $ret, $be.ret); }

        | tn = not tr = REGEXP be = bit_expr
        { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($tr, $ret, $be.ret)); }

        | ti = IN_SYM LPAREN es = subselect RPAREN
        { $ret = act.buildBinaryExpression($ti, $ret, $es.ret); }

        | tn = not ti = IN_SYM LPAREN es = subselect RPAREN
        { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($ti, $ret, $es.ret)); }

        | ti = IN_SYM LPAREN el = expr_list RPAREN
        { $ret = act.buildBinaryExpression($ti, $ret, $el.ret); }

        | tn = not ti = IN_SYM LPAREN el = expr_list RPAREN
        { $ret = act.buildUnaryExpression($tn.ret, act.buildBinaryExpression($ti, $ret, $el.ret)); }

        | tb = BETWEEN_SYM be = bit_expr AND_SYM p = predicate
        { $ret = act.buildBetween($tb, $ret, $be.ret, $p.ret, false); }

        | tn = not tb = BETWEEN_SYM be = bit_expr AND_SYM p = predicate
        { $ret = act.buildUnaryExpression($tn.ret, act.buildBetween($tb, $ret, $be.ret, $p.ret, true)); }
    )?
;

//bit_expr returns [Expression ret] @init { $ret = null; }
//:
//    se = simple_expr
//    { $ret = $se.ret; }
//
//    | e1 = bit_expr t1 = XOR2 e2 = bit_expr
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = ASTERISK e2 = bit_expr // prec '*'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = DIV e2 = bit_expr // prec '/'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = MOD e2 = bit_expr // prec '%'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = DIV_SYM e2 = bit_expr // prec DIV_SYM
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = MOD_SYM e2 = bit_expr // prec MOD_SYM
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = MINUS e2 = bit_expr // prec '-'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = MINUS ti = INTERVAL_SYM ex2 = expr ei = interval // prec '-'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, act.buildInterval($ti, $ex2.ret, $ei.ret)); }
//
//    | e1 = bit_expr t1 = PLUS e2 = bit_expr // prec '+'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = PLUS ti = INTERVAL_SYM ex2 = expr ei = interval // prec '+'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, act.buildInterval($ti, $ex2.ret, $ei.ret)); }
//
//    | e1 = bit_expr t1 = SHIFT_LEFT e2 = bit_expr // prec SHIFT_LEFT
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = SHIFT_RIGHT e2 = bit_expr // prec SHIFT_RIGHT
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = BITAND e2 = bit_expr // prec '&'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//    | e1 = bit_expr t1 = BITOR e2 = bit_expr // prec '|'
//    { $ret = act.buildBinaryExpression($t1, $e1.ret, $e2.ret); }
//
//;

bit_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_and_expr
    { $ret = $e.ret; }

    (
        t1 = BITOR e2 = bit_and_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_and_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_shift_left_expr
    { $ret = $e.ret; }

    (
        t1 = BITAND e2 = bit_shift_left_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_shift_left_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_shift_right_expr
    { $ret = $e.ret; }

    (
        t1 = SHIFT_LEFT e2 = bit_shift_right_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_shift_right_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_plus_expr
    { $ret = $e.ret; }

    (
        t1 = SHIFT_RIGHT e2 = bit_plus_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_plus_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_minus_expr
    { $ret = $e.ret; }

    (
        t1 = PLUS e2 = bit_minus_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_minus_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_mul_expr
    { $ret = $e.ret; }

    (
        t1 = MINUS e2 = bit_mul_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_mul_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_div_expr
    { $ret = $e.ret; }

    (
        t1 = ASTERISK e2 = bit_div_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_div_expr returns [Expression ret] @init { $ret = null; }
:
    e = bit_mod_expr
    { $ret = $e.ret; }

    (
        t1 = DIV e2 = bit_mod_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

        | t1 = DIV_SYM e2 = bit_mod_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_mod_expr returns [Expression ret] @init { $ret = null; }
:
    se = bit_xor_expr
    { $ret = $se.ret; }

    (
        t1 = MOD e2 = bit_xor_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

        | t1 = MOD_SYM e2 = bit_xor_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;

bit_xor_expr returns [Expression ret] @init { $ret = null; }
:
    se = simple_expr
    { $ret = $se.ret; }

    (
        t1 = XOR2 e2 = simple_expr
        { $ret = act.buildBinaryExpression($t1, $ret, $e2.ret); }

    )*
;


or returns [Token ret] @init { $ret = null; }
:
    t = OR_SYM
    { $ret = $t; }

    | t = OR_OR_SYM
    { $ret = $t; }

;

and returns [Token ret] @init { $ret = null; }
:
    t = AND_SYM
    { $ret = $t; }

    | t = AND_AND_SYM
    { $ret = $t; }

;

not returns [Token ret] @init { $ret = null; }
:
    t = NOT_SYM
    { $ret = $t; }

    | t = NOT2_SYM
    { $ret = $t;
    }

;

not2 returns [Token ret] @init { $ret = null; }
:
    t = NOT2_SYM
    { $ret = $t; }

;

comp_op returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = EQ
    | t = EQUAL_SYM
    | t = GE
    | t = GT_SYM
    | t = LE
    | t = LT
    | t = NE
;

all_or_any returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = ALL
    | t = ANY_SYM
;

simple_expr returns [Expression ret] @init { $ret = null; }
:
    i = simple_ident
    { $ret = $i.ret; }

//    | ti = INTERVAL_SYM e1 = expr unit = interval t = PLUS e2 = expr // prec INTERVAL_SYM
//    { $ret = act.buildBinaryExpression($t, act.buildInterval($ti, $e1.ret, $unit.ret), $e2.ret); }

    | ti = INTERVAL_SYM ex = expr unit = interval
    { $ret = act.buildInterval($ti, $ex.ret, $unit.ret); }

    | t = PLUS se = simple_expr // prec NEG
    { $ret = act.buildUnaryExpression($t, $se.ret); }

    | t = MINUS se = simple_expr // prec NEG
    { $ret = act.buildUnaryExpression($t, $se.ret); }

    | t = TILDE se = simple_expr // prec NEG
    { $ret = act.buildUnaryExpression($t, $se.ret); }

    | tn = not2 simple_expr // prec NEG
    { $ret = act.buildUnaryExpression($tn.ret, $se.ret); }

    | t = BINARY se = simple_expr // prec NEG
    { $ret = act.buildUnaryExpression($t, $se.ret); }

    | se = simple_expr tc = COLLATE_SYM it = ident_or_text // prec NEG
    { $ret = act.buildBinaryExpression($tc, $se.ret, $it.ret); }

    | fck = function_call_keyword
    { $ret = $fck.ret; }

    | fcn = function_call_nonkeyword
    {$ret = $fcn.ret; }

    | fcg = function_call_generic
    { $ret = $fcg.ret; }

    | fcc = function_call_conflict
    { $ret = $fcc.ret; }

    | l = literal
    { $ret = $l.ret; }

    | p = param_marker
    { $ret = $p.ret; }

    | v = variable
    { $ret = $v.ret; }

    | su = sum_expr
    { $ret = $su.ret; }

    | LPAREN sel = subselect RPAREN
    { $ret = $sel.ret; }

    | LPAREN e = expr RPAREN
    { $ret = $e.ret; }

    | t = LPAREN el = expr_list2 RPAREN
    { $ret = act.buildRow($t, $el.ret); }

    | t = ROW_SYM LPAREN expr_list2 RPAREN
    { $ret = act.buildRow($t, $el.ret); }

    | t = EXISTS LPAREN s = subselect RPAREN
    { $ret = act.buildUnaryExpression($t, $s.ret); }

    //    | '{' ident expr '}'

    | t = MATCH il = ident_list_arg AGAINST LPAREN be = bit_expr
    fulltext_options RPAREN
    { $ret = act.buildMatchAgainst($t, $be.ret, $il.ret); }

    | t = CAST_SYM LPAREN e = expr AS ct = cast_type RPAREN
    { $ret = act.buildCast($t, $e.ret, $ct.ret); }

    | t = CASE_SYM e = expr?
    { $ret = act.buildCase($t, $e.ctx == null ? null : $e.ret); }
    whenlist = when_list
    { act.caseAddWhenClauseList((Case)$ret, $whenlist.ret); }
    ( ELSE elseexpr = expr
        { act.caseSetElse((Case)$ret, $elseexpr.ret); }
    )?
    END


    | CONVERT_SYM LPAREN expr COMMA cast_type RPAREN
    | CONVERT_SYM LPAREN expr USING charset_name RPAREN
    | DEFAULT LPAREN simple_ident RPAREN
    | VALUES LPAREN simple_ident_nospvar RPAREN
    //        | simple_ident JSON_SEPARATOR_SYM text_string_literal
    //        | simple_ident JSON_UNQUOTED_SEPARATOR_SYM text_string_literal

;

function_call_keyword returns [InnerFunctionCall ret] @init { $ret = null; }
:
    t = CHAR_SYM LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = CHAR_SYM LPAREN el = expr_list USING charset_name RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = CURRENT_USER optional_braces?
    { $ret = act.buildInnerFunctionCall($t); }

    | t = DATE_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = DAY_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = HOUR_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }
    | t = INSERT LPAREN e1 = expr COMMA e2 = expr COMMA e3 = expr COMMA e4 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret, $e3.ret, $e4.ret); }

//    | t = INTERVAL_SYM LPAREN expr COMMA expr RPAREN // prec INTERVAL_SYM
//    | t = INTERVAL_SYM LPAREN expr COMMA expr COMMA expr_list RPAREN // prec INTERVAL_SYM
    | t = INTERVAL_SYM LPAREN el = expr_list RPAREN // prec INTERVAL_SYM
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = LEFT LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = MINUTE_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = MONTH_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }
    //    | t = RIGHT LPAREN expr COMMA expr RPAREN

    | t = RIGHT LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = SECOND_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = TIME_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = TIMESTAMP LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = TIMESTAMP LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = YEAR_SYM LPAREN e = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e.ret); }

    | t = USER LPAREN RPAREN
    { $ret = act.buildInnerFunctionCall($t); }

    | trim = trim_function_call_keyword
    { $ret = $trim.ret; }

;

trim_function_call_keyword returns [InnerFunctionCall ret]
@init { $ret = null; }
:
    t = TRIM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = TRIM LPAREN e1 = expr FROM e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = TRIM LPAREN p1 = LEADING e1 = expr FROM e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($p1), $e1.ret, $e2.ret); }

    | t = TRIM LPAREN p1 = TRAILING e1 = expr FROM e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($p1), $e1.ret, $e2.ret); }

    | t = TRIM LPAREN p1 = BOTH e1 = expr FROM e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($p1), $e1.ret, $e2.ret); }

    | t = TRIM LPAREN p1 = LEADING FROM e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($p1), $e1.ret); }

    | t = TRIM LPAREN p1 = TRAILING FROM e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($p1), $e1.ret); }

    | t = TRIM LPAREN p1 = BOTH FROM e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($p1), $e1.ret); }

;


function_call_nonkeyword returns [InnerFunctionCall ret] @init { $ret = null; }
:
    t = ADDDATE_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = ADDDATE_SYM LPAREN e1 = expr COMMA ti = INTERVAL_SYM e2 = expr unit = interval RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, act.buildInterval($ti, $e2.ret, $unit.ret)); }

    | t = CURDATE optional_braces?
    { $ret = act.buildInnerFunctionCall($t); }

    | t = CURTIME func_datetime_precision
    | t = DATE_ADD_INTERVAL LPAREN e1 = expr COMMA ti = INTERVAL_SYM e2 = expr unit = interval RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, act.buildInterval($ti, $e2.ret, $unit.ret)); }
    // prec INTERVAL_SYM

    | t = DATE_SUB_INTERVAL LPAREN e1 = expr COMMA ti = INTERVAL_SYM e2 = expr unit = interval RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, act.buildInterval($ti, $e2.ret, $unit.ret)); }
    // prec INTERVAL_SYM

    | t = EXTRACT_SYM LPAREN unit = interval FROM e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($unit.ret), $e1.ret); }

    | t = GET_FORMAT LPAREN dt = date_time_type COMMA e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($dt.ret), $e1.ret); }

    | n = now
    { $ret = $n.ret; }

    | t = POSITION_SYM LPAREN be = bit_expr IN_SYM e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $be.ret, $e2.ret); }

    | t = SUBDATE_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = SUBDATE_SYM LPAREN e1 = expr COMMA ti = INTERVAL_SYM e2 = expr unit = interval RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, act.buildInterval($ti, $e2.ret, $unit.ret)); }

    | t = SUBSTRING LPAREN e1 = expr COMMA e2 = expr COMMA e3 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret, $e3.ret); }

    | t = SUBSTRING LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = SUBSTRING LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = SUBSTRING LPAREN e1 = expr FROM e2 = expr FOR_SYM e3 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret, $e3.ret); }

    | t = SUBSTRING LPAREN e1 = expr FROM e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret ); }

    | t = SYSDATE pars = func_datetime_precision
    { $ret = act.buildInnerFunctionCall($t, $pars.ret ); }

    | t = TIMESTAMP_ADD LPAREN ts = interval_time_stamp COMMA e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($ts.ret), $e1.ret, $e2.ret); }

    | t = TIMESTAMP_DIFF LPAREN ts = interval_time_stamp COMMA e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, (Expression)act.tokenToLiteral($ts.ret), $e1.ret, $e2.ret); }
    | t = UTC_DATE_SYM optional_braces?
    { $ret = act.buildInnerFunctionCall($t); }

    | t = UTC_TIME_SYM pars = func_datetime_precision
    { $ret = act.buildInnerFunctionCall($t, $pars.ret ); }

    | t = UTC_TIMESTAMP_SYM pars = func_datetime_precision
    { $ret = act.buildInnerFunctionCall($t, $pars.ret ); }

;

function_call_conflict returns [InnerFunctionCall ret] @init { $ret = null; }
:
    t = ASCII_SYM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = CHARSET LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = COALESCE LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = COLLATION_SYM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = DATABASE LPAREN RPAREN
    { $ret = act.buildInnerFunctionCall($t); }

    | t = IF LPAREN e1 = expr COMMA e2 = expr COMMA e3 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret, $e3.ret); }

    | t = FORMAT_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = FORMAT_SYM LPAREN e1 = expr COMMA e2 = expr COMMA e3 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret, $e3.ret); }

    | t = MICROSECOND_SYM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = MOD_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = PASSWORD LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = QUARTER_SYM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = REPEAT_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = REPLACE LPAREN e1 = expr COMMA e2 = expr COMMA e3 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret, $e3.ret); }

    | t = REVERSE_SYM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = ROW_COUNT_SYM LPAREN RPAREN
    { $ret = act.buildInnerFunctionCall($t); }

    | t = TRUNCATE_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = WEEK_SYM LPAREN e1 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = WEEK_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = WEIGHT_STRING_SYM LPAREN e1 = expr opt_ws_levels? RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = WEIGHT_STRING_SYM LPAREN e1 = expr AS CHAR_SYM ws_nweights
    opt_ws_levels? RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = WEIGHT_STRING_SYM LPAREN e1 = expr AS BINARY ws_nweights RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | t = WEIGHT_STRING_SYM LPAREN e1 = expr COMMA ulong_num COMMA ulong_num
    COMMA ulong_num RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret); }

    | gf = geometry_function
    { $ret = $gf.ret; }

;

geometry_function returns [InnerFunctionCall ret] @init { $ret = null; }
:
    t = CONTAINS_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = GEOMETRYCOLLECTION LPAREN el = expr_list? RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ctx == null ? null : $el.ret); }

    | t = LINESTRING LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = MULTILINESTRING LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = MULTIPOINT LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = MULTIPOLYGON LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

    | t = POINT_SYM LPAREN e1 = expr COMMA e2 = expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, $e1.ret, $e2.ret); }

    | t = POLYGON LPAREN el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, $el.ret); }

;

function_call_generic returns [FunctionCall ret] @init { $ret = null; }
:
    i = ident_sys LPAREN RPAREN
    { $ret = act.buildUserFunctionCall($i.ret); }

    | i = ident_sys LPAREN uel = opt_udf_expr_list RPAREN
    { $ret = act.buildUserFunctionCall($i.ret, act.buildFunctionParams($uel.ret)); }

    | i1 = ident t = DOT i2 = ident LPAREN RPAREN
    { $ret = act.buildUserFunctionCall(act.buildBinaryExpression($t, $i1.ret, $i2.ret)); }

    | i1 = ident t = DOT i2 = ident LPAREN el = expr_list RPAREN
    { $ret = act.buildUserFunctionCall(act.buildBinaryExpression($t, $i1.ret, $i2.ret), act.buildFunctionParams($el.ret)); }

;

fulltext_options
:
    opt_natural_language_mode? opt_query_expansion?
    | IN_SYM BOOLEAN_SYM MODE_SYM
;

opt_natural_language_mode
:
    IN_SYM NATURAL LANGUAGE_SYM MODE_SYM
;

opt_query_expansion
:
    WITH QUERY_SYM EXPANSION_SYM
;

opt_udf_expr_list returns [ExpressionList ret]
:
    el = udf_expr_list
    { $ret = $el.ret; }
;

udf_expr_list returns [ExpressionList ret]
:
    e = udf_expr
    { $ret = act.buildExpressionList($e.ret); }

    (
        COMMA e = udf_expr
        { act.addToExpressionList($ret, $e.ret); }

    )*
;

udf_expr returns [Expression ret]
:
    e = expr
    { $ret = $e.ret; }

    (
        a = select_alias [ $e.ret]
        { $ret = $a.ret; }

    )?
;

sum_expr returns [InnerFunctionCall ret] @init { $ret = null; }
:
    t = AVG_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = AVG_SYM LPAREN td = DISTINCT ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($td, $ie.ret_tok, $ie.ret)); }

    | t = BIT_AND LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = BIT_OR LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = BIT_XOR LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = COUNT_SYM LPAREN a = opt_all? star = ASTERISK RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($a.ctx == null ? null : $a.ret, null, act.buildAsterisk($star))); }

    | t = COUNT_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = COUNT_SYM LPAREN td = DISTINCT el = expr_list RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($td, null , $el.ret)); }

    | t = MIN_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = MIN_SYM LPAREN td = DISTINCT ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($td, $ie.ret_tok, $ie.ret)); }

    | t = MAX_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = MAX_SYM LPAREN td = DISTINCT ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($td, $ie.ret_tok, $ie.ret)); }

    | t = STD_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = VARIANCE_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = STDDEV_SAMP_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = VAR_SAMP_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = SUM_SYM LPAREN ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($ie.ret_tok, null, $ie.ret)); }

    | t = SUM_SYM LPAREN td = DISTINCT ie = in_sum_expr RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($td, $ie.ret_tok, $ie.ret)); }

    | t = GROUP_CONCAT_SYM LPAREN d = opt_distinct? el = expr_list
    opt_gorder_clause opt_gconcat_separator RPAREN
    { $ret = act.buildInnerFunctionCall($t, act.buildFunctionParams($d.ctx == null ? null : $d.ret, null , $el.ret)); }

;

variable returns [Expression ret] @init { $ret = null; }
:
    '@' v = variable_aux
    { $ret = $v.ret; }

;

variable_aux returns [Expression ret] @init { $ret = null; }
:
    i = ident_or_text t = SET_VAR e = expr
    { $ret = act.buildSetVar($t, $i.ret, $e.ret); }

    | i = ident_or_text
    { $ret = $i.ret; }

    | '@' opt_var_ident_type i = ident_or_text
    { $ret = $i.ret;}

    (
        c = opt_component
        { $ret = act.buildBinaryExpression($c.ret_tok, $i.ret, $c.ret); }

    )?
;

opt_distinct returns [Token ret] @init { $ret = null; }
:
    t = DISTINCT { $ret = $t; }
;

opt_gconcat_separator
:
    SEPARATOR_SYM text_string
;

opt_gorder_clause
:
    ORDER_SYM BY gorder_list
;

gorder_list
:
    gorder_list COMMA order_expr
    | order_expr
;

in_sum_expr returns [Token ret_tok, Expression ret]
@init { $ret_tok = null; $ret = null; }
:
    (
        t = opt_all
        { $ret_tok = $t.ret; }

    )? e = expr
    { $ret = $e.ret; }

;

cast_type returns [Type ret] @init { $ret = null; }
:
    t1 = BINARY
    { $ret = act.newStringType($t1, null); }

    | t1 = BINARY fl = field_length
    { $ret = act.newStringType($t1, $fl.ret); }

    | t1 = CHAR_SYM binary?
    { $ret = act.newStringType($t1, null); }

    | t1 = CHAR_SYM fl = field_length binary?
    { $ret = act.newStringType($t1, $fl.ret); }

    | t1 = NCHAR_SYM
    { $ret = act.newStringType($t1, null); }

    | t1 = NCHAR_SYM fl = field_length
    { $ret = act.newStringType($t1, $fl.ret); }

    | t1 = SIGNED_SYM
    { $ret = act.newNumericType($t1, null, null, null); }

    | t1 = SIGNED_SYM INT_SYM
    { $ret = act.newNumericType($t1, null, null, null); }

    | t1 = UNSIGNED
    { $ret = act.newNumericType($t1, null, null, null); }

    | t1 = UNSIGNED INT_SYM
    { $ret = act.newNumericType($t1, null, null, null); }

    | t1 = DATE_SYM
    { $ret = act.newDateType($t1, null, null); }

    | t1 = TIME_SYM pr = type_datetime_precision
    { $ret = act.newDateType($t1, null, $pr.ret); }

    | t1 = DATETIME pr = type_datetime_precision
    { $ret = act.newDateType($t1, null, $pr.ret); }

    | t1 = DECIMAL_SYM fo = float_options
    { $ret = act.newNumericType($t1, $fo.prec, $fo.scale, null); }

    | t1 = JSON_SYM
;

expr_list returns [ExpressionList ret] @init { $ret = null; }
:
    e = expr
    { $ret = act.buildExpressionList($e.ret); }

    (
        COMMA e = expr
        { act.addToExpressionList($ret, $e.ret); }

    )*
;

// expression list with at least two elements
// MySQL uses 'expr COMMA expr_list'

expr_list2 returns [ExpressionList ret] @init { $ret = null; }
:
    e = expr
    { $ret = act.buildExpressionList($e.ret); }

    (
        COMMA e = expr
        { act.addToExpressionList($ret, $e.ret); }

    )+
;

ident_list_arg returns [ExpressionList ret] @init { $ret = null; }
:
    il = ident_list
    { $ret = $il.ret; }

    | LPAREN il = ident_list RPAREN
    { $ret = $il.ret; }

;

ident_list returns [ ExpressionList ret] @init { $ret = null; }
:
    i = simple_ident
    { $ret = act.buildExpressionList($i.ret); }

    (
        COMMA i = simple_ident
        { act.addToExpressionList($ret, $i.ret); }

    )*
;

// LITERALS

intervalLiteral returns [Literal ret] @init { $ret = null; }
:
    i = INTERVAL_SYM expr interval
    {  $ret = act.buildLiteral($i); }

;

interval returns [Token ret] @init { $ret = null; }
:
    i = interval_time_stamp
    { $ret = $i.ret; }
	| (
      | t = DAY_HOUR_SYM
      | t = DAY_MICROSECOND_SYM
      | t = DAY_MINUTE_SYM
      | t = DAY_SECOND_SYM
      | t = HOUR_MICROSECOND_SYM
      | t = HOUR_MINUTE_SYM
      | t = HOUR_SECOND_SYM
      | t = MINUTE_MICROSECOND_SYM
      | t = MINUTE_SECOND_SYM
      | t = SECOND_MICROSECOND_SYM
      | t = YEAR_MONTH_SYM
      ) { $ret = $t; }
;

interval_time_stamp returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = DAY_SYM
    | t = WEEK_SYM
    | t = HOUR_SYM
    | t = MINUTE_SYM
    | t = MONTH_SYM
    | t = QUARTER_SYM
    | t = SECOND_SYM
    | t = MICROSECOND_SYM
    | t = YEAR_SYM
;

date_time_type returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = DATE_SYM
    | t = TIME_SYM
    | t = TIMESTAMP
    | t = DATETIME
;

text_string_sys returns [Literal ret] @init { $ret = null; }
:
    t = LIT_STRING
    {  $ret = act.buildLiteral($t); }

;

text_string_literal returns [Literal ret] @init { $ret = null; }
:
    t = LIT_STRING
    {  $ret = act.buildLiteral($t); }

;

text_string_filesystem returns [Literal ret] @init { $ret = null; }
:
    t = LIT_STRING
    {  $ret = act.buildLiteral($t); }

;

text_literal returns [Literal ret] @init { $ret = null; }
:
    t = LIT_STRING
    {  $ret = act.buildLiteral($t); }

;

text_string returns [Literal ret] @init { $ret = null; }
:
    t = text_string_literal
    { $ret = $t.ret; }

    | hex = hex_num_lit
    {  $ret = $hex.ret; }

    | bin = bin_num_lit
    {  $ret = $bin.ret; }

;

literal returns [Literal ret] @init { $ret = null; }
:
//    i = intervalLiteral
//    {  $ret = $i.ret; }
    str = text_literal
    {  $ret = $str.ret; }

    | temp = temporal_lit
    {  $ret = $temp.ret; }

    | num = num_lit
    {  $ret = $num.ret; }

    | n = null_lit
    {  $ret = $n.ret; }

    | b = boolean_lit
    {  $ret = $b.ret; }

    | hex = hex_num_lit
    {  $ret = $hex.ret; }

    | bin = bin_num_lit
    {  $ret = $bin.ret; }

;

boolean_lit returns [Literal ret] @init { $ret = null; }
:
    f = FALSE_SYM
    {  $ret = act.buildLiteral($f); }

    | t = TRUE_SYM
    {  $ret = act.buildLiteral($t); }

;

null_lit returns [Literal ret] @init { $ret = null; }
:
    n = NULL_SYM
    {  $ret = act.buildLiteral($n); }

;

temporal_lit returns [Literal ret] @init { $ret = null; }
:
    DATE_SYM l = LIT_STRING
    {  $ret = act.buildLiteral($l); }

    | TIME_SYM l = LIT_STRING
    {  $ret = act.buildLiteral($l); }

    | TIMESTAMP l = LIT_STRING
    {  $ret = act.buildLiteral($l); }

;

// MySQL HEX_NUM

hex_num_lit returns [Literal ret] @init { $ret = null; }
:
    l = HEX_NUM
    {  $ret = act.buildLiteral($l); }

;

// MySQL BIN_NUM

bin_num_lit returns [Literal ret] @init { $ret = null; }
:
    l = LIT_BINARY
    {  $ret = act.buildLiteral($l); }

;

// MySQL NUM_literal

num_lit returns [Literal ret] @init { $ret = null; }
:
    l = LIT_DECIMAL
    {  $ret = act.buildLiteral($l); }

    | l = LIT_INTEGER
    {  $ret = act.buildLiteral($l); }

    | l = LIT_REAL
    {  $ret = act.buildLiteral($l); }

;

param_marker returns [Id ret] @init { $ret = null; }
:
    t = PARAM_MARKER
    { $ret = act.buildId($t); }

;

signed_literal returns [Literal ret] @init { $ret = null; }
:
    (
        PLUS
        | MINUS
    ) // TODO: add this sign to the literal
    l = LIT_INTEGER
    {  $ret = act.buildLiteral($l); }

    | ll = literal
    {  $ret = $ll.ret; }

;

ulong_num
:
    num_lit
    //          NUM
    //        | HEX_NUM
    //        | LONG_NUM
    //        | ULONGLONG_NUM
    //        | DECIMAL_NUM
    //        | FLOAT_NUM

;

real_ulong_num
:
    num_lit
    //          NUM
    //        | HEX_NUM
    //        | LONG_NUM
    //        | ULONGLONG_NUM
    //        | dec_num_error

;

ulonglong_num
:
    num_lit
    //          NUM
    //        | ULONGLONG_NUM
    //        | LONG_NUM
    //        | DECIMAL_NUM
    //        | FLOAT_NUM

;

real_ulonglong_num
:
    num_lit
    //          NUM
    //        | ULONGLONG_NUM
    //        | LONG_NUM
    //        | dec_num_error

;

dec_num_error
:
    dec_num
;

dec_num
:
    num_lit
    //          DECIMAL_NUM
    //        | FLOAT_NUM

;

opt_procedure_analyse_clause
:
    PROCEDURE_SYM ANALYSE_SYM LPAREN opt_procedure_analyse_params RPAREN
;

opt_procedure_analyse_params
:
    procedure_analyse_param COMMA procedure_analyse_param
;

procedure_analyse_param
:
    num_lit
;

// IDENTIFIERS

insert_ident returns [Expression ret] @init { $ret = null; }
:
    i = simple_ident_nospvar
    {  $ret = $i.ret; }

    | t = table_wild
    {  $ret = $t.ret; }

;

table_wild returns [BinaryExpression ret] @init { $ret = null; }
:
    i = ident d1 = DOT s = ASTERISK
    {  $ret = act.buildBinaryExpression($d1, $i.ret, act.buildAsterisk($s)); }

    | i1 = ident d1 = DOT i2 = ident d2 = DOT s = ASTERISK
    {  $ret = act.buildBinaryExpression($d2, act.buildBinaryExpression($d1, $i1.ret, $i2.ret), act.buildAsterisk($s)); }

;

order_expr returns [OrderByElement ret] @init { $ret = null; }
:
    e = expr
    { $ret = act.buildOrderByElement($e.ret); }

    (
        dir = order_dir
        { act.orderByElementSetKind($ret, $dir.ret); }

    )?

;


// [schema.][table.]column
// or .column for Delphi -- unsupported now

field_ident returns [Expression ret] @init { $ret = null; }
:
    i1 = ident
    { $ret = $i1.ret; }

    | i1 = ident o = DOT i2 = ident
    { $ret = act.buildBinaryExpression($o, $i1.ret, $i2.ret); }

    | i1 = ident o1 = DOT i2 = ident o2 = DOT i3 = ident
    { $ret = act.buildBinaryExpression($o2, act.buildBinaryExpression($o1, $i1.ret, $i2.ret), $i3.ret); }

    | o = DOT i2 = ident // for Delphi
    { $ret = $i2.ret; }

;

table_ident returns [Expression ret] @init { $ret = null; }
:
    i1 = ident
    { $ret = $i1.ret; }

    | i1 = ident o = DOT i2 = ident
    { $ret = act.buildBinaryExpression($o, $i1.ret, $i2.ret); }

    | o = DOT i1 = ident // TODO: not supported now, for Delphi
    { $ret = $i1.ret; }

;

table_ident_opt_wild returns [Expression ret]
:
    i = ident
    { $ret = $i.ret; }

    | i1 = ident t1 = DOT a = ASTERISK
    { $ret = act.buildBinaryExpression($t1, $i1.ret, act.buildAsterisk($a)); }

    | i1 = ident t1 = DOT i2 = ident
    { $ret = act.buildBinaryExpression($t1, $i1.ret, $i2.ret); }

    | i1 = ident t1 = DOT i2 = ident t2 = DOT a = ASTERISK
    { $ret = act.buildBinaryExpression($t2, act.buildBinaryExpression($t1, $i1.ret, $i2.ret), act.buildAsterisk($a)); }

;

simple_ident returns [Expression ret] @init { $ret = null; }
:
    i = ident
    {$ret = $i.ret; }

    | si = simple_ident_q
    {$ret = $si.ret; }

;

simple_ident_nospvar returns [Expression ret] @init { $ret = null; }
:
    i = ident
    {$ret = $i.ret; }

    | si = simple_ident_q
    {$ret = $si.ret; }

;

simple_ident_q returns [BinaryExpression ret] @init { $ret = null; }
:
    i1 = ident o = DOT i2 = ident
    { $ret = act.buildBinaryExpression($o, $i1.ret, $i2.ret); }

    | o = DOT i1 = ident DOT i2 = ident
    { $ret = act.buildBinaryExpression($o, $i1.ret, $i2.ret); }

    | i1 = ident o1 = DOT i2 = ident o2 = DOT i3 = ident
    { $ret = act.buildBinaryExpression($o2, act.buildBinaryExpression($o1, $i1.ret, $i2.ret), $i3.ret); }

;

ident returns [Expression ret] @init { $ret = null; }
:
    i = ident_sys
    {  $ret=$i.ret; }

    | k = keyword
    {  $ret = act.buildId($k.ret); }

;

ident_sys returns [Expression ret] @init { $ret = null; }
:
    i = ID
    { $ret = act.buildId($i); }

    | iq = ID_QUOTED
    { $ret = act.buildId($iq, true); }

    | in = ID_UNRESOLVED
    { $ret = act.buildJoker($in); }

;

ident_or_text returns [Expression ret] @init { $ret = null; }
:
    i = ident
    {  $ret = $i.ret; }

    | s = LIT_STRING
    {  $ret = act.buildId($s); }

;

user
:
    ident_or_text
    | ident_or_text AT ident_or_text
    | CURRENT_USER optional_braces?
;

/*
   UNIONS : glue selects together
*/
opt_union_clause [Query left] returns [BinaryQuery ret]
:
    l = union_list [left]
    { $ret = $l.ret; }

;

union_list [Query left] returns [BinaryQuery ret] @init { $ret = null; }
:
    t = UNION_SYM opt = union_option? right = select_init
    { $ret = act.buildBinaryQuery($t, ($opt.ctx==null ? null : $opt.ret), $left, $right.ret); }

;

union_opt [Query left] returns [Query ret]
:
    l = union_list [$left]
    { $ret = $l.ret; }

    | order_or_limit [$left]
    { $ret = $left; }

;

// union_order_or_limit
order_or_limit [Query left]
:
    order = order_clause
    { act.querySetOrderBy($left, $order.ret); }

    (
        limit = limit_clause
        { act.querySetLimit($left, $limit.ret); }

    )?
    | limit = limit_clause
    { act.querySetLimit($left, $limit.ret); }

;

union_option returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = DISTINCT
    | t = ALL
;

query_specification returns [SelectExpression ret] @init { $ret = null; }
:
    st = SELECT_SYM sel = select_part2_derived [$st]
    { $ret = $sel.ret; }

    table_expression [$ret]
    | LPAREN par = select_paren_derived RPAREN
    { $ret = $par.ret; }

;

query_expression_body returns [Query ret] @init { $ret = null; }
:
    q = query_specification
    { $ret = $q.ret; }

    | qe = query_expression_body UNION_SYM opts = union_option qs =
    query_specification
    { $ret = act.buildBinaryQuery($opts.ret, null, $qe.ret, $qs.ret); }

;

/* Corresponds to <query expression> in the SQL:2003 standard. */
subselect returns [Query ret] @init { $ret = null; }
:
    q = query_expression_body
    { $ret = $q.ret; }

;

opt_query_spec_options returns [ List<Token> ret]
@init { $ret = new ArrayList<Token>(); }
:
    list = query_spec_option_list
    { $ret = $list.ret; }

;

query_spec_option_list returns [ List<Token> ret]
@init { $ret = new ArrayList<Token>(); }
:
    (
        qo = query_spec_option
        { $ret.add($qo.ret); }

    )+
;

query_spec_option returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = STRAIGHT_JOIN
    | t = HIGH_PRIORITY
    | t = DISTINCT
    | t = SQL_SMALL_RESULT
    | t = SQL_BIG_RESULT
    | t = SQL_BUFFER_RESULT
    | t = SQL_CALC_FOUND_ROWS
    | t = ALL
;

// Keyword that are allowed for identifiers (except SP labels)
keyword returns [Token ret] @init { $ret = null; }
:
    kw = keyword_sp
    { $ret = $kw.ret; }
    
	| kwu = keyword_undoc
	{ $ret = $kwu.ret; }
	
    |
    (
        t = ACCOUNT_SYM
        | t = ASCII_SYM
        | t = ALWAYS_SYM
        | t = BACKUP_SYM
        | t = BEGIN_SYM
        | t = BYTE_SYM
        | t = CACHE_SYM
        | t = CHARSET
        | t = CHECKSUM_SYM
        | t = CLOSE_SYM
        | t = COMMENT_SYM
        | t = COMMIT_SYM
        | t = CONTAINS_SYM
        | t = DEALLOCATE_SYM
        | t = DO_SYM
        | t = END
        | t = EXECUTE_SYM
        | t = FLUSH_SYM
        | t = FOLLOWS_SYM
        | t = FORMAT_SYM
        | t = GROUP_REPLICATION
        | t = HANDLER_SYM
        | t = HELP_SYM
        | t = HOST_SYM
        | t = INSTALL_SYM
        | t = LANGUAGE_SYM
        | t = NO_SYM
        | t = OPEN_SYM
        | t = OPTIONS_SYM
        | t = OWNER_SYM
        | t = PARSER_SYM
        | t = PARSE_GCOL_EXPR_SYM
        | t = PORT_SYM
        | t = PRECEDES_SYM
        | t = PREPARE_SYM
        | t = REMOVE_SYM
        | t = REPAIR
        | t = RESET_SYM
        | t = RESTORE_SYM
        | t = ROLLBACK_SYM
        | t = SAVEPOINT_SYM
        | t = SECURITY_SYM
        | t = SERVER_SYM
        | t = SHUTDOWN
        | t = SIGNED_SYM
        | t = SOCKET_SYM
        | t = SLAVE
        | t = SONAME_SYM
        | t = START_SYM
        | t = STOP_SYM
        | t = TRUNCATE_SYM
        | t = UNICODE_SYM
        | t = UNINSTALL_SYM
        | t = WRAPPER_SYM
        | t = XA_SYM
        | t = UPGRADE_SYM
    )
    { $ret = $t; }

;

// Some miraculous keywords also allowed as identifiers 
keyword_undoc returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
	t = POSITION_SYM
	{ $ret = $t; }
;

// Keywords that are allowed for labels in SPs.
keyword_sp returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
    t = ACTION
    | t = ADDDATE_SYM
    | t = AFTER_SYM
    | t = AGAINST
    | t = AGGREGATE_SYM
    | t = ALGORITHM_SYM
    | t = ANALYSE_SYM
    | t = ANY_SYM
    | t = AT_SYM
    | t = AUTO_INC
    | t = AUTOEXTEND_SIZE_SYM
    | t = AVG_ROW_LENGTH
    | t = AVG_SYM
    | t = BINLOG_SYM
    | t = BIT_SYM
    | t = BLOCK_SYM
    | t = BOOL_SYM
    | t = BOOLEAN_SYM
    | t = BTREE_SYM
    | t = CASCADED
    | t = CATALOG_NAME_SYM
    | t = CHAIN_SYM
    | t = CHANGED
    | t = CHANNEL_SYM
    | t = CIPHER_SYM
    | t = CLIENT_SYM
    | t = CLASS_ORIGIN_SYM
    | t = COALESCE
    | t = CODE_SYM
    | t = COLLATION_SYM
    | t = COLUMN_NAME_SYM
    | t = COLUMN_FORMAT_SYM
    | t = COLUMNS
    | t = COMMITTED_SYM
    | t = COMPACT_SYM
    | t = COMPLETION_SYM
    | t = COMPRESSED_SYM
    | t = COMPRESSION_SYM
    | t = ENCRYPTION_SYM
    | t = CONCURRENT
    | t = CONNECTION_SYM
    | t = CONSISTENT_SYM
    | t = CONSTRAINT_CATALOG_SYM
    | t = CONSTRAINT_SCHEMA_SYM
    | t = CONSTRAINT_NAME_SYM
    | t = CONTEXT_SYM
    | t = CPU_SYM
    | t = CUBE_SYM
    /*
          Although a reserved keyword in SQL:2003 (and :2008),
          not reserved in MySQL per WL#2111 specification.
        */
    | t = CURRENT_SYM
    | t = CURSOR_NAME_SYM
    | t = DATA_SYM
    | t = DATAFILE_SYM
    | t = DATETIME
    | t = DATE_SYM
    | t = DAY_SYM
    | t = DEFAULT_AUTH_SYM
    | t = DEFINER_SYM
    | t = DELAY_KEY_WRITE_SYM
    | t = DES_KEY_FILE
    | t = DIAGNOSTICS_SYM
    | t = DIRECTORY_SYM
    | t = DISABLE_SYM
    | t = DISCARD
    | t = DISK_SYM
    | t = DUMPFILE
    | t = DUPLICATE_SYM
    | t = DYNAMIC_SYM
    | t = ENDS_SYM
    | t = ENUM
    | t = ENGINE_SYM
    | t = ENGINES_SYM
    | t = ERROR_SYM
    | t = ERRORS
    | t = ESCAPE_SYM
    | t = EVENT_SYM
    | t = EVENTS_SYM
    | t = EVERY_SYM
    | t = EXCHANGE_SYM
    | t = EXPANSION_SYM
    | t = EXPIRE_SYM
    | t = EXPORT_SYM
    | t = EXTENDED_SYM
    | t = EXTENT_SIZE_SYM
    | t = FAULTS_SYM
    | t = FAST_SYM
    | t = FOUND_SYM
    | t = ENABLE_SYM
    | t = FULL
    | t = FILE_SYM
    | t = FILE_BLOCK_SIZE_SYM
    | t = FILTER_SYM
    | t = FIRST_SYM
    | t = FIXED_SYM
    | t = GENERAL
    | t = GEOMETRY_SYM
    | t = GEOMETRYCOLLECTION
    | t = GET_FORMAT
    | t = GRANTS
    | t = GLOBAL_SYM
    | t = HASH_SYM
    | t = HOSTS_SYM
    | t = HOUR_SYM
    | t = IDENTIFIED_SYM
    | t = IGNORE_SERVER_IDS_SYM
    | t = INVOKER_SYM
    | t = IMPORT
    | t = INDEXES
    | t = INITIAL_SIZE_SYM
    | t = INSTANCE_SYM
    | t = IO_SYM
    | t = IPC_SYM
    | t = ISOLATION
    | t = ISSUER_SYM
    | t = INSERT_METHOD
    | t = JSON_SYM
    | t = KEY_BLOCK_SIZE
    | t = LAST_SYM
    | t = LEAVES
    | t = LESS_SYM
    | t = LEVEL_SYM
    | t = LINESTRING
    | t = LIST_SYM
    | t = LOCAL_SYM
    | t = LOCKS_SYM
    | t = LOGFILE_SYM
    | t = LOGS_SYM
    | t = MAX_ROWS
    | t = MASTER_SYM
    | t = MASTER_HEARTBEAT_PERIOD_SYM
    | t = MASTER_HOST_SYM
    | t = MASTER_PORT_SYM
    | t = MASTER_LOG_FILE_SYM
    | t = MASTER_LOG_POS_SYM
    | t = MASTER_USER_SYM
    | t = MASTER_PASSWORD_SYM
    | t = MASTER_SERVER_ID_SYM
    | t = MASTER_CONNECT_RETRY_SYM
    | t = MASTER_RETRY_COUNT_SYM
    | t = MASTER_DELAY_SYM
    | t = MASTER_SSL_SYM
    | t = MASTER_SSL_CA_SYM
    | t = MASTER_SSL_CAPATH_SYM
    | t = MASTER_TLS_VERSION_SYM
    | t = MASTER_SSL_CERT_SYM
    | t = MASTER_SSL_CIPHER_SYM
    | t = MASTER_SSL_CRL_SYM
    | t = MASTER_SSL_CRLPATH_SYM
    | t = MASTER_SSL_KEY_SYM
    | t = MASTER_AUTO_POSITION_SYM
    | t = MAX_CONNECTIONS_PER_HOUR
    | t = MAX_QUERIES_PER_HOUR
    | t = MAX_SIZE_SYM
    | t = MAX_UPDATES_PER_HOUR
    | t = MAX_USER_CONNECTIONS_SYM
    | t = MEDIUM_SYM
    | t = MEMORY_SYM
    | t = MERGE_SYM
    | t = MESSAGE_TEXT_SYM
    | t = MICROSECOND_SYM
    | t = MIGRATE_SYM
    | t = MINUTE_SYM
    | t = MIN_ROWS
    | t = MODIFY_SYM
    | t = MODE_SYM
    | t = MONTH_SYM
    | t = MULTILINESTRING
    | t = MULTIPOINT
    | t = MULTIPOLYGON
    | t = MUTEX_SYM
    | t = MYSQL_ERRNO_SYM
    | t = NAME_SYM
    | t = NAMES_SYM
    | t = NATIONAL_SYM
    | t = NCHAR_SYM
    | t = NDBCLUSTER_SYM
    | t = NEVER_SYM
    | t = NEXT_SYM
    | t = NEW_SYM
    | t = NO_WAIT_SYM
    | t = NODEGROUP_SYM
    | t = NONE_SYM
    | t = NUMBER_SYM
    | t = NVARCHAR_SYM
    | t = OFFSET_SYM
    | t = ONE_SYM
    | t = ONLY_SYM
    | t = PACK_KEYS_SYM
    | t = PAGE_SYM
    | t = PARTIAL
    | t = PARTITIONING_SYM
    | t = PARTITIONS_SYM
    | t = PASSWORD
    | t = PHASE_SYM
    | t = PLUGIN_DIR_SYM
    | t = PLUGIN_SYM
    | t = PLUGINS_SYM
    | t = POINT_SYM
    | t = POLYGON
    | t = PRESERVE_SYM
    | t = PREV_SYM
    | t = PRIVILEGES
    | t = PROCESS
    | t = PROCESSLIST_SYM
    | t = PROFILE_SYM
    | t = PROFILES_SYM
    | t = PROXY_SYM
    | t = QUARTER_SYM
    | t = QUERY_SYM
    | t = QUICK
    | t = READ_ONLY_SYM
    | t = REBUILD_SYM
    | t = RECOVER_SYM
    | t = REDO_BUFFER_SIZE_SYM
    | t = REDOFILE_SYM
    | t = REDUNDANT_SYM
    | t = RELAY
    | t = RELAYLOG_SYM
    | t = RELAY_LOG_FILE_SYM
    | t = RELAY_LOG_POS_SYM
    | t = RELAY_THREAD
    | t = RELOAD
    | t = REORGANIZE_SYM
    | t = REPEATABLE_SYM
    | t = REPLICATION
    | t = REPLICATE_DO_DB
    | t = REPLICATE_IGNORE_DB
    | t = REPLICATE_DO_TABLE
    | t = REPLICATE_IGNORE_TABLE
    | t = REPLICATE_WILD_DO_TABLE
    | t = REPLICATE_WILD_IGNORE_TABLE
    | t = REPLICATE_REWRITE_DB
    | t = RESOURCES
    | t = RESUME_SYM
    | t = RETURNED_SQLSTATE_SYM
    | t = RETURNS_SYM
    | t = REVERSE_SYM
    | t = ROLLUP_SYM
    | t = ROTATE_SYM
    | t = ROUTINE_SYM
    | t = ROWS_SYM
    | t = ROW_COUNT_SYM
    | t = ROW_FORMAT_SYM
    | t = ROW_SYM
    | t = RTREE_SYM
    | t = SCHEDULE_SYM
    | t = SCHEMA_NAME_SYM
    | t = SECOND_SYM
    | t = SERIAL_SYM
    | t = SERIALIZABLE_SYM
    | t = SESSION_SYM
    | t = SIMPLE_SYM
    | t = SHARE_SYM
    | t = SLOW
    | t = SNAPSHOT_SYM
    | t = SOUNDS_SYM
    | t = SOURCE_SYM
    | t = SQL_AFTER_GTIDS
    | t = SQL_AFTER_MTS_GAPS
    | t = SQL_BEFORE_GTIDS
    | t = SQL_CACHE_SYM
    | t = SQL_BUFFER_RESULT
    | t = SQL_NO_CACHE_SYM
    | t = SQL_THREAD
    | t = STACKED_SYM
    | t = STARTS_SYM
    | t = STATS_AUTO_RECALC_SYM
    | t = STATS_PERSISTENT_SYM
    | t = STATS_SAMPLE_PAGES_SYM
    | t = STATUS_SYM
    | t = STORAGE_SYM
    | t = STRING_SYM
    | t = SUBCLASS_ORIGIN_SYM
    | t = SUBDATE_SYM
    | t = SUBJECT_SYM
    | t = SUBPARTITION_SYM
    | t = SUBPARTITIONS_SYM
    | t = SUPER_SYM
    | t = SUSPEND_SYM
    | t = SWAPS_SYM
    | t = SWITCHES_SYM
    | t = TABLE_NAME_SYM
    | t = TABLES
    | t = TABLE_CHECKSUM_SYM
    | t = TABLESPACE_SYM
    | t = TEMPORARY
    | t = TEMPTABLE_SYM
    | t = TEXT_SYM
    | t = THAN_SYM
    | t = TRANSACTION_SYM
    | t = TRIGGERS_SYM
    | t = TIMESTAMP
    | t = TIMESTAMP_ADD
    | t = TIMESTAMP_DIFF
    | t = TIME_SYM
    | t = TYPES_SYM
    | t = TYPE_SYM
    //        | t=UDF_RETURNS_SYM

    | t = FUNCTION_SYM
    | t = UNCOMMITTED_SYM
    | t = UNDEFINED_SYM
    | t = UNDO_BUFFER_SIZE_SYM
    | t = UNDOFILE_SYM
    | t = UNKNOWN_SYM
    | t = UNTIL_SYM
    | t = USER
    | t = USE_FRM
    | t = VALIDATION_SYM
    | t = VARIABLES
    | t = VIEW_SYM
    | t = VALUE_SYM
    | t = WARNINGS
    | t = WAIT_SYM
    | t = WEEK_SYM
    | t = WITHOUT_SYM
    | t = WORK_SYM
    | t = WEIGHT_STRING_SYM
    | t = X509_SYM
    | t = XID_SYM
    | t = XML_SYM
    | t = YEAR_SYM
;
