/* 
 * Completely rewritten from the sqlite-parser of Bart Kiers (https://github.com/bkiers/sqlite-parser)
 */
parser grammar SQLiteParser;

options {
	tokenVocab = SQLiteLexer;
	language = Java;
	superClass = SQLParser;
}

@header {
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.builder.*;
import sqlinspect.sql.asg.clause.*;
import sqlinspect.sql.asg.common.*;
import sqlinspect.sql.asg.expr.*;
import sqlinspect.sql.asg.schema.*;
import sqlinspect.sql.asg.statm.*;
import sqlinspect.sql.asg.type.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
}

@members {
    private static final Logger logger = LoggerFactory.getLogger(SQLiteParser.class);

    private SQLiteParserActions act = new SQLiteParserActions(this);
    
    {
        setErrorHandler(new SQLErrorStrategy(SQLiteLexer.SEMICOLON));
    }
    
    // a bit of a hack, see https://github.com/antlr/antlr4/issues/1133
    public SQLiteParser(TokenStream input, ASG asg) {
    	super(input, asg);
    	_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
    }
    
    
    @Override
    public SQLDialect getDialect() {
    	return SQLDialect.SQLITE;
    }
    
    @Override
    protected SQLParserActions getActions() {
    	return act;
    }
    
    @Override
    public List<Statement> parseStatements() {
    	sqlErrorListener.clear();
        return start().ret;
    }
}

//------------------------------------------------------------------
// PARSER RULES
//------------------------------------------------------------------

start returns [List<Statement> ret]
@init { $ret = new ArrayList<Statement>(); }
:
	(
		stmt = statement
		{ $ret.add($stmt.ret); }

	)* EOF
;

statement returns [Statement ret] @init {
    int lastErrorCount = 0;
    Statement s = null;
    $ret = null;
 }
:
	(
		K_EXPLAIN
		(
			K_QUERY K_PLAN
		)?
	)?
	(
		alter = alter_table_stmt
		{ s = $alter.ret; }

		| analyze_stmt
		| attach_stmt
		| begin_stmt
		| commit_stmt
		| create_index_stmt
		| create = create_table_stmt
		{ s = $create.ret; }

		| create_trigger_stmt
		| create_view_stmt
		| create_virtual_table_stmt
		| delete = delete_stmt
		{ s = $delete.ret; }

		| detach_stmt
		| drop_index_stmt
		| drop = drop_table_stmt
		{ s = $drop.ret; }

		| drop_trigger_stmt
		| drop_view = drop_view_stmt
		{ s = $drop_view.ret; }

		| insert = insert_stmt
		{ s = $insert.ret; }

		| pragma = pragma_stmt
		{ s = $pragma.ret; }
		
		| reindex_stmt
		| release_stmt
		| rollback_stmt
		| savepoint_stmt
		| select = select_stmt
		{ s = $select.ret; }

		| update = update_stmt
		{ s = $update.ret; }

		| vacuum_stmt
	) t = SEMICOLON
	| t = SEMICOLON
;

catch [ RecognitionException re ] {
    _localctx.exception = re;
    _errHandler.reportError(this, re);
    _errHandler.recover(this, re);
    setStmtError(true);
    setStmtErrorDetails(sqlErrorListener.getLast());
    logger.debug("LAST ERROR: " + (sqlErrorListener.getLast() == null ? "NULL" : sqlErrorListener.getLast().toString()));
} catch [ASGException e] {
    SQLErrorStrategy.consumeUntil(this, SQLiteLexer.SEMICOLON);
    logger.error("ASGException while parsing statement: ", e);
    setStmtError(true);
    setStmtErrorDetails(new SQLError(0, 0, e.toString(), e));
} catch [Throwable tr] {
    SQLErrorStrategy.consumeUntil(this, SQLiteLexer.SEMICOLON);
    logger.error("Unexpected exception while parsing statement: ", tr);
    setStmtError(true);
    setStmtErrorDetails(new SQLError(0, 0, tr.toString(), tr));
} finally {
	if (s == null && isStmtError()) {
		s = new ParserErrorBuilder(asg).result();
	}
	
    if (s != null) {
    	if (isStmtError()) {
    		SQLError err = getStmtErrorDetails();
    		s.setError(true);
    		if (err != null) {
    			s.setErrorMessage(err.getMessage());
    			s.setErrorLine(err.getLine());
    			s.setErrorCol(err.getCharPositionInLine());
			
    		}
    	}
        act.finishNode(s, $t);
    }
    $ret = s;
    setStmtError(false);
}
alter_table_stmt returns [AlterTable ret]
@init { $ret = null; Table table = null; }
:
	t = K_ALTER K_TABLE tn = table_name_with_db
	{ $ret = act.buildAlterTable($t, $tn.ret); }

	(
		K_RENAME K_TO new_table_name
		| K_ADD K_COLUMN? column_def [table]
	)
;

analyze_stmt
:
	K_ANALYZE
	(
		database_name
		| table_or_index_name
		| database_name DOT table_or_index_name
	)?
;

attach_stmt
:
	K_ATTACH K_DATABASE? expr K_AS database_name
;

begin_stmt
:
	K_BEGIN
	(
		K_DEFERRED
		| K_IMMEDIATE
		| K_EXCLUSIVE
	)?
	(
		K_TRANSACTION transaction_name?
	)?
;

commit_stmt
:
	(
		K_COMMIT
		| K_END
	)
	(
		K_TRANSACTION transaction_name?
	)?
;

create_index_stmt
:
	K_CREATE K_UNIQUE? K_INDEX
	(
		K_IF K_NOT K_EXISTS
	)?
	(
		database_name DOT
	)? index_name K_ON table_name LPAREN indexed_column
	(
		COMMA indexed_column
	)* RPAREN
	(
		K_WHERE expr
	)?
;

create_table_stmt returns [CreateTable ret]
@init { $ret = null; boolean ifNotExists = false; boolean isTemporary = false; }
:
	t = K_CREATE
	(
		K_TEMP
		{ isTemporary = true; }

		| K_TEMPORARY
		{ isTemporary = true; }

	)? K_TABLE
	(
		K_IF K_NOT K_EXISTS
		{ ifNotExists = true; }

	)? tab = table_name_with_db
	{ $ret = act.buildCreateTable($t, $tab.ret, ifNotExists, false, isTemporary, null); }

	(
		column_def_list [$ret.getSchemaTable()]?
		(
			K_WITHOUT IDENTIFIER
		)?
		| K_AS select_stmt
	)
;

column_def_list [Table table] returns [ArrayList<Column> ret]
@init { $ret = null; }
:
	LPAREN col_def = column_def [table]
	{ $ret = new ArrayList<Column>(); }

	(
		COMMA col_def = column_def [table]
		{ $ret.add($column_def.ret); }

	)*
	(
		COMMA table_constraint
	)* RPAREN
;

create_trigger_stmt
:
	K_CREATE
	(
		K_TEMP
		| K_TEMPORARY
	)? K_TRIGGER
	(
		K_IF K_NOT K_EXISTS
	)?
	(
		database_name DOT
	)? trigger_name
	(
		K_BEFORE
		| K_AFTER
		| K_INSTEAD K_OF
	)?
	(
		K_DELETE
		| K_INSERT
		| K_UPDATE
		(
			K_OF column_name
			(
				COMMA column_name
			)*
		)?
	) K_ON
	(
		database_name DOT
	)? table_name
	(
		K_FOR K_EACH K_ROW
	)?
	(
		K_WHEN expr
	)? K_BEGIN
	(
		(
			update_stmt
			| insert_stmt
			| delete_stmt
			| select_stmt
		) ';'
	)+ K_END
;

create_view_stmt
:
	K_CREATE
	(
		K_TEMP
		| K_TEMPORARY
	)? K_VIEW
	(
		K_IF K_NOT K_EXISTS
	)?
	(
		database_name DOT
	)? view_name K_AS select_stmt
;

create_virtual_table_stmt
:
	K_CREATE K_VIRTUAL K_TABLE
	(
		K_IF K_NOT K_EXISTS
	)?
	(
		database_name DOT
	)? table_name K_USING module_name
	(
		LPAREN module_argument
		(
			COMMA module_argument
		)* RPAREN
	)?
;

delete_stmt returns [Delete ret]
:
	with_clause? dt = K_DELETE K_FROM from = qualified_table_name
	{ $ret = act.buildDelete($dt, $from.ret); }

	(
		K_WHERE e = expr
		{ act.deleteSetWhere($ret, $e.ret); }

	)?
	(
		o = orderby_clause
		{ act.deleteSetOrder($ret, $o.ret); }

	)?
	(
		l = limit_clause
		{ act.deleteSetLimit($ret, $l.ret); }

	)?
;

detach_stmt
:
	K_DETACH K_DATABASE? database_name
;

drop_index_stmt
:
	K_DROP K_INDEX
	(
		K_IF K_EXISTS
	)?
	(
		database_name DOT
	)? index_name
;

drop_table_stmt returns [ DropTable ret]
@init { $ret = null; boolean ifExists = false; }
:
	t = K_DROP K_TABLE
	(
		K_IF K_EXISTS
		{ ifExists = true; }

	)? table = table_name_with_db
	{ $ret = act.buildDropTable($t, $table.ret, ifExists, false, false); }

;

drop_trigger_stmt
:
	K_DROP K_TRIGGER
	(
		K_IF K_EXISTS
	)?
	(
		database_name DOT
	)? trigger_name
;

drop_view_stmt returns [ DropView ret]
@init { $ret = null; boolean ifExists = false; }
:
	t = K_DROP K_VIEW
	(
		K_IF K_EXISTS
		{ ifExists = true; }

	)? view = view_name_with_db
	{ $ret = act.buildDropView($t, $view.ret, ifExists); }

;

insert_stmt returns [Insert ret]
:
	with_clause?
	(
		t = K_INSERT
		| t = K_REPLACE
		| t = K_INSERT K_OR K_REPLACE
		| t = K_INSERT K_OR K_ROLLBACK
		| t = K_INSERT K_OR K_ABORT
		| t = K_INSERT K_OR K_FAIL
		| t = K_INSERT K_OR K_IGNORE
	) K_INTO table = table_name_with_db
	{ $ret = act.buildInsert($t, false, $table.ret, null); }

	(
		LPAREN cl = column_name_list RPAREN
		{ act.insertSetColumnList($ret, $cl.ret); }

	)?
	(
		values = values_expr
		{ act.insertSetQuery($ret, $values.ret); }

		| query = binary_query
		{ act.insertSetQuery($ret, $query.ret); }

		| K_DEFAULT K_VALUES
	)
;

pragma_name_with_db returns [Expression ret]
:
	dn = database_name t1 = DOT pn = pragma_name
	{ $ret = act.buildBinaryExpression($t1, $dn.ret, $pn.ret); }

	| pn = pragma_name
	{ $ret = $pn.ret; }

;


pragma_stmt returns [Pragma ret]
:
	p = K_PRAGMA
	pn = pragma_name_with_db
	{ $ret = act.buildPragma($p, $pn.ret); }
	
	(
		'=' pv = pragma_value
		{ act.pragmaSetValue($ret, $pv.ret); }
				
		| LPAREN pv2 = pragma_value RPAREN
		{ act.pragmaSetValue($ret, $pv2.ret); }
		
	)?
;

reindex_stmt
:
	K_REINDEX
	(
		collation_name
		|
		(
			database_name DOT
		)?
		(
			table_name
			| index_name
		)
	)?
;

release_stmt
:
	K_RELEASE K_SAVEPOINT? savepoint_name
;

rollback_stmt
:
	K_ROLLBACK
	(
		K_TRANSACTION transaction_name?
	)?
	(
		K_TO K_SAVEPOINT? savepoint_name
	)?
;

savepoint_stmt
:
	K_SAVEPOINT savepoint_name
;

select_stmt returns [Select ret]
:
	q = binary_query
	{ $ret = act.buildSelect($q.ret); }

;

binary_query returns [Query ret]
:
	with_clause? sel1 = select_or_values
	{ $ret = $sel1.ret; }

	(
		op = compound_operator sel2 = select_or_values
		{ $ret = act.buildBinaryQuery($op.ret1, $op.ret2, $ret, $sel2.ret); }

	)*
	(
		order = orderby_clause
		{ act.querySetOrderBy($ret, $order.ret); }

	)?
	(
		limit = limit_clause
		{ act.querySetLimit($ret, $limit.ret); }

	)?
;

orderby_clause returns [ExpressionList ret]
:
	t = K_ORDER K_BY ot1 = ordering_term
	{ $ret = act.buildExpressionList($ot1.ret); }

	(
		COMMA ot2 = ordering_term
		{ act.addToExpressionList($ret, $ot2.ret); }

	)*
;

limit_clause returns [Limit ret]
:
	l = K_LIMIT
	(
		e1 = expr COMMA e2 = expr
		{ $ret = act.buildLimit($l, $e2.ret, $e1.ret); }

		| e1 = expr K_OFFSET e2 = expr
		{ $ret = act.buildLimit($l, $e1.ret, $e2.ret); }

		| e1 = expr
		{ $ret = act.buildLimit($l, $e1.ret, null); }

	)
;

select_or_values returns [Query ret]
:
	sel = select_expr
	{ $ret = $sel.ret; }

	| val = values_expr
	{ $ret = $val.ret; }

;

select_expr returns [SelectExpression ret]
:
	st = K_SELECT dt = distinct_or_all? fl = select_field_list
	{ $ret = act.buildSelectExpression($st, $dt.ctx==null ? null : $dt.ret, $fl.ret); }

	(
		K_FROM from_expr = select_table_ref_list
		{ act.selectExpressionSetFrom($ret, $from_expr.ret); }

	)?
	(
		K_WHERE where_expr = expr
		{ act.selectExpressionSetWhere($ret, $where_expr.ret); }

	)?
	(
		K_GROUP K_BY el = expression_list
		{ act.selectExpressionSetGroupBy($ret, $el.ret); }

		(
			K_HAVING e = expr
			{ act.selectExpressionSetHaving($ret, $e.ret); }

		)?
	)?
;

values_expr returns [Values ret]
:
	t = K_VALUES vol = values_operand_list
	{ $ret = act.buildValues($t, $vol.ret); }

;

distinct_or_all returns [Token ret]
:
	dt = K_DISTINCT
	{ $ret = $dt; }

	| dt = K_ALL
	{ $ret = $dt; }

;

values_operand_list returns [ExpressionList ret]
@init { $ret = null; boolean listed = false; }
:
	LPAREN el1 = expression_list RPAREN
	{ $ret = $el1.ret; }

	(
		COMMA LPAREN el2 = expression_list RPAREN
		{
         if (listed == false) {
           $ret = act.buildExpressionList($ret);
           listed = true;
         }
         act.addToExpressionList($ret, $el2.ret);
        }

	)*
;

expression_list returns [ExpressionList ret]
:
	e1 = expr
	{ $ret = act.buildExpressionList($e1.ret); }

	(
		COMMA e2 = expr
		{ act.addToExpressionList($ret, $e2.ret); }

	)*
;

select_field_list returns [ExpressionList ret]
:
	rc1 = result_column
	{ $ret = act.buildExpressionList($rc1.ret); }

	(
		COMMA rc2 = result_column
		{ act.addToExpressionList($ret, $rc2.ret); }

	)*
;

select_table_ref_list returns [Expression ret] @init { $ret = null; }
:
	tab = table_or_subquery
	{ $ret = $tab.ret; }

	(
		t = COMMA tab2 = table_or_subquery
		{ $ret = act.buildJoin($ret, $tab2.ret, $t); }

	)*
	| jc = join_clause
	{ $ret = $jc.ret; }

;

update_stmt returns [Update ret]
:
	with_clause? t = K_UPDATE
	(
		K_OR K_ROLLBACK
		| K_OR K_ABORT
		| K_OR K_REPLACE
		| K_OR K_FAIL
		| K_OR K_IGNORE
	)? tn = qualified_table_name K_SET values = update_set_expr_list
	{ $ret = act.buildUpdate($t, $tn.ret, $values.ret); }

	(
		K_WHERE e = expr
		{ act.updateSetWhere($ret, $e.ret); }

	)?
	(
		o = orderby_clause
		{ act.updateSetOrderBy($ret, $o.ret); }

	)?
	(
		l = limit_clause
		{ act.updateSetLimit($ret, $l.ret); }

	)?
;

update_set_expr_list returns [ExpressionList ret]
:
	cn = column_name t = ASSIGN e = expr
	{ $ret = act.buildExpressionList(act.buildBinaryExpression($t, $cn.ret, $e.ret)); }

	(
		COMMA cn = column_name t = ASSIGN e = expr
		{ act.addToExpressionList($ret, act.buildBinaryExpression($t, $cn.ret, $e.ret)); }

	)*
;

vacuum_stmt
:
	K_VACUUM
;

column_def [Table table] returns [Column ret] @init { $ret = null; }
:
	cn = column_name t1 = type_name? cl = column_constraint_list?
	{ $ret = act.newColumn(table, $cn.ret, $t1.ctx != null ? $t1.ret : null, $cl.ctx != null ? $cl.ret : null); }

;

column_constraint_list returns [List<ColumnAttribute> ret]
@init { $ret = new ArrayList<ColumnAttribute>(); }
:
	cc = column_constraint
	{ $ret.add($cc.ret); }

	(
		cc = column_constraint
		{ $ret.add($cc.ret); }

	)*
;

type_name returns [Type ret] @init { $ret = null; }
:
	t = name tp = type_param?
	{ $ret = act.newSQLiteType($t.ret, $tp.ctx==null ? null : $tp.ret1, $tp.ctx==null ? null : $tp.ret2); }

;

type_param returns [Expression ret1, Expression ret2]
:
	LPAREN sn1 = signed_number RPAREN
	{ $ret1 = $sn1.ret; }

	| LPAREN sn1 = signed_number COMMA sn2 = signed_number RPAREN
	{ $ret1 = $sn1.ret; $ret2=$sn2.ret;}

;

column_constraint returns [ColumnAttribute ret]
:
	(
		K_CONSTRAINT name
	)?
	(
		t1 = K_PRIMARY K_KEY
		(
			K_ASC
			| K_DESC
		)? conflict_clause? K_AUTOINCREMENT?
		{ $ret = act.buildColumnAttribute($t1); }

		| tn = K_NOT t1 = K_NULL conflict_clause?
		{ $ret = act.buildColumnAttribute($t1, $tn); }

		| t1 = K_NULL conflict_clause?
		{ $ret = act.buildColumnAttribute($t1); }

		| t1 = K_UNIQUE conflict_clause?
		{ $ret = act.buildColumnAttribute($t1); }

		| t1 = K_CHECK LPAREN ex = expr RPAREN
		{ $ret = act.buildColumnAttribute($t1, $ex.ret); }

		| t1 = K_DEFAULT
		(
			sn = signed_number
			{ $ret = act.buildColumnAttribute($t1, $sn.ret); }

			| lv = literal_value
			{ $ret = act.buildColumnAttribute($t1, $lv.ret); }

			| LPAREN ex = expr RPAREN
			{ $ret = act.buildColumnAttribute($t1, $ex.ret); }

		)
		| K_COLLATE collation_name
		| foreign_key_clause
	)
;

conflict_clause
:
	K_ON K_CONFLICT
	(
		K_ROLLBACK
		| K_ABORT
		| K_FAIL
		| K_IGNORE
		| K_REPLACE
	)
;

/*
    SQLite understands the following binary operators, in order from highest to
    lowest precedence:

    ||
    *    /    %
    +    -
    <<   >>   &    |
    <    <=   >    >=
    =    ==   !=   <>   IS   IS NOT   IN   LIKE   GLOB   MATCH   REGEXP
    AND
    OR
*/
expr returns [Expression ret]
:
	lv = literal_value
	{ $ret = $lv.ret; }
	//	| BIND_PARAMETER

	|
	(
		(
			dn = database_name t1 = DOT
		)? tn = table_name t2 = DOT
		{ if ($dn.ctx!=null) $ret=act.buildBinaryExpression($t1, $dn.ret, $tn.ret);
			else $ret = $tn.ret;
		}

	)? cn = column_name
	{ if ($tn.ctx!=null) $ret=act.buildBinaryExpression($t2, $tn.ret, $cn.ret);
		else $ret = $cn.ret;
	}

	| uo = unary_operator e1 = expr
	{ $ret = act.buildUnaryExpression($uo.ret, $e1.ret); }

	| e1 = expr t = PIPE2 e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr
	(
		t = ASTERISK
		| t = DIV
		| t = MOD
	) e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr
	(
		t = PLUS
		| t = MINUS
	) e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr
	(
		t = LT2
		| t = GT2
		| t = AMP
		| t = PIPE
		| t = XOR
	) e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr
	(
		t = LT
		| t = LT_EQ
		| t = GT
		| t = GT_EQ
	) e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr
	(
		t = ASSIGN
		| t = EQ
		| t = NOT_EQ1
		| t = NOT_EQ2
	) e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr t = K_IN sq = in_subquery
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $sq.ret); }

	| e1 = expr tnot = K_NOT t = K_IN sq = in_subquery
	{ $ret = act.buildUnaryExpression($tnot, act.buildBinaryExpression($t, $e1.ret, $sq.ret)); }

	| e1 = expr t = K_AND e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr t = K_OR e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| fn = function_name LPAREN
	{ $ret = act.buildInnerFunctionCall($fn.ret); }

	(
		tdist = K_DISTINCT el = expression_list
		{ act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($tdist, null, $el.ret)); }

		| el = expression_list
		{ act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($el.ret)); }

		| a = ASTERISK
		{ act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams(act.buildAsterisk($a))); }

	)? RPAREN
	| LPAREN e1 = expr RPAREN
	{ $ret = $e1.ret; }

	| t = K_CAST LPAREN e1 = expr K_AS to = type_name RPAREN
	{ $ret = act.buildCast($t, $e1.ret, $to.ret); }

	| e1 = expr t = K_COLLATE collation_name // TODO: handle in AST

	| e1 = expr tnot = K_NOT
	(
		t = K_LIKE
		| t = K_GLOB
		| t = K_REGEXP
		| t = K_MATCH
	) e2 = expr
	{ $ret = act.buildUnaryExpression($tnot, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

	| e1 = expr
	(
		t = K_LIKE
		| t = K_GLOB
		| t = K_REGEXP
		| t = K_MATCH
	) e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	(
		K_ESCAPE expr
	)? // TODO: handle in AST

	| e1 = expr
	(
		t1 = K_ISNULL
		{ $ret = act.buildIsNull($e1.ret, false, $t1, null); }

		| t1 = K_NOTNULL
		{ $ret = act.buildIsNull($e1.ret, true, $t1, null); }

		| t1 = K_NOT t2 = K_NULL
		{ $ret = act.buildIsNull($e1.ret, true, $t1, $t2); }

		| t1 = K_IS t2 = K_NULL
		{ $ret = act.buildIsNull($e1.ret, false, $t1, $t2); }

		| t1 = K_IS K_NOT t2 = K_NULL
		{ $ret = act.buildIsNull($e1.ret, true, $t1, $t2); }

	)
	| e1 = expr t = K_IS tnot = K_NOT e2 = expr
	{ $ret = act.buildUnaryExpression($tnot, act.buildBinaryExpression($t, $e1.ret, $e2.ret)); }

	| e1 = expr t = K_IS e2 = expr
	{ $ret = act.buildBinaryExpression($t, $e1.ret, $e2.ret); }

	| e1 = expr t = K_BETWEEN e2 = expr and = K_AND e3 = expr
	{ $ret = act.buildBetween($t, $e1.ret, $e2.ret, $e3.ret, false); }

	| e1 = expr tnot = K_NOT t = K_BETWEEN e2 = expr and = K_AND e3 = expr
	{ $ret = act.buildBetween($t, $e1.ret, $e2.ret, $e3.ret, true); }

	| tnot = K_NOT t = K_EXISTS LPAREN s = binary_query RPAREN
	{ $ret = act.buildUnaryExpression($tnot,act.buildUnaryExpression($t, $s.ret)); }

	| t = K_EXISTS LPAREN s = binary_query RPAREN
	{ $ret = act.buildUnaryExpression($t, $s.ret); }

	| LPAREN s = binary_query RPAREN
	{ $ret = $s.ret; }

	| t = K_CASE e = expr?
	{ $ret = act.buildCase($t, $e.ctx == null ? null : $e.ret); }

	whenlist = when_list
	{ act.caseAddWhenClauseList((Case)$ret, $whenlist.ret); }

	(
		K_ELSE elseexpr = expr
		{ act.caseSetElse((Case)$ret, $elseexpr.ret); }

	)? K_END
	| rf = raise_function
	{ $ret = $rf.ret; }

;

in_subquery returns [Expression ret]
:
	LPAREN
	(
		se = binary_query
		{ $ret = $se.ret; }

		| el = expression_list
		{ $ret = $el.ret; }

	)? RPAREN
	| dn = database_name t2 = DOT tn = table_name
	{ $ret = act.buildBinaryExpression($t2, $dn.ret, $tn.ret); }

	| tn = table_name
	{ $ret = $tn.ret; }

;

when_list returns [List<WhenClause> ret]
@init { $ret = new ArrayList<WhenClause>(); }
:
	t = K_WHEN e1 = expr K_THEN e2 = expr
	{ $ret.add(act.buildWhenClause($t, $e1.ret, $e2.ret));}

	(
		t = K_WHEN e1 = expr K_THEN e2 = expr
		{ $ret.add(act.buildWhenClause($t, $e1.ret, $e2.ret));}

	)*
;

foreign_key_clause
:
	K_REFERENCES foreign_table
	(
		LPAREN column_name
		(
			COMMA column_name
		)* RPAREN
	)?
	(
		(
			K_ON
			(
				K_DELETE
				| K_UPDATE
			)
			(
				K_SET K_NULL
				| K_SET K_DEFAULT
				| K_CASCADE
				| K_RESTRICT
				| K_NO K_ACTION
			)
			| K_MATCH name
		)
	)*
	(
		K_NOT? K_DEFERRABLE
		(
			K_INITIALLY K_DEFERRED
			| K_INITIALLY K_IMMEDIATE
		)?
	)?
;

raise_function returns [InnerFunctionCall ret]
:
	t = K_RAISE LPAREN
	{ $ret = act.buildInnerFunctionCall(act.buildId($t)); }

	(
		K_IGNORE
		|
		(
			K_ROLLBACK
			| K_ABORT
			| K_FAIL
		) COMMA error_message
	) RPAREN
;

indexed_column
:
	(
		column_name
		| expr
	)
	(
		K_COLLATE collation_name
	)?
	(
		K_ASC
		| K_DESC
	)?
;

table_constraint
:
	(
		K_CONSTRAINT name
	)?
	(
		(
			K_PRIMARY K_KEY
			| K_UNIQUE
		) LPAREN indexed_column
		(
			COMMA indexed_column
		)* RPAREN conflict_clause?
		| K_CHECK LPAREN expr RPAREN
		| K_FOREIGN K_KEY LPAREN column_name
		(
			COMMA column_name
		)* RPAREN foreign_key_clause
	)
;

with_clause
:
	K_WITH K_RECURSIVE? common_table_expression
	(
		COMMA common_table_expression
	)*
;

qualified_table_name returns [Expression ret]
:
	(
		dn = database_name t1 = DOT tn = table_name
		{ $ret=act.buildBinaryExpression($t1, $dn.ret, $tn.ret); }

		| tn = table_name
		{ $ret = $tn.ret; }

	)
	(
		K_INDEXED K_BY index_name
		| K_NOT K_INDEXED
	)?
;

ordering_term returns [OrderByElement ret] @init { $ret = null; }
:
	e = expr
	{ $ret = act.buildOrderByElement($e.ret); }

	(
		K_COLLATE collation_name
	)?
	(
		(
			t = K_ASC
			| t = K_DESC
		)
		{ act.orderByElementSetKind($ret, $t); }

	)?
;

pragma_value returns [Expression ret]
:
	sn = signed_number
	{ $ret = $sn.ret; }
	
	| n = name
	{ $ret = $n.ret; }
	
	| sl = STRING_LITERAL
	{ $ret = act.buildLiteral($sl); }
;

common_table_expression
:
	table_name
	(
		LPAREN column_name
		(
			COMMA column_name
		)* RPAREN
	)? K_AS LPAREN select_stmt RPAREN
;

result_column returns [Expression ret]
:
	a = ASTERISK
	{ $ret = act.buildAsterisk($a); }

	| tn = table_name dt = DOT a = ASTERISK
	{  $ret = act.buildBinaryExpression($dt, $tn.ret, act.buildAsterisk($a)); }

	| e = expr
	{ $ret = $e.ret; }

	(
		al = alias [$e.ret]
		{ $ret = $al.ret; }

	)?
;

alias [Expression left] returns [Alias ret]
:
	t = K_AS? ca = column_alias
	{ $ret = act.buildAlias($left, $ca.ret, $t); }

	| ca = column_alias
	{ $ret = act.buildAlias($left, $ca.ret, null); }

;

table_or_subquery returns [Expression ret]
:
	(
		sn = schema_name dt = DOT
	)? tn = table_name
	{ if ($sn.ctx!=null) {
		$ret = act.buildBinaryExpression($dt, $sn.ret, $tn.ret);
	  } else {
	  	$ret = $tn.ret;
	  }
	}

	(
		ta = table_alias [$ret]
		{ $ret = $ta.ret; }

	)?
	(
		K_INDEXED K_BY index_name
		| K_NOT K_INDEXED
	)? // TODO: add to ast

	|
	(
		sn = schema_name dt = DOT
	)? fn = table_function_name LPAREN
	{ if ($sn.ctx!=null)
		$ret = act.buildUserFunctionCall(act.buildBinaryExpression($dt, $sn.ret, $fn.ret));
	  else 
		$ret = act.buildUserFunctionCall($fn.ret);
	}

	(
		el = expression_list
		{ act.functionCallSetParamList((FunctionCall)$ret, act.buildFunctionParams($el.ret)); }

	)? RPAREN
	(
		ta = table_alias [$ret]
		{ $ret = $ta.ret; }

	)?
	| LPAREN
	(
		ts1 = table_or_subquery
		{ $ret = $ts1.ret; }

		(
			ct = COMMA ts2 = table_or_subquery
			{ $ret = act.buildJoin($ret, $ts2.ret, $ct, null, null); }

		)*
		| jc = join_clause
		{ $ret = $jc.ret; }

	) RPAREN
	| LPAREN bq = binary_query RPAREN
	{ $ret = $bq.ret; }

	(
		ta = table_alias [$ret]
		{ $ret = $ta.ret; }

	)?
;

join_clause returns [Join ret] @init { $ret = null; }
:
	left = table_or_subquery op = join_operator right = table_or_subquery jc =
	join_constraint?
	{ $ret = act.buildJoin($left.ret, $right.ret, $op.tok1, $op.tok2, $jc.ctx == null ? null : $jc.ret); }

	(
		op = join_operator right = table_or_subquery jc = join_constraint?
		{ $ret = act.buildJoin($ret, $right.ret, $op.tok1, $op.tok2, $jc.ctx == null ? null : $jc.ret); }

	)*
;

join_operator returns [Token tok1, Token tok2]
:
	t = COMMA
	{ $tok1 = $t; }

	|
	(
		t = K_NATURAL
		{ $tok1 = $t; }
	)?
	(
		t = K_LEFT
		{ $tok1 = $t; }

		(
			t2 = K_OUTER
			{ $tok2 = $t2; }

		)?
		| t = K_INNER
		{ $tok1 = $t; }

		| t = K_CROSS
		{ $tok1 = $t; }

	)? K_JOIN
;

join_constraint returns [JoinConditionClause ret]
:
	ton = K_ON e = expr
	{ $ret = act.buildOn($e.ret, $ton); }

	| tusing = K_USING LPAREN cn = column_names RPAREN
	{ $ret = act.buildUsing($cn.ret, $tusing); }

;

column_names returns [ExpressionList ret]
:
	cn1 = column_name
	{ $ret = act.buildExpressionList($cn1.ret); }

	(
		COMMA cn2 = column_name
		{ act.addToExpressionList($ret, $cn2.ret); }

	)*
;

compound_operator returns [Token ret1, Token ret2]
@init { $ret1 = null; $ret2 = null; }
:
	t1 = K_UNION
	{ $ret1 = $t1; }

	| t1 = K_UNION t2 = K_ALL
	{ $ret1 = $t1; $ret2 = $t2; }

	| t1 = K_INTERSECT
	{ $ret1 = $t1; }

	| t1 = K_EXCEPT
	{ $ret1 = $t1; }

;

signed_number returns [Expression ret]
:
	t = PLUS nl = numeric_literal_value
	{ $ret = act.addLiteralSign($t, $nl.ret); }

	| t = MINUS nl = numeric_literal_value
	{ $ret = act.addLiteralSign($t, $nl.ret); }

	| nl = numeric_literal_value
	{ $ret = $nl.ret; }

;

literal_value returns [Literal ret]
:
	nl = numeric_literal_value
	{ $ret = $nl.ret; }

	| sl = STRING_LITERAL
	{ $ret = act.buildLiteral($sl); }

	| bl = BLOB_LITERAL
	{ $ret = act.buildLiteral($bl); }

	| n = K_NULL
	{ $ret = act.buildLiteral($n); }

	| t = K_TRUE
	{ $ret = act.buildLiteral($t); }

	| f = K_FALSE
	{ $ret = act.buildLiteral($f); }

	| ct = K_CURRENT_TIME
	{ $ret = act.buildLiteral($ct); }

	| cd = K_CURRENT_DATE
	{ $ret = act.buildLiteral($cd); }

	| cts = K_CURRENT_TIMESTAMP
	{ $ret = act.buildLiteral($cts); }

;

/*
 * The SQLite syntax has a general numeric literal grouping together integer,
 * decimal and float literals.  
 */
numeric_literal_value returns [Literal ret]
:
	il = INTEGER_LITERAL
	{ $ret = act.buildLiteral($il); }

	| dl = DECIMAL_LITERAL
	{ $ret = act.buildLiteral($dl); }

	| fl = FLOATINGPOINT_LITERAL
	{ $ret = act.buildLiteral($fl); }

;

unary_operator returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
	t = MINUS
	| t = PLUS
	| t = TILDE
	| t = K_NOT
;

error_message
:
	STRING_LITERAL
;

module_argument
:
	expr
	| column_def [null]
;

column_alias returns [Expression ret]
:
	i = ident
	{ $ret = $i.ret; }

	| s = string_literal
	{ $ret = $s.ret; }

;

string_literal returns [Literal ret]
:
	t = STRING_LITERAL
	{ $ret = act.buildLiteral($t); }

;

ident returns [Expression ret]
:
	i = IDENTIFIER
	{ $ret = act.buildId($i); }

	| in = ID_UNRESOLVED
	{ $ret = act.buildJoker($in); }

;

keyword returns [Token ret] @init { $ret = null; }
@after { $ret = $t; }
:
	t = K_ABORT
	| t = K_ACTION
	| t = K_ADD
	| t = K_AFTER
	| t = K_ALL
	| t = K_ALTER
	| t = K_ANALYZE
	| t = K_AND
	| t = K_AS
	| t = K_ASC
	| t = K_ATTACH
	| t = K_AUTOINCREMENT
	| t = K_BEFORE
	| t = K_BEGIN
	| t = K_BETWEEN
	| t = K_BY
	| t = K_CASCADE
	| t = K_CASE
	| t = K_CAST
	| t = K_CHECK
	| t = K_COLLATE
	| t = K_COLUMN
	| t = K_COMMIT
	| t = K_CONFLICT
	| t = K_CONSTRAINT
	| t = K_CREATE
	| t = K_CROSS
	| t = K_CURRENT_DATE
	| t = K_CURRENT_TIME
	| t = K_CURRENT_TIMESTAMP
	| t = K_DATABASE
	| t = K_DEFAULT
	| t = K_DEFERRABLE
	| t = K_DEFERRED
	| t = K_DELETE
	| t = K_DESC
	| t = K_DETACH
	| t = K_DISTINCT
	| t = K_DROP
	| t = K_EACH
	| t = K_ELSE
	| t = K_END
	| t = K_ESCAPE
	| t = K_EXCEPT
	| t = K_EXCLUSIVE
	| t = K_EXISTS
	| t = K_EXPLAIN
	| t = K_FAIL
	| t = K_FALSE
	| t = K_FOR
	| t = K_FOREIGN
	| t = K_FROM
	| t = K_FULL
	| t = K_GLOB
	| t = K_GROUP
	| t = K_HAVING
	| t = K_IF
	| t = K_IGNORE
	| t = K_IMMEDIATE
	| t = K_IN
	| t = K_INDEX
	| t = K_INDEXED
	| t = K_INITIALLY
	| t = K_INNER
	| t = K_INSERT
	| t = K_INSTEAD
	| t = K_INTERSECT
	| t = K_INTO
	| t = K_IS
	| t = K_ISNULL
	| t = K_JOIN
	| t = K_KEY
	| t = K_LEFT
	| t = K_LIKE
	| t = K_LIMIT
	| t = K_MATCH
	| t = K_NATURAL
	| t = K_NO
	| t = K_NOT
	| t = K_NOTNULL
	| t = K_NULL
	| t = K_OF
	| t = K_OFF
	| t = K_OFFSET
	| t = K_ON
	| t = K_OR
	| t = K_ORDER
	| t = K_OUTER
	| t = K_PLAN
	| t = K_PRAGMA
	| t = K_PRIMARY
	| t = K_QUERY
	| t = K_RAISE
	| t = K_RECURSIVE
	| t = K_REFERENCES
	| t = K_REGEXP
	| t = K_REINDEX
	| t = K_RELEASE
	| t = K_RENAME
	| t = K_REPLACE
	| t = K_RESTRICT
	| t = K_RIGHT
	| t = K_ROLLBACK
	| t = K_ROW
	| t = K_SAVEPOINT
	| t = K_SELECT
	| t = K_SET
	| t = K_TABLE
	| t = K_TEMP
	| t = K_TEMPORARY
	| t = K_THEN
	| t = K_TO
	| t = K_TRANSACTION
	| t = K_TRUE
	| t = K_TRIGGER
	| t = K_UNION
	| t = K_UNIQUE
	| t = K_UPDATE
	| t = K_USING
	| t = K_YES
	| t = K_VACUUM
	| t = K_VALUES
	| t = K_VIEW
	| t = K_VIRTUAL
	| t = K_WHEN
	| t = K_WHERE
	| t = K_WITH
	| t = K_WITHOUT
;

name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

function_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

database_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

schema_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

table_function_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

table_name_with_db returns [Expression ret]
:
	dn = database_name t1 = DOT tn = table_name
	{ $ret=act.buildBinaryExpression($t1, $dn.ret, $tn.ret); }

	| tn = table_name
	{ $ret = $tn.ret; }

;

table_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

table_or_index_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

new_table_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

column_name_list returns [ExpressionList ret]
:
	cn = column_name
	{ $ret = act.buildExpressionList($cn.ret); }

	(
		COMMA cn = column_name
		{ act.addToExpressionList($ret, $cn.ret); }

	)*
;

column_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

collation_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

foreign_table returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

index_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

trigger_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

view_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

view_name_with_db returns [Expression ret]
:
	dn = database_name t1 = DOT tn = view_name
	{ $ret=act.buildBinaryExpression($t1, $dn.ret, $tn.ret); }

	| tn = view_name
	{ $ret = $tn.ret; }

;

module_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

pragma_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

savepoint_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

table_alias [Expression left] returns [Alias ret]
:
	t = K_AS i = ident
	{ $ret = act.buildAlias($left, $i.ret, $t); }

	| i = ident
	{ $ret = act.buildAlias($left, $i.ret, null); }

	| t = K_AS sl = string_literal
	{ $ret = act.buildAlias($left, $sl.ret, $t); }

	| sl = string_literal
	{ $ret = act.buildAlias($left, $sl.ret, null); }

	| t = K_AS LPAREN a = table_alias [$left] RPAREN
	{ $ret = act.buildAlias($left, $a.ret, $t); }

	| LPAREN a = table_alias [$left] RPAREN
	{ $ret = act.buildAlias($left, $a.ret, null); }

;

transaction_name returns [Expression ret]
:
	an = any_name
	{ $ret = $an.ret; }

;

any_name returns [Expression ret]
:
	i = ident
	{ $ret = $i.ret; }

	| kw = keyword
	{ $ret = act.buildId($kw.ret); }

	| s = STRING_LITERAL
	{ $ret = act.buildId($s); }

	| LPAREN an = any_name RPAREN
	{ $ret = $an.ret; }

;
