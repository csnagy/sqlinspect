/* 
 * Based on the sqlite-parser of Bart Kiers (https://github.com/bkiers/sqlite-parser)
 */
lexer grammar SQLiteLexer;

options {
  language = Java;
  superClass = SQLLexer;
}

//------------------------------------------------------------------
// LEXER RULES
//------------------------------------------------------------------

fragment DIGIT : [0-9];

fragment A_	: ('a'|'A');
fragment B_	: ('b'|'B');
fragment C_	: ('c'|'C');
fragment D_	: ('d'|'D');
fragment E_	: ('e'|'E');
fragment F_	: ('f'|'F');
fragment G_	: ('g'|'G');
fragment H_	: ('h'|'H');
fragment I_	: ('i'|'I');
fragment J_	: ('j'|'J');
fragment K_	: ('k'|'K');
fragment L_	: ('l'|'L');
fragment M_	: ('m'|'M');
fragment N_	: ('n'|'N');
fragment O_	: ('o'|'O');
fragment P_	: ('p'|'P');
fragment Q_	: ('q'|'Q');
fragment R_	: ('r'|'R');
fragment S_	: ('s'|'S');
fragment T_	: ('t'|'T');
fragment U_	: ('u'|'U');
fragment V_	: ('v'|'V');
fragment W_	: ('w'|'W');
fragment X_	: ('x'|'X');
fragment Y_	: ('y'|'Y');
fragment Z_	: ('z'|'Z');

COMMA: ',';
SEMICOLON : ';';
DOT : '.';
LPAREN : '(';
RPAREN : ')';
ASSIGN : '=';
ASTERISK : '*';
PLUS : '+';
MINUS : '-';
TILDE : '~';
PIPE2 : '||';
DIV : '/';
MOD : '%';
LT2 : '<<';
GT2 : '>>';
AMP : '&';
XOR : '^';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';

// http://www.sqlite.org/lang_keywords.html
K_ABORT: A_ B_ O_ R_ T_;
K_ACTION: A_ C_ T_ I_ O_ N_;
K_ADD: A_ D_ D_;
K_AFTER: A_ F_ T_ E_ R_;
K_ALL: A_ L_ L_;
K_ALTER: A_ L_ T_ E_ R_;
K_ANALYZE: A_ N_ A_ L_ Y_ Z_ E_;
K_AND: A_ N_ D_;
K_AS: A_ S_;
K_ASC: A_ S_ C_;
K_ATTACH: A_ T_ T_ A_ C_ H_;
K_AUTOINCREMENT: A_ U_ T_ O_ I_ N_ C_ R_ E_ M_ E_ N_ T_;
K_BEFORE: B_ E_ F_ O_ R_ E_;
K_BEGIN: B_ E_ G_ I_ N_;
K_BETWEEN: B_ E_ T_ W_ E_ E_ N_;
K_BY: B_ Y_;
K_CASCADE: C_ A_ S_ C_ A_ D_ E_;
K_CASE: C_ A_ S_ E_;
K_CAST: C_ A_ S_ T_;
K_CHECK: C_ H_ E_ C_ K_;
K_COLLATE: C_ O_ L_ L_ A_ T_ E_;
K_COLUMN: C_ O_ L_ U_ M_ N_;
K_COMMIT: C_ O_ M_ M_ I_ T_;
K_CONFLICT: C_ O_ N_ F_ L_ I_ C_ T_;
K_CONSTRAINT: C_ O_ N_ S_ T_ R_ A_ I_ N_ T_;
K_CREATE: C_ R_ E_ A_ T_ E_;
K_CROSS: C_ R_ O_ S_ S_;
K_CURRENT_DATE: C_ U_ R_ R_ E_ N_ T_ '_' D_ A_ T_ E_;
K_CURRENT_TIME: C_ U_ R_ R_ E_ N_ T_ '_' T_ I_ M_ E_;
K_CURRENT_TIMESTAMP: C_ U_ R_ R_ E_ N_ T_ '_' T_ I_ M_ E_ S_ T_ A_ M_ P_;
K_DATABASE: D_ A_ T_ A_ B_ A_ S_ E_;
K_DEFAULT: D_ E_ F_ A_ U_ L_ T_;
K_DEFERRABLE: D_ E_ F_ E_ R_ R_ A_ B_ L_ E_;
K_DEFERRED: D_ E_ F_ E_ R_ R_ E_ D_;
K_DELETE: D_ E_ L_ E_ T_ E_;
K_DESC: D_ E_ S_ C_;
K_DETACH: D_ E_ T_ A_ C_ H_;
K_DISTINCT: D_ I_ S_ T_ I_ N_ C_ T_;
K_DROP: D_ R_ O_ P_;
K_EACH: E_ A_ C_ H_;
K_ELSE: E_ L_ S_ E_;
K_END: E_ N_ D_;
K_ESCAPE: E_ S_ C_ A_ P_ E_;
K_EXCEPT: E_ X_ C_ E_ P_ T_;
K_EXCLUSIVE: E_ X_ C_ L_ U_ S_ I_ V_ E_;
K_EXISTS: E_ X_ I_ S_ T_ S_;
K_EXPLAIN: E_ X_ P_ L_ A_ I_ N_;
K_FAIL: F_ A_ I_ L_;
K_FALSE: F_ A_ L_ S_ E_;
K_FOR: F_ O_ R_;
K_FOREIGN: F_ O_ R_ E_ I_ G_ N_;
K_FROM: F_ R_ O_ M_;
K_FULL: F_ U_ L_ L_;
K_GLOB: G_ L_ O_ B_;
K_GROUP: G_ R_ O_ U_ P_;
K_HAVING: H_ A_ V_ I_ N_ G_;
K_IF: I_ F_;
K_IGNORE: I_ G_ N_ O_ R_ E_;
K_IMMEDIATE: I_ M_ M_ E_ D_ I_ A_ T_ E_;
K_IN: I_ N_;
K_INDEX: I_ N_ D_ E_ X_;
K_INDEXED: I_ N_ D_ E_ X_ E_ D_;
K_INITIALLY: I_ N_ I_ T_ I_ A_ L_ L_ Y_;
K_INNER: I_ N_ N_ E_ R_;
K_INSERT: I_ N_ S_ E_ R_ T_;
K_INSTEAD: I_ N_ S_ T_ E_ A_ D_;
//K_INTEGER: I_ N_ T_ E_ G_ E_ R_;
K_INTERSECT: I_ N_ T_ E_ R_ S_ E_ C_ T_;
K_INTO: I_ N_ T_ O_;
K_IS: I_ S_;
K_ISNULL: I_ S_ N_ U_ L_ L_;
K_JOIN: J_ O_ I_ N_;
K_KEY: K_ E_ Y_;
K_LEFT: L_ E_ F_ T_;
K_LIKE: L_ I_ K_ E_;
K_LIMIT: L_ I_ M_ I_ T_;
K_MATCH: M_ A_ T_ C_ H_;
K_NATURAL: N_ A_ T_ U_ R_ A_ L_;
K_NO: N_ O_;
K_NOT: N_ O_ T_;
K_NOTNULL: N_ O_ T_ N_ U_ L_ L_;
K_NULL: N_ U_ L_ L_;
K_OF: O_ F_;
K_OFF: O_ F_ F_;
K_OFFSET: O_ F_ F_ S_ E_ T_;
K_ON: O_ N_;
K_OR: O_ R_;
K_ORDER: O_ R_ D_ E_ R_;
K_OUTER: O_ U_ T_ E_ R_;
K_PLAN: P_ L_ A_ N_;
K_PRAGMA: P_ R_ A_ G_ M_ A_;
K_PRIMARY: P_ R_ I_ M_ A_ R_ Y_;
K_QUERY: Q_ U_ E_ R_ Y_;
K_RAISE: R_ A_ I_ S_ E_;
K_RECURSIVE: R_ E_ C_ U_ R_ S_ I_ V_ E_;
K_REFERENCES: R_ E_ F_ E_ R_ E_ N_ C_ E_ S_;
K_REGEXP: R_ E_ G_ E_ X_ P_;
K_REINDEX: R_ E_ I_ N_ D_ E_ X_;
K_RELEASE: R_ E_ L_ E_ A_ S_ E_;
K_RENAME: R_ E_ N_ A_ M_ E_;
K_REPLACE: R_ E_ P_ L_ A_ C_ E_;
K_RESTRICT: R_ E_ S_ T_ R_ I_ C_ T_;
K_RIGHT: R_ I_ G_ H_ T_;
K_ROLLBACK: R_ O_ L_ L_ B_ A_ C_ K_;
K_ROW: R_ O_ W_;
K_SAVEPOINT: S_ A_ V_ E_ P_ O_ I_ N_ T_;
K_SELECT: S_ E_ L_ E_ C_ T_;
K_SET: S_ E_ T_;
K_TABLE: T_ A_ B_ L_ E_;
K_TEMP: T_ E_ M_ P_;
K_TEMPORARY: T_ E_ M_ P_ O_ R_ A_ R_ Y_;
//K_TEXT: T_ E_ X_ T_;
K_THEN: T_ H_ E_ N_;
K_TO: T_ O_;
K_TRANSACTION: T_ R_ A_ N_ S_ A_ C_ T_ I_ O_ N_;
K_TRIGGER: T_ R_ I_ G_ G_ E_ R_;
K_TRUE: T_ R_ U_ E_;
K_UNION: U_ N_ I_ O_ N_;
K_UNIQUE: U_ N_ I_ Q_ U_ E_;
K_UPDATE: U_ P_ D_ A_ T_ E_;
K_USING: U_ S_ I_ N_ G_;
K_YES: Y_ E_ S_;
K_VACUUM: V_ A_ C_ U_ U_ M_;
K_VALUES: V_ A_ L_ U_ E_ S_;
K_VIEW: V_ I_ E_ W_;
K_VIRTUAL: V_ I_ R_ T_ U_ A_ L_;
K_WHEN: W_ H_ E_ N_;
K_WHERE: W_ H_ E_ R_ E_;
K_WITH: W_ I_ T_ H_;
K_WITHOUT: W_ I_ T_ H_ O_ U_ T_;

IDENTIFIER
 : '"' (~'"' | '""')* '"'
 | '`' (~'`' | '``')* '`'
 | '[' ~']'* ']'
 | [a-zA-Z_] [a-zA-Z_0-9]* // TODO check: needs more chars in set
 ;
 
ID_UNRESOLVED
 : [a-zA-Z_0-9]* '{{' [a-zA-Z_0-9] ([a-zA-Z_0-9] | '$'| '.' )* '}}' ([a-zA-Z_0-9])*
 ;

INTEGER_LITERAL
: DIGIT+
;

DECIMAL_LITERAL
: DIGIT+ '.' DIGIT*
| '.' DIGIT+
;

FLOATINGPOINT_LITERAL
: DIGIT+ ( '.' DIGIT* )? E_ [-+]? DIGIT+
| '.' DIGIT+ E_ [-+]? DIGIT+
;

//NUMERIC_LITERAL
// : DIGIT+ ( '.' DIGIT* )? ( E_ [-+]? DIGIT+ )?
// | '.' DIGIT+ ( E_ [-+]? DIGIT+ )?
// ;

//BIND_PARAMETER
// : '?' DIGIT*
// | [:@$] IDENTIFIER
// ;

fragment
STRING_QUOTE
 : '\'' ( ~'\'' | '\'\'' )* '\''
 ;

STRING_LITERAL
 : STRING_QUOTE
 ;

BLOB_LITERAL
 : X_ STRING_QUOTE
 ;

SINGLE_LINE_COMMENT
 : '--' ~[\r\n]* -> channel(HIDDEN)
 ;

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

WS	: ( ' '|'\r'|'\t'|'\n' ) -> channel(HIDDEN);
