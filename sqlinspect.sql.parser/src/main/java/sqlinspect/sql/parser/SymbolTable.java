package sqlinspect.sql.parser;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.common.NodeKind;

public final class SymbolTable {

	private static final Logger LOG = LoggerFactory.getLogger(SymbolTable.class);

	private final Multimap<String, SymbolDesc> symbols; // string key ->
														// symboldesc
	private final Map<Base, Multimap<String, SymbolDesc>> symbolsByScopes; // scope
																			// ->
																			// (stringkey
																			// ->
																			// symboldesc)
	private final Map<Base, Base> scopes; // child scope -> parent

	public void clear() {
		symbols.clear();
		symbolsByScopes.clear();
		scopes.clear();
	}

	public SymbolTable() {
		symbols = HashMultimap.create();
		symbolsByScopes = new HashMap<>();
		scopes = new HashMap<>();
	}

	public void insert(String key, SymbolDesc sd) {
		if (key == null) {
			throw new IllegalArgumentException("Key cannot be null!");
		}

		symbols.put(key.intern(), sd);
		Multimap<String, SymbolDesc> scopeSymbols = symbolsByScopes.get(sd.getScope());
		if (scopeSymbols == null) {
			scopeSymbols = HashMultimap.create();
		}
		scopeSymbols.put(key.intern(), sd);
		symbolsByScopes.put(sd.getScope(), scopeSymbols);
		if (LOG.isTraceEnabled()) {
			LOG.trace("Symbol inserted to symbol table: {} sd: {}", key, sd);
		}
	}

	public void insertScope(Base scope, Base parent) {
		scopes.put(scope, parent);
		symbolsByScopes.computeIfAbsent(scope, k -> HashMultimap.create());
	}

	public void erase(String key, SymbolDesc sd) {
		eraseFromSymbols(key.intern(), sd);
	}

	public void eraseFromSymbols(String key, SymbolDesc sd) {
		symbols.remove(key.intern(), sd);
	}

	public void eraseFromSymbolsByScopes(String key, SymbolDesc sd) {
		Multimap<String, SymbolDesc> scopeSymbols = symbolsByScopes.get(sd.getScope());
		if (scopeSymbols != null) {
			scopeSymbols.remove(key.intern(), sd);
			if (scopeSymbols.isEmpty()) {
				symbolsByScopes.remove(sd.getScope());
			}
		}
	}

	public Collection<Base> findInScopeOnly(String key, Base scope, NodeKind... kinds) {
		// logger.debug("Find ID '" + strTab.get(key) + "' ( " + key + ") in scope " +
		// scope.toString());
		// dumpScope(scope);

		Collection<Base> results = new HashSet<>();
		Multimap<String, SymbolDesc> scopeSymbols = symbolsByScopes.get(scope);
		if (scopeSymbols != null) {
			Collection<SymbolDesc> syms = scopeSymbols.get(key.intern());
			if (syms != null) {
				for (SymbolDesc sd : syms) {
					results.add(sd.getSymbol());
				}
			}
		}
		return results;
	}

	public Collection<Base> findInScopes(String key, Base scope, NodeKind... kinds) {
		Collection<Base> results = new HashSet<>();
		Set<Base> parents = getParentScopes(scope);
		Collection<SymbolDesc> sds = symbols.get(key.intern());

		for (SymbolDesc sd : sds) {
			if (parents.contains(sd.getScope()) && (kinds == null || sd.match(kinds))) {
				results.add(sd.getSymbol());
			}
		}

		return results;
	}

	public Collection<Base> findInInnerMostScope(String key, Base scope, NodeKind... kinds) {
		// logger.debug("Find ID '" + strTab.get(key) + "' (" + key + ") in innermost
		// scope " + scope.toString());

		Collection<Base> results = new HashSet<>();
		Base currentScope = scope;
		while (currentScope != null) {
			results.addAll(findInScopeOnly(key.intern(), currentScope, kinds));

			if (!results.isEmpty()) {
				// StringBuilder sb = new StringBuilder();
				// for (Base n : results) {
				// sb.append(n.toString() + ' ');
				// }
				// logger.debug("Result set: " + sb);

				return results;
			}

			Base parent = scopes.get(currentScope);
			currentScope = parent;
		}

		return results;
	}

	public Collection<Base> findAll(String key, NodeKind... kinds) {
		Collection<Base> results = new HashSet<>();
		for (SymbolDesc sd : symbols.get(key.intern())) {
			if (sd.match(kinds)) {
				results.add(sd.getSymbol());
			}
		}
		return results;
	}

	private Set<Base> getParentScopes(Base scope) {
		Set<Base> results = new HashSet<>();

		Base sc = scope;
		while (sc != null) {
			results.add(sc);
			sc = scopes.get(sc);
		}

		return results;
	}

	public void dump(String filename) {
		try (PrintWriter pw = new PrintWriter(filename, StandardCharsets.UTF_8.name())) {
			dumpSymbols(pw);
			dumpSymbolsByScopes(pw);
			dumpScopes(pw);
		} catch (FileNotFoundException e) {
			LOG.error("File not found!", e);
		} catch (UnsupportedEncodingException e) {
			LOG.error("Unknown character encoding!", e);
		}
	}

	private void dumpSymbols(PrintWriter pw) {
		pw.println("Total number of symbols in symbol map: " + symbols.size());
		for (Entry<String, SymbolDesc> e : symbols.entries()) {
			pw.println(e.getKey() + ":  " + e.getValue().toString());
		}
		pw.println();
	}

	private void dumpSymbolsByScopes(PrintWriter pw) {
		pw.println("Total number of scopes in symbols by scope map:" + symbolsByScopes.size());
		for (Entry<Base, Multimap<String, SymbolDesc>> e : symbolsByScopes.entrySet()) {
			pw.println("Scope: " + e.getKey().toString());
			Multimap<String, SymbolDesc> s = e.getValue();
			for (Entry<String, SymbolDesc> e2 : s.entries()) {
				pw.println("  " + e2.getKey() + ":  " + e2.getValue().toString());
			}
		}
		pw.println();
	}

	private void dumpScopes(PrintWriter pw) {
		pw.println("Scopes:");
		for (Entry<Base, Base> e : scopes.entrySet()) {
			pw.println(e.getKey().toString() + " -> " + e.getValue().toString());
		}
		pw.println();
	}

	@SuppressWarnings("unused")
	private void dumpScope(Base scope) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Scope {}: ", scope);
			Multimap<String, SymbolDesc> s = symbolsByScopes.get(scope);
			for (Entry<String, SymbolDesc> e : s.entries()) {
				LOG.debug("  key : {} - {}", e.getKey(), e.getValue());
			}
		}
	}
}
