package sqlinspect.sql.parser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.Lexer;

public abstract class SQLLexer extends Lexer {

	protected SQLLexer(CharStream input) {
		super(input);
		removeErrorListener(ConsoleErrorListener.INSTANCE);
		addErrorListener(SQLErrorListener.getInstance());
	}
}
