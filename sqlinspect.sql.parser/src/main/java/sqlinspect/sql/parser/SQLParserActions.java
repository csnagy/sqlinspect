package sqlinspect.sql.parser;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.builder.AlterTableBuilder;
import sqlinspect.sql.asg.builder.AnyTypeBuilder;
import sqlinspect.sql.asg.builder.ArrayTypeBuilder;
import sqlinspect.sql.asg.builder.AsteriskBuilder;
import sqlinspect.sql.asg.builder.BetweenBuilder;
import sqlinspect.sql.asg.builder.BinaryExpressionBuilder;
import sqlinspect.sql.asg.builder.BinaryQueryBuilder;
import sqlinspect.sql.asg.builder.CaseBuilder;
import sqlinspect.sql.asg.builder.CastBuilder;
import sqlinspect.sql.asg.builder.ColumnAttributeBuilder;
import sqlinspect.sql.asg.builder.ColumnBuilder;
import sqlinspect.sql.asg.builder.CreateDatabaseBuilder;
import sqlinspect.sql.asg.builder.CreateTableBuilder;
import sqlinspect.sql.asg.builder.CreateViewBuilder;
import sqlinspect.sql.asg.builder.DatabaseBuilder;
import sqlinspect.sql.asg.builder.DateTypeBuilder;
import sqlinspect.sql.asg.builder.DefaultBuilder;
import sqlinspect.sql.asg.builder.DeleteBuilder;
import sqlinspect.sql.asg.builder.DropDatabaseBuilder;
import sqlinspect.sql.asg.builder.DropFunctionBuilder;
import sqlinspect.sql.asg.builder.DropTableBuilder;
import sqlinspect.sql.asg.builder.DropViewBuilder;
import sqlinspect.sql.asg.builder.ExpressionListBuilder;
import sqlinspect.sql.asg.builder.FunctionBuilder;
import sqlinspect.sql.asg.builder.FunctionDefParamsBuilder;
import sqlinspect.sql.asg.builder.FunctionParamsBuilder;
import sqlinspect.sql.asg.builder.IdBuilder;
import sqlinspect.sql.asg.builder.InnerFunctionCallBuilder;
import sqlinspect.sql.asg.builder.InsertBuilder;
import sqlinspect.sql.asg.builder.IntervalBuilder;
import sqlinspect.sql.asg.builder.IsNullBuilder;
import sqlinspect.sql.asg.builder.JoinBuilder;
import sqlinspect.sql.asg.builder.JokerBuilder;
import sqlinspect.sql.asg.builder.LimitBuilder;
import sqlinspect.sql.asg.builder.LiteralBuilder;
import sqlinspect.sql.asg.builder.MapTypeBuilder;
import sqlinspect.sql.asg.builder.MatchAgainstBuilder;
import sqlinspect.sql.asg.builder.NumericTypeBuilder;
import sqlinspect.sql.asg.builder.OnBuilder;
import sqlinspect.sql.asg.builder.OrderByElementBuilder;
import sqlinspect.sql.asg.builder.PragmaBuilder;
import sqlinspect.sql.asg.builder.RowBuilder;
import sqlinspect.sql.asg.builder.SchemaBuilder;
import sqlinspect.sql.asg.builder.SelectBuilder;
import sqlinspect.sql.asg.builder.SelectExpressionBuilder;
import sqlinspect.sql.asg.builder.SetBuilder;
import sqlinspect.sql.asg.builder.SetValueBuilder;
import sqlinspect.sql.asg.builder.SetVarBuilder;
import sqlinspect.sql.asg.builder.ShowCreateTableBuilder;
import sqlinspect.sql.asg.builder.StringTypeBuilder;
import sqlinspect.sql.asg.builder.StructFieldBuilder;
import sqlinspect.sql.asg.builder.StructTypeBuilder;
import sqlinspect.sql.asg.builder.TableBuilder;
import sqlinspect.sql.asg.builder.TruncateBuilder;
import sqlinspect.sql.asg.builder.UnaryExpressionBuilder;
import sqlinspect.sql.asg.builder.UpdateBuilder;
import sqlinspect.sql.asg.builder.UseBuilder;
import sqlinspect.sql.asg.builder.UserFunctionCallBuilder;
import sqlinspect.sql.asg.builder.UsingBuilder;
import sqlinspect.sql.asg.builder.ValuesBuilder;
import sqlinspect.sql.asg.builder.ViewBuilder;
import sqlinspect.sql.asg.builder.WhenClauseBuilder;
import sqlinspect.sql.asg.clause.JoinConditionClause;
import sqlinspect.sql.asg.clause.Limit;
import sqlinspect.sql.asg.clause.On;
import sqlinspect.sql.asg.clause.Using;
import sqlinspect.sql.asg.clause.WhenClause;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.common.NodeKind;
import sqlinspect.sql.asg.expr.Asterisk;
import sqlinspect.sql.asg.expr.Between;
import sqlinspect.sql.asg.expr.BinaryExpression;
import sqlinspect.sql.asg.expr.BinaryQuery;
import sqlinspect.sql.asg.expr.Case;
import sqlinspect.sql.asg.expr.Cast;
import sqlinspect.sql.asg.expr.Default;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.FunctionCall;
import sqlinspect.sql.asg.expr.FunctionParams;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.InnerFunctionCall;
import sqlinspect.sql.asg.expr.Interval;
import sqlinspect.sql.asg.expr.IsNull;
import sqlinspect.sql.asg.expr.Join;
import sqlinspect.sql.asg.expr.Joker;
import sqlinspect.sql.asg.expr.Literal;
import sqlinspect.sql.asg.expr.MatchAgainst;
import sqlinspect.sql.asg.expr.OrderByElement;
import sqlinspect.sql.asg.expr.Query;
import sqlinspect.sql.asg.expr.Row;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.expr.SetVar;
import sqlinspect.sql.asg.expr.UnaryExpression;
import sqlinspect.sql.asg.expr.UserFunctionCall;
import sqlinspect.sql.asg.expr.Values;
import sqlinspect.sql.asg.kinds.AliasKind;
import sqlinspect.sql.asg.kinds.BinaryExpressionKind;
import sqlinspect.sql.asg.kinds.BinaryQueryKind;
import sqlinspect.sql.asg.kinds.ColumnAttributeKind;
import sqlinspect.sql.asg.kinds.DateTypeKind;
import sqlinspect.sql.asg.kinds.FunctionParamsKind;
import sqlinspect.sql.asg.kinds.JoinKind;
import sqlinspect.sql.asg.kinds.LiteralKind;
import sqlinspect.sql.asg.kinds.NullOrderKind;
import sqlinspect.sql.asg.kinds.NumericTypeKind;
import sqlinspect.sql.asg.kinds.OrderByElementKind;
import sqlinspect.sql.asg.kinds.SetQuantifierKind;
import sqlinspect.sql.asg.kinds.StringTypeKind;
import sqlinspect.sql.asg.kinds.TimeUnitKind;
import sqlinspect.sql.asg.kinds.UnaryExpressionKind;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.ColumnAttribute;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Function;
import sqlinspect.sql.asg.schema.FunctionDefParams;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.schema.View;
import sqlinspect.sql.asg.statm.AlterTable;
import sqlinspect.sql.asg.statm.CreateDatabase;
import sqlinspect.sql.asg.statm.CreateTable;
import sqlinspect.sql.asg.statm.CreateView;
import sqlinspect.sql.asg.statm.Delete;
import sqlinspect.sql.asg.statm.DropDatabase;
import sqlinspect.sql.asg.statm.DropFunction;
import sqlinspect.sql.asg.statm.DropTable;
import sqlinspect.sql.asg.statm.DropView;
import sqlinspect.sql.asg.statm.Insert;
import sqlinspect.sql.asg.statm.Pragma;
import sqlinspect.sql.asg.statm.Select;
import sqlinspect.sql.asg.statm.Set;
import sqlinspect.sql.asg.statm.SetValue;
import sqlinspect.sql.asg.statm.ShowCreateTable;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.statm.Truncate;
import sqlinspect.sql.asg.statm.Update;
import sqlinspect.sql.asg.statm.Use;
import sqlinspect.sql.asg.type.AnyType;
import sqlinspect.sql.asg.type.ArrayType;
import sqlinspect.sql.asg.type.DateType;
import sqlinspect.sql.asg.type.MapType;
import sqlinspect.sql.asg.type.NumericType;
import sqlinspect.sql.asg.type.StringType;
import sqlinspect.sql.asg.type.StructField;
import sqlinspect.sql.asg.type.StructType;
import sqlinspect.sql.asg.type.Type;
import sqlinspect.sql.asg.util.ASGUtils;

public abstract class SQLParserActions {
	private static final Logger LOG = LoggerFactory.getLogger(SQLParserActions.class);

	protected final SQLParser parser;
	protected final ASG asg;

	private static final String NULLSTR = "NULL";

	protected Database actualDatabase;
	protected Schema actualSchema;
	private int statements;

	protected SQLParserActions(SQLParser parser) {
		this.parser = parser;
		this.asg = parser.asg;
	}

	public void init() {
		actualDatabase = retrieveDatabase(parser.getDefaultDatabase());
		asg.getRoot().setDefaultDatabase(actualDatabase);
		actualSchema = retrieveDefaultSchema(actualDatabase);
		statements = 0;
	}

	/**
	 * This method is called before the main rule of the parser is executed.
	 */
	public void startInput() {
		// there is nothing to do here now.
	}

	public int getStatements() {
		return statements;
	}

	protected boolean checkNull(Object param, String paramName) {
		if (param == null) {
			StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
			StackTraceElement e = stacktrace[2];
			String methodName = e.getMethodName();
			LOG.trace("Null value for '{}' in '{}'", paramName, methodName);
			return true;
		} else {
			return false;
		}
	}

	public void finishNode(Base n, Token semi) {
		if (checkNull(n, "n")) {
			return;
		}

		statements++;

		LOG.trace("Finish node: {} at line {}", n, semi != null ? semi.getLine() : "NULL");

		if (n instanceof Statement) {
			Statement stmt = (Statement) n;
			if (stmt.getEndLine() == 0 && semi != null) {
				copyAllPosition(stmt, semi);
			}
			if (stmt.getPath() == null) {
				stmt.setPath(parser.getInputName());
			}
			actualDatabase.addStatement(stmt);
			((Statement) n).setInternalId(parser.getInternalId());
		} else {
			LOG.warn("Expected a statement node, but got something different!");
		}
	}

	public Database retrieveDatabase(Expression expr) {
		if (expr instanceof Id) {
			Id dbId = (Id) expr;
			return retrieveDatabase(dbId.getName());
		} else if (expr instanceof Joker) {
			// no database for joker node, but we don't throw an exception here
			return null;
		} else {
			throw new ASGException("Invalid expression for database" + expr.toString());
		}
	}

	public Database retrieveDatabase(String name) {
		SQLRoot root = asg.getRoot();
		for (Database db : root.getDatabases()) {
			if (db.getName().equalsIgnoreCase(name)) {
				return db;
			}
		}

		return introduceDatabase(name);
	}

	public Database retrieveDefaultDatabase() {
		return retrieveDatabase(parser.getDefaultDatabase());
	}

	public Database introduceDatabase(String name) {
		Database db = new DatabaseBuilder(asg).setName(name).result();
		SQLRoot root = asg.getRoot();
		root.addDatabase(db);

		Schema sch = retrieveDefaultSchema(db);
		db.setDefaultSchema(sch);

		LOG.debug("Database created: {}", db.getName());
		return db;
	}

	/**
	 * Retrieve the schema node from the AST satisfying the given conditions. If it
	 * does not exist, create a new one.
	 *
	 * @param db   Database node in which we are looking for the schema.
	 * @param name Name of the schema.
	 * @return The schema under the database.
	 */
	public Schema retrieveSchema(Database db, String name) {
		if (name == null || name.isEmpty()) {
			return retrieveDefaultSchema(db);
		}

		for (Schema sch : db.getSchemas()) {
			if (sch.getName().equalsIgnoreCase(name)) {
				return sch;
			}
		}

		return introduceSchema(db, name);
	}

	public Schema retrieveSchema(Expression expr) {
		if (expr instanceof Id) {
			return retrieveSchema((Id) expr);
		} else if (expr instanceof BinaryExpression) {
			return retrieveSchema((BinaryExpression) expr);
		} else {
			LOG.error("Malforemd expression for schema: {}", expr);
			return null;
		}
	}

	/**
	 * Retrieve the schema node from the AST satisfying the given conditions. If it
	 * does not exist, create a new one.
	 *
	 * The BinaryExpression should be in the form [db].schema. If db is not
	 * specified, it will look for the schema under the default database.
	 *
	 * @param binary The name of the schema specified by a BinaryExpression
	 * @return The schema under the database.
	 */
	public Schema retrieveSchema(BinaryExpression binary) {
		Expression rhs = binary.getRight();
		if (rhs.getNodeKind() != NodeKind.ID) {
			LOG.error("Cannot retrieve schema for non-ID element in binary expression.");
			return null;
		}
		Id schId = (Id) rhs;

		Database db;
		Expression lhs = binary.getLeft();
		if (lhs.getNodeKind() == NodeKind.ID) {
			Id dbId = (Id) lhs;
			db = retrieveDatabase(dbId.getName());
		} else {
			db = retrieveDefaultDatabase();
		}

		return retrieveSchema(db, schId.getName());
	}

	public Schema retrieveSchema(Id id) {
		return retrieveSchema(actualDatabase, id.getName());
	}

	/**
	 * Retrieve the default schema of a database. If it does not exist, create a new
	 * one.
	 *
	 * @param db Database under which we want to introduce the new Schema node.
	 * @return The default schema node of the database.
	 */

	public Schema retrieveDefaultSchema(Database db) {
		Schema sch = db.getDefaultSchema();
		if (sch == null) {
			sch = retrieveSchema(db, parser.getDefaultSchema());
		}
		db.setDefaultSchema(sch);
		return sch;
	}

	/**
	 * Introduce a new schema.
	 *
	 * @param db   Database under which we want to introduce the new Schema node.
	 * @param name Name of the schema.
	 * @return The newly created Schema node.
	 */

	public Schema introduceSchema(Database db, String name) {
		Schema sch = new SchemaBuilder(asg).setName(name).result();
		db.addSchema(sch);

		LOG.debug("Schema created: {}.{}", db.getName(), sch.getName());

		return sch;
	}

	public Table retrieveTable(Expression expr) {
		if (expr instanceof BinaryExpression) {
			return retrieveTable((BinaryExpression) expr);
		} else if (expr instanceof Id) {
			return retrieveTable((Id) expr);
		} else {
			LOG.error("Invalid table expression in retrieveTable: {}", expr);
			return null;
		}
	}

	public Table retrieveTable(BinaryExpression tabExpr) {
		if (checkNull(tabExpr, "tabExpr")) {
			return null;
		}

		BinaryExpression bin = tabExpr;

		Expression rhs = bin.getRight();
		if (!(rhs instanceof Id)) {
			LOG.error("Invalid table expression in retrieveTable: {}", tabExpr);
			return null;
		}

		Expression lhs = bin.getLeft();
		Schema sch = retrieveSchema(lhs);
		if (sch == null) {
			LOG.error("Could not determine schema for retrieveTable: {}", tabExpr);
			return null;
		}

		Id tabId = (Id) rhs;
		return retrieveTable(sch, tabId.getName());
	}

	public Table retrieveTable(Id tabId) {
		if (checkNull(tabId, "tabId")) {
			return null;
		}
		return retrieveTable(actualSchema, tabId.getName());
	}

	public Table retrieveTable(Schema sch, String name) {
		if (name == null || name.isEmpty()) {
			LOG.error("Table name cannot be nulll or empty!");
			return null;
		}

		for (Table tab : sch.getTables()) {
			if (tab.getName().equalsIgnoreCase(name)) {
				return tab;
			}
		}

		return introduceTable(sch, name);
	}

	public Table introduceTable(Schema sch, String name) {
		Table tab = new TableBuilder(asg).setName(name).result();
		sch.addTable(tab);

		LOG.debug("Table created: {}.{}", sch.getName(), tab.getName());

		return tab;
	}

	public View retrieveView(Expression expr) {
		if (expr instanceof BinaryExpression) {
			return retrieveView((BinaryExpression) expr);
		} else if (expr instanceof Id) {
			return retrieveView((Id) expr);
		} else {
			LOG.error("Malformed view expression: {}", expr);
			return null;
		}
	}

	public View retrieveView(BinaryExpression expr) {
		if (checkNull(expr, "expr")) {
			return null;
		}

		BinaryExpression bin = expr;

		Expression rhs = bin.getRight();
		if (!(rhs instanceof Id)) {
			LOG.error("Invalid expression in retrieveView: {}", expr);
			return null;
		}

		Expression lhs = bin.getLeft();
		Schema sch = retrieveSchema(lhs);

		if (sch == null) {
			LOG.error("Could not determine schema for retrieveView: {}", expr);
			return null;
		}

		Id viewId = (Id) rhs;
		return retrieveView(sch, viewId.getName());
	}

	public View retrieveView(Id viewId) {
		if (checkNull(viewId, "viewId")) {
			return null;
		}
		return retrieveView(actualSchema, viewId.getName());
	}

	public View retrieveView(Schema sch, String name) {
		if (name == null || name.isEmpty()) {
			LOG.error("View name cannot be nulll or empty!");
			return null;
		}

		for (View view : sch.getViews()) {
			if (view.getName().equalsIgnoreCase(name)) {
				return view;
			}
		}

		return introduceView(sch, name);
	}

	public View introduceView(Schema sch, String name) {
		View view = new ViewBuilder(asg).setName(name).result();
		sch.addView(view);

		LOG.debug("View created: {}.{}", sch.getName(), view.getName());

		return view;
	}

	public Column retrieveColumn(Table tab, String name) {
		if (name == null) {
			LOG.error("Column name cannot be null!");
			return null;
		}

		if (name.isEmpty()) {
			LOG.error("Column name cannot be empty!");
			return null;
		}

		for (Column col : tab.getColumns()) {
			if (col.getName().equalsIgnoreCase(name)) {
				return col;
			}
		}

		return introduceColumn(tab, name);
	}

	public Column retrieveColumn(View view, String name) {
		if (name == null) {
			LOG.error("Column name cannot be null!");
			return null;
		}

		if (name.isEmpty()) {
			LOG.error("Column name cannot be empty!");
			return null;
		}

		for (Column col : view.getColumns()) {
			if (col.getName().equalsIgnoreCase(name)) {
				return col;
			}
		}

		return introduceColumn(view, name);
	}

	public Column introduceColumn(Table tab, String name) {
		Column col = new ColumnBuilder(asg).setName(name).result();
		tab.addColumn(col);

		LOG.debug("Column created: {} under {}", col.getName(), tab.getName());

		return col;
	}

	public Column introduceColumn(View view, String name) {
		Column col = new ColumnBuilder(asg).setName(name).result();
		view.addColumn(col);

		LOG.debug("Column created: {} under {}", col.getName(), view.getName());

		return col;
	}

	public AnyType retrieveAnyType() {
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof AnyType) {
				return (AnyType) t;
			}
		}

		return introduceAnyType();
	}

	public AnyType introduceAnyType() {
		AnyTypeBuilder ab = new AnyTypeBuilder(asg);
		AnyType at = ab.result();
		asg.getRoot().getDefaultDatabase().addType(at);
		LOG.trace("New AnyType created: {}", at);
		return at;
	}

	public NumericType retrieveNumericType(NumericTypeKind kind, int precision, int scale, boolean unsigned,
			boolean zerofill) {
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof NumericType) {
				NumericType nt = (NumericType) t;
				if (nt.getKind() == kind && nt.getPrecision() == precision && nt.getScale() == scale
						&& nt.getUnsigned() == unsigned && nt.getZerofill() == zerofill) {
					return nt;
				}
			}
		}

		return introduceNumericType(kind, precision, scale, unsigned, zerofill);
	}

	public NumericType introduceNumericType(NumericTypeKind kind, int precision, int scale, boolean unsigned,
			boolean zerofill) {
		NumericTypeBuilder nb = new NumericTypeBuilder(asg);
		NumericType nt = nb.setKind(kind).setPrecision(precision).setScale(scale).setUnsigned(unsigned)
				.setZerofill(zerofill).result();

		asg.getRoot().getDefaultDatabase().addType(nt);

		LOG.debug("New NumericType created: {}", nt);

		return nt;
	}

	public DateType retrieveDateType(Token name, int length, int precision) {
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof DateType) {
				DateType dt = (DateType) t;
				if (dt.getKind().toString().equalsIgnoreCase(name.getText()) && dt.getPrecision() == precision
						&& dt.getLength() == length) {
					return dt;
				}
			}
		}

		return introduceDateType(name, length, precision);
	}

	public DateType introduceDateType(Token name, int length, int precision) {
		DateTypeBuilder dtb = new DateTypeBuilder(asg);
		DateType dt = dtb.setKind(toDateTypeKind(name)).setPrecision(precision).setLength(length).result();

		asg.getRoot().getDefaultDatabase().addType(dt);

		LOG.debug("New DateType created: {}", dt);

		return dt;
	}

	public StringType retrieveStringType(StringTypeKind kind, int length) {
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof StringType) {
				StringType st = (StringType) t;
				if (st.getKind() == kind && st.getLength() == length) {
					return st;
				}
			}
		}

		return introduceStringType(kind, length);
	}

	public StringType introduceStringType(StringTypeKind kind, int length) {
		StringTypeBuilder stb = new StringTypeBuilder(asg);
		StringType t = stb.setKind(kind).setLength(length).result();

		asg.getRoot().getDefaultDatabase().addType(t);

		LOG.debug("New StringType created: {}", t);

		return t;
	}

	public ArrayType retrieveArrayType(Type valueType) {
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof ArrayType) {
				ArrayType at = (ArrayType) t;
				if (at.getValueType().equals(valueType)) {
					return at;
				}
			}
		}

		return introduceArrayType(valueType);
	}

	public ArrayType introduceArrayType(Type valueType) {
		ArrayTypeBuilder atb = new ArrayTypeBuilder(asg);
		ArrayType at = atb.result();
		at.setValueType(valueType);

		asg.getRoot().getDefaultDatabase().addType(at);

		LOG.debug("New ArrayType created: {}", at);

		return at;
	}

	public MapType retrieveMapType(Type keyType, Type valueType) {
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof MapType) {
				MapType mt = (MapType) t;
				if (mt.getValueType().equals(valueType) && mt.getKeyType().equals(keyType)) {
					return mt;
				}
			}
		}

		return introduceMapType(keyType, valueType);
	}

	public MapType introduceMapType(Type keyType, Type valueType) {
		MapTypeBuilder mtb = new MapTypeBuilder(asg);
		MapType mt = mtb.result();
		mt.setKeyType(keyType);
		mt.setValueType(valueType);

		asg.getRoot().getDefaultDatabase().addType(mt);

		LOG.debug("New MapType created: {}", mt);

		return mt;
	}

	public StructType retrieveStructType(List<StructField> fields) {
		int fsize = fields.size();
		for (Type t : actualDatabase.getTypes()) {
			if (t instanceof StructType) {
				StructType st = (StructType) t;
				List<StructField> fields2 = st.getFields();
				if (fields2.size() != fsize) {
					continue;
				}
				boolean match = true;
				for (int i = 0; i < fsize; i++) {
					StructField f = fields.get(i);
					boolean hasMatch = false;
					for (int j = 0; j < fsize; j++) {
						if (!f.match(fields2.get(j))) {
							hasMatch = true;
						}
					}
					match = hasMatch;
				}
				if (match) {
					return st;
				}
			}
		}

		return introduceStructType(fields);
	}

	public StructType introduceStructType(List<StructField> fields) {
		StructTypeBuilder stb = new StructTypeBuilder(asg);
		StructType st = stb.result();
		for (StructField f : fields) {
			st.addField(f);
		}

		asg.getRoot().getDefaultDatabase().addType(st);

		LOG.debug("New StructType created: {}", st);

		return st;
	}

	protected boolean isDBRef(Expression expr) {
		return expr instanceof Id && ((Id) expr).getName() != null;
	}

	protected Database getDBRef(Expression expr) {
		if (expr instanceof Id) {
			return retrieveDatabase(((Id) expr).getName());
		}
		return null;
	}

	protected Function retrieveFunction(Expression name, FunctionDefParams params, Type returnType) {
		if (name instanceof BinaryExpression) {
			BinaryExpression bin = (BinaryExpression) name;
			Expression left = bin.getLeft();
			Expression right = bin.getRight();

			if (isDBRef(left) && right instanceof Id) {
				Database db = getDBRef(left);
				if (db == null) {
					LOG.warn("Could not retrieve database: {}", left);
					return null;
				}

				return retrieveFunction(db.getDefaultSchema(), ((Id) right).getName(), params, returnType);
			} else {
				LOG.warn("Malformed expression for function name: {}", name);
			}
		} else if (name instanceof Id) {
			return retrieveFunction(actualSchema, ((Id) name).getName(), params, returnType);
		} else {
			LOG.warn("Malformed expression for function name: {}", name);
		}

		return null;
	}

	public Function introduceFunction(Schema schema, String name, FunctionDefParams params, Type returnType) {
		if (schema == null) {
			LOG.error("Schema cannot be null!");
			return null;
		}
		if (name == null) {
			LOG.error("Name cannot be null!");
			return null;
		}

		FunctionBuilder b = new FunctionBuilder(asg);
		b.setName(name);
		b.setParameterList(params);
		b.setReturnType(returnType);
		schema.addFunction(b.result());
		return b.result();
	}

	protected Function retrieveFunction(Schema schema, String name, FunctionDefParams params, Type returnType) {
		if (schema == null) {
			LOG.error("Schema cannot be null!");
			return null;
		}
		if (name == null) {
			LOG.error("Name cannot be null!");
			return null;
		}

		for (Function func : schema.getFunctions()) {
			if (func.getName().equalsIgnoreCase(name)
					&& ASGUtils.functionDefParamsEquals(func.getParameterList(), params)) {
				return func;
			}
		}

		return introduceFunction(schema, name, params, returnType);
	}

	public Function introduceInnerFunction(String name, FunctionParams params) {
		FunctionBuilder b = new FunctionBuilder(asg);
		b.setName(name);

		if (params != null) {
			FunctionDefParamsBuilder fdb = new FunctionDefParamsBuilder(asg);
			Expression par = params.getParameter();
			if (par instanceof ExpressionList) {
				ExpressionList parList = (ExpressionList) par;
				for (int i = 0; i < parList.getExpressions().size(); i++) {
					fdb.addParameter(retrieveAnyType());
				}
			}
			b.setParameterList(fdb.result());
		}
		b.setReturnType(retrieveAnyType());
		asg.getRoot().addInnerFunction(b.result());
		return b.result();
	}

	protected Function retrieveInnerFunction(String name, FunctionParams params) {
		for (Function func : asg.getRoot().getInnerFunctions()) {
			if (func.getName().equalsIgnoreCase(name)
					&& ASGUtils.functionParamsEquals(func.getParameterList(), params)) {
				return func;
			}
		}

		return introduceInnerFunction(name, params);
	}

	public Id buildId(Token t) {
		return buildId(t, false);
	}

	public Id buildId(Token t, boolean quoted) {
		IdBuilder b = new IdBuilder(asg);
		if (quoted) {
			b.setName(t.getText().substring(1, t.getText().length() - 1));
			b.setQuoted(true);
		} else {
			b.setName(t.getText());
		}
		copyAllPosition(b.result(), t);
		return b.result();
	}

	// statements

	public Table newTable(Expression expr) {
		Schema sch = actualSchema;
		Id tableId = null;

		// table can be specified in the form of: [db.][schema].table

		if (expr instanceof BinaryExpression) {
			BinaryExpression binExpr = (BinaryExpression) expr;
			Expression lhs = binExpr.getLeft();
			Expression rhs = binExpr.getRight();

			sch = retrieveSchema(lhs);

			if (rhs instanceof Id) {
				tableId = (Id) rhs;
			} else {
				LOG.warn("Malformed table expression: {}", expr);
			}
		} else if (expr instanceof Id) {
			tableId = (Id) expr;
		} else {
			LOG.warn("Invalid expression for newTable action! {} ", expr);
		}

		if (tableId == null) {
			LOG.error("Could not get id for new table!");
			return null;
		}

		return retrieveTable(sch, tableId.getName());
	}

	public Column newColumn(Table tab, Expression expr, Type type) {
		if (!parser.isInterpretSchema()) {
			return null;
		}
		if (tab == null) {
			LOG.warn("Table null for newColumn. Joker node?");
			return null;
		}

		Id columnId = null;
		if (expr instanceof BinaryExpression) {
			// MySQL allows the usage of db.table.column, but in a create table
			// table can refer only to the actual table.
			// We suppose that the left hand side is correct, and don't check
			// it.

			BinaryExpression binExpr = (BinaryExpression) expr;
			Expression rhs = binExpr.getRight();

			if (rhs instanceof Id) {
				columnId = (Id) rhs;
			} else {
				LOG.warn("Invalid right hand side of column identifier in newColumn action! {}", expr);
			}
		} else if (expr instanceof Id) {
			columnId = (Id) expr;
		} else {
			LOG.warn("Invalid expression for newColumn action! {}", expr);
		}

		if (columnId == null) {
			LOG.error("Could not get id for new column!");
			return null;
		}

		Column col = retrieveColumn(tab, columnId.getName());

		if (type != null) {
			col.setType(type);
		}

		return col;
	}

	public Column newColumn(Table tab, Expression expr, Type type, ColumnAttribute attribute) {
		List<ColumnAttribute> attrs = new ArrayList<>();
		attrs.add(attribute);
		return newColumn(tab, expr, type, attrs);
	}

	public Column newColumn(Table table, Expression expr, Type type, List<ColumnAttribute> attributes) {
		if (table == null) {
			LOG.debug("Null table for newColumn. Joker node?");
			return null;
		}

		Column col = newColumn(table, expr, type);

		if (col != null && attributes != null) {
			for (ColumnAttribute attr : attributes) {
				if (attr != null) {
					col.addAttribute(attr);
				}
			}
		}

		return col;
	}

	public Column newColumn(View view, Expression expr, Type type) {
		Id columnId = null;
		if (expr instanceof BinaryExpression) {
			BinaryExpression binExpr = (BinaryExpression) expr;
			Expression rhs = binExpr.getRight();

			if (rhs instanceof Id) {
				columnId = (Id) rhs;
			} else {
				LOG.warn("Invalid right hand side of column identifier in newColumn action! {}", expr);
			}
		} else if (expr instanceof Id) {
			columnId = (Id) expr;
		} else {
			LOG.warn("Invalid expression for newColumn action! {}", expr);
		}

		if (columnId == null) {
			LOG.error("Could not get id for new column!");
			return null;
		}

		Column col = retrieveColumn(view, columnId.getName());

		if (type != null) {
			col.setType(type);
		}

		return col;
	}

	public Column newColumn(View view, Expression expr, Type type, ColumnAttribute attribute) {
		List<ColumnAttribute> attrs = new ArrayList<>();
		attrs.add(attribute);
		return newColumn(view, expr, type, attrs);
	}

	public Column newColumn(View view, Expression expr, Type type, List<ColumnAttribute> attributes) {
		Column col = newColumn(view, expr, type);

		if (attributes != null) {
			for (ColumnAttribute attr : attributes) {
				if (attr != null) {
					col.addAttribute(attr);
				}
			}
		}

		return col;
	}

	public NumericType newNumericType(Token typeTok, Token precTok, Token scaleTok, List<Token> options) {
		boolean unsigned = false;
		boolean zerofill = false;

		int precision = precTok == null ? 0 : Integer.parseInt(precTok.getText());
		int scale = scaleTok == null ? 0 : Integer.parseInt(scaleTok.getText());

		if (options != null) {
			for (Token t : options) {
				if ("UNSIGNED".equalsIgnoreCase(t.getText())) {
					unsigned = true;
				} else if ("ZEROFILL".equalsIgnoreCase(t.getText())) {
					zerofill = true;
				}
			}
		}

		return retrieveNumericType(toNumericTypeKind(typeTok), precision, scale, unsigned, zerofill);
	}

	public DateType newDateType(Token typeTok, Token lengthTok, Token precisionTok) {
		int length = lengthTok == null ? 0 : Integer.parseInt(lengthTok.getText());
		int precision = precisionTok == null ? 0 : Integer.parseInt(precisionTok.getText());

		return retrieveDateType(typeTok, length, precision);
	}

	public StringType newStringType(Token nameTok, Token lengthTok) {
		return newStringType(toStringTypeKind(nameTok), lengthTok);
	}

	public StringType newStringType(StringTypeKind kind, Token lengthTok) {
		int length = lengthTok == null ? 0 : Integer.parseInt(lengthTok.getText());

		return retrieveStringType(kind, length);
	}

	public ArrayType newArrayType(Token typeTok, Type valueType) {
		return retrieveArrayType(valueType);
	}

	public MapType newMapType(Token typeTok, Type keyType, Type valueType) {
		return retrieveMapType(keyType, valueType);
	}

	public StructType newStructType(Token typeTok, List<StructField> fields) {
		return retrieveStructType(fields);
	}

	public StructField buildStructField(Token name, Type type) {
		StructFieldBuilder b = new StructFieldBuilder(asg);
		b.setName(name.getText());
		b.setValueType(type);
		return b.result();
	}

	public CreateTable buildCreateTable(Token t, Expression table, boolean ifNotExists) {
		return buildCreateTable(t, table, ifNotExists, false, false, null);
	}

	public CreateTable buildCreateTable(Token t, Expression table, boolean ifNotExists, boolean isExternal,
			boolean isTemporary, Token location) {
		CreateTableBuilder b = new CreateTableBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfNotExists(ifNotExists);
		if (parser.isInterpretSchema() && table != null && !(table instanceof Joker)) {
			Table tab = newTable(table);
			// we can still fail here, e.g., in cases like joker.joker
			if (tab != null) {
				tab.setExternal(isExternal);
				tab.setTemporary(isTemporary);
				if (location != null) {
					tab.setLocation(location.getText());
				}

				b.setTable(table);

				if (table instanceof Id) {
					((Id) table).setRefersTo(tab);
				}

				b.setSchemaTable(tab);
			}
		}
		return b.result();
	}

	public void tableSetLocation(Table t, Token location) {
		if (checkNull(t, "t") || checkNull(location, "location")) {
			return;
		}
		t.setLocation(location.getText());
	}

	protected void copyTable(Table to, Table from) {
		if (checkNull(to, "to") || checkNull(from, "from")) {
			return;
		}

		LOG.debug("Copy table information to {} from {}", to.getName(), from.getName());

		if (from.getColumns() != null) {
			for (Column col : from.getColumns()) {
				Column col2 = retrieveColumn(to, col.getName());
				if (col.getType() != null) {
					col2.setType(col.getType());
				}
			}
		}
	}

	public void createTableLike(Table newTable, Expression otherTableExpr) {
		Table otherTable = retrieveTable(otherTableExpr);
		copyTable(newTable, otherTable);
	}

	public CreateView buildCreateView(Token t, Expression name, boolean ifNotExists) {
		CreateViewBuilder b = new CreateViewBuilder(asg);
		copyAllPosition(b.result(), t);
		View view = retrieveView(name);
		b.setView(view);
		return b.result();
	}

	public void createViewSetQuery(CreateView view, Query query) {
		if (checkNull(view, "view") || checkNull(query, "query")) {
			return;
		}
		view.setQuery(query);
	}

	public CreateDatabase buildCreateDatabase(Token t, Expression db, boolean ifNotExists) {
		return buildCreateDatabase(t, db, ifNotExists, null);
	}

	public CreateDatabase buildCreateDatabase(Token t, Expression dbExpr, boolean ifNotExists, Token location) {
		CreateDatabaseBuilder b = new CreateDatabaseBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfNotExists(ifNotExists);
		if (dbExpr != null && parser.isInterpretSchema()) {
			Database db = retrieveDatabase(dbExpr);
			if (db != null) {
				b.setDatabase(db);
				if (location != null) {
					db.setLocation(location.getText());
				}
			} else {
				LOG.debug("Got null as database for expression: {}", dbExpr);
			}
			copyEndPosition(b.result(), dbExpr);
		}
		return b.result();
	}

	public void databaseSetLocation(Database db, Token location) {
		if (checkNull(db, "db") || checkNull(location, "location")) {
			return;
		}
		db.setLocation(location.getText());
	}

	public Truncate buildTruncate(Token t, Expression table, boolean ifExists) {
		TruncateBuilder b = new TruncateBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfExists(ifExists);
		if (table != null) {
			b.setTable(table);
			copyEndPosition(b.result(), table);
		}

		return b.result();
	}

	public DropDatabase buildDropDatabase(Token t, Expression db, boolean ifExists) {
		DropDatabaseBuilder b = new DropDatabaseBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfExists(ifExists);
		if (db != null) {
			b.setDatabase(db);
			copyEndPosition(b.result(), db);
		}

		return b.result();
	}

	public DropFunction buildDropFunction(Token t, Expression func, boolean ifExists) {
		DropFunctionBuilder b = new DropFunctionBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfExists(ifExists);
		if (func != null) {
			b.setFunction(func);
			copyEndPosition(b.result(), func);
		}

		return b.result();
	}

	public DropTable buildDropTable(Token t, Expression table, boolean ifExists, boolean isTemporary, boolean purge) {
		DropTableBuilder b = new DropTableBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfExists(ifExists);
		b.setTemporary(isTemporary);
		b.setPurge(purge);
		if (table != null) {
			b.setTableList(table);
			copyEndPosition(b.result(), table);
		}

		return b.result();
	}

	public AlterTable buildAlterTable(Token t, Expression table) {
		AlterTableBuilder b = new AlterTableBuilder(asg);
		copyAllPosition(b.result(), t);
		if (!checkNull(table, "table")) {
			b.setTable(table);
			copyEndPosition(b.result(), table);
		}
		return b.result();
	}

	public DropView buildDropView(Token t, Expression view, boolean ifExists) {
		DropViewBuilder b = new DropViewBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfExists(ifExists);
		if (view != null) {
			b.setViewList(view);
			copyEndPosition(b.result(), view);
		}

		return b.result();
	}

	public Pragma buildPragma(Token t, Expression name) {
		PragmaBuilder b = new PragmaBuilder(asg);
		copyAllPosition(b.result(), t);
		if (name != null) {
			b.setName(name);
			copyEndPosition(b.result(), name);
		}

		return b.result();
	}

	public void pragmaSetValue(Pragma p, Expression value) {
		if (checkNull(p, "set") || checkNull(value, "value")) {
			return;
		}
		p.setValue(value);
		copyEndPosition(p, value);
	}

	public ShowCreateTable buildShowCreateTable(Token t, Expression expr) {
		ShowCreateTableBuilder b = new ShowCreateTableBuilder(asg);
		copyAllPosition(b.result(), t);
		if (!checkNull(expr, "expr")) {
			b.setTable(expr);
			copyEndPosition(b.result(), expr);
		}
		return b.result();
	}

	public Use buildUse(Token t, Expression expr) {
		UseBuilder b = new UseBuilder(asg);
		copyAllPosition(b.result(), t);
		if (!checkNull(expr, "expr")) {
			b.setDatabase(expr);
			copyEndPosition(b.result(), expr);

			if (parser.isInterpretSchema()) {
				Database db = retrieveDatabase(expr);
				if (db != null) {
					actualDatabase = db;
					actualSchema = retrieveDefaultSchema(db);
				} else {
					LOG.debug("Got null as database for expression: {}", expr);
				}
			}
		}
		return b.result();
	}

	public Set buildSet(Token t) {
		SetBuilder b = new SetBuilder(asg);
		copyAllPosition(b.result(), t);
		return b.result();
	}

	public void addSetValue(Set set, SetValue value) {
		if (checkNull(set, "set") || checkNull(value, "value")) {
			return;
		}
		set.addSetValue(value);
		copyEndPosition(set, value);
	}

	public SetValue buildSetValue(Expression key, Expression value) {
		SetValueBuilder b = new SetValueBuilder(asg);
		if (key != null) {
			b.setVariable(key);
			copyAllPosition(b.result(), key);
		}
		if (value != null) {
			b.setValue(value);
			copyEndPosition(b.result(), value);
		}

		return b.result();
	}

	public Delete buildDelete(Token delTok, Expression from) {
		DeleteBuilder b = new DeleteBuilder(asg);
		copyAllPosition(b.result(), delTok);

		if (!checkNull(from, "from")) {
			b.setFrom(from);
			copyEndPosition(b.result(), from);
		}

		return b.result();
	}

	public void deleteSetTableReferenceList(Delete del, ExpressionList tableRefList) {
		if (!checkNull(del, "del") && !checkNull(tableRefList, "tableRefList")) {
			del.setTableReferenceList(tableRefList);
			copyEndPosition(del, tableRefList);
		}
	}

	public void deleteSetUsing(Delete del, Expression using) {
		if (!checkNull(del, "del") && !checkNull(using, "using")) {
			del.setUsing(using);
			copyEndPosition(del, using);
		}
	}

	public void deleteSetWhere(Delete del, Expression where) {
		if (!checkNull(del, "del") && !checkNull(where, "where")) {
			del.setWhere(where);
			copyEndPosition(del, where);
		}
	}

	public void deleteSetOrder(Delete del, ExpressionList order) {
		if (!checkNull(del, "del") && !checkNull(order, "order")) {
			del.setOrder(order);
			copyEndPosition(del, order);
		}
	}

	public void deleteSetLimit(Delete del, Limit limit) {
		if (!checkNull(del, "del") && !checkNull(limit, "limit")) {
			del.setLimit(limit);
			copyEndPosition(del, limit);
		}
	}

	public Update buildUpdate(Token t, Expression table, ExpressionList set) {
		UpdateBuilder b = new UpdateBuilder(asg);
		copyAllPosition(b.result(), t);
		if (table != null) {
			b.setTableReferenceList(table);
			copyEndPosition(b.result(), table);
		}
		if (set != null) {
			b.setSetList(set);
			copyStartPosition(b.result(), set);
		}
		return b.result();
	}

	public void updateSetWhere(Update update, Expression where) {
		if (update != null && where != null) {
			update.setWhere(where);
		}
	}

	public void updateSetFrom(Update update, Expression from) {
		if (update != null && from != null) {
			update.setFrom(from);
		}
	}

	public void updateSetOrderBy(Update update, ExpressionList orderBy) {
		if (update != null && orderBy != null) {
			update.setOrderBy(orderBy);
		}
	}

	public void updateSetLimit(Update update, Limit limit) {
		if (update != null && limit != null) {
			update.setLimit(limit);
		}
	}

	public Insert buildInsert(Token t, boolean overwrite, Expression table, ExpressionList with) {
		InsertBuilder b = new InsertBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setOverwrite(overwrite);
		if (table != null) {
			b.setTable(table);
			copyEndPosition(b.result(), table);
		}
		if (with != null) {
			b.setWith(with);
			copyStartPosition(b.result(), with);
		}
		return b.result();
	}

	public void insertSetColumnList(Insert insert, ExpressionList cols) {
		if (insert != null && cols != null) {
			insert.setColumnList(cols);
			copyEndPosition(insert, cols);
		}
	}

	public void insertSetQuery(Insert insert, Query query) {
		if (insert != null && query != null) {
			insert.setQuery(query);
			copyEndPosition(insert, query);
		}
	}

	public void insertSetPartition(Insert insert, ExpressionList list) {
		if (insert != null && list != null) {
			insert.setPartition(list);
			copyEndPosition(insert, list);
		}
	}

	public void insertSetSetList(Insert insert, ExpressionList list) {
		if (insert != null && list != null) {
			insert.setSetList(list);
			copyEndPosition(insert, list);
		}
	}

	public Select buildSelect(Query query) {
		SelectBuilder b = new SelectBuilder(asg);
		b.setQuery(query);
		copyAllPosition(b.result(), query);
		return b.result();
	}

	public void querySetWith(Query query, ExpressionList list) {
		query.setWith(list);
	}

	public void querySetOrderBy(Query query, ExpressionList elements) {
		if (query != null && elements != null) {
			query.setOrderBy(elements);
		}
	}

	public void querySetLimit(Query query, Limit limit) {
		if (query != null && limit != null) {
			query.setLimit(limit);
		}
	}

	public SelectExpression buildSelectExpression(Token t, Token quantifierTok, Expression list) {
		SelectExpressionBuilder b = new SelectExpressionBuilder(asg);
		copyAllPosition(b.result(), t);
		if (list != null) {
			b.setColumnList(list);
			copyEndPosition(b.result(), list);
		}
		if (quantifierTok != null) {
			b.setQuantifierKind(toSetQuantifierKind(quantifierTok));
		}
		return b.result();
	}

	public void selectExpressionOptions(SelectExpression select, List<Token> options) {
		if (checkNull(select, "select") || checkNull(options, "options")) {
			return;
		}

		// TODO: it would be nicer to support all the select options here
		for (Token opt : options) {
			if (opt.getType() == MySQLLexer.ALL || opt.getType() == MySQLLexer.DISTINCT) {
				select.setQuantifierKind(toSetQuantifierKind(opt));
			}
		}

	}

	public void selectExpressionSetFrom(SelectExpression select, Expression from) {
		if (checkNull(select, "select") || checkNull(from, "from")) {
			return;
		}

		select.setFrom(from);
		copyEndPosition(select, from);
	}

	public void selectExpressionSetInto(SelectExpression select, ExpressionList into) {
		if (checkNull(select, "select") || checkNull(into, "into")) {
			return;
		}

		select.setInto(into);
		copyEndPosition(select, into);
	}

	public void selectExpressionSetWhere(SelectExpression select, Expression where) {
		if (checkNull(select, "select") || checkNull(where, "where")) {
			return;
		}

		select.setWhere(where);
		copyEndPosition(select, where);
	}

	public void selectExpressionSetGroupBy(SelectExpression select, ExpressionList list) {
		if (checkNull(select, "select") || checkNull(list, "list")) {
			return;
		}
		select.setGroupBy(list);
		copyEndPosition(select, list);
	}

	public void selectExpressionSetHaving(SelectExpression select, Expression expr) {
		if (checkNull(select, "select") || checkNull(expr, "expr")) {
			return;
		}
		select.setHaving(expr);
		copyEndPosition(select, expr);
	}

	public Limit buildLimit(Token t, Expression expr, Expression offset) {
		LimitBuilder b = new LimitBuilder(asg);
		if (t != null) {
			copyAllPosition(b.result(), t);
		}
		if (expr != null) {
			b.setRowcount(expr);
			copyEndPosition(b.result(), expr);
		}
		if (offset != null) {
			b.setOffset(offset);
		}

		return b.result();
	}

	public void limitSetOffset(Limit limit, Expression expr) {
		if (limit != null && expr != null) {
			limit.setOffset(expr);
			copyEndPosition(limit, expr);
		}
	}

	public OrderByElement buildOrderByElement(Expression expr) {
		OrderByElementBuilder b = new OrderByElementBuilder(asg);
		if (!checkNull(expr, "expr")) {
			b.setExpression(expr);
			copyAllPosition(b.result(), expr);
		}
		b.setKind(OrderByElementKind.NONE);
		b.setNullOrder(NullOrderKind.NONE);
		return b.result();
	}

	public void orderByElementSetKind(OrderByElement order, Token t) {
		if (checkNull(order, "order") || checkNull(t, "t")) {
			return;
		}

		order.setKind(toOrderByElementKind(t));
		copyEndPosition(order, t);
	}

	public void orderByElementSetNullOrder(OrderByElement order, Token t1, Token t2) {
		if (checkNull(order, "order") || checkNull(t1, "t1") || checkNull(t2, "t2")) {
			return;
		}
		order.setNullOrder(toNullOrderKind(t1, t2));
		copyEndPosition(order, t2);
	}

	public Values buildValues(Token t, Expression list) {
		ValuesBuilder b = new ValuesBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setList(list);
		return b.result();
	}

	public Row buildRow(Token t, ExpressionList list) {
		RowBuilder b = new RowBuilder(asg);

		if (!checkNull(t, "t")) {
			copyAllPosition(b.result(), t);
		}

		b.setList(list);

		return b.result();
	}

	public BinaryQuery buildBinaryQuery(Token t1, Token t2, Query left, Query right) {
		BinaryQueryBuilder b = new BinaryQueryBuilder(asg);
		b.setKind(toBinaryQueryKind(t1, t2));
		copyAllPosition(b.result(), t1);
		b.setLeft(left);
		copyStartPosition(b.result(), left);
		b.setRight(right);
		copyEndPosition(b.result(), right);
		return b.result();
	}

	public Default buildDefault(Token t) {
		DefaultBuilder db = new DefaultBuilder(asg);
		copyAllPosition(db.result(), t);
		return db.result();
	}

	public MatchAgainst buildMatchAgainst(Token t, Expression against, ExpressionList list) {
		MatchAgainstBuilder b = new MatchAgainstBuilder(asg);

		if (!checkNull(t, "t")) {
			copyAllPosition(b.result(), t);
		}

		if (!checkNull(against, "against")) {
			b.setAgainst(against);
			copyEndPosition(b.result(), against);
		}

		if (!checkNull(list, "list")) {
			b.setIdentList(list);
			copyEndPosition(b.result(), list);
		}
		return b.result();
	}

	public ExpressionList buildExpressionList(Expression expr) {
		ExpressionListBuilder b = new ExpressionListBuilder(asg);
		b.addExpression(expr);
		copyAllPosition(b.result(), expr);
		return b.result();
	}

	public ExpressionList buildExpressionList(Expression expr1, Expression expr2) {
		ExpressionListBuilder b = new ExpressionListBuilder(asg);
		b.addExpression(expr1);
		copyAllPosition(b.result(), expr1);
		b.addExpression(expr2);
		copyAllPosition(b.result(), expr2);
		return b.result();
	}

	public ExpressionList buildExpressionList(List<Expression> exprs) {
		ExpressionListBuilder b = new ExpressionListBuilder(asg);
		for (Expression expr : exprs) {
			b.addExpression(expr);
			copyAllPosition(b.result(), expr);
		}
		return b.result();
	}

	public ExpressionList buildOrAddToExpressionList(Expression left, Expression expr) {
		ExpressionList list;
		if (left instanceof ExpressionList) {
			list = (ExpressionList) left;
		} else {
			ExpressionListBuilder eb = new ExpressionListBuilder(asg);
			list = eb.result();
			copyAllPosition(list, left);
			list.addExpression(left);
		}

		if (expr != null) {
			list.addExpression(expr);
			copyEndPosition(list, expr);
		}

		return list;
	}

	public void addToExpressionList(ExpressionList list, Expression expr) {
		if (list == null) {
			LOG.trace("Null list in addToExpressionList!");
			return;
		}
		if (expr == null) {
			LOG.trace("Null expr in addToExpressionList!;");
			return;
		}

		list.addExpression(expr);
		copyEndPosition(list, expr);
	}

	public ExpressionList getExpressionList(Expression expr) {
		if (expr instanceof ExpressionList) {
			return (ExpressionList) expr;
		} else {
			return buildExpressionList(expr);
		}
	}

	public BinaryExpression buildBinaryExpression(Token t, Expression left, Expression right) {
		BinaryExpressionBuilder b = new BinaryExpressionBuilder(asg);
		b.setKind(toBinaryExpressionKind(t));
		copyAllPosition(b.result(), t);

		setBinaryExpressionLeftRight(b, left, right);

		return b.result();
	}

	public BinaryExpression buildBinaryExpression(Token t1, Token t2, Expression left, Expression right) {
		BinaryExpressionBuilder b = new BinaryExpressionBuilder(asg);
		b.setKind(toBinaryExpressionKind(t1, t2));
		copyAllPosition(b.result(), t1);

		setBinaryExpressionLeftRight(b, left, right);

		return b.result();
	}

	public BinaryExpression buildBinaryExpression(Token t1, Token t2, Token t3, Expression left, Expression right) {
		BinaryExpressionBuilder b = new BinaryExpressionBuilder(asg);
		b.setKind(toBinaryExpressionKind(t1, t2, t3));
		copyAllPosition(b.result(), t1);

		setBinaryExpressionLeftRight(b, left, right);

		return b.result();
	}

	private void setBinaryExpressionLeftRight(BinaryExpressionBuilder b, Expression left, Expression right) {
		if (left != null) {
			b.setLeft(left);
			copyStartPosition(b.result(), left);
		}

		if (right != null) {
			b.setRight(right);
			copyEndPosition(b.result(), right);
		}
	}

	public SetVar buildSetVar(Token tok, Expression var, Expression value) {
		SetVarBuilder b = new SetVarBuilder(asg);
		copyAllPosition(b.result(), tok);

		if (!checkNull(var, "var") && !checkNull(value, "value")) {
			b.setSetValue(buildSetValue(var, value));
			copyEndPosition(b.result(), value);
		}

		return b.result();
	}

	public ColumnAttribute buildColumnAttribute(Token t1) {
		ColumnAttributeBuilder b = new ColumnAttributeBuilder(asg);
		b.setKind(toColumnAttributeKind(t1, null));
		return b.result();
	}

	public ColumnAttribute buildColumnAttribute(Token t1, Expression expr) {
		ColumnAttribute attr = buildColumnAttribute(t1);
		if (!checkNull(expr, "expr")) {
			attr.setExpression(expr);
		}
		return attr;
	}

	public ColumnAttribute buildColumnAttribute(Token t1, Token t2) {
		ColumnAttributeBuilder b = new ColumnAttributeBuilder(asg);
		b.setKind(toColumnAttributeKind(t1, t2));
		return b.result();
	}

	public Asterisk buildAsterisk(Token tok) {
		AsteriskBuilder b = new AsteriskBuilder(asg);
		copyAllPosition(b.result(), tok);
		return b.result();
	}

	public Joker buildJoker(Token tok) {
		JokerBuilder b = new JokerBuilder(asg);
		copyAllPosition(b.result(), tok);
		return b.result();
	}

	public Join buildJoin(Expression left, Expression right, Token tok) {
		JoinBuilder b = new JoinBuilder(asg);
		b.setKind(toJoinKind(tok));
		copyAllPosition(b.result(), tok);
		b.setLeft(left);
		copyStartPosition(b.result(), left);
		b.setRight(right);
		copyEndPosition(b.result(), right);
		return b.result();
	}

	public Join buildJoin(Expression left, Expression right, Token tok1, Token tok2) {
		JoinBuilder b = new JoinBuilder(asg);
		if (tok2 == null) {
			b.setKind(toJoinKind(tok1));
		} else {
			b.setKind(toJoinKind(tok1, tok2));
		}
		if (tok1 != null) {
			copyAllPosition(b.result(), tok1);
		}
		b.setLeft(left);
		copyStartPosition(b.result(), left);
		b.setRight(right);
		copyEndPosition(b.result(), right);
		return b.result();
	}

	public Join buildJoin(Expression left, Expression right, Token tok1, Token tok2, JoinConditionClause cond) {
		Join join = buildJoin(left, right, tok1, tok2);
		if (join != null && cond != null) {
			join.setCondition(cond);
		}
		return join;
	}

	public void joinSetCondition(Join join, JoinConditionClause cond) {
		if (join != null) {
			join.setCondition(cond);
		}
	}

	public On buildOn(Expression expr, Token t) {
		OnBuilder b = new OnBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setExpression(expr);
		copyEndPosition(b.result(), expr);
		return b.result();
	}

	public Using buildUsing(ExpressionList list, Token t) {
		UsingBuilder b = new UsingBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setColumnList(list);
		copyEndPosition(b.result(), list);
		return b.result();
	}

	public UserFunctionCall buildUserFunctionCall(Token t) {
		UserFunctionCallBuilder b = new UserFunctionCallBuilder(asg);
		Id callId = buildId(t);
		b.setExpression(callId);
		copyAllPosition(b.result(), t);
		return b.result();
	}

	public UserFunctionCall buildUserFunctionCall(Expression expr) {
		UserFunctionCallBuilder b = new UserFunctionCallBuilder(asg);
		b.setExpression(expr);
		copyAllPosition(b.result(), expr);
		return b.result();
	}

	public UserFunctionCall buildUserFunctionCall(Expression expr, FunctionParams params) {
		UserFunctionCallBuilder b = new UserFunctionCallBuilder(asg);
		b.setExpression(expr);
		copyAllPosition(b.result(), expr);
		b.setParamList(params);
		return b.result();
	}

	public InnerFunctionCall buildInnerFunctionCall(Token t) {
		InnerFunctionCallBuilder b = new InnerFunctionCallBuilder(asg);
		Function func = retrieveInnerFunction(t.getText(), null);
		Id callId = buildId(t);
		callId.setRefersTo(func);
		b.setExpression(callId);
		copyAllPosition(b.result(), t);
		return b.result();
	}

	public InnerFunctionCall buildInnerFunctionCall(Expression idExpr) {
		if (!(idExpr instanceof Id)) {
			LOG.warn("Function name null for inner function call. Joker node?");
			return null;
		}
		Id id = (Id) idExpr;
		Function func = retrieveInnerFunction(id.getName(), null);
		id.setRefersTo(func);
		InnerFunctionCallBuilder b = new InnerFunctionCallBuilder(asg);
		b.setExpression(id);
		copyAllPosition(b.result(), id);
		return b.result();
	}

	public InnerFunctionCall buildInnerFunctionCall(Token t, FunctionParams params) {
		InnerFunctionCallBuilder b = new InnerFunctionCallBuilder(asg);
		Function func = retrieveInnerFunction(t.getText(), params);
		Id callId = buildId(t);
		callId.setRefersTo(func);
		b.setExpression(callId);
		copyAllPosition(b.result(), t);
		if (params != null) {
			b.setParamList(params);
		}
		return b.result();
	}

	public InnerFunctionCall buildInnerFunctionCall(Token t, ExpressionList list) {
		return buildInnerFunctionCall(t, buildFunctionParams(list));
	}

	public InnerFunctionCall buildInnerFunctionCall(Token t, Expression... exprs) {
		ExpressionListBuilder lb = new ExpressionListBuilder(asg);
		for (Expression e : exprs) {
			lb.addExpression(e);
		}
		return buildInnerFunctionCall(t, lb.result());
	}

	public void functionCallSetParamList(FunctionCall call, FunctionParams params) {
		call.setParamList(params);
	}

	public FunctionParams buildFunctionParams(Expression expr) {
		return buildFunctionParams(null, null, expr);
	}

	public FunctionParams buildFunctionParams(Token t1, Token t2, Expression expr) {
		FunctionParamsBuilder b = new FunctionParamsBuilder(asg);
		b.setKind(toFunctionParamsKind(t1, t2));
		b.setParameter(expr);
		copyAllPosition(b.result(), expr);
		if (t1 != null && t2 == null) { // ALL|DISTINCT expr
			copyStartPosition(b.result(), t1);
		}
		if (t2 != null) { // expr IGNORE NULLS
			copyEndPosition(b.result(), t2);
		}
		return b.result();
	}

	public Literal buildLiteral(Token t) {
		LiteralBuilder b = new LiteralBuilder(asg);
		b.setKind(toLiteralKind(t));
		b.setValue(t.getText());
		copyAllPosition(b.result(), t);
		return b.result();
	}

	public Literal tokenToLiteral(Token t) {
		LiteralBuilder b = new LiteralBuilder(asg);
		b.setKind(LiteralKind.STRING);
		b.setValue(t.getText());
		copyAllPosition(b.result(), t);
		return b.result();
	}

	public Expression addLiteralSign(Token sign, Expression expr) {
		if (expr instanceof Literal) {
			Literal lit = (Literal) expr;
			if (lit.getKind() == LiteralKind.INTEGER || lit.getKind() == LiteralKind.DECIMAL) {
				lit.setValue(sign.getText() + lit.getValue());
			} else {
				LOG.trace("Invalid literal in addLiteralSign: {}", lit.getKind());
			}
			return expr;
		} else {
			return buildUnaryExpression(sign, expr);
		}
	}

	public UnaryExpression buildUnaryExpression(Token t, Expression expr) {
		UnaryExpressionBuilder b = new UnaryExpressionBuilder(asg);
		b.setKind(toUnaryExpressionKind(t));
		copyAllPosition(b.result(), t);
		b.setExpression(expr);
		copyEndPosition(b.result(), expr);
		return b.result();
	}

	public Interval buildInterval(Token intervalTok, Expression expr, Token unitTok) {
		IntervalBuilder b = new IntervalBuilder(asg);
		copyAllPosition(b.result(), intervalTok);
		b.setKind(toTimeUnitKind(unitTok));
		copyEndPosition(b.result(), unitTok);
		b.setValue(expr);
		return b.result();
	}

	public IsNull buildIsNull(Expression expr, boolean isNot, Token t1, Token t2) {
		IsNullBuilder b = new IsNullBuilder(asg);
		copyAllPosition(b.result(), t2);
		b.setExpression(expr);
		copyStartPosition(b.result(), expr);
		b.setNot(isNot);
		return b.result();
	}

	public Between buildBetween(Token tok, Expression e1, Expression e2, Expression e3, boolean isNot) {
		BetweenBuilder b = new BetweenBuilder(asg);
		copyAllPosition(b.result(), tok);
		if (e1 != null) {
			b.setFirst(e1);
			copyEndPosition(b.result(), e1);
		}
		if (e2 != null) {
			b.setSecond(e2);
			copyEndPosition(b.result(), e2);
		}
		if (e3 != null) {
			b.setThird(e3);
			copyEndPosition(b.result(), e3);
		}
		b.setIsNot(isNot);
		return b.result();
	}

	public Cast buildCast(Token tok, Expression arg, Type toType) {
		CastBuilder b = new CastBuilder(asg);
		copyAllPosition(b.result(), tok);
		if (arg != null) {
			b.setExpression(arg);
			copyEndPosition(b.result(), arg);
		}
		if (toType != null) {
			b.setToType(toType);
			copyEndPosition(b.result(), arg);
		}
		return b.result();
	}

	public Case buildCase(Token t, Expression selector) {
		CaseBuilder b = new CaseBuilder(asg);
		copyAllPosition(b.result(), t);
		if (selector != null) {
			b.setSelector(selector);
			copyEndPosition(b.result(), selector);
		}
		return b.result();
	}

	public Case buildCase(Token t, Expression selector, List<WhenClause> whenList, Expression elseExpr) {
		CaseBuilder b = new CaseBuilder(asg);
		copyAllPosition(b.result(), t);
		if (selector != null) {
			b.setSelector(selector);
			copyEndPosition(b.result(), selector);
		}

		for (WhenClause when : whenList) {
			caseAddWhenClause(b.result(), when);
		}

		if (elseExpr != null) {
			b.setElse(elseExpr);
			copyEndPosition(b.result(), elseExpr);
		}

		return b.result();

	}

	public void caseAddWhenClause(Case c, WhenClause when) {
		if (c != null && when != null) {
			c.addWhen(when);
			copyEndPosition(c, when);
		}
	}

	public void caseAddWhenClauseList(Case c, List<WhenClause> whenList) {
		if (c != null) {
			for (WhenClause when : whenList) {
				caseAddWhenClause(c, when);
			}
		}
	}

	public void caseSetElse(Case c, Expression elseExpr) {
		if (c != null && elseExpr != null) {
			c.setElse(elseExpr);
		}
	}

	public WhenClause buildWhenClause(Token t, Expression whenExpr, Expression thenExpr) {
		WhenClauseBuilder b = new WhenClauseBuilder(asg);
		copyAllPosition(b.result(), t);
		if (whenExpr != null) {
			b.setCondition(whenExpr);
			copyEndPosition(b.result(), whenExpr);
		}
		if (thenExpr != null) {
			b.setResult(thenExpr);
			copyEndPosition(b.result(), thenExpr);
		}
		return b.result();
	}

	// POSITION INFORMATION

	public void copyStartPosition(Base to, Token t) {
		if (checkNull(to, "to") || checkNull(t, "t")) {
			return;
		}
		to.setPath(parser.getInputName());
		to.setStartLine(t.getLine() + parser.getInitialRow());
		to.setStartCol(t.getCharPositionInLine());
	}

	public void copyStartPosition(Base to, Base from) {
		if (checkNull(to, "to") || checkNull(from, "from")) {
			return;
		}
		to.setPath(from.getPath());
		to.setStartLine(from.getStartLine());
		to.setStartCol(from.getStartCol());

	}

	public void copyEndPosition(Base to, Token t) {
		if (checkNull(to, "to") || checkNull(t, "t")) {
			return;
		}
		to.setEndLine(t.getLine() + parser.getInitialRow());
		to.setEndCol((t.getStopIndex() - t.getStartIndex()) + t.getCharPositionInLine());
	}

	public void copyEndPosition(Base to, Base from) {
		if (checkNull(to, "to") || checkNull(from, "from")) {
			return;
		}
		to.setEndLine(from.getEndLine());
		to.setEndCol(from.getEndCol());

	}

	public void copyAllPosition(Base to, Base from) {
		if (checkNull(to, "to") || checkNull(from, "from")) {
			return;
		}
		copyStartPosition(to, from);
		copyEndPosition(to, from);
	}

	public void copyAllPosition(Base to, Token t) {
		if (checkNull(to, "to") || checkNull(t, "t")) {
			return;
		}
		copyStartPosition(to, t);
		copyEndPosition(to, t);
	}

	protected String tokenToString(Token tok) {
		return tok == null ? NULLSTR : tok.toString();
	}

	protected abstract SetQuantifierKind toSetQuantifierKind(Token tok);

	protected abstract BinaryQueryKind toBinaryQueryKind(Token t1, Token t2);

	protected abstract AliasKind toAliasKind(Token tok);

	protected abstract JoinKind toJoinKind(Token tok);

	protected abstract JoinKind toJoinKind(Token tok, Token tok2);

	protected abstract UnaryExpressionKind toUnaryExpressionKind(Token tok);

	protected abstract BinaryExpressionKind toBinaryExpressionKind(Token tok);

	protected abstract BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2);

	protected abstract BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2, Token t3);

	protected abstract LiteralKind toLiteralKind(Token tok);

	protected abstract OrderByElementKind toOrderByElementKind(Token t);

	protected abstract NullOrderKind toNullOrderKind(Token t1, Token t2);

	protected abstract NumericTypeKind toNumericTypeKind(Token tok);

	protected abstract DateTypeKind toDateTypeKind(Token tok);

	protected abstract StringTypeKind toStringTypeKind(Token tok);

	protected abstract ColumnAttributeKind toColumnAttributeKind(Token tok1, Token tok2);

	protected abstract FunctionParamsKind toFunctionParamsKind(Token t1, Token t2);

	protected abstract TimeUnitKind toTimeUnitKind(Token t);
}
