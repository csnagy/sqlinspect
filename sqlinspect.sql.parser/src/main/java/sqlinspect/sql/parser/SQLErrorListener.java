package sqlinspect.sql.parser;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLErrorListener extends BaseErrorListener {

	private static final Logger LOG = LoggerFactory.getLogger(SQLErrorListener.class);

	private static final SQLErrorListener instance = new SQLErrorListener();

	private static final List<SQLError> errors = new ArrayList<>();

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		errors.add(new SQLError(line, charPositionInLine, msg, e));
		LOG.error("line {}: {} {}", line, charPositionInLine, msg);
	}

	public void clear() {
		errors.clear();
	}

	public SQLError getLast() {
		if (!errors.isEmpty()) {
			return errors.get(errors.size() - 1);
		} else {
			return null;
		}
	}

	public static SQLErrorListener getInstance() {
		return instance;
	}
}
