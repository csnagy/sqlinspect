package sqlinspect.sql.parser;

import java.util.Locale;

import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.builder.AliasBuilder;
import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.expr.Alias;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.Joker;
import sqlinspect.sql.asg.expr.Literal;
import sqlinspect.sql.asg.kinds.AliasKind;
import sqlinspect.sql.asg.kinds.BinaryExpressionKind;
import sqlinspect.sql.asg.kinds.BinaryQueryKind;
import sqlinspect.sql.asg.kinds.ColumnAttributeKind;
import sqlinspect.sql.asg.kinds.DateTypeKind;
import sqlinspect.sql.asg.kinds.FunctionParamsKind;
import sqlinspect.sql.asg.kinds.JoinKind;
import sqlinspect.sql.asg.kinds.LiteralKind;
import sqlinspect.sql.asg.kinds.NullOrderKind;
import sqlinspect.sql.asg.kinds.NumericTypeKind;
import sqlinspect.sql.asg.kinds.OrderByElementKind;
import sqlinspect.sql.asg.kinds.SetQuantifierKind;
import sqlinspect.sql.asg.kinds.StringTypeKind;
import sqlinspect.sql.asg.kinds.TimeUnitKind;
import sqlinspect.sql.asg.kinds.UnaryExpressionKind;
import sqlinspect.sql.asg.type.Type;

public final class SQLiteParserActions extends SQLParserActions {
	private static final Logger LOG = LoggerFactory.getLogger(SQLiteParserActions.class);

	public SQLiteParserActions(SQLParser parser) {
		super(parser);
	}

	public Alias buildAlias(Expression left, Expression right, Token t) {
		AliasBuilder b = new AliasBuilder(asg);
		b.setExpression(left);
		b.setKind(toAliasKind(t));

		if (right instanceof Id) {
			Id rId = (Id) right;
			b.setName(rId.getName());
		} else if (right instanceof Literal) {
			Literal rLiteral = (Literal) right;
			b.setName(rLiteral.getValue());
		} else if (right instanceof Joker) {
			Joker jok = (Joker) right;
			b.setName(jok.getName());
		} else {
			LOG.debug("Alias without unsupported right side: {} (left: {})", right, left);
		}

		copyAllPosition(b.result(), left);
		copyEndPosition(b.result(), right);

		return b.result();
	}

	/*
	 * Determination Of Column Affinity
	 *
	 * The affinity of a column is determined by the declared type of the column,
	 * according to the following rules in the order shown:
	 *
	 * If the declared type contains the string "INT" then it is assigned INTEGER
	 * affinity.
	 *
	 * If the declared type of the column contains any of the strings "CHAR",
	 * "CLOB", or "TEXT" then that column has TEXT affinity. Notice that the type
	 * VARCHAR contains the string "CHAR" and is thus assigned TEXT affinity.
	 *
	 * If the declared type for a column contains the string "BLOB" or if no type is
	 * specified then the column has affinity BLOB.
	 *
	 * If the declared type for a column contains any of the strings "REAL", "FLOA",
	 * or "DOUB" then the column has REAL affinity.
	 *
	 * Otherwise, the affinity is NUMERIC.
	 *
	 * Note that the order of the rules for determining column affinity is
	 * important. A column whose declared type is "CHARINT" will match both rules 1
	 * and 2 but the first rule takes precedence and so the column affinity will be
	 * INTEGER.
	 *
	 * https://www.sqlite.org/datatype3.html
	 */
	public Type newSQLiteType(Expression typeExpr, Expression precExpr, Expression scaleExpr) {

		int precision = 0;
		int scale = 0;

		if (precExpr instanceof Literal) {
			Literal prec = (Literal) precExpr;
			precision = Integer.parseInt(prec.getValue());
		}

		if (scaleExpr instanceof Literal) {
			Literal sc = (Literal) scaleExpr;
			scale = Integer.parseInt(sc.getValue());
		}

		String typeName = "";
		if (typeExpr instanceof Id) {
			typeName = ((Id) typeExpr).getName();
		}

		if (typeName.contains("INT")) {
			return retrieveNumericType(NumericTypeKind.INT, precision, scale, false, false);
		} else if (typeName.contains("CLOB") || typeName.contains("CHAR") || typeName.contains("TEXT")) {
			return retrieveStringType(StringTypeKind.TEXT, precision);
		} else if (typeName.contains("BLOB")) {
			return retrieveStringType(StringTypeKind.BLOB, precision);
		} else if (typeName.contains("REAL") || typeName.contains("FLOA") || typeName.contains("DOUB")) {
			return retrieveNumericType(NumericTypeKind.FLOAT, precision, scale, false, false);
		} else {
			return retrieveNumericType(NumericTypeKind.NUMERIC, precision, scale, false, false);
		}
	}

	@Override
	protected AliasKind toAliasKind(Token tok) {
		if (tok == null) {
			return AliasKind.NONE;
		} else if (tok.getType() == SQLiteLexer.K_AS) {
			return AliasKind.AS;
		} else {
			throw new ASGException("Invalid token in toAliasKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok) {
		if (tok == null) {
			return JoinKind.INNER;
		}

		switch (tok.getType()) {
		case SQLiteLexer.COMMA:
			return JoinKind.COMMA;
		case SQLiteLexer.K_CROSS:
			return JoinKind.CROSS;
		case SQLiteLexer.K_NATURAL:
			return JoinKind.NATURAL;
		case SQLiteLexer.K_LEFT:
			return JoinKind.LEFTOUTER;
		case SQLiteLexer.K_INNER:
			return JoinKind.INNER;
		default:
			throw new ASGException("Invalid token in toJoinKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok1, Token tok2) {
		if (tok1 != null && tok1.getType() == SQLiteLexer.K_LEFT && tok2 != null
				&& tok2.getType() == SQLiteLexer.K_OUTER) {
			return JoinKind.LEFTOUTER;

		}

		throw new ASGException("Invalid tokens in toJoinKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
	}

	@Override
	protected UnaryExpressionKind toUnaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toUnaryExpressionKind!");
		}

		switch (tok.getType()) {
		case SQLiteLexer.PLUS:
			return UnaryExpressionKind.PLUS;
		case SQLiteLexer.MINUS:
			return UnaryExpressionKind.MINUS;
		case SQLiteLexer.K_NOT:
			return UnaryExpressionKind.NOT;
		case SQLiteLexer.K_ASC:
			return UnaryExpressionKind.ASCENDING;
		case SQLiteLexer.K_DESC:
			return UnaryExpressionKind.DESCENDING;
		case SQLiteLexer.LPAREN:
		case SQLiteLexer.RPAREN:
			return UnaryExpressionKind.PARENS;
		case SQLiteLexer.TILDE:
			return UnaryExpressionKind.BITNOT;
		case SQLiteLexer.K_EXISTS:
			return UnaryExpressionKind.EXISTS;
		default:
			throw new ASGException("Invalid token in toUnaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toBinaryExpressionKind!");
		}

		switch (tok.getType()) {
		case SQLiteLexer.PLUS:
			return BinaryExpressionKind.PLUS;
		case SQLiteLexer.MINUS:
			return BinaryExpressionKind.MINUS;
		case SQLiteLexer.ASTERISK:
			return BinaryExpressionKind.MULTIPLY;
		case SQLiteLexer.DIV:
			return BinaryExpressionKind.DIVIDE;
		case SQLiteLexer.MOD:
			return BinaryExpressionKind.MODULO;
		case SQLiteLexer.K_AND:
			return BinaryExpressionKind.AND;
		case SQLiteLexer.K_OR:
		case SQLiteLexer.PIPE2:
			return BinaryExpressionKind.OR;
		case SQLiteLexer.AMP:
			return BinaryExpressionKind.BITAND;
		case SQLiteLexer.PIPE:
			return BinaryExpressionKind.BITOR;
		case SQLiteLexer.XOR:
			return BinaryExpressionKind.BITXOR;
		case SQLiteLexer.ASSIGN:
		case SQLiteLexer.EQ:
			return BinaryExpressionKind.EQUALS;
		case SQLiteLexer.NOT_EQ1:
		case SQLiteLexer.NOT_EQ2:
			return BinaryExpressionKind.NOTEQUALS;
		case SQLiteLexer.GT:
			return BinaryExpressionKind.GREATERTHAN;
		case SQLiteLexer.LT:
			return BinaryExpressionKind.LESSTHAN;
		case SQLiteLexer.GT_EQ:
			return BinaryExpressionKind.GREATEROREQUALS;
		case SQLiteLexer.LT_EQ:
			return BinaryExpressionKind.LESSOREQUALS;
		case SQLiteLexer.K_LIKE:
			return BinaryExpressionKind.LIKE;
		case SQLiteLexer.K_IS:
			return BinaryExpressionKind.IS;
		case SQLiteLexer.DOT:
			return BinaryExpressionKind.FIELDSELECTOR;
		case SQLiteLexer.K_IN:
			return BinaryExpressionKind.IN;
		case SQLiteLexer.K_REGEXP:
			return BinaryExpressionKind.REGEXP;
		case SQLiteLexer.K_GLOB:
			return BinaryExpressionKind.GLOB;
		case SQLiteLexer.K_MATCH:
			return BinaryExpressionKind.MATCH;
		default:
			throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2) {
		throw new ASGException(
				"Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2, Token t3) {
		throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", "
				+ tokenToString(t2) + ", " + tokenToString(t3));
	}

	@Override
	protected LiteralKind toLiteralKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toLiteralKind!");
		}

		switch (tok.getType()) {

		case SQLiteLexer.INTEGER_LITERAL:
			return LiteralKind.INTEGER;
		case SQLiteLexer.DECIMAL_LITERAL:
			return LiteralKind.DECIMAL;
		case SQLiteLexer.FLOATINGPOINT_LITERAL:
			return LiteralKind.FLOAT;
		case SQLiteLexer.STRING_LITERAL:
			return LiteralKind.STRING;
		case SQLiteLexer.BLOB_LITERAL:
			return LiteralKind.BLOB;
		case SQLiteLexer.K_NULL:
			return LiteralKind.NULL;
		case SQLiteLexer.K_TRUE:
		case SQLiteLexer.K_FALSE:
			return LiteralKind.BOOLEAN;
		case SQLiteLexer.K_CURRENT_DATE:
			return LiteralKind.CURRENTDATE;
		case SQLiteLexer.K_CURRENT_TIME:
			return LiteralKind.CURRENTTIME;
		case SQLiteLexer.K_CURRENT_TIMESTAMP:
			return LiteralKind.CURRENTTIMESTAMP;
		default:
			throw new ASGException("Invalid token in toLiteralKind: " + tokenToString(tok));
		}
	}

	@Override
	public BinaryQueryKind toBinaryQueryKind(Token t1, Token t2) {
		if (t1 != null && t1.getType() == SQLiteLexer.K_UNION) {
			if (t2 != null) {
				switch (t2.getType()) {
				case SQLiteLexer.K_ALL:
					return BinaryQueryKind.UNIONALL;
				case SQLiteLexer.K_DISTINCT:
					return BinaryQueryKind.UNIONDISTINCT;
				default:
					throw new ASGException(
							"Invalid Union in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
				}
			} else {
				return BinaryQueryKind.UNION;
			}
		}
		throw new ASGException("Invalid tokens in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected SetQuantifierKind toSetQuantifierKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toSetQuantifierKind!");
		}

		switch (tok.getType()) {
		case SQLiteLexer.K_ALL:
			return SetQuantifierKind.ALL;
		case SQLiteLexer.K_DISTINCT:
			return SetQuantifierKind.DISTINCT;
		default:
			throw new ASGException("Invalid token in toSqtQuantifierKind: " + tokenToString(tok));
		}
	}

	@Override
	protected ColumnAttributeKind toColumnAttributeKind(Token tok1, Token tok2) {
		if (tok1 == null) {
			throw new ASGException("Null token in toColumnAttributeKind!");
		}

		switch (tok1.getType()) {
		case SQLiteLexer.K_NULL:
			if (tok2 != null && tok2.getType() == SQLiteLexer.K_NOT) {
				return ColumnAttributeKind.NOTNULL;
			} else {
				return ColumnAttributeKind.NULL;
			}
		case SQLiteLexer.K_DEFAULT:
			return ColumnAttributeKind.DEFAULT;
		case SQLiteLexer.K_AUTOINCREMENT:
			return ColumnAttributeKind.AUTOINCREMENT;
		case SQLiteLexer.K_PRIMARY:
			return ColumnAttributeKind.PRIMARYKEY;
		case SQLiteLexer.K_UNIQUE:
			return ColumnAttributeKind.UNIQUE;
		case SQLiteLexer.K_CHECK:
			return ColumnAttributeKind.CHECK;
		default:
			throw new ASGException(
					"Invalid token in toColumnAttributeKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
		}
	}

	public OrderByElementKind toOrderByElementKind1(Token t) {
		return toOrderByElementKind(t);
	}

	@Override
	protected OrderByElementKind toOrderByElementKind(Token t) {
		if (t == null) {
			return OrderByElementKind.NONE;
		} else if (t.getType() == SQLiteLexer.K_ASC) {
			return OrderByElementKind.ASC;
		} else if (t.getType() == SQLiteLexer.K_DESC) {
			return OrderByElementKind.DESC;
		} else {
			throw new ASGException("Invalid token in toOrderByElementKind: " + tokenToString(t));
		}
	}

	@Override
	protected NullOrderKind toNullOrderKind(Token t1, Token t2) {
		throw new ASGException("NullOrderKind is not supported in MySQL!");
	}

	@Override
	protected NumericTypeKind toNumericTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toNumericTypeKind!");
		}

		return toNumericTypeKind(tok.getText());
	}

	private NumericTypeKind toNumericTypeKind(String name) {
		if (name == null) {
			throw new ASGException("Null name in toDateTypeKind!");
		}

		switch (name.toUpperCase(Locale.ENGLISH)) {
		case "INT":
		case "INTEGER":
			return NumericTypeKind.INT;
		case "TINYINT":
			return NumericTypeKind.TINYINT;
		case "SMALLINT":
			return NumericTypeKind.SMALLINT;
		case "MEDIUMINT":
			return NumericTypeKind.MEDIUMINT;
		case "BIGINT":
			return NumericTypeKind.BIGINT;
		case "REAL":
			return NumericTypeKind.REAL;
		case "DOUBLE":
			return NumericTypeKind.DOUBLE;
		case "BIT":
			return NumericTypeKind.BIT;
		// SERIAL - MySQL maps it to BIGINT UNSIGNED NOT NULL AUTO_INCREMENT
		// UNIQUE.
		case "SERIAL":
			return NumericTypeKind.SERIAL;
		// MySQL mapped types
		case "BOOL":
		case "BOOLEAN":
			return NumericTypeKind.BOOLEAN;
		case "FIXED":
		case "DECIMAL":
			return NumericTypeKind.DECIMAL;
		case "FLOAT":
		case "FLOAT4":
			return NumericTypeKind.FLOAT;
		case "FLOAT8":
			return NumericTypeKind.DOUBLE;
		case "INT1":
			return NumericTypeKind.TINYINT;
		case "INT2":
			return NumericTypeKind.SMALLINT;
		case "INT3":
			return NumericTypeKind.MEDIUMINT;
		case "INT4":
			return NumericTypeKind.INT;
		case "INT8":
			return NumericTypeKind.BIGINT;
		case "MIDDLEINT":
			return NumericTypeKind.MEDIUMINT;
		case "NUMERIC":
			return NumericTypeKind.DECIMAL;
		default:
			throw new ASGException("Invalid name string in toNumericTypeKind: " + name);
		}
	}

	@Override
	protected DateTypeKind toDateTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toDateTypeKind!");
		}

		throw new ASGException("Invalid token in toDateTypeKind: " + tokenToString(tok));
	}

	@Override
	protected StringTypeKind toStringTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toStringTypeKind!");
		}

		return toStringTypeKind(tok.getText());
	}

	private StringTypeKind toStringTypeKind(String name) {
		if (name == null) {
			throw new ASGException("Null name in toStringTypeKind!");
		}

		switch (name.toUpperCase(Locale.ENGLISH)) {
		case "CHAR":
			return StringTypeKind.CHAR;
		case "VARCHAR":
			return StringTypeKind.VARCHAR;
		case "NCHAR":
			return StringTypeKind.NCHAR;
		case "NVARCHAR":
			return StringTypeKind.NVARCHAR;
		case "BINARY":
			return StringTypeKind.BINARY;
		case "VARBINARY":
			return StringTypeKind.VARBINARY;
		case "BLOB":
			return StringTypeKind.BLOB;
		case "TEXT":
			return StringTypeKind.TEXT;
		// MySQL mapped types
		case "TINYTEXT":
			return StringTypeKind.TINYTEXT;
		case "LONGTEXT":
			return StringTypeKind.LONGTEXT;
		case "MEDIUMTEXT":
			return StringTypeKind.MEDIUMTEXT;
		case "TINYBLOB":
			return StringTypeKind.TINYBLOB;
		case "LONGBLOB":
			return StringTypeKind.LONGBLOB;
		case "MEDIUMBLOB":
			return StringTypeKind.MEDIUMBLOB;

		default:
			throw new ASGException("Invalid name string in toStringTypeKind: " + name);
		}
	}

	@Override
	protected FunctionParamsKind toFunctionParamsKind(Token t1, Token t2) {
		if (t1 == null && t2 == null) {
			return FunctionParamsKind.NONE;
		} else if (t1 != null) {
			switch (t1.getType()) {
			case SQLiteLexer.K_ALL:
				return FunctionParamsKind.ALL;
			case SQLiteLexer.K_DISTINCT:
				return FunctionParamsKind.DISTINCT;
			default:
				throw new ASGException(
						"Invalid token in toFunctionParamsKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		} else {
			throw new ASGException("Invalid token in toFunctionParamsKind! " + "NULL" + " , " + tokenToString(t2));
		}
	}

	@Override
	protected TimeUnitKind toTimeUnitKind(Token t) {
		throw new ASGException("Invalid token in toTimeUnitKind: " + tokenToString(t));
	}

}
