package sqlinspect.sql.parser;

import java.util.List;
import java.util.Locale;

import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.builder.AliasBuilder;
import sqlinspect.sql.asg.builder.AnalyticFunctionCallBuilder;
import sqlinspect.sql.asg.builder.AnalyticWindowBuilder;
import sqlinspect.sql.asg.builder.BoundaryBuilder;
import sqlinspect.sql.asg.builder.CreateFunctionBuilder;
import sqlinspect.sql.asg.builder.FromExprBuilder;
import sqlinspect.sql.asg.builder.FunctionDefParamsBuilder;
import sqlinspect.sql.asg.builder.KeyValueBuilder;
import sqlinspect.sql.asg.builder.TableRefBuilder;
import sqlinspect.sql.asg.builder.TableSampleClauseBuilder;
import sqlinspect.sql.asg.builder.UnaryExpressionBuilder;
import sqlinspect.sql.asg.builder.UpsertBuilder;
import sqlinspect.sql.asg.builder.UseBuilder;
import sqlinspect.sql.asg.builder.ViewDefinitionBuilder;
import sqlinspect.sql.asg.clause.TableSampleClause;
import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.expr.Alias;
import sqlinspect.sql.asg.expr.AnalyticFunctionCall;
import sqlinspect.sql.asg.expr.AnalyticWindow;
import sqlinspect.sql.asg.expr.Boundary;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.FromExpr;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.Joker;
import sqlinspect.sql.asg.expr.KeyValue;
import sqlinspect.sql.asg.expr.Literal;
import sqlinspect.sql.asg.expr.Query;
import sqlinspect.sql.asg.expr.TableRef;
import sqlinspect.sql.asg.expr.UnaryExpression;
import sqlinspect.sql.asg.expr.ViewDefinition;
import sqlinspect.sql.asg.kinds.AliasKind;
import sqlinspect.sql.asg.kinds.AnalyticWindowKind;
import sqlinspect.sql.asg.kinds.BinaryExpressionKind;
import sqlinspect.sql.asg.kinds.BinaryQueryKind;
import sqlinspect.sql.asg.kinds.BoundaryKind;
import sqlinspect.sql.asg.kinds.ColumnAttributeKind;
import sqlinspect.sql.asg.kinds.DateTypeKind;
import sqlinspect.sql.asg.kinds.FunctionParamsKind;
import sqlinspect.sql.asg.kinds.JoinKind;
import sqlinspect.sql.asg.kinds.LiteralKind;
import sqlinspect.sql.asg.kinds.NullOrderKind;
import sqlinspect.sql.asg.kinds.NumericTypeKind;
import sqlinspect.sql.asg.kinds.OrderByElementKind;
import sqlinspect.sql.asg.kinds.SetQuantifierKind;
import sqlinspect.sql.asg.kinds.StringTypeKind;
import sqlinspect.sql.asg.kinds.TimeUnitKind;
import sqlinspect.sql.asg.kinds.UnaryExpressionKind;
import sqlinspect.sql.asg.schema.Function;
import sqlinspect.sql.asg.schema.FunctionDefParams;
import sqlinspect.sql.asg.statm.CreateFunction;
import sqlinspect.sql.asg.statm.Upsert;
import sqlinspect.sql.asg.statm.Use;
import sqlinspect.sql.asg.type.Type;

public final class ImpalaParserActions extends SQLParserActions {
	private static final Logger LOG = LoggerFactory.getLogger(ImpalaParserActions.class);

	public ImpalaParserActions(SQLParser parser) {
		super(parser);
	}

	public Upsert buildUpsert(Token t, Expression table, ExpressionList with) {
		UpsertBuilder b = new UpsertBuilder(asg);
		copyAllPosition(b.result(), t);
		if (table != null) {
			b.setTable(table);
			copyEndPosition(b.result(), table);
		}
		if (with != null) {
			b.setWith(with);
			copyStartPosition(b.result(), with);
		}
		return b.result();
	}

	public void upsertSetQuery(Upsert upsert, Query query) {
		if (upsert != null && query != null) {
			upsert.setQuery(query);
			copyEndPosition(upsert, query);
		}
	}

	public void upsertSetColumnList(Upsert upsert, ExpressionList cols) {
		if (upsert != null && cols != null) {
			upsert.setColumnList(cols);
			copyEndPosition(upsert, cols);
		}
	}

	@Override
	public Use buildUse(Token t, Expression db) {
		UseBuilder b = new UseBuilder(asg);
		copyAllPosition(b.result(), t);
		if (db != null) {
			b.setDatabase(db);
			copyEndPosition(b.result(), db);
		}
		return b.result();
	}

	public CreateFunction buildCreateFunction(Token t, Expression name, FunctionDefParams params, Type returnType,
			boolean ifNotExists) {
		CreateFunctionBuilder b = new CreateFunctionBuilder(asg);
		copyAllPosition(b.result(), t);
		b.setIfNotExists(ifNotExists);
		if (name != null) {
			Function f = retrieveFunction(name, params, returnType);
			b.setFunction(f);
		}
		return b.result();
	}

	public FunctionDefParams buildFunctionDefParams(List<Type> params, boolean varargs) {
		FunctionDefParamsBuilder b = new FunctionDefParamsBuilder(asg);
		if (params != null) {
			for (Type p : params) {
				b.addParameter(p);
			}
		}
		b.setVarargs(varargs);
		return b.result();
	}

	// CLAUSES ----------------------------
	public TableSampleClause buildTableSampleClause(Token t1, Literal percentage, Literal repeat) {
		TableSampleClauseBuilder b = new TableSampleClauseBuilder(asg);
		copyAllPosition(b.result(), t1);
		if (percentage != null) {
			b.setPercentage(percentage);
			copyEndPosition(b.result(), percentage);
		}
		if (repeat != null) {
			b.setRepeat(repeat);
			copyEndPosition(b.result(), repeat);
		}
		return b.result();
	}

	// EXPRESSIONS ------------------------

	public TableRef buildTableRef(Expression expr, Expression name) {
		TableRefBuilder b = new TableRefBuilder(asg);
		b.setTable(expr);
		copyAllPosition(b.result(), expr);
		if (name != null) {
			if (name instanceof Id) {
				Id rId = (Id) name;
				b.setName(rId.getName());
			} else if (name instanceof Literal) {
				Literal rLiteral = (Literal) name;
				b.setName(rLiteral.getValue());
			} else if (name instanceof Joker) {
				Joker jok = (Joker) name;
				b.setName(jok.getName());
			} else {
				LOG.warn("TableRef without unsupported name type: {}", name);
			}
		}
		return b.result();
	}

	public void tableRefSetTableSample(TableRef ref, TableSampleClause sample) {
		if (ref != null && sample != null) {
			ref.setTableSample(sample);
		}
	}

	public UnaryExpression buildFactorial(Token t, Expression expr) {
		UnaryExpressionBuilder b = new UnaryExpressionBuilder(asg);
		b.setKind(UnaryExpressionKind.FACTORIAL);
		copyAllPosition(b.result(), t);
		b.setExpression(expr);
		copyEndPosition(b.result(), expr);
		return b.result();
	}

	public Alias buildAlias(Expression expr, Expression alias, Token t) {
		AliasBuilder b = new AliasBuilder(asg);
		copyAllPosition(b.result(), alias);
		b.setExpression(expr);
		copyStartPosition(b.result(), expr);

		b.setKind(toAliasKind(t));

		if (alias instanceof Id) {
			Id rId = (Id) alias;
			b.setName(rId.getName());
		} else if (alias instanceof Literal) {
			Literal rLiteral = (Literal) alias;
			b.setName(rLiteral.getValue());
		} else if (alias instanceof Joker) {
			Joker jok = (Joker) alias;
			b.setName(jok.getName());
		} else {
			LOG.warn("Alias without unsupported right side: {}", expr);
		}

		return b.result();
	}

	public ViewDefinition buildViewDefinition(Expression name, Query query, ExpressionList cols) {
		ViewDefinitionBuilder b = new ViewDefinitionBuilder(asg);

		if (name != null) {
			copyAllPosition(b.result(), name);
		}

		if (name instanceof Id) {
			Id rId = (Id) name;
			b.setName(rId.getName());
		} else if (name instanceof Literal) {
			Literal rLiteral = (Literal) name;
			b.setName(rLiteral.getValue());
		} else if (name instanceof Joker) {
			Joker jok = (Joker) name;
			b.setName(jok.getName());
		} else {
			LOG.warn("ViewDefinition without unsupported name: {}", name);
		}

		if (query != null) {
			b.setQuery(query);
			copyEndPosition(b.result(), name);
		}

		if (cols != null) {
			b.setColumnList(cols);
			copyEndPosition(b.result(), cols);
		}

		return b.result();
	}

	public AnalyticFunctionCall buildAnalyticFunctionCall(Expression expr) {
		AnalyticFunctionCallBuilder b = new AnalyticFunctionCallBuilder(asg);
		b.setExpression(expr);
		copyAllPosition(b.result(), expr);
		return b.result();
	}

	public void analyticFunctionCallSetPartition(AnalyticFunctionCall call, ExpressionList list) {
		call.setPartition(list);
		copyEndPosition(call, list);
	}

	public void analyticFunctionCallSetOrderBy(AnalyticFunctionCall call, ExpressionList list) {
		call.setOrderBy(list);
		copyEndPosition(call, list);
	}

	public void analyticFunctionCallSetWindow(AnalyticFunctionCall call, AnalyticWindow window) {
		call.setWindow(window);
		copyEndPosition(call, window);
	}

	public AnalyticWindow buildAnalyticWindow(Token t, Boundary left, Boundary right) {
		AnalyticWindowBuilder b = new AnalyticWindowBuilder(asg);
		b.setKind(toAnalyticWindowKind(t));
		copyAllPosition(b.result(), t);
		b.setLeft(left);
		b.setRight(right);
		return b.result();
	}

	public Boundary buildBoundary(Token t1, Token t2, Expression expr) {
		BoundaryBuilder b = new BoundaryBuilder(asg);
		if (t1 != null) {
			b.setKind(toBoundaryKind(t1, t2));
			if (t2 != null) {
				copyStartPosition(b.result(), t1);
			} else { // expr FOLLOWING
				copyEndPosition(b.result(), t1);
			}
		}

		return b.result();
	}

	public FromExpr buildFromExpr(Expression expr, Expression from) {
		FromExprBuilder b = new FromExprBuilder(asg);
		b.setExpression(expr);
		copyAllPosition(b.result(), expr);
		b.setFrom(from);
		copyEndPosition(b.result(), from);
		return b.result();
	}

	public KeyValue buildKeyValue(Token tok, Expression key, Expression expr) {
		KeyValueBuilder b = new KeyValueBuilder(asg);
		if (tok != null) {
			copyAllPosition(b.result(), tok);
		} else if (key != null) {
			copyAllPosition(b.result(), key);
		}
		if (key != null) {
			b.setKey(key);
			copyStartPosition(b.result(), key);
		}
		if (expr != null) {
			b.setValue(expr);
			copyEndPosition(b.result(), expr);
		}
		return b.result();
	}

	// TYPE ------------------------

	// KINDS ------------------------

	@Override
	protected AliasKind toAliasKind(Token tok) {
		if (tok == null) {
			return AliasKind.NONE;
		} else if (tok.getType() == ImpalaLexer.KW_AS) {
			return AliasKind.AS;
		} else {
			throw new ASGException("Invalid token in toAliasKind: " + tokenToString(tok));
		}
	}

	@Override
	protected UnaryExpressionKind toUnaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toUnaryExpressionKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.BITNOT:
			return UnaryExpressionKind.BITNOT;
		case ImpalaLexer.NOT:
		case ImpalaLexer.KW_NOT:
			return UnaryExpressionKind.NOT;
		case ImpalaLexer.ADD:
			return UnaryExpressionKind.PLUS;
		case ImpalaLexer.SUBTRACT:
			return UnaryExpressionKind.MINUS;
		case ImpalaLexer.KW_EXISTS:
			return UnaryExpressionKind.EXISTS;
		default:
			throw new ASGException("Invalid token in toUnaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toBinaryExpressionKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.ADD:
			return BinaryExpressionKind.PLUS;
		case ImpalaLexer.SUBTRACT:
			return BinaryExpressionKind.MINUS;
		case ImpalaLexer.STAR:
			return BinaryExpressionKind.MULTIPLY;
		case ImpalaLexer.DIVIDE:
		case ImpalaLexer.KW_DIV:
			return BinaryExpressionKind.DIVIDE;
		case ImpalaLexer.MOD:
			return BinaryExpressionKind.MODULO;
		case ImpalaLexer.BITAND:
			return BinaryExpressionKind.BITAND;
		case ImpalaLexer.BITOR:
			return BinaryExpressionKind.OR;
		case ImpalaLexer.BITXOR:
			return BinaryExpressionKind.BITXOR;
		case ImpalaLexer.EQUAL:
			return BinaryExpressionKind.EQUALS;
		case ImpalaLexer.NOTEQUAL:
			return BinaryExpressionKind.NOTEQUALS;
		case ImpalaLexer.GREATERTHAN:
			return BinaryExpressionKind.GREATERTHAN;
		case ImpalaLexer.LESSTHAN:
			return BinaryExpressionKind.LESSTHAN;
		case ImpalaLexer.KW_AND:
			return BinaryExpressionKind.AND;
		case ImpalaLexer.KW_OR:
			return BinaryExpressionKind.OR;
		case ImpalaLexer.KW_IS:
			return BinaryExpressionKind.IS;
		case ImpalaLexer.DOT:
			return BinaryExpressionKind.FIELDSELECTOR;
		case ImpalaLexer.KW_IN:
			return BinaryExpressionKind.IN;
		case ImpalaLexer.KW_LIKE:
			return BinaryExpressionKind.LIKE;
		case ImpalaLexer.KW_ILIKE:
			return BinaryExpressionKind.ILIKE;
		case ImpalaLexer.KW_RLIKE:
			return BinaryExpressionKind.RLIKE;
		case ImpalaLexer.KW_REGEXP:
			return BinaryExpressionKind.REGEXP;
		case ImpalaLexer.KW_IREGEXP:
			return BinaryExpressionKind.IREGEXP;
		default:
			throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2) {
		if (t1 != null && t2 != null) {
			if ((t1.getType() == ImpalaLexer.NOT && t2.getType() == ImpalaLexer.EQUAL)
					|| (t1.getType() == ImpalaLexer.LESSTHAN && t2.getType() == ImpalaLexer.GREATERTHAN)) {
				return BinaryExpressionKind.NOTEQUALS;
			}

			if (t1.getType() == ImpalaLexer.LESSTHAN && t2.getType() == ImpalaLexer.EQUAL) {
				return BinaryExpressionKind.LESSOREQUALS;
			}

			if (t1.getType() == ImpalaLexer.GREATERTHAN && t2.getType() == ImpalaLexer.EQUAL) {
				return BinaryExpressionKind.GREATEROREQUALS;
			}
		}
		throw new ASGException(
				"Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2, Token t3) {
		if (t1 != null && t2 != null && t3 != null) {
			if (t1.getType() == ImpalaLexer.LESSTHAN && t2.getType() == ImpalaLexer.EQUAL
					&& t3.getType() == ImpalaLexer.GREATERTHAN) {
				return BinaryExpressionKind.NOTDISTINCTFROM;
			}
			if (t1.getType() == ImpalaLexer.KW_IS && t2.getType() == ImpalaLexer.KW_DISTINCT
					&& t3.getType() == ImpalaLexer.KW_FROM) {
				return BinaryExpressionKind.DISTINCTFROM;
			}

			if (t1.getType() == ImpalaLexer.KW_IS && t2.getType() == ImpalaLexer.KW_NOT
					&& t3.getType() == ImpalaLexer.KW_DISTINCT) {
				return BinaryExpressionKind.NOTDISTINCTFROM;
			}
		}
		throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", "
				+ tokenToString(t2) + ", " + tokenToString(t3));
	}

	@Override
	protected LiteralKind toLiteralKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toLiteralKind!");
		}

		switch (tok.getType()) {

		case ImpalaLexer.INTEGER_LITERAL:
			return LiteralKind.INTEGER;
		case ImpalaLexer.STRING_LITERAL:
			return LiteralKind.STRING;
		case ImpalaLexer.DECIMAL_LITERAL:
			return LiteralKind.DECIMAL;
		case ImpalaLexer.KW_NULL:
			return LiteralKind.NULL;
		case ImpalaLexer.KW_TRUE:
			return LiteralKind.BOOLEAN;
		case ImpalaLexer.KW_FALSE:
			return LiteralKind.BOOLEAN;
		default:
			throw new ASGException("Invalid token in toLiteralKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toJoinKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.COMMA:
			return JoinKind.COMMA;
		case ImpalaLexer.KW_INNER:
		case ImpalaLexer.KW_JOIN:
			return JoinKind.INNER;
		case ImpalaLexer.KW_CROSS:
			return JoinKind.CROSS;
		case ImpalaLexer.KW_LEFT:
			return JoinKind.LEFTOUTER;
		case ImpalaLexer.KW_RIGHT:
			return JoinKind.RIGHTOUTER;
		default:
			throw new ASGException("Invalid token in toJoinKind: " + tokenToString(tok));

		}
	}

	@Override
	protected JoinKind toJoinKind(Token t1, Token t2) {
		if (t2 == null && (t1 == null || t1.getType() == ImpalaLexer.KW_INNER)) {
			return JoinKind.INNER;
		}

		if (t1 != null && t2 != null && t2.getType() == ImpalaLexer.KW_OUTER) {
			switch (t1.getType()) {
			case ImpalaLexer.KW_LEFT:
				return JoinKind.LEFTOUTER;
			case ImpalaLexer.KW_RIGHT:
				return JoinKind.RIGHTOUTER;
			case ImpalaLexer.KW_FULL:
				return JoinKind.FULLOUTER;
			default:
				throw new ASGException(
						"Invalid outer join in toJoinKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		}

		if (t1 != null && t2 != null && t2.getType() == ImpalaLexer.KW_SEMI) {
			switch (t1.getType()) {
			case ImpalaLexer.KW_LEFT:
				return JoinKind.LEFTSEMI;
			case ImpalaLexer.KW_RIGHT:
				return JoinKind.RIGHTSEMI;
			default:
				throw new ASGException(
						"Invalid semi join in toJoinKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		}

		if (t1 != null && t2 != null && t2.getType() == ImpalaLexer.KW_ANTI) {
			switch (t1.getType()) {
			case ImpalaLexer.KW_LEFT:
				return JoinKind.LEFTANTI;
			case ImpalaLexer.KW_RIGHT:
				return JoinKind.RIGHTANTI;
			default:
				throw new ASGException(
						"Invalid anti join in toJoinKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		}

		throw new ASGException("Invalid tokens in toJoinKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected BinaryQueryKind toBinaryQueryKind(Token t1, Token t2) {
		if (t1 != null && t1.getType() == ImpalaLexer.KW_UNION) {
			if (t2 != null) {
				switch (t2.getType()) {
				case ImpalaLexer.KW_ALL:
					return BinaryQueryKind.UNIONALL;
				case ImpalaLexer.KW_DISTINCT:
					return BinaryQueryKind.UNIONDISTINCT;
				default:
					throw new ASGException(
							"Invalid Union in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
				}
			} else {
				return BinaryQueryKind.UNION;
			}
		}
		throw new ASGException("Invalid tokens in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected SetQuantifierKind toSetQuantifierKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toSetQuantifierKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.KW_ALL:
			return SetQuantifierKind.ALL;
		case ImpalaLexer.KW_DISTINCT:
			return SetQuantifierKind.DISTINCT;
		default:
			throw new ASGException("Invalid token in toSqtQuantifierKind: " + tokenToString(tok));
		}
	}

	protected AnalyticWindowKind toAnalyticWindowKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toAnalyticWindowKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.KW_ROWS:
			return AnalyticWindowKind.ROWS;
		case ImpalaLexer.KW_RANGE:
			return AnalyticWindowKind.RANGE;
		default:
			throw new ASGException("Invalid token in toAnalyticWindowKind: " + tokenToString(tok));
		}
	}

	@Override
	protected FunctionParamsKind toFunctionParamsKind(Token t1, Token t2) {
		if (t1 == null && t2 == null) {
			return FunctionParamsKind.NONE;
		} else if (t1 != null && t2 != null && t1.getType() == ImpalaLexer.KW_IGNORE
				&& t2.getType() == ImpalaLexer.KW_NULLS) {
			return FunctionParamsKind.IGNORENULLS;
		} else if (t1 != null) {
			switch (t1.getType()) {
			case ImpalaLexer.KW_ALL:
				return FunctionParamsKind.ALL;
			case ImpalaLexer.KW_DISTINCT:
				return FunctionParamsKind.DISTINCT;
			default:
				throw new ASGException(
						"Invalid token in toFunctionParamsKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		} else {
			throw new ASGException("Invalid token in toFunctionParamsKind: null, " + tokenToString(t2));
		}
	}

	protected BoundaryKind toBoundaryKind(Token t1, Token t2) {
		if (t1 != null && t2 != null) {
			if (t1.getType() == ImpalaLexer.KW_UNBOUNDED) {
				if (t2.getType() == ImpalaLexer.KW_PRECEDING) {
					return BoundaryKind.UNBOUNDEDPRECEDING;
				} else if (t2.getType() == ImpalaLexer.KW_FOLLOWING) {
					return BoundaryKind.UNBOUNDEDFOLLOWING;
				} else {
					throw new ASGException(
							"Invalid token in toBoundaryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
				}
			}
		} else if (t1 != null) {
			if (t1.getType() == ImpalaLexer.KW_PRECEDING) {
				return BoundaryKind.PRECEDING;
			} else if (t1.getType() == ImpalaLexer.KW_FOLLOWING) {
				return BoundaryKind.FOLLOWING;
			} else {
				throw new ASGException(
						"Invalid token in toBoundaryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		}
		throw new ASGException("Invalid token in toBoundaryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected OrderByElementKind toOrderByElementKind(Token t) {
		if (t == null) {
			return OrderByElementKind.NONE;
		} else if (t.getType() == ImpalaLexer.KW_ASC) {
			return OrderByElementKind.ASC;
		} else if (t.getType() == ImpalaLexer.KW_DESC) {
			return OrderByElementKind.DESC;
		} else {
			throw new ASGException("Invalid token in toOrderByElementKind: " + tokenToString(t));
		}
	}

	@Override
	protected NullOrderKind toNullOrderKind(Token t1, Token t2) {
		if (t1 == null && t2 == null) {
			return NullOrderKind.NONE;
		}
		if (t1 != null && t2 != null) {
			if (t1.getType() == ImpalaLexer.KW_NULLS && t2.getType() == ImpalaLexer.KW_FIRST) {
				return NullOrderKind.NULLSFIRST;
			} else if (t1.getType() == ImpalaLexer.KW_NULLS && t2.getType() == ImpalaLexer.KW_LAST) {
				return NullOrderKind.NULLSLAST;
			}
		}
		throw new ASGException("Invalid token in toNullOrderKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected TimeUnitKind toTimeUnitKind(Token t) {
		if (t == null || t.getType() != ImpalaLexer.IDENT) {
			throw new ASGException("Invalid token in toTimeUnitKind: " + tokenToString(t));
		}

		switch (t.getText().toUpperCase(Locale.ENGLISH)) {
		case "YEAR":
		case "YEARS":
			return TimeUnitKind.YEAR;
		case "MONTH":
		case "MONTHS":
			return TimeUnitKind.MONTH;
		case "WEEK":
		case "WEEKS":
			return TimeUnitKind.WEEK;
		case "DAY":
		case "DAYS":
			return TimeUnitKind.DAY;
		case "HOUR":
		case "HOURS":
			return TimeUnitKind.HOUR;
		case "MINUTE":
		case "MINUTES":
			return TimeUnitKind.MINUTE;
		case "SECOND":
		case "SECONDS":
			return TimeUnitKind.SECOND;
		case "MILLISECOND":
		case "MILLISECONDS":
			return TimeUnitKind.MILLISECOND;
		case "MICROSECOND":
		case "MICROSECONDS":
			return TimeUnitKind.MICROSECOND;
		case "NANOSECOND":
		case "NANOSECONDS":
			return TimeUnitKind.NANOSECOND;
		default:
			throw new ASGException("Invalid token in toTimeUnitKind: " + tokenToString(t));
		}
	}

	@Override
	protected NumericTypeKind toNumericTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toNumericTypeKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.KW_TINYINT:
			return NumericTypeKind.TINYINT;
		case ImpalaLexer.KW_SMALLINT:
			return NumericTypeKind.SMALLINT;
		case ImpalaLexer.KW_BIGINT:
			return NumericTypeKind.BIGINT;
		case ImpalaLexer.KW_BOOLEAN:
			return NumericTypeKind.BOOLEAN;
		case ImpalaLexer.KW_FLOAT:
			return NumericTypeKind.FLOAT;
		case ImpalaLexer.KW_DOUBLE:
			return NumericTypeKind.DOUBLE;
		case ImpalaLexer.KW_INT:
			return NumericTypeKind.INT;
		case ImpalaLexer.KW_DECIMAL:
			return NumericTypeKind.DECIMAL;
		default:
			throw new ASGException("Invalid token in toNumericTypeKind: " + tokenToString(tok));
		}
	}

	@Override
	protected DateTypeKind toDateTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toDateTypeKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.KW_DATE:
			return DateTypeKind.DATE;
		case ImpalaLexer.KW_TIMESTAMP:
			return DateTypeKind.TIMESTAMP;
		case ImpalaLexer.KW_DATETIME:
			return DateTypeKind.DATETIME;
		default:
			throw new ASGException("Invalid token in toDateTypeKind: " + tokenToString(tok));
		}
	}

	@Override
	protected StringTypeKind toStringTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toStringTypeKind!");
		}

		switch (tok.getType()) {
		case ImpalaLexer.KW_STRING:
			return StringTypeKind.STRING;
		case ImpalaLexer.KW_VARCHAR:
			return StringTypeKind.VARCHAR;
		case ImpalaLexer.KW_BINARY:
			return StringTypeKind.BINARY;
		case ImpalaLexer.KW_CHAR:
			return StringTypeKind.CHAR;
		default:
			throw new ASGException("Invalid token in toStringTypeKind: " + tokenToString(tok));
		}
	}

	@Override
	protected ColumnAttributeKind toColumnAttributeKind(Token tok1, Token tok2) {
		if (tok1 == null) {
			throw new ASGException("Null token in toColumnAttributeKind!");
		}

		switch (tok1.getType()) {
		case ImpalaLexer.KW_NULL:
			if (tok2 != null && tok2.getType() == ImpalaLexer.KW_NOT) {
				return ColumnAttributeKind.NOTNULL;
			} else {
				return ColumnAttributeKind.NULL;
			}
		case ImpalaLexer.KW_DEFAULT:
			return ColumnAttributeKind.DEFAULT;
		case ImpalaLexer.KW_PRIMARY:
			return ColumnAttributeKind.PRIMARYKEY;
		case ImpalaLexer.KW_COMPRESSION:
			return ColumnAttributeKind.UNIQUE;
		case ImpalaLexer.KW_BLOCKSIZE:
			return ColumnAttributeKind.UNIQUE;
		case ImpalaLexer.KW_COMMENT:
			return ColumnAttributeKind.COMMENT;
		default:
			throw new ASGException(
					"Invalid token in toColumnAttributeKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
		}
	}

}
