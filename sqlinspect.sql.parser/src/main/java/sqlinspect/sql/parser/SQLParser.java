package sqlinspect.sql.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.TokenStream;

import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.statm.Statement;

public abstract class SQLParser extends Parser {
	protected final ASG asg;
	protected boolean stmtError;
	protected SQLError stmtErrorDetails;

	private static final String UNKNOWN_INPUT = "<unknown>";
	private static final String DEFAULT_SCHEMA = "SCH";
	private static final String DEFAULT_DB = "DB";

	protected String defaultSchema = DEFAULT_SCHEMA;
	protected String defaultDatabase = DEFAULT_DB;
	protected boolean interpretSchema = true;

	private String inputName;
	private int internalId;
	private int initialRow;

	protected static final SQLErrorListener sqlErrorListener = SQLErrorListener.getInstance();

	protected SQLParser(TokenStream input) {
		super(input);
		asg = new ASG();
		removeErrorListener(ConsoleErrorListener.INSTANCE);
		addErrorListener(sqlErrorListener);
	}

	protected SQLParser(TokenStream input, ASG asg) {
		super(input);
		this.asg = asg;
		removeErrorListener(ConsoleErrorListener.INSTANCE);
		addErrorListener(sqlErrorListener);
	}

	public String getDefaultSchema() {
		return defaultSchema;
	}

	public void setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
	}

	public String getDefaultDatabase() {
		return defaultDatabase;
	}

	public void setDefaultDatabase(String defaultDatabase) {
		this.defaultDatabase = defaultDatabase;
	}

	public boolean isInterpretSchema() {
		return interpretSchema;
	}

	public void setInterpretSchema(boolean interpretSchema) {
		this.interpretSchema = interpretSchema;
	}

	public void setInputName(String str) {
		inputName = str;
	}

	public String getInputName() {
		return inputName;
	}

	public int getInternalId() {
		return internalId;
	}

	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}

	public void setInitialRow(int initialRow) {
		this.initialRow = initialRow;
	}

	public int getInitialRow() {
		return initialRow;
	}

	public static SQLParser create(SQLDialect dialect, File file) throws IOException {
		return create(dialect, file, new ASG());
	}

	public static SQLParser create(SQLDialect dialect, File file, ASG asg) throws IOException {
		SQLParser parser;
		if (dialect == SQLDialect.IMPALA) {
			SQLLexer lexer = new ImpalaLexer(CharStreams.fromFileName(file.getPath()));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser = new ImpalaParser(tokens, asg);
		} else if (dialect == SQLDialect.SQLITE) {
			SQLLexer lexer = new SQLiteLexer(CharStreams.fromFileName(file.getPath()));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser = new SQLiteParser(tokens, asg);
		} else if (dialect == SQLDialect.MYSQL) {
			SQLLexer lexer = new MySQLLexer(CharStreams.fromFileName(file.getPath()));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser = new MySQLParser(tokens, asg);
		} else {
			throw new IllegalArgumentException("Invalid dialect" + dialect);
		}
		parser.setInputName(file.getPath());
		parser.getActions().init();
		return parser;
	}

	public static SQLParser create(SQLDialect dialect, String string) {
		return create(dialect, string, new ASG());
	}

	public static SQLParser create(SQLDialect dialect, String string, ASG asg) {
		SQLParser parser;
		if (dialect == SQLDialect.IMPALA) {
			SQLLexer lexer = new ImpalaLexer(CharStreams.fromString(string));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser = new ImpalaParser(tokens, asg);
		} else if (dialect == SQLDialect.SQLITE) {
			SQLLexer lexer = new SQLiteLexer(CharStreams.fromString(string));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser = new SQLiteParser(tokens, asg);
		} else if (dialect == SQLDialect.MYSQL) {
			SQLLexer lexer = new MySQLLexer(CharStreams.fromString(string));
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser = new MySQLParser(tokens, asg);
		} else {
			throw new IllegalArgumentException("Invalid dialect" + dialect);
		}
		parser.setInputName(UNKNOWN_INPUT);
		parser.getActions().init();
		return parser;
	}

	protected void setStmtError(boolean error) {
		this.stmtError = error;
	}

	public boolean isStmtError() {
		return stmtError;
	}

	protected void setStmtErrorDetails(SQLError error) {
		this.stmtErrorDetails = error;
	}

	public SQLError getStmtErrorDetails() {
		return stmtErrorDetails;
	}

	public void clearDFA() {
		if (_interp != null) {
			_interp.clearDFA();
		}
	}

	/**
	 * The default parsing method. Parses the input and returns its syntax tree.
	 *
	 * @return The ASG of the input.
	 */
	public ASG parse() {
		parseStatements();
		return asg;
	}

	/**
	 * Returns the list of Statement nodes instead of the root of the ASG.
	 *
	 * @return The new Statement nodes in the input.
	 */
	public abstract List<Statement> parseStatements();

	/**
	 * Returns the SQL dialect of the parser. Must be defined in child classes.
	 *
	 * @return Dialect of the parser.
	 */
	public abstract SQLDialect getDialect();

	public ASG getASG() {
		return asg;
	}

	protected abstract SQLParserActions getActions();
}
