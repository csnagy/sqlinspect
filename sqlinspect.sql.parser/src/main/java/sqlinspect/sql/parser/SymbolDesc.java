package sqlinspect.sql.parser;

import java.util.Arrays;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.common.NodeKind;

public class SymbolDesc {

	private Base symbol;
	private NodeKind kind;
	private Base scope;

	public SymbolDesc(Base symbol, NodeKind kind, Base scope) {
		this.symbol = symbol;
		this.kind = kind;
		this.scope = scope;
	}

	public Base getSymbol() {
		return symbol;
	}

	public void setSymbol(Base symbol) {
		this.symbol = symbol;
	}

	public NodeKind getKind() {
		return kind;
	}

	public void setKind(NodeKind kind) {
		this.kind = kind;
	}

	public Base getScope() {
		return scope;
	}

	public void setScope(Base scope) {
		this.scope = scope;
	}

	@Override
	public int hashCode() {
		int hash = 8;
		hash = 31 * hash + symbol.hashCode();
		hash = 31 * hash + kind.hashCode();
		hash = 31 * hash + scope.hashCode();
		return hash;
	}

	@Override
	public boolean equals(Object other) {
		boolean ret = false;
		if (other instanceof SymbolDesc) {
			SymbolDesc sd = (SymbolDesc) other;
			ret = symbol.equals(sd.getSymbol()) && kind.equals(sd.getKind()) && scope.equals(sd.getScope());
		}
		return ret;
	}

	public boolean match(SymbolDesc sd) {
		return (symbol == null || symbol.equals(sd.getSymbol())) && (kind == NodeKind.BASE || kind.equals(sd.getKind()))
				&& (scope == null || scope.equals(sd.getScope()));
	}

	public boolean match(NodeKind... kinds) {
		return kinds == null || Arrays.asList(kinds).contains(this.kind);
	}

	@Override
	public String toString() {
		return "SymbolDesc [symbol=" + symbol + ", kind=" + kind + ", scope=" + scope + "]";
	}
}
