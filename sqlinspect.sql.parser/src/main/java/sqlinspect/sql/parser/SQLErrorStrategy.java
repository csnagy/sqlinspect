package sqlinspect.sql.parser;

import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLErrorStrategy extends DefaultErrorStrategy {
	private static final Logger LOG = LoggerFactory.getLogger(SQLErrorStrategy.class);

	protected static final SQLErrorListener sqlErrorListener = SQLErrorListener.getInstance();

	private final int TOK_SEMICOLON;

	public SQLErrorStrategy(int tokSemicolon) {
		super();
		this.TOK_SEMICOLON = tokSemicolon;
	}

	public static void consumeUntil(Parser recognizer, int tokType) {
		Token currTok = recognizer.getCurrentToken();
		while (currTok != null && currTok.getType() != Token.EOF && currTok.getType() != tokType) {
			recognizer.consume();
			currTok = recognizer.getCurrentToken();
		}
		if (currTok != null) {
			LOG.debug("Tokens consumed until next '{}' ({}) at line:{} col: {}", currTok.getText(), currTok.getType(),
					currTok.getLine(), currTok.getCharPositionInLine());
		} else {
			LOG.debug("All tokens consumed.");
		}
	}

	@Override
	public void reportError(Parser recognizer, RecognitionException e) {
		super.reportError(recognizer, e);
		if (recognizer instanceof SQLParser) {
			((SQLParser) recognizer).setStmtError(true);
			((SQLParser) recognizer).setStmtErrorDetails(sqlErrorListener.getLast());
		}
		LOG.error("RecognitionException while parsing: ", e);
	}

	@Override
	public void recover(Parser recognizer, RecognitionException e) {
		if (TOK_SEMICOLON != 0) {
			consumeUntil(recognizer, TOK_SEMICOLON);
		} else {
			super.recover(recognizer, e);
		}
	}
}
