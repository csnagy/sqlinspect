package sqlinspect.sql.parser;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.builder.TableBuilder;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.common.NodeKind;
import sqlinspect.sql.asg.expr.BinaryExpression;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.ExpressionList;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.Join;
import sqlinspect.sql.asg.expr.SelectExpression;
import sqlinspect.sql.asg.expr.TableRef;
import sqlinspect.sql.asg.expr.UnaryExpression;
import sqlinspect.sql.asg.kinds.UnaryExpressionKind;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.traversal.PreOrderTraversal;
import sqlinspect.sql.asg.visitor.Visitor;

@SuppressWarnings("unused")
public final class SchemaRevEng extends Visitor {

	private static final Logger LOG = LoggerFactory.getLogger(SchemaRevEng.class);

	private final ASG asg;

	private Optional<Database> actualDatabase = Optional.empty();
	private Optional<Schema> actualSchema = Optional.empty();
	private Optional<Table> actualTable = Optional.empty();

	private final Deque<Set<Base>> visibleScopes = new ArrayDeque<>();

	private void setScopesToDefault() {
		actualDatabase = Optional.of(asg.getRoot().getDefaultDatabase());
		actualSchema = Optional.of(actualDatabase.get().getDefaultSchema());
		actualTable = Optional.empty();
	}

	private void pushScope(Base n) {
		Set<Base> s = new HashSet<>();
		s.add(n);
		visibleScopes.push(s);
	}

	private void popScope() {
		visibleScopes.pop();
	}

	private void insertToTopScope(Base n) {
		visibleScopes.getFirst().add(n);

	}


	public SchemaRevEng(ASG asg) {
		super();
		this.asg = asg;
	}

	@Override
	public void visit(SelectExpression n) {
		super.visit(n);

		resolveSelectExpression(n);
	}

	private void resolveSelectExpression(SelectExpression n) {
		Expression from = n.getFrom();
		if (from != null) {
			resolveFrom(from);
		} else {
			LOG.trace("Missing From clause of SelectExpression! {}", n);
		}
	}

	private void resolveFrom(Expression from) {
		if (from.getNodeKind() == NodeKind.EXPRESSIONLIST) {
			final ExpressionList exprList = (ExpressionList) from;
			for (Expression expr : exprList.getExpressions()) {
				resolveFromElement(expr);
			}
		} else {
			resolveFromElement(from);
		}
	}

	private void resolveFromElement(Expression expr) {
		if (expr.getNodeKind() == NodeKind.ID) {
			final Id id = (Id) expr;
			if (id.getRefersTo() == null) {
				newTable(id.getName());
			}
		} else if (expr.getNodeKind() == NodeKind.BINARYEXPRESSION) {
			final BinaryExpression binary = (BinaryExpression) expr;
			final Expression right = binary.getRight();
			if (right instanceof Id) {
				final Id id = (Id) right;
				if (id.getRefersTo() == null) {
					newTable(id.getName());
				}
			}
		} else if (expr.getNodeKind() == NodeKind.UNARYEXPRESSION) {
			final UnaryExpression unary = (UnaryExpression) expr;
			if (unary.getKind() == UnaryExpressionKind.BRACKETS) {
				resolveFromElement(unary.getExpression());
			} else {
				LOG.error("Unhandled unary kind, or missing subexpression! {}", unary);
			}
		} else if (expr.getNodeKind() == NodeKind.SELECTEXPRESSION) {
			final SelectExpression subQuery = (SelectExpression) expr;
			resolveSelectExpression(subQuery);
		} else if (expr.getNodeKind() == NodeKind.ALIAS) {
			// TODO
		} else if (expr.getNodeKind() == NodeKind.JOIN) {
			final Join join = (Join) expr;
			if (join.getLeft() != null) {
				resolveFromElement(join.getLeft());
			} else {
				LOG.trace("Missing left side of Join! {}", join);
			}
			if (join.getRight() != null) {
				resolveFromElement(join.getRight());
			} else {
				LOG.trace("Missing right side of Join! {}", join);
			}
		} else if (expr.getNodeKind() == NodeKind.TABLEREF) {
			final TableRef tableRef = (TableRef) expr;
			if (tableRef.getTable() != null) {
				resolveFromElement(tableRef.getTable());
			} else {
				LOG.trace("Missing table expression for TableRef! {}", tableRef);
			}
		} else {
			LOG.trace("Unhandled From element! {}", expr);
		}
	}

	private void newTable(String name) {
		if (name.isEmpty()) {
			LOG.warn("Id with empty name!");
			return;
		}

		Schema schema = asg.getRoot().getDefaultDatabase().getDefaultSchema();
		for (Table tab : schema.getTables()) {
			if (tab.getName().equalsIgnoreCase(name)) {
				// Table already exists
				return;
			}
		}

		Table tab = new TableBuilder(asg).setName(name).result();
		schema.addTable(tab);

		LOG.debug("Table created: {}.{}", schema.getName(), tab.getName());
	}


	public static void runRevengSchema(ASG asg) {
		LOG.info("Reverse Engineer Schema");
		SchemaRevEng visitor = new SchemaRevEng(asg);
		PreOrderTraversal traversal = new PreOrderTraversal(visitor);
		traversal.traverse(asg.getRoot());
		LOG.info("Done.");
	}

}
