package sqlinspect.sql.parser;

public class SQLError {
	private int line;
	private int charPositionInLine;
	private String message;
	private Throwable exception;

	public SQLError(int line, int charPositionInLine, String msg, Throwable e) {
		this.line = line;
		this.charPositionInLine = charPositionInLine;
		this.message = msg;
		this.exception = e;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getCharPositionInLine() {
		return charPositionInLine;
	}

	public void setCharPositionInLine(int charPositionInLine) {
		this.charPositionInLine = charPositionInLine;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String msg) {
		this.message = msg;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}
}