package sqlinspect.sql.parser;

public class ParserException extends RuntimeException {

	private static final long serialVersionUID = 6839353332631579909L;

	public ParserException() {
		super();
	}

	public ParserException(String message) {
		super(message);
	}

	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParserException(Throwable cause) {
		super(cause);
	}
}
