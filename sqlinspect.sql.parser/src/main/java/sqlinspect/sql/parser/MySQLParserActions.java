package sqlinspect.sql.parser;

import java.util.Locale;

import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sql.asg.builder.AliasBuilder;
import sqlinspect.sql.asg.common.ASGException;
import sqlinspect.sql.asg.expr.Alias;
import sqlinspect.sql.asg.expr.Expression;
import sqlinspect.sql.asg.expr.Id;
import sqlinspect.sql.asg.expr.Joker;
import sqlinspect.sql.asg.expr.Literal;
import sqlinspect.sql.asg.kinds.AliasKind;
import sqlinspect.sql.asg.kinds.BinaryExpressionKind;
import sqlinspect.sql.asg.kinds.BinaryQueryKind;
import sqlinspect.sql.asg.kinds.ColumnAttributeKind;
import sqlinspect.sql.asg.kinds.DateTypeKind;
import sqlinspect.sql.asg.kinds.FunctionParamsKind;
import sqlinspect.sql.asg.kinds.JoinKind;
import sqlinspect.sql.asg.kinds.LiteralKind;
import sqlinspect.sql.asg.kinds.NullOrderKind;
import sqlinspect.sql.asg.kinds.NumericTypeKind;
import sqlinspect.sql.asg.kinds.OrderByElementKind;
import sqlinspect.sql.asg.kinds.SetQuantifierKind;
import sqlinspect.sql.asg.kinds.StringTypeKind;
import sqlinspect.sql.asg.kinds.TimeUnitKind;
import sqlinspect.sql.asg.kinds.UnaryExpressionKind;

public final class MySQLParserActions extends SQLParserActions {
	private static final Logger LOG = LoggerFactory.getLogger(MySQLParserActions.class);

	public MySQLParserActions(SQLParser parser) {
		super(parser);
	}

	public Alias buildAlias(Expression left, Expression right, Token t) {
		AliasBuilder b = new AliasBuilder(asg);
		b.setExpression(left);
		b.setKind(toAliasKind(t));

		if (right instanceof Id) {
			Id rId = (Id) right;
			b.setName(rId.getName());
		} else if (right instanceof Literal) {
			Literal rLiteral = (Literal) right;
			b.setName(rLiteral.getValue());
		} else if (right instanceof Joker) {
			Joker jok = (Joker) right;
			b.setName(jok.getName());
		} else {
			LOG.debug("Alias without unsupported right side: {} (left: {})", right, left);
		}

		copyAllPosition(b.result(), left);
		copyEndPosition(b.result(), right);

		return b.result();
	}

	@Override
	protected AliasKind toAliasKind(Token tok) {
		if (tok == null) {
			return AliasKind.NONE;
		} else if (tok.getType() == MySQLLexer.AS) {
			return AliasKind.AS;
		} else {
			throw new ASGException("Invalid token in toAliasKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toJoinKind!");
		}

		switch (tok.getType()) {
		case MySQLLexer.COMMA:
			return JoinKind.COMMA;
		case MySQLLexer.INNER_SYM:
		case MySQLLexer.JOIN_SYM:
			return JoinKind.INNER;
		case MySQLLexer.CROSS:
			return JoinKind.CROSS;
		case MySQLLexer.STRAIGHT_JOIN:
			return JoinKind.STRAIGHTJOIN;
		case MySQLLexer.NATURAL:
			return JoinKind.NATURAL;
		case MySQLLexer.LEFT:
			return JoinKind.LEFTOUTER;
		case MySQLLexer.RIGHT:
			return JoinKind.RIGHTOUTER;
		default:
			throw new ASGException("Invalid token in toJoinKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok1, Token tok2) {
		if (tok1 != null && tok1.getType() == MySQLLexer.NATURAL) {
			if (tok2 != null && tok2.getType() == MySQLLexer.LEFT) {
				return JoinKind.NATURALLEFTOUTER;
			} else if (tok2 != null && tok2.getType() == MySQLLexer.RIGHT) {
				return JoinKind.NATURALRIGHTOUTER;
			} else if (tok2 == null) {
				return JoinKind.NATURAL;
			}
		}

		throw new ASGException("Invalid tokens in toJoinKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
	}

	@Override
	protected UnaryExpressionKind toUnaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toUnaryExpressionKind!");
		}

		switch (tok.getType()) {
		case MySQLLexer.PLUS:
			return UnaryExpressionKind.PLUS;
		case MySQLLexer.MINUS:
			return UnaryExpressionKind.MINUS;
		case MySQLLexer.NOT2_SYM:
		case MySQLLexer.NOT_SYM:
			return UnaryExpressionKind.NOT;
		case MySQLLexer.ASC:
			return UnaryExpressionKind.ASCENDING;
		case MySQLLexer.DESC:
			return UnaryExpressionKind.DESCENDING;
		case MySQLLexer.LPAREN:
		case MySQLLexer.RPAREN:
			return UnaryExpressionKind.PARENS;
		case MySQLLexer.TILDE:
			return UnaryExpressionKind.BITNOT;
		case MySQLLexer.EXISTS:
			return UnaryExpressionKind.EXISTS;
		case MySQLLexer.BINARY:
			return UnaryExpressionKind.BINARY;
		default:
			throw new ASGException("Invalid token in toUnaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toBinaryExpressionKind!");
		}

		switch (tok.getType()) {
		case MySQLLexer.PLUS:
			return BinaryExpressionKind.PLUS;
		case MySQLLexer.MINUS:
			return BinaryExpressionKind.MINUS;
		case MySQLLexer.ASTERISK:
			return BinaryExpressionKind.MULTIPLY;
		case MySQLLexer.DIV:
		case MySQLLexer.DIV_SYM:
			return BinaryExpressionKind.DIVIDE;
		case MySQLLexer.MOD:
		case MySQLLexer.MOD_SYM:
			return BinaryExpressionKind.MODULO;
		case MySQLLexer.AND_SYM:
		case MySQLLexer.AND_AND_SYM:
			return BinaryExpressionKind.AND;
		case MySQLLexer.OR_SYM:
		case MySQLLexer.OR_OR_SYM:
			return BinaryExpressionKind.OR;
		case MySQLLexer.BITAND:
			return BinaryExpressionKind.AND;
		case MySQLLexer.BITOR:
			return BinaryExpressionKind.OR;
		case MySQLLexer.XOR:
			return BinaryExpressionKind.XOR;
		case MySQLLexer.BIT_XOR:
			return BinaryExpressionKind.BITXOR;
		case MySQLLexer.EQUAL_SYM:
		case MySQLLexer.EQ:
			return BinaryExpressionKind.EQUALS;
		case MySQLLexer.NE:
			return BinaryExpressionKind.NOTEQUALS;
		case MySQLLexer.GT_SYM:
			return BinaryExpressionKind.GREATERTHAN;
		case MySQLLexer.LT:
			return BinaryExpressionKind.LESSTHAN;
		case MySQLLexer.GE:
			return BinaryExpressionKind.GREATEROREQUALS;
		case MySQLLexer.LE:
			return BinaryExpressionKind.LESSOREQUALS;
		case MySQLLexer.LIKE:
			return BinaryExpressionKind.LIKE;
		case MySQLLexer.IS:
			return BinaryExpressionKind.IS;
		case MySQLLexer.DOT:
			return BinaryExpressionKind.FIELDSELECTOR;
		case MySQLLexer.IN_SYM:
			return BinaryExpressionKind.IN;
		case MySQLLexer.REGEXP:
			return BinaryExpressionKind.REGEXP;
		case MySQLLexer.SOUNDS_SYM:
			return BinaryExpressionKind.SOUNDSLIKE;
		default:
			throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2) {
		throw new ASGException(
				"Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2, Token t3) {
		throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", "
				+ tokenToString(t2) + ", " + tokenToString(t3));
	}

	@Override
	protected LiteralKind toLiteralKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toLiteralKind!");
		}

		switch (tok.getType()) {

		case MySQLLexer.HEX_NUM:
			return LiteralKind.HEXADECIMAL;
		case MySQLLexer.LIT_BINARY:
			return LiteralKind.BINARY;
		case MySQLLexer.LIT_INTEGER:
			return LiteralKind.INTEGER;
		case MySQLLexer.LIT_DECIMAL:
			return LiteralKind.DECIMAL;
		case MySQLLexer.LIT_REAL:
			return LiteralKind.DOUBLE;
		case MySQLLexer.LIT_STRING:
			return LiteralKind.STRING;
		case MySQLLexer.NULL_SYM:
			return LiteralKind.NULL;
		case MySQLLexer.TRUE_SYM:
		case MySQLLexer.FALSE_SYM:
			return LiteralKind.BOOLEAN;
		case MySQLLexer.INTERVAL_SYM:
			return LiteralKind.INTERVAL;
		case MySQLLexer.DATE_SYM:
			return LiteralKind.DATE;
		case MySQLLexer.DATETIME:
			return LiteralKind.DATETIME;
		case MySQLLexer.TIMESTAMP:
			return LiteralKind.TIMESTAMP;
		case MySQLLexer.UNKNOWN_SYM:
			return LiteralKind.UNKNOWN;
		default:
			throw new ASGException("Invalid token in toLiteralKind: " + tokenToString(tok));
		}
	}

	@Override
	public BinaryQueryKind toBinaryQueryKind(Token t1, Token t2) {
		if (t1 != null && t1.getType() == MySQLLexer.UNION_SYM) {
			if (t2 != null) {
				switch (t2.getType()) {
				case MySQLLexer.ALL:
					return BinaryQueryKind.UNIONALL;
				case MySQLLexer.DISTINCT:
					return BinaryQueryKind.UNIONDISTINCT;
				default:
					throw new ASGException(
							"Invalid Union in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
				}
			} else {
				return BinaryQueryKind.UNION;
			}
		}
		throw new ASGException("Invalid tokens in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected SetQuantifierKind toSetQuantifierKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toSetQuantifierKind!");
		}

		switch (tok.getType()) {
		case MySQLLexer.ALL:
			return SetQuantifierKind.ALL;
		case MySQLLexer.DISTINCT:
			return SetQuantifierKind.DISTINCT;
		default:
			throw new ASGException("Invalid token in toSqtQuantifierKind: " + tokenToString(tok));
		}
	}

	@Override
	protected ColumnAttributeKind toColumnAttributeKind(Token tok1, Token tok2) {
		if (tok1 == null) {
			throw new ASGException("Null token in toColumnAttributeKind!");
		}

		switch (tok1.getType()) {
		case MySQLLexer.NULL_SYM:
			if (tok2 != null && tok2.getType() == MySQLLexer.NOT_SYM) {
				return ColumnAttributeKind.NOTNULL;
			} else {
				return ColumnAttributeKind.NULL;
			}
		case MySQLLexer.DEFAULT:
			return ColumnAttributeKind.DEFAULT;
		case MySQLLexer.AUTO_INC:
			return ColumnAttributeKind.AUTOINCREMENT;
		case MySQLLexer.KEY_SYM:
			return ColumnAttributeKind.PRIMARYKEY;
		case MySQLLexer.UNIQUE_SYM:
			return ColumnAttributeKind.UNIQUE;
		case MySQLLexer.COMMENT_SYM:
			return ColumnAttributeKind.COMMENT;
		case MySQLLexer.SERIAL_SYM:
			return ColumnAttributeKind.SERIAL;
		default:
			throw new ASGException(
					"Invalid token in toColumnAttributeKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
		}
	}

	public OrderByElementKind toOrderByElementKind1(Token t) {
		return toOrderByElementKind(t);
	}

	@Override
	protected OrderByElementKind toOrderByElementKind(Token t) {
		if (t == null) {
			return OrderByElementKind.NONE;
		} else if (t.getType() == MySQLLexer.ASC) {
			return OrderByElementKind.ASC;
		} else if (t.getType() == MySQLLexer.DESC) {
			return OrderByElementKind.DESC;
		} else {
			throw new ASGException("Invalid token in toOrderByElementKind: " + tokenToString(t));
		}
	}

	@Override
	protected NullOrderKind toNullOrderKind(Token t1, Token t2) {
		throw new ASGException("NullOrderKind is not supported in MySQL!");
	}

	@Override
	protected NumericTypeKind toNumericTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toNumericTypeKind!");
		}

		return toNumericTypeKind(tok.getText());
	}

	private NumericTypeKind toNumericTypeKind(String name) {
		if (name == null) {
			throw new ASGException("Null name in toDateTypeKind!");
		}

		switch (name.toUpperCase(Locale.ENGLISH)) {
		case "INT":
		case "INTEGER":
			return NumericTypeKind.INT;
		case "TINYINT":
			return NumericTypeKind.TINYINT;
		case "SMALLINT":
			return NumericTypeKind.SMALLINT;
		case "MEDIUMINT":
			return NumericTypeKind.MEDIUMINT;
		case "BIGINT":
			return NumericTypeKind.BIGINT;
		case "REAL":
			return NumericTypeKind.REAL;
		case "DOUBLE":
			return NumericTypeKind.DOUBLE;
		case "BIT":
			return NumericTypeKind.BIT;
		// SERIAL - MySQL maps it to BIGINT UNSIGNED NOT NULL AUTO_INCREMENT
		// UNIQUE.
		case "SERIAL":
			return NumericTypeKind.SERIAL;
		// MySQL mapped types
		case "BOOL":
		case "BOOLEAN":
			return NumericTypeKind.BOOLEAN;
		case "FIXED":
		case "DECIMAL":
			return NumericTypeKind.DECIMAL;
		case "FLOAT":
		case "FLOAT4":
			return NumericTypeKind.FLOAT;
		case "FLOAT8":
			return NumericTypeKind.DOUBLE;
		case "INT1":
			return NumericTypeKind.TINYINT;
		case "INT2":
			return NumericTypeKind.SMALLINT;
		case "INT3":
			return NumericTypeKind.MEDIUMINT;
		case "INT4":
			return NumericTypeKind.INT;
		case "INT8":
			return NumericTypeKind.BIGINT;
		case "MIDDLEINT":
			return NumericTypeKind.MEDIUMINT;
		case "NUMERIC":
			return NumericTypeKind.DECIMAL;
		default:
			throw new ASGException("Invalid name string in toNumericTypeKind: " + name);
		}
	}

	@Override
	protected DateTypeKind toDateTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toDateTypeKind!");
		}

		switch (tok.getType()) {
		case MySQLLexer.YEAR_SYM:
			return DateTypeKind.YEAR;
		case MySQLLexer.DATE_SYM:
			return DateTypeKind.DATE;
		case MySQLLexer.TIME_SYM:
			return DateTypeKind.TIME;
		case MySQLLexer.TIMESTAMP:
			return DateTypeKind.TIMESTAMP;
		case MySQLLexer.DATETIME:
			return DateTypeKind.DATETIME;
		default:
			throw new ASGException("Invalid token in toDateTypeKind: " + tokenToString(tok));
		}
	}

	@Override
	protected StringTypeKind toStringTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toStringTypeKind!");
		}

		return toStringTypeKind(tok.getText());
	}

	private StringTypeKind toStringTypeKind(String name) {
		if (name == null) {
			throw new ASGException("Null name in toStringTypeKind!");
		}

		switch (name.toUpperCase(Locale.ENGLISH)) {
		case "CHAR":
			return StringTypeKind.CHAR;
		case "VARCHAR":
			return StringTypeKind.VARCHAR;
		case "NCHAR":
			return StringTypeKind.NCHAR;
		case "NVARCHAR":
			return StringTypeKind.NVARCHAR;
		case "BINARY":
			return StringTypeKind.BINARY;
		case "VARBINARY":
			return StringTypeKind.VARBINARY;
		case "BLOB":
			return StringTypeKind.BLOB;
		case "TEXT":
			return StringTypeKind.TEXT;
		// MySQL mapped types
		case "TINYTEXT":
			return StringTypeKind.TINYTEXT;
		case "LONGTEXT":
			return StringTypeKind.LONGTEXT;
		case "MEDIUMTEXT":
			return StringTypeKind.MEDIUMTEXT;
		case "TINYBLOB":
			return StringTypeKind.TINYBLOB;
		case "LONGBLOB":
			return StringTypeKind.LONGBLOB;
		case "MEDIUMBLOB":
			return StringTypeKind.MEDIUMBLOB;

		default:
			throw new ASGException("Invalid name string in toStringTypeKind: " + name);
		}
	}

	@Override
	protected FunctionParamsKind toFunctionParamsKind(Token t1, Token t2) {
		if (t1 == null && t2 == null) {
			return FunctionParamsKind.NONE;
		} else if (t1 != null) {
			switch (t1.getType()) {
			case MySQLLexer.ALL:
				return FunctionParamsKind.ALL;
			case MySQLLexer.DISTINCT:
				return FunctionParamsKind.DISTINCT;
			default:
				throw new ASGException(
						"Invalid token in toFunctionParamsKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		} else {
			throw new ASGException("Invalid token in toFunctionParamsKind! " + "NULL" + " , " + tokenToString(t2));
		}
	}

	@Override
	protected TimeUnitKind toTimeUnitKind(Token t) {
		switch (t.getType()) {
		case MySQLLexer.DAY_SYM:
			return TimeUnitKind.DAY;
		case MySQLLexer.WEEK_SYM:
			return TimeUnitKind.WEEK;
		case MySQLLexer.HOUR_SYM:
			return TimeUnitKind.HOUR;
		case MySQLLexer.MINUTE_SYM:
			return TimeUnitKind.MINUTE;
		case MySQLLexer.MONTH_SYM:
			return TimeUnitKind.MONTH;
		case MySQLLexer.QUARTER_SYM:
			return TimeUnitKind.QUARTER;
		case MySQLLexer.SECOND_SYM:
			return TimeUnitKind.SECOND;
		case MySQLLexer.MICROSECOND_SYM:
			return TimeUnitKind.MICROSECOND;
		case MySQLLexer.YEAR_SYM:
			return TimeUnitKind.YEAR;
		case MySQLLexer.DAY_HOUR_SYM:
			return TimeUnitKind.DAYHOUR;
		case MySQLLexer.DAY_MICROSECOND_SYM:
			return TimeUnitKind.DAYMICROSECOND;
		case MySQLLexer.DAY_MINUTE_SYM:
			return TimeUnitKind.DAYMINUTE;
		case MySQLLexer.DAY_SECOND_SYM:
			return TimeUnitKind.DAYSECOND;
		case MySQLLexer.HOUR_MICROSECOND_SYM:
			return TimeUnitKind.HOURMICROSECOND;
		case MySQLLexer.HOUR_MINUTE_SYM:
			return TimeUnitKind.HOURMINUTE;
		case MySQLLexer.HOUR_SECOND_SYM:
			return TimeUnitKind.HOURSECOND;
		case MySQLLexer.MINUTE_MICROSECOND_SYM:
			return TimeUnitKind.MINUTEMICROSECOND;
		case MySQLLexer.MINUTE_SECOND_SYM:
			return TimeUnitKind.MINUTESECOND;
		case MySQLLexer.SECOND_MICROSECOND_SYM:
			return TimeUnitKind.SECONDMICROSECOND;
		case MySQLLexer.YEAR_MONTH_SYM:
			return TimeUnitKind.YEARMONTH;
		default:
			throw new ASGException("Invalid token in toTimeUnitKind: " + tokenToString(t));
		}
	}

}
