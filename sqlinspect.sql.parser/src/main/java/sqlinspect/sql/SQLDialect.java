package sqlinspect.sql;

public enum SQLDialect {
	MYSQL("MySQL"), IMPALA("Apache Impala"), SQLITE("SQLite");

	private final String text;

	SQLDialect(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public static boolean isSQLDialect(String value) {
		for (SQLDialect d : values()) {
			if (d.toString().equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static SQLDialect getSQLDialect(String value) {
		for (SQLDialect d : values()) {
			if (d.toString().equalsIgnoreCase(value)) {
				return d;
			}
		}
		throw new IllegalArgumentException("Invalid dialect: " + value);
	}
}
