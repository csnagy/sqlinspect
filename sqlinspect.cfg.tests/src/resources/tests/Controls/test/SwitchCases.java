package test;

public class SwitchCases {
	public void testSwitch1(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
			break;
		case 2:
			System.out.println("2");
			break;
		case 3:
			System.out.println("3");
			break;
		default:
			System.out.println("Unknown");
		}
	}

	public void testSwitch2(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
			break;
		case 2:
			System.out.println("2");
			break;
		case 3:
			System.out.println("3");
			break;
		default:
			System.out.println("Unknown");
		}
		x++;
	}
	
	public void testSwitch3(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
			break;
		case 2:
			System.out.println("2");
			System.out.println("2");
		case 3:
			System.out.println("3");
			break;
		default:
			System.out.println("Unknown");
		}
	}
	
	public void testSwitch4(int x) {
		x++;
		switch (x) {
		case 1:
			System.out.println("1");
			break;
		case 2:
			System.out.println("2");
			System.out.println("2");
		case 3:
			System.out.println("3");
			break;
		default:
			System.out.println("Unknown");
		}
		x++;
	}	

	public void testSwitch5(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
			break;
		case 2:
			System.out.println("2");
			break;
		}
		x++;
	}

	public void testSwitch6(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
			System.out.println("1");
		case 2:
			System.out.println("2");
			System.out.println("2");
		}
		x++;
	}

	public void testSwitch7(int a, int b, int c) {
		switch (a) {
		case 1:
			b = 2;
		case 2:
			c = 3;
			break;
		case 3:
			b = 1;
			c = 1;
		default:
			a = 0;
			b = 0;
			c = 0;
		}
	}

	public int testSwitchReturn1(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
		case 2:
			System.out.println("2");
			return x;
		default:
			return x + 2;
		}
	}

	public int testSwitchReturn2(int x) {
		switch (x) {
		case 1:
			System.out.println("1");
		case 2:
			System.out.println("2");
			return x + 1;
		default:
			x = x + 2;
		}
		return x + 2;
	}
}