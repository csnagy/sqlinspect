package test;

public class WhileCases {
	public void testWhile1(int x) {
		while (true)
			;
	}

	public void testWhile2(int x) {
		while (x < 10) {
			x++;
		}
	}

	public void testWhileBreak1(int x) {
		while (x < 10) {
			x++;
			if (x == 4) {
				break;
			}
			x += 2;
		}
	}

	public void testWhileBreak2(int x) {
		while (x < 10) {
			x++;
			if (x == 4) {
				break;
			}
		}
		x += 2;
	}

	public void testWhileContinue1(int x) {
		while (x < 10) {
			x++;
			if (x == 4) {
				continue;
			}
			x += 2;
		}
	}

	public void testWhileContinue2(int x) {
		while (x < 10) {
			x++;
			if (x == 4) {
				continue;
			}
		}
		x += 2;
	}

	public int testWhileReturn1(int x) {
		while (x < 100) {
			x++;
			if (x > 30) {
				return x + 10;
			}
		}
		x = x + 20;
		return x;
	}

	public int testWhileReturn2(int x) {
		while (x < 100) {
			x++;
			return x + 10;
		}
		x = x + 20;
		return x;
	}

}