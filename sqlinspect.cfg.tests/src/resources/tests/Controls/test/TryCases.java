package test;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TryCases {
	public void testTry1(int x) {
		try {
		} catch (Exception e) {
		}
	}

	public void testTry2(int x) {
		try {
		} finally {
		}
	}

	public void testTry3(InputStream in) {
		try (BufferedInputStream s = new BufferedInputStream(in)) {
		} catch (IOException e) {
		} finally {
		}
	}

	public void testTry4(int x) {
		x = x + 1;
		try {
			x = x + 2;
		} catch (Exception e) {
			System.err.println("Error");
		}
	}

	public void testTry5(int x) {
		try {
			x = x + 1;
		} catch (Exception e) {
			System.err.println("Error");
		}
	}

	public void testTry6(int x) {
		try {
			x = x + 1;
		} catch (NullPointerException e) {
			System.err.println("Error");
		} catch (IllegalArgumentException e) {
			System.err.println("Error");
		}
	}

	public void testTry7(int x) {
		try {
			x = x + 1;
		} catch (NullPointerException | IllegalArgumentException e) {
			System.err.println("Error");
		}
	}

	public void testTry8(int x) {
		try {
			x = x + 1;
		} catch (NullPointerException e) {
			System.err.println("Error");
		} catch (IllegalArgumentException e) {
			System.err.println("Error");
		} finally {
			x = 12;
		}
		x = x + 2;
	}

	public void testTry9(InputStream in1, InputStream in2) {
		int y = 0;
		try (BufferedInputStream s1 = new BufferedInputStream(in1);
				BufferedInputStream s2 = new BufferedInputStream(in2)) {
			y = s1.available();
		} catch (Exception e) {
			System.err.println("Error");
		}
		y += 10;
	}
}