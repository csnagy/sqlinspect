package test;

public class BodyCases {
	public void testBody1(int x, int y) {
		x = 1;
		y = 2;
	}

	public int testBody2(int z) {
		return z + 2;
	}

	public void testBody3() {
		int x = 10;
		int y = x + 10;
		return;
	}
}