package test;

public class MixedCases {
	public void testMixed1(int x) {
		try {
			if (x>10) {
				x=1;
				for (int y=0; y<10; y++) {
					if (y>11) {
						x=x+y;
						return;
					}
				}
			} else {
				x=2;
			}
		} catch (Exception e) {
			x=3;
		} finally {
			x=x+2;
		}
		x=x+1;
	}

}