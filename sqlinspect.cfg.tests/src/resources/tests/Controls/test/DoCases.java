package test;

public class DoCases {
	public void testDo1(int x) {
		do {
		} while (true);
	}

	public void testDo2(int x) {
		do {
			x++;
		} while (x <= 10);
	}

	public void testDo3(int x) {
		do {
			if (x > 10) {
				break;
			} else {
				x = x + 1;
			}
		} while (x <= 30);
	}

	public void testDoContinue1(int x) {
		do {
			if (x > 10) {
				continue;
			} else {
				x = x + 1;
			}
		} while (x <= 30);
	}

	public void testDoBreak1(int x) {
		do {
			if (x > 10) {
				break;
			} else {
				x = x + 1;
			}
		} while (x <= 30);
	}

	public int testDoReturn1(int x) {
		do {
			x++;
			return x + 10;
		} while (x < 10);
	}

	public int testDoReturn2(int x) {
		do {
			x++;
			if (x > 20) {
				return x + 20;
			}
		} while (x < 10);
		return x + 10;
	}
}