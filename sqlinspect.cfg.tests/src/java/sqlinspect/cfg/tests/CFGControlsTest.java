package sqlinspect.cfg.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jface.preference.IPreferenceStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGStore;
import sqlinspect.cfg.MethodDeclVisitor;
import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.AndroidHotspotFinder;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.utils.ASTStore;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.plugin.utils.EclipseProjectUtil;
import sqlinspect.sql.SQLDialect;

class CFGControlsTest {
	private static final Logger LOG = LoggerFactory.getLogger(CFGSimpleTest.class);

	protected static final String TESTS_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/tests";
	protected static final String TEST_REF_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/references";

	private final ASTStore astStore = new ASTStore();
	private final CFGStore cfgStore = new CFGStore();

	private final Map<String, MethodDeclaration> methodMap = new HashMap<>();

	private static final String projectname = "Controls";
	private static final String projectdir = TESTS_DIR + "/" + projectname;
	private static final String cfgSuffix = "-cfg.dump";
	private static final String cfgExportDir = projectname;
	private static final String cfgReferenceDir = TEST_REF_DIR + "/" + projectname;

	private void setProjectProperties(IProject iproj) {
		IPreferenceStore prefs = Activator.getDefault().getCurrentPreferenceStore(iproj);
		prefs.setValue(PreferenceConstants.STRING_RESOLVER, InterQueryResolver.class.getSimpleName());
		prefs.setValue(PreferenceConstants.HOTSPOT_FINDERS, AndroidHotspotFinder.class.getSimpleName());
		prefs.setValue(PreferenceConstants.DIALECT, SQLDialect.SQLITE.toString());
		prefs.setValue(PreferenceConstants.RUN_SMELL_DETECTORS, false);
		prefs.setValue(PreferenceConstants.RUN_SQL_METRICS, false);
		prefs.setValue(PreferenceConstants.RUN_TABLE_ACCESS, false);
	}

	protected void createOutputDir(String outputDir) throws IOException {
		File dir = new File(outputDir);
		Path path = dir.toPath();
		if (!Files.exists(path)) {
			Files.createDirectory(path);
		}
	}

	private void analyzeProject(String projectname, String projectdir)
			throws CoreException, JavaModelException, FileNotFoundException, UnsupportedEncodingException, IOException {
		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir);
		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);
		setProjectProperties(iproj);

		Set<MethodDeclaration> methods = new HashSet<>();

		if (iproj.hasNature(JavaCore.NATURE_ID)) {
			IJavaProject javaProject = project.getIJavaProject();
			IPackageFragment[] packages = javaProject.getPackageFragments();
			for (IPackageFragment p : packages) {
				if (p.getKind() == IPackageFragmentRoot.K_SOURCE) {
					for (ICompilationUnit unit : p.getCompilationUnits()) {
						MethodDeclVisitor visitor = new MethodDeclVisitor();
						methods.addAll(visitor.run(astStore.get(unit)));
					}
				}
			}
		} else {
			LOG.error("The project is not a Java project!");
		}

		for (MethodDeclaration method : methods) {
			methodMap.put(ASTUtils.getMethodQualifiedName(method), method);
			LOG.debug("Method: {}", ASTUtils.getMethodQualifiedName(method));
		}
	}

	private void testMethodCFG(String methodSignature) throws IOException {
		final String cfgExport = cfgExportDir + "/" + methodSignature + cfgSuffix;
		final String cfgReference = cfgReferenceDir + "/" + methodSignature + cfgSuffix;
		LOG.debug("Exporting CFG of method {} to {}", methodSignature, cfgExport);

		try (PrintWriter pw = new PrintWriter(new File(cfgExport), StandardCharsets.UTF_8)) {

			MethodDeclaration md = methodMap.get(methodSignature);

			CFG cfg = cfgStore.get(md);

			cfg.dump(pw);
		}

		LOG.debug("CFG exported to {}", cfgExport);
		assertEquals(FileUtils.readLines(new File(cfgExport), StandardCharsets.UTF_8),
				FileUtils.readLines(new File(cfgReference), StandardCharsets.UTF_8));
	}

	@BeforeEach
	void prepareProject() throws CoreException, IOException {
		analyzeProject(projectname, projectdir);
		createOutputDir(cfgExportDir);
	}

	// Do While

	@Test
	void testDo1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDo1");
	}

	@Test
	void testDo2() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDo2");
	}

	@Test
	void test3() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDo3");
	}

	@Test
	void testDoBreak1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoBreak1");
	}

	@Test
	void testDoContinue1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoContinue1");
	}

	@Test
	void testDoReturn1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoReturn1");
	}

	@Test
	void testDoReturn2() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoReturn2");
	}

	// Enhanced For Cases

	@Test
	void testEnhancedFor1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedFor1");
	}

	@Test
	void testEnhancedForBreak1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForBreak1");
	}

	@Test
	void testEnhancedForBreak2() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForBreak2");
	}

	@Test
	void testEnhancedForContinue1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForContinue1");
	}

	@Test
	void testEnhancedForContinue2() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForContinue2");
	}

	@Test
	void testEnhancedForReturn1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForReturn1");
	}

	@Test
	void testEnhancedForReturn2() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForReturn2");
	}

	// For Cases

	@Test
	void testFor1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testFor1");
	}

	@Test
	void testFor2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testFor2");
	}

	@Test
	void testFor3() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testFor3");
	}

	@Test
	void testForBreak1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak1");
	}

	@Test
	void testForBreak2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak2");
	}

	@Test
	void testForBreak3() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak3");
	}

	@Test
	void testForBreak4() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak4");
	}

	@Test
	void testForContinue1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue1");
	}

	@Test
	void testForContinue2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue2");
	}

	@Test
	void testForContinue3() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue3");
	}

	@Test
	void testForContinue4() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue4");
	}

	@Test
	void testForReturn1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForReturn1");
	}

	@Test
	void testForReturn2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForReturn2");
	}

	// If Cases

	@Test
	void testReturnIf1() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIfReturn1");
	}

	@Test
	void testReturnIf2() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIfReturn2");
	}

	@Test
	void testReturnIf3() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIfReturn3");
	}

	@Test
	void testIf1() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf1");
	}

	@Test
	void testIf2() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf2");
	}

	@Test
	void testIf3() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf3");
	}

	@Test
	void testIf4() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf4");
	}

	@Test
	void testIf5() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf5");
	}

	@Test
	void testIf6() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf6");
	}

	// Mixed Cases

	@Test
	void testMixed1() throws CoreException, IOException {
		testMethodCFG("test.MixedCases.testMixed1");
	}

	// Body Cases

	@Test
	void testBody1() throws CoreException, IOException {
		testMethodCFG("test.BodyCases.testBody1");
	}

	@Test
	void testBody2() throws CoreException, IOException {
		testMethodCFG("test.BodyCases.testBody2");
	}

	@Test
	void testBody3() throws CoreException, IOException {
		testMethodCFG("test.BodyCases.testBody3");
	}

	// Switch Cases

	@Test
	void testSwitch1() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch1");
	}

	@Test
	void testSwitch2() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch2");
	}

	@Test
	void testSwitch3() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch3");
	}

	@Test
	void testSwitch4() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch4");
	}

	@Test
	void testSwitch5() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch5");
	}

	@Test
	void testSwitch6() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch6");
	}

	@Test
	void testSwitch7() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch7");
	}

	@Test
	void testSwitchReturn1() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitchReturn1");
	}

	@Test
	void testSwitchReturn2() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitchReturn2");
	}

	// Try Cases
	@Test
	void testTry1() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry1");
	}

	@Test
	void testTry2() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry2");
	}

	@Test
	void testTry3() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry3");
	}

	@Test
	void testTry4() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry4");
	}

	@Test
	void testTry5() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry5");
	}

	@Test
	void testTry6() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry6");
	}

	@Test
	void testTry7() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry7");
	}

	@Test
	void testTry8() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry8");
	}

	@Test
	void testTry9() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry9");
	}

	@Test
	void testWhile1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhile1");
	}

	@Test
	void testWhile2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhile2");
	}

	@Test
	void testWhileBreak1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileBreak1");
	}

	@Test
	void testWhileBreak2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileBreak2");
	}

	@Test
	void testWhileContinue1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileContinue1");
	}

	@Test
	void testWhileContinue2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileContinue2");
	}

	@Test
	void testWhileReturn1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileReturn1");
	}

	@Test
	void testWhileReturn2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileReturn2");
	}
}
