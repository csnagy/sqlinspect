package sqlinspect.cfg.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jface.preference.IPreferenceStore;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.cfg.MethodDeclVisitor;
import sqlinspect.plugin.Activator;
import sqlinspect.plugin.extractors.AndroidHotspotFinder;
import sqlinspect.plugin.extractors.InterQueryResolver;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.utils.ASTStore;
import sqlinspect.plugin.utils.EclipseProjectUtil;
import sqlinspect.sql.SQLDialect;

class CFGSimpleTest {
	private static final Logger LOG = LoggerFactory.getLogger(CFGSimpleTest.class);

	protected static final String TESTS_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/tests";
	protected static final String TEST_REF_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/references";

	private void setProjectProperties(IProject iproj) {
		IPreferenceStore prefs = Activator.getDefault().getCurrentPreferenceStore(iproj);
		prefs.setValue(PreferenceConstants.STRING_RESOLVER, InterQueryResolver.class.getSimpleName());
		prefs.setValue(PreferenceConstants.HOTSPOT_FINDERS, AndroidHotspotFinder.class.getSimpleName());
		prefs.setValue(PreferenceConstants.DIALECT, SQLDialect.SQLITE.toString());
		prefs.setValue(PreferenceConstants.RUN_SMELL_DETECTORS, false);
		prefs.setValue(PreferenceConstants.RUN_SQL_METRICS, false);
		prefs.setValue(PreferenceConstants.RUN_TABLE_ACCESS, false);
	}

	private void analyzeProject(String projectname, String projectdir, String cfgExport, String cfgReference)
			throws CoreException, JavaModelException, FileNotFoundException, UnsupportedEncodingException, IOException {
		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir);
		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		Project project = projectRepository.createProject(iproj);
		setProjectProperties(iproj);

		ASTStore astStore = new ASTStore();
		CFGStore cfgStore = new CFGStore();

		Set<MethodDeclaration> methods = new HashSet<>();

		if (iproj.hasNature(JavaCore.NATURE_ID)) {
			IJavaProject javaProject = project.getIJavaProject();
			IPackageFragment[] packages = javaProject.getPackageFragments();
			for (IPackageFragment p : packages) {
				if (p.getKind() == IPackageFragmentRoot.K_SOURCE) {
					for (ICompilationUnit unit : p.getCompilationUnits()) {
						MethodDeclVisitor visitor = new MethodDeclVisitor();
						methods.addAll(visitor.run(astStore.get(unit)));
					}
				}
			}
		} else {
			LOG.error("The project is not a Java project!");
		}

		methods.stream().forEach(method -> cfgStore.get(method));

		LOG.debug("Exports {}", cfgExport);

		try (PrintWriter pw = new PrintWriter(new File(cfgExport), StandardCharsets.UTF_8)) {
			cfgStore.dump(pw);
		}

		LOG.debug("CFGStore exported to {}", cfgExport);
		assertEquals(FileUtils.readLines(new File(cfgExport), StandardCharsets.UTF_8),
				FileUtils.readLines(new File(cfgReference), StandardCharsets.UTF_8));
	}

	@Test
	void testSimple() throws CoreException, IOException {
		String projectname = "Simple";
		String projectdir = TESTS_DIR + "/" + projectname;
		String cfgSuffix = "-cfg.dump";
		String cfgExport = projectname + cfgSuffix;
		String cfgReference = TEST_REF_DIR + "/" + projectname + cfgSuffix;

		analyzeProject(projectname, projectdir, cfgExport, cfgReference);
	}
}
