FROM ubuntu:20.04
RUN apt update
RUN apt-get -y install xserver-xorg-video-dummy openjdk-11-jdk
WORKDIR /opt/sqlinspect
COPY sqlinspect.product/target/products/sqlinspect.product/linux/gtk/x86_64 /opt/sqlinspect
RUN ln -s /opt/sqlinspect/sqlinspect /usr/local/bin/sqlinspect
CMD /root
