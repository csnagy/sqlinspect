# SQLInspect #

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://bitbucket.org/csnagy/sqlinspect)
[![MIT license](https://img.shields.io/badge/license-NewBSD-green)](https://opensource.org/license/bsd-3-clause/)

## Introduction ##

SQLInspect is a static SQL analyzer with plug-in support for Eclipse to inspect database usage in Java applications. It statically extracts SQL queries embedded in Java and performs various analyses to support developers in SQL-related maintenance tasks.

SQLInspect implements:

 * A static control-flow-based SQL extraction
 * A SQL parser that can tolerate unresolved code fragments
 * An identifier resolution of database objects
 * SQL quality metrics
 * SQL bad smell detectors
 * A tree-matching algorithm to search for a query within the source code
 
 ![SQLInspect Screenshot](sqlinspect.png)

## Getting Started ##

SQLInspect can be used as an Eclipse plug-in or a command line tool.

See the [Getting Started Guide](docs/getting_started.md) for instructions on installing and running SQLInspect.

## Documentation ##

- [Getting Started Guide](docs/getting_started.md)
- [Analysis details](docs/analyses.md)
- [SQL Smells](docs/smells.md)
- [Metrics](docs/metrics.md)
- [Developer Guide](docs/development.md)


## Licensing ##

SQLInspect is released under [New BSD License](https://opensource.org/license/bsd-3-clause/). See [LICENSE.txt](https://bitbucket.org/csnagy/sqlinspect/src/master/LICENSE.txt) for details.

© [Csaba Nagy](https://csnagy.github.io) ([Software Institute](https://www.si.usi.ch/), [USI Università della Svizzera italiana](https://www.usi.ch/it), Switzerland)

## References ##

  1. Csaba Nagy and Anthony Cleve. "SQLInspect: A Static Analyzer to Inspect Database Usage in Java Applications" In Proceedings of the 40th International Conference on Software Engineering (ICSE 2018), May 27 - 3 June 2018, Gothenburg, Sweden
  2. Csaba Nagy and Anthony Cleve. "Static Code Smell Detection in SQL Queries Embedded in Java Code", In Proceedings of the 17th IEEE International Working Conference on Source Code Analysis and Manipulation (SCAM 2017), September 17-18, 2017 - Shanghai, China
  3. Loup Meurice, Csaba Nagy, and Anthony Cleve. "Static Analysis of Dynamic Database Usage in Java Systems", In Proceedings of the 28th International Conference on Advanced Information Systems Engineering (CAiSE 2016), 13-17 June 2016, Ljubljana, Slovenia
  4. Bill Karwin. "SQL Antipatterns: Avoiding the Pitfalls of Database Programming", The Pragmatic Bookshelf, 2010
