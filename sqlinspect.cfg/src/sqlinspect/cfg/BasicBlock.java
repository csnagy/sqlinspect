package sqlinspect.cfg;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicBlock {
	private static final Logger LOG = LoggerFactory.getLogger(BasicBlock.class);

	private final int id;
	private final List<ASTNode> nodes = new ArrayList<>();
	private final Set<BasicBlock> prevs = new HashSet<>();
	private final Set<BasicBlock> succs = new HashSet<>();

	public BasicBlock(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public List<ASTNode> nodes() {
		return nodes;
	}

	public Set<BasicBlock> succs() {
		return succs;
	}

	public Set<BasicBlock> prevs() {
		return prevs;
	}

	public void dump() {
		LOG.debug("BasicBlock: {}", id);
		final String prevsStr = prevs.stream().map(bb -> Integer.toString(bb.getId()))
				.collect(Collectors.joining(", "));
		final String succsStr = succs.stream().map(bb -> Integer.toString(bb.getId()))
				.collect(Collectors.joining(", "));
		LOG.debug("Prevs: {}", prevsStr);
		LOG.debug("Succs: {}", succsStr);
		LOG.debug("Statements: ");
		nodes.stream().forEach(n -> LOG.debug("  : {}", n));
	}

	public void dump(PrintWriter pw) {
		pw.format("  BasicBlock: %d%n", id);
		final String prevsStr = prevs.stream().sorted((bb1, bb2) -> Integer.compare(bb1.getId(), bb2.getId()))
				.map(bb -> Integer.toString(bb.getId())).collect(Collectors.joining(", "));
		final String succsStr = succs.stream().sorted((bb1, bb2) -> Integer.compare(bb1.getId(), bb2.getId()))
				.map(bb -> Integer.toString(bb.getId())).collect(Collectors.joining(", "));
		pw.format("  Prevs: %s%n", prevsStr);
		pw.format("  Succs: %s%n", succsStr);
		pw.println("  Statements: ");
		nodes.stream().forEach(n -> pw.println("  : " + n));
		pw.println();
	}
}
