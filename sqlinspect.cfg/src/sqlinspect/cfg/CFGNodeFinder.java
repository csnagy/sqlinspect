package sqlinspect.cfg;

import java.util.ListIterator;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Statement;

public class CFGNodeFinder {
	private final CFG cfg;
	private final ASTNode node;
	private ListIterator<ASTNode> listIterator;
	private BasicBlock block;

	public CFGNodeFinder(CFG cfg, ASTNode node) {
		this.cfg = cfg;
		this.node = node;
	}

	public boolean findStatement() {
		for (BasicBlock actualBB : cfg.basicblocks()) {
			ListIterator<ASTNode> lit = actualBB.nodes().listIterator(actualBB.nodes().size());
			while (lit.hasPrevious()) {
				ASTNode actualNode = lit.previous();
				if (node.equals(getParentStatement(actualNode))) {
					block = actualBB;
					lit.next(); // step back
					listIterator = lit;
					return true;
				}
			}
		}
		return false;
	}

	public ListIterator<ASTNode> getListIterator() {
		return listIterator;
	}

	public BasicBlock getBlock() {
		return block;
	}

	private static Statement getParentStatement(ASTNode node) {
		ASTNode parent = node;
		while (parent != null && !(parent instanceof Statement)) {
			parent = parent.getParent();
		}
		if (parent instanceof Statement statement) {
			return statement;
		}
		return null;
	}

}
