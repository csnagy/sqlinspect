package sqlinspect.cfg;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AssertStatement;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CFGBuilder extends ASTVisitor {
	private static final Logger LOG = LoggerFactory.getLogger(CFGBuilder.class);
	private final CFG cfg;

	private Optional<BasicBlock> actualBB = Optional.empty();

	private final Map<ASTNode, BasicBlock> leaders = new HashMap<>();

	private final Map<ASTNode, ASTNode> leaderAfterRefs = new HashMap<>();
	private final Deque<ASTNode> leaderAfterRefStack = new ArrayDeque<>();

	private final Map<ASTNode, ASTNode> leaderRefs = new HashMap<>();
	private final Deque<ASTNode> leaderRefStack = new ArrayDeque<>();

	private Optional<ASTNode> lastStmt = Optional.empty();

	private final List<ASTNode> pendingStmt = new ArrayList<>();

	private final Map<ASTNode, List<ASTNode>> controlEdgeOut = new HashMap<>();

	private boolean noLoops;

	public CFGBuilder(MethodDeclaration method) {
		super();
		cfg = new CFG(method);
	}

	public CFG run(MethodDeclaration method) {
		clearSets();
		buildBasicBlocksAndControlDependencies(method);
		collectPendingEdges(method);
		createControlEdgesFromTheEntryPoint(method);
		createControlEdgesForBasicBlocks(method);
		clearSets();
		return cfg;
	}

	private void createControlEdgesForBasicBlocks(MethodDeclaration method) {
		for (BasicBlock bb : cfg.basicblocks()) {
			if (!bb.nodes().isEmpty()) {
				ASTNode lastStatementOfBasicBlock = bb.nodes().get(bb.nodes().size() - 1);
				if (lastStatementOfBasicBlock instanceof ReturnStatement) {
					closeBasicBlockWithReturn(bb);
				} else if (lastStatementOfBasicBlock instanceof BreakStatement breakStatement) {
					closeBasicBlockWithBreak(method, bb, lastStatementOfBasicBlock, breakStatement);
				} else if (lastStatementOfBasicBlock instanceof ContinueStatement continueStatement) {
					closeBasicBlockWithContinue(bb, lastStatementOfBasicBlock, continueStatement);
				} else {
					List<ASTNode> outEdges = controlEdgeOut.get(lastStatementOfBasicBlock);
					if (outEdges != null && !outEdges.isEmpty()) {
						outEdges.stream().filter(outLeader -> outLeader == method)
								.forEach(outBB -> connectBB(bb, cfg.getExit()));
						outEdges.stream().filter(outLeader -> outLeader != method).map(leaders::get)
								.filter(Objects::nonNull).forEach(outBB -> connectBB(bb, outBB));
					} else if (!bb.equals(cfg.getExit())) {
						LOG.trace("BB without out edge: {}", bb.getId());
						closeBasicBlockWithReturn(bb);
					}
				}
			}
		}
	}

	private void closeBasicBlockWithContinue(BasicBlock basicBlock, ASTNode lastStatementOfBasicBlock,
			ContinueStatement continueStatement) {
		if (!noLoops) {
			Optional<Statement> wdf = getEnclosingWDF(continueStatement);
			if (wdf.isPresent()) {
				ASTNode leaderWDF = leaderRefs.get(wdf.get());
				if (leaderWDF != null) {
					BasicBlock nextBB = leaders.get(leaderWDF);
					connectBB(basicBlock, nextBB);
				}
			} else {
				LOG.warn("Continue statement without parent while/do/for: {}", lastStatementOfBasicBlock);
			}
		}
	}

	private void closeBasicBlockWithBreak(MethodDeclaration method, BasicBlock basicBlock, ASTNode lastStatementOfBasicBlock,
			BreakStatement breakStatement) {
		Optional<Statement> swdf = getEnclosingSWDF(breakStatement);
		if (swdf.isPresent()) {
			ASTNode leaderAfterSWDF = leaderAfterRefs.get(swdf.get());
			if (leaderAfterSWDF != null) {
				if (leaderAfterSWDF.equals(method)) {
					closeBasicBlockWithReturn(basicBlock);
				} else {
					BasicBlock nextBB = leaders.get(leaderAfterSWDF);
					if (nextBB != null) {
						connectBB(basicBlock, nextBB);
					}
				}
			}
		} else {
			LOG.warn("Break statement without parent switch/while/do/for: {}", lastStatementOfBasicBlock);
		}
	}

	private void closeBasicBlockWithReturn(BasicBlock basicBlock) {
		connectBB(basicBlock, cfg.getExit());
	}

	private void createControlEdgesFromTheEntryPoint(MethodDeclaration method) {
		List<ASTNode> firstEdges = controlEdgeOut.get(method);
		if (firstEdges != null && !firstEdges.isEmpty()) {
			for (ASTNode firstEdge : firstEdges) {
				BasicBlock firstBB = leaders.get(firstEdge);
				if (firstBB != null) {
					connectBB(cfg.getEntry(), firstBB);
				} else {
					connectBB(cfg.getEntry(), cfg.getExit());
				}
			}
		} else {
			connectBB(cfg.getEntry(), cfg.getExit());
		}
	}

	private void buildBasicBlocksAndControlDependencies(MethodDeclaration method) {
		method.accept(this);
	}

	public CFG run(MethodDeclaration method, boolean noLoops) {
		this.noLoops = noLoops;
		return run(method);
	}

	private void clearSets() {
		leaders.clear();
		leaderAfterRefs.clear();
		leaderAfterRefStack.clear();
		leaderRefs.clear();
		leaderRefStack.clear();
		pendingStmt.clear();
		controlEdgeOut.clear();
	}

	public static CFG build(MethodDeclaration method) {
		CFGBuilder cfgBuilder = new CFGBuilder(method);
		return cfgBuilder.run(method);
	}

	public static CFG build(MethodDeclaration method, boolean noLoops) {
		CFGBuilder cfgBuilder = new CFGBuilder(method);
		return cfgBuilder.run(method, noLoops);
	}

	@Override
	public boolean visit(MethodDeclaration method) {

		lastStmt = Optional.of(method);
		pendingStmt.add(method);

		Block body = method.getBody();
		if (body != null) {
			body.accept(this);
		}

		return false;
	}

	protected void collectPendingEdges(MethodDeclaration method) {
		if (!leaderRefStack.isEmpty()) {
			ASTNode leaderRef = leaderRefStack.pop();
			leaderRefs.put(leaderRef, method);
		}
		if (!leaderAfterRefStack.isEmpty()) {
			ASTNode leaderAfterRef = leaderAfterRefStack.pop();
			leaderAfterRefs.put(leaderAfterRef, method);
		}
		if (!actualBB.isPresent()) {
			for (ASTNode pending : pendingStmt) {
				controlEdgeOut.computeIfAbsent(pending, k -> new ArrayList<>()).add(method);
			}
			pendingStmt.clear();
		}
	}

	protected void addToActualBasicBlock(ASTNode node) {
		if (!actualBB.isPresent()) {
			actualBB = Optional.of(cfg.createBasicBlock());
			leaders.put(node, actualBB.get());
			if (!leaderRefStack.isEmpty()) {
				ASTNode leaderRef = leaderRefStack.pop();
				leaderRefs.put(leaderRef, node);
			}
			if (!leaderAfterRefStack.isEmpty()) {
				ASTNode leaderAfterRef = leaderAfterRefStack.pop();
				leaderAfterRefs.put(leaderAfterRef, node);
			}
			for (ASTNode pending : pendingStmt) {
				controlEdgeOut.computeIfAbsent(pending, k -> new ArrayList<>()).add(node);
			}
			pendingStmt.clear();
		}
		actualBB.get().nodes().add(node);
		lastStmt = Optional.of(node);
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(ReturnStatement node) {
		addToActualBasicBlock(node);
		actualBB = Optional.empty();
		return false;
	}

	@Override
	public boolean visit(ContinueStatement node) {
		addToActualBasicBlock(node);
		actualBB = Optional.empty();
		return false;
	}

	@Override
	public boolean visit(BreakStatement node) {
		addToActualBasicBlock(node);
		actualBB = Optional.empty();
		return false;
	}

	@Override
	public boolean visit(ThrowStatement node) {
		addToActualBasicBlock(node);
		actualBB = Optional.empty();
		return false;
	}

	@Override
	public boolean visit(ConstructorInvocation node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(AssertStatement node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(LabeledStatement node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(SwitchCase node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		addToActualBasicBlock(node);
		return false;
	}

	@Override
	public boolean visit(IfStatement node) {
		Expression cond = node.getExpression();
		if (cond == null) {
			LOG.error("Malformed if statement: {}", node);
			return false;
		}

		addToActualBasicBlock(cond);

		// handle then branch
		Statement thenStmt = node.getThenStatement();
		Optional<ASTNode> lastThenStmt = Optional.empty();
		if (thenStmt != null) {
			pendingStmt.add(cond);
			leaderRefStack.add(thenStmt);
			actualBB = Optional.empty();
			thenStmt.accept(this);
			lastThenStmt = lastStmt;

		}

		// handle else branch
		Statement elseStmt = node.getElseStatement();
		Optional<ASTNode> lastElseStmt = Optional.empty();
		if (elseStmt != null) {
			List<ASTNode> olds = new ArrayList<>(pendingStmt);
			pendingStmt.clear();
			pendingStmt.add(cond);
			leaderRefStack.add(elseStmt);
			actualBB = Optional.empty();
			elseStmt.accept(this);
			lastElseStmt = lastStmt;
			pendingStmt.addAll(olds);
		}

		if (lastThenStmt.isPresent()) {
			pendingStmt.add(lastThenStmt.get());
		}

		if (lastElseStmt.isPresent()) {
			pendingStmt.add(lastElseStmt.get());
		} else {
			pendingStmt.add(cond);
		}

		actualBB = Optional.empty();
		return false;
	}

	@Override
	public boolean visit(SwitchStatement stmt) {
		Expression cond = stmt.getExpression();
		if (cond == null) {
			LOG.warn("Malformed Switch statement {}", stmt);
			return false;
		}

		leaderRefStack.add(stmt);
		addToActualBasicBlock(cond);
		actualBB = Optional.empty();

		for (Object el : stmt.statements()) {
			if (el instanceof Statement statement) {
				if (statement instanceof SwitchCase) {
					pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
					actualBB = Optional.empty();
					controlEdgeOut.computeIfAbsent(cond, k -> new ArrayList<>()).add(statement);
				}
				statement.accept(this);
			}
		}

		pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
		leaderAfterRefStack.add(stmt);
		actualBB = Optional.empty();
		return false;
	}

	@Override
	public boolean visit(ForStatement stmt) {

		handleForInitializers(stmt);

		ASTNode prevStmt = lastStmt.orElseThrow(IllegalStateException::new);

		Expression cond = stmt.getExpression();
		if (cond != null) {
			handleForCondition(stmt, prevStmt, cond);
		}

		Statement body = stmt.getBody();
		Optional<ASTNode> lastBodyStatement = Optional.empty();
		if (body != null) {
			handleForBody(prevStmt, cond, body);
			lastBodyStatement = lastStmt;
		}

		Optional<ASTNode> lastUpdater = Optional.empty();
		if (!stmt.updaters().isEmpty()) {
			handleForUpdaters(stmt);
			lastUpdater = lastStmt;
		}

		if (!noLoops) {
			if (lastUpdater.isPresent()) {
				controlEdgeOut.computeIfAbsent(lastUpdater.get(), k -> new ArrayList<>()).add(cond);
			} else if (lastBodyStatement.isPresent()) {
				controlEdgeOut.computeIfAbsent(lastBodyStatement.get(), k -> new ArrayList<>()).add(cond);
			}
		} else {
			if (lastUpdater.isPresent()) {
				pendingStmt.add(lastUpdater.get());
			} else if (lastBodyStatement.isPresent()) {
				pendingStmt.add(lastBodyStatement.get());
			}
		}

		if (cond != null) {
			pendingStmt.add(cond);
			leaderAfterRefStack.add(stmt);
			actualBB = Optional.empty();
		}

		return false;
	}

	private void handleForUpdaters(ForStatement stmt) {
		leaderRefStack.add((ASTNode) stmt.updaters().get(0));
		actualBB = Optional.empty();
		pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
		for (Object o : stmt.updaters()) {
			if (o instanceof Expression updater) {
				addToActualBasicBlock(updater);
			} else {
				LOG.error("Invalid updater: {}", o);
			}
		}
		leaderRefs.put(stmt, (ASTNode) stmt.updaters().get(0));
	}

	private void handleForBody(ASTNode prevStmt, Expression cond, Statement body) {
		if (cond != null) {
			pendingStmt.add(cond);
		} else {
			pendingStmt.add(prevStmt);
		}
		leaderRefStack.add(body);
		actualBB = Optional.empty();
		body.accept(this);
	}

	private void handleForCondition(ForStatement stmt, ASTNode prevStmt, Expression cond) {
		leaderRefStack.add(cond);
		actualBB = Optional.empty();
		pendingStmt.add(prevStmt);
		addToActualBasicBlock(cond);
		leaderRefs.put(stmt, leaderRefs.get(cond));
	}

	private void handleForInitializers(ForStatement stmt) {
		for (Object o : stmt.initializers()) {
			if (o instanceof Expression initializer) {
				addToActualBasicBlock(initializer);
			} else {
				LOG.error("Invalid initializer: {}", o);
			}
		}
	}

	@Override
	public boolean visit(EnhancedForStatement stmt) {

		pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
		actualBB = Optional.empty();
		leaderRefStack.add(stmt);

		SingleVariableDeclaration param = stmt.getParameter();
		if (param != null) {
			addToActualBasicBlock(param);
		}

		Expression cond = stmt.getExpression();
		if (cond != null) {
			addToActualBasicBlock(cond);
			pendingStmt.add(cond);
			actualBB = Optional.empty();
		}

		Statement body = stmt.getBody();
		Optional<ASTNode> lastBody = Optional.empty();
		if (body != null) {
			body.accept(this);
			lastBody = lastStmt;
		}

		if (cond != null) {
			pendingStmt.add(cond);
			actualBB = Optional.empty();
			leaderAfterRefStack.add(stmt);
		}

		if (cond != null && lastBody.isPresent()) {
			if (!noLoops) {
				controlEdgeOut.computeIfAbsent(lastBody.get(), k -> new ArrayList<>()).add(leaderRefs.get(stmt));
			} else {
				pendingStmt.add(lastBody.get());
			}
		}

		return false;
	}

	@Override
	public boolean visit(WhileStatement stmt) {
		Expression cond = stmt.getExpression();

		pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
		actualBB = Optional.empty();
		if (cond != null) {
			leaderRefStack.add(stmt);
			addToActualBasicBlock(cond);
			pendingStmt.add(cond);
			actualBB = Optional.empty();
		}

		Statement body = stmt.getBody();
		Optional<ASTNode> lastBody = Optional.empty();
		if (body != null) {
			leaderRefStack.add(body);
			body.accept(this);
			lastBody = lastStmt;
		}

		if (cond != null) {
			pendingStmt.add(cond);
			actualBB = Optional.empty();
			leaderAfterRefStack.add(stmt);
		}

		if (cond != null && lastBody.isPresent()) {
			if (!noLoops) {
				controlEdgeOut.computeIfAbsent(lastBody.get(), k -> new ArrayList<>()).add(cond);
			} else {
				pendingStmt.add(lastBody.get());
			}
		}

		return false;
	}

	@Override
	public boolean visit(DoStatement stmt) {

		pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
		actualBB = Optional.empty();

		Statement body = stmt.getBody();
		if (body != null) {
			leaderRefStack.add(body);
			body.accept(this);
			pendingStmt.add(lastStmt.orElseThrow(IllegalStateException::new));
		}

		Expression cond = stmt.getExpression();
		if (cond != null) {
			actualBB = Optional.empty();
			leaderRefStack.add(stmt);
			addToActualBasicBlock(cond);
			pendingStmt.add(cond);
			actualBB = Optional.empty();
			leaderAfterRefStack.add(stmt);
		}

		if (!noLoops && cond != null && body != null) {
			controlEdgeOut.computeIfAbsent(cond, k -> new ArrayList<>()).add(leaderRefs.get(body));
		}

		return false;
	}

	@Override
	public boolean visit(TryStatement stmt) {
		handleTryStatementResources(stmt);

		ASTNode prevStmt = lastStmt.orElseThrow(IllegalStateException::new);
		pendingStmt.add(prevStmt);
		actualBB = Optional.empty();
		List<Optional<ASTNode>> lastStmts = new ArrayList<>();

		Statement body = stmt.getBody();
		if (body != null) {
			handleTryStatementBody(lastStmts, body);
		}

		for (Object e : stmt.catchClauses()) {
			if (e instanceof CatchClause catchClause) {
				handleTryStatementCatchClause(prevStmt, lastStmts, catchClause);
			}
		}

		Statement finallyBody = stmt.getFinally();
		if (finallyBody != null) {
			handleTryStatementFinallyBody(stmt, body, finallyBody);
		} else {
			lastStmts.stream().filter(Optional::isPresent).forEach(s -> pendingStmt.add(s.get()));
			actualBB = Optional.empty();
		}

		return false;
	}

	private void handleTryStatementFinallyBody(TryStatement stmt, Statement body, Statement finallyBody) {
		leaderRefStack.add(finallyBody);
		actualBB = Optional.empty();
		finallyBody.accept(this);
		ASTNode finallyLeader = leaderRefs.get(finallyBody);
		for (Object e : stmt.catchClauses()) {
			if (e instanceof CatchClause catchClause) {
				ASTNode catchLeader = leaderRefs.get(catchClause);
				controlEdgeOut.computeIfAbsent(catchLeader, k -> new ArrayList<>()).add(finallyLeader);
			}
		}
		if (body != null) {
			ASTNode bodyLeader = leaderRefs.get(body);
			controlEdgeOut.computeIfAbsent(bodyLeader, k -> new ArrayList<>()).add(finallyLeader);
		}
	}

	private void handleTryStatementCatchClause(ASTNode prevStmt, List<Optional<ASTNode>> lastStmts,
			CatchClause catchClause) {
		Statement catchBody = catchClause.getBody();
		pendingStmt.add(prevStmt);
		actualBB = Optional.empty();
		leaderRefStack.add(catchClause);
		if (catchBody != null) {
			SingleVariableDeclaration catchException = catchClause.getException();
			if (catchException != null) {
				catchException.accept(this);
			}
			catchBody.accept(this);
			lastStmts.add(lastStmt);
		}
		ASTNode leader = leaderRefs.get(catchClause);
		controlEdgeOut.computeIfAbsent(prevStmt, k -> new ArrayList<>()).add(leader);
	}

	private void handleTryStatementBody(List<Optional<ASTNode>> lastStmts, Statement body) {
		leaderRefStack.add(body);
		body.accept(this);
		lastStmts.add(lastStmt);
	}

	private void handleTryStatementResources(TryStatement stmt) {
		for (Object o : stmt.resources()) {
			if (o instanceof Expression resource) {
				addToActualBasicBlock(resource);
			}
		}
	}

	private static void connectBB(BasicBlock from, BasicBlock to) {
		from.succs().add(to);
		to.prevs().add(from);
	}

	private Optional<Statement> getEnclosingSWDF(BreakStatement node) {
		ASTNode parent = node;

		while (parent != null && !(parent instanceof SwitchStatement || parent instanceof WhileStatement
				|| parent instanceof DoStatement || parent instanceof ForStatement
				|| parent instanceof EnhancedForStatement)) {
			parent = parent.getParent();
		}

		if (parent != null) {
			return Optional.of((Statement) parent);
		}

		return Optional.empty();
	}

	private Optional<Statement> getEnclosingWDF(ContinueStatement node) {
		ASTNode parent = node;

		while (parent != null && !(parent instanceof WhileStatement || parent instanceof DoStatement
				|| parent instanceof ForStatement || parent instanceof EnhancedForStatement)) {
			parent = parent.getParent();
		}

		if (parent != null) {
			return Optional.of((Statement) parent);
		}

		return Optional.empty();
	}

}