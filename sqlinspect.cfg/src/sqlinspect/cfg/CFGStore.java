package sqlinspect.cfg;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;

@Creatable
@Singleton
public class CFGStore {
	private final Map<MethodDeclaration, CFG> cfgs = new HashMap<>();

	private final boolean noLoops;

	public CFGStore(boolean noLoops) {
		this.noLoops = noLoops;
	}

	public CFGStore() {
		this.noLoops = true;
	}

	public CFG get(MethodDeclaration method) {
		return cfgs.computeIfAbsent(method, cfg -> CFGBuilder.build(cfg, noLoops));
	}

	public void clear() {
		cfgs.clear();
	}

	public void remove(MethodDeclaration method) {
		cfgs.remove(method);
	}

	public Map<MethodDeclaration, CFG> getCfgs() {
		return cfgs;
	}

	public void dump(PrintWriter pw) {
		cfgs.entrySet().stream().sorted((e1, e2) -> getMethodID(e1.getKey()).compareTo(getMethodID(e2.getKey())))
				.forEach(e -> {
					pw.format("Method: %s%n", e.getKey().resolveBinding());
					e.getValue().dump(pw);
					pw.println("------------");
				});
	}

	private static String getMethodID(MethodDeclaration method) {
		ASTNode root = method.getRoot();
		if (root instanceof CompilationUnit compilationUnit) {
			IJavaElement element = compilationUnit.getJavaElement();
			if (element != null) {
				return element.getPath().toString() + ":" + method.getStartPosition();
			}
		}
		return "UNKNOWN:" + method.getStartPosition();
	}

}
