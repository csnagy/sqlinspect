package sqlinspect.cfg;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public abstract class CFGRevBreadthFirst {
	private static class DepthFirstElement {
		private final BasicBlock element;
		private final int depth;

		public DepthFirstElement(BasicBlock element, int depth) {
			this.element = element;
			this.depth = depth;
		}

		public int getDepth() {
			return depth;
		}

		public BasicBlock getElement() {
			return element;
		}
	}

	public void run(BasicBlock startBB, int maxDepth) {
		Deque<DepthFirstElement> toVisit = new ArrayDeque<>();
		Set<BasicBlock> visited = new HashSet<>();

		toVisit.add(new DepthFirstElement(startBB, 0));
		while (!toVisit.isEmpty()) {
			DepthFirstElement actualElement = toVisit.pop();
			BasicBlock actualBB = actualElement.getElement();
			visited.add(actualBB);

			if (visit(actualBB) && (actualElement.getDepth() < maxDepth || maxDepth == 0)) {
				actualBB.prevs().stream().filter(bb -> !visited.contains(bb))
						.forEach(bb -> toVisit.addLast(new DepthFirstElement(bb, actualElement.getDepth() + 1)));
			}
		}
	}

	protected abstract boolean visit(BasicBlock bb);

}
