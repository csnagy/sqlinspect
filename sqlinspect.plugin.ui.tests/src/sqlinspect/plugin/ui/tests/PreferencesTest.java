package sqlinspect.plugin.ui.tests;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable;
import org.eclipse.swtbot.swt.finder.junit5.SWTBotJunit5Extension;
import org.eclipse.swtbot.swt.finder.matchers.WithText;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotLabel;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotMenu;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotRootMenu;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.utils.EclipseProjectUtil;

// Don't forget to run the test outside of the UI thread

// SWTBot supports Junit5, see: https://bugs.eclipse.org/bugs/show_bug.cgi?id=559531
// SWTBotJunit5Extension captures screenshots on test failures
@ExtendWith(SWTBotJunit5Extension.class)
class PreferencesTest {
	private static SWTWorkbenchBot bot;

	protected static final String TESTS_DIR = System.getenv().get("TEST_RES_DIR") + "/tests";
	protected static final String TEST_REF_DIR = System.getenv().get("TEST_RES_DIR") + "/references";

	private Project createProject() throws CoreException {
		String projectname = "Testproj";
		String projectdir = TESTS_DIR + "/" + projectname;

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir, "src");
		IEclipseContext eclipseContext = EclipseContextFactory.create("test context");
		ProjectRepository projectRepository = ContextInjectionFactory.make(ProjectRepository.class, eclipseContext);
		return projectRepository.createProject(iproj);
	}

	@BeforeAll
	static void beforeClass() {
		bot = new SWTWorkbenchBot();
		try {
			bot.viewByTitle("Welcome").close();
		} catch (WidgetNotFoundException e) {
			// ignore
		}
	}

	@BeforeEach
	void beforeEach() {
		// Workaround for Xvfb error WidgetNotFoundException: The widget was null
		// See: https://wiki.eclipse.org/SWTBot/Troubleshooting
		UIThreadRunnable.syncExec(new VoidResult() {
			@Override
			public void run() {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell().forceActive();
			}
		});
	}

	@AfterEach
	void afterEach() {
		bot.resetWorkbench();
	}

	private boolean platformHasSystemMenu() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		Menu systemMenu = UIThreadRunnable.syncExec(new Result<Menu>() {
			@Override
			public Menu run() {
				return workbench.getDisplay().getSystemMenu();
			}
		});
		return systemMenu != null;
	}

	private void openPreferencesFromSystemMenu() {
		// SWTBot cannot select the "Eclipse" menu on MacOS, see:
		// https://www.eclipse.org/forums/index.php/t/854280/

		IWorkbench workbench = PlatformUI.getWorkbench();
		workbench.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
				if (window != null) {
					Menu appMenu = workbench.getDisplay().getSystemMenu();
					for (MenuItem item : appMenu.getItems()) {
						if (item.getText().startsWith("Settings")) {
							Event event = new Event();
							event.time = (int) System.currentTimeMillis();
							event.widget = item;
							event.display = workbench.getDisplay();
							item.setSelection(true);
							item.notifyListeners(SWT.Selection, event);
							break;
						}
					}
				}
			}
		});
	}

	private void openPreferencesFromWindowMenu() {
		SWTBotMenu windowMenu = bot.menu("Window");
		SWTBotMenu preferencesMenu = windowMenu.menu("Settings");
		preferencesMenu.click();
	}

	private void openPreferences() {
		if (platformHasSystemMenu()) {
			openPreferencesFromSystemMenu();
		} else {
			openPreferencesFromWindowMenu();
		}
	}

	@Test
	void canOpenSQLInspectPreferencePage() {
		openPreferences();
		SWTBotShell shell = bot.shell("Preferences");
		shell.activate();
		SWTBotTree sqlinspectSettings = bot.tree().select("SQLInspect");
		assertNotNull(sqlinspectSettings);
	}

	@Test
	void canOpenSQLInspectPropertyPage() throws CoreException {
		@SuppressWarnings("unused")
		Project project = createProject();
		bot.menu("Window").menu("Show View").menu("Project Explorer").click();
		bot.viewByTitle("Project Explorer").show();
		SWTBotTree testProj = bot.tree().select("Testproj");
		assertNotNull(testProj);

		SWTBotRootMenu contextMenu = testProj.contextMenu();
		contextMenu.menu("Properties").click();
		SWTBotTreeItem sqlinspectSettings = bot.tree().getTreeItem("SQLInspect").select();
		assertNotNull(sqlinspectSettings);
	}

	@Test
	void canAnalyzeProject() throws CoreException {
		@SuppressWarnings("unused")
		Project project = createProject();
		bot.menu("Window").menu("Show View").menu("Project Explorer").click();
		bot.viewByTitle("Project Explorer").show();
		SWTBotTree testProj = bot.tree().select("Testproj");
		assertNotNull(testProj);

		SWTBotRootMenu contextMenu = testProj.contextMenu();
		SWTBotMenu sqlinspectMenu = contextMenu.menu("SQLInspect");
		assertNotNull(sqlinspectMenu);

		sqlinspectMenu.menu("Analyze").click();
		Matcher<Shell> shellMatcher = WithText.withTextIgnoringCase("sqlinspect.plugin");
		bot.waitUntil(org.eclipse.swtbot.swt.finder.waits.Conditions.waitForShell(shellMatcher), 60000);
		SWTBotShell dialog = bot.shell("sqlinspect.plugin");
		assertNotNull(dialog);

		SWTBotLabel label = bot.label("Testproj project was analyzed successfully!");
		assertNotNull(label);

		bot.button("OK").click();
		bot.waitUntil(org.eclipse.swtbot.swt.finder.waits.Conditions.shellCloses(dialog));
	}
}
