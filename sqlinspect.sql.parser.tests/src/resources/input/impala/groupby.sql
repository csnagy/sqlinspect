select a, b, count(c) from test group by 1, 2;
select a, b, count(c) from test group by a, b;
select a, b, count(c) from test group by true, false, NULL;
-- semantically wrong but parses fine
select a, b, count(c) from test group by 1, b;
select a, b, count(c) from test group 1, 2; -- error
select a, b, count(c) from test group by order by a; -- error

