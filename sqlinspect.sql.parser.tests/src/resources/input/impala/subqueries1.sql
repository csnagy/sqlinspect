-- Binary predicate with an arithmetic expr
SELECT * FROM foo WHERE a+1 > (SELECT count(a) FROM bar);
SELECT * FROM foo WHERE (SELECT count(a) FROM bar) < a+1;

-- [NOT] IN nested predicates
SELECT * FROM foo WHERE a IN (SELECT a FROM bar);
SELECT * FROM foo WHERE a NOT IN (SELECT a FROM bar);

-- [NOT] EXISTS predicates
SELECT * FROM foo WHERE EXISTS (SELECT a FROM bar WHERE b > 0);
SELECT * FROM foo WHERE NOT EXISTS (SELECT a FROM bar WHERE b > 0);
SELECT * FROM foo WHERE NOT (EXISTS (SELECT a FROM bar));

-- Compound nested predicates
SELECT * FROM foo WHERE a = (SELECT count(a) FROM bar) AND b != (SELECT count(b) FROM baz) and c IN (SELECT c FROM qux);
SELECT * FROM foo WHERE EXISTS (SELECT a FROM bar WHERE b < 0) AND NOT EXISTS (SELECT a FROM baz WHERE b > 0);
SELECT * FROM foo WHERE EXISTS (SELECT a from bar) AND NOT EXISTS (SELECT a FROM baz) AND b IN (SELECT b FROM bar) AND c NOT IN (SELECT c FROM qux) AND d = (SELECT max(d) FROM quux);

-- Nested parentheses
SELECT * FROM foo WHERE EXISTS ((SELECT * FROM bar));
SELECT * FROM foo WHERE EXISTS (((SELECT * FROM bar)));
SELECT * FROM foo WHERE a IN ((SELECT a FROM bar));
SELECT * FROM foo WHERE a = ((SELECT max(a) FROM bar));

-- More than one nesting level
SELECT * FROM foo WHERE a IN (SELECT a FROM bar WHERE b IN (SELECT b FROM baz));
SELECT * FROM foo WHERE EXISTS (SELECT a FROM bar WHERE b NOT IN (SELECT b FROM baz WHERE c < 10 AND d = (SELECT max(d) FROM qux)));

-- Malformed nested subqueries
-- Missing or misplaced parenthesis around a subquery
SELECT * FROM foo WHERE a IN SELECT a FROM bar; -- error
SELECT * FROM foo WHERE a = SELECT count(*) FROM bar; -- error
SELECT * FROM foo WHERE EXISTS SELECT * FROM bar; -- error
SELECT * FROM foo WHERE a IN (SELECT a FROM bar; -- error
SELECT * FROM foo WHERE a IN SELECT a FROM bar); -- error
SELECT * FROM foo WHERE a IN (SELECT) a FROM bar; -- error

-- Invalid syntax for [NOT] EXISTS
SELECT * FROM foo WHERE a EXISTS (SELECT * FROM bar); -- error
SELECT * FROM foo WHERE a NOT EXISTS (SELECT * FROM bar); -- error

-- Set operations between subqueries
SELECT * FROM foo WHERE EXISTS ((SELECT a FROM bar) UNION (SELECT a FROM baz)); -- error

-- Nested predicate in the HAVING clause
SELECT a, count(*) FROM foo GROUP BY a HAVING count(*) > (SELECT count(*) FROM bar);
SELECT a, count(*) FROM foo GROUP BY a HAVING 10 > (SELECT count(*) FROM bar);

-- Subquery in the SELECT clause
SELECT a, b, (SELECT c FROM foo) FROM foo;
SELECT (SELECT a FROM foo), b, c FROM bar;
SELECT (SELECT (SELECT a FROM foo) FROM bar) FROM baz;
SELECT (SELECT a FROM foo);

-- Malformed subquery in the SELECT clause
SELECT SELECT a FROM foo FROM bar; -- error
SELECT (SELECT a FROM foo FROM bar; -- error
SELECT SELECT a FROM foo) FROM bar; -- error
SELECT (SELECT) a FROM foo; -- error

-- Subquery in the GROUP BY clause
SELECT a, count(*) FROM foo GROUP BY (SELECT a FROM bar);
SELECT a, count(*) FROM foo GROUP BY a, (SELECT b FROM bar);

-- Malformed subquery in the GROUP BY clause
SELECT a, count(*) FROM foo GROUP BY SELECT a FROM bar; -- error
SELECT a, count(*) FROM foo GROUP BY (SELECT) a FROM bar; -- error
SELECT a, count(*) FROM foo GROUP BY (SELECT a FROM bar; -- error

-- Subquery in the ORDER BY clause
SELECT a, b FROM foo ORDER BY (SELECT a FROM bar);
SELECT a, b FROM foo ORDER BY (SELECT a FROM bar) DESC;
SELECT a, b FROM foo ORDER BY a ASC, (SELECT a FROM bar) DESC;

-- Malformed subquery in the ORDER BY clause
SELECT a, count(*) FROM foo ORDER BY SELECT a FROM bar; -- error
SELECT a, count(*) FROM foo ORDER BY (SELECT) a FROM bar DESC; -- error
SELECT a, count(*) FROM foo ORDER BY (SELECT a FROM bar ASC; -- error

