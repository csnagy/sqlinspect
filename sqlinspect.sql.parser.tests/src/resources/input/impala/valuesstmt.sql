-- Values stmt with a single row.
values(1, 'a', abc, 1.0, *);
select * from (values(1, 'a', abc, 1.0, *)) as t;
values(1, 'a', abc, 1.0, *) union all values(1, 'a', abc, 1.0, *);
insert into t values(1, 'a', abc, 1.0, *);
upsert into t values(1, 'a', abc, 1.0, *);
-- Values stmt with multiple rows.
values(1, abc), ('x', cde), (2), (efg, fgh, ghi);
select * from (values(1, abc), ('x', cde), (2), (efg, fgh, ghi)) as t;
values(1, abc), ('x', cde), (2), (efg, fgh, ghi) 
        union all values(1, abc), ('x', cde), (2), (efg, fgh, ghi);
insert into t values(1, abc), ('x', cde), (2), (efg, fgh, ghi);
upsert into t values(1, abc), ('x', cde), (2), (efg, fgh, ghi);
-- Test additional parenthesis.
(values(1, abc), ('x', cde), (2), (efg, fgh, ghi));
values((1, abc), ('x', cde), (2), (efg, fgh, ghi));
(values((1, abc), ('x', cde), (2), (efg, fgh, ghi)));
-- Test alias inside select list to assign column names.
values(1 as x, 2 as y, 3 as z);
-- Test order by and limit.
values(1, 'a') limit 10;
values(1, 'a') order by 1;
values(1, 'a') order by 1 limit 10;
values(1, 'a') order by 1 offset 10;
values(1, 'a') offset 10;
values(1, 'a'), (2, 'b') order by 1 limit 10;
values((1, 'a'), (2, 'b')) order by 1 limit 10;

values(); -- error
values 1, 'a', abc, 1.0; -- error
values(1, 'a') values(1, 'a'); -- error
select values(1, 'a'); -- error
select * from values(1, 'a', abc, 1.0) as t; -- error
values((1, 2, 3), values(1, 2, 3)); -- error
values((1, 'a'), (1, 'a') order by 1); -- error
values((1, 'a'), (1, 'a') limit 10); -- error

