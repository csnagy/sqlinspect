select a 'b' from tbl;
select a as 'b' from tbl;
select a 'x', b as 'y', c 'z' from tbl;
select a 'x', b as 'y', sum(x) over () 'z' from tbl;
select a.b.c.d 'x' from tbl;
select a.b.c.d as 'x' from tbl;
-- Table aliases.
select a from tbl 'b';
select a from tbl as 'b';
select a from db.tbl 'b';
select a from db.tbl as 'b';
select a from db.tbl.col 'b';
select a from db.tbl.col as 'b';
select a from (select * from tbl) 'b';
select a from (select * from tbl) as 'b';
select a from (select * from tbl b) as 'b';

-- With-clause view aliases.
with 't' as (select 1) select * from t;

a from tbl; -- error
select a as a, b c d from tbl; -- error

