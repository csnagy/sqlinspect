-- Test regular exps.
select case a when '5' then x when '6' then y else z end from t;
select case when 'a' then x when false then y else z end from t;
-- Test predicates in case, when, then, and else exprs.
select case when a > 2 then x when false then false else true end from t;
select case false when a > 2 then x when '6' then false else true end from t;
-- Test NULLs;
select case NULL when NULL then NULL when NULL then NULL else NULL end from t;
select case when NULL then NULL when NULL then NULL else NULL end from t;
-- Missing end.
select case a when true then x when false then y else z from t; -- error
-- Missing else after first when.
select case a when true when false then y else z end from t; -- error
-- Incorrectly placed comma.
select case a when true, false then y else z end from t; -- error

