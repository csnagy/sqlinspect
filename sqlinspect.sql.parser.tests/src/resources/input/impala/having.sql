select a, b, count(c) from test group by a, b having count(*) > 5;
select a, b, count(c) from test group by a, b having NULL;
select a, b, count(c) from test group by a, b having true;
select a, b, count(c) from test group by a, b having false;
-- Non-predicate exprs that return boolean.
select count(c) from test group by a having if (a > b, true, false);
select count(c) from test group by a having case a when b then true else false end;
-- Arbitrary non-predicate exprs parse ok but are semantically incorrect.
select a, b, count(c) from test group by a, b having 5;
select a, b, count(c) from test group by a, b having order by 5; -- error
select a, b, count(c) from test having count(*) > 5 group by a, b; -- error

