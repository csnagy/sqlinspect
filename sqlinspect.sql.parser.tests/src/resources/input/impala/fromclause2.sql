-- Test NULLs in on clause.
select * from src src1 
        left outer join src src2 on NULL 
        right outer join src src3 on (NULL) 
        full outer join src src3 on NULL 
        left semi join src src3 on (NULL)
        left anti join src src3 on (NULL)
        right semi join src src3 on (NULL) 
        right anti join src src3 on (NULL) 
        join src src3 on NULL 
        inner join src src3 on (NULL)
        where src2.bla = src3.bla 
        order by src1.key, src1.value, src2.key, src2.value, src3.key, src3.value;
-- Arbitrary exprs in on clause parse ok.
select * from src src1 join src src2 on ('a');
select * from src src1 join src src2 on (f(a, b));
select * from src src1  left outer join src src2 on (src1.key = src2.key and); -- error

-- Using clause requires SlotRef.
select * from src src1 join src src2 using (1); -- error
select * from src src1 join src src2 using (f(id)); -- error
-- Using clause required parenthesis.
select * from src src1 join src src2 using id; -- error

-- Cross joins do not accept on/using
select * from a cross join b on (a.id = b.id); -- error
select * from a cross join b using (id); -- error

