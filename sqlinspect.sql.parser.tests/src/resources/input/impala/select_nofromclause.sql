select 1 + 1, 'two', f(3), a + b;
select 1 + 1 'two' f(3) a + b; -- error
select a, 2 where a > 2; -- error
select a, 2 group by; -- error
select a, 2 order by 1; -- error
select a, 2 limit 1; -- error
select a, 2 order by 1 limit 1; -- error

