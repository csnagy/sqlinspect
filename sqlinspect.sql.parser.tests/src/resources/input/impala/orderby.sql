select int_col, string_col, bigint_col, count(*) from alltypes order by string_col, 15.7 * float_col, int_col + bigint_col;
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col asc, 15.7 * float_col desc, int_col + bigint_col asc;
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col asc, float_col desc, int_col + bigint_col asc nulls first;
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col asc, float_col desc, int_col + bigint_col desc nulls last;
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col asc, float_col desc, int_col + bigint_col nulls first;
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col asc, float_col desc nulls last, int_col + bigint_col nulls first;
select int_col from alltypes order by true, false, NULL;

select int_col, string_col, bigint_col, count(*) from alltypes order by by string_col asc desc; -- error
select int_col, string_col, bigint_col, count(*) from alltypes nulls first; -- error
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col nulls; -- error
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col nulls first asc; -- error
select int_col, string_col, bigint_col, count(*) from alltypes order by string_col nulls first last; -- error

