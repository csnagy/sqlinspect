-- Support unqualified and fully-qualified table names
CREATE TABLE Foo1 (i int);
CREATE TABLE Foo.Bar (i int);
CREATE TABLE IF NOT EXISTS Foo.Bar2 (i int);
CREATE TABLE Foo.Bar3 LIKE Foo.Bar1;
CREATE TABLE IF NOT EXISTS Bar4 LIKE Bar1;
CREATE EXTERNAL TABLE IF NOT EXISTS Bar5 LIKE Bar1;
CREATE EXTERNAL TABLE IF NOT EXISTS Bar6 LIKE Bar1 LOCATION '/a/b';
CREATE TABLE Foo7 LIKE Foo1 COMMENT 'sdafsdf';
CREATE TABLE Foo8 LIKE Foo7 COMMENT '';
CREATE TABLE Foo9 LIKE Foo1 STORED AS PARQUETFILE;
CREATE TABLE Foo10 LIKE Foo1 COMMENT 'tbl' STORED AS PARQUETFILE LOCATION '/a/b';
CREATE TABLE Foo11 LIKE Foo1 STORED AS TEXTFILE LOCATION '/a/b';
CREATE TABLE Foo12 LIKE PARQUET '/user/foo';

-- Table and column names starting with digits.
CREATE TABLE 01_Foo (01_i int, 02_j string);

-- Unpartitioned tables
CREATE TABLE Foo13 (i int, s string);
CREATE EXTERNAL TABLE Foo14 (i int, s string);
CREATE EXTERNAL TABLE Foo15 (i int, s string) LOCATION '/test-warehouse/';
CREATE TABLE Foo16 (i int, s string) COMMENT 'hello' LOCATION '/a/b/';
CREATE TABLE Foo17 (i int, s string) COMMENT 'hello' LOCATION '/a/b/' TBLPROPERTIES ('123'='1234');
-- No column definitions.
CREATE TABLE Foo18 COMMENT 'hello' LOCATION '/a/b/' TBLPROPERTIES ('123'='1234');

-- Partitioned tables
CREATE TABLE Foo19 (i int) PARTITIONED BY (j string);
CREATE TABLE Foo20 (i int) PARTITIONED BY (s string, d double);
CREATE TABLE Foo21 (i int, s string) PARTITIONED BY (s string, d double) COMMENT 'hello' LOCATION '/a/b/';
-- No column definitions.
CREATE TABLE Foo22 PARTITIONED BY (s string, d double) COMMENT 'hello' LOCATION '/a/b/';
CREATE TABLE Foo23 (i int) PARTITIONED BY (int); -- error
CREATE TABLE Foo24 (i int) PARTITIONED BY (); -- error
CREATE TABLE Foo25 (i int) PARTITIONED BY; -- error

-- Sort by clause
CREATE TABLE Foo26 (i int, j int) SORT BY ();
CREATE TABLE Foo27 (i int) SORT BY (i);
CREATE TABLE Foo28 (i int) SORT BY (j);
CREATE TABLE Foo29 (i int, j int) SORT BY (i,j);
CREATE EXTERNAL TABLE Foo30 (i int, s string) SORT BY (s) LOCATION '/test-warehouse/';
CREATE TABLE Foo31 (i int, s string) SORT BY (s) COMMENT 'hello' LOCATION '/a/b/' TBLPROPERTIES ('123'='1234');

-- SORT BY must be the first table option
CREATE TABLE Foo50 (i int, s string) COMMENT 'hello' SORT BY (s) LOCATION '/a/b/' TBLPROPERTIES ('123'='1234'); -- error
CREATE TABLE Foo51 (i int, s string) COMMENT 'hello' LOCATION '/a/b/' SORT BY (s) TBLPROPERTIES ('123'='1234'); -- error
CREATE TABLE Foo52 (i int, s string) COMMENT 'hello' LOCATION '/a/b/' TBLPROPERTIES ('123'='1234') SORT BY (s); -- error

-- Malformed SORT BY clauses
CREATE TABLE Foo60 (i int, j int) SORT BY; -- error
CREATE TABLE Foo61 (i int, j int) SORT BY (i,); -- error
CREATE TABLE Foo62 (i int, j int) SORT BY (int); -- error

-- Create table like other table with sort columns
CREATE TABLE Foo71 SORT BY(bar) LIKE Baz STORED AS TEXTFILE LOCATION '/a/b';
CREATE TABLE SORT BY(bar) Foo72 LIKE Baz STORED AS TEXTFILE LOCATION '/a/b'; -- error
-- SORT BY must be the first table option
CREATE TABLE Foo73 LIKE Baz STORED AS TEXTFILE LOCATION '/a/b' SORT BY(bar); -- error

-- CTAS with sort columns
CREATE TABLE Foo81 SORT BY(bar) AS SELECT * FROM BAR;
CREATE TABLE Foo82 AS SELECT * FROM BAR SORT BY(bar); -- error

-- Create table like file with sort columns
CREATE TABLE Foo83 LIKE PARQUET '/user/foo' SORT BY (id);
CREATE TABLE Foo84 SORT BY (id) LIKE PARQUET '/user/foo'; -- error

-- Column comments
CREATE TABLE Foo91 (i int COMMENT 'hello', s string);
CREATE TABLE Foo92 (i int COMMENT 'hello', s string COMMENT 'hi');
CREATE TABLE T1 (i int COMMENT 'hi') PARTITIONED BY (j int COMMENT 'bye');

