select a, b, c from t where i between x and y;
select a, b, c from t where i not between x and y;
select a, b, c from t where true not between false and NULL;
select a, b, c from t where 'abc' between 'a' like 'a' and 'b' like 'b';
-- Additional conditions before and after between predicate.
select a, b, c from t where true and false and i between x and y;
select a, b, c from t where i between x and y and true and false;
select a, b, c from t where i between x and (y and true) and false;
select a, b, c from t where i between x and (y and (true and false));
-- Chaining/nesting of between predicates.
select a, b, c from t 
	where true between false and true and 'b' between 'a' and 'c';
-- true between ('b' between 'a' and 'b') and ('bb' between 'aa' and 'cc)
select a, b, c from t 
	where true between 'b' between 'a' and 'c' and 'bb' between 'aa' and 'cc';
-- Missing condition expr.
select a, b, c from t where between 5 and 10; -- error
-- Missing lower bound.
select a, b, c from t where i between and 10; -- error
-- Missing upper bound.
select a, b, c from t where i between 5 and; -- error
-- Missing exprs after between.
select a, b, c from t where i between; -- error
-- AND has a higher precedence than OR.
select a, b, c from t where true between 5 or 10 and 20; -- error

