select a from tbl;
select a, b, c, d from tbl;
select true, false, NULL from tbl;
select all a, b, c from tbl;
a from tbl; -- error
select a b c from tbl; -- error
select all from tbl; -- error

