with t as (select 1 as a) select a from t;
with t(x) as (select 1 as a) select x from t;
with t as (select c from tab) select * from t;
with t(x, y) as (select * from tab) select * from t;
with t as (values(1, 2, 3), (4, 5, 6)) select * from t;
with t(x, y, z) as (values(1, 2, 3), (4, 5, 6)) select * from t;
with t1 as (select 1 as a), t2 as (select 2 as a) select a from t1;
with t1 as (select c from tab), t2 as (select c from tab)
        select c from t2;
with t1(x) as (select c from tab), t2(x) as (select c from tab)
        select x from t2;
-- With clause and union statement.
with t1 as (select 1 as a), t2 as (select 2 as a)
        select a from t1 union all select a from t2;
-- With clause and join.
with t1 as (select 1 as a), t2 as (select 2 as a)
        select a from t1 inner join t2 on t1.a = t2.a;
-- With clause in inline view.
select * from (with t as (select 1 as a) select * from t) as a;
select * from (with t(x) as (select 1 as a) select * from t) as a;
-- With clause in query statement of insert statement.
insert into x with t as (select * from tab) select * from t;
insert into x with t(x, y) as (select * from tab) select * from t;
insert into x with t as (values(1, 2, 3)) select * from t;
insert into x with t(x, y) as (values(1, 2, 3)) select * from t;
-- With clause before insert statement.
with t as (select 1) insert into x select * from t;
with t(x) as (select 1) insert into x select * from t;
-- With clause in query statement of upsert statement.
upsert into x with t as (select * from tab) select * from t;
upsert into x with t(x, y) as (select * from tab) select * from t;
upsert into x with t as (values(1, 2, 3)) select * from t;
upsert into x with t(x, y) as (values(1, 2, 3)) select * from t;
-- With clause before upsert statement.
with t as (select 1) upsert into x select * from t;
with t(x) as (select 1) upsert into x select * from t;

-- Test quoted identifier or string literal as table alias.
with `t1` as (select 1 a), 't2' as (select 2 a), "t3" as (select 3 a)
        select a from t1 union all select a from t2 union all select a from t3;
-- Multiple with clauses. Operands must be in parenthesis to
-- have their own with clause.
with t as (select 1)
        (with t as (select 2) select * from t) union all
        (with t as (select 3) select * from t);
with t as (select 1)
        (with t as (select 2) select * from t) union all
        (with t as (select 3) select * from t) order by 1 limit 1;
-- Multiple with clauses. One before the insert and one inside the query statement.
with t as (select 1) insert into x with t as (select 2) select * from t;
with t(c1) as (select 1)
        insert into x with t(c2) as (select 2) select * from t;
-- Multiple with clauses. One before the upsert and one inside the query statement.
with t as (select 1) upsert into x with t as (select 2) select * from t;
with t(c1) as (select 1)
        upsert into x with t(c2) as (select 2) select * from t;

-- Empty with clause.
with t as () select 1; -- error
with t(x) as () select 1; -- error
-- No labels inside parenthesis.
with t() as (select 1 as a) select a from t; -- error
-- Missing select, union or insert statement after with clause.
select * from (with t as (select 1 as a)) as a; -- error
with t as (select 1); -- error
-- Missing parenthesis around with query statement.
with t as select 1 as a select a from t; -- error
with t as select 1 as a union all select a from t; -- error
with t1 as (select 1 as a), t2 as select 2 as a select a from t; -- error
with t as select 1 as a select a from t; -- error
-- Missing parenthesis around column labels.
with t c1 as (select 1 as a) select c1 from t; -- error
-- Insert in with clause is not valid.
with t as (insert into x select * from tab) select * from t; -- error
with t(c1) as (insert into x select * from tab) select * from t; -- error
-- Upsert in with clause is not valid.
with t as (upsert into x select * from tab) select * from t; -- error
with t(c1) as (upsert into x select * from tab) select * from t; -- error
-- Union operands need to be parenthesized to have their own with clause.
select * from t union all with t as (select 2) select * from t; -- error


