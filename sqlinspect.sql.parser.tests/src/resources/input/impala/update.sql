update t set x = 3 where a < b;
update t set x = (3 + length("hallo")) where a < 'adasas';
update t set x = 3;
update t set x=3, x=4 from a.b t where b = 10;
update t; -- error
update t set x < 3; -- error
update t set x; -- error
update t set 4 = x; -- error
update from t set x = 3; -- error
update t where x = 4; -- error
update t a set a = 10  where x = 4; -- error
update t a from t b where set a = 10 x = 4; -- error
update (select * from functional_kudu.testtbl) a set name = '10'; -- error
