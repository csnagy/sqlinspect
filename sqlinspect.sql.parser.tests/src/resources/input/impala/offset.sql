select a from test order by a limit 10 offset 5;
select a from test order by a limit 10 offset 0;
select a from test order by a limit 10 offset 0 + 5 / 2;
select a from test order by a asc limit 10 offset 5;
select a from test order by a offset 5;
select a from test limit 10 offset 5; -- Parses OK, doesn't analyze
select a from test offset 5; -- Parses OK, doesn't analyze
select a from (select a from test offset 5) A; -- Doesn't analyze
select a from (select a from test order by a offset 5) A;
select a from test order by a limit offset; -- error
select a from test order by a limit offset 5; -- error

