-- Test chained timestamp arithmetic exprs.
select a + interval b years + interval c months + interval d days;
select a - interval b years - interval c months - interval d days;
select a + interval b years - interval c months + interval d days;
-- Starting with interval.
select interval b years + a + interval c months + interval d days;
select interval b years + a - interval c months - interval d days;
select interval b years + a - interval c months + interval d days;

-- Too many arguments.
select date_sub(a, c, interval b year); -- error
select date_sub(a, interval b year, c); -- error
