CREATE VIEW Bar1 AS SELECT a, b, c from t;
CREATE VIEW Bar2 COMMENT 'test' AS SELECT a, b, c from t;
CREATE VIEW Bar3 (x, y, z) AS SELECT a, b, c from t;
CREATE VIEW Bar4 (x, y COMMENT 'foo', z) AS SELECT a, b, c from t;
CREATE VIEW Bar5 (x, y, z) COMMENT 'test' AS SELECT a, b, c from t;
CREATE VIEW IF NOT EXISTS Bar6 AS SELECT a, b, c from t;

CREATE VIEW Foo.Bar11 AS SELECT a, b, c from t;
CREATE VIEW Foo.Bar12 COMMENT 'test' AS SELECT a, b, c from t;
CREATE VIEW Foo.Bar13 (x, y, z) AS SELECT a, b, c from t;
CREATE VIEW Foo.Bar14 (x, y, z COMMENT 'foo') AS SELECT a, b, c from t;
CREATE VIEW Foo.Bar15 (x, y, z) COMMENT 'test' AS SELECT a, b, c from t;
CREATE VIEW IF NOT EXISTS Foo.Bar16 AS SELECT a, b, c from t;

-- Test all valid query statements as view definitions.
CREATE VIEW Bar21 AS SELECT 1, 2, 3;
CREATE VIEW Bar22 AS VALUES(1, 2, 3);
CREATE VIEW Bar23 AS SELECT 1, 2, 3 UNION ALL select 4, 5, 6;
CREATE VIEW Bar24 AS WITH t AS (SELECT 1, 2, 3) SELECT * FROM t;

-- Mismatched number of columns in column definition and view definition parses ok.
CREATE VIEW Bar25 (x, y) AS SELECT 1, 2, 3;

-- No view name.
CREATE VIEW AS SELECT c FROM t; -- error
-- Missing AS keyword
CREATE VIEW Bar31 SELECT c FROM t; -- error
-- Empty column definition not allowed.
CREATE VIEW Foo.Bar32 () AS SELECT c FROM t; -- error
-- Column definitions cannot include types.
CREATE VIEW Foo.Bar33 (x int) AS SELECT c FROM t; -- error
CREATE VIEW Foo.Bar34 (x int COMMENT 'x') AS SELECT c FROM t; -- error
-- A type does not parse as an identifier.
CREATE VIEW Foo.Bar35 (int COMMENT 'x') AS SELECT c FROM t; -- error
-- Missing view definition.
CREATE VIEW Foo.Bar36 (x) AS; -- error
-- Invalid view definitions. A view definition must be a query statement.
CREATE VIEW Foo.Bar37 (x) AS INSERT INTO t select * from t; -- error
CREATE VIEW Foo.Bar38 (x) AS UPSERT INTO t select * from t; -- error
CREATE VIEW Foo.Bar39 (x) AS CREATE TABLE Wrong (i int); -- error
CREATE VIEW Foo.Bar40 (x) AS ALTER TABLE Foo COLUMNS (i int, s string); -- error
CREATE VIEW Foo.Bar41 (x) AS CREATE VIEW Foo.Bar AS SELECT 1; -- error
CREATE VIEW Foo.Bar42 (x) AS ALTER VIEW Foo.Bar AS SELECT 1; -- error

