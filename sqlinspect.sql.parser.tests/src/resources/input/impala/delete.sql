delete from t;
delete a from t a;
delete a from t a join b on a.id = b.id where true;
delete a from t a join b where true;
delete t from t;
delete t from t where a < b;
delete a from t a where a < b;
delete FROM t where a < b;
delete t where a < b;
delete t;
delete t join f on t.id = f.id; -- error

