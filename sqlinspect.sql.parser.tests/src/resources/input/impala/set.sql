SET foo='bar';
SET foo=\"bar\";
SET foo=bar;
SET foo = bar;
SET foo=1;
SET foo=true;
SET foo=false;
SET foo=1.2;
SET foo=null;
SET foo=10g;
SET `foo`=0;
SET foo='';
SET;

SET foo; -- error
SET foo=; -- error
SET foo=1+2; -- error
