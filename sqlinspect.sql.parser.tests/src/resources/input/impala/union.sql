-- Single union test.
select a from test union select a from test;
select a from test union all select a from test;
select a from test union distinct select a from test;
-- Chained union test.
select a from test union select a from test
        union select a from test union select a from test;
select a from test union all select a from test
        union all select a from test union all select a from test;
select a from test union distinct select a from test
        union distinct select a from test union distinct select a from test ;
-- Mixed union with all and distinct.
select a from test union select a from test 
        union all select a from test union distinct select a from test;
-- No from clause.
select sin() union select cos();
select sin() union all select cos();
select sin() union distinct select cos();

-- All select blocks in parenthesis.
(select a from test) union (select a from test)
        union (select a from test) union (select a from test);
-- Union with order by,
(select a from test) union (select a from test) 
        union (select a from test) union (select a from test) order by a;
(select a from test) union (select a from test) 
        union (select a from test) union (select a from test) order by a nulls first;
-- Union with limit.
(select a from test) union (select a from test) 
        union (select a from test) union (select a from test) limit 10;
-- Union with order by, offset and limit.
(select a from test) union (select a from test)
        union (select a from test) union (select a from test) order by a limit 10;
(select a from test) union (select a from test)
        union (select a from test) union (select a from test) order by a 
        nulls first limit 10;
(select a from test) union (select a from test)
        union (select a from test) union (select a from test) order by a 
        nulls first offset 10;
select a from test union (select a from test) 
        union (select a from test) union (select a from test) offset 10; -- error
-- Union with some select blocks in parenthesis, and others not.
(select a from test) union select a from test 
        union (select a from test) union select a from test;
select a from test union (select a from test) 
        union select a from test union (select a from test);
-- Union with order by, offset and limit binding to last select.
(select a from test) union (select a from test) 
        union select a from test union select a from test order by a limit 10;
(select a from test) union (select a from test) 
        union select a from test union select a from test order by a offset 10;
(select a from test) union (select a from test) 
        union select a from test union select a from test order by a;

-- Union with order by and limit.
-- Last select with order by and limit is in parenthesis.
select a from test union (select a from test)
        union select a from test union (select a from test order by a limit 10)
        order by a limit 1;
select a from test union (select a from test)
        union select a from test union (select a from test order by a offset 10)
        order by a limit 1;
select a from test union (select a from test)
        union select a from test union (select a from test order by a)
        order by a limit 1;
-- Union with order by, offset in first operand.
select a from test order by a union select a from test;
select a from test order by a offset 5 union select a from test;
select a from test offset 5 union select a from test;
-- Union with order by and limit.
-- Last select with order by and limit is not in parenthesis.
select a from test union select a from test
        union select a from test union select a from test order by a limit 10
        order by a limit 1;

-- Nested unions with order by and limit.
select a union
        ((select b) union (select c) order by 1 limit 1);
select a union
        ((select b) union
          ((select c) union (select d)
           order by 1 limit 1)
         order by 1 limit 1);

-- Union in insert query.
insert into table t select a from test union select a from test;
insert into table t select a from test union select a from test
        union select a from test union select a from test;
insert overwrite table t select a from test union select a from test;
insert overwrite table t select a from test union select a from test
        union select a from test union select a from test;

-- Union in upsert query.
upsert into table t select a from test union select a from test;
upsert into table t select a from test union select a from test
        union select a from test union select a from test;

-- No complete select statement on lhs.
a from test union select a from test; -- error
-- No complete select statement on rhs.
select a from test union a from test; -- error
-- Union cannot be a column or table since it's a keyword.
select union from test; -- error
select a from union; -- error


