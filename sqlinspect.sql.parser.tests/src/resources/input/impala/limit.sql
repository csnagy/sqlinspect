select a, b, c from test inner join test2 using(a) limit 10;
select a, b, c from test inner join test2 using(a) limit 10 + 10;
-- The following will parse because limit takes an expr, though they will fail in
-- analysis
select a, b, c from test inner join test2 using(a) limit 'a';
select a, b, c from test inner join test2 using(a) limit a;
select a, b, c from test inner join test2 using(a) limit true;
select a, b, c from test inner join test2 using(a) limit false;
select a, b, c from test inner join test2 using(a) limit NULL;
-- Not an expr, will not parse
select a, b, c from test inner join test2 using(a) limit 10 where a > 10; -- error

