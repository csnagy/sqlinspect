select f1(5), f2('five'), f3(5.0, i + 5) from t;
select f1(true), f2(true and false), f3(null) from t;
select f1(*);
select f1(distinct col);
select f1(distinct col, col2);
select decode(col, col2, col3);
select f( from t; -- error
select f(5.0 5.0) from t; -- error

