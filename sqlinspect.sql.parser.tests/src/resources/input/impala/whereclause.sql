select a, b from t where a > 15;
select a, b from t where true;
select a, b from t where NULL;
-- Non-predicate exprs that return boolean.
select a, b from t where case a when b then true else false end;
select a, b from t where if (a > b, true, false);
select a, b from t where bool_col;
-- Arbitrary non-predicate exprs parse ok but are semantically incorrect.
select a, b from t where 10.5;
select a, b from t where trim('abc');
select a, b from t where s + 20;
select a, b from t where a > 15 from test; -- error
select a, b where a > 15; -- error
select where a, b from t; -- error

