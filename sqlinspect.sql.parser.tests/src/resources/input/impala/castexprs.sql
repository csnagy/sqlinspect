select cast(a + 5.0 as string) from t;
select cast(NULL as string) from t;
select cast(a + 5.0 as badtype) from t; -- error
select cast(a + 5.0, string) from t; -- error

