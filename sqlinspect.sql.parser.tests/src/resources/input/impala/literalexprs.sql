select -1 from t where -1;
select - 1 from t where - 1;
select a - - 1 from t where a - - 1;
select a - - - 1 from t where a - - - 1;

-- positive integer literal
select +1 from t where +1;
select + 1 from t where + 1;
select a + + 1 from t where a + + 1;
select a + + + 1 from t where a + + + 1;

-- float literals
select +1.0 from t where +1.0;
select +-1.0 from t where +-1.0;
select +1.-0 from t where +1.-0;

-- test scientific notation
select 8e6 from t where 8e6;
select +8e6 from t where +8e6;
select 8e+6 from t where 8e+6;
select -8e6 from t where -8e6;
select 8e-6 from t where 8e-6;
select -8e-6 from t where -8e-6;
-- with a decimal point
select 4.5e2 from t where 4.5e2;
select +4.5e2 from t where +4.5e2;
select 4.5e+2 from t where 4.5e+2;
select -4.5e2 from t where -4.5e2;
select 4.5e-2 from t where 4.5e-2;
select -4.5e-2 from t where -4.5e-2;
-- with a decimal point but without a number before the decimal
select .7e9 from t where .7e9;
select +.7e9 from t where +.7e9;
select .7e+9 from t where .7e+9;
select -.7e9 from t where -.7e9;
select .7e-9 from t where .7e-9;
select -.7e-9 from t where -.7e-9;

-- mixed signs
select -+-1 from t where -+-1;
select - +- 1 from t where - +- 1;
select 1 + -+ 1 from t where 1 + -+ 1;

-- Boolean literals
select true from t where true;
select false from t where false;

-- Null literal
select NULL from t where NULL;

-- -- is parsed as a comment starter
select --1; -- error

-- Postfix operators must be binary
select 1- from t; -- error
select 1 + from t; -- error

-- Only - and + can be unary operators
select /1 from t; -- error
select *1 from t; -- error
select &1 from t; -- error
select =1 from t; -- error

-- test string literals with and without quotes in the literal
select 'five', 5, 5.0, i + 5 from t;
select "\"five\"" from t;
select "'five'" from t;
select "'five" from t;

-- missing quotes
--select \'5 from t; -- error
--select \"5 from t; -- error
--select '5 from t; -- error
--select `5 from t; -- error
--select \"\"five\"\" from t\n; -- error
select 5.0.5 from t; -- error

select '\0' from t;
select "\0" from t;
select '\\' from t;
select "\\" from t;
select '\b' from t;
select "\b" from t;
select '\n' from t;
select "\n" from t;
select '\r' from t;
select "\r" from t;
select '\t' from t;
select "\t" from t;
select '\Z' from t;
select "\Z" from t;
select '\%' from t;
select "\%" from t;
select '\\%' from t;
select "\\%" from t;
select '\_' from t;
select "\_" from t;
select '\\_' from t;
select "\\_0" from t;
select '\0\\\b\n\r\t\Z\%\\%\_\\_' from t;
select "\0\\\b\n\r\t\Z\%\\%\_\\_" from t;
select 'a\0b\\c\bd\ne\rf\tg\Zh\%i\\%j\_k\\_l' from t;
select "a\0b\\c\bd\ne\rf\tg\Zh\%i\\%j\_k\\_l" from t;
select '\a\b\c\d\1\2\3\$\&\*' from t;
select "\a\b\c\d\1\2\3\$\&\*" from t;

select '@';
