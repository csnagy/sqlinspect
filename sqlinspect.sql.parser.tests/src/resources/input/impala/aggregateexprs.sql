select count(*), count(a), count(distinct a, b) from t;
select count(NULL), count(TRUE), count(FALSE), count(distinct TRUE, FALSE, NULL) from t;
select count(all *) from t;
select count(all 1) from t;
select min(a), min(distinct a) from t;
select max(a), max(distinct a) from t;
select sum(a), sum(distinct a) from t;
select avg(a), avg(distinct a) from t;
select distinct a, b, c from t;
select distinctpc(a), distinctpc(distinct a) from t;
select distinctpcsa(a), distinctpcsa(distinct a) from t;
select ndv(a), ndv(distinct a) from t;
select group_concat(a) from t;
select group_concat(a, ', ') from t;
select group_concat(a, ', ', c) from t;

