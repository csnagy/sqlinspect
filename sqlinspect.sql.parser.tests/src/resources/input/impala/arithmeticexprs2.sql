select (i + 5)(1 - i) from t; -- error
select %a from t; -- error
select *a from t; -- error
select /a from t; -- error
select &a from t; -- error
select |a from t; -- error
select ^a from t; -- error
select a ~ a from t; -- error

