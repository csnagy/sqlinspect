select sum(v) over (partition by a, 2*b order by 3*c rows between
	2+2 preceding and 2-2 following) from t;
select sum(v) over (order by 3*c rows between
	unbounded preceding and unbounded following) from t;
select sum(v) over (partition by a, 2*b) from t;
select sum(v) over (partition by a, 2*b order by 3*c range between 
	unbounded preceding and unbounded following) from t;
select sum(v) over (order by 3*c range between
	2 following and 4 following) from t;
select sum(v) over (partition by a, 2*b) from t;
select 2 * x, sum(v) over (partition by a, 2*b order by 3*c rows between
	2+2 preceding and 2-2 following), rank() over (), y from t;
-- not a function call
select v over (partition by a, 2*b order by 3*c rows between 2
	preceding and 2 following) from t; -- error
-- something missing
select sum(v) over (partition a, 2*b order by 3*c rows between
	unbounded preceding and current row) from t; -- error
select sum(v) over (partition by a, 2*b order 3*c rows between 2
	preceding and 2 following) from t; -- error
select sum(v) over (partition by a, 2*b order by 3*c rows 2
	preceding and 2 following) from t; -- error
select sum(v) over (partition by a, 2*b) from t;
-- Special case for DECODE, which results in a parse error when used in
-- an analytic context. Note that "ecode() over ()" would parse fine since
-- that is handled by the standard function call lookup.
select decode(1, 2, 3) over () from t; -- error

