select * from tbl;
select tbl.* from tbl;
select db.tbl.* from tbl;
select db.tbl.struct_col.* from tbl;
select * + 5 from tbl; -- error
select (*) from tbl; -- error
select *.id from tbl; -- error
select * from tbl.*; -- error
select * from tbl where * = 5; -- error
select * from tbl where f(*) = 5;
select * from tbl where tbl.* = 5; -- error
select * from tbl where f(tbl.*) = 5; -- error
