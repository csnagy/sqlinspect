-- Table alias most come before TABLESAMPLE.
select * from t tablesample (10) a; -- error
-- Hints must come after TABLESAMPLE.
select * from t [schedule_remote] tablesample (10); -- error
select * from t /* +schedule_remote */ tablesample (10); -- error
-- Missing SYSTEM.
select * from t tablesample (10); -- error
-- Missing parenthesis.
select * from t tablesample system 10; -- error
-- Percent must be int literal.
select * from t tablesample system (10 + 10; -- error
-- Missing random seed.
select * from t tablesample system (10) repeatable; -- error
-- Random seed must be an int literal.
select * from t tablesample system (10) repeatable (10 + 10); -- error
-- Negative precent.
select * from t tablesample system (-10); -- error
-- Negative random seed.
select * from t tablesample system (10) repeatable(-10); -- error

