select a, b, c from t where i in (x, y, z);
select a, b, c from t where i not in (x, y, z);
-- Test NULL and boolean literals.
select a, b, c from t where NULL in (NULL, NULL, NULL);
select a, b, c from t where true in (true, false, true);
select a, b, c from t where NULL not in (NULL, NULL, NULL);
select a, b, c from t where true not in (true, false, true);
-- Missing condition expr.
select a, b, c from t where in (x, y, z); -- error
-- Missing parentheses around in list.
select a, b, c from t where i in x, y, z; -- error
select a, b, c from t where i in (x, y, z; -- error
select a, b, c from t where i in x, y, z); -- error
-- Missing in list.
select a, b, c from t where i in; -- error
select a, b, c from t where i in ( ); -- error

