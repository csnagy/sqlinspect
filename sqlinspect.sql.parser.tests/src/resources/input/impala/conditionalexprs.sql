select if(TRUE, TRUE, FALSE) from t;
select if(NULL, NULL, NULL) from t;
select c1, c2, if(TRUE, TRUE, FALSE) from t;
select if(1 = 2, c1, c2) from t;
select if(1 = 2, c1, c2);
select if(); -- error

