SET @name = 43;
SET @total_tax = (SELECT SUM(tax) FROM taxable_transactions);

SET GLOBAL max_connections = 1000;
SET @@global.max_connections = 1000;

SET SESSION sql_mode = 'TRADITIONAL';
SET @@session.sql_mode = 'TRADITIONAL';
SET @@sql_mode = 'TRADITIONAL';

SET @x = 1, SESSION sql_mode = '';

SET GLOBAL sort_buffer_size = 1000000, SESSION sort_buffer_size = 1000000;
SET @@global.sort_buffer_size = 1000000, @@local.sort_buffer_size = 1000000;
SET GLOBAL max_connections = 1000, sort_buffer_size = 1000000;

SET @@session.max_join_size=DEFAULT;
SET @@session.max_join_size=@@global.max_join_size;
