SELECT
 MAX(bytes)
FROM
 tracks;
SELECT
 trackid,
 name,
 bytes
FROM
 tracks
WHERE
 bytes = (SELECT max(bytes) FROM tracks);
SELECT
 albumid,
 MAX(bytes)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 albumid,
 max(bytes)
FROM
 tracks
GROUP BY
 albumid
HAVING MAX(bytes) > 6000000;
