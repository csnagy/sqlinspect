CREATE TABLE contacts (
 first_name text NOT NULL,
 last_name text NOT NULL,
 email text NOT NULL
);
	
CREATE UNIQUE INDEX idx_contacts_email ON contacts (email);
INSERT INTO contacts (first_name, last_name, email)
VALUES
 (
 'John',
 'Doe',
 'john.doe@sqlitetutorial.net'
 );
INSERT INTO contacts (first_name, last_name, email)
VALUES
 (
 'Johny',
 'Doe',
 'john.doe@sqlitetutorial.net'
 );
INSERT INTO contacts (first_name, last_name, email)
VALUES
 (
 'David',
 'Brown',
 'david.brown@sqlitetutorial.net'
 ),
 (
 'Lisa',
 'Smith',
 'lisa.smith@sqlitetutorial.net'
 );
SELECT
 first_name,
 last_name,
 email
FROM
 contacts
WHERE
 email = 'lisa.smith@sqlitetutorial.net';
EXPLAIN QUERY PLAN 
SELECT
 first_name,
 last_name,
 email
FROM
 contacts
WHERE
 email = 'lisa.smith@sqlitetutorial.net';
CREATE INDEX idx_contacts_name 
ON contacts (first_name, last_name);

DROP INDEX idx_contacts_name;
