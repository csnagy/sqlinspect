CREATE TABLE devices (
 name text NOT NULL,
 model text NOT NULL,
 serial integer NOT NULL UNIQUE
);
 
INSERT INTO devices (name, model, serial)
VALUES
 (
 'HP ZBook 17 G3 Mobile Workstation',
 'ZBook',
 'SN-2015'
 );
ALTER TABLE devices
RENAME TO equipment;
SELECT
 name,
 model,
 serial
FROM
 equipment;
ALTER TABLE equipment 
ADD COLUMN location text;
