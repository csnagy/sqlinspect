PRAGMA index_info('idx52');

PRAGMA auto_vacuum;
PRAGMA db.auto_vacuum;

PRAGMA auto_vacuum = 0;
PRAGMA db.auto_vacuum = 0;

PRAGMA case_sensitive_like = true;
PRAGMA case_sensitive_like = false;

PRAGMA case_sensitive_like = on;
PRAGMA case_sensitive_like = off;

PRAGMA case_sensitive_like = yes;
PRAGMA case_sensitive_like = no;
