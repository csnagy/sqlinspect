SELECT
 trackid,
 name
FROM
 tracks
WHERE
 name GLOB 'Man*';
SELECT
 trackid,
 name
FROM
 tracks
WHERE
 name GLOB '*Man';
SELECT
 trackid,
 name
FROM
 tracks
WHERE
 name GLOB '?ere*';
SELECT
 trackid,
 name
FROM
 tracks
WHERE
 name GLOB '*[1-9]*';
SELECT
 trackid,
 name
FROM
 tracks
WHERE
 name GLOB '*[^1-9]*';
SELECT
 trackid,
 name
FROM
 tracks
WHERE
 name GLOB '*[1-9]';