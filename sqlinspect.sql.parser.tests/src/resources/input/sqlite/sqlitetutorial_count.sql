SELECT
 count(*)
FROM
 tracks;
SELECT
 count(*)
FROM
 tracks
WHERE
 albumid = 10;
SELECT
 albumid,
 count(*)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 albumid,
 count(*)
FROM
 tracks
GROUP BY
 albumid
HAVING count(*) > 25;
SELECT
 tracks.albumid,
 name,
 count(*)
FROM
 tracks
INNER JOIN albums on albums.albumid = tracks.albumid
GROUP BY
 tracks.albumid
HAVING count(*) > 25
ORDER BY COUNT(*) desc;
SELECT
 employeeid,
 lastname,
 firstname,
 title
FROM
 employees;
SELECT
 COUNT(title)
FROM
 employees;
SELECT
 COUNT(DISTINCT title)
FROM
 employees;