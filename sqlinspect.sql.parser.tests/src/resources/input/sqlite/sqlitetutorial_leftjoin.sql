SELECT
 artists.ArtistId,
 albumId
FROM
 artists
LEFT JOIN albums ON albums.artistid = artists.artistid
ORDER BY
 albumid;
SELECT
 artists.ArtistId,
 albumId
FROM
 artists
LEFT JOIN albums ON albums.artistid = artists.artistid
WHERE
 albumid IS NULL;
