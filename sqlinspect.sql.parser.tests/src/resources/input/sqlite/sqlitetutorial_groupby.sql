SELECT
 albumid,
 COUNT(trackid)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 albumid,
 COUNT(trackid)
FROM
 tracks
GROUP BY
 albumid
ORDER BY COUNT(trackid) DESC;
SELECT
 tracks.albumid,
 title,
 COUNT(trackid)
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid
GROUP BY
 tracks.albumid;
SELECT
 tracks.albumid,
 title,
 COUNT(trackid)
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid
GROUP BY
 tracks.albumid
HAVING COUNT(trackid) > 15;
SELECT
 albumid,
 sum(milliseconds) length,
 sum(bytes) size
FROM
 tracks
GROUP BY
 albumid;
SELECT
 tracks.albumid,
 title,
 min(milliseconds),
 max(milliseconds),
 round(avg(milliseconds),2)
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid
GROUP BY
 tracks.albumid;
SELECT
 mediatypeid,
 genreid,
 count(trackid)
FROM
 tracks
GROUP BY
 mediatypeid,
 genreid;
