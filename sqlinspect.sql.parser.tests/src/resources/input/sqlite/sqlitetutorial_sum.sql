SELECT
 SUM(milliseconds)
FROM
 tracks;
SELECT
 albumid,
 avg(milliseconds)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 tracks.albumid,
 title,
 SUM(milliseconds)
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid
GROUP BY
 tracks.albumid;
SELECT
 tracks.albumid,
 title,
 SUM(milliseconds)
FROM
 tracks
INNER JOIN albums ON albums.AlbumId = tracks.albumid
GROUP BY
 tracks.albumid
HAVING
 SUM(milliseconds) > 1000000;
