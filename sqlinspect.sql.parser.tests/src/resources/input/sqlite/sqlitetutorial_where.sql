SELECT
 name,
 milliseconds,
  bytes,
 albumid
FROM
 tracks
WHERE
 albumid = 1;
SELECT
 name,
 milliseconds,
 bytes,
 albumid
FROM
 tracks
WHERE
 albumid = 1
AND milliseconds > 250000;
SELECT
 name,
 albumid,
 composer
FROM
 tracks
WHERE
 composer LIKE '%Smith%'
ORDER BY
 albumid;
SELECT
 name,
 albumid,
 mediatypeid
FROM
 tracks
WHERE
 mediatypeid IN (2, 3);
