SELECT
 trackid,
 name,
 title
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid;
SELECT
 trackid,
 name,
 tracks.AlbumId as album_id_tracks,
  albums.AlbumId as album_id_albums,
 title
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid;
SELECT
 trackid,
 tracks.name AS Track,
 albums.title AS Album,
 artists.name AS Artist
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid
INNER JOIN artists ON artists.artistid = albums.artistid;
SELECT
 trackid,
 tracks.name AS Track,
 albums.title AS Album,
 artists.name AS Artist
FROM
 tracks
INNER JOIN albums ON albums.albumid = tracks.albumid
INNER JOIN artists ON artists.artistid = albums.artistid
WHERE
 artists.artistid = 10;
