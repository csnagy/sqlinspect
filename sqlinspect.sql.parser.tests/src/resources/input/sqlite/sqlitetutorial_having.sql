SELECT
 albumid,
 COUNT(trackid)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 albumid,
 COUNT(trackid)
FROM
 tracks
GROUP BY
 albumid
HAVING albumid = 1;
SELECT
 albumid,
 COUNT(trackid)
FROM
 tracks
GROUP BY
 albumid
HAVING count(albumid) BETWEEN 18 AND 20
ORDER BY albumid;
SELECT
 tracks.albumid,
 title,
 sum(Milliseconds) AS length
FROM
 tracks
INNER JOIN albums ON albums.AlbumId = tracks.AlbumId
GROUP BY
 tracks.albumid
HAVING
 length > 60000000;
