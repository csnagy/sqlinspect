INSERT INTO artists (name)
VALUES
 ('Bud Powell');
INSERT INTO artists (name)
VALUES
 ("Buddy Rich"),
 ("Candido"),
 ("Charlie Byrd");
INSERT INTO artists DEFAULT VALUES;
CREATE TABLE artists_backup(
 artistid INTEGER PRIMARY KEY AUTOINCREMENT,
 name NVARCHAR
);
INSERT INTO artists_backup SELECT
 artistid,
 name
FROM
 artists;
