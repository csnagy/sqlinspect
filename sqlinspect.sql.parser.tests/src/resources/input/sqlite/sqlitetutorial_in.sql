SELECT
 trackid,
 name,
 mediatypeid
FROM
 tracks
WHERE
 mediatypeid IN (1, 2)
ORDER BY
 name ASC;
SELECT
 trackid,
 name,
 MediaTypeId
FROM
 tracks
WHERE
 mediatypeid = 1 OR mediatypeid = 2
ORDER BY
 name ASC;
SELECT
 albumid
FROM
 albums
WHERE
 artistid = 12;
SELECT
 trackid,
 name,
 albumid
FROM
 tracks
WHERE
 albumid IN (
 SELECT
 albumid
 FROM
 albums
 WHERE
 artistid = 12
 );
SELECT
 trackid,
 name,
 genreid
FROM
 tracks
WHERE
 genreid NOT IN (1, 2,3);