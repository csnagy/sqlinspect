CREATE TABLE countries (
 country_id INTEGER PRIMARY KEY,
 name text NOT NULL
);
CREATE TABLE languages (
 language_id integer,
 name text NOT NULL,
 PRIMARY KEY (language_id)
);
CREATE TABLE country_languages (
 country_id integer NOT NULL,
 language_id integer NOT NULL,
 PRIMARY KEY (country_id, language_id),
 FOREIGN KEY (country_id) REFERENCES countries (country_id) 
            ON DELETE CASCADE ON UPDATE NO ACTION,
 FOREIGN KEY (language_id) REFERENCES languages (language_id) 
            ON DELETE CASCADE ON UPDATE NO ACTION
);
CREATE TABLE cities (
 id INTEGER NOT NULL,
 name text NOT NULL
);
 
INSERT INTO cities (id, name)
VALUES
 (1, 'San Jose');
