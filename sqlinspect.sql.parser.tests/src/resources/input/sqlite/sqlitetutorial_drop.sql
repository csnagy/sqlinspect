CREATE TABLE IF NOT EXISTS people (
 person_id integer PRIMARY KEY,
 first_name text,
 last_name text,
 address_id integer,
 FOREIGN KEY (address_id) REFERENCES addresses (address_id)
);
 
CREATE TABLE IF NOT EXISTS addresses (
 address_id integer PRIMARY KEY,
 house_no text,
 street text,
 city text,
 postal_code text,
 country text
);

INSERT INTO addresses (
 house_no,
 street,
 city,
 postal_code,
 country
)
VALUES
 (
 '3960',
 'North 1st Street',
 'San Jose ',
 '95134',
 'USA '
 );
 
INSERT INTO people (
 first_name,
 last_name,
 address_id
)
VALUES
 ('John', 'Doe', 1);

DROP TABLE addresses;
