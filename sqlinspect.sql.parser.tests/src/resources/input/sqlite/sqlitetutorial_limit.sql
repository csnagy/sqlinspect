SELECT
 trackId,
 name
FROM
 tracks
LIMIT 10;
SELECT
 trackId,
 name
FROM
 tracks
LIMIT 10 OFFSET 10;
SELECT
 trackid,
 name,
 bytes
FROM
 tracks
ORDER BY
 bytes DESC
LIMIT 10;
SELECT
 trackid,
 name,
 milliseconds
FROM
 tracks
ORDER BY
 milliseconds ASC
LIMIT 5;
SELECT
 trackid,
 name,
 milliseconds
FROM
 tracks
ORDER BY
 milliseconds DESC
LIMIT 1 OFFSET 1;
SELECT
 trackid,
 name,
 bytes
FROM
 tracks
ORDER BY
 bytes
LIMIT 1 OFFSET 2;