SELECT trackid,
       name,
       albumid
  FROM tracks
 WHERE albumid = (
                     SELECT albumid
                       FROM albums
                      WHERE title = 'Let There Be Rock'
                 );
SELECT customerid,
       firstname,
       lastname
  FROM customers
 WHERE supportrepid IN (
           SELECT employeeid
             FROM employees
            WHERE country = 'Canada'
       );
SELECT avg(sum(bytes) ) 
  FROM tracks
 GROUP BY albumid;
SELECT avg(album.size) 
  FROM (
           SELECT sum(bytes) size
             FROM tracks
            GROUP BY albumid
       )
       AS album;
SELECT albumid,
       title
  FROM albums
 WHERE 10000000 > (
                      SELECT sum(bytes) 
                        FROM tracks
                       WHERE tracks.AlbumId = albums.AlbumId
                  )
 ORDER BY title;
SELECT albumid,
       title,
       (
           SELECT count(trackid) 
             FROM tracks
            WHERE tracks.AlbumId = albums.AlbumId
       )
       tracks_count
  FROM albums
 ORDER BY tracks_count DESC;
