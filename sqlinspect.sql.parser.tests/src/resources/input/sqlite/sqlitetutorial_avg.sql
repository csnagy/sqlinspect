CREATE TABLE avg_tests (val);
INSERT INTO avg_tests (val)
VALUES
 (1),
 (2),
 (10.1),
 (20.5),
 ('8'),
 ('B'),
 (NULL),
 (x'0010'),
 (x'0011');
SELECT rowid,
       val
  FROM avg_tests;
SELECT
 avg(val)
FROM
 avg_tests
WHERE
 rowid < 5;
SELECT
 avg(val)
FROM
 avg_tests;
INSERT INTO avg_tests (val)
VALUES (10.1);
SELECT
 avg(val)
FROM
 avg_tests;
SELECT
 avg(DISTINCT val)
FROM
 avg_tests;
SELECT
 avg(milliseconds)
FROM
 tracks;
SELECT
 albumid,
 avg(milliseconds)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 tracks.albumid,
 title,
 round(avg(milliseconds), 2) avg_length
FROM
 tracks
INNER JOIN albums ON albums.AlbumId = tracks.albumid
GROUP BY
 tracks.albumid;
SELECT
 tracks.albumid,
 title,
 round(avg(milliseconds),2)  avg_leng
FROM
 tracks
INNER JOIN albums ON albums.AlbumId = tracks.albumid
GROUP BY
 tracks.albumid
HAVING
 avg_leng BETWEEN 100000 AND 200000;
