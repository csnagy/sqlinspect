SELECT
 customerid,
 firstname,
 lastname,
 CASE country
 WHEN 'USA' THEN
 'Dosmetic'
 ELSE
 'Foreign'
 END CustomerGroup
FROM
 customers
ORDER BY
 LastName,
 FirstName;
SELECT
 trackid,
 name,
 CASE
 WHEN milliseconds < 60000 THEN
 'short'
 WHEN milliseconds > 6000 AND milliseconds < 300000 THEN 'medium'
 ELSE
 'long'
 END category
FROM
 tracks;
