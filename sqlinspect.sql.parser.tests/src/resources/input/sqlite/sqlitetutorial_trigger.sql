CREATE TABLE leads (
 id integer PRIMARY KEY,
 first_name text NOT NULL,
 last_name text NOT NULL,
 phone text NOT NULL,
 email text NOT NULL,
 source text NOT NULL
);
CREATE TRIGGER validate_email_before_insert_leads BEFORE INSERT ON leads
BEGIN
 SELECT
 CASE
 WHEN NEW.email NOT LIKE '%_@__%.__%' THEN
 RAISE (
 ABORT,
 'Invalid email address'
 )
 END;
END;
INSERT INTO leads (
 first_name,
 last_name,
 email,
 phone
)
VALUES
 (
 'John',
 'Doe',
 'jjj',
 '4089009334'
 );
INSERT INTO leads (
 first_name,
 last_name,
 email,
 phone
)
VALUES
 (
 'John',
 'Doe',
 'john.doe@sqlitetutorial.net',
 '4089009334'
 );
SELECT
 first_name,
 last_name,
 email,
 phone
FROM
 leads;
CREATE TABLE lead_logs (
 id INTEGER PRIMARY KEY,
 old_id int,
 new_id int,
 old_phone text,
 new_phone text,
 old_email text,
 new_email text,
 user_action text,
 created_at text
);
CREATE TRIGGER log_contact_after_update AFTER UPDATE ON leads
WHEN old.phone <> new.phone
OR old.email <> new.email
BEGIN
 INSERT INTO lead_logs (
 old_id,
 new_id,
 old_phone,
 new_phone,
 old_email,
 new_email,
 user_action,
 created_at
 )
VALUES
 (
 old.id,
 new.id,
 old.phone,
 new.phone,
 old.email,
 new.email,
 'UPDATE',
 DATETIME('NOW')
 ) ;
END;
UPDATE leads
SET last_name = 'Smith'
WHERE
 id = 1;
UPDATE leads
SET phone = '4089998888',
 email = 'john.smith@sqlitetutorial.net'
WHERE
 id = 1;
SELECT
 old_phone, new_phone, old_email, new_email, user_action
FROM
 lead_logs;
DROP TRIGGER validate_email_before_insert_leads;
