SELECT
 min(Milliseconds)
FROM
 tracks;
SELECT
 trackid,
 name,
 milliseconds
FROM
 tracks
WHERE
 milliseconds = (
 SELECT
 min(Milliseconds)
 FROM
 tracks
 );
SELECT
 albumid,
 min(milliseconds)
FROM
 tracks
GROUP BY
 albumid;
SELECT
 albumid,
 min(milliseconds)
FROM
 tracks
GROUP BY
 albumid
HAVING
 MIN(milliseconds) < 10000;