UPDATE employees
SET lastname = 'Smith'
WHERE
 employeeid = 3;
UPDATE employees
SET city = 'Toronto',
    state = 'ON',
    postalcode = 'M5P 2N7'
WHERE
    employeeid = 4;
UPDATE employees
SET email = lower(
 firstname || "." || lastname || "@chinookcorp.com"
)
ORDER BY
 firstname
LIMIT 1;
UPDATE employees
SET email = lower(
 firstname || "." || lastname || "@chinookcorp.com"
);
