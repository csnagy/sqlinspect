create table t (x int, y int);

select * from t where x>10 and y<0;
select x,y from t;
select t.x, t.y from t;

select x,y from (select * from t);
select t2.x, t2.y from (select * from t) as t2;
