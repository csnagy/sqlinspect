package sqlinspect.sql.parser.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.parser.SQLParser;
import sqlinspect.sql.parser.SymbolTable;
import sqlinspect.sql.parser.VisitorExtraEdges;

public abstract class AbstractParserTest extends AbstractSQLAnTest {
	protected static final String DEFAULT_OUTPUT = "test.json";
	protected final String inputDir;
	protected final String outputDir;
	protected final String refDir;

	protected SQLDialect dialect;

	protected AbstractParserTest(SQLDialect dialect, String inputDir, String outputDir, String refDir) {
		super();
		this.dialect = dialect;
		this.inputDir = inputDir;
		this.outputDir = outputDir;
		this.refDir = refDir;
	}

	protected void createOutputDir() throws IOException {
		File dir = new File(outputDir);
		if (!dir.exists() && !dir.mkdir()) {
			throw new IOException("Could not create directory: " + outputDir);
		}
	}

	@BeforeEach
	void setUp() throws IOException {
		createOutputDir();
	}

	protected void testFile(String testname) throws JsonParseException, JsonMappingException, IOException {
		File input = new File(inputDir, testname + ".sql");
		File output = new File(outputDir, testname + ".json");
		File reference = new File(refDir + testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		ASG asg = parser.parse();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);

		ASGUtils.exportJson(asg, output);

		ObjectMapper mapper = new ObjectMapper();
		mapper.writerWithDefaultPrettyPrinter().writeValue(output, asg.getRoot());
		SQLRoot rootReference = mapper.readValue(reference, SQLRoot.class);
		SQLRoot rootTest = asg.getRoot();
		assertTrue(rootTest.matchTree(rootReference, false, false, false));
	}

	protected void testString(String s, String testname) throws JsonParseException, JsonMappingException, IOException {
		parseString(s, testname, true);
	}

	protected void parseString(String input, String testname, boolean assertASG)
			throws JsonParseException, JsonMappingException, IOException {
		File output = new File(outputDir, testname + ".json");
		File reference = new File(refDir, testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		ASG asg = parser.parse();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);

		if (assertASG) {
			ASGUtils.exportJson(asg, output);
			SQLRoot root = asg.getRoot();
			ObjectMapper mapper = new ObjectMapper();
			SQLRoot root2 = mapper.readValue(reference, SQLRoot.class);
			assertTrue(root.matchTree(root2, false, false, false));
		}
	}

	protected List<Statement> parseStringToStmt(String input, String testname, boolean assertASG)
			throws JsonParseException, JsonMappingException, IOException {
		File output = new File(outputDir, testname + ".json");
		File reference = new File(refDir, testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		List<Statement> stmts = parser.parseStatements();
		ASG asg = parser.getASG();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);

		if (assertASG) {
			ASGUtils.exportJson(asg, output);
			SQLRoot root = asg.getRoot();
			ObjectMapper mapper = new ObjectMapper();
			SQLRoot root2 = mapper.readValue(reference, SQLRoot.class);
			assertTrue(root.matchTree(root2, false, false, false));
		}

		return stmts;
	}
}
