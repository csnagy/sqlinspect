package sqlinspect.sql.parser.tests;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sql.SQLDialect;

class MySQLParserTest extends AbstractParserTest {

	public MySQLParserTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/mysql/", "mysql", TEST_REF_DIR + "/mysql/");
	}

	@Test
	void testSet() throws JsonParseException, JsonMappingException, IOException {
		testFile("set");
	}

	@Test
	void testRecover() throws JsonParseException, JsonMappingException, IOException {
		testFile("recover");
	}

	@Test
	void testFunction() throws JsonParseException, JsonMappingException, IOException {
		testFile("function");
	}

	@Test
	void testSelectSum() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT " + "SUM(IF(beta_idx=1, beta_value,0)) as beta1_value, "
				+ "SUM(IF(beta_idx=2, beta_value,0)) as beta2_value, "
				+ "SUM(IF(beta_idx=3, beta_value,0)) as beta3_value "
				+ "FROM alpha JOIN beta WHERE alpha_id = beta_alpha_id;";
		testString(s, "selectsum");
	}

	@Test
	void testSelectLimit() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT * FROM foo ORDER BY RAND(NOW()) LIMIT 1;";
		testString(s, "selectlimit");
	}

	@Test
	void testSelectCase() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT dienst.dienst, dienst.url, dienst.info, dienst_prijs.dienst_eenheid, dienst_prijs.prijs, dienst_prijs.inc_btw, dienst_prijs.dienst_optie, \n"
				+ "CASE dienst_prijs.prijs \n" + "WHEN dienst_prijs.prijs = '0.00' THEN 1000 \n"
				+ "WHEN dienst_prijs.prijs > '0.00' THEN 10 \n" + "ELSE NULL\n" + "END AS orderme \n"
				+ "FROM dienst, dienst_prijs \n" + "WHERE dienst.taal = 'nl' && \n"
				+ "dienst.dienst_type = 'Internet toegang' && \n" + "dienst.dienst != 'alle diensten' && \n"
				+ "dienst.publiceer != '' && \n" + "dienst_prijs.dienst_eenheid IN ( 'maand', 'jaar' ) && \n"
				+ "dienst.dienst = dienst_prijs.dienst \n" + "ORDER BY orderme, dienst_prijs.prijs;";
		testString(s, "selectcase");
	}

	@Test
	void testInExpr() throws JsonParseException, JsonMappingException, IOException {
		String s = "select * from t where x in ('asd','qwe');\n" + "select * from t where x in ('asd, qwer');";
		testString(s, "inExpr");
	}

	@Test
	void testSelectIfSub() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT\n" + "s.`state_code`,\n" + "s.`state_name`,\n" + "IF(state_id IN\n"
				+ "(SELECT d2s.state_id\n" + "FROM drove_to_states d2s\n" + "WHERE driver_id = '%u'\n"
				+ "), 1, null) \n" + "`selected`\n" + "FROM `states` s, \n" + "ORDER BY `state_name` ASC;";
		testString(s, "selectifsub");
	}

	@Test
	void testSelectUnion() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT cheap AS priceCat, COUNT(*) productCount FROM MyProducts WHERE price < 1000\n" + "UNION\n"
				+ "SELECT moderate AS priceCat, COUNT(*) FROM MyProducts WHERE price >= 1000 AND price <2000\n"
				+ "UNION\n" + "SELECT expensive AS priceCat, COUNT(*) FROM MyProducts WHERE price >= 2000;";
		testString(s, "selectunion");
	}

	@Test
	void testSelectLarge() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT '%c%' as Chapter,\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status IN ('new','assigned') ) AS 'New',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='document_interface' ) AS 'Document\\\n"
				+ " Interface',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='interface_development' ) AS 'Inter\\\n"
				+ "face Development',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='interface_check' ) AS 'Interface C\\\n"
				+ "heck',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='document_routine' ) AS 'Document R\\\n"
				+ "outine',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='full_development' ) AS 'Full Devel\\\n"
				+ "opment',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='peer_review_1' ) AS 'Peer Review O\\\n"
				+ "ne',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%'AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='peer_review_2' ) AS 'Peer Review Tw\\\n"
				+ "o',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='qa' ) AS 'QA',\n"
				+ "(SELECT count(ticket.id) AS Matches FROM engine.ticket INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%'AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine' AND ticket.status='closed' ) AS 'Closed',\n"
				+ "count(id) AS Total,\n" + "ticket.id AS _id\n" + "FROM engine.ticket\n"
				+ "INNER JOIN engine.ticket_custom ON ticket.id = ticket_custom.ticket\n"
				+ "WHERE ticket_custom.name='chapter' AND ticket_custom.value LIKE '%c%' AND type='New material' AND milestone='1.1.12' AND component NOT LIKE 'internal_engine';";
		testString(s, "selectlarge");
	}

	@Test
	void testParserError() throws JsonParseException, JsonMappingException, IOException {
		String s = "SELECT * FROM t where test isnull asd;";
		testString(s, "parserError");
	}
}
