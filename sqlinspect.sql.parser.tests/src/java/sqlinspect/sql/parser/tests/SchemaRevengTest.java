package sqlinspect.sql.parser.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.util.ASGUtils;
import sqlinspect.sql.parser.SQLParser;
import sqlinspect.sql.parser.SchemaRevEng;
import sqlinspect.sql.parser.SymbolTable;
import sqlinspect.sql.parser.VisitorExtraEdges;

class SchemaRevengTest extends AbstractParserTest {

	public SchemaRevengTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/common/", "common", TEST_REF_DIR + "/common/");
	}

	@Override
	protected void testFile(String testname) throws JsonParseException, JsonMappingException, IOException {
		File input = new File(inputDir, testname + ".sql");
		File output = new File(outputDir, testname + ".json");
		File reference = new File(refDir, testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		ASG asg = parser.parse();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);
		SchemaRevEng.runRevengSchema(asg);
		ASGUtils.exportJson(asg, output);

		ObjectMapper mapper = new ObjectMapper();
		SQLRoot rootTest = asg.getRoot();
		mapper.writerWithDefaultPrettyPrinter().writeValue(output, rootTest);
		SQLRoot rootReference = mapper.readValue(reference, SQLRoot.class);
		assertTrue(rootTest.matchTree(rootReference, false, false, false));
	}

	@Override
	public void testString(String input, String testname) throws JsonParseException, JsonMappingException, IOException {
		File output = new File(outputDir, testname + ".json");
		File reference = new File(refDir, testname + ".json");

		SQLParser parser = SQLParser.create(dialect, input);
		ASG asg = parser.parse();
		SymbolTable symTab = new SymbolTable();
		VisitorExtraEdges.resolveIdentifiers(asg, symTab);
		SchemaRevEng.runRevengSchema(asg);
		ASGUtils.exportJson(asg, output);

		SQLRoot rootTest = asg.getRoot();
		ObjectMapper mapper = new ObjectMapper();
		SQLRoot rootReference = mapper.readValue(reference, SQLRoot.class);
		assertTrue(rootTest.matchTree(rootReference, false, false, false));
	}

	@BeforeEach
	@Override
	void setUp() throws IOException {
		super.setUp();
	}

	@Test
	void testRevEng1() throws JsonParseException, JsonMappingException, IOException {
		testString("SELECT * from tab;", "testRevEng1");
	}

	@Test
	void testRevEng2() throws JsonParseException, JsonMappingException, IOException {
		testString("SELECT x,y from tab1,tab2;", "testRevEng2");
	}

	@Test
	void testRevEng3() throws JsonParseException, JsonMappingException, IOException {
		testString("SELECT x,y from tab1;", "testRevEng3");
	}
}