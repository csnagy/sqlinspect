package sqlinspect.sql.parser.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.List;

import org.junit.jupiter.api.Test;

import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.statm.Statement;

class SQLMatchTest extends AbstractParserTest {
	public SQLMatchTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/mysql/", "mysql", TEST_REF_DIR + "/mysql/");
	}

	private boolean match(Statement stmt1, Statement stmt2, boolean ignoreLiterals, boolean ignoreNames,
			boolean ignoreRefs) throws Throwable {
		MethodHandles.Lookup lookup = MethodHandles.lookup();
		MethodHandle mh = lookup.findVirtual(stmt1.getClass(), "matchTree",
				MethodType.methodType(boolean.class, Base.class, boolean.class, boolean.class, boolean.class));
		return (boolean) mh.invoke(stmt1, stmt2, ignoreLiterals, ignoreNames, ignoreRefs);
	}

	@Test
	void match1() throws Throwable {
		String s1 = "SELECT * FROM {{null}};";
		String s2 = "SELECT * FROM t;";
		List<Statement> stmts1 = parseStringToStmt(s1, "match1s1", false);
		List<Statement> stmts2 = parseStringToStmt(s2, "match1s2", false);
		Statement stmt1 = (Statement) (stmts1.toArray()[0]);
		Statement stmt2 = (Statement) (stmts2.toArray()[0]);
		assertTrue(match(stmt1, stmt2, false, false, false));
	}

	@Test
	void match2() throws Throwable {
		String s1 = "SELECT * FROM t1;";
		String s2 = "SELECT * FROM t;";
		List<Statement> stmts1 = parseStringToStmt(s1, "match2s1", false);
		List<Statement> stmts2 = parseStringToStmt(s2, "match2s2", false);
		Statement stmt1 = (Statement) (stmts1.toArray()[0]);
		Statement stmt2 = (Statement) (stmts2.toArray()[0]);
		assertFalse(match(stmt1, stmt2, false, false, false));
	}

	@Test
	void match3() throws Throwable {
		String s1 = "SELECT * FROM t1;";
		String s2 = "SELECT * FROM {{null}};";
		List<Statement> stmts1 = parseStringToStmt(s1, "match3s1", false);
		List<Statement> stmts2 = parseStringToStmt(s2, "match3s2", false);
		Statement stmt1 = (Statement) (stmts1.toArray()[0]);
		Statement stmt2 = (Statement) (stmts2.toArray()[0]);
		assertTrue(match(stmt1, stmt2, false, false, false));
	}

	@Test
	void match4() throws Throwable {
		String s1 = "SELECT a,b,c FROM t WHERE a=2;";
		String s2 = "SELECT d,e,f FROM t2 WHERE d=3;";
		List<Statement> stmts1 = parseStringToStmt(s1, "match4s1", false);
		List<Statement> stmts2 = parseStringToStmt(s2, "match4s2", false);
		Statement stmt1 = (Statement) (stmts1.toArray()[0]);
		Statement stmt2 = (Statement) (stmts2.toArray()[0]);
		assertFalse(match(stmt1, stmt2, false, false, false));
		assertFalse(match(stmt1, stmt2, true, false, false));
		assertTrue(match(stmt1, stmt2, true, true, true));
	}

	@Test
	void match5() throws Throwable {
		String s1 = "SELECT a,b,c FROM t WHERE a=2;";
		String s2 = "SELECT a,b,c FROM t WHERE a=3;";
		List<Statement> stmts1 = parseStringToStmt(s1, "match5s1", false);
		List<Statement> stmts2 = parseStringToStmt(s2, "match5s2", false);
		Statement stmt1 = (Statement) (stmts1.toArray()[0]);
		Statement stmt2 = (Statement) (stmts2.toArray()[0]);
		assertFalse(match(stmt1, stmt2, false, false, false));
		assertTrue(match(stmt1, stmt2, true, true, false));
		assertTrue(match(stmt1, stmt2, true, false, false));
	}

	@Test
	void match6() throws Throwable {
		String s1 = "SELECT a,b,c FROM t WHERE a=3;";
		String s2 = "SELECT d,e,f FROM t2 WHERE d=3;";
		List<Statement> stmts1 = parseStringToStmt(s1, "match6s1", false);
		List<Statement> stmts2 = parseStringToStmt(s2, "match6s2", false);
		Statement stmt1 = (Statement) (stmts1.toArray()[0]);
		Statement stmt2 = (Statement) (stmts2.toArray()[0]);
		assertFalse(match(stmt1, stmt2, false, false, false));
		assertFalse(match(stmt1, stmt2, true, false, false));
		assertTrue(match(stmt1, stmt2, false, true, true));
		assertTrue(match(stmt1, stmt2, true, true, true));
	}

}
