package sqlinspect.sql.parser.tests;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sql.SQLDialect;

class ExtraEdgesTest extends AbstractParserTest {

	public ExtraEdgesTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/common/", "common", TEST_REF_DIR + "/common/");
	}

	@Test
	void extraedges() throws JsonParseException, JsonMappingException, IOException {
		testFile("extraedges");
	}
}
