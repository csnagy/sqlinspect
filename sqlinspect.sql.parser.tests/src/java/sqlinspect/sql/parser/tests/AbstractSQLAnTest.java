package sqlinspect.sql.parser.tests;

import java.io.File;

public abstract class AbstractSQLAnTest {
	protected static final File TEST_INPUT_DIR = new File(System.getenv().get("TEST_RES_DIR"), "input");
	protected static final File TEST_REF_DIR = new File(System.getenv().get("TEST_RES_DIR"), "reference");
}
