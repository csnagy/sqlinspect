
package sqlinspect.sql.parser.tests;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sql.SQLDialect;

class CreateTableTest extends AbstractParserTest {

	public CreateTableTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/mysql/", "mysql", TEST_REF_DIR + "/mysql/");
	}

	@Test
	void testSimpleCreateTable() throws JsonParseException, JsonMappingException, IOException {
		testFile("createtable");
	}
}
