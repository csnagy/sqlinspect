package sqlinspect.sql.parser.tests;

import java.io.IOException;
import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sql.SQLDialect;

class ImpalaParserTest extends AbstractParserTest {
	private final String[] operands = { "i", "5", "true", "NULL", "'a'", "(1.5 * 8)" };

	public ImpalaParserTest() {
		super(SQLDialect.IMPALA, TEST_INPUT_DIR + "/impala/", "impala", TEST_REF_DIR + "/impala/");
	}

	@Test
	void testSelectNoFromClause() throws JsonParseException, JsonMappingException, IOException {
		testFile("select_nofromclause");
	}

	@Test
	void testSelect() throws JsonParseException, JsonMappingException, IOException {
		testFile("select");
	}

	@Test
	void testAlias() throws JsonParseException, JsonMappingException, IOException {
		testFile("alias");
	}

	@Test
	void testStar() throws JsonParseException, JsonMappingException, IOException {
		testFile("star");
	}

	@Test
	void testFromClause1() throws JsonParseException, JsonMappingException, IOException {
		String tblRefs[] = { "tbl", "db.tbl", "db.tbl.col", "db.tbl.col.fld" };
		StringBuilder sb = new StringBuilder();

		String s1 = "select * from $TBL src1 " + "left outer join $TBL src2 on "
				+ "  src1.key = src2.key and src1.key < 10 and src2.key > 10 " + "right outer join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "full outer join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "left semi join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "left anti join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "right semi join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "right anti join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 " + "inner join $TBL src3 on "
				+ "  src2.key = src3.key and src3.key < 10 ";

		String s2 = "select * from $TBL src1 " + "left outer join $TBL src2 using (a, b, c) "
				+ "right outer join $TBL src3 using (d, e, f) " + "full outer join $TBL src4 using (d, e, f) "
				+ "left semi join $TBL src5 using (d, e, f) " + "left anti join $TBL src6 using (d, e, f) "
				+ "right semi join $TBL src7 using (d, e, f) " + "right anti join $TBL src8 using (d, e, f) "
				+ "join $TBL src9 using (d, e, f) " + "inner join $TBL src10 using (d, e, f) ";
		String s3 = "select * from $TBL cross join $TBL";

		for (String tbl : tblRefs) {
			sb.append(s1.replace("$TBL", tbl));
			sb.append(";\n");
			sb.append(s2.replace("$TBL", tbl));
			sb.append(";\n");
			sb.append(s3.replace("$TBL", tbl));
			sb.append(";\n");
		}

		testString(sb.toString(), "fromclause1");
	}

	@Test
	void testFromClause2() throws JsonParseException, JsonMappingException, IOException {
		testFile("fromclause2");
	}

	@Test
	void testTableSample1() throws JsonParseException, JsonMappingException, IOException {
		String tblRefs[] = { "tbl", "db.tbl", "db.tbl.col", "db.tbl.col.fld" };
		String tblAliases[] = { "", "t" };
		String tblSampleClauses[] = { "", "tablesample system(10)", "tablesample system(100) repeatable(20)" };
		String tblHints[] = { "", "/* +schedule_remote */", "[schedule_random_replica]" };

		StringBuilder sb = new StringBuilder();

		for (String tbl : tblRefs) {
			for (String alias : tblAliases) {
				for (String smp : tblSampleClauses) {
					for (String hint : tblHints) {
						// Single table.
						sb.append(String.format("select * from %s %s %s %s;%n", tbl, alias, smp, hint));
						// Multi table.
						sb.append(String.format("select a.* from %s %s %s %s join %s %s %s %s using (id);%n", tbl,
								alias, smp, hint, tbl, alias, smp, hint));
						sb.append(String.format("select a.* from %s %s %s %s, %s %s %s %s;%n", tbl, alias, smp, hint,
								tbl, alias, smp, hint));
						// Inline view.
						sb.append(
								String.format("select * from (select 1 from %s %s) v %s %s;%n", tbl, alias, smp, hint));

					}
				}
			}
		}

		testString(sb.toString(), "tablesample1");
	}

	@Test
	void testTableSample2() throws JsonParseException, JsonMappingException, IOException {
		testFile("tablesample2");
	}

	@Test
	void testWhereClause() throws JsonParseException, JsonMappingException, IOException {
		testFile("whereclause");
	}

	@Test
	void testGroupBy() throws JsonParseException, JsonMappingException, IOException {
		testFile("groupby");
	}

	@Test
	void testOrderBy() throws JsonParseException, JsonMappingException, IOException {
		testFile("orderby");
	}

	@Test
	void testHaving() throws JsonParseException, JsonMappingException, IOException {
		testFile("having");
	}

	@Test
	void testLimit() throws JsonParseException, JsonMappingException, IOException {
		testFile("limit");
	}

	@Test
	void testOffset() throws JsonParseException, JsonMappingException, IOException {
		testFile("offset");
	}

	@Test
	void testUnion() throws JsonParseException, JsonMappingException, IOException {
		testFile("union");
	}

	@Test
	void testValuesStmt() throws JsonParseException, JsonMappingException, IOException {
		testFile("valuesstmt");
	}

	@Test
	void testWithClause() throws JsonParseException, JsonMappingException, IOException {
		testFile("withclause");
	}

	@Test
	void testNumericLiteralMinMaxValues() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();

		// Test integer types.
		sb.append(String.format("select %s;%n", Byte.toString(Byte.MIN_VALUE)))
				.append(String.format("select %s;%n", Byte.toString(Byte.MAX_VALUE)))
				.append(String.format("select %s;%n", Short.toString(Short.MIN_VALUE)))
				.append(String.format("select %s;%n", Short.toString(Short.MAX_VALUE)))
				.append(String.format("select %s;%n", Integer.toString(Integer.MIN_VALUE)))
				.append(String.format("select %s;%n", Integer.toString(Integer.MAX_VALUE)))
				.append(String.format("select %s;%n", Long.toString(Long.MIN_VALUE)))
				.append(String.format("select %s;%n", Long.toString(Long.MAX_VALUE)));

		// Overflowing long integers parse ok. Analysis will handle it.
		sb.append(String.format("select %s1;%n", Long.toString(Long.MIN_VALUE)))
				.append(String.format("select %s1;%n", Long.toString(Long.MAX_VALUE)));

		// Test min int64-1.
		BigInteger minMinusOne = BigInteger.valueOf(Long.MAX_VALUE);
		minMinusOne = minMinusOne.add(BigInteger.ONE);
		sb.append(String.format("select %s;%n", minMinusOne.toString()));

		// Test max int64+1.
		BigInteger maxPlusOne = BigInteger.valueOf(Long.MAX_VALUE);
		maxPlusOne = maxPlusOne.add(BigInteger.ONE);
		sb.append(String.format("select %s;%n", maxPlusOne.toString()));

		// Test floating-point types.
		sb.append(String.format("select %s;%n", Float.toString(Float.MIN_VALUE)))
				.append(String.format("select %s;%n", Float.toString(Float.MAX_VALUE)))
				.append(String.format("select -%s;%n", Float.toString(Float.MIN_VALUE)))
				.append(String.format("select -%s;%n", Float.toString(Float.MAX_VALUE)))
				.append(String.format("select %s;%n", Double.toString(Double.MIN_VALUE)))
				.append(String.format("select %s;%n", Double.toString(Double.MAX_VALUE)))
				.append(String.format("select -%s;%n", Double.toString(Double.MIN_VALUE)))
				.append(String.format("select -%s;%n", Double.toString(Double.MAX_VALUE)));

		// Test overflow and underflow
		sb.append(String.format("select %s1;%n", Double.toString(Double.MIN_VALUE)))
				.append(String.format("select %s1;%n", Double.toString(Double.MAX_VALUE)));

		testString(sb.toString(), "numericliteralminmaxvalues");
	}

	@Test
	void testLiteralExprs() throws JsonParseException, JsonMappingException, IOException {
		testFile("literalexprs");
	}

	@Test
	void testFunctioncallExpr() throws JsonParseException, JsonMappingException, IOException {
		testFile("functioncallexpr");
	}

	@Test
	void testArithmeticExprs1() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		String[] infixOps = { "*", "/", "%", "DIV", "+", "-", "&", "|", "^" };
		String[] prefixOps = { "~" };
		String[] postfixOps = { "!" };

		for (String lop : operands) {
			for (String rop : operands) {
				for (String op : infixOps) {
					String expr = String.format("%s %s %s", lop, op, rop);
					sb.append(String.format("select %s from t where %s;%n", expr, expr));
				}
				for (String op : postfixOps) {
					String expr = String.format("%s %s", lop, op);
					sb.append(String.format("select %s from t where %s;%n", expr, expr));
				}
				for (String op : prefixOps) {
					String expr = String.format("%s %s", op, lop);
					sb.append(String.format("select %s from t where %s;%n", expr, expr));
				}
			}
		}
		testString(sb.toString(), "arithmeticexprs1");
	}

	@Test
	void testArithmeticExprs2() throws JsonParseException, JsonMappingException, IOException {
		testFile("arithmeticexprs2");
	}

	@Test
	void testFactorialPrecedence() throws JsonParseException, JsonMappingException, IOException {
		testFile("factorialprecedence");
	}

	@Test
	void testTimestampArithmeticExprs1() throws JsonParseException, JsonMappingException, IOException {
		final String[] timeunits = { "YEAR", "MONTH", "WEEK", "DAY", "HOUR", "MINUTE", "SECOND", "MILLISECOND",
				"MICROSECOND", "NANOSECOND" };
		StringBuilder sb = new StringBuilder();

		for (String timeUnit : timeunits) {
			// Non-function call like versions.
			sb.append("select a + interval b " + timeUnit + ";\n").append("select a - interval b " + timeUnit + ";\n")
					.append("select NULL + interval NULL " + timeUnit + ";\n")
					.append("select NULL - interval NULL " + timeUnit + ";\n");

			// Reversed interval and timestamp is ok for addition.
			sb.append("select interval b " + timeUnit + " + a;\n")
					.append("select interval NULL " + timeUnit + " + NULL;\n");

			// Reversed interval and timestamp is an error for subtraction.
			sb.append("select interval b " + timeUnit + " - a; -- error\n");

			// Function-call like versions.
			sb.append("select date_add(a, interval b " + timeUnit + ");\n")
					.append("select date_sub(a, interval b " + timeUnit + ");\n")
					.append("select date_add(NULL, interval NULL " + timeUnit + ");\n")
					.append("select date_sub(NULL, interval NULL " + timeUnit + ");\n");

			// Invalid function name for timestamp arithmetic expr should parse
			// ok.
			sb.append("select error(a, interval b " + timeUnit + ");\n");

			// Invalid time unit parses ok.
			sb.append("select error(a, interval b error);\n");

			// Missing 'interval' keyword. Note that the non-function-call like
			// version will
			// pass without 'interval' because the time unit is recognized as an
			// alias.
			sb.append("select date_add(a, b " + timeUnit + "); -- error\n")
					.append("select date_sub(a, b " + timeUnit + "); -- error\n");

			sb.append("select date_sub(distinct NULL, interval NULL " + timeUnit + "); -- error\n")
					.append("select date_sub(*, interval NULL " + timeUnit + "); -- error\n");
		}

		testString(sb.toString(), "timestamparithmeticexprs1");
	}

	@Test
	void testTimestampArithmeticExprs2() throws JsonParseException, JsonMappingException, IOException {
		testFile("timestamparithmeticexprs2");
	}

	@Test
	void testCaseExprs() throws JsonParseException, JsonMappingException, IOException {
		testFile("caseexprs");
	}

	@Test
	void testCastExprs() throws JsonParseException, JsonMappingException, IOException {
		testFile("castexprs");
	}

	@Test
	void testConditionalExprs() throws JsonParseException, JsonMappingException, IOException {
		testFile("conditionalexprs");
	}

	@Test
	void testAggregateExprs() throws JsonParseException, JsonMappingException, IOException {
		testFile("aggregateexprs");
	}

	@Test
	void testAnalyticExprs() throws JsonParseException, JsonMappingException, IOException {
		testFile("analyticexprs");
	}

	@Test
	void testPredicates() throws JsonParseException, JsonMappingException, IOException {
		String[] operations = { "=", "!=", "<=", ">=", ">", "<", "IS DISTINCT FROM", "IS NOT DISTINCT FROM", "like",
				"ilike", "rlike", "regexp", "iregexp" };
		StringBuilder sb = new StringBuilder();

		for (String lop : operands) {
			for (String rop : operands) {
				for (String op : operations) {
					String expr = String.format("%s %s %s", lop, op, rop);
					sb.append(String.format("select %s from t where %s;%n", expr, expr));
				}
			}
			String isNullExr = String.format("%s is null", lop);
			String isNotNullExr = String.format("%s is not null", lop);
			sb.append(String.format("select %s from t where %s;%n", isNullExr, isNullExr))
					.append(String.format("select %s from t where %s;%n", isNotNullExr, isNotNullExr));
		}

		testString(sb.toString(), "predicates");
	}

	private String composeCompoundPredicates(String andStr, String orStr, String notStr) {
		StringBuilder sb = new StringBuilder();
		// select a, b, c from t where a = 5 and f(b)
		sb.append("select a, b, c from t where a = 5 " + andStr + " f(b);\n");
		// select a, b, c from t where a = 5 or f(b)
		sb.append("select a, b, c from t where a = 5 " + orStr + " f(b);\n");
		// select a, b, c from t where (a = 5 or f(b)) and c = 7
		sb.append("select a, b, c from t where (a = 5 " + orStr + " f(b)) " + andStr + " c = 7;\n");
		// select a, b, c from t where not a = 5
		sb.append("select a, b, c from t where " + notStr + "a = 5;\n");
		// select a, b, c from t where not f(a)
		sb.append("select a, b, c from t where " + notStr + "f(a);\n");
		// select a, b, c from t where (not a = 5 or not f(b)) and not c = 7
		sb.append("select a, b, c from t where (" + notStr + "a = 5 " + orStr + " " + notStr + "f(b)) " + andStr + " "
				+ notStr + "c = 7;\n");
		// select a, b, c from t where (!(!a = 5))
		sb.append("select a, b, c from t where (" + notStr + "(" + notStr + "a = 5));\n");
		// select a, b, c from t where (!(!f(a)))
		sb.append("select a, b, c from t where (" + notStr + "(" + notStr + "f(a)));\n");
		// semantically incorrect negation, but parses ok
		sb.append("select a, b, c from t where a = " + notStr + "5;\n");
		// unbalanced parentheses
		sb.append("select a, b, c from t where " + "(a = 5 " + orStr + " b = 6) " + andStr + " c = 7); -- error\n");
		sb.append("select a, b, c from t where " + "((a = 5 " + orStr + " b = 6) " + andStr + " c = 7; -- error\n");
		// incorrectly positioned negation (!)
		sb.append("select a, b, c from t where a = 5 " + orStr + " " + notStr + "; -- error\n");
		sb.append("select a, b, c from t where " + notStr + "(a = 5) " + orStr + " " + notStr + "; -- error\n");
		return sb.toString();
	}

	private String composeLiteralTruthValues(String andStr, String orStr, String notStr) {
		StringBuilder sb = new StringBuilder();
		String[] truthValues = { "true", "false", "null" };
		for (String l : truthValues) {
			for (String r : truthValues) {
				String andExpr = String.format("%s %s %s", l, andStr, r);
				String orExpr = String.format("%s %s %s", l, orStr, r);
				sb.append(String.format("select %s from t where %s;%n", andExpr, andExpr))
						.append(String.format("select %s from t where %s;%n", orExpr, orExpr));
			}
			String notExpr = String.format("%s %s", notStr, l);
			sb.append(String.format("select %s from t where %s;%n", notExpr, notExpr));
		}
		return sb.toString();
	}

	@Test
	void testCompoundPredicates() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		String[] andStrs = { "and", "&&" };
		String[] orStrs = { "or", "||" };
		// Note the trailing space in "not ". We want to test "!" without a
		// space.
		String[] notStrs = { "!", "not " };
		// Test all combinations of representations for 'or', 'and', and 'not'.
		for (String andStr : andStrs) {
			for (String orStr : orStrs) {
				for (String notStr : notStrs) {
					sb.append(composeCompoundPredicates(andStr, orStr, notStr))
							.append(composeLiteralTruthValues(andStr, orStr, notStr));
				}
			}
		}

		// Test right associativity of NOT.
		for (String notStr : notStrs) {
			sb.append(String.format("select %s a != b;%n", notStr));
			// The NOT should be applied on the result of a != b, and not on a
			// only.
		}
		testString(sb.toString(), "compoundpredicates");
	}

	@Test
	void testBetweenPredicate() throws JsonParseException, JsonMappingException, IOException {
		testFile("betweenpredicate");
	}

	@Test
	void testInPredicate() throws JsonParseException, JsonMappingException, IOException {
		testFile("inpredicate");
	}

	@Test
	void testSlotRef() throws JsonParseException, JsonMappingException, IOException {
		testFile("slotref");
	}

	@Test
	void testInsert() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		for (String qualifier : new String[] { "overwrite", "into" }) {
			for (String optTbl : new String[] { "", "table" }) {
				// Entire unpartitioned table.
				sb.append(String.format("insert %s %s t select a from src where b > 5;%n", qualifier, optTbl));
				// Static partition with one partitioning key.
				sb.append(String.format("insert %s %s t partition (pk1=10) select a from src where b > 5;%n", qualifier,
						optTbl));
				// Dynamic partition with one partitioning key.
				sb.append(String.format("insert %s %s t partition (pk1) select a from src where b > 5;%n", qualifier,
						optTbl));
				// Static partition with two partitioning keys.
				sb.append(
						String.format("insert %s %s t partition (pk1=10, pk2=20) " + "select a from src where b > 5;%n",
								qualifier, optTbl));
				// Fully dynamic partition with two partitioning keys.
				sb.append(String.format("insert %s %s t partition (pk1, pk2) select a from src where b > 5;%n",
						qualifier, optTbl));
				// Partially dynamic partition with two partitioning keys.
				sb.append(String.format("insert %s %s t partition (pk1=10, pk2) select a from src where b > 5;%n",
						qualifier, optTbl));
				// Partially dynamic partition with two partitioning keys.
				sb.append(String.format("insert %s %s t partition (pk1, pk2=20) select a from src where b > 5%n",
						qualifier, optTbl));
				// Static partition with two NULL partitioning keys.
				sb.append(String.format(
						"insert %s %s t partition (pk1=NULL, pk2=NULL) " + "select a from src where b > 5;%n",
						qualifier, optTbl));
				// Static partition with boolean partitioning keys.
				sb.append(String.format(
						"insert %s %s t partition (pk1=false, pk2=true) " + "select a from src where b > 5;%n",
						qualifier, optTbl));
				// Static partition with arbitrary exprs as partitioning keys.
				sb.append(String.format(
						"insert %s %s t partition (pk1=abc, pk2=(5*8+10)) " + "select a from src where b > 5;%n",
						qualifier, optTbl));
				sb.append(String.format("insert %s %s t partition (pk1=f(a), pk2=!true and false) "
						+ "select a from src where b > 5;%n", qualifier, optTbl));
				// Permutation
				sb.append(String.format("insert %s %s t(a,b,c) values(1,2,3);%n", qualifier, optTbl));
				// Permutation with mismatched select list (should parse fine)
				sb.append(String.format("insert %s %s t(a,b,c) values(1,2,3,4,5,6);%n", qualifier, optTbl));
				// Permutation and partition
				sb.append(String.format("insert %s %s t(a,b,c) partition(d) values(1,2,3,4);%n", qualifier, optTbl));
				// Empty permutation list
				sb.append(String.format("insert %s %s t() select 1 from a;%n", qualifier, optTbl));
				// Permutation with optional query statement
				sb.append(String.format("insert %s %s t() partition(d);%n ", qualifier, optTbl));
				sb.append(String.format("insert %s %s t();%n ", qualifier, optTbl));
				// No comma in permutation list
				sb.append(String.format("insert %s %s t(a b c) select 1 from a; -- error%n", qualifier, optTbl));
				// Can't use strings as identifiers in permutation list
				sb.append(String.format("insert %s %s t('a') select 1 from a; -- error%n", qualifier, optTbl));
				// Expressions not allowed in permutation list
				sb.append(String.format("insert %s %s t(a=1, b) select 1 from a; -- error%n", qualifier, optTbl));
			}
		}
		testString(sb.toString(), "insert");
	}

	@Test
	void testUpsert() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		for (String optTbl : new String[] { "", "table" }) {
			// SELECT clause
			sb.append(String.format("upsert into %s t select a from src;%n", optTbl));
			// VALUES clause
			sb.append(String.format("upsert into %s t values (1, 2, 3);%n", optTbl));
			// Permutation
			sb.append(String.format("upsert into %s t (a,b,c) values(1,2,3);%n", optTbl));
			// Permutation with mismatched select list (should parse fine)
			sb.append(String.format("upsert into %s t (a,b,c) values(1,2,3,4,5,6);%n", optTbl));
			// Empty permutation list
			sb.append(String.format("upsert into %s t () select 1 from a;%n", optTbl));
			// Permutation with optional query statement
			sb.append(String.format("upsert into %s t ();%n", optTbl));
			// WITH clause
			sb.append(String.format(
					"with x as (select a from src where b > 5) upsert into %s " + "t select * from x;%n", optTbl));

			// Missing query statement
			sb.append(String.format("upsert into %s t; -- error%n", optTbl));
			// Missing 'into'.
			sb.append(String.format("upsert %s t select a from src where b > 5; -- error%n", optTbl));
			// Missing target table identifier.
			sb.append(String.format("upsert into %s select a from src where b > 5; -- error%n", optTbl));
			// No comma in permutation list
			sb.append(String.format("upsert %s t(a b c) select 1 from a; -- error%n", optTbl));
			// Can't use strings as identifiers in permutation list
			sb.append(String.format("upsert %s t('a') select 1 from a; -- error%n", optTbl));
			// Expressions not allowed in permutation list
			sb.append(String.format("upsert %s t(a=1, b) select 1 from a; -- error%n", optTbl));
			// Upsert doesn't support ignore.
			sb.append(String.format("upsert ignore into %s t select a from src; -- error%n", optTbl));
			// Upsert doesn't support partition clauses.
			sb.append(String.format("upsert into %s t partition (pk1=10) select a from src; -- error%n", optTbl));
			// Upsert doesn't support overwrite.
			sb.append(String.format("upsert overwrite %s t select 1 from src; -- error%n", optTbl));
		}
		testString(sb.toString(), "upsert");
	}

	@Test
	void testUpdate() throws JsonParseException, JsonMappingException, IOException {
		testFile("update");
	}

	@Test
	void testUse() throws JsonParseException, JsonMappingException, IOException {
		testFile("use");
	}

	@Test
	void testDelete() throws JsonParseException, JsonMappingException, IOException {
		testFile("delete");
	}

	@Test
	void testCreateDB() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		String[] dbKeywords = { "DATABASE", "SCHEMA" };
		int i = 0;
		for (String kw : dbKeywords) {
			sb.append(String.format("CREATE %s Foo%d;%n", kw, i++));
			sb.append(String.format("CREATE %s IF NOT EXISTS Foo%d;%n", kw, i++));

			sb.append(String.format("CREATE %s Foo%d COMMENT 'Some comment';%n", kw, i++));
			sb.append(String.format("CREATE %s Foo%d LOCATION '/hdfs_location';%n", kw, i++));
			sb.append(String.format("CREATE %s Foo%d LOCATION '/hdfs_location';%n", kw, i++));
			sb.append(String.format("CREATE %s Foo%d COMMENT 'comment' LOCATION '/hdfs_location';%n", kw, i++));

			// Only string literals are supported
			sb.append(String.format("CREATE %s Foo%d COMMENT mytable; -- error%n", kw, i++));
			sb.append(String.format("CREATE %s Foo%d LOCATION /hdfs_location; -- error%n", kw, i++));

			// COMMENT needs to be *before* LOCATION
			sb.append(
					String.format("CREATE %s Foo%d LOCATION '/hdfs/location' COMMENT 'comment'; -- error%n", kw, i++));

			sb.append(String.format("CREATE %s Foo%d COMMENT LOCATION '/hdfs_location'; -- error%n", kw, i++));
			sb.append(String.format("CREATE %s Foo%d LOCATION; -- error%n", kw, i++));
			sb.append(String.format("CREATE %s Foo%d LOCATION 'dfsd' 'dafdsf'; -- error%n", kw, i++));

			sb.append(String.format("CREATE Foo%d; -- error%n", i++));
			sb.append(String.format("CREATE %s 'Foo%d'; -- error%n", kw, i++));
			sb.append(String.format("CREATE %s; -- error%n", kw));
			sb.append(String.format("CREATE %s IF EXISTS Foo%d; -- error%n", kw, i++));

			sb.append(String.format("CREATE %sS Foo%d; -- error%n", kw, i++));
		}
		testString(sb.toString(), "createdb");
	}

	@Test
	void testCreateFunction() throws JsonParseException, JsonMappingException, IOException {
		testFile("createfunction");
	}

	@Test
	void testCreateTable1() throws JsonParseException, JsonMappingException, IOException {
		testFile("createtable1");
	}

	@Test
	void testCreateTableProperties() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		// Table Properties
		String[] tblPropTypes = { "TBLPROPERTIES", "WITH SERDEPROPERTIES" };
		int i = 0;

		for (String propType : tblPropTypes) {
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s ('a'='b', 'c'='d', 'e'='f');%n", i++, propType));
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s; -- error%n", i++, propType));
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s (); -- error%n", i++, propType));
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s ('a'); -- error%n", i++, propType));
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s ('a'=); -- error%n", i++, propType));
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s ('a'=c); -- error%n", i++, propType));
			sb.append(String.format("CREATE TABLE Foo%d (i int) %s (a='c'); -- error%n", i++, propType));
		}
		sb.append(String.format("CREATE TABLE Foo%d (i int) WITH SERDEPROPERTIES ('a'='b') "
				+ "TBLPROPERTIES ('c'='d', 'e'='f'); -- error%n", i++));
		// TBLPROPERTIES must go after SERDEPROPERTIES
		sb.append(String.format("CREATE TABLE Foo%d (i int) TBLPROPERTIES ('c'='d', 'e'='f') "
				+ "WITH SERDEPROPERTIES ('a'='b'); -- error%n", i++));

		sb.append(String.format("CREATE TABLE Foo%d (i int) SERDEPROPERTIES ('a'='b'); -- error%n", i++));

		sb.append(String.format("CREATE TABLE Foo%d (i int, s string) STORED AS SEQFILE; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d (i int, s string) STORED TEXTFILE; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d LIKE Bar STORED AS TEXT; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d LIKE Bar COMMENT; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d LIKE Bar STORED TEXTFILE; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d LIKE Bar STORED AS; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d LIKE Bar LOCATION; -- error%n", i));

		testString(sb.toString(), "createtable_properties");
	}

	@Test
	void testCreateTable2() throws JsonParseException, JsonMappingException, IOException {
		testFile("createtable2");
	}

	@Test
	void testCreateTableKudu() throws JsonParseException, JsonMappingException, IOException {
		// Column options for Kudu tables
		String[] encodings = { "encoding auto_encoding", "encoding plain_encoding", "encoding prefix_encoding",
				"encoding group_varint", "encoding rle", "encoding dict_encoding", "encoding bit_shuffle",
				"encoding unknown", "" };
		String[] compression = { "compression default_compression", "compression no_compression", "compression snappy",
				"compression lz4", "compression zlib", "compression unknown", "" };

		StringBuilder sb = new StringBuilder();
		int i = 0;

		String[] nullability = { "not null", "null", "" };
		String[] defaultVal = { "default 10", "" };
		String[] blockSize = { "block_size 4096", "" };
		for (String enc : encodings) {
			for (String comp : compression) {
				for (String nul : nullability) {
					for (String def : defaultVal) {
						for (String block : blockSize) {
							sb.append(String.format(
									"CREATE TABLE Foo%d (i int PRIMARY KEY " + "%s %s %s %s %s) STORED AS KUDU;%n", i++,
									nul, enc, comp, def, block));
							sb.append(String.format(
									"CREATE TABLE Foo%d (i int PRIMARY KEY " + "%s %s %s %s %s) STORED AS KUDU;%n", i++,
									block, nul, enc, comp, def));
							sb.append(String.format(
									"CREATE TABLE Foo%d (i int PRIMARY KEY " + "%s %s %s %s %s) STORED AS KUDU;%n", i++,
									def, block, nul, enc, comp));
							sb.append(String.format(
									"CREATE TABLE Foo%d (i int PRIMARY KEY " + "%s %s %s %s %s) STORED AS KUDU;%n", i++,
									comp, def, block, nul, enc));
							sb.append(String.format(
									"CREATE TABLE Foo%d (i int PRIMARY KEY " + "%s %s %s %s %s) STORED AS KUDU;%n", i++,
									enc, comp, def, block, nul));
							sb.append(String.format(
									"CREATE TABLE Foo%d (i int PRIMARY KEY " + "%s %s %s %s %s) STORED AS KUDU;%n", i++,
									enc, comp, block, def, nul));
						}
					}
				}
			}
		}
		// Column option is specified multiple times for the same column
		sb.append(String.format(
				"CREATE TABLE Foo%d(a int PRIMARY KEY ENCODING RLE ENCODING PLAIN) " + "STORED AS KUDUl; -- error%n",
				i++));
		// Constant expr used in DEFAULT
		sb.append(String.format("CREATE TABLE Foo%d(a int PRIMARY KEY, b int DEFAULT 1+1) STORED AS KUDU;%n", i++));
		sb.append(String.format(
				"CREATE TABLE Foo%d(a int PRIMARY KEY, b float DEFAULT cast(1.1 as float)) " + "STORED AS KUDU;%n",
				i++));
		// Non-literal value used in BLOCK_SIZE
		sb.append(String.format(
				"CREATE TABLE Foo%d(a int PRIMARY KEY, b int BLOCK_SIZE 1+1) " + "STORED AS KUDU; -- error%n", i++));
		sb.append(String.format("CREATE TABLE Foo%d(a int PRIMARY KEY BLOCK_SIZE -1) STORED AS KUDU; -- error%n", i));

		testString(sb.toString(), "createtable_kudu");
	}

	@Test
	void testCreateView() throws JsonParseException, JsonMappingException, IOException {
		testFile("createview");
	}

	@Test
	void testTruncate() throws JsonParseException, JsonMappingException, IOException {
		testFile("truncate");
	}

	@Test
	void testDrop() throws JsonParseException, JsonMappingException, IOException {
		testFile("drop");
	}

	private String typeDefsOK(String type, int i) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("CREATE TABLE t%d_1 (i %s);%n", i, type));
		sb.append(String.format("SELECT CAST (i AS %s);%n", type));
		// Test typeDefStr in complex types.
		sb.append(String.format("CREATE TABLE t%d_2 (i MAP<%s, %s>);%n", i, type, type));
		sb.append(String.format("CREATE TABLE t%d_3 (i ARRAY<%s>);%n", i, type));
		sb.append(String.format("CREATE TABLE t%d_4 (i STRUCT<f:%s>);%n", i, type));
		return sb.toString();
	}

	private String typeDefsError(String type, int i) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("CREATE TABLE t%d_1 (i %s); -- error%n", i, type));
		sb.append(String.format("SELECT CAST (i AS %s); -- error%n", type));
		return sb.toString();
	}

	@Test
	void testTypeDefs() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		String[] simpleTypes = { "BOOLEAN", "TINYINT", "SMALLINT", "INT", "INTEGER", "BIGIN", "FLOAT", "DOUBLE", "REAL",
				"STRING", "CHAR(1)", "VARCHAR(1)", "VARCHAR(20)", "BINARY", "DECIMAL", "TIMESTAMP" };
		String[] decimalOK = { "DECIMAL(1)", "DECIMAL(1,2)", "DECIMAL(2,1)", "DECIMAL(6,6)", "DECIMAL(100,0)",
				"DECIMAL(0,0)" };
		String[] decimalERROR = { "DECIMAL()", "DECIMAL(a)", "DECIMAL(1,a)", "DECIMAL(1,2,3)", "DECIMAL(-1)" };
		String[] complexOK = { "ARRAY<BIGINT>", "MAP<TINYINT, DOUBLE>", "STRUCT<f:TINYINT>",
				"STRUCT<a:TINYINT, b:BIGINT, c:DOUBLE>",
				"STRUCT<a:TINYINT COMMENT 'x', b:BIGINT, c:DOUBLE COMMENT 'y'>" };
		String[] complexERROR = { "CHAR()", "CHAR(1, 1)", "ARRAY<>", "ARRAY BIGINT", "MAP<>", "MAP<TINYINT>",
				"MAP<TINYINT, BIGINT, DOUBLE>", "STRUCT<>", "STRUCT<TINYINT>", "STRUCT<a TINYINT>",
				"STRUCT<'a':TINYINT>" };

		int i = 0;
		for (String s : simpleTypes) {
			sb.append(typeDefsOK(s, i++));
		}

		for (String s : decimalOK) {
			sb.append(typeDefsOK(s, i++));
		}

		for (String s : decimalERROR) {
			sb.append(typeDefsError(s, i++));
		}

		for (String s : complexOK) {
			sb.append(typeDefsOK(s, i++));
		}

		for (String s : complexERROR) {
			sb.append(typeDefsError(s, i++));
		}

		testString(sb.toString(), "typedefs");
	}

	@Test
	void testSubQueries1() throws JsonParseException, JsonMappingException, IOException {
		testFile("subqueries1");
	}

	@Test
	void testSubQueries2() throws JsonParseException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		// Binary nested predicates
		String subquery = "(SELECT count(*) FROM bar)";
		String[] operators = { "=", "!=", "<>", ">", ">=", "<", "<=", "<=>", "IS DISTINCT FROM",
				"IS NOT DISTINCT FROM" };
		for (String op : operators) {
			sb.append(String.format("SELECT * FROM foo WHERE a %s %s;%n", op, subquery));
			sb.append(String.format("SELECT * FROM foo WHERE %s %s a;%n", subquery, op));
		}

		// Binary predicate between subqueries
		for (String op : operators) {
			sb.append(String.format("SELECT * FROM foo WHERE %s %s %s;%n", subquery, op, subquery));
		}

		testString(sb.toString(), "subqueries2");
	}

	@Test
	void testSet() throws JsonParseException, JsonMappingException, IOException {
		testFile("set");
	}

	@Test
	void testRecover() throws JsonParseException, JsonMappingException, IOException {
		testFile("recover");
	}

}
