package sqlinspect.sql.parser.tests;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sql.SQLDialect;

class SQLiteParserTest extends AbstractParserTest {

	public SQLiteParserTest() {
		super(SQLDialect.SQLITE, TEST_INPUT_DIR + "/sqlite/", "sqlite", TEST_REF_DIR + "/sqlite/");
	}

	@Test
	void testAlterTable() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_altertable");
	}

	@Test
	void testAutoIncrememt() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_autoincrement");
	}

	@Test
	void testAvg() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_avg");
	}

	@Test
	void testCase() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_case");
	}

	@Test
	void testCount() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_count");
	}

	@Test
	void testCreateView() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_createview");
	}

	@Test
	void testCrossJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_crossjoin");
	}

	@Test
	void testDataTypes() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_datatypes");
	}

	@Test
	void testDelete() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_delete");
	}

	@Test
	void testDistinct() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_distinct");
	}

	@Test
	void testDrop() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_drop");
	}

	@Test
	void testFullOuterJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_fullouterjoin");
	}

	@Test
	void testGlob() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_glob");
	}

	@Test
	void testGroupBy() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_groupby");
	}

	@Test
	void testHaving() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_having");
	}

	@Test
	void testIn() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_in");
	}

	@Test
	void testIndex() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_index");
	}

	@Test
	void testIndexExpression() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_indexexpression");
	}

	@Test
	void testInnerjoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_innerjoin");
	}

	@Test
	void testInsert() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_insert");
	}

	@Test
	void testLeftJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_leftjoin");
	}

	@Test
	void testLike() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_like");
	}

	@Test
	void testLimit() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_limit");
	}

	@Test
	void testMax() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_max");
	}

	@Test
	void testMin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_min");
	}

	@Test
	void testOrderBy() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_orderby");
	}

	@Test
	void testPriKey() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_prikey");
	}

	@Test
	void testSelect() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_select");
	}

	@Test
	void testSelfJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_selfjoin");
	}

	@Test
	void testSubQuery() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_subquery");
	}

	@Test
	void testSum() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_sum");
	}

	@Test
	void testTrigger() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_trigger");
	}

	@Test
	void testUnion() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_union");
	}

	@Test
	void testUpdate() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_update");
	}

	@Test
	void testVacuum() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_vacuum");
	}

	@Test
	void testWhere() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_where");
	}

	@Test
	void testCreateTable() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_createtable");
	}

	@Test
	void testPragma() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_pragma");
	}

	@Test
	void testJoins() throws JsonParseException, JsonMappingException, IOException {
		testFile("joins");
	}

}
