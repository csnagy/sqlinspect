package sqlinspect.sql.parser.tests;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;

import org.junit.jupiter.api.Test;

import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.parser.MySQLParser;
import sqlinspect.sql.parser.SQLParser;

class ParserAPITests extends AbstractSQLAnTest {
	private static final File INPUT_DIR = new File(TEST_INPUT_DIR, "common");

	@Test
	void shoudThrowNoSuchFileException() {
		assertThrows(NoSuchFileException.class, () -> {
			SQLParser.create(SQLDialect.MYSQL, new File(INPUT_DIR, "apitest_nofile.sql"));
		});
	}

	@Test
	void shoudCreateParserForFile() throws IOException {
		SQLParser parser = SQLParser.create(SQLDialect.MYSQL, new File(INPUT_DIR, "apitest.sql"));
		boolean isMySQLParser = parser instanceof MySQLParser;
		assertTrue(isMySQLParser);
	}

	@Test
	void shoudCreateParserForString() throws IOException {
		SQLParser parser = SQLParser.create(SQLDialect.MYSQL, "SELECT * FROM T;");
		boolean isMySQLParser = parser instanceof MySQLParser;
		assertTrue(isMySQLParser);
	}

	@Test
	void shoudCreateASG() throws IOException {
		SQLParser parser = SQLParser.create(SQLDialect.MYSQL, "SELECT * FROM T;");
		ASG asg = parser.parse();
		assertNotNull(asg);
	}

}
