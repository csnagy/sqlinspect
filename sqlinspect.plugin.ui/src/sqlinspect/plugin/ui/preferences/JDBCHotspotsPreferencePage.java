package sqlinspect.plugin.ui.preferences;

import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotParameterSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotRegexpSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotSignatureDialog;
import sqlinspect.plugin.ui.preferences.fieldeditors.StringListFieldEditor;

public class JDBCHotspotsPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferences.JDBCHotspotsPreferencePage";

	public JDBCHotspotsPreferencePage() {
		super();
		setDescription("JDBC Hotspot Settings");
	}

	@Override
	protected void createEditors(Composite parent) {
		StringListFieldEditor queryHotspotsEditor = new StringListFieldEditor(PreferenceConstants.JDBC_STMT_EXECUTE,
				"Statement.execute signatures", HotspotParameterSignatureDialog.class, parent);
		addField(queryHotspotsEditor, parent);

		StringListFieldEditor pstmtHotspotsEditor = new StringListFieldEditor(PreferenceConstants.JDBC_PSTMT_EXECUTE,
				"PreparedStatement.execute signatures", HotspotSignatureDialog.class, parent);
		addField(pstmtHotspotsEditor, parent);

		StringListFieldEditor jdbcPstmtEditor = new StringListFieldEditor(PreferenceConstants.JDBC_PSTMT,
				"Connection.prepareStatement signatures", HotspotParameterSignatureDialog.class, parent);
		addField(jdbcPstmtEditor, parent);

		StringListFieldEditor jdbcPstmtSetterEditor = new StringListFieldEditor(PreferenceConstants.JDBC_PSTMT_SETTER,
				"PreparedStatement.set signatures", HotspotRegexpSignatureDialog.class, parent);
		addField(jdbcPstmtSetterEditor, parent);
	}

}