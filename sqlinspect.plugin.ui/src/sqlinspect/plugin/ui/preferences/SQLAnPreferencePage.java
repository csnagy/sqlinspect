package sqlinspect.plugin.ui.preferences;

import java.util.stream.Stream;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPreferencePage;

import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.sql.SQLDialect;

public class SQLAnPreferencePage extends AbstractPreferencePage implements IWorkbenchPreferencePage {

	public static final String PREFERENCE_PAGE_ID = "sqlinspect.plugin.ui.preferences.SQLAnPreferencePage";
	public static final String PROPERTIES_PAGE_ID = "sqlinspect.plugin.ui.properties.SQLAnPreferencePage";

	private static final String[] FILTER_EXTS = { "*.sql", "*.*" };

	private BooleanFieldEditor loadSchemaFromDB;
	private BooleanFieldEditor loadSchemaFromFile;
	private StringFieldEditor dbURLTextField;
	private StringFieldEditor dbUserTextField;
	private StringFieldEditor dbPasswordTextField;
	private FileFieldEditor dbSchemaFileField;

	public SQLAnPreferencePage() {
		super();
		setDescription("SQL Analyzer Settings");
	}

	@Override
	protected void createEditors(Composite parent) {
		createSQLDialectSelector(parent);

		createSeparator(parent);

		createDatabaseConnectionContent(parent);

		createSeparator(parent);

		createAnalyzerSettings(parent);
	}

	private void createSQLDialectSelector(Composite top) {
		String[][] dialects = Stream.of(SQLDialect.values()).map(d -> new String[] { d.toString(), d.toString() })
				.toArray(String[][]::new);
		Composite composite = createGridComposite(top, GRID_COLS, 2);
		ComboFieldEditor dialectComboField = new ComboFieldEditor(PreferenceConstants.DIALECT, "SQL Dialect", dialects,
				composite);
		addField(dialectComboField, composite);
	}

	private void createDatabaseConnectionContent(final Composite top) {
		Composite dbSchemaComposite = createGridComposite(top, GRID_COLS, 2);

		loadSchemaFromDB = new BooleanFieldEditor(PreferenceConstants.LOAD_SCHEMA_FROM_DB, "Load schema from database",
				dbSchemaComposite);
		addField(loadSchemaFromDB, dbSchemaComposite, 2);

		dbURLTextField = new StringFieldEditor(PreferenceConstants.DB_URL, "DB URL", dbSchemaComposite);
		addField(dbURLTextField, dbSchemaComposite);

		dbUserTextField = new StringFieldEditor(PreferenceConstants.DB_USER, "DB User", dbSchemaComposite);
		addField(dbUserTextField, dbSchemaComposite);

		dbPasswordTextField = new StringFieldEditor(PreferenceConstants.DB_PASSWORD, "DB Password", dbSchemaComposite);
		addField(dbPasswordTextField, dbSchemaComposite);

		Composite fileSchemaComposite = createGridComposite(top, GRID_COLS, 2);

		loadSchemaFromFile = new BooleanFieldEditor(PreferenceConstants.LOAD_SCHEMA_FROM_FILE, "Load schema from file",
				fileSchemaComposite);
		addField(loadSchemaFromFile, fileSchemaComposite, 2);

		Composite subComposite = createGridComposite(fileSchemaComposite, GRID_COLS, 2);
		dbSchemaFileField = new FileFieldEditor(PreferenceConstants.DB_SCHEMA, "Schema file", subComposite);
		dbSchemaFileField.setFileExtensions(FILTER_EXTS);
		addField(dbSchemaFileField, subComposite);

		loadSchemaFromDB.setPropertyChangeListener((PropertyChangeEvent event) -> selectSchemaFromDB(
				(Boolean) event.getNewValue(), dbSchemaComposite, fileSchemaComposite));

		loadSchemaFromFile.setPropertyChangeListener((PropertyChangeEvent event) -> selectSchemaFromFile(
				(Boolean) event.getNewValue(), dbSchemaComposite, subComposite));

		selectSchemaFromDB(loadSchemaFromDB.getBooleanValue(), dbSchemaComposite, fileSchemaComposite);
		selectSchemaFromFile(loadSchemaFromFile.getBooleanValue(), dbSchemaComposite, subComposite);
	}

	private void createAnalyzerSettings(Composite top) {
		Composite composite = createGridComposite(top, GRID_COLS, 2);
		BooleanFieldEditor runSmellDetectorsField = new BooleanFieldEditor(PreferenceConstants.RUN_SMELL_DETECTORS,
				"Run smell detectors", composite);
		addField(runSmellDetectorsField, composite);

		BooleanFieldEditor runTableAccessField = new BooleanFieldEditor(PreferenceConstants.RUN_TABLE_ACCESS,
				"Run table/column access analysis", composite);
		addField(runTableAccessField, composite);

		BooleanFieldEditor runSQLMetricsField = new BooleanFieldEditor(PreferenceConstants.RUN_SQL_METRICS,
				"Calculate query metrics", composite);
		addField(runSQLMetricsField, composite);

		BooleanFieldEditor dumpStatsField = new BooleanFieldEditor(PreferenceConstants.DUMP_STATISTICS,
				"Dump analyzer statistics", composite);
		addField(dumpStatsField, composite);
	}

	private void selectSchemaFromDB(boolean enabled, Composite comp1, Composite comp2) {
		dbURLTextField.setEnabled(enabled, comp1);
		dbUserTextField.setEnabled(enabled, comp1);
		dbPasswordTextField.setEnabled(enabled, comp1);
		loadSchemaFromFile.setEnabled(!enabled, comp2);
	}

	private void selectSchemaFromFile(boolean enabled, Composite comp1, Composite comp2) {
		loadSchemaFromDB.setEnabled(!enabled, comp1);
		dbSchemaFileField.setEnabled(enabled, comp2);
	}
}