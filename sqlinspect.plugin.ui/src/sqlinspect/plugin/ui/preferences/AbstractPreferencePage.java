package sqlinspect.plugin.ui.preferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.dialogs.PropertyPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;

public abstract class AbstractPreferencePage extends PropertyPage implements IWorkbenchPreferencePage {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractPreferencePage.class);

	private final List<FieldEditor> editors = new ArrayList<>();
	private final Map<FieldEditor, Composite> editorParents = new HashMap<>();

	private Optional<IProject> project = Optional.empty();

	protected static final int GRID_COLS = 2;

	protected AbstractPreferencePage() {
		super();
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected final Control createContents(Composite parent) {
		Composite top = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().applyTo(top);
		GridLayoutFactory.fillDefaults().numColumns(GRID_COLS).applyTo(top);

		project = getSelectedProject();
		if (project.isPresent()) {
			boolean enabled = Activator.isProjectSettingsEnabledInSession(project.get())
					|| Activator.isProjectSettingsEnabledInPersitentProperty(project.get());
			Activator.enableProjectSettingsInSession(project.get(), enabled);
		}

		initPreferencesStore();

		createEditors(top);

		enableAndUpdateEditors();
		enableButtons();

		return top;
	}

	protected abstract void createEditors(Composite parent);

	protected static Composite createGridComposite(Composite top, int colSpan, int gridColumns) {
		Composite composite = new Composite(top, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).span(colSpan, 1).applyTo(composite);
		GridLayoutFactory.fillDefaults().numColumns(gridColumns).applyTo(composite);
		return composite;
	}

	protected static void createSeparator(Composite parent) {
		Label sep = new Label(parent, SWT.SEPARATOR | SWT.SHADOW_OUT | SWT.HORIZONTAL);
		GridDataFactory.fillDefaults().span(GRID_COLS, 1).applyTo(sep);
	}

	protected static Button createLabeledCheck(String title, String tooltip, boolean value, Composite defPanel) {
		Button fButton = new Button(defPanel, SWT.CHECK | SWT.LEFT);
		GridDataFactory.fillDefaults().applyTo(fButton);
		fButton.setText(title);
		fButton.setSelection(value);
		fButton.setToolTipText(tooltip);
		return fButton;
	}

	protected void addField(FieldEditor fieldEditor, Composite parent) {
		fieldEditor.setPage(this);
		fieldEditor.setPreferenceStore(getPreferenceStore());
		fieldEditor.load();
		editors.add(fieldEditor);
		editorParents.put(fieldEditor, parent);
	}

	protected void addField(FieldEditor fieldEditor, Composite parent, int columns) {
		fieldEditor.fillIntoGrid(parent, columns);
		addField(fieldEditor, parent);
	}

	@Override
	protected void performDefaults() {
		for (FieldEditor editor : editors) {
			editor.loadDefault();
		}
		super.performDefaults();
	}

	@Override
	public boolean performCancel() {
		if (project.isPresent()) {
			Activator.enableProjectSettingsInSession(project.get(),
					Activator.isProjectSettingsEnabledInPersitentProperty(project.get()));
		}
		return super.performCancel();
	}

	@Override
	public boolean performOk() {
		if (project.isPresent()) {
			try {
				Activator.enableProjectSettingsInPersistentProperty(project.get(),
						Activator.isProjectSettingsEnabledInSession(project.get()));
			} catch (CoreException e) {
				LOG.error("Could not save project property", e);
			}
		}

		for (FieldEditor editor : editors) {
			editor.store();
		}
		return super.performOk();
	}

	protected void initPreferencesStore() {
		if (project.isPresent() && Activator.isProjectSettingsEnabledInSession(project.get())) {
			LOG.trace("Init preference store with project settings");
			setPreferenceStore(Activator.getProjectPreferenceStore(project.get()));
		} else {
			LOG.trace("Init preference store with workspace settings");
			setPreferenceStore(Activator.getDefault().getPreferenceStore());
		}
	}

	@Override
	public void init(IWorkbench workbench) {
		// nothing to do here
	}

	protected List<FieldEditor> getEditors() {
		return editors;
	}

	protected void createWorkspaceButtons(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().span(GRID_COLS, 1).applyTo(composite);
		GridLayoutFactory.fillDefaults().margins(0, 0).numColumns(2).applyTo(composite);

		composite.setFont(parent.getFont());

		Button enableProjectCheck = createLabeledCheck("Enable project specific settings",
				"These settings would be used for the current project only",
				Activator.isProjectSettingsEnabledInSession(project.get()), composite);

		enableProjectCheck.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				boolean newValue = enableProjectCheck.getSelection();
				if (newValue != Activator.isProjectSettingsEnabledInSession(project.get())) {
					Activator.enableProjectSettingsInSession(project.get(), newValue);
					initPreferencesStore();
					enableAndUpdateEditors();
					enableButtons();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				// ignored
			}
		});

		Link workspaceSettingsLink = createLink(composite, "Configure Workspace Settings...");
		GridDataFactory.fillDefaults().align(SWT.END, SWT.CENTER).grab(true, false).applyTo(workspaceSettingsLink);

		Label sep = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).applyTo(sep);
	}

	protected Optional<IProject> getSelectedProject() {
		IAdaptable resource = getElement();
		if (resource != null) {
			return Optional.of(resource.getAdapter(IProject.class));
		}
		return Optional.empty();
	}

	protected void enableAndUpdateEditors() {
		boolean enabled = !project.isPresent() || Activator.isProjectSettingsEnabledInSession(project.get());
		for (FieldEditor editor : getEditors()) {
			Composite parent = getEditorParent(editor);
			editor.setEnabled(enabled, parent);
			if (!editor.getPreferenceStore().equals(getPreferenceStore())) {
				editor.setPreferenceStore(getPreferenceStore());
				editor.load();
			}
		}
	}

	protected void enableButtons() {
		boolean enabled = !project.isPresent() || Activator.isProjectSettingsEnabledInSession(project.get());
		if (getApplyButton() != null) {
			getApplyButton().setEnabled(enabled);
		}
		if (getDefaultsButton() != null) {
			getDefaultsButton().setEnabled(enabled);
		}
	}

	protected Composite getEditorParent(FieldEditor editor) {
		return editorParents.get(editor);
	}

	protected Optional<IProject> getProject() {
		return project;
	}

	private Link createLink(Composite composite, String text) {
		Link link = new Link(composite, SWT.NONE);
		link.setFont(composite.getFont());
		link.setText("<A>" + text + "</A>");
		link.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String id = SQLInspectPreferencePage.PREFERENCE_PAGE_ID;
				int result = PreferencesUtil.createPreferenceDialogOn(getShell(), id, null, null).open();
				if (result == Window.OK) {
					enableAndUpdateEditors();
					enableButtons();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				widgetSelected(event);
			}
		});
		link.setToolTipText("Configure global workspace (user) settings");
		return link;
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			initPreferencesStore();
			enableAndUpdateEditors();
			enableButtons();
		}
		super.setVisible(visible);
	}

}