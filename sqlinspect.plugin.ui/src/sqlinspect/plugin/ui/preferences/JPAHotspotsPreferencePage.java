package sqlinspect.plugin.ui.preferences;

import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotParameterSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotRegexpSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotSignatureDialog;
import sqlinspect.plugin.ui.preferences.fieldeditors.StringListFieldEditor;

public class JPAHotspotsPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferences.JPAHotspotsPreferencePage";

	public JPAHotspotsPreferencePage() {
		super();
		setDescription("JPA Hotspots");
	}

	@Override
	protected void createEditors(Composite parent) {
		StringListFieldEditor jpaCreateQueryEditor = new StringListFieldEditor(PreferenceConstants.JPA_QUERY_CREATE,
				"EntityManager.createQuery signatures", HotspotParameterSignatureDialog.class, parent);
		addField(jpaCreateQueryEditor, parent);

		StringListFieldEditor jpaQueryExecuteEditor = new StringListFieldEditor(PreferenceConstants.JPA_QUERY_EXEC,
				"Query.execute signatures", HotspotSignatureDialog.class, parent);
		addField(jpaQueryExecuteEditor, parent);

		StringListFieldEditor jpaQuerySetEditor = new StringListFieldEditor(PreferenceConstants.JPA_PSTMT_SETTER,
				"Query.set signatures", HotspotRegexpSignatureDialog.class, parent);
		addField(jpaQuerySetEditor, parent);
	}
}