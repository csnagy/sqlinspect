package sqlinspect.plugin.ui.preferences;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.IWorkbenchPreferenceContainer;

public class SQLInspectPreferencePage extends AbstractPreferencePage implements IWorkbenchPreferencePage {

	public static final String PREFERENCE_PAGE_ID = "sqlinspect.plugin.ui.preferences.SQLInspectPreferencePage";

	public SQLInspectPreferencePage() {
		super();
		setDescription("General SQLInspect Settings");
	}

	@Override
	protected void createEditors(Composite parent) {
		if (getProject().isPresent()) {
			createWorkspaceButtons(parent);
		}

		createLabel(parent);

		createSQLAnLink(parent);

		createSQLExtractorLink(parent);

		noDefaultAndApplyButton();
		Dialog.applyDialogFont(parent);
	}

	private void createSQLExtractorLink(Composite parent) {
		Link linkSQLExtractor = new Link(parent, SWT.NONE);
		GridDataFactory.fillDefaults().span(GRID_COLS, 1).applyTo(linkSQLExtractor);
		linkSQLExtractor.setText("  - <a>SQL Extractor</a> - Settings of the SQL extractor.");
		linkSQLExtractor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				IWorkbenchPreferenceContainer container = (IWorkbenchPreferenceContainer) getContainer();
				if (getProject().isPresent()) {
					container.openPage(SQLExtractorPreferencePage.PROPERTIES_PAGE_ID, null);
				} else {
					container.openPage(SQLExtractorPreferencePage.PREFERENCE_PAGE_ID, null);
				}
			}
		});
	}

	private void createSQLAnLink(Composite parent) {
		Link linkSQLAn = new Link(parent, SWT.NONE);
		GridDataFactory.fillDefaults().span(GRID_COLS, 1).applyTo(linkSQLAn);
		linkSQLAn.setText("  - <a>SQL Analyzer</a> - Settings of the SQL parser and analysis.");
		linkSQLAn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				IWorkbenchPreferenceContainer container = (IWorkbenchPreferenceContainer) getContainer();
				if (getProject().isPresent()) {
					container.openPage(SQLAnPreferencePage.PROPERTIES_PAGE_ID, null);
				} else {
					container.openPage(SQLAnPreferencePage.PREFERENCE_PAGE_ID, null);
				}
			}
		});
	}

	private void createLabel(Composite parent) {
		Label label = new Label(parent, SWT.WRAP);
		GridDataFactory.fillDefaults().span(GRID_COLS, 1).applyTo(label);
		if (getProject().isPresent()) {
			label.setText("Project-specific settings of the plugin can be configured under: ");
		} else {
			label.setText("Workspace settings of the plugin can be configured under: ");
		}
	}

	@Override
	public void init(IWorkbench workbench) {
		// no need to do anything here by default
	}
}