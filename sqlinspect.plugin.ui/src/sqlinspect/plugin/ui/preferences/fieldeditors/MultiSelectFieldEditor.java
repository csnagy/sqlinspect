package sqlinspect.plugin.ui.preferences.fieldeditors;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import sqlinspect.plugin.preferences.PreferenceHelper;

public class MultiSelectFieldEditor extends FieldEditor {

	private Composite top;

	private CheckboxTableViewer checkboxTableViewer;

	public MultiSelectFieldEditor(String name, String labelText, List<String> elements, Composite parent) {
		super(name, labelText, parent);
		addElements(elements);
	}

	@Override
	protected void adjustForNumColumns(int numColumns) {
		((GridData) top.getLayoutData()).horizontalSpan = numColumns;
	}

	@Override
	protected void doFillIntoGrid(Composite parent, int numColumns) {
		top = parent;
		GridDataFactory.fillDefaults().span(1, 1).applyTo(top);

		Label label = getLabelControl(top);
		GridDataFactory.fillDefaults().span(1, 1).applyTo(label);

		checkboxTableViewer = CheckboxTableViewer.newCheckList(top, SWT.MULTI | SWT.CHECK | SWT.BORDER);

		GridDataFactory.fillDefaults().grab(true, true).span(numColumns - 1, 1).hint(150, 300)
				.applyTo(checkboxTableViewer.getTable());
	}

	private void addElements(List<String> elements) {
		checkboxTableViewer.add(elements.toArray());
	}

	@Override
	protected void doLoad() {
		String items = getPreferenceStore().getString(getPreferenceName());
		selectItems(PreferenceHelper.parseStringList(items));
	}

	@Override
	protected void doLoadDefault() {
		String items = getPreferenceStore().getDefaultString(getPreferenceName());
		selectItems(PreferenceHelper.parseStringList(items));
	}

	@Override
	protected void doStore() {
		List<String> selectedItems = Arrays.asList(checkboxTableViewer.getCheckedElements()).stream()
				.map(String.class::cast).toList();
		String s = PreferenceHelper.createStringList(selectedItems);
		if (s != null) {
			getPreferenceStore().setValue(getPreferenceName(), s);
		}
	}

	@Override
	public int getNumberOfControls() {
		return 2;
	}

	@Override
	public void setEnabled(boolean enabled, Composite parent) {
		top.setEnabled(enabled);
		checkboxTableViewer.getControl().setEnabled(enabled);
		super.setEnabled(enabled, parent);
	}

	private void selectItems(List<String> items) {
		checkboxTableViewer.setCheckedElements(items.toArray());
	}

}
