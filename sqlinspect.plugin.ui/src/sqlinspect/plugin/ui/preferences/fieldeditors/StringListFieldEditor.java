package sqlinspect.plugin.ui.preferences.fieldeditors;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.preferences.PreferenceHelper;

public class StringListFieldEditor extends ListEditor {

	private static final Logger LOG = LoggerFactory.getLogger(StringListFieldEditor.class);

	private final Class<? extends InputDialog> inputDialogClass;

	public StringListFieldEditor(String name, String labelText, Class<? extends InputDialog> inputDialogClass,
			Composite parent) {
		super(name, labelText, parent);
		this.inputDialogClass = inputDialogClass;
	}

	@Override
	protected String createList(String[] items) {
		return PreferenceHelper.createStringList(items);
	}

	@Override
	protected String getNewInputObject() {
		try {
			InputDialog inputDialog = inputDialogClass.getDeclaredConstructor(getShell().getClass())
					.newInstance(getShell());
			inputDialog.create();

			if (inputDialog.open() == Window.OK) {
				return inputDialog.getValue();
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			LOG.error("Exception", e);
		}

		// dialog was canceled
		return null;
	}

	@Override
	protected String[] parseString(String stringList) {
		return PreferenceHelper.parseStringArray(stringList);
	}

}
