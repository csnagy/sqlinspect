package sqlinspect.plugin.ui.preferences;

import org.eclipse.swt.widgets.Composite;

public class HotspotFindersPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferences.HotspotFinderPreferencePage";

	public HotspotFindersPreferencePage() {
		super();
		setDescription("Hotspot Finders' Settings");
	}

	@Override
	protected void createEditors(Composite parent) {
		// nothing to do here
	}
}