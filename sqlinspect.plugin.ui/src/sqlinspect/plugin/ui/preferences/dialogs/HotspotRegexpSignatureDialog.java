package sqlinspect.plugin.ui.preferences.dialogs;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Shell;

import sqlinspect.plugin.extractors.JDBCHotspotFinder;

public class HotspotRegexpSignatureDialog extends InputDialog {

	private static class HotspotRegexpSignatureInputValidator implements IInputValidator {
		@Override
		public String isValid(String newText) {
			if (newText.isBlank()) {
				return "Specify a signature.";
			}
			return null; // valid signature
		}
	}

	public HotspotRegexpSignatureDialog(Shell parent) {
		super(parent, "Add a hotspot signature",
				"Format: regular expression (e.g., \"" + JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_SETTER[0] + "\")", "",
				new HotspotRegexpSignatureInputValidator());
	}
}
