package sqlinspect.plugin.ui.preferences.dialogs;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Shell;

import sqlinspect.plugin.extractors.JDBCHotspotFinder;

public class HotspotParameterSignatureDialog extends InputDialog {

	private static class HotspotSignatureInputValidator implements IInputValidator {
		@Override
		public String isValid(String newText) {
			if (newText.isBlank()) {
				return "Specify a signature.";
			}
			try {
				String[] parts = newText.split("\\/");
				if (parts.length != 2) {
					return "A parameter to be traced must be specified!";
				} else if (Integer.parseInt(parts[1]) < 0) {
					return "Parameter number cannot be negative.";
				}
			} catch (NumberFormatException e) {
				return "Invalid parameter number.";
			}
			return null; // valid signature
		}
	}

	public HotspotParameterSignatureDialog(Shell parent) {
		super(parent, "Add a hotspot signature",
				"Format: methodsignature/parameternumber\n"
						+ "- methodsignature: Fully qualified signature of the method\n"
						+ "- parameternumber: The number of the parameter to be traced (usually a String)\n"
						+ "  (Write '0' if the receiver object should be traced back instead of a parameter.)\n"
						+ "Example: \"" + JDBCHotspotFinder.DEFAULT_JDBC_STMT_EXECUTE[0] + "\n",
				"", new HotspotSignatureInputValidator());
	}
}
