package sqlinspect.plugin.ui.preferences.dialogs;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Shell;

import sqlinspect.plugin.extractors.JDBCHotspotFinder;

public class HotspotSignatureDialog extends InputDialog {

	private static class HotspotSignatureInputValidator implements IInputValidator {
		@Override
		public String isValid(String newText) {
			if (newText.isBlank()) {
				return "Specify a signature.";
			}
			return null; // valid signature
		}
	}

	public HotspotSignatureDialog(Shell parent) {
		super(parent, "Add a hotspot signature",
				"Format: methodsignature (e.g., \"" + JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_EXECUTE[0] + "\")", "",
				new HotspotSignatureInputValidator());
	}
}
