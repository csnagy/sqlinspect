package sqlinspect.plugin.ui.preferences;

import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotParameterSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotRegexpSignatureDialog;
import sqlinspect.plugin.ui.preferences.fieldeditors.StringListFieldEditor;

public class AndroidHotspotsPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferences.AndroidHotspotsPreferencePage";

	public AndroidHotspotsPreferencePage() {
		super();
		setDescription("Android Hotspots");
	}

	@Override
	protected void createEditors(Composite parent) {
		StringListFieldEditor andrExecSQLEditor = new StringListFieldEditor(PreferenceConstants.ANDR_EXECSQL,
				"SQLiteDatabase.execSQL signatures", HotspotParameterSignatureDialog.class, parent);
		addField(andrExecSQLEditor, parent);

		StringListFieldEditor andrStmtExecEditor = new StringListFieldEditor(PreferenceConstants.ANDR_STMT_EXEC,
				"SQLiteStatement.execute signatures", HotspotParameterSignatureDialog.class, parent);
		addField(andrStmtExecEditor, parent);

		StringListFieldEditor andrCompileStmtEditor = new StringListFieldEditor(PreferenceConstants.ANDR_COMPILE_STMT,
				"SQLiteDatabase.compileStatement signatures", HotspotParameterSignatureDialog.class, parent);
		addField(andrCompileStmtEditor, parent);

		StringListFieldEditor andrCursorRawqueryEditor = new StringListFieldEditor(
				PreferenceConstants.ANDR_CURSOR_RAWQUERY, "SQLiteDatabase.rawQuery signatures",
				HotspotParameterSignatureDialog.class, parent);
		addField(andrCursorRawqueryEditor, parent);

		StringListFieldEditor andrPstmtSetterEditor = new StringListFieldEditor(
				PreferenceConstants.ANDR_PSTMT_SETTER, "SQLiteProgram.bind signatures",
				HotspotRegexpSignatureDialog.class, parent);
		addField(andrPstmtSetterEditor, parent);
	}
}