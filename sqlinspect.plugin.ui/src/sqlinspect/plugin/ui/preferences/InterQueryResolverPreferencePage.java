package sqlinspect.plugin.ui.preferences;

import java.util.stream.Stream;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.extractors.LoopHandling;
import sqlinspect.plugin.preferences.PreferenceConstants;

public class InterQueryResolverPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferences.InterQueryResolverPreferencePage";

	public InterQueryResolverPreferencePage() {
		super();
		setDescription("Interpocedural Query Resolver Settings");
	}

	@Override
	protected void createEditors(Composite parent) {
		IntegerFieldEditor maxCFGDepthFieldEditor = new IntegerFieldEditor(PreferenceConstants.INTERSR_MAX_CFG_DEPTH,
				"Max. CFG depth", parent);
		addField(maxCFGDepthFieldEditor, parent);

		IntegerFieldEditor maxCallDepthFieldEditor = new IntegerFieldEditor(PreferenceConstants.INTERSR_MAX_CALL_DEPTH,
				"Max. call depth", parent);
		addField(maxCallDepthFieldEditor, parent);

		String[][] options = Stream.of(LoopHandling.values()).map(lh -> new String[] { lh.toString(), lh.toString() })
				.toArray(String[][]::new);
		ComboFieldEditor loopHandlingField = new ComboFieldEditor(PreferenceConstants.INTERSR_LOOPHANDLING,
				"Loop handling", options, parent);
		addField(loopHandlingField, parent);
	}
}