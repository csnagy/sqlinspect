package sqlinspect.plugin.ui.preferences;

import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotParameterSignatureDialog;
import sqlinspect.plugin.ui.preferences.fieldeditors.StringListFieldEditor;

public class SpringHotspotsPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferencesSpringHotspotsPreferencePage";

	public SpringHotspotsPreferencePage() {
		super();
		setDescription("Spring Hotspots");
	}

	@Override
	protected void createEditors(Composite parent) {
		StringListFieldEditor springSignaturesEditor = new StringListFieldEditor(
				PreferenceConstants.SPRING_HIBERNATETEMPLATE_HOTSPOTS, "HibernateTemplate.find/iterate signatures",
				HotspotParameterSignatureDialog.class, parent);
		addField(springSignaturesEditor, parent);
	}
}