package sqlinspect.plugin.ui.preferences;

import java.util.List;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.extractors.HotspotFinderFactory;
import sqlinspect.plugin.extractors.QueryResolverFactory;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.ui.preferences.fieldeditors.MultiSelectFieldEditor;

public class SQLExtractorPreferencePage extends AbstractPreferencePage {

	public static final String PREFERENCE_PAGE_ID = "sqlinspect.plugin.ui.preferences.SQLExtractorPreferencePage";
	public static final String PROPERTIES_PAGE_ID = "sqlinspect.plugin.ui.properties.SQLExtractorPreferencePage";

	public SQLExtractorPreferencePage() {
		super();
		setDescription("SQL Extractor Settings");
	}

	@Override
	protected void createEditors(Composite parent) {
		createHotspotFinder(parent);
		createStringResolver(parent);
	}

	private void createStringResolver(Composite parent) {
		String[][] resolvers = QueryResolverFactory.getQueryResolvers().stream()
				.map(qr -> new String[] { qr.getSimpleName(), qr.getSimpleName() }).toArray(String[][]::new);
		ComboFieldEditor stringResolverField = new ComboFieldEditor(PreferenceConstants.STRING_RESOLVER,
				"Query resolver", resolvers, parent);
		addField(stringResolverField, parent);
	}

	private void createHotspotFinder(Composite parent) {
		List<String> hotspotFinders = HotspotFinderFactory.getHotspotFindersAsStringList();
		MultiSelectFieldEditor hotspotFinderField = new MultiSelectFieldEditor(PreferenceConstants.HOTSPOT_FINDERS,
				"Hotspot finders", hotspotFinders, parent);
		addField(hotspotFinderField, parent);
	}
}