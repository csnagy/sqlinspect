package sqlinspect.plugin.ui.preferences;

import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotParameterSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotRegexpSignatureDialog;
import sqlinspect.plugin.ui.preferences.dialogs.HotspotSignatureDialog;
import sqlinspect.plugin.ui.preferences.fieldeditors.StringListFieldEditor;

public class HibernateHotspotsPreferencePage extends AbstractPreferencePage {

	public static final String PAGE_ID = "sqlinspect.plugin.ui.preferences.HibernateHotspotsPreferencePage";

	public HibernateHotspotsPreferencePage() {
		super();
		setDescription("Hibernate Hotspots");
	}

	@Override
	protected void createEditors(Composite parent) {
		StringListFieldEditor hibernateSessionCreateQueryEditor = new StringListFieldEditor(
				PreferenceConstants.HIBERNATE_QUERY_CREATE, "Session.createQuery signatures",
				HotspotParameterSignatureDialog.class, parent);
		addField(hibernateSessionCreateQueryEditor, parent);

		StringListFieldEditor hibernateQueryExecuteEditor = new StringListFieldEditor(
				PreferenceConstants.HIBERNATE_QUERY_EXEC, "Query.execute signatures", HotspotSignatureDialog.class,
				parent);
		addField(hibernateQueryExecuteEditor, parent);

		StringListFieldEditor hibernateQuerySetEditor = new StringListFieldEditor(
				PreferenceConstants.HIBERNATE_PSTMT_SETTER, "Query.set signatures", HotspotRegexpSignatureDialog.class,
				parent);
		addField(hibernateQuerySetEditor, parent);
	}
}