package sqlinspect.plugin.ui.views;

import javax.inject.Inject;

import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.ui.utils.ImageUtils;
import sqlinspect.sql.asg.base.Named;
import sqlinspect.sql.asg.base.SQLRoot;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.util.ASGUtils;

public class SchemaView extends AbstractSQLInspectViewer {

	public static final String ID = SchemaView.class.getName();
	public static final String MENU_ID = "sqlinspect.plugin.ui.popupmenu.SchemaView";

	public static final Object[] NO_ELEMENTS = {};

	@Inject
	private EMenuService menuService;

	@Inject
	private ProjectRepository projectRepository;

	@Inject
	private ASGRepository asgRepository;

	private static class ViewContentProvider implements ITreeContentProvider {
		private final ProjectRepository projectRepository;
		private final ASGRepository asgRepository;

		public ViewContentProvider(ProjectRepository projectRepository, ASGRepository asgRepository) {
			super();
			this.projectRepository = projectRepository;
			this.asgRepository = asgRepository;
		}

		@Override
		public Object[] getElements(Object parent) {
			if (parent instanceof ProjectRepository projectRepositoryElement) {
				return projectRepositoryElement.getProjects().toArray();
			} else {
				return NO_ELEMENTS;
			}
		}

		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof Project project) {
				return asgRepository.getASG(project).getRoot().getDatabases().toArray();
			} else if (element instanceof Database database) {
				return database.getSchemas().toArray();
			} else if (element instanceof Schema schema) {
				return schema.getTables().toArray();
			} else if (element instanceof Table table) {
				return table.getColumns().toArray();
			}
			return NO_ELEMENTS;
		}

		private Project getDatabaseParent(Database db) {
			SQLRoot dbParent = (SQLRoot) db.getParent();
			for (Project project : projectRepository.getProjects()) {
				if (asgRepository.getASG(project).getRoot().equals(dbParent)) {
					return project;
				}
			}
			return null;
		}

		@Override
		public Object getParent(Object element) {
			if (element instanceof Database database) {
				return getDatabaseParent(database);
			} else if (element instanceof Schema schema) {
				return schema.getParent();
			} else if (element instanceof Table table) {
				return table.getParent();
			}
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Project project) {
				return !asgRepository.getASG(project).getRoot().getDatabases().isEmpty();
			} else if (element instanceof Database database) {
				return !database.getSchemas().isEmpty();
			} else if (element instanceof Schema schema) {
				return !schema.getTables().isEmpty();
			} else if (element instanceof Table table) {
				return !table.getColumns().isEmpty();
			}
			return false;
		}
	}

	private class ViewNameLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object element) {
			if (element instanceof Project) {
				return ImageUtils.getIcon(ImageUtils.TEXT_FILE);
			} else if (element instanceof Database) {
				return ImageUtils.getIcon(ImageUtils.DATABASE);
			} else if (element instanceof Schema) {
				return ImageUtils.getIcon(ImageUtils.DATABASE5);
			} else if (element instanceof Table) {
				return ImageUtils.getIcon(ImageUtils.LIST_VIEW);
			} else {
				return null;
			}
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Project project) {
				StyledString ret = new StyledString(project.getName());
				if (asgRepository.getASG(project).getRoot() != null) {
					ret.append(" (" + asgRepository.getASG(project).getRoot().getDatabases().size() + ")",
							StyledString.COUNTER_STYLER);
				}
				return ret;
			} else if (element instanceof Database database) {
				StyledString ret = new StyledString(database.getName());
				ret.append(" (" + database.getSchemas().size() + ")", StyledString.COUNTER_STYLER);
				return ret;
			} else if (element instanceof Schema schema) {
				StyledString ret = new StyledString(schema.getName());
				ret.append(" (" + schema.getTables().size() + ")", StyledString.COUNTER_STYLER);
				return ret;
			} else if (element instanceof Table table) {
				StyledString ret = new StyledString(table.getName());
				ret.append(" (" + table.getColumns().size() + ")", StyledString.COUNTER_STYLER);
				return ret;
			} else if (element instanceof Named named) {
				return new StyledString(named.getName());
			} else {
				return new StyledString("NA");
			}
		}
	}

	private static class ViewTypeLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object arg0) {
			return null;
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Column column) {
				return new StyledString(ASGUtils.columnTypeToString(column));
			} else {
				return new StyledString("");
			}
		}
	}

	private static final class SchemaViewerComparator extends ViewerComparator {
		private static final int DESCENDING = 1;
		private static final int ASCENDING = 0;
		private int direction;
		private int propertyIndex;

		public SchemaViewerComparator() {
			super();
			propertyIndex = 0;
			direction = ASCENDING;
		}

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = ASCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (propertyIndex == 0 && viewer instanceof TreeViewer treeViewer) {
				IBaseLabelProvider blp = treeViewer.getLabelProvider(propertyIndex);
				if (blp instanceof DelegatingStyledCellLabelProvider dlp) {
					IStyledLabelProvider lp = dlp.getStyledStringProvider();
					StyledString s1 = lp.getStyledText(e1);
					StyledString s2 = lp.getStyledText(e2);
					if (s1 != null && s2 != null) {
						ret = s1.getString().compareTo(s2.getString());
					}
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}
	}

	@Override
	public Viewer createViewer(Composite parent) {
		TreeViewer viewer = new TreeViewer(parent);
		viewer.setContentProvider(new ViewContentProvider(projectRepository, asgRepository));
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		TreeViewerColumn viewerColumn = new TreeViewerColumn(viewer, SWT.LEFT);
		viewerColumn.getColumn().setWidth(600);
		viewerColumn.getColumn().setText("Name");
		viewerColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewNameLabelProvider()));
		viewerColumn.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn, 0));

		TreeViewerColumn viewerColumn2 = new TreeViewerColumn(viewer, SWT.NONE);
		viewerColumn2.getColumn().setWidth(150);
		viewerColumn2.getColumn().setText("Type");
		viewerColumn2.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewTypeLabelProvider()));

		viewer.setInput(projectRepository);
		viewer.setComparator(new SchemaViewerComparator());

		createContextMenu(viewer);

		return viewer;
	}

	private void createContextMenu(TreeViewer viewer) {
		menuService.registerContextMenu(viewer.getControl(), MENU_ID);
	}

	private SelectionAdapter getSelectionAdapter(final TreeViewerColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeViewer viewer = (TreeViewer) getViewer();
				SchemaViewerComparator comparator = (SchemaViewerComparator) viewer.getComparator();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTree().setSortColumn(column.getColumn());
				viewer.getTree().setSortDirection(dir);
				viewer.refresh();
			}
		};
	}

}
