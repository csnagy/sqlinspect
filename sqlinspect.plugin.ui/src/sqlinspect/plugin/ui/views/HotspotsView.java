package sqlinspect.plugin.ui.views;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.ui.JavaElementComparator;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jdt.ui.StandardJavaElementContentProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.model.QueryPart;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.ui.utils.EditorUtils;
import sqlinspect.plugin.ui.utils.ImageUtils;
import sqlinspect.plugin.utils.ASTUtils;

public class HotspotsView extends AbstractSQLInspectViewer {

	public static final String ID = HotspotsView.class.getName();
	public static final String MENU_ID = "sqlinspect.plugin.ui.popupmenu.HotspotsView";

	private static final Logger LOG = LoggerFactory.getLogger(HotspotsView.class);

	private static final int COL_IND_NAME = 0;
	private static final int COL_IND_FILE = 1;
	private static final int COL_IND_LINE = 2;

	private Action doubleClickAction;

	@Inject
	private ProjectRepository projectRepository;

	@Inject
	private HotspotRepository hotspotRepository;

	@Inject
	private MetricRepository metricRepository;

	@Inject
	private EMenuService menuService;

	private static class ViewContentProvider implements ITreeContentProvider {
		private final ProjectRepository projectRepository;
		private final HotspotRepository hotspotRepository;
		private final MetricRepository metricRepository;

		private final StandardJavaElementContentProvider standardProvider = new StandardJavaElementContentProvider();

		public ViewContentProvider(ProjectRepository projectRepository, HotspotRepository hotspotRepository, MetricRepository metricRepository) {
			this.projectRepository = projectRepository;
			this.hotspotRepository = hotspotRepository;
			this.metricRepository = metricRepository;
		}

		@Override
		public Object[] getElements(Object input) {
			if (input instanceof ProjectRepository projectRepositoryInput) {
				List<IJavaElement> ret = new ArrayList<>();
				if (projectRepositoryInput.getProjects() != null) {
					for (Project proj : projectRepositoryInput.getProjects()) {
						ret.add(proj.getIJavaProject());
					}
				}
				return ret.toArray();
			} else {
				return new Object[0];
			}
		}

		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof ICompilationUnit icu) {
				Project proj = projectRepository.getProject(icu.getJavaProject().getProject());
				if (proj != null) {
					List<Hotspot> hs = hotspotRepository.getHotspots(proj, icu);
					return hs.toArray();
				}
			} else if (element instanceof IJavaElement) {
				List<IJavaElement> ret = new ArrayList<>();
				for (Object retElement : standardProvider.getChildren(element)) {
					if (retElement instanceof IJavaElement javaElement) {
						IJavaProject javaProject = javaElement.getJavaProject();
						Project proj = projectRepository.getProject(javaProject.getProject());

						Object m = metricRepository.getJavaMetrics(proj).get(javaElement);
						if (m != null) {
							ret.add(javaElement);
						}
					}
				}
				return ret.toArray();
			} else if (element instanceof Hotspot hotspot) {
				List<Query> queries = hotspot.getQueries();
				if (queries != null) {
					return queries.toArray();
				}
			} else if (element instanceof Query query) {
				return new QueryPart[] { query.getRoot() };
			} else if (element instanceof QueryPart queryPart) {
				return queryPart.getChildren().toArray();

			}
			return new Object[0];
		}

		@Override
		public Object getParent(Object element) {
			if (element instanceof Hotspot hotspot) {
				return hotspot.getProject();
			} else if (element instanceof Query query) {
				return query.getHotspot();
			} else if (element instanceof QueryPart querypart) {
				return querypart.getParent();
			} else if (standardProvider.getParent(element) != null) {
				return standardProvider.getParent(element);
			} else {
				return null;
			}
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof ProjectRepository projectRepositoryElement) {
				return !projectRepositoryElement.getProjects().isEmpty();
			} else if (element instanceof ICompilationUnit icu) {
				Project proj = projectRepository.getProject(icu.getJavaProject().getProject());
				if (proj != null) {
					return !hotspotRepository.getHotspots(proj, icu).isEmpty();
				}
				return false;
			} else if (element instanceof IJavaElement) {
				return standardProvider.hasChildren(element);
			} else if (element instanceof Hotspot hotspot) {
				return !hotspot.getQueries().isEmpty();
			} else if (element instanceof Query query) {
				return query.getRoot() != null;
			} else if (element instanceof QueryPart queryPart) {
				return !queryPart.getChildren().isEmpty();
			} else {
				return false;
			}
		}
	}

	private static class ViewNameLabelProvider extends JavaElementLabelProvider {
		@Override
		public Image getImage(Object element) {
			if (element instanceof Hotspot) {
				return ImageUtils.getIcon(ImageUtils.DATA_BACKUP);
			} else if (element instanceof IJavaElement) {
				return super.getImage(element);
			} else {
				return null;
			}
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof IJavaElement) {
				return super.getStyledText(element);
			} else if (element instanceof Hotspot hotspot) {
				ASTNode exec = hotspot.getExec();
				if (exec != null) {
					ICompilationUnit unit = ASTUtils.getICompilationUnit(hotspot.getExec());
					if (unit != null) {
						return new StyledString(unit.getElementName() + ":" + hotspot.getStartLine());
					}
				}
				return new StyledString("");
			} else if (element instanceof Query query) {
				return new StyledString(query.getValue());
			} else if (element instanceof QueryPart queryPart) {
				return new StyledString(queryPart.getShortDesc(20, true) + ": ").append(queryPart.getValue(),
						StyledString.DECORATIONS_STYLER);
			} else {
				return new StyledString(element.toString());
			}
		}
	}

	private static class ViewFileLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object arg0) {
			return null;
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Project) {
				return new StyledString("");
			} else if (element instanceof Hotspot hotspot) {
				String text = ((CompilationUnit) hotspot.getExec().getRoot()).getJavaElement().getElementName();
				return new StyledString(text);
			} else if (element instanceof Query query) {
				QueryPart queryPart = query.getRoot();
				return new StyledString(
						ASTUtils.getCompilationUnit(queryPart.getNode()).getJavaElement().getElementName());
			} else if (element instanceof QueryPart queryPart) {
				return new StyledString(
						ASTUtils.getCompilationUnit(queryPart.getNode()).getJavaElement().getElementName());
			} else {
				return new StyledString("");
			}
		}
	}

	private static class ViewHotspotFinderLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object arg0) {
			return null;
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Hotspot hotspot) {
				String text = hotspot.getHotspotFinder().getName();
				return new StyledString(text);
			} else {
				return new StyledString("");
			}
		}
	}

	private static final class HotspotsViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;
		private final JavaElementComparator javaComparator = new JavaElementComparator();

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (propertyIndex == COL_IND_NAME) {
				ret = javaComparator.compare(viewer, e1, e2);
			} else if (propertyIndex == COL_IND_LINE) {
				if (viewer instanceof TreeViewer treeViewer) {
					IBaseLabelProvider blp = treeViewer.getLabelProvider(propertyIndex);
					if (blp instanceof DelegatingStyledCellLabelProvider dlp) {
						IStyledLabelProvider lp = dlp.getStyledStringProvider();
						String s1 = lp.getStyledText(e1).getString();
						String s2 = lp.getStyledText(e2).getString();
						try {
							if (s1 != null && !s1.isEmpty() && s2 != null && !s2.isEmpty()) {
								ret = Integer.compare(Integer.parseInt(s1), Integer.parseInt(s2));
							}
						} catch (NumberFormatException e) {
							LOG.error("Error parsing line number under Hotspots Viewer", e);
						}
					}
				}
			} else {
				// comparing string values, case sensitive
				ret = super.compare(viewer, e1, e2);
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}

	}

	private static class ViewLineLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object arg0) {
			return null;
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Project) {
				return new StyledString("");
			} else if (element instanceof Hotspot hotspot) {
				String text = Integer.toString(ASTUtils.getLineNumber(hotspot.getExec()));
				return new StyledString(text);
			} else if (element instanceof Query query) {
				QueryPart queryPart = query.getRoot();
				return new StyledString(Integer.toString(ASTUtils.getLineNumber(queryPart.getNode())));
			} else if (element instanceof QueryPart queryPart) {
				return new StyledString(Integer.toString(ASTUtils.getLineNumber(queryPart.getNode())));
			} else {
				return new StyledString("");
			}
		}

	}

	@Override
	public Viewer createViewer(Composite parent) {
		final TreeViewer viewer = new TreeViewer(parent);
		viewer.setContentProvider(new ViewContentProvider(projectRepository, hotspotRepository, metricRepository));
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		final TreeViewerColumn viewerColumn = new TreeViewerColumn(viewer, SWT.NONE);
		viewerColumn.getColumn().setWidth(600);
		viewerColumn.getColumn().setText("Name");
		viewerColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewNameLabelProvider()));
		viewerColumn.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn, COL_IND_NAME));

		final TreeViewerColumn viewerColumn2 = new TreeViewerColumn(viewer, SWT.NONE);
		viewerColumn2.getColumn().setWidth(150);
		viewerColumn2.getColumn().setText("File");
		viewerColumn2.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewFileLabelProvider()));
		viewerColumn2.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn2, COL_IND_FILE));

		final TreeViewerColumn viewerColumn3 = new TreeViewerColumn(viewer, SWT.NONE);
		viewerColumn3.getColumn().setWidth(50);
		viewerColumn3.getColumn().setText("Line");
		viewerColumn3.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewLineLabelProvider()));
		viewerColumn3.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn3, COL_IND_LINE));

		final TreeViewerColumn viewerColumn4 = new TreeViewerColumn(viewer, SWT.NONE);
		viewerColumn4.getColumn().setWidth(50);
		viewerColumn4.getColumn().setText("HotspotFinder");
		viewerColumn4.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewHotspotFinderLabelProvider()));
		viewerColumn4.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn4, COL_IND_LINE));

		viewer.setInput(projectRepository);

		viewer.setComparator(new HotspotsViewerComparator());

		createContextMenu(viewer);
		hookDoubleClickAction(viewer);

		return viewer;
	}

	private void createContextMenu(TreeViewer viewer) {
		menuService.registerContextMenu(viewer.getControl(), MENU_ID);
	}

	@Override
	protected void makeActions() {
		doubleClickAction = new Action() {
			@Override
			public void run() {
				doubleClick();
			}
		};
	}

	public void expandAll() {
		TreeViewer viewer = (TreeViewer) getViewer();
		// workaround of mouse pointer flickering
		Runnable task = () -> {
			try {
				viewer.getTree().setRedraw(false);
				viewer.expandAll();
			} finally {
				viewer.getTree().setRedraw(true);
			}
		};
		BusyIndicator.showWhile(Display.getCurrent(), task);
	}

	public void expandToHotspots() {
		TreeViewer viewer = (TreeViewer) getViewer();
		// workaround of mouse pointer flickering
		Runnable task = () -> {
			try {
				viewer.getTree().setRedraw(false);
				viewer.expandToLevel(5); // project,package,class,hotspot
			} finally {
				viewer.getTree().setRedraw(true);
			}
		};
		BusyIndicator.showWhile(Display.getCurrent(), task);
	}

	public void expandToQueries() {
		TreeViewer viewer = (TreeViewer) getViewer();
		// workaround of mouse pointer flickering
		Runnable task = () -> {
			try {
				viewer.getTree().setRedraw(false);
				viewer.expandToLevel(6); // project,package,class,hotspot,query
			} finally {
				viewer.getTree().setRedraw(true);
			}
		};
		BusyIndicator.showWhile(Display.getCurrent(), task);
	}

	public void collapseAll() {
		TreeViewer viewer = (TreeViewer) getViewer();
		viewer.collapseAll();
	}

	private void hookDoubleClickAction(TreeViewer viewer) {
		IDoubleClickListener doubleClickListener = event -> doubleClickAction.run();
		viewer.addDoubleClickListener(doubleClickListener);
	}

	private void selectQueryInEditor() {
		ISelection selection = getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof Query query) {
			Hotspot hotspot = query.getHotspot();
			selectHotspotInEditor(hotspot);
		} else {
			LOG.error("Selection must be a query!");
		}
	}

	private void selectHotspotInEditor() {
		ISelection selection = getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof Hotspot hotspot) {
			selectHotspotInEditor(hotspot);
		} else {
			LOG.error("Selection must be a hotspot!");
		}
	}

	private void selectHotspotInEditor(Hotspot hs) {
		ASTNode node = hs.getExec();
		if (node != null) {
			int offset = node.getStartPosition();
			int length = node.getLength();
			CompilationUnit cu = hs.getUnit();
			String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
			IPath path = Path.fromOSString(sourcePath);
			IEditorPart editorPart = EditorUtils.openEditor(path);
			if (editorPart instanceof ITextEditor textEditor) {
				EditorUtils.selectInEditor(textEditor, offset, length);
			}
		}
	}

	private void selectQueryPartInEditor() {
		ISelection selection = getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof QueryPart queryPart) {
			ASTNode node = queryPart.getNode();
			if (node != null) {
				int offset = node.getStartPosition();
				int length = node.getLength();
				CompilationUnit cu = ASTUtils.getCompilationUnit(node);
				String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
				IPath path = Path.fromOSString(sourcePath);
				IEditorPart editorPart = EditorUtils.openEditor(path);
				if (editorPart instanceof ITextEditor textEditor) {
					EditorUtils.selectInEditor(textEditor, offset, length);
				}
			}

		} else {
			LOG.error("Selection must be a query part!");
		}
	}

	private void doubleClick() {
		ISelection selection = getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof QueryPart) {
			selectQueryPartInEditor();
		} else if (obj instanceof Query) {
			selectQueryInEditor();
		} else if (obj instanceof Hotspot) {
			selectHotspotInEditor();
		}
	}

	public void select(List<?> elements) {
		StructuredSelection ssel = new StructuredSelection(elements);
		getViewer().setSelection(ssel);
	}

	public void select(Object element) {
		StructuredSelection ssel = new StructuredSelection(element);
		getViewer().setSelection(ssel);
	}

	private SelectionAdapter getSelectionAdapter(final TreeViewerColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeViewer viewer = (TreeViewer) getViewer();
				HotspotsViewerComparator comparator = (HotspotsViewerComparator) viewer.getComparator();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTree().setSortColumn(column.getColumn());
				viewer.getTree().setSortDirection(dir);
				viewer.refresh();
			}
		};
	}
}