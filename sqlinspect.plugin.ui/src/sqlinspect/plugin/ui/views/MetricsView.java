package sqlinspect.plugin.ui.views;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.ui.JavaElementComparator;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jdt.ui.StandardJavaElementContentProvider;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TreeColumn;

import sqlinspect.plugin.metrics.java.JavaMetricDesc;
import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.ui.utils.ImageUtils;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;

public class MetricsView extends AbstractSQLInspectViewer {

	public static final String ID = MetricsView.class.getName();
	public static final String MENU_ID = "sqlinspect.plugin.ui.popupmenu.MetricsView";

	@Inject
	private EMenuService menuService;

	public static final Object[] NO_ELEMENTS = {};

	@Inject
	private ProjectRepository projectRepository;

	@Inject
	private HotspotRepository hotspotRepository;

	@Inject
	private MetricRepository metricRepository;

	@Inject
	private TableAccessRepository tableAccessRepository;

	private static final class MetricsViewLabelProvider extends JavaElementLabelProvider {
		public MetricsViewLabelProvider(int style) {
			super(style);
		}

		@Override
		public Image getImage(Object element) {
			if (element instanceof Hotspot) {
				return ImageUtils.getIcon(ImageUtils.DATA_BACKUP);
			} else if (element instanceof IJavaElement) {
				return super.getImage(element);
			} else {
				return null;
			}
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Hotspot hotspot) {
				ASTNode exec = hotspot.getExec();
				if (exec != null) {
					ICompilationUnit unit = ASTUtils.getICompilationUnit(hotspot.getExec());
					if (unit != null) {
						return new StyledString(unit.getElementName() + ":" + hotspot.getStartLine());
					}
				}
				return new StyledString("");
			} else if (element instanceof Query query) {
				return new StyledString(query.getValue());
			} else if (element instanceof IJavaElement) {
				return super.getStyledText(element);
			} else {
				return null;
			}
		}
	}

	private static final class ViewContentProvider implements ITreeContentProvider {
		private final StandardJavaElementContentProvider standardProvider = new StandardJavaElementContentProvider();
		private final ProjectRepository projectRepository;
		private final HotspotRepository hotspotRepository;
		private final MetricRepository metricRepository;

		public ViewContentProvider(ProjectRepository projectRepository, HotspotRepository hotspotRepository,
				MetricRepository metricRepository) {
			super();
			this.projectRepository = projectRepository;
			this.hotspotRepository = hotspotRepository;
			this.metricRepository = metricRepository;
		}

		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof ICompilationUnit icu) {
				Project proj = projectRepository.getProject(icu.getJavaProject().getProject());
				if (proj != null) {
					List<Hotspot> hs = hotspotRepository.getHotspots(proj, icu);
					return hs.toArray();
				}
			} else if (element instanceof Hotspot hotspot) {
				List<Query> queries = hotspot.getQueries();
				if (queries != null) {
					return queries.toArray();
				}
			} else if (element instanceof IJavaElement) {
				List<IJavaElement> ret = new ArrayList<>();
				for (Object retElement : standardProvider.getChildren(element)) {
					if (retElement instanceof IJavaElement javaElement) {
						IJavaProject javaProject = javaElement.getJavaProject();
						Project proj = projectRepository.getProject(javaProject.getProject());

						Object m = metricRepository.getJavaMetrics(proj).get(javaElement);
						if (m != null) {
							ret.add(javaElement);
						}
					}
				}
				return ret.toArray();
			}
			return new Object[0];
		}

		@Override
		public Object[] getElements(Object input) {
			if (input instanceof ProjectRepository projectRepositoryInput) {
				List<IJavaElement> ret = new ArrayList<>();
				for (Project proj : projectRepositoryInput.getProjects()) {
					ret.add(proj.getIJavaProject());
				}
				return ret.toArray();
			} else {
				return new Object[0];
			}
		}

		@Override
		public Object getParent(Object element) {
			if (element instanceof Hotspot hotspot) {
				return hotspot.getProject().getIJavaProject();
			} else if (element instanceof Query query) {
				return query.getHotspot();
			} else if (standardProvider.getParent(element) != null) {
				return standardProvider.getParent(element);
			} else {
				return null;
			}
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof ProjectRepository projectRepositoryElement) {
				return !projectRepositoryElement.getProjects().isEmpty();
			} else if (element instanceof Hotspot hotspot) {
				return !hotspot.getQueries().isEmpty();
			} else if (element instanceof ICompilationUnit icu) {
				Project proj = projectRepository.getProject(icu.getJavaProject().getProject());
				return proj != null && !hotspotRepository.getHotspots(proj, icu).isEmpty();
			} else if (element instanceof IJavaElement) {
				return standardProvider.hasChildren(element);
			} else {
				return false;
			}
		}

	}

	private static final class JavaMetricLabelProvider extends CellLabelProvider {
		private final ProjectRepository projectRepository;
		private final MetricRepository metricRepository;
		private final TableAccessRepository tableAccessRepository;
		private final JavaMetricDesc metric;

		public JavaMetricLabelProvider(ProjectRepository projectRepository, MetricRepository metricRepository,
				TableAccessRepository tableAccessRepository, JavaMetricDesc metric) {
			super();
			this.projectRepository = projectRepository;
			this.metricRepository = metricRepository;
			this.tableAccessRepository = tableAccessRepository;
			this.metric = metric;
		}

		@Override
		public void update(ViewerCell cell) {
			Integer val = getJavaMetric(cell.getElement());
			if (val != null) {
				cell.setText(val.toString());
			} else {
				cell.setText("");
			}
		}

		public Integer getJavaMetric(Object element) {
			if (element instanceof IJavaElement javaElement) {
				IJavaProject javaProject = javaElement.getJavaProject();
				Project proj = projectRepository.getProject(javaProject.getProject());
				Map<JavaMetricDesc, Integer> elementMetrics = metricRepository.getJavaMetrics(proj).get(javaElement);

				if (elementMetrics != null) {
					return elementMetrics.get(metric);
				} else {
					return null;
				}
			} else if (element instanceof Hotspot hotspot) {
				if (metric == JavaMetricDesc.NumberOfQueries) {
					return hotspot.getQueries().size();
				} else if (metric == JavaMetricDesc.NumberOfTableAccesses) {
					return getHSTA(hotspot);
				} else if (metric == JavaMetricDesc.NumberOfColumnAccesses) {
					return getHSCA(hotspot);
				}
			} else if (element instanceof Query query) {
				if (metric == JavaMetricDesc.NumberOfTableAccesses) {
					return getTA(query);
				} else if (metric == JavaMetricDesc.NumberOfColumnAccesses) {
					return getCA(query);
				}
			}

			return null;
		}

		private Integer getHSTA(Hotspot hs) {
			int maxta = -1;
			for (Query q : hs.getQueries()) {
				Integer ta = getTA(q);
				if (ta != null && ta > maxta) {
					maxta = ta;
				}
			}
			if (maxta >= 0) {
				return maxta;
			} else {
				return null;
			}
		}

		private Integer getHSCA(Hotspot hs) {
			int maxca = -1;
			for (Query q : hs.getQueries()) {
				Integer ca = getCA(q);
				if (ca != null && ca > maxca) {
					maxca = ca;
				}
			}
			if (maxca >= 0) {
				return maxca;
			} else {
				return null;
			}
		}

		private Integer getTA(Query q) {
			List<Statement> stmts = q.getStatements();
			if (stmts.size() != 1) {
				return null;
			}
			Statement stmt = (Statement) (stmts.toArray()[0]);
			Project proj = q.getHotspot().getProject();
			Map<Table, Integer> tac = tableAccessRepository.getTableAccesses(proj).get(stmt);
			if (tac != null) {
				return tac.size();
			} else {
				return null;
			}
		}

		private Integer getCA(Query q) {
			List<Statement> stmts = q.getStatements();
			if (stmts.size() != 1) {
				return null;
			}
			Statement stmt = (Statement) (stmts.toArray()[0]);
			Project proj = q.getHotspot().getProject();
			Map<Column, Integer> cac = tableAccessRepository.getColumnAccesses(proj).get(stmt);
			if (cac != null) {
				return cac.size();
			} else {
				return null;
			}
		}

	}

	private static final class SQLMetricLabelProvider extends CellLabelProvider {
		private final MetricRepository metricRepository;
		private final sqlinspect.plugin.metrics.sql.SQLMetricDesc metric;

		public SQLMetricLabelProvider(MetricRepository metricRepository,
				sqlinspect.plugin.metrics.sql.SQLMetricDesc metric) {
			super();
			this.metricRepository = metricRepository;
			this.metric = metric;
		}

		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			if (element instanceof Query query) {
				Integer val = getQuerySQLMetric(query, metric);
				if (val != null) {
					cell.setText(val.toString());
				} else {
					cell.setText("");
				}
			}
		}

		public sqlinspect.plugin.metrics.sql.SQLMetricDesc getMetric() {
			return metric;
		}

		public Integer getQuerySQLMetric(Query q, sqlinspect.plugin.metrics.sql.SQLMetricDesc md) {
			List<Statement> stmts = q.getStatements();
			if (stmts.size() != 1) {
				return null;
			}
			Statement stmt = (Statement) (stmts.toArray()[0]);
			Project proj = q.getHotspot().getProject();
			Map<sqlinspect.plugin.metrics.sql.SQLMetricDesc, Integer> stmtMetrics = metricRepository.getSQLMetrics(proj)
					.get(stmt);
			if (stmtMetrics != null) {
				return stmtMetrics.get(md);
			} else {
				return null;
			}
		}
	}

	private static final class MetricsViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;
		private final JavaElementComparator javaComparator = new JavaElementComparator();

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (propertyIndex == 0) {
				ret = javaComparator.compare(viewer, e1, e2);
			} else {
				if (viewer instanceof TreeViewer treeViewer) {
					IBaseLabelProvider blp = treeViewer.getLabelProvider(propertyIndex);
					if (blp instanceof SQLMetricLabelProvider lp && e1 instanceof Query q1 && e2 instanceof Query q2) {
						Integer m1 = lp.getQuerySQLMetric(q1, lp.getMetric());
						Integer m2 = lp.getQuerySQLMetric(q2, lp.getMetric());
						if (m1 != null && m2 != null) {
							ret = Integer.compare(m1, m2);
						}
					} else if (blp instanceof JavaMetricLabelProvider lp) {
						Integer m1 = lp.getJavaMetric(e1);
						Integer m2 = lp.getJavaMetric(e2);
						if (m1 != null && m2 != null) {
							ret = Integer.compare(m1, m2);
						}
					}
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}

	}

	@Override
	public Viewer createViewer(Composite parent) {
		TreeViewer viewer = new TreeViewer(parent);
		viewer.setContentProvider(new ViewContentProvider(projectRepository, hotspotRepository, metricRepository));
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		JavaMetricDesc[] metrics = { JavaMetricDesc.NumberOfHotspots, JavaMetricDesc.NumberOfQueries,
				JavaMetricDesc.NumberOfTableAccesses, JavaMetricDesc.NumberOfColumnAccesses };
		sqlinspect.plugin.metrics.sql.SQLMetricDesc[] sqlMetrics = {
				sqlinspect.plugin.metrics.sql.SQLMetricDesc.NumberOfResultFields,
				sqlinspect.plugin.metrics.sql.SQLMetricDesc.NumberOfQueries,
				sqlinspect.plugin.metrics.sql.SQLMetricDesc.NestingLevel };

		int colNum = 0;
		TreeViewerColumn col = new TreeViewerColumn(viewer, SWT.LEFT);
		col.getColumn().setWidth(600);
		col.getColumn().setText("Name");
		col.setLabelProvider(new DelegatingStyledCellLabelProvider(
				new MetricsViewLabelProvider(JavaElementLabelProvider.SHOW_DEFAULT)));
		col.getColumn().addSelectionListener(getSelectionAdapter(col, colNum++));

		for (JavaMetricDesc m : metrics) {
			createJavaMetricColumn(viewer, m, colNum++);
		}

		for (sqlinspect.plugin.metrics.sql.SQLMetricDesc m : sqlMetrics) {
			createSQLMetricColumn(viewer, m, colNum++);
		}

		viewer.setInput(projectRepository);
		viewer.setComparator(new MetricsViewerComparator());

		createContextMenu(viewer);

		return viewer;
	}

	private void createContextMenu(TreeViewer viewer) {
		menuService.registerContextMenu(viewer.getControl(), MENU_ID);
	}

	private void createJavaMetricColumn(TreeViewer viewer, JavaMetricDesc metric, int colNum) {
		TreeViewerColumn col = new TreeViewerColumn(viewer, SWT.NONE);
		col.setLabelProvider(
				new JavaMetricLabelProvider(projectRepository, metricRepository, tableAccessRepository, metric));

		TreeColumn treeCol = col.getColumn();
		treeCol.setWidth(150);
		treeCol.setText(metric.getShortName());
		treeCol.setToolTipText(metric.getShortName() + ": " + metric.getDescription());
		treeCol.addSelectionListener(getSelectionAdapter(col, colNum));
	}

	private void createSQLMetricColumn(TreeViewer viewer, sqlinspect.plugin.metrics.sql.SQLMetricDesc metric,
			int colNum) {
		TreeViewerColumn col = new TreeViewerColumn(viewer, SWT.NONE);
		col.setLabelProvider(new SQLMetricLabelProvider(metricRepository, metric));

		TreeColumn treeCol = col.getColumn();
		treeCol.setWidth(150);
		treeCol.setText(metric.getShortName());
		treeCol.setToolTipText(metric.getShortName() + ": " + metric.getDescription());
		treeCol.addSelectionListener(getSelectionAdapter(col, colNum));
	}

	private SelectionAdapter getSelectionAdapter(final TreeViewerColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeViewer viewer = (TreeViewer) getViewer();
				MetricsViewerComparator comparator = (MetricsViewerComparator) viewer.getComparator();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTree().setSortColumn(column.getColumn());
				viewer.getTree().setSortDirection(dir);
				viewer.refresh();
			}
		};
	}

	public void expandAll() {
		TreeViewer viewer = (TreeViewer) getViewer();
		// workaround of mouse pointer flickering
		Runnable task = () -> {
			try {
				viewer.getTree().setRedraw(false);
				viewer.expandAll();
			} finally {
				viewer.getTree().setRedraw(true);
			}
		};
		BusyIndicator.showWhile(Display.getCurrent(), task);
	}

	public void collapseAll() {
		TreeViewer viewer = (TreeViewer) getViewer();
		viewer.collapseAll();
	}
}