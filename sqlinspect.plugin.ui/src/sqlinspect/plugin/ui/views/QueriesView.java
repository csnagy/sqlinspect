package sqlinspect.plugin.ui.views;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.utils.ASTUtils;

public class QueriesView extends AbstractSQLInspectViewer {

	public static final String ID = QueriesView.class.getName();
	public static final String MENU_ID = "sqlinspect.plugin.ui.popupmenu.QueriesView";

	private static final Logger LOG = LoggerFactory.getLogger(QueriesView.class);

	private QueriesViewerComparator comparator;

	@Inject
	private ProjectRepository projectRepository;

	@Inject
	private HotspotRepository hotspotRepository;

	@Inject
	private EMenuService menuService;

	private static class ViewContentProvider implements IStructuredContentProvider {
		private ProjectRepository projectRepository;
		private HotspotRepository hotspotRepository;

		public ViewContentProvider(ProjectRepository projectRepository, HotspotRepository hotspotRepository) {
			this.projectRepository = projectRepository;
			this.hotspotRepository = hotspotRepository;
		}

		@Override
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			// No implementation needed here.
		}

		@Override
		public void dispose() {
			// No implementation needed here.
		}

		@Override
		public Object[] getElements(Object parent) {
			List<Query> queries = new ArrayList<>();
			for (Project p : projectRepository.getProjects()) {
				queries.addAll(hotspotRepository.getQueries(p));
			}
			return queries.toArray();
		}
	}

	private static class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public String getColumnText(Object obj, int index) {
			Query q = (Query) obj;
			Hotspot hs = q.getHotspot();
			String text = "";
			switch (index) {
			case 0: /* project name */
				if (hs != null && hs.getProject() != null) {
					text = q.getHotspot().getProject().getName();
				} else {
					text = "No Unit";
				}
				break;
			case 1: /* compilation unit name */
				if (hs != null && hs.getUnit() != null) {
					text = ((CompilationUnit) hs.getExec().getRoot()).getJavaElement().getElementName();
				} else {
					text = "No Unit";
				}
				break;
			case 2: /* line number */
				if (hs != null && hs.getExec() != null && hs.getUnit() != null) {
					text = Integer.toString(ASTUtils.getLineNumber(hs.getExec()));
				} else {
					text = "No Line";
				}
				break;
			case 3: /* hotspot statement */
				if (hs != null && hs.getExec() != null) {
					text = hs.getExec().toString();
				} else {
					text = "No Exec";
				}
				break;
			case 4: /* query */
				String val = q.getValue();
				if (!val.isEmpty()) {
					text = val;
				} else {
					text = "{{empty}}";
				}
				break;
			case 5: /* hotspotfinder */
				if (hs != null && hs.getHotspotFinder() != null) {
					text = hs.getHotspotFinder().getName();
				} else {
					text = "No HotspotFinder";
				}
				break;
			default:
				LOG.error("Unhandled column index!");
				break;
			}

			return text;
		}

		@Override
		public Image getColumnImage(Object arg0, int arg1) {
			return null;
		}
	}

	private static class QueriesViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (!(e1 instanceof Query) || !(e2 instanceof Query)) {
				return ret;
			}
			Query q1 = (Query) e1;
			Query q2 = (Query) e2;
			Hotspot hs1 = q1.getHotspot();
			Hotspot hs2 = q2.getHotspot();

			if (hs1 == null || hs2 == null) {
				ret = 0;
			} else {

				switch (propertyIndex) {
				case 0: /* project name */
					ret = hs1.getProject().getName().compareTo(hs2.getProject().getName());
					break;
				case 1: /* compilation unit name */
					if (hs1.getExec() == null || hs2.getExec() == null) {
						ret = 0;
					} else {
						CompilationUnit cu1 = (CompilationUnit) hs1.getExec().getRoot();
						CompilationUnit cu2 = (CompilationUnit) hs2.getExec().getRoot();
						ret = cu1.getJavaElement().getElementName().compareTo(cu2.getJavaElement().getElementName());
					}
					break;
				case 2: /* line number */
					ret = Integer.compare(ASTUtils.getLineNumber(hs1.getExec()), ASTUtils.getLineNumber(hs2.getExec()));
					break;
				case 3: /* hotspot statement */
					if (hs1.getExec() != null && hs2.getExec() != null) {
						ret = hs1.getExec().toString().compareTo(hs2.getExec().toString());
					} else {
						ret = 0;
					}
					break;
				case 4: /* query */
					String q1text = q1.getValue();
					String q2text = q2.getValue();
					ret = q1text.compareTo(q2text);
					break;
				case 5: /* HotspotFinder */
					ret = hs1.getHotspotFinder().getName().compareTo(hs2.getHotspotFinder().getName());
					break;
				default:
					ret = 0;
					break;
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}

	}

	@Override
	protected Viewer createViewer(Composite parent) {
		TableViewer viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		viewer.setContentProvider(new ViewContentProvider(projectRepository, hotspotRepository));
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setInput(projectRepository);

		Table table = viewer.getTable();
		table.setLayoutData(new GridData(GridData.FILL_BOTH));

		final TableColumn tc1 = new TableColumn(table, SWT.LEFT);
		int colNum = 0;
		tc1.setText("Project");
		tc1.setWidth(100);
		tc1.addSelectionListener(getSelectionAdapter(tc1, colNum));

		final TableColumn tc2 = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc2.setText("Class");
		tc2.setWidth(150);
		tc2.addSelectionListener(getSelectionAdapter(tc2, colNum));

		final TableColumn tc3 = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc3.setText("Line");
		tc3.setWidth(60);
		tc3.addSelectionListener(getSelectionAdapter(tc3, colNum));

		final TableColumn tc4 = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc4.setText("Exec");
		tc4.setWidth(200);
		tc4.addSelectionListener(getSelectionAdapter(tc4, colNum));

		final TableColumn tc5 = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc5.setText("Query");
		tc5.setWidth(300);
		tc5.addSelectionListener(getSelectionAdapter(tc5, colNum));

		final TableColumn tc6 = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc6.setText("HotspotFinder");
		tc6.setWidth(300);
		tc6.addSelectionListener(getSelectionAdapter(tc6, colNum));

		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		comparator = new QueriesViewerComparator();
		viewer.setComparator(comparator);

		createContextMenu(viewer);

		return viewer;
	}

	private void createContextMenu(TableViewer viewer) {
		menuService.registerContextMenu(viewer.getControl(), MENU_ID);
	}

	private SelectionAdapter getSelectionAdapter(final TableColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableViewer viewer = (TableViewer) getViewer();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(column);
				refresh();
			}
		};
	}
}