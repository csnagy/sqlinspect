package sqlinspect.plugin.ui.views;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.ui.UI4Activator;

public abstract class AbstractSQLInspectView {

	private Composite parent;
	private Composite welcome;
	private Control main;
	private StackLayout layout;

	@Inject
	private ProjectRepository projectRepository;

	private static final char SBOLD = '\uFFFA';
	private static final char EBOLD = '\uFFFB';

	protected abstract Control createMain(Composite parent);

	private static void replaceBold(StyledText styledText, Font font) {
		int startoffset = styledText.getText().indexOf(SBOLD, 0);
		while (startoffset >= 0) {
			styledText.replaceTextRange(startoffset, 1, "");
			int endoffset = styledText.getText().indexOf(EBOLD, startoffset);
			if (endoffset > startoffset) {
				styledText.replaceTextRange(endoffset, 1, "");
				StyleRange style = new StyleRange();
				style.start = startoffset;
				style.length = endoffset - startoffset;
				style.font = font;
				styledText.setStyleRange(style);
				startoffset = styledText.getText().indexOf(SBOLD, endoffset);
			} else {
				startoffset = -1;
			}
		}
	}

	private void createWelcome(Composite parent) {
		welcome = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(welcome);

		String welcomeText = "Welcome to " + UI4Activator.PLUGIN_NAME + "!\n";
		String text = welcomeText + "\nUse " + UI4Activator.PLUGIN_NAME
				+ " to explore the SQL code embedded in your project.\n\n"
				+ "  1. Set configuration parameters for the analysis in the property page (" + UI4Activator.PLUGIN_NAME
				+ ") under your " + SBOLD + "project settings" + EBOLD + ".\n"
				+ "  2. Start an analysis of your project in the project menu " + SBOLD + UI4Activator.PLUGIN_NAME
				+ " > Analyze current project" + EBOLD + ".\n" + "  3. Open " + SBOLD + "Window > Show View > "
				+ UI4Activator.PLUGIN_NAME + EBOLD + " and explore the results in the " + SBOLD
				+ "Queries/Hotspots/Schema/Metrics views" + EBOLD + ".\n" + "  4. Look for SQL smells under the "
				+ SBOLD + "Problems view" + EBOLD + ".\n" + "  5. Have fun! ;-)";

		StyledText styledText = new StyledText(welcome, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(styledText);

		styledText.setText(text);

		FontData data = styledText.getFont().getFontData()[0];
		Font font1 = new Font(styledText.getDisplay(), data.getName(), data.getHeight() + 4, data.getStyle());
		StyleRange welcomeStyle = new StyleRange();
		welcomeStyle.start = 0;
		welcomeStyle.length = welcomeText.length();
		welcomeStyle.font = font1;
		styledText.setStyleRange(welcomeStyle);

		Font font2 = new Font(styledText.getDisplay(), data.getName(), data.getHeight() - 1, data.getStyle());
		Font font2b = new Font(styledText.getDisplay(), data.getName(), data.getHeight() - 1, SWT.BOLD);
		StyleRange mainStyle = new StyleRange();
		mainStyle.start = welcomeText.length();
		mainStyle.length = text.length() - welcomeText.length();
		mainStyle.font = font2;
		styledText.setStyleRange(mainStyle);
		replaceBold(styledText, font2b);

		welcome.addDisposeListener(e -> {
			font1.dispose();
			font2.dispose();
			font2b.dispose();
		});
	}

	@PostConstruct
	public final void createPartControl(Composite parent) {
		this.parent = parent;

		layout = new StackLayout();
		parent.setLayout(layout);

		createWelcome(parent);
		main = createMain(parent);

		if (projectRepository.getProjects().isEmpty() && welcome != null) {
			layout.topControl = welcome;
		} else {
			layout.topControl = main;
		}

		parent.layout();

		makeActions();
		contributeToActionBars();
	}

	protected void contributeToActionBars() {
		// normally there is nothing to do here
	}

	protected void makeActions() {
		// normally there is nothing to do here
	}

	protected Composite getParent() {
		return parent;
	}

	protected void showMain() {
		if (!layout.topControl.equals(main)) {
			layout.topControl = main;
			parent.layout();
		}
	}

	protected void showWelcome() {
		layout.topControl = welcome;
		parent.layout();
	}

	protected boolean isWelcome() {
		return layout != null && layout.topControl.equals(welcome);
	}

	@Focus
	public void setFocus() {
		if (main != null) {
			main.setFocus();
		} else {
			welcome.setFocus();
		}
	}

	public void refresh() {
		if (projectRepository.getProjects().isEmpty()) {
			if (!isWelcome()) {
				showWelcome();
			}
		} else {
			showMain();
		}
	}
}
