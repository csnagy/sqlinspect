package sqlinspect.plugin.ui.views;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public abstract class AbstractSQLInspectViewer extends AbstractSQLInspectView {

	private Viewer viewer;

	@Override
	protected Control createMain(Composite parent) {
		viewer = createViewer(parent);
		return viewer.getControl();
	}

	protected abstract Viewer createViewer(Composite parent);

	public Viewer getViewer() {
		return viewer;
	}

	@Override
	public void refresh() {
		super.refresh();
		getViewer().refresh();
	}
}
