package sqlinspect.plugin.ui.handlers;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.ui.utils.EditorUtils;
import sqlinspect.plugin.ui.views.AbstractSQLInspectViewer;

public class QuerySliceHandler {
	private static final Logger LOG = LoggerFactory.getLogger(QuerySliceHandler.class);

	@SuppressWarnings("unchecked")
	@CanExecute
	public boolean canExecute(@Active MPart activePart) {
		if (activePart != null && activePart.getObject() instanceof AbstractSQLInspectViewer sqlInspectViewer) {
			ISelection selection = sqlInspectViewer.getViewer().getSelection();
			if (selection instanceof StructuredSelection structuredSelection) {
				return structuredSelection.toList().stream().anyMatch(Query.class::isInstance);
			}
		}
		return false;
	}

	@Execute
	public void execute(@Active MPart activePart) {
		if (activePart.getObject() instanceof AbstractSQLInspectViewer sqlInspectViewer) {
			handleViewSelection(sqlInspectViewer);
		} else {
			LOG.error("Unhandled part for QuerySliceHandler: {}", activePart.getObject());
		}
	}

	private static void handleViewSelection(AbstractSQLInspectViewer view) {
		ISelection selection = view.getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof Query query) {
			ASTNode node = query.getHotspot().getExec();
			if (node != null) {
				CompilationUnit cu = query.getHotspot().getUnit();
				String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
				IPath path = Path.fromOSString(sourcePath);
				IEditorPart editorPart = EditorUtils.openEditor(path);
				if (editorPart instanceof ITextEditor textEditor) {
					EditorUtils.multiSelectInEditor(textEditor, query);
				}
			}
		} else {
			LOG.error("Selection must be a query!");
		}
	}
}
