package sqlinspect.plugin.ui.handlers;

import java.util.List;
import java.util.Optional;

import javax.inject.Named;

import org.eclipse.core.resources.IProject;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;
import sqlinspect.plugin.ui.utils.Editor4Utils;
import sqlinspect.plugin.ui.utils.EditorUtils;

public class AddPrepareHotspotDescHandler extends AddHotspotDescHandler {
	private static final Logger LOG = LoggerFactory.getLogger(AddPrepareHotspotDescHandler.class);

	@Execute
	public void execute(EPartService partService, @Active MPart activePart,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			@Named(IServiceConstants.ACTIVE_SELECTION) @org.eclipse.e4.core.di.annotations.Optional Object selection) {
		Optional<String> hsDesc = getHotspotDescription(activePart, selection, true);
		if (!hsDesc.isPresent()) {
			LOG.error(
					"Error retrieving the signature of the selected code range. Is this a method invocation? Please check the log for details.");
			return;
		}

		try {
			IEditorInput editorInput = Editor4Utils.getEditorInput(activePart);

			ICompilationUnit icu = EditorUtils.getEditorInputCU(editorInput);

			IProject proj = icu.getJavaProject().getProject();
			addPrepareHotspotToProject(proj, hsDesc.get());
			showSuccess(shell,
					"'" + hsDesc.get() + "' added to project '" + proj.getName() + "' as Connection.prepare Hotspot");
		} catch (IllegalStateException e) {
			LOG.error("Could not get editor input", e);
		}
	}

	public static void addPrepareHotspotToProject(IProject project, String hsDesc) {
		String prepareHotspotPreference = Activator.getDefault().getCurrentPreferenceStore(project)
				.getString(PreferenceConstants.JDBC_PSTMT);
		List<String> prepareHotspots = PreferenceHelper.parseStringList(prepareHotspotPreference);
		prepareHotspots.add(hsDesc);
		Activator.getDefault().getCurrentPreferenceStore(project).setValue(PreferenceConstants.JDBC_PSTMT,
				PreferenceHelper.createStringList(prepareHotspots));
	}

}
