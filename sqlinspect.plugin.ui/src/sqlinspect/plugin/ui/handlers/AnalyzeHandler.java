package sqlinspect.plugin.ui.handlers;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.preferences.PreferenceHelper;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.SmellRepository;
import sqlinspect.plugin.smells.android.common.AndroidSmell;
import sqlinspect.plugin.smells.android.common.AndroidSmellCertKind;
import sqlinspect.plugin.smells.sql.common.SQLSmell;
import sqlinspect.plugin.smells.sql.common.SQLSmellCertKind;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.plugin.ui.utils.HandlerUtils;
import sqlinspect.plugin.ui.views.AbstractSQLInspectView;
import sqlinspect.plugin.ui.views.HotspotsView;
import sqlinspect.plugin.ui.views.MetricsView;
import sqlinspect.plugin.ui.views.QueriesView;
import sqlinspect.plugin.ui.views.SchemaView;
import sqlinspect.plugin.ui.viewsupport.AndroidMarkers;
import sqlinspect.plugin.ui.viewsupport.SQLMarkers;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.sql.BenchMark;

public class AnalyzeHandler {
	private static final Logger LOG = LoggerFactory.getLogger(AnalyzeHandler.class);

	@Inject
	private ProjectRepository projectRepository;
	@Inject
	private HotspotRepository hotspotRepository;
	@Inject
	private SmellRepository smellRepository;

	@CanExecute
	public boolean canExecute(
			@Named(IServiceConstants.ACTIVE_SELECTION) @org.eclipse.e4.core.di.annotations.Optional Object selection) {
		return selection != null && isJavaProjectSelected(selection) || isJavaElementSelected(selection);
	}

	@Execute
	public void execute(EPartService partService, ESelectionService selectionService,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		final Optional<IProject> selectedProject = HandlerUtils.getSelectedProject(selectionService.getSelection());

		try {
			if (!selectedProject.isPresent() || !(selectedProject.get().hasNature(JavaCore.NATURE_ID))) {
				MessageDialog.openError(shell, Activator.PLUGIN_ID, "The selected project has to be a Java Project!");
				return;
			}
		} catch (CoreException e) {
			LOG.error("Could not retrieve project nature.", e);
			return;
		}

		final Project project = projectRepository.createProject(selectedProject.get());

		if (project.isAnalyzed()) {
			boolean ret = MessageDialog.openConfirm(shell, Activator.PLUGIN_ID,
					"This project has been analyzed already, the results of the previous analysis will be lost. Are you sure you want to reanalyze it?");
			if (!ret) {
				return;
			}
		}

		Job job = new Job("Analyzing " + project.getName()) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
				subMonitor.setTaskName("Analyzing project " + project.getName());
				BenchMark bm = new BenchMark();
				IJavaProject javaProject = project.getIJavaProject();
				bm.startAll(javaProject.getElementName() + ":ALL");

				ProjectAnalyzer projectAnalyzer = Activator.getDefault().createProjectAnalyzer(project);

				clearMarkers(javaProject);

				boolean loadSchema = isLoadSchema(project);
				List<String> schemaFiles = getSchemaFiles(project);

				if (!loadSchema) {
					subMonitor.setTaskName("Parsing schemas: " + String.join(", ", schemaFiles));
					subMonitor.worked(5);
					projectAnalyzer.parseSchemaFromFiles(schemaFiles);
				}

				subMonitor.setTaskName("Extracting hotspots.");
				subMonitor.setWorkRemaining(90);
				bm.run(javaProject.getElementName() + ":HOTSPOTS", () -> {
					final Optional<IJavaElement> selectedElement = getSelectedElement(selectionService);
					try {
						projectAnalyzer.extractHotspotsOfJavaElement(selectedElement.orElse(null),
								subMonitor.split(30));
					} catch (Exception e) {
						LOG.error("Error extracting hotspots {}: ", selectedElement, e);
						showError(shell, "Error occurred while extracting hotspots from "
								+ (selectedElement.isEmpty() ? project.getIProject().getName() : selectedElement.get())
								+ "! See the log file for details!");
					}
				});

				subMonitor.setTaskName("Parsing SQL.");
				bm.run(javaProject.getElementName() + ":QUERIES", () -> {
					try {
						projectAnalyzer.parseQueries(subMonitor.split(40));
					} catch (Exception e) {
						LOG.error("Error analyzing project {}: ", project.getIProject().getName(), e);
						showError(shell, "Error occurred while analyzing queries from the project "
								+ project.getIProject().getName() + "! See the log file for details!");
					}
				});

				if (isRunSQLMetrics(project)) {
					subMonitor.setTaskName("Calculate SQL metrics.");
					bm.run(javaProject.getElementName() + ":SQLMETRICS", projectAnalyzer::runSQLMetrics);

					subMonitor.setTaskName("Calculate Java metrics.");
					bm.run(javaProject.getElementName() + ":JAVAMETRICS", projectAnalyzer::runJavaMetrics);
				}

				if (isRunSmellDetectors(project)) {
					subMonitor.setTaskName("Run SQL smells.");
					bm.run(javaProject.getElementName() + ":SQLSMELLS", projectAnalyzer::runSQLSmellDetectors);
				}

				subMonitor.setTaskName("Run Android smells.");
				bm.run(javaProject.getElementName() + ":ANDROIDSMELLS", projectAnalyzer::runAndroidSmellDetectors);

				if (isRunTAA(project)) {
					subMonitor.setTaskName("Run Table Access Analysis.");
					bm.run(javaProject.getElementName() + ":TAA", projectAnalyzer::runTAA);
				}

				subMonitor.worked(10);
				subMonitor.setTaskName("Update views.");
				bm.run(javaProject.getElementName() + ":UPDATEVIEWS", () -> {
					updateViews(partService);
					updateSQLSmellMarkers(project);
					updateAndroidSmellMarkers(project);
					updateHotspotMarkers(project);
				});

				project.setAnalyzed(true);
				bm.stopAll(javaProject.getElementName() + ":ALL");

				subMonitor.worked(8);
				boolean dumpStatistics = Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
						.getBoolean(PreferenceConstants.DUMP_STATISTICS);
				if (dumpStatistics) {
					subMonitor.setTaskName("Dump statistics.");

					bm.dumpFile(new File(project.getIProject().getName() + "-SQLInspect.stats"));
					hotspotRepository.dumpStats(project, new File(project.getIProject().getName() + "-Model.stats"));
				}

				subMonitor.worked(2);

				showSuccess(shell, project.getIProject().getName() + " project was analyzed successfully!");
				return Status.OK_STATUS;
			}

			private List<String> getSchemaFiles(final Project project) {
				String schemaFilesPreference = Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
						.getString(PreferenceConstants.DB_SCHEMA);
				String[] schemaFiles = PreferenceHelper.parseStringArray(schemaFilesPreference);
				return Arrays.asList(schemaFiles);
			}

			private boolean isLoadSchema(final Project project) {
				return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
						.getBoolean(PreferenceConstants.LOAD_SCHEMA_FROM_DB);
			}
		};

		job.schedule();
	}

	private boolean isJavaProjectSelected(Object selection) {
		if (selection instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			try {
				if (element instanceof IProject project && project.hasNature(JavaCore.NATURE_ID)) {
					return true;
				}
			} catch (CoreException e) {
				LOG.error("Could not get project nature.", e);
			}
		}
		return false;
	}

	private boolean isJavaElementSelected(Object selection) {
		if (selection instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			if (element instanceof IJavaElement) {
				return true;
			}
		}
		return false;
	}

	private Optional<IJavaElement> getSelectedElement(ESelectionService selectionService) {
		Object sel = selectionService.getSelection();
		if (sel instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			if (element instanceof IPackageFragment || element instanceof ICompilationUnit) {
				return Optional.of((IJavaElement) element);
			}
		}
		return Optional.empty();
	}

	private void showError(final Shell shell, final String message) {
		Runnable task = () -> MessageDialog.openError(shell, Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	private void showSuccess(final Shell shell, final String message) {
		Runnable task = () -> MessageDialog.openInformation(shell, Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	private void updateViews(EPartService partService) {
		List<String> views = List.of(QueriesView.ID, HotspotsView.ID, SchemaView.ID, MetricsView.ID);
		Runnable task = () -> {
			for (String viewId : views) {
				MPart viewPart = partService.findPart(viewId);
				if (viewPart instanceof AbstractSQLInspectView view) {
					view.refresh();
				} else {
					LOG.debug("Could not retrieve view: {}", viewId);
				}
			}
		};
		Display.getDefault().asyncExec(task);
	}

	private int toPriority(SQLSmellCertKind severity) {
		switch (severity) {
		case HIGH_CERTAINTY:
			return IMarker.PRIORITY_HIGH;
		case NORMAL_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		case LOW_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		default:
			return IMarker.PRIORITY_NORMAL;
		}
	}

	private void updateSQLSmellMarkers(Project project) {
		Set<SQLSmell> smells = smellRepository.getSQLSmells(project);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		for (SQLSmell s : smells) {
			try {
				String sourcePath = s.getNode().getPath();
				IPath path = Path.fromOSString(sourcePath);

				IFile file = workspace.getRoot().getFileForLocation(path.makeAbsolute());

				if (file == null) {
					LOG.error("Could not get file for path {}", path);
					continue;
				}

				LOG.debug("New sql smell marker for compilation unit: {}", file);

				final String message = s.getMessage();
				final int lineNo = s.getNode().getStartLine();
				final int prio = toPriority(s.getCertainty());
				final String kind = s.getKind().toString();
				final int sev = IMarker.SEVERITY_WARNING;

				IMarker[] prevmarkers = file.findMarkers(SQLMarkers.SQLSMELL, false, IResource.DEPTH_ZERO);
				boolean alreadyset = false;
				for (IMarker m : prevmarkers) {
					final String pmessage = m.getAttribute(IMarker.MESSAGE, null);
					final int plineNo = m.getAttribute(IMarker.LINE_NUMBER, 0);
					final int pprio = m.getAttribute(IMarker.PRIORITY, 0);
					final int psev = m.getAttribute(IMarker.SEVERITY, 0);
					final String pkind = m.getAttribute(SQLMarkers.SQLSMELL_ATTR_RULENAME, null);
					if (lineNo == plineNo && message.equals(pmessage) && prio == pprio && kind.equals(pkind)
							&& sev == psev) {
						alreadyset = true;
					}
				}

				if (!alreadyset) {
					final IMarker marker = file.createMarker(SQLMarkers.SQLSMELL);
					marker.setAttribute(IMarker.MESSAGE, message);
					marker.setAttribute(IMarker.PRIORITY, prio);
					marker.setAttribute(IMarker.LINE_NUMBER, lineNo);
					marker.setAttribute(IMarker.SEVERITY, sev);
					marker.setAttribute(SQLMarkers.SQLSMELL_ATTR_RULENAME, kind);
				}
			} catch (CoreException e) {
				LOG.error("Error creating marker!", e);
			}
		}
	}

	private void updateAndroidSmellMarkers(Project project) {
		Set<AndroidSmell> smells = smellRepository.getAndroidSmells(project);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		for (AndroidSmell s : smells) {
			try {
				IPath path = ASTUtils.getPath(s.getNode());
				IFile file = workspace.getRoot().getFile(path);
				LOG.debug("New android smell marker for compilation unit: {}", file);

				final String message = s.getMessage();
				final int lineNo = ASTUtils.getLineNumber(s.getNode());
				final int prio = toPriority(s.getCertainty());
				final String kind = s.getKind().toString();
				final int sev = IMarker.SEVERITY_WARNING;

				IMarker[] prevmarkers = file.findMarkers(AndroidMarkers.ANDROIDSMELL, false, IResource.DEPTH_ZERO);
				boolean alreadyset = false;
				for (IMarker m : prevmarkers) {
					final String pmessage = m.getAttribute(IMarker.MESSAGE, null);
					final int plineNo = m.getAttribute(IMarker.LINE_NUMBER, 0);
					final int pprio = m.getAttribute(IMarker.PRIORITY, 0);
					final int psev = m.getAttribute(IMarker.SEVERITY, 0);
					final String pkind = m.getAttribute(AndroidMarkers.SMELL_ATTR_RULENAME, null);
					if (lineNo == plineNo && message.equals(pmessage) && prio == pprio && kind.equals(pkind)
							&& sev == psev) {
						alreadyset = true;
					}
				}

				if (!alreadyset) {
					final IMarker marker = file.createMarker(AndroidMarkers.ANDROIDSMELL);
					marker.setAttribute(IMarker.MESSAGE, message);
					marker.setAttribute(IMarker.PRIORITY, prio);
					marker.setAttribute(IMarker.LINE_NUMBER, lineNo);
					marker.setAttribute(IMarker.SEVERITY, sev);
					marker.setAttribute(AndroidMarkers.SMELL_ATTR_RULENAME, kind);
				}
			} catch (CoreException e) {
				LOG.error("Error creating marker!", e);
			}
		}
	}

	private int toPriority(AndroidSmellCertKind certainty) {
		switch (certainty) {
		case HIGH_CERTAINTY:
			return IMarker.PRIORITY_HIGH;
		case NORMAL_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		case LOW_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		default:
			return IMarker.PRIORITY_NORMAL;
		}
	}

	private void clearMarkers(IJavaProject pr) {
		LOG.debug("Deleting markers for project: {}", pr.getElementName());

		try {
			IResource resource = pr.getCorrespondingResource();
			clearMarkers(resource, IResource.DEPTH_INFINITE);
		} catch (JavaModelException e) {
			LOG.error("Could not delete markers! Could not get resource for project!", e);
		}
	}

	private void clearMarkers(IResource resource, int depth) {
		try {
			resource.deleteMarkers(SQLMarkers.SQLSMELL, true, depth);
			resource.deleteMarkers(SQLMarkers.SQLQUERY, true, depth);
			resource.deleteMarkers(AndroidMarkers.ANDROIDSMELL, true, depth);
		} catch (CoreException e) {
			LOG.error("Could not delete markers for resource!", e);
		}
	}

	private void updateHotspotMarkers(Project project) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		for (Query q : hotspotRepository.getQueries(project)) {
			CompilationUnit cu = q.getHotspot().getUnit();
			String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
			IPath path = Path.fromOSString(sourcePath);
			IFile file = workspace.getRoot().getFile(path);
			ICompilationUnit compUnit = (ICompilationUnit) JavaCore.create(file);

			if (compUnit == null) {
				LOG.error("Could not get compilation unit for query: {}", q);
				continue;
			}

			LOG.debug("New query marker for compilation unit: {}", sourcePath);

			try {
				IResource resource = compUnit.getCorrespondingResource();

				if (resource == null) {
					LOG.error("Could not get resource for compilation unit! {}", path.toOSString());
					return;
				}

				IMarker marker = resource.createMarker(SQLMarkers.SQLQUERY);
				marker.setAttribute(IMarker.MESSAGE, "Query: " + q.getValue());
				marker.setAttribute(IMarker.PRIORITY, IMarker.PRIORITY_LOW);
				marker.setAttribute(IMarker.LINE_NUMBER, ASTUtils.getLineNumber(q.getHotspot().getExec()));
				marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO);
			} catch (CoreException e) {
				LOG.error("Error creating hotspot marker!", e);
			}
		}
	}

	private boolean isRunSmellDetectors(Project project) {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getBoolean(PreferenceConstants.RUN_SMELL_DETECTORS);
	}

	private boolean isRunTAA(Project project) {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getBoolean(PreferenceConstants.RUN_TABLE_ACCESS);
	}

	private boolean isRunSQLMetrics(Project project) {
		return Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getBoolean(PreferenceConstants.RUN_SQL_METRICS);
	}
}
