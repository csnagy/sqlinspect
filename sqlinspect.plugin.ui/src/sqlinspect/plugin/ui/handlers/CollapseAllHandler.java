package sqlinspect.plugin.ui.handlers;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import sqlinspect.plugin.ui.views.HotspotsView;
import sqlinspect.plugin.ui.views.MetricsView;

public class CollapseAllHandler {
	@Execute
	public void execute(@Active MPart activePart) {
		if (activePart != null && activePart.getObject() instanceof HotspotsView hotspotsView) {
			hotspotsView.collapseAll();
		} else if (activePart != null && activePart.getObject() instanceof MetricsView metricsView) {
			metricsView.collapseAll();
		}
	}
}
