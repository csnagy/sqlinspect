package sqlinspect.plugin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.search.ui.NewSearchUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.plugin.ui.search.TAASearchQuery;
import sqlinspect.plugin.ui.views.SchemaView;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;

public class SearchTableAccessesHandler {
	private static final Logger LOG = LoggerFactory.getLogger(SearchTableAccessesHandler.class);

	@Inject
	private ASGRepository asgRepository;

	@Inject
	private TableAccessRepository tableAccessRepository;

	@Inject
	private QueryRepository queryRepository;

	@SuppressWarnings("unchecked")
	@CanExecute
	public boolean canExecute(@Active MPart activePart) {
		if (activePart != null && activePart.getObject() instanceof SchemaView schemaView) {
			ISelection selection = schemaView.getViewer().getSelection();
			if (selection instanceof StructuredSelection structuredSelection) {
				return structuredSelection.toList().stream().anyMatch(s -> s instanceof Table || s instanceof Column);
			}
		}
		return false;
	}

	@Execute
	public void execute(@Active MPart activePart) {
		if (activePart != null && activePart.getObject() instanceof SchemaView schemaView) {
			handleViewSelection(schemaView);
		} else {
			LOG.error("Unhandled part for SearchTableAccessesHandler: {}", activePart);
		}
	}

	private void handleViewSelection(SchemaView view) {
		ISelection selection = view.getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		Table table = null;
		Column column = null;
		if (obj instanceof Table t) {
			table = t;
		} else if (obj instanceof Column c) {
			column = c;
			table = (Table) column.getParent();
		} else {
			LOG.error("Selection is not a table or column!");
			return;
		}

		if (table != null) {
			Project proj = asgRepository.getParentProject(table);
			TAASearchQuery query = new TAASearchQuery(proj, table, column, tableAccessRepository, queryRepository);
			NewSearchUI.runQueryInForeground(null, query);
			NewSearchUI.activateSearchResultView();
		} else {
			LOG.error("Not a table/column was selected!");
		}
	}

}
