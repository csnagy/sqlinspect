package sqlinspect.plugin.ui.handlers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IProject;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.ui.dialogs.QueriesExportDialog;
import sqlinspect.plugin.ui.utils.HandlerUtils;
import sqlinspect.plugin.utils.JSONQueries;
import sqlinspect.plugin.utils.XMLQueries;

public class QueriesExportHandler {
	private static final String JSON = "json";
	private static final Logger LOG = LoggerFactory.getLogger(QueriesExportHandler.class);

	@Inject
	private ProjectRepository projectRepository;

	@Inject
	private HotspotRepository hotspotRepository;


	@CanExecute
	public boolean canExecute(
			@Named(IServiceConstants.ACTIVE_SELECTION) @org.eclipse.e4.core.di.annotations.Optional Object selection) {
		return selection != null && HandlerUtils.isJavaProjectSelected(selection)
				|| HandlerUtils.isJavaElementSelected(selection);
	}

	@Execute
	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		final Optional<IProject> selectedProject = HandlerUtils.getSelectedProject(selectionService.getSelection());
		if (!selectedProject.isPresent()) {
			MessageDialog.openError(shell, Activator.PLUGIN_ID, "Please, select a project first!");
			return;
		}

		Project project = projectRepository.getProject(selectedProject.get());
		if (project == null || !project.isAnalyzed()) {
			MessageDialog.openError(shell, Activator.PLUGIN_ID,
					"The project has not been analyzed yet. Please, analyze it first!");
			return;
		}

		QueriesExportDialog ed = new QueriesExportDialog(shell, project);
		ed.create();

		if (ed.open() == Window.OK) {
			String exportFile = ed.getOutputFile();

			String exportFormat = "";
			int i = exportFile.lastIndexOf('.');
			if (i > 0) {
				exportFormat = exportFile.substring(i + 1);
			}

			try {
				List<Query> queries = hotspotRepository.getQueries(project);
				if (JSON.equalsIgnoreCase(exportFormat)) {
					JSONQueries jsonQueries = new JSONQueries(new File(exportFile));
					jsonQueries.writeQueries(queries, false);
				} else {
					XMLQueries xmlQueries = new XMLQueries(new File(exportFile));
					xmlQueries.writeQueries(queries, false);
				}
			} catch (IOException e) {
				LOG.error("IO error while writing queries: ", e);
				MessageDialog.openError(shell, Activator.PLUGIN_ID, "Could not write to file. See log for details.");
			}

			MessageDialog.openInformation(shell, Activator.PLUGIN_ID,
					"Queries were successfully exported to " + exportFile);
		}
	}
}
