package sqlinspect.plugin.ui.handlers;

import java.util.Optional;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.ui.utils.Editor4Utils;
import sqlinspect.plugin.ui.utils.EditorUtils;
import sqlinspect.plugin.utils.ASTUtils;
import sqlinspect.plugin.utils.BindingUtil;

public class AddHotspotDescHandler {
	private static final String JAVA_LANG_STRING = "java.lang.String";
	private static final Logger LOG = LoggerFactory.getLogger(AddHotspotDescHandler.class);

	protected Optional<String> getHotspotDescOfMethodInvocation(MethodInvocation call, boolean withStringParam) {
		IMethodBinding mb = call.resolveMethodBinding();
		if (mb == null) {
			LOG.error("Could not get method binding for node: {}", call);
			return Optional.empty();
		}

		String qsig = BindingUtil.qualifiedSignature(mb);
		if (withStringParam) {
			int stringParam = getStringParamNum(mb);

			if (stringParam == 0) {
				LOG.error("The method must have at least one parameter! {}", mb);
				return Optional.empty();
			}
			return Optional.of(qsig + "/" + stringParam);
		} else {
			return Optional.of(qsig);
		}
	}

	protected Optional<ASTNode> getSelectedNode(MPart editorPart, Object selection) {
		IEditorInput editorInput;
		try {
			editorInput = Editor4Utils.getEditorInput(editorPart);
		} catch (IllegalStateException e) {
			LOG.error("Could not get editorinput", e);
			return Optional.empty();
		}
		ICompilationUnit icu = EditorUtils.getEditorInputCU(editorInput);
		if (icu == null) {
			LOG.error("Could not get compilation unit for editor!");
			return Optional.empty();
		}

		if (!(selection instanceof ITextSelection)) {
			LOG.error("Selection is not a text selection!");
			return Optional.empty();
		}

		ITextSelection textsel = (ITextSelection) selection;
		return Optional.ofNullable(ASTUtils.findASTNodeInICU(icu, textsel.getOffset(), textsel.getLength()));
	}

	protected Optional<MethodInvocation> getSelectedMethodInvocation(MPart editorPart, Object selection) {
		Optional<ASTNode> node = getSelectedNode(editorPart, selection);
		if (!node.isPresent()) {
			LOG.error("No node selected.");
			return Optional.empty();
		}

		ASTNode miNode = ASTUtils.getParent(node.get(), MethodInvocation.class);

		if (!(miNode instanceof MethodInvocation)) {
			LOG.error("Selection must be a method invocation!");
			return Optional.empty();
		}

		return Optional.of((MethodInvocation) miNode);
	}

	protected Optional<String> getHotspotDescription(MPart editorPart, Object selection, boolean withStringParam) {
		Optional<MethodInvocation> mi = getSelectedMethodInvocation(editorPart, selection);

		if (!mi.isPresent()) {
			LOG.error("Could not get method invocation!");
			return Optional.empty();
		}

		return getHotspotDescOfMethodInvocation(mi.get(), withStringParam);
	}

	protected void showError(Shell shell, final String message) {
		Runnable task = () -> MessageDialog.openError(shell, Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	protected void showSuccess(Shell shell, final String message) {
		Runnable task = () -> MessageDialog.openInformation(shell, Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	private int getStringParamNum(IMethodBinding binding) {
		ITypeBinding[] parTypes = binding.getParameterTypes();
		for (int i = 0; i < parTypes.length; i++) {
			ITypeBinding pt = parTypes[i];
			if (JAVA_LANG_STRING.equals(pt.getQualifiedName())) {
				return i + 1;
			}
		}

		return 0;
	}
}
