package sqlinspect.plugin.ui.handlers;

import java.io.File;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IProject;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.MetricRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.ui.dialogs.SQLMetricsExportDialog;
import sqlinspect.plugin.ui.utils.HandlerUtils;

public class SQLMetricsExportHandler {
	@Inject
	private ProjectRepository projectRepository;
	
	@Inject
	private MetricRepository metricRepository;

	@CanExecute
	public boolean canExecute(
			@Named(IServiceConstants.ACTIVE_SELECTION) @org.eclipse.e4.core.di.annotations.Optional Object selection) {
		return selection != null && HandlerUtils.isJavaProjectSelected(selection)
				|| HandlerUtils.isJavaElementSelected(selection);
	}

	@Execute
	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		final Optional<IProject> selectedProject = HandlerUtils.getSelectedProject(selectionService.getSelection());
		if (!selectedProject.isPresent()) {
			MessageDialog.openError(shell, Activator.PLUGIN_ID, "Please, select a project first!");
			return;
		}

		Project project = projectRepository.getProject(selectedProject.get());
		if (project == null || !project.isAnalyzed()) {
			MessageDialog.openError(shell, Activator.PLUGIN_ID,
					"The project has not been analyzed yet. Please, analyze it first!");
			return;
		}

		SQLMetricsExportDialog ed = new SQLMetricsExportDialog(shell, project);
		ed.create();

		if (ed.open() == Window.OK) {
			String exportFile = ed.getOutputFile();
			if (exportFile == null || exportFile.isEmpty()) {
				return;
			}

			String exportFormat = "";

			int i = exportFile.lastIndexOf('.');
			if (i > 0) {
				exportFormat = exportFile.substring(i + 1);
			}

			metricRepository.reportSQLMetrics(project, new File(exportFile), exportFormat);
			MessageDialog.openInformation(shell, Activator.PLUGIN_ID,
					"SQL metrics were successfully exported to " + exportFile);
		}
	}
}
