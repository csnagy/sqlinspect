package sqlinspect.plugin.ui.handlers;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.ui.utils.EditorUtils;
import sqlinspect.plugin.ui.views.AbstractSQLInspectViewer;

public class ShowQueryHandler {
	private static final Logger LOG = LoggerFactory.getLogger(ShowQueryHandler.class);

	@SuppressWarnings("unchecked")
	@CanExecute
	public boolean canExecute(@Active MPart activePart) {
		if (activePart != null && activePart.getObject() instanceof AbstractSQLInspectViewer sqlInspectViewer) {
			ISelection selection = sqlInspectViewer.getViewer().getSelection();
			if (selection instanceof StructuredSelection structuredSelection) {
				return structuredSelection.toList().stream().anyMatch(Query.class::isInstance);
			}
		}
		return false;
	}

	@Execute
	public void execute(@Active MPart activePart) {
		if (activePart != null && activePart.getObject() instanceof AbstractSQLInspectViewer sqlInspectViewer) {
			ISelection selection = sqlInspectViewer.getViewer().getSelection();
			if (selection instanceof IStructuredSelection structuredSelection) {
				Object selectedElement = structuredSelection.getFirstElement();
				if (selectedElement instanceof Query query) {
					showQuery(query);
				} else {
					LOG.error("Selection must be a Query");
				}
			} else {
				LOG.error("Selection must be a StructuredSelection");
			}
		} else {
			LOG.error("Part must be an AbstractSQLInspectViewer");
		}
	}

	private static void showQuery(Query query) {
		ASTNode node = query.getHotspot().getExec();
		if (node != null) {
			int offset = node.getStartPosition();
			int length = node.getLength();
			CompilationUnit cu = query.getHotspot().getUnit();
			String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
			IPath path = Path.fromOSString(sourcePath);
			IEditorPart editorPart = EditorUtils.openEditor(path);
			if (editorPart instanceof ITextEditor textEditor) {
				EditorUtils.selectInEditor(textEditor, offset, length);
			}
		}
	}

}
