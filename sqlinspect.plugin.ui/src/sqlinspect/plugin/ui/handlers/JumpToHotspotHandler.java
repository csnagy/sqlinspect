package sqlinspect.plugin.ui.handlers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.eclipse.core.runtime.IPath;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.HotspotRepository;
import sqlinspect.plugin.ui.utils.Editor4Utils;
import sqlinspect.plugin.ui.views.AbstractSQLInspectViewer;
import sqlinspect.plugin.ui.views.HotspotsView;

public class JumpToHotspotHandler {
	private static final Logger LOG = LoggerFactory.getLogger(JumpToHotspotHandler.class);

	@Inject
	private HotspotRepository hotspotRepository;

	@SuppressWarnings("unchecked")
	@CanExecute
	public boolean canExecute(@Active MPart part, ESelectionService selectionService) {
		if (part != null && part.getObject() instanceof AbstractSQLInspectViewer sqlInspectViewer) {
			ISelection selection = sqlInspectViewer.getViewer().getSelection();
			if (selection instanceof StructuredSelection structuredSelection) {
				return structuredSelection.toList().stream().anyMatch(s -> s instanceof Query || s instanceof Hotspot);
			}
		} else if (part != null && selectionService.getSelection() instanceof ITextSelection) {
			return true;
		}
		return false;
	}

	@Execute
	public void execute(EPartService partService, ESelectionService selectionService) {
		MPart part = partService.getActivePart();
		if (part != null && part.getObject() instanceof AbstractSQLInspectViewer sqlInspectViewer) {
			handleQueriesViewSelection(sqlInspectViewer, partService);
		} else if (part != null) {
			handleEditorSelection(hotspotRepository, part, partService, selectionService);
		} else {
			LOG.error("NLL part for JumpToHotspotHandler!");
		}
	}

	private static void handleQueriesViewSelection(AbstractSQLInspectViewer view, EPartService partService) {
		ISelection selection = view.getViewer().getSelection();
		if (selection instanceof StructuredSelection structuredSelection) {
			// query selected in queries view
			@SuppressWarnings("unchecked")
			Stream<Query> queries = structuredSelection.toList().stream().filter(Query.class::isInstance)
					.map(Query.class::cast);
			List<Hotspot> hotspots = queries.map(Query::getHotspot).toList();
			jumpToHotspots(partService, hotspots);
		}
	}

	private static void handleEditorSelection(HotspotRepository hotspotRepository, MPart editor, EPartService partService,
			ESelectionService selectionService) {
		Object selection = selectionService.getSelection();
		if (selection instanceof ITextSelection textSelection) {
			// hotspot selected in text editor
			int startline = textSelection.getStartLine() + 1;
			Optional<IPath> path = Editor4Utils.getInputURI(editor);
			if (path.isPresent()) {
				jumpToHotspotByLocation(hotspotRepository, partService, path.get(), startline);
			}
		} else {
			LOG.error("No hotspot was selected!");
		}
	}

	private static void jumpToHotspotByLocation(HotspotRepository hotspotRepository, EPartService partService,
			IPath path, int startline) {
		List<Hotspot> hotspots = hotspotRepository.findHotspots(path, startline);
		jumpToHotspots(partService, hotspots);
	}

	private static void jumpToHotspots(EPartService partService, List<Hotspot> hotspots) {
		MPart hotspotsViewPart = partService.showPart(HotspotsView.ID, PartState.ACTIVATE);
		HotspotsView hv = (HotspotsView) hotspotsViewPart.getObject();
		hv.expandToHotspots();
		hv.select(hotspots);
	}
}
