package sqlinspect.plugin.ui.viewsupport;

public final class AndroidMarkers {
	public static final String ANDROIDSMELL = "sqlinspect.marker.AndroidSmell";
	public static final String SMELL_ATTR_RULENAME = "rulename";

	private AndroidMarkers() {
	}
}
