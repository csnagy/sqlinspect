package sqlinspect.plugin.ui.viewsupport;

import org.eclipse.jface.text.source.Annotation;

public class SQLAnnotation extends Annotation {
	public static final String QUERYPART = "sqlinspect.annotation.QueryPart";
	public static final String SQLSMELL = "sqlinspect.annotation.SQLSmell";
	public static final String SQLQUERY = "sqlinspect.annotation.SQLQuery";

	public SQLAnnotation(String type, String text) {
		super(type, false, text);
	}
}
