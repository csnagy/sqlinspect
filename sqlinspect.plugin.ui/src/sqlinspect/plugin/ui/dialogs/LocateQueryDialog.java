package sqlinspect.plugin.ui.dialogs;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.ui.utils.ImageUtils;

public class LocateQueryDialog extends TitleAreaDialog {

	private static final String DEFAULT_SQL = "SELECT * FROM t;";

	private String sqlQuery = DEFAULT_SQL;

	private Text sqlTextField;

	private final Project project;

	public LocateQueryDialog(Shell parentShell, Project project) {
		super(parentShell);
		this.project = project;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Locate SQL Query");
		setMessage("Locate the execution point of a given SQL query.", IMessageProvider.INFORMATION);
		setTitleImage(ImageUtils.getIcon(ImageUtils.EXPORT_4));
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		Label l = new Label(composite, SWT.NONE);
		l.setText("Selected project: " + project.getName());

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		l.setLayoutData(gd);

		l = new Label(composite, SWT.NONE);
		l.setText("SQL query:");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		gd.verticalSpan = 5;
		l.setLayoutData(gd);

		sqlTextField = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		gd = new GridData(GridData.FILL_BOTH);
		gd.minimumWidth = 150;
		sqlTextField.setLayoutData(gd);
		sqlTextField.setText(sqlQuery);

		return null;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	private void saveInput() {
		sqlQuery = sqlTextField.getText();
	}

	@Override
	protected void okPressed() {
		saveInput();
		super.okPressed();
	}

	public String getSQLQuery() {
		return sqlQuery;
	}
}
