package sqlinspect.plugin.ui.dialogs;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.ui.utils.ImageUtils;

public class TAAExportDialog extends TitleAreaDialog {

	private String outputFile = "output.xml";

	private Text outputTextField;

	private static final String[] FILTER_NAMES = { "XML Files (*.xml)", "Text Files (*.txt)", "All Files (*.*)" };

	// These filter extensions are used to filter which files are displayed.
	private static final String[] FILTER_EXTS = { "*.xml", "*.txt", "*.*" };

	private final Project project;

	public TAAExportDialog(Shell parentShell, Project project) {
		super(parentShell);
		this.project = project;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Export results of table access analysis");
		setMessage("Provide an output file (from the possible file formats).", IMessageProvider.INFORMATION);
		setTitleImage(ImageUtils.getIcon(ImageUtils.EXPORT_4));
	}

	@Override
	protected Control createDialogArea(final Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		outputFile = project.getName() + "-taa.xml";

		Label l = new Label(composite, SWT.NONE);
		l.setText("Selected project: " + project.getName());
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		l.setLayoutData(gd);

		l = new Label(composite, SWT.NONE);
		l.setText("Output file:");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		l.setLayoutData(gd);

		outputTextField = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.WRAP);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.minimumWidth = 150;
		outputTextField.setLayoutData(gd);
		outputTextField.setText(outputFile);

		Button fileButton = new Button(composite, SWT.PUSH);
		fileButton.setText("Browse");
		fileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// User has selected to open a single file
				FileDialog dlg = new FileDialog(parent.getShell(), SWT.SAVE);
				dlg.setFilterNames(FILTER_NAMES);
				dlg.setFilterExtensions(FILTER_EXTS);
				dlg.setFileName(outputFile);
				String fn = dlg.open();
				if (fn != null) {
					outputTextField.setText(fn);
				}
			}
		});

		return null;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	// save content of the Text fields because they get disposed
	// as soon as the Dialog closes
	private void saveInput() {
		outputFile = outputTextField.getText();
	}

	@Override
	protected void okPressed() {
		saveInput();
		super.okPressed();
	}

	public String getOutputFile() {
		return outputFile;
	}
}
