package sqlinspect.plugin.ui.search;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.text.Match;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.statm.Statement;
import sqlinspect.sql.asg.util.ASGUtils;

public class TAASearchQuery implements ISearchQuery {

	private final QuerySearchResult result = new QuerySearchResult(this);

	private TableAccessRepository tableAccessRepository;
	private QueryRepository queryRepository;

	private final Project project;
	private final Table table;
	private final Column column;

	public TAASearchQuery(Project selectedProject, Table selectedTable, Column selectedColumn, TableAccessRepository tableAccessRepository, QueryRepository queryRepository) {
		this.project = selectedProject;
		this.table = selectedTable;
		this.column = selectedColumn;
		this.tableAccessRepository = tableAccessRepository;
		this.queryRepository=queryRepository;
	}

	@Override
	public boolean canRerun() {
		return true;
	}

	@Override
	public boolean canRunInBackground() {
		return true;
	}

	@Override
	public String getLabel() {
		if (column != null) {
			return "Accesses of column '" + ASGUtils.getQualifiedName(column) + '\'';
		} else if (table != null) {
			return "Accesses of table '" + ASGUtils.getQualifiedName(table) + '\'';
		} else {
			return "Table access search";
		}
	}

	@Override
	public ISearchResult getSearchResult() {
		return result;
	}

	@Override
	public IStatus run(IProgressMonitor monitor) {
		if (project == null) {
			String message = "Error, no project was specified!";
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, message, null);
		}

		if (column != null) {
			Map<Statement, Map<Column, Integer>> columnAcc = tableAccessRepository.getColumnAccesses(project);
			for (Map.Entry<Statement, Map<Column, Integer>> e : columnAcc.entrySet()) {
				Map<Column, Integer> acc = e.getValue();
				if (acc.containsKey(column)) {
					Statement stmt = e.getKey();
					Query q = queryRepository.getQueryMap(project).get(stmt);
					if (q != null) {
						result.addMatch(new Match(q, 0, 0));
					}
				}
			}
		} else if (table != null) {
			Map<Statement, Map<Table, Integer>> tableAcc = tableAccessRepository.getTableAccesses(project);
			for (Map.Entry<Statement, Map<Table, Integer>> e : tableAcc.entrySet()) {
				Map<Table, Integer> acc = e.getValue();
				if (acc.containsKey(table)) {
					Statement stmt = e.getKey();
					Query q = queryRepository.getQueryMap(project).get(stmt);
					if (q != null) {
						result.addMatch(new Match(q, 0, 0));
					}
				}
			}
		} else {
			String message = "Error, no table/column was specified!";
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, message, null);
		}

		String message = "Search completed successfully";
		return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, message, null);
	}

}
