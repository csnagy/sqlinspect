package sqlinspect.plugin.ui.search;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.search.ui.ISearchResultPage;
import org.eclipse.search.ui.text.AbstractTextSearchResult;
import org.eclipse.search.ui.text.AbstractTextSearchViewPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Hotspot;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.ui.utils.EditorUtils;
import sqlinspect.plugin.utils.ASTUtils;

public class QuerySearchResultPage extends AbstractTextSearchViewPage implements ISearchResultPage {
	private static final Logger LOG = LoggerFactory.getLogger(QuerySearchResultPage.class);

	private TAASearchContentProvider contentProvider;
	private final QueriesViewerComparator comparator = new QueriesViewerComparator();
	private TableViewer viewer;

	private Action selectQueryInEditorAction;
	private Action highlightQueryInEditorAction;

	private static class TAASearchLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public String getColumnText(Object obj, int index) {
			Query q = (Query) obj;
			Hotspot hs = q.getHotspot();
			String text = "";
			switch (index) {
			case 0: /* project name */
				if (hs != null && hs.getProject() != null) {
					text = q.getHotspot().getProject().getName();
				} else {
					text = "No Unit";
				}
				break;
			case 1: /* compilation unit name */
				if (hs != null && hs.getUnit() != null) {
					text = ((CompilationUnit) hs.getExec().getRoot()).getJavaElement().getElementName();
				} else {
					text = "No Unit";
				}
				break;
			case 2: /* line number */
				if (hs != null && hs.getExec() != null && hs.getUnit() != null) {
					text = Integer.toString(ASTUtils.getLineNumber(hs.getExec()));
				} else {
					text = "No Line";
				}
				break;
			case 3: /* hotspot statement */
				if (hs != null && hs.getExec() != null) {
					text = hs.getExec().toString();
				} else {
					text = "No Exec";
				}
				break;
			case 4: /* query */
				String val = q.getValue();
				if (!val.isEmpty()) {
					text = val;
				} else {
					text = "{{empty}}";
				}
				break;
			default:
				LOG.error("Unhandled column index!");
				break;
			}

			return text;
		}

		@Override
		public Image getColumnImage(Object arg0, int arg1) {
			return null;
		}
	}

	private static class TAASearchContentProvider implements IStructuredContentProvider {
		public static final Object[] EMPTY_ARRAY = new Object[0];
		private AbstractTextSearchResult searchResult;
		private TableViewer tableViewer;

		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof AbstractTextSearchResult textSearchResult) {
				return textSearchResult.getElements();
			} else {
				return EMPTY_ARRAY;
			}
		}

		@Override
		public void dispose() {
			// nothing
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			tableViewer = (TableViewer) viewer;
			searchResult = (AbstractTextSearchResult) newInput;
		}

		public void elementsChanged(Object[] updatedElements) {
			for (Object element : updatedElements) {
				if (searchResult.getMatchCount(element) > 0) {
					if (tableViewer.testFindItem(element) != null) {
						tableViewer.refresh(element);
					} else {
						tableViewer.add(element);
					}
				} else {
					tableViewer.remove(element);
				}
			}
		}

		public void clear() {
			tableViewer.refresh();
		}
	}

	private static class QueriesViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;

		public QueriesViewerComparator() {
			super();
			this.propertyIndex = 0;
			direction = DESCENDING;
		}

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (!(e1 instanceof Query) || !(e2 instanceof Query)) {
				return ret;
			}
			Query q1 = (Query) e1;
			Query q2 = (Query) e2;
			Hotspot hs1 = q1.getHotspot();
			Hotspot hs2 = q2.getHotspot();

			if (hs1 == null || hs2 == null) {
				ret = 0;
			} else {

				switch (propertyIndex) {
				case 0: /* project name */
					ret = hs1.getProject().getName().compareTo(hs2.getProject().getName());
					break;
				case 1: /* compilation unit name */
					if (hs1.getExec() == null || hs2.getExec() == null) {
						ret = 0;
					} else {
						CompilationUnit cu1 = (CompilationUnit) hs1.getExec().getRoot();
						CompilationUnit cu2 = (CompilationUnit) hs2.getExec().getRoot();
						ret = cu1.getJavaElement().getElementName().compareTo(cu2.getJavaElement().getElementName());
					}
					break;
				case 2: /* line number */
					ret = Integer.compare(ASTUtils.getLineNumber(hs1.getExec()), ASTUtils.getLineNumber(hs2.getExec()));
					break;
				case 3: /* hotspot statement */
					if (hs1.getExec() != null && hs2.getExec() != null) {
						ret = hs1.getExec().toString().compareTo(hs2.getExec().toString());
					} else {
						ret = 0;
					}
					break;
				case 4: /* query */
					String q1text = q1.getValue();
					String q2text = q2.getValue();
					ret = q1text.compareTo(q2text);
					break;
				default:
					ret = 0;
					break;
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}

	}

	public QuerySearchResultPage() {
		super(AbstractTextSearchViewPage.FLAG_LAYOUT_FLAT);
	}

	@Override
	protected void clear() {
		if (contentProvider != null) {
			contentProvider.clear();
		}
	}

	@Override
	protected void configureTableViewer(TableViewer viewer) {
		this.viewer = viewer;
		viewer.setLabelProvider(new TAASearchLabelProvider());
		contentProvider = new TAASearchContentProvider();
		viewer.setContentProvider(contentProvider);
		viewer.setComparator(comparator);

		Table table = viewer.getTable();

		TableColumn tc = new TableColumn(table, SWT.LEFT);
		int colNum = 0;
		tc.setText("Project");
		tc.setWidth(100);
		tc.addSelectionListener(getSelectionAdapter(viewer, comparator, tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Class");
		tc.setWidth(150);
		tc.addSelectionListener(getSelectionAdapter(viewer, comparator, tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Line");
		tc.setWidth(60);
		tc.addSelectionListener(getSelectionAdapter(viewer, comparator, tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Exec");
		tc.setWidth(200);
		tc.addSelectionListener(getSelectionAdapter(viewer, comparator, tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Query");
		tc.setWidth(300);
		tc.addSelectionListener(getSelectionAdapter(viewer, comparator, tc, colNum));

		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		makeActions();
		hookDoubleClickAction();
	}

	private SelectionAdapter getSelectionAdapter(final TableViewer viewer, final QueriesViewerComparator comparator,
			final TableColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(column);
				viewer.refresh();
			}
		};
	}

	@Override
	protected void configureTreeViewer(TreeViewer viewer) {
		throw new IllegalStateException("Doesn't support tree mode.");
	}

	@Override
	protected void elementsChanged(Object[] objects) {
		if (contentProvider != null) {
			contentProvider.elementsChanged(objects);
		}
	}

	@Override
	protected void fillContextMenu(IMenuManager manager) {
		super.fillContextMenu(manager);
		manager.add(selectQueryInEditorAction);
		manager.add(highlightQueryInEditorAction);
	}

	private void makeActions() {
		selectQueryInEditorAction = new Action() {
			@Override
			public void run() {
				selectQueryInEditor();
			}
		};
		selectQueryInEditorAction.setText("Jump to query");
		selectQueryInEditorAction.setToolTipText("Select query execution point in the text editor.");

		highlightQueryInEditorAction = new Action() {
			@Override
			public void run() {
				highlightQueryInEditor();
			}
		};
		highlightQueryInEditorAction.setText("Highlight query");
		highlightQueryInEditorAction.setToolTipText("Highlight query parts in the text editor.");
	}

	private void hookDoubleClickAction() {
		IDoubleClickListener listener = event -> selectQueryInEditorAction.run();
		viewer.addDoubleClickListener(listener);
	}

	private void selectQueryInEditor() {
		ISelection selection = viewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof Query query) {
			ASTNode node = query.getHotspot().getExec();
			if (node != null) {
				int offset = node.getStartPosition();
				int length = node.getLength();
				CompilationUnit cu = query.getHotspot().getUnit();
				String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
				IPath path = Path.fromOSString(sourcePath);
				IEditorPart editorPart = EditorUtils.openEditor(path);
				if (editorPart instanceof ITextEditor textEditor) {
					EditorUtils.selectInEditor(textEditor, offset, length);
				}
			}

		} else {
			LOG.error("Selection must be a query!");
		}
	}

	private void highlightQueryInEditor() {
		ISelection selection = viewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof Query query) {
			ASTNode node = query.getHotspot().getExec();
			if (node != null) {
				CompilationUnit cu = query.getHotspot().getUnit();
				String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
				IPath path = Path.fromOSString(sourcePath);
				IEditorPart editorPart = EditorUtils.openEditor(path);
				if (editorPart instanceof ITextEditor textEditor) {
					EditorUtils.multiSelectInEditor(textEditor, query);
				}
			}

		} else {
			LOG.error("Selection must be a query!");
		}
	}
}
