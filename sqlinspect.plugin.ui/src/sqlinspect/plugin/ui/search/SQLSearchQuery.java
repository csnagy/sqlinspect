package sqlinspect.plugin.ui.search;

import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.text.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.Activator;
import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.preferences.PreferenceConstants;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.sqlan.ProjectAnalyzer;
import sqlinspect.sql.SQLDialect;
import sqlinspect.sql.asg.base.Base;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.statm.Statement;

public class SQLSearchQuery implements ISearchQuery {
	private static final Logger LOG = LoggerFactory.getLogger(SQLSearchQuery.class);

	private final QuerySearchResult result = new QuerySearchResult(this);
	private final String sql;
	private final Set<Project> projectScope = new HashSet<>();

	private static final String MESSAGE_SUCCESS = "Search completed successfully";
	private static final String MESSAGE_ERROR_NO_QUERY = "Error, no query was specified!";

	private final boolean ignoreLiterals;
	private final boolean ignoreNames;
	private final boolean ignoreRefs;

	private ProjectRepository projectRepository;
	private QueryRepository queryRepository;
	private ASGRepository asgRepository;

	public SQLSearchQuery(String sql, boolean ignoreLiterals, boolean ignoreNames, boolean ignoreRefs,
			ProjectRepository projectRepository, QueryRepository queryRepository, ASGRepository asgRepository) {
		this.sql = sql;
		this.ignoreLiterals = ignoreLiterals;
		this.ignoreNames = ignoreNames;
		this.ignoreRefs = ignoreRefs;
		this.projectRepository = projectRepository;
		this.queryRepository = queryRepository;
		this.asgRepository = asgRepository;
	}

	@Override
	public boolean canRerun() {
		return true;
	}

	@Override
	public boolean canRunInBackground() {
		return true;
	}

	@Override
	public String getLabel() {
		if (sql != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("Matches for '").append(sql).append('\'');
			if (!projectScope.isEmpty()) {
				sb.append(" under '");
				Project[] projects = projectScope.toArray(new Project[0]);
				for (int i = 0; i < projects.length - 1; i++) {
					sb.append(projects[i].getIProject().getName()).append(", ");
				}
				sb.append(projects[projects.length - 1].getIProject().getName()).append('\'');
			}
			return sb.toString();
		} else {
			return "Query search";
		}
	}

	@Override
	public ISearchResult getSearchResult() {
		return result;
	}

	@Override
	public IStatus run(IProgressMonitor monitor) {
		if (sql == null || sql.isEmpty()) {
			String message = MESSAGE_ERROR_NO_QUERY;
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, message, null);
		}

		Project[] projects = projectScope.isEmpty()
				? projectRepository.getProjects().toArray(new Project[projectRepository.getProjects().size()])
				: projectScope.toArray(new Project[0]);

		for (Project project : projects) {
			try {
				List<Statement> ret = searchStatement(project, sql, ignoreLiterals, ignoreNames, ignoreRefs);
				if (ret != null) {
					for (Statement stmt : ret) {
						Query query = queryRepository.getQueryMap(project).get(stmt);
						if (query != null) {
							result.addMatch(new Match(query, 0, 0));
						}
					}
				}
			} catch (IOException e) {
				LOG.error("IOException: ", e);
			}
		}

		return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, MESSAGE_SUCCESS, null);
	}

	public Set<Project> getProjectScope() {
		return projectScope;
	}

	private List<Statement> searchStatement(Project project, String sql, boolean ignoreLiterals, boolean ignoreNames,
			boolean ignoreRefs) throws IOException {
		LOG.debug("Searching for query: {}", sql);

		List<Statement> ref = ProjectAnalyzer.parseSQL(sql, getSQLDialect(project));

		if (ref.isEmpty()) {
			throw new IllegalArgumentException("Error parsing the statement.");
		}

		if (ref.size() != 1) {
			throw new IllegalArgumentException("Only one statement can be specified.");
		}

		Statement refStmt = ref.get(0);
		LOG.debug("Searching for statement: {}", refStmt);

		ArrayList<Statement> ret = new ArrayList<>();
		for (Database db : asgRepository.getASG(project).getRoot().getDatabases()) {
			for (Statement stmt : db.getStatements()) {
				if (!stmt.equals(refStmt)) {
					try {
						MethodHandles.Lookup lookup = MethodHandles.lookup();
						MethodHandle mh = lookup.findVirtual(refStmt.getClass(), "matchTree", MethodType
								.methodType(boolean.class, Base.class, boolean.class, boolean.class, boolean.class));
						boolean res = (boolean) mh.invoke(refStmt, stmt, ignoreLiterals, ignoreNames, ignoreRefs);
						if (res) {
							LOG.debug("Found matching statement: {}", stmt);
							ret.add(stmt);
						}
					} catch (Throwable e) {
						LOG.error("Could not invoke matchTree method: ", e);
					}
				}
			}
		}

		return ret;
	}

	private SQLDialect getSQLDialect(Project project) {
		String dialectString = Activator.getDefault().getCurrentPreferenceStore(project.getIProject())
				.getString(PreferenceConstants.DIALECT);
		return SQLDialect.getSQLDialect(dialectString);
	}

}
