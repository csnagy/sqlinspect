package sqlinspect.plugin.ui.search;


import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.text.AbstractTextSearchResult;
import org.eclipse.search.ui.text.IEditorMatchAdapter;
import org.eclipse.search.ui.text.IFileMatchAdapter;

public class QuerySearchResult extends AbstractTextSearchResult implements ISearchResult {

	private final ISearchQuery query;

	public QuerySearchResult(ISearchQuery query) {
		super();
		this.query = query;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getLabel() {
		if (query != null) {
			return query.getLabel() + ": " + getElements().length + " locations found.";
		} else {
			return "No locations found.";
		}
	}

	@Override
	public ISearchQuery getQuery() {
		return query;
	}

	@Override
	public String getTooltip() {
		return "Found locations.";
	}

	@Override
	public IEditorMatchAdapter getEditorMatchAdapter() {
		return null;
	}

	@Override
	public IFileMatchAdapter getFileMatchAdapter() {
		return null;
	}
}
