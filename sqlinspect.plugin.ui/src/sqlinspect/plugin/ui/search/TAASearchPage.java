package sqlinspect.plugin.ui.search;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.search.ui.ISearchPage;
import org.eclipse.search.ui.ISearchPageContainer;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Project;
import sqlinspect.plugin.repository.ASGRepository;
import sqlinspect.plugin.repository.ProjectRepository;
import sqlinspect.plugin.repository.QueryRepository;
import sqlinspect.plugin.repository.TableAccessRepository;
import sqlinspect.sql.asg.common.ASG;
import sqlinspect.sql.asg.schema.Column;
import sqlinspect.sql.asg.schema.Database;
import sqlinspect.sql.asg.schema.Schema;
import sqlinspect.sql.asg.schema.Table;
import sqlinspect.sql.asg.util.ASGUtils;

public class TAASearchPage extends DialogPage implements ISearchPage {

	private static final Logger LOG = LoggerFactory.getLogger(TAASearchPage.class);

	private ISearchPageContainer container;

	private Combo projectCombo;
	private Combo tableCombo;
	private Combo columnCombo;

	@Inject
	private ProjectRepository projectRepository;

	@Inject
	private ASGRepository asgRepository;

	@Inject
	private TableAccessRepository tableAccessRepository;

	@Inject
	private QueryRepository queryRepository;

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		Label label = new Label(composite, SWT.NONE);
		label.setText("Project: ");
		label.pack();

		projectCombo = new Combo(composite, SWT.READ_ONLY);
		List<String> projectNames = projectRepository.getProjectNames();
		GridData gd = new GridData();
		gd.minimumWidth = 150;
		projectCombo.setLayoutData(gd);
		if (projectNames != null && !projectNames.isEmpty()) {
			projectCombo.setItems(projectNames.toArray(new String[0]));
		} else {
			projectCombo.setItems("Analyze a project first!");
			projectCombo.setText(projectCombo.getItem(0));
		}
		projectCombo.pack();
		projectCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				projectSelected();
			}
		});

		label = new Label(composite, SWT.NONE);
		label.setText("Table: ");
		label.pack();

		tableCombo = new Combo(composite, SWT.READ_ONLY);
		tableCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				tableSelected();
			}
		});
		gd = new GridData();
		gd.minimumWidth = 150;
		tableCombo.setLayoutData(gd);

		label = new Label(composite, SWT.NONE);
		label.setText("Column: ");
		label.pack();

		columnCombo = new Combo(composite, SWT.READ_ONLY);
		gd = new GridData();
		gd.minimumWidth = 150;
		columnCombo.setLayoutData(gd);

		label = new Label(composite, SWT.NONE);
		label.setText("Note: if a column is not selected, the results will be shown at table level.");
		gd = new GridData();
		gd.horizontalSpan = 2;
		label.setLayoutData(gd);

		setControl(composite);
	}

	@Override
	public boolean performAction() {
		TAASearchQuery query = new TAASearchQuery(getSelectedProject(), getSelectedTable(), getSelectedColumn(),
				tableAccessRepository, queryRepository);
		NewSearchUI.runQueryInForeground(container.getRunnableContext(), query);
		return true;
	}

	@Override
	public void setContainer(ISearchPageContainer spc) {
		this.container = spc;
	}

	private List<String> getProjectTableNames(Project proj) {
		List<String> ret = new ArrayList<>();
		for (Database db : asgRepository.getASG(proj).getRoot().getDatabases()) {
			for (Schema sch : db.getSchemas()) {
				for (Table tab : sch.getTables()) {
					ret.add(ASGUtils.getQualifiedName(tab));
				}
			}
		}
		return ret;
	}

	private void projectSelected() {
		Project proj = getSelectedProject();
		if (proj != null) {
			List<String> tableNames = getProjectTableNames(proj);
			tableCombo.setItems(tableNames.toArray(new String[0]));
			tableCombo.pack();
			columnCombo.setItems();
			columnCombo.pack();
		} else {
			LOG.error("Could not update tables combo, got null project for selection from project list.");
		}
	}

	private List<String> getTableColumnNames(Table tab) {
		List<String> ret = new ArrayList<>();
		for (Column col : tab.getColumns()) {
			ret.add(col.getName() + ": " + ASGUtils.columnTypeToString(col));
		}
		return ret;
	}

	private void tableSelected() {
		Table tab = getSelectedTable();
		if (tab != null) {
			List<String> columnNames = getTableColumnNames(tab);
			columnCombo.setItems(columnNames.toArray(new String[0]));
			columnCombo.pack();
		} else {
			LOG.error("Could not update columns combo, got null table.");
		}
	}

	private Table findTable(Project proj, String qName) {
		if (proj != null) {
			ASG asg = asgRepository.getASG(proj);
			return ASGUtils.findTable(asg, qName);
		}
		return null;
	}

	private Project getSelectedProject() {
		return projectRepository.getProject(projectCombo.getText());
	}

	private Table getSelectedTable() {
		Project proj = getSelectedProject();
		return findTable(proj, tableCombo.getText());
	}

	private Column getSelectedColumn() {
		Table tab = getSelectedTable();
		String[] colParts = columnCombo.getText().split(":");
		return ASGUtils.findColumn(tab, colParts[0]);
	}

}
