package sqlinspect.plugin.ui.utils;

import java.util.Optional;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HandlerUtils {
	private static final Logger LOG = LoggerFactory.getLogger(HandlerUtils.class);

	private HandlerUtils() {
		// Private constructor to prevent instantiation.
	}

	public static boolean isJavaProjectSelected(Object selection) {
		if (selection instanceof IStructuredSelection structuredSelection) {
			try {
				Object element = structuredSelection.getFirstElement();
				if (element instanceof IProject project && project.hasNature(JavaCore.NATURE_ID)) {
					return true;
				}
			} catch (CoreException e) {
				LOG.error("Could not get project nature for selected project", e);
			}
		}
		return false;
	}

	public static boolean isJavaElementSelected(Object selection) {
		if (selection instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			if (element instanceof IJavaElement) {
				return true;
			}
		}
		return false;
	}

	public static Optional<IProject> getSelectedProject(Object selection) {
		if (selection instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			if (element instanceof IJavaElement javaElement) {
				return Optional.of(javaElement.getJavaProject().getProject());
			} else if (element instanceof IProject project) {
				return Optional.of(project);
			}
		}
		return Optional.empty();
	}
}
