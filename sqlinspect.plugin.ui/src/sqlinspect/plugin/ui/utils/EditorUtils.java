package sqlinspect.plugin.ui.utils;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.ITextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.plugin.model.Query;
import sqlinspect.plugin.model.QueryPart;
import sqlinspect.plugin.ui.viewsupport.SQLAnnotation;

// TODO E4: migrate EditorUtility
public final class EditorUtils {
	private static final Logger LOG = LoggerFactory.getLogger(EditorUtils.class);

	private EditorUtils() {
		// Private constructor to prevent instantiation.
	}

	public static IEditorPart getActiveEditor() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null) {
			IWorkbenchPage page = window.getActivePage();
			if (page != null) {
				return page.getActiveEditor();
			}
		} else {
			LOG.error("Could not get current workbench");
		}
		return null;
	}

	public static IEditorPart openEditor(IPath path) {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) {
			LOG.error("Could not get current workbench!");
			return null;
		}

		IWorkbenchPage page = window.getActivePage();
		if (page == null) {
			LOG.error("Could not get active page!");
			return null;
		}

		IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(path.toOSString());
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IFile file = workspace.getRoot().getFile(path);
		try {
			return page.openEditor(new FileEditorInput(file), desc.getId());
		} catch (PartInitException e) {
			LOG.error("Could not open editor!", e);
		}

		return null;
	}

	public static ITextEditor openTextEditor(IPath path) {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) {
			LOG.error("Could not get current workbench!");
			return null;
		}

		IWorkbenchPage page = window.getActivePage();
		if (page == null) {
			LOG.error("Could not get active page!");
			return null;
		}

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IFile file = workspace.getRoot().getFile(path);

		IJavaElement sourceJavaElement = JavaCore.create(file);

		try {
			return (ITextEditor) JavaUI.openInEditor(sourceJavaElement);
		} catch (PartInitException | JavaModelException e) {
			LOG.error("Error opening the editor: ", e);
		}

		return null;
	}

	public static IProject getSelectedProject(IWorkbenchWindow window) {
		ISelectionService service = window.getSelectionService();
		if (service == null) {
			LOG.error("Could not get selection service for current workbench.");
			return null;
		}

		ISelection sel = service.getSelection();
		if (sel instanceof IStructuredSelection structuredSelection) {
			Object element = structuredSelection.getFirstElement();
			if (element instanceof IProject project) {
				return project;
			} else if (element instanceof IResource resource) {
				return resource.getProject();
			} else if (element instanceof IAdaptable adaptable) {
				IResource res = adaptable.getAdapter(IResource.class);
				return res.getProject();
			} else if (element instanceof IJavaProject javaProject) {
				return javaProject.getProject();
			} else if (element instanceof IJavaElement javaElement) {
				IJavaProject javaProject = javaElement.getJavaProject();
				if (javaProject != null) {
					return javaProject.getProject();
				}
			} else {
				LOG.warn("Selected project could not be specified!");
			}
		}
		return null;
	}

	public static ICompilationUnit getEditorInputCU(IEditorInput editorInput) {
		ITypeRoot root = JavaUI.getEditorInputTypeRoot(editorInput);
		Object ret = root.getAdapter(ICompilationUnit.class);
		if (ret instanceof ICompilationUnit icu) {
			return icu;
		}
		return null;
	}

	public static void selectInEditor(ITextEditor editor, int offset, int length) {
		IEditorPart active = getActiveEditor();
		if (active == null) {
			return;
		}

		if (!active.equals(editor)) {
			editor.getSite().getPage().activate(editor);
		}
		editor.selectAndReveal(offset, length);
	}

	public static void multiSelectInEditor(ITextEditor editor, Query query) {
		IEditorPart active = getActiveEditor();
		if (active == null) {
			return;
		}

		if (!active.equals(editor)) {
			editor.getSite().getPage().activate(editor);
		}
		AnnotationModel annotationModel = (AnnotationModel) editor.getDocumentProvider()
				.getAnnotationModel(editor.getEditorInput());
		Iterator<Annotation> annotationIterator = annotationModel.getAnnotationIterator();
		while (annotationIterator.hasNext()) {
			Annotation currentAnnotation = annotationIterator.next();
			if (SQLAnnotation.QUERYPART.equals(currentAnnotation.getType())) {
				annotationModel.removeAnnotation(currentAnnotation);
			}
		}

		for (QueryPart qp : query.getRoot().getSubTree()) {
			// add the query parts
			ASTNode qpNode = qp.getNode();
			SQLAnnotation qpAnnotation = new SQLAnnotation(SQLAnnotation.QUERYPART,
					"Query part: " + qp.toString() + "\n String value: " + qp.getValue());
			Position qpPosition = new Position(qpNode.getStartPosition(), qpNode.getLength());
			annotationModel.addAnnotation(qpAnnotation, qpPosition);
			editor.setHighlightRange(qpNode.getStartPosition(), qpNode.getLength(), true);
		}

		// add the execution point
		SQLAnnotation annotation = new SQLAnnotation(SQLAnnotation.QUERYPART, "Query: " + query.getValue());
		ASTNode node = query.getHotspot().getExec();
		Position position = new Position(node.getStartPosition(), node.getLength());
		annotationModel.addAnnotation(annotation, position);
		editor.setHighlightRange(node.getStartPosition(), node.getLength(), true);
	}

}
