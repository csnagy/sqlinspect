package sqlinspect.plugin.ui.utils;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ImageUtils {
	private static final Logger LOG = LoggerFactory.getLogger(ImageUtils.class);

	public static final int ICON = 0;
	public static final int IMAGE = 1;
	private static final IPath ICONS_PATH = new Path("$nl$/icons");
	private static final IPath IMG_PATH = new Path("$nl$/images");
	public static final String REFRESH = "refresh.png";
	public static final String IMPORT_4 = "import-64.png";
	public static final String EXPORT_4 = "export-64.png";
	public static final String DATA_BACKUP = "data-backup-16.png";
	public static final String DATA_CONFIGURATION = "data-configuration-16.png";
	public static final String DATABASE = "database-16.png";
	public static final String DATABASE_2 = "database-32.png";
	public static final String DATABASE5 = "database-5-16.png";
	public static final String DATABASE5_4 = "database-5-64.png";
	public static final String DATABASE_DASHBOARD = "database-dashboard-16.png";
	public static final String DATABASE_DASHBOARD_4 = "database-dashboard-64.png";
	public static final String LIST_VIEW = "list-view-16.png";
	public static final String LIST_VIEW_4 = "list-view-64.png";
	public static final String COLUMN_VIEW = "column-view-16.png";
	public static final String COLUMN_VIEW_4 = "column-view-64.png";
	public static final String PROJECT = "project-16.png";
	public static final String PROJECT_4 = "project-64.png";
	public static final String TEXT_FILE = "text-file-16.png";
	public static final String ANALYTICS = "analytics-16.png";
	public static final String ARROW_31 = "arrow-31-16.png";
	public static final String ARROW_31_2 = "arrow-31-32.png";
	public static final String ARROW_31_4 = "arrow-31-64.png";
	public static final String ARROW_53 = "arrow-53-16.png";
	public static final String ARROW_53_2 = "arrow-53-32.png";
	public static final String ARROW_53_4 = "arrow-53-64.png";

	public static final String IMG_WELCOME_CONTEXTMENU = "welcome_contextmenu.png";
	public static final String IMG_WELCOME_CONTEXTMENU_SMALL = "welcome_contextmenu_small.png";

	/**
	 * Image utility class. Not to be instantiated.
	 */
	private ImageUtils() {
	}

	public static Image getIcon(String name) {
		Bundle bundle = FrameworkUtil.getBundle(ImageUtils.class);
		return createImageDescriptor(bundle, ICONS_PATH.append(name)).createImage();
	}

	public static Image getImage(String name) {
		Bundle bundle = FrameworkUtil.getBundle(ImageUtils.class);
		return createImageDescriptor(bundle, IMG_PATH.append(name)).createImage();
	}

	public static void setImageDescriptors(IAction action, String name, int type) {
		Bundle bundle = FrameworkUtil.getBundle(ImageUtils.class);
		ImageDescriptor id;
		if (type == ICON) {
			id = createImageDescriptor(bundle, ICONS_PATH.append(name));
		} else {
			id = createImageDescriptor(bundle, IMG_PATH.append(name));
		}

		if (id != null) {
			action.setDisabledImageDescriptor(id);
			action.setHoverImageDescriptor(id);
			action.setImageDescriptor(id);
		} else {
			action.setImageDescriptor(ImageDescriptor.getMissingImageDescriptor());
		}
	}

	public static ImageDescriptor createImageDescriptor(Bundle bundle, IPath path) {
		URL url = FileLocator.find(bundle, path, null);
		if (url != null) {
			return ImageDescriptor.createFromURL(url);
		} else {
			LOG.error("Could not find image {} in bundle {}", path, bundle);
		}
		return null;
	}

}
