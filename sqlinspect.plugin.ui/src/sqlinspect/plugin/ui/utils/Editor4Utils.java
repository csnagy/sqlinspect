package sqlinspect.plugin.ui.utils;

import java.util.Optional;

import org.eclipse.core.runtime.IPath;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.internal.e4.compatibility.CompatibilityEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("restriction")
public final class Editor4Utils {
	private static final Logger LOG = LoggerFactory.getLogger(Editor4Utils.class);

	private Editor4Utils() {
		// Private constructor to prevent instantiation.
	}

	public static Optional<IPath> getInputURI(MPart editorPart) {
		IEditorInput editorInput = getEditorInput(editorPart);
		if (editorInput instanceof FileEditorInput fileEditorInput) {
			IPath path = fileEditorInput.getFile().getFullPath();
			return Optional.of(path);
		} else {
			LOG.error("Editor is not a FileEditor");
			return Optional.empty();
		}
	}

	public static IEditorInput getEditorInput(MPart editorPart) {
		if (editorPart.getObject() instanceof CompatibilityEditor compatibilityEditor) {
			return compatibilityEditor.getEditor().getEditorInput();
		} else {
			throw new IllegalArgumentException("Part is not an editor");
		}
	}
}
